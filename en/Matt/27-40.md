---
version: 1
---
- **καὶ λέγοντες**: The word καὶ is a conjunction meaning "and." The participle λέγοντες is the nominative plural present active participle of λέγω, meaning "saying" or "they were saying." The phrase thus means "and saying."

- **Ὁ καταλύων τὸν ναὸν**: The article ὁ is the masculine nominative singular definite article, meaning "the." The participle καταλύων is the nominative singular present active participle of καταλύω, meaning "destroying" or "breaking down." The noun τὸν ναὸν is the accusative singular of ναός, meaning "temple." The phrase thus means "the one who is destroying the temple."

- **καὶ ἐν τρισὶν ἡμέραις οἰκοδομῶν**: The conjunction καὶ means "and." The preposition ἐν means "in." The cardinal number τρισὶν means "three." The noun ἡμέραις is the dative plural of ἡμέρα, meaning "days." The participle οἰκοδομῶν is the nominative singular present active participle of οἰκοδομέω, meaning "building." The phrase thus means "and in three days building."

- **σῶσον σεαυτόν**: The verb σῶσον is the second person singular aorist imperative of σῴζω, meaning "save." The pronoun σεαυτόν is the accusative singular reflexive pronoun, meaning "yourself." The phrase thus means "save yourself."

- **εἰ υἱὸς εἶ τοῦ θεοῦ**: The conjunction εἰ means "if." The noun υἱὸς is the nominative singular of υἱός, meaning "son." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The article τοῦ is the masculine genitive singular definite article, meaning "of the." The noun θεοῦ is the genitive singular of θεός, meaning "God." The phrase thus means "if you are the son of God."

- **κατάβηθι ἀπὸ τοῦ σταυροῦ**: The verb κατάβηθι is the second person singular aorist imperative of καταβαίνω, meaning "come down." The preposition ἀπὸ means "from." The article τοῦ is the masculine genitive singular definite article, meaning "of the." The noun σταυροῦ is the genitive singular of σταυρός, meaning "cross." The phrase thus means "come down from the cross."

The entire verse can be literally translated as: "And saying, 'The one who is destroying the temple and in three days building, save yourself if you are the son of God, come down from the cross.'"