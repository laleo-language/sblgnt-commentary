---
version: 1
---
- **καὶ τότε**: The conjunction καὶ means "and" and τότε means "then." So this phrase means "and then."
- **σκανδαλισθήσονται πολλοὶ**: The verb σκανδαλισθήσονται is the third person plural future passive indicative of σκανδαλίζω, which means "to stumble" or "to be offended." The adjective πολλοὶ means "many." So this phrase means "many will stumble" or "many will be offended."
- **καὶ ἀλλήλους παραδώσουσιν**: The conjunction καὶ means "and." The pronoun ἀλλήλους means "each other" or "one another." The verb παραδώσουσιν is the third person plural future active indicative of παραδίδωμι, which means "to betray" or "to hand over." So this phrase means "and they will betray each other" or "and they will hand each other over."
- **καὶ μισήσουσιν ἀλλήλους**: The conjunction καὶ means "and." The verb μισήσουσιν is the third person plural future active indicative of μισέω, which means "to hate." The pronoun ἀλλήλους means "each other" or "one another." So this phrase means "and they will hate each other."
 
Literal translation: "And then many will stumble and betray each other and hate each other."

The verse describes a future event where many people will stumble, betray one another, and hate one another.