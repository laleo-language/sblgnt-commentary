---
version: 1
---
- **οἱ δὲ ἀπῆλθον**: The word οἱ is the nominative plural of ὁ, which means "the." The conjunction δὲ means "but" or "and." The verb ἀπῆλθον is the third person plural aorist indicative of ἀπέρχομαι, meaning "they went away" or "they departed." The phrase thus means "but they went away."

- **πάλιν ἐξελθὼν**: The adverb πάλιν means "again" or "once more." The participle ἐξελθὼν is the nominative masculine singular aorist participle of ἐξέρχομαι, meaning "having gone out" or "having come out." The phrase thus means "again, having gone out."

- **περὶ ἕκτην καὶ ἐνάτην ὥραν**: The preposition περὶ means "about" or "around." The numeral ἕκτην is the accusative feminine singular of ἕκτος, meaning "sixth." The conjunction καὶ means "and." The numeral ἐνάτην is the accusative feminine singular of ἐνάτος, meaning "ninth." The noun ὥραν is the accusative singular of ὥρα, which means "hour." The phrase thus means "about the sixth and ninth hour."

- **ἐποίησεν ὡσαύτως**: The verb ἐποίησεν is the third person singular aorist indicative of ποιέω, meaning "he did" or "he made." The adverb ὡσαύτως means "in the same way" or "likewise." The phrase thus means "he did likewise."

The literal translation of the verse is: "But they went away. Again, having gone out, about the sixth and ninth hour, he did likewise."