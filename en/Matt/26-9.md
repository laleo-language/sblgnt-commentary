---
version: 1
---
- **ἐδύνατο γὰρ**: The verb ἐδύνατο is the third person singular imperfect indicative of δύναμαι, which means "he was able" or "he could." The word γὰρ is a conjunction that means "for" or "because." The phrase thus means "for he was able."

- **τοῦτο πραθῆναι**: The word τοῦτο is the accusative singular of οὗτος, which means "this." The verb πραθῆναι is the aorist passive infinitive of πράσσω, which means "to sell." The phrase thus means "to sell this."

- **πολλοῦ καὶ δοθῆναι πτωχοῖς**: The word πολλοῦ is the genitive singular of πολύς, which means "much" or "many." The conjunction καὶ means "and." The verb δοθῆναι is the aorist passive infinitive of δίδωμι, which means "to give." The noun πτωχοῖς is the dative plural of πτωχός, which means "poor." The phrase thus means "to give to many and to the poor."

The entire verse can be translated as: "For this could have been sold for much and given to the poor."