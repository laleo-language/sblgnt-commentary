---
version: 1
---
- **ὁ δὲ Ἰησοῦς**: The word ὁ is the definite article, meaning "the." The noun Ἰησοῦς is the nominative singular of Ἰησοῦς, which means "Jesus." The phrase thus means "Jesus."

- **πάλιν κράξας φωνῇ μεγάλῃ**: The adverb πάλιν means "again." The participle κράξας is the nominative singular masculine of κράζω, which means "crying out." The noun φωνῇ is the dative singular feminine of φωνή, which means "voice." The adjective μεγάλῃ is the dative singular feminine of μέγας, which means "loud" or "great." The phrase thus means "crying out with a loud voice."

- **ἀφῆκεν τὸ πνεῦμα**: The verb ἀφῆκεν is the third person singular aorist indicative of ἀφίημι, which means "he let go" or "he released." The article τὸ is the definite article, meaning "the." The noun πνεῦμα is the accusative singular of πνεῦμα, which means "spirit." The phrase thus means "he let go of the spirit."

Putting it all together, the verse can be translated as: "But Jesus, crying out with a loud voice, let go of the spirit."