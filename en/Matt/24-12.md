---
version: 1
---
- **καὶ διὰ τὸ πληθυνθῆναι τὴν ἀνομίαν**: The conjunction καὶ means "and." The preposition διὰ means "through" or "because of." The article τὸ is the accusative singular of ὁ, meaning "the." The verb πληθυνθῆναι is the aorist passive infinitive of πληθύνω, meaning "to increase" or "to multiply." The noun ἀνομίαν is the accusative singular of ἀνομία, which means "lawlessness" or "iniquity." The phrase thus means "and because of the increase of lawlessness."

- **ψυγήσεται ἡ ἀγάπη τῶν πολλῶν**: The verb ψυγήσεται is the future passive indicative of ψύχω, meaning "to grow cold" or "to become indifferent." The article ἡ is the nominative singular of ὁ, meaning "the." The noun ἀγάπη is the nominative singular of ἀγάπη, which means "love." The genitive plural τῶν πολλῶν means "of the many." The phrase thus means "love will grow cold for the many."

The literal translation of the entire verse is: "And because of the increase of lawlessness, love will grow cold for the many."