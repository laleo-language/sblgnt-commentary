---
version: 1
---
- **αὕτη** is the nominative singular feminine form of the pronoun οὗτος, meaning "this." It refers to the previous phrase, "ἡ μεγάλη καὶ πρώτη ἐντολή."

- **ἐστὶν** is the third person singular present indicative form of the verb εἰμί, meaning "to be." It indicates that something exists or is in a certain state. In this case, it means "is."

- **ἡ μεγάλη καὶ πρώτη** is a noun phrase consisting of two adjectives and a noun. **ἡ** is the definite article in the nominative singular feminine form, meaning "the." **μεγάλη** is the nominative singular feminine form of the adjective μέγας, meaning "great" or "big." **καὶ** is a conjunction meaning "and." **πρώτη** is the nominative singular feminine form of the adjective πρῶτος, meaning "first." **ἐντολή** is the nominative singular feminine form of the noun ἐντολή, meaning "commandment" or "instruction."

The phrase ἡ μεγάλη καὶ πρώτη ἐντολή means "the great and first commandment."

- The literal translation of the verse is: "This is the great and first commandment."