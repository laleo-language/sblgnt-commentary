---
version: 1
---
- **μωροὶ καὶ τυφλοί**: The word μωροὶ is the nominative plural of μωρός, which means "foolish" or "stupid." The word τυφλοί is also the nominative plural, but this time of τυφλός, which means "blind." The phrase μωροὶ καὶ τυφλοί translates to "fools and blind."

- **τίς γὰρ μείζων ἐστίν**: The word τίς is the nominative singular of τίς, which means "who." The word γὰρ is a conjunction meaning "for" or "because." The word μείζων is the nominative singular comparative form of μέγας, which means "greater." The verb ἐστίν is the third person singular present indicative of εἰμί, which means "to be." The phrase τίς γὰρ μείζων ἐστίν translates to "for who is greater."

- **ὁ χρυσὸς ἢ ὁ ναὸς ὁ ⸀ἁγιάσας τὸν χρυσόν**: The word ὁ is the definite article meaning "the." The word χρυσὸς is the nominative singular of χρυσός, which means "gold." The word ἢ is a conjunction meaning "or." The word ναὸς is the nominative singular of ναός, which means "temple." The word ὁ is again the definite article. The verb ἁγιάσας is the nominative singular articular participle of ἁγιάζω, which means "having sanctified" or "having made holy." The phrase τὸν χρυσόν is the accusative singular of χρυσός, meaning "the gold." The phrase ὁ χρυσὸς ἢ ὁ ναὸς ὁ ἁγιάσας τὸν χρυσόν translates to "the gold or the temple that sanctified the gold."

The literal translation of the verse is: "Fools and blind, for who is greater, the gold or the temple that sanctified the gold?"