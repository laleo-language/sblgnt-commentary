---
version: 1
---
- **καὶ ὑμεῖς**: The conjunction καὶ means "and," and the pronoun ὑμεῖς is the second person plural pronoun, meaning "you." So this phrase means "and you."

- **πληρώσατε τὸ μέτρον**: The verb πληρόω is the second person plural aorist imperative, meaning "fill." The noun μέτρον is the accusative singular of μέτρον, meaning "measure." So this phrase means "fill the measure."

- **τῶν πατέρων ὑμῶν**: The genitive plural article τῶν indicates possession, so it means "of the." The noun πατέρων is the genitive plural of πατήρ, meaning "fathers." The pronoun ὑμῶν is the second person plural pronoun, meaning "your." So this phrase means "of your fathers."

Putting it all together, the literal translation of the sentence is "And you, fill the measure of your fathers."

In this verse, there are no complex grammar constructions or ambiguous syntax. It is a straightforward command for the audience to fill the measure of their fathers.