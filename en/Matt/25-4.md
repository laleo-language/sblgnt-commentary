---
version: 1
---
- **αἱ δὲ φρόνιμοι**: The article αἱ is the feminine nominative plural of ὁ, which means "the." The adjective φρόνιμοι is the feminine nominative plural of φρόνιμος, meaning "wise" or "prudent." The phrase thus means "the wise (women)."

- **ἔλαβον ἔλαιον**: The verb ἔλαβον is the third person plural aorist indicative of λαμβάνω, meaning "they received" or "they took." The noun ἔλαιον is the accusative singular of ἔλαιον, which means "oil." The phrase thus means "they received oil."

- **ἐν τοῖς ἀγγείοις**: The preposition ἐν means "in." The article τοῖς is the masculine/neuter dative plural of ὁ. The noun ἀγγείοις is the dative plural of ἀγγεῖον, meaning "vessel" or "jar." The phrase thus means "in the vessels."

- **μετὰ τῶν λαμπάδων ἑαυτῶν**: The preposition μετὰ means "with." The article τῶν is the masculine/neuter genitive plural of ὁ. The noun λαμπάδων is the genitive plural of λαμπάς, meaning "lamp" or "torch." The pronoun ἑαυτῶν is the genitive plural reflexive pronoun, meaning "their own." The phrase thus means "with their own lamps."

- The entire verse could be literally translated as: "But the wise (women) took oil in the vessels with their own lamps."

In this verse, there are no particularly difficult or ambiguous grammatical constructions. It simply describes the actions of the wise women who took oil with them in their vessels along with their lamps.