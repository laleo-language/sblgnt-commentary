---
version: 1
---
- **πορευθεὶς**: This is the aorist participle of the verb πορεύομαι, meaning "to go" or "to journey." It is declined in the masculine nominative singular form, agreeing with the understood subject "he." The participle is used here to indicate that the action of going or journeying happened prior to the main verb.
- **ὁ τὰ πέντε τάλαντα λαβὼν**: This is a noun phrase consisting of an article, a noun, and a participle. The article ὁ is the masculine singular definite article meaning "the." The noun τάλαντα is declined in the accusative plural form and means "talents," a unit of currency in ancient Greece. The participle λαβὼν is the aorist active participle of the verb λαμβάνω, meaning "to take" or "to receive." The phrase as a whole means "having taken the five talents."
- **ἠργάσατο**: This is the third person singular aorist middle indicative form of the verb ἐργάζομαι, meaning "to work" or "to labor." The middle voice indicates that the subject is performing the action on themselves. The verb here is in the aorist tense, indicating a completed action in the past.
- **ἐν αὐτοῖς**: This is a prepositional phrase consisting of the preposition ἐν, meaning "in," and the pronoun αὐτοῖς, meaning "them." The pronoun αὐτοῖς refers back to the five talents mentioned earlier. The phrase as a whole means "in them."
- **ἐκέρδησεν**: This is the third person singular aorist indicative form of the verb κερδαίνω, meaning "to gain" or "to profit." The verb here is in the aorist tense, indicating a completed action in the past.
- **ἄλλα πέντε**: This is a noun phrase consisting of an adjective and a numeral. The adjective ἄλλα means "other" or "another," and it is declined in the accusative neuter plural form to agree with the understood noun τάλαντα. The numeral πέντε means "five" and is declined in the accusative neuter plural form. The phrase as a whole means "another five."

Literal translation: "Having gone, he who had taken the five talents worked in them and gained another five."

The passage is from Matthew 25:16, where Jesus tells a parable about the kingdom of heaven. The phrase describes one of the servants who took the five talents, worked with them, and gained five more talents.