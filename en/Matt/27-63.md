---
version: 1
---
- **λέγοντες**: This is the present participle of the verb λέγω, meaning "saying." It is in the nominative plural form, so it could be translated as "they were saying" or "saying."

- **Κύριε**: This is the vocative form of the noun Κύριος, which means "Lord." It is used here as a direct address to someone. It would be translated as "Lord."

- **ἐμνήσθημεν**: This is the first person plural aorist indicative passive form of the verb μιμνήσκομαι, which means "to remember." It is in the aorist tense, indicating a completed action in the past. It would be translated as "we remembered."

- **ὅτι**: This is a subordinating conjunction meaning "that" or "because." It introduces a subordinate clause that explains the reason for the previous verb.

- **ἐκεῖνος**: This is the nominative singular masculine form of the pronoun ἐκεῖνος, which means "that one" or "he." It refers to a specific person mentioned before or known from the context. It would be translated as "that one."

- **ὁ πλάνος**: This is the nominative singular masculine form of the noun πλάνος, which means "deceiver" or "impostor." It is used here as a descriptive noun to refer to someone.

- **εἶπεν**: This is the third person singular aorist indicative active form of the verb λέγω, meaning "he said." It is in the aorist tense, indicating a completed action in the past. It would be translated as "he said."

- **ἔτι**: This is an adverb meaning "still" or "yet." It is used here to indicate that the action of the previous verb is still true or relevant.

- **Μετὰ τρεῖς ἡμέρας ἐγείρομαι**: This is a complex phrase. Μετὰ is a preposition meaning "after." Τρεῖς is the numeral "three." ἡμέρας is the accusative plural form of ἡμέρα, meaning "day." ἐγείρομαι is the first person singular present middle or passive form of the verb ἐγείρω, which means "to rise" or "to be raised." The phrase can be translated as "After three days I rise" or "After three days I am raised."

The literal translation of the verse would be: "saying, 'Lord, we remembered that that deceiver said while still alive, 'After three days I rise.'"