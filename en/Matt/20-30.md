---
version: 1
---
- **καὶ ἰδοὺ δύο τυφλοὶ καθήμενοι παρὰ τὴν ὁδόν**: The word καὶ is a conjunction meaning "and." The word ἰδοὺ is an interjection that can be translated as "behold" or "look." The word δύο is the cardinal number "two." The word τυφλοὶ is the nominative plural form of τυφλός, which means "blind." The word καθήμενοι is the nominative plural masculine participle of κάθημαι, meaning "sitting." The word παρὰ is a preposition meaning "beside" or "near." The word τὴν is the accusative singular feminine article meaning "the." The word ὁδόν is the accusative singular feminine form of ὁδός, which means "road" or "way." The phrase can be translated as "And behold, two blind men sitting beside the road."

- **ἀκούσαντες ὅτι Ἰησοῦς παράγει**: The word ἀκούσαντες is the nominative plural masculine participle of ἀκούω, meaning "having heard." The word ὅτι is a conjunction meaning "that." The word Ἰησοῦς is the nominative singular form of Ἰησοῦς, which is the name "Jesus." The word παράγει is the present indicative active third person singular form of παράγω, meaning "he is passing by." The phrase can be translated as "Having heard that Jesus is passing by."

- **ἔκραξαν λέγοντες**: The word ἔκραξαν is the aorist indicative active third person plural form of κράζω, meaning "they cried out." The word λέγοντες is a present participle of λέγω, meaning "saying." The phrase can be translated as "They cried out, saying."

- **Κύριε, ἐλέησον ἡμᾶς**: The word Κύριε is the vocative singular form of κύριος, meaning "Lord." The word ἐλέησον is the aorist imperative second person singular form of ἐλεέω, meaning "have mercy." The word ἡμᾶς is the accusative plural form of ἐγώ, meaning "us." The phrase can be translated as "Lord, have mercy on us."

- **υἱὸς Δαυίδ**: The word υἱὸς is the nominative singular form of υἱός, meaning "son." The word Δαυίδ is the genitive singular form of Δαυίδ, which is the name "David." The phrase can be translated as "Son of David."

The entire verse can be literally translated as "And behold, two blind men sitting beside the road, having heard that Jesus is passing by, they cried out, saying, 'Lord, have mercy on us, Son of David.'"