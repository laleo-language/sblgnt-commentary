---
version: 1
---
- **ἵνα πληρωθῇ**: The word ἵνα is a subordinating conjunction that introduces a purpose clause. It is often translated as "so that" or "in order that." The verb πληρωθῇ is the third person singular aorist passive subjunctive of πληρόω, meaning "to fulfill" or "to complete." The phrase ἵνα πληρωθῇ means "so that it might be fulfilled."
- **τὸ ῥηθὲν**: The article τὸ is the neuter singular nominative of ὁ, meaning "the." The word ῥηθὲν is the neuter singular aorist participle of λέγω, meaning "spoken" or "said." Together, the phrase τὸ ῥηθὲν means "the thing that was spoken."
- **διὰ Ἠσαΐου τοῦ προφήτου λέγοντος**: The preposition διὰ means "through" or "by means of." The genitive Ἠσαΐου is the name "Isaiah." The genitive τοῦ προφήτου means "of the prophet." The participle λέγοντος is the genitive singular masculine present active participle of λέγω, meaning "saying" or "who says." Together, the phrase διὰ Ἠσαΐου τοῦ προφήτου λέγοντος means "through Isaiah the prophet who says."
  
The literal translation of the entire verse is: "So that the thing that was spoken through Isaiah the prophet might be fulfilled."