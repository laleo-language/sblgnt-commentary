---
version: 1
---
- **καὶ ἔδωκαν αὐτὰ**: The conjunction καὶ means "and." The verb ἔδωκαν is the third person plural aorist indicative of δίδωμι, meaning "they gave." The pronoun αὐτὰ is the accusative plural of αὐτός, which means "they" or "them." The phrase thus means "and they gave them."

- **εἰς τὸν ἀγρὸν τοῦ κεραμέως**: The preposition εἰς means "into" or "to." The article τὸν is the accusative singular masculine of ὁ, which means "the." The noun ἀγρός is the accusative singular of ἀγρός, which means "field." The genitive τοῦ is the genitive singular masculine of ὁ, which means "of." The noun κεραμεύς is the genitive singular masculine of κεραμεύς, which means "potter." The phrase thus means "into the field of the potter."

- **καθὰ συνέταξέν μοι κύριος**: The conjunction καθὰ means "just as" or "according to." The verb συνέταξέν is the third person singular aorist indicative of συντάσσω, meaning "he arranged" or "he appointed." The pronoun μοι is the dative singular of ἐγώ, which means "to me." The noun κύριος is the nominative singular masculine of κύριος, which means "Lord." The phrase thus means "just as the Lord appointed to me."

The literal translation of Matthew 27:10 is: "And they gave them into the field of the potter, just as the Lord appointed to me."