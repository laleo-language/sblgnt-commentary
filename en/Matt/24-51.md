---
version: 1
---
- **καὶ διχοτομήσει αὐτὸν**: The word καὶ is a conjunction meaning "and." The verb διχοτομήσει is the third person singular future indicative of διχοτομέω, meaning "he will cut in two." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." So this phrase means "and he will cut him in two."

- **καὶ τὸ μέρος αὐτοῦ**: The word καὶ is a conjunction meaning "and." The definite article τὸ is the neuter singular nominative of ὁ, meaning "the." The noun μέρος is the neuter singular nominative of μέρος, meaning "part." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." So this phrase means "and his part."

- **μετὰ τῶν ὑποκριτῶν θήσει**: The preposition μετὰ means "with." The definite article τῶν is the masculine plural genitive of ὁ, meaning "the." The noun ὑποκριτῶν is the masculine plural genitive of ὑποκριτής, meaning "hypocrites." The verb θήσει is the third person singular future indicative of τίθημι, meaning "he will place." So this phrase means "with the hypocrites he will place."

- **ἐκεῖ ἔσται ὁ κλαυθμὸς καὶ ὁ βρυγμὸς τῶν ὀδόντων**: The adverb ἐκεῖ means "there." The verb ἔσται is the third person singular future indicative of εἰμί, meaning "he will be." The definite article ὁ is the masculine singular nominative of ὁ, meaning "the." The noun κλαυθμὸς is the nominative singular of κλαυθμός, meaning "weeping." The conjunction καὶ means "and." The definite article ὁ is the masculine singular nominative of ὁ, meaning "the." The noun βρυγμὸς is the nominative singular of βρυγμός, meaning "gnashing." The genitive plural noun τῶν is the genitive plural of ὁ, meaning "of the." The noun ὀδόντων is the genitive plural of ὀδούς, meaning "teeth." So this phrase means "there will be weeping and gnashing of teeth."

The literal translation of the verse is: "And he will cut him in two, and his part with the hypocrites he will place; there will be weeping and gnashing of teeth."