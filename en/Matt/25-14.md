---
version: 1
---
- **Ὥσπερ γὰρ ἄνθρωπος ἀποδημῶν**: The word Ὥσπερ is a conjunction meaning "just as" or "like." The noun ἄνθρωπος is the nominative singular of ἄνθρωπος, which means "man" or "person." The participle ἀποδημῶν is the present active nominative singular participle of ἀποδημέω, meaning "departing" or "traveling away." The phrase thus means "Just as a man was departing."

- **ἐκάλεσεν τοὺς ἰδίους δούλους**: The verb ἐκάλεσεν is the third person singular aorist indicative of καλέω, meaning "he called." The article τοὺς is the accusative plural of the definite article ὁ, meaning "the." The adjective ἰδίους is the accusative plural of ἴδιος, meaning "his own." The noun δούλους is the accusative plural of δοῦλος, which means "servants" or "slaves." The phrase thus means "he called his own servants."

- **καὶ παρέδωκεν αὐτοῖς τὰ ὑπάρχοντα αὐτοῦ**: The conjunction καὶ means "and." The verb παρέδωκεν is the third person singular aorist indicative of παραδίδωμι, meaning "he handed over" or "he entrusted." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The article τὰ is the accusative plural of the definite article ὁ, meaning "the." The noun ὑπάρχοντα is the accusative plural of ὑπάρχων, which means "possessions" or "property." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "and he handed over to them his possessions."

The literal translation of the verse is: "Just as a man was departing, he called his own servants and he handed over to them his possessions."