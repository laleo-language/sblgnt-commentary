---
version: 1
---
- **Ἀζὼρ δὲ ἐγέννησεν τὸν Σαδώκ**: The name Ἀζώρ is in the nominative singular. It is the father of Σαδώκ, which is in the accusative singular. The verb ἐγέννησεν is the third person singular aorist indicative of the verb γεννάω, meaning "he begat" or "he fathered." The phrase can be translated as "Azor begat Sadoc."

- **Σαδὼκ δὲ ἐγέννησεν τὸν Ἀχίμ**: The name Σαδώκ is in the nominative singular, and it is the father of Ἀχίμ, which is in the accusative singular. The verb ἐγέννησεν is the third person singular aorist indicative of γεννάω. The phrase can be translated as "Sadoc begat Achim."

- **Ἀχὶμ δὲ ἐγέννησεν τὸν Ἐλιούδ**: The name Ἀχίμ is in the nominative singular, and it is the father of Ἐλιούδ, which is in the accusative singular. The verb ἐγέννησεν is the third person singular aorist indicative of γεννάω. The phrase can be translated as "Achim begat Eliud."

The entire verse can be translated as "Azor begat Sadoc, Sadoc begat Achim, Achim begat Eliud."