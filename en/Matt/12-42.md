---
version: 1
---
- **βασίλισσα νότου ἐγερθήσεται**: The word βασίλισσα is the nominative singular of βασίλισσα, which means "queen." The word νότου is the genitive singular of νότος, which means "south." The verb ἐγερθήσεται is the third person singular future passive indicative of ἐγείρω, meaning "she will rise." The phrase thus means "the queen of the south will rise."

- **ἐν τῇ κρίσει**: The preposition ἐν means "in" and the noun κρίσει is the dative singular of κρίσις, which means "judgment." The phrase can be translated as "in the judgment."

- **μετὰ τῆς γενεᾶς ταύτης**: The preposition μετὰ means "with" and the noun γενεᾶς is the genitive singular of γενεά, which means "generation." The demonstrative pronoun ταύτης means "this." The phrase can be translated as "with this generation."

- **καὶ κατακρινεῖ αὐτήν**: The conjunction καὶ means "and" and the verb κατακρινεῖ is the third person singular future active indicative of κατακρίνω, meaning "he will condemn." The pronoun αὐτήν is the accusative singular of αὐτός, which means "her." The phrase can be translated as "and he will condemn her."

- **ὅτι ἦλθεν ἐκ τῶν περάτων τῆς γῆς**: The conjunction ὅτι means "because" or "for." The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "he came." The preposition ἐκ means "from" and the noun περάτων is the genitive plural of πέρας, which means "end" or "boundary." The article τῶν is the genitive plural form of the definite article, meaning "the." The noun γῆς is the genitive singular of γῆ, which means "earth" or "land." The phrase can be translated as "because he came from the ends of the earth."

- **ἀκοῦσαι τὴν σοφίαν Σολομῶνος**: The verb ἀκοῦσαι is the aorist infinitive of ἀκούω, meaning "to hear." The article τὴν is the accusative singular form of the definite article, meaning "the." The noun σοφίαν is the accusative singular of σοφία, which means "wisdom." The genitive form Σολομῶνος means "of Solomon." The phrase can be translated as "to hear the wisdom of Solomon."

- **καὶ ἰδοὺ πλεῖον Σολομῶνος ὧδε**: The conjunction καὶ means "and." The interjection ἰδοὺ is used to draw attention to something and can be translated as "behold" or "look." The adjective πλεῖον is the comparative form of πολύς, meaning "greater" or "more." The genitive form Σολομῶνος means "of Solomon." The adverb ὧδε means "here." The phrase can be translated as "and behold, someone greater than Solomon is here."

The literal translation of the entire verse is: "The queen of the south will rise in the judgment with this generation, and he will condemn her because he came from the ends of the earth to hear the wisdom of Solomon, and behold, someone greater than Solomon is here."