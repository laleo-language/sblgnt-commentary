---
version: 1
---
- **ὕστερον δὲ**: The word ὕστερον is an adverb meaning "later" or "afterwards." The word δὲ is a conjunction that can be translated as "but" or "and." So this phrase means "but later" or "and afterwards."

- **ἀπέστειλεν πρὸς αὐτοὺς**: The verb ἀπέστειλεν is the third person singular aorist indicative of ἀποστέλλω, which means "he sent." The preposition πρὸς means "to" or "towards." The pronoun αὐτοὺς is the accusative plural of αὐτός, which means "them." So this phrase means "he sent to them."

- **τὸν υἱὸν αὐτοῦ λέγων**: The noun υἱὸν is the accusative singular of υἱός, which means "son." The pronoun αὐτοῦ is the genitive singular of αὐτός, which means "his." The participle λέγων is the present active nominative singular of λέγω, which means "saying." So this phrase means "his son, saying."

- **Ἐντραπήσονται τὸν υἱόν μου**: The verb Ἐντραπήσονται is the third person plural future middle indicative of ἐντρέπω, which means "they will respect" or "they will have reverence for." The article τὸν is the accusative singular of ὁ, which means "the." The noun υἱόν is the accusative singular of υἱός, which means "son." The pronoun μου is the genitive singular of ἐγώ, which means "my." So this phrase means "they will respect my son."

Therefore, the literal translation of the verse is: "But later he sent to them his son, saying: 'They will respect my son.'"