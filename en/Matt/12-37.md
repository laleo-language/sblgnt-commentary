---
version: 1
---
- **ἐκ γὰρ τῶν λόγων σου**: The preposition ἐκ means "out of" or "from." The article τῶν is the genitive plural of ὁ, meaning "the." The noun λόγων is the genitive plural of λόγος, which means "word" or "speech." The pronoun σου is the genitive singular of σύ, meaning "you." The phrase can be translated as "from your words."

- **δικαιωθήσῃ**: This is the third person singular future passive indicative of δικαιόω, which means "to justify." The verb form indicates that the subject will be justified.

- **καὶ ἐκ τῶν λόγων σου καταδικασθήσῃ**: The conjunction καὶ means "and." The preposition ἐκ means "out of" or "from." The article τῶν is the genitive plural of ὁ, meaning "the." The noun λόγων is the genitive plural of λόγος, which means "word" or "speech." The pronoun σου is the genitive singular of σύ, meaning "you." The verb καταδικασθήσῃ is the second person singular future passive indicative of καταδικάζω, which means "to condemn." The phrase can be translated as "and from your words you will be condemned."

The phrase ἐκ γὰρ τῶν λόγων σου δικαιωθήσῃ, καὶ ἐκ τῶν λόγων σου καταδικασθήσῃ can be translated as "for by your words you will be justified, and by your words you will be condemned."

In Matthew 12:37, Jesus is speaking to the Pharisees, warning them about the power and importance of their words. He tells them that they will be judged based on their words, and that their words have the power to either justify or condemn them. This verse emphasizes the responsibility that comes with speaking, and the impact our words can have on our own judgment.