---
version: 1
---
- **Τῇ δὲ πρώτῃ τῶν ἀζύμων**: The word Τῇ is the dative singular of the definite article ὁ, which means "the." The word δὲ is a conjunction that can be translated as "but" or "and." The word πρώτῃ is the dative singular of πρῶτος, which means "first." The phrase τῶν ἀζύμων is a genitive plural construction, where ἀζύμων is the genitive plural of ἄζυμος, which means "unleavened." The phrase thus means "On the first day of the unleavened bread."

- **προσῆλθον οἱ μαθηταὶ τῷ Ἰησοῦ**: The verb προσῆλθον is the third person plural aorist indicative of the verb προσέρχομαι, which means "they approached" or "they came to." The noun μαθηταὶ is the nominative plural of μαθητής, which means "disciples" or "followers." The noun τῷ Ἰησοῦ is the dative singular of Ἰησοῦς, which means "to Jesus." The phrase thus means "The disciples came to Jesus."

- **λέγοντες**: The verb λέγοντες is the present active participle of the verb λέγω, which means "saying." The participle is in the nominative plural form, agreeing with the noun μαθηταὶ. The phrase can be translated as "saying."

- **Ποῦ θέλεις ἑτοιμάσωμέν σοι φαγεῖν τὸ πάσχα**: The word Ποῦ is an adverb meaning "where." The verb θέλεις is the second person singular present indicative of θέλω, which means "you want" or "you desire." The verb ἑτοιμάσωμέν is the first person plural aorist subjunctive of ἑτοιμάζω, which means "we may prepare." The pronoun σοι is the dative singular of σύ, which means "for you." The verb φαγεῖν is the aorist infinitive of ἐσθίω, which means "to eat." The noun τὸ πάσχα is the accusative singular of πάσχα, which means "Passover." The phrase can be translated as "Where do you want us to prepare for you to eat the Passover?"

- The literal translation of the entire verse is "On the first day of the unleavened bread, the disciples came to Jesus, saying, 'Where do you want us to prepare for you to eat the Passover?'"