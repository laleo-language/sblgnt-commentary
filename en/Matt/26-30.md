---
version: 1
---
- **καὶ ὑμνήσαντες**: The conjunction καὶ means "and" and the participle ὑμνήσαντες is the nominative plural masculine of the verb ὑμνέω, which means "to sing praises." The phrase means "and having sung praises."

- **ἐξῆλθον**: The verb ἐξέρχομαι is the third person plural aorist indicative of ἐξέρχομαι, meaning "they went out."

- **εἰς τὸ Ὄρος τῶν Ἐλαιῶν**: The preposition εἰς means "to" or "into." The article τὸ is the accusative singular neuter form of the definite article, meaning "the." The noun Ὄρος is the accusative singular neuter form of ὄρος, which means "mountain" or "hill." The prepositional phrase τῶν Ἐλαιῶν means "of the olive trees." The phrase as a whole means "to the Mountain of Olives."

The literal translation of the verse is: "And having sung praises, they went out to the Mountain of Olives."