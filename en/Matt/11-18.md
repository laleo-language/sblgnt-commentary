---
version: 1
---
- **ἦλθεν γὰρ Ἰωάννης**: The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "he came." The noun Ἰωάννης is the nominative singular of Ἰωάννης, which is the name "John." The phrase thus means "John came."

- **μήτε ἐσθίων μήτε πίνων**: The word μήτε is a conjunction meaning "neither" or "nor." The participle ἐσθίων is the present active nominative singular masculine participle of ἐσθίω, meaning "eating." The participle πίνων is the present active nominative singular masculine participle of πίνω, meaning "drinking." The phrase thus means "neither eating nor drinking."

- **καὶ λέγουσιν**: The conjunction καὶ means "and." The verb λέγουσιν is the third person plural present indicative of λέγω, meaning "they say." The phrase thus means "and they say."

- **Δαιμόνιον ἔχει**: The noun Δαιμόνιον is the accusative singular of Δαιμόνιον, which means "demon" or "evil spirit." The verb ἔχει is the third person singular present indicative of ἔχω, meaning "he has." The phrase thus means "he has a demon."

The literal translation of the entire verse is: "For John came, neither eating nor drinking, and they say, 'He has a demon.'"