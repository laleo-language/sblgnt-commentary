---
version: 1
---
- **Ὁ δὲ Ἰησοῦς**: The article ὁ is the nominative singular masculine form of the definite article and it means "the." The word δὲ is a conjunction that is often translated as "and" or "but." The noun Ἰησοῦς is the nominative singular masculine form of Ἰησοῦς, which means "Jesus." The phrase thus means "But Jesus."

- **ἐστάθη ἔμπροσθεν τοῦ ἡγεμόνος**: The verb ἐστάθη is the third person singular aorist indicative middle/passive form of ἵστημι, which means "he stood." The preposition ἔμπροσθεν means "in front of" and it takes the genitive case. The article τοῦ is the genitive singular masculine form of the definite article. The noun ἡγεμόνος is the genitive singular masculine form of ἡγεμών, which means "governor." The phrase thus means "he stood in front of the governor."

- **καὶ ἐπηρώτησεν αὐτὸν ὁ ἡγεμὼν λέγων**: The conjunction καὶ means "and." The verb ἐπηρώτησεν is the third person singular aorist indicative active form of ἐπερωτάω, which means "he asked." The pronoun αὐτὸν is the accusative singular masculine form of αὐτός, which means "him." The article ὁ is the nominative singular masculine form of the definite article. The noun ἡγεμὼν is the nominative singular masculine form of ἡγεμών, which means "governor." The participle λέγων is the nominative singular masculine present active form of λέγω, which means "saying." The phrase thus means "And the governor asked him, saying."

- **Σὺ εἶ ὁ βασιλεὺς τῶν Ἰουδαίων**: The pronoun Σὺ is the second person singular pronoun, which means "you." The verb εἶ is the second person singular present indicative active form of εἰμί, which means "you are." The article ὁ is the nominative singular masculine form of the definite article. The noun βασιλεὺς is the nominative singular masculine form of βασιλεύς, which means "king." The preposition τῶν means "of" and it takes the genitive case. The noun Ἰουδαίων is the genitive plural masculine form of Ἰουδαῖος, which means "Jews." The phrase thus means "You are the king of the Jews."

- **ὁ δὲ Ἰησοῦς ἔφη**: The article ὁ is the nominative singular masculine form of the definite article. The noun Ἰησοῦς is the nominative singular masculine form of Ἰησοῦς, which means "Jesus." The verb ἔφη is the third person singular imperfect indicative active form of φημί, which means "he said." The phrase thus means "But Jesus said."

The entire verse can be literally translated as "But Jesus stood in front of the governor; and the governor asked him, saying, 'Are you the king of the Jews?' And Jesus said, 'You say so.'"