---
version: 1
---
- **δεσμεύουσιν**: This is the third person plural present indicative active of the verb δεσμεύω, meaning "they bind" or "they tie up."
- **δὲ**: This is a conjunction that can be translated as "but" or "and." In this context, it is best translated as "and."
- **φορτία**: This is the accusative plural of the noun φορτίον, which means "burdens" or "loads."
- **βαρέα**: This is the accusative plural neuter of the adjective βαρύς, meaning "heavy."
- **καὶ**: This is a conjunction meaning "and."
- **ἐπιτιθέασιν**: This is the third person plural present indicative active of the verb ἐπιτίθημι, meaning "they place" or "they put."
- **ἐπὶ**: This is a preposition meaning "on" or "upon."
- **τοὺς ὤμους**: This is the accusative plural of the noun ὦμος, which means "shoulders."
- **τῶν ἀνθρώπων**: This is the genitive plural of the noun ἄνθρωπος, meaning "of men" or "of people."
- **αὐτοὶ**: This is a pronoun meaning "they themselves."
- **δὲ**: This is a conjunction that can be translated as "but" or "and." In this context, it is best translated as "but."
- **τῷ δακτύλῳ**: This is the dative singular of the noun δάκτυλος, which means "finger."
- **αὐτῶν**: This is a pronoun meaning "their."
- **οὐ θέλουσιν**: This is the third person plural present indicative active of the verb θέλω, meaning "they do not want" or "they do not wish."
- **κινῆσαι**: This is the aorist infinitive of the verb κινέω, meaning "to move" or "to stir."
- **αὐτά**: This is a pronoun meaning "them."

Literal translation: "But they tie up heavy burdens and place them on the shoulders of men, but they themselves do not want to move them with their finger."

The verse is not syntactically ambiguous and the meaning is relatively straightforward.