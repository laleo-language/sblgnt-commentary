---
version: 1
---
- **Ὀψὲ δὲ σαββάτων**: The word Ὀψὲ is an adverb meaning "late" or "afterwards." The word δὲ is a conjunction meaning "but" or "and." The noun σαββάτων is the genitive plural of σάββατον, which means "Sabbath" or "week." The phrase means "late on the Sabbath" or "after the Sabbath."

- **τῇ ἐπιφωσκούσῃ εἰς μίαν σαββάτων**: The definite article τῇ is the dative singular feminine form of the article ὁ, meaning "the." The verb ἐπιφωσκούσῃ is the present participle in the dative singular feminine form of ἐπιφωσκέω, meaning "to dawn" or "to begin to dawn." The preposition εἰς means "into" or "towards." The adjective μίαν is the accusative singular feminine form of εἷς, meaning "one." The noun σαββάτων is the genitive plural of σάββατον, which means "Sabbath" or "week." The phrase means "at the dawning into the first day of the week."

- **ἦλθεν Μαριὰμ ἡ Μαγδαληνὴ καὶ ἡ ἄλλη Μαρία**: The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "she came" or "she went." The proper noun Μαριὰμ is the accusative singular form of Μαρία, which means "Mary." The proper noun Μαγδαληνὴ is the nominative singular form of Μαγδαληνή, which means "Magdalene" (referring to Mary Magdalene). The conjunction καὶ means "and." The definite article ἡ is the nominative singular feminine form of the article ὁ, meaning "the." The adjective ἄλλη is the nominative singular feminine form of ἄλλος, meaning "other" or "another." The noun Μαρία is the nominative singular form of Μαρία, which means "Mary." The phrase means "Mary Magdalene and the other Mary went."

- **θεωρῆσαι τὸν τάφον**: The infinitive θεωρῆσαι is the aorist infinitive of θεωρέω, meaning "to look at" or "to behold." The definite article τὸν is the accusative singular masculine form of the article ὁ, meaning "the." The noun τάφον is the accusative singular form of τάφος, which means "tomb" or "grave." The phrase means "to look at the tomb."

The literal translation of the verse is: "Late on the Sabbath, as it began to dawn into the first day of the week, Mary Magdalene and the other Mary went to look at the tomb."