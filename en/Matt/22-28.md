---
version: 1
---
- **ἐν τῇ ἀναστάσει οὖν**: The preposition ἐν means "in" and the noun τῇ ἀναστάσει is the dative singular of ἀνάστασις, which means "resurrection." The word οὖν means "therefore" or "so." The phrase thus means "in the resurrection, therefore."

- **τίνος τῶν ἑπτὰ ἔσται γυνή**: The pronoun τίνος is the genitive singular of τίς, which means "who" or "which." The genitive case here indicates possession, so it can be translated as "whose." The article τῶν is the genitive plural of ὁ, which means "the." The noun ἑπτὰ is the nominative plural of ἕπτα, which means "seven." The verb ἔσται is the third person singular future indicative of εἰμί, meaning "will be." The noun γυνή is the nominative singular of γυνή, which means "woman." The phrase can be translated as "whose wife will she be among the seven women?"

- **πάντες γὰρ ἔσχον αὐτήν**: The adjective πάντες is the nominative plural of πᾶς, which means "all" or "every." The conjunction γὰρ means "for" or "because." The verb ἔσχον is the third person plural aorist indicative of ἔχω, meaning "they had" or "they obtained." The pronoun αὐτήν is the accusative singular of αὐτός, which means "her" or "it." The phrase can be translated as "for they all had her."

The entire verse can be translated as: "In the resurrection, therefore, whose wife will she be among the seven women? For they all had her."