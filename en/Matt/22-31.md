---
version: 1
---
- **περὶ δὲ τῆς ἀναστάσεως τῶν νεκρῶν**: The word περὶ is a preposition meaning "concerning" or "about." The genitive article τῆς indicates that the following noun is in the genitive case, and the noun ἀναστάσεως is the genitive singular of ἀνάστασις, which means "resurrection." The genitive plural noun τῶν νεκρῶν means "of the dead" or "of those who have died." So this phrase can be translated as "concerning the resurrection of the dead."

- **οὐκ ἀνέγνωτε τὸ ῥηθὲν ὑμῖν**: The word οὐκ is the negative particle meaning "not." The verb ἀνέγνωτε is the second person plural aorist indicative of ἀναγινώσκω, which means "you have not read." The neuter article τὸ indicates that the following noun is a neuter singular, and the noun ῥηθὲν is the aorist passive participle of λέγω, which means "spoken" or "said." The pronoun ὑμῖν means "to you" or "for you." So this phrase can be translated as "you have not read what was spoken to you."

- **ὑπὸ τοῦ θεοῦ λέγοντος**: The preposition ὑπὸ means "by" or "under." The genitive article τοῦ indicates that the following noun is in the genitive case, and the noun θεοῦ is the genitive singular of θεός, which means "God." The present participle λέγοντος is the genitive singular masculine form of λέγω, which means "saying" or "speaking." So this phrase can be translated as "by God saying."

The entire verse can be translated as: "But concerning the resurrection of the dead, have you not read what was spoken to you by God saying?"