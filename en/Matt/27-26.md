---
version: 1
---
- **τότε ἀπέλυσεν αὐτοῖς τὸν Βαραββᾶν**: The word τότε means "then" or "at that time." The verb ἀπέλυσεν is the third person singular aorist indicative active of ἀπολύω, which means "he released" or "he let go." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The article τὸν is the accusative singular of ὁ, which means "the." The name Βαραββᾶν refers to Barabbas, a criminal who was released instead of Jesus.

- **τὸν δὲ Ἰησοῦν φραγελλώσας παρέδωκεν**: The article τὸν is again the accusative singular of ὁ, meaning "the." The name Ἰησοῦν refers to Jesus. The participle φραγελλώσας is the nominative masculine singular aorist active participle of φραγελλόω, which means "having flogged" or "having scourged." The verb παρέδωκεν is the third person singular aorist indicative active of παραδίδωμι, meaning "he handed over" or "he delivered."

- **ἵνα σταυρωθῇ**: The conjunction ἵνα introduces a purpose clause and can be translated as "so that" or "in order that." The verb σταυρωθῇ is the third person singular aorist passive subjunctive of σταυρόω, meaning "he may be crucified."

The literal translation of the sentence is: "Then he released Barabbas to them, but having flogged Jesus, he handed him over so that he may be crucified."