---
version: 1
---
- **καὶ λέγετε**: The verb λέγετε is the second person plural present indicative of λέγω, meaning "you say" or "you are saying."
- **Εἰ ἤμεθα ἐν ταῖς ἡμέραις τῶν πατέρων ἡμῶν**: The word Εἰ is a conjunction meaning "if." The verb ἤμεθα is the first person plural imperfect indicative of εἰμί, meaning "we were" or "we existed." The preposition ἐν means "in." The noun ταῖς ἡμέραις is the dative plural of ἡμέρα, meaning "days." The genitive plural τῶν πατέρων is from the noun πατήρ, meaning "fathers." The phrase can be translated as "If we were in the days of our fathers."
- **οὐκ ἂν ἤμεθα αὐτῶν κοινωνοὶ**: The negative particle οὐκ negates the following verb. The word ἂν is a particle used to express a hypothetical or potential action. The verb ἤμεθα is the first person plural imperfect indicative of εἰμί, meaning "we were" or "we existed." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The noun κοινωνοὶ is the nominative plural of κοινωνός, meaning "partakers" or "sharers." The phrase can be translated as "We would not have been their partakers."
- **ἐν τῷ αἵματι τῶν προφητῶν**: The preposition ἐν means "in." The noun αἵματι is the dative singular of αἷμα, meaning "blood." The genitive plural τῶν προφητῶν is from the noun προφήτης, meaning "prophets." The phrase can be translated as "in the blood of the prophets."

The entire verse can be translated as "And you say, 'If we had lived in the days of our fathers, we would not have been their partakers in the blood of the prophets.'"