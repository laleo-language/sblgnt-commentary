---
version: 1
---
- **ἐὰν δὲ εἴπωμεν**: The word ἐὰν is a conjunction meaning "if" or "whenever." The verb form εἴπωμεν is the first person plural aorist subjunctive of λέγω, meaning "we say" or "we speak." The phrase thus means "if we say."

- **Ἐξ ἀνθρώπων**: The word Ἐξ is a preposition meaning "from" or "out of." The noun ἀνθρώπων is the genitive plural of ἄνθρωπος, which means "man" or "person." The phrase means "from men" or "by human authority."

- **φοβούμεθα τὸν ὄχλον**: The verb φοβούμεθα is the first person plural present indicative middle/passive of φοβέω, meaning "we fear" or "we are afraid." The noun phrase τὸν ὄχλον is the accusative singular of ὄχλος, which means "crowd" or "multitude." The phrase means "we fear the crowd."

- **πάντες γὰρ ὡς προφήτην ἔχουσιν τὸν Ἰωάννην**: The word πάντες is an adjective meaning "all" or "everyone." The conjunction γὰρ is a particle indicating cause or explanation, often translated as "for." The adverb ὡς can mean "as" or "like." The verb ἔχουσιν is the third person plural present indicative active of ἔχω, meaning "they have" or "they possess." The noun phrase τὸν Ἰωάννην is the accusative singular of Ἰωάννης, which is a proper noun referring to John. The phrase means "for everyone considers John as a prophet."

The literal translation of the verse is: "But if we say, 'From men,' we fear the crowd, for everyone considers John as a prophet."