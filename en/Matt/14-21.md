---
version: 1
---
- **οἱ δὲ ἐσθίοντες**: The article οἱ is the nominative plural masculine form of ὁ, meaning "the." The verb ἐσθίοντες is the present participle of ἐσθίω, meaning "they were eating." The phrase means "the ones who were eating."

- **ἦσαν ἄνδρες**: The verb ἦσαν is the third person plural imperfect indicative of εἰμί, meaning "they were." The noun ἄνδρες is the nominative plural masculine form of ἀνήρ, meaning "men." The phrase means "there were men."

- **ὡσεὶ πεντακισχίλιοι**: The adverb ὡσεὶ means "about" or "approximately." The adjective πεντακισχίλιοι is the nominative plural masculine form of πεντακισχίλιοι, meaning "five thousand." The phrase means "about five thousand."

- **χωρὶς γυναικῶν καὶ παιδίων**: The preposition χωρὶς means "apart from" or "without." The genitive plural form γυναικῶν is the genitive plural feminine form of γυνή, meaning "women." The genitive plural form παιδίων is the genitive plural neuter form of παιδίον, meaning "children." The phrase means "apart from women and children."

The literal translation of the entire verse is: "The ones who were eating were men, about five thousand, apart from women and children."