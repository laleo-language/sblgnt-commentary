---
version: 1
---
- **καὶ ἐξελθόντες οἱ δοῦλοι ἐκεῖνοι**: The conjunction καὶ means "and." The participle ἐξελθόντες is the nominative plural masculine aorist participle of ἐξέρχομαι, which means "going out" or "having gone out." The article οἱ is the nominative plural masculine definite article, and the noun δοῦλοι is the nominative plural masculine form of δοῦλος, meaning "servants." The pronoun ἐκεῖνοι is the nominative plural masculine form of ἐκεῖνος, which means "those." The phrase thus means "and those servants going out."

- **εἰς τὰς ὁδοὺς**: The preposition εἰς means "into" or "to." The article τὰς is the accusative plural feminine definite article, and the noun ὁδοὺς is the accusative plural feminine form of ὁδός, meaning "roads" or "ways." The phrase means "into the roads" or "to the ways."

- **συνήγαγον πάντας ὧς εὗρον**: The verb συνήγαγον is the third person plural aorist indicative active form of συνάγω, meaning "they gathered" or "they collected." The pronoun πάντας is the accusative plural masculine form of πᾶς, meaning "all" or "everyone." The relative pronoun ὧς is the accusative plural masculine form of ὅς, which means "whom" or "who." The verb εὗρον is the third person plural aorist indicative active form of εὑρίσκω, meaning "they found." The phrase means "they gathered everyone whom they found."

- **πονηρούς τε καὶ ἀγαθούς**: The adjective πονηρούς is the accusative plural masculine form of πονηρός, meaning "wicked" or "evil." The conjunction τε means "and." The adjective ἀγαθούς is the accusative plural masculine form of ἀγαθός, meaning "good." The phrase means "the wicked and the good."

- **καὶ ἐπλήσθη ὁ γάμος ἀνακειμένων**: The conjunction καὶ means "and." The verb ἐπλήσθη is the third person singular aorist passive indicative form of πίμπλημι, meaning "it was filled." The article ὁ is the nominative singular masculine definite article, and the noun γάμος is the nominative singular masculine form of γάμος, meaning "wedding." The participle ἀνακειμένων is the genitive plural masculine present participle form of ἀνάκειμαι, meaning "those reclining" or "those lying down." The phrase means "and the wedding was filled with those reclining."

The sentence can be literally translated as: "And those servants going out into the roads gathered everyone whom they found, the wicked and the good; and the wedding was filled with those reclining." 

In this verse, the servants are gathering people for a wedding feast, and they gather both the wicked and the good. The wedding feast becomes full with those who are reclining to eat.