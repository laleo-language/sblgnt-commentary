---
version: 1
---
- **καὶ λέγει αὐτοῖς**: The word καὶ is a conjunction meaning "and." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says" or "he is saying." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "And he says to them."

- **Τίνος ἡ εἰκὼν αὕτη**: The word Τίνος is an interrogative pronoun meaning "whose." The article ἡ is the nominative feminine singular form of ὁ, meaning "the." The noun εἰκὼν is the nominative feminine singular of εἰκών, meaning "image" or "likeness." The pronoun αὕτη is the nominative feminine singular form of αὐτός, meaning "this." The phrase thus means "Whose is this image?"

- **καὶ ἡ ἐπιγραφή**: The word καὶ is a conjunction meaning "and." The article ἡ is the nominative feminine singular form of ὁ, meaning "the." The noun ἐπιγραφή is the nominative feminine singular of ἐπιγραφή, meaning "inscription" or "label." The phrase thus means "And the inscription."

The entire verse, translated literally, is: "And he says to them, 'Whose is this image and the inscription?'"