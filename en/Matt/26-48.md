---
version: 1
---
- **ὁ δὲ παραδιδοὺς αὐτὸν**: The article ὁ is the nominative singular masculine, meaning "the." The verb παραδιδοὺς is the present participle of παραδίδωμι, meaning "delivering." The pronoun αὐτὸν is the accusative singular masculine, meaning "him." The phrase thus means "the one delivering him."

- **ἔδωκεν αὐτοῖς σημεῖον**: The verb ἔδωκεν is the third person singular aorist indicative of δίδωμι, meaning "he gave." The pronoun αὐτοῖς is the dative plural, meaning "to them." The noun σημεῖον is the accusative singular, meaning "sign" or "miracle." The phrase thus means "he gave them a sign."

- **λέγων**: The present participle of λέγω, meaning "saying."

- **Ὃν ἂν φιλήσω αὐτός ἐστιν**: The relative pronoun Ὃν is the accusative singular masculine, meaning "whom." The verb φιλήσω is the first person singular future indicative of φιλέω, meaning "I will kiss." The pronoun αὐτός is the third person singular masculine, meaning "he." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The phrase thus means "whom I will kiss, he is."

- **κρατήσατε αὐτόν**: The verb κρατήσατε is the second person plural aorist imperative of κρατέω, meaning "seize" or "arrest." The pronoun αὐτόν is the accusative singular masculine, meaning "him." The phrase thus means "seize him."

The literal translation of the entire verse is: "The one delivering him gave them a sign, saying, 'Whom I will kiss, he is. Seize him.'"