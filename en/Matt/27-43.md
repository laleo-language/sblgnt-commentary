---
version: 1
---
- **πέποιθεν ἐπὶ τὸν θεόν**: The verb πέποιθεν is the third person singular perfect indicative of πείθω, which means "to trust" or "to have confidence." The preposition ἐπὶ means "on" or "upon," and the article τὸν is the accusative singular masculine definite article. The noun θεόν is the accusative singular of θεός, meaning "God." The phrase can be translated as "he has trusted in God."

- **ῥυσάσθω ⸀νῦν εἰ θέλει αὐτόν**: The verb ῥυσάσθω is the third person singular aorist imperative middle/passive of ῥύομαι, meaning "to deliver" or "to rescue." The adverb νῦν means "now." The conjunction εἰ means "if." The verb θέλει is the third person singular present indicative of θέλω, meaning "he wants." The pronoun αὐτόν is the accusative singular masculine pronoun, meaning "him." The phrase can be translated as "let him now be rescued if he wants him."

- **εἶπεν γὰρ ὅτι Θεοῦ εἰμι υἱός**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The conjunction γὰρ means "for." The pronoun ὅτι introduces a subordinate clause and means "that." The noun Θεοῦ is the genitive singular of θεός, meaning "God." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "I am." The noun υἱός is the nominative singular masculine of υἱός, meaning "son." The phrase can be translated as "for he said, 'I am the Son of God.'"

The literal translation of the entire verse is: "He has trusted in God, let him now be rescued if he wants him; for he said, 'I am the Son of God.'"