---
version: 1
---
- **ὁ μὴ ὢν μετ' ἐμοῦ**: The article ὁ is the nominative singular masculine form, meaning "the." The word μὴ is a negative particle, meaning "not." The verb ὢν is the present participle of εἰμί, "to be." The preposition μετ' means "with." And the pronoun ἐμοῦ is the genitive singular form of ἐγώ, meaning "me." The phrase can be translated as "the one who is not with me."

- **κατ' ἐμοῦ**: The preposition κατ' means "according to" or "against." And the pronoun ἐμοῦ is the genitive singular form of ἐγώ, meaning "me." The phrase can be translated as "against me" or "in opposition to me."

- **ἐστιν**: The verb ἐστιν is the third person singular present indicative of εἰμί, "to be." It means "he/she/it is."

- **καὶ ὁ μὴ συνάγων μετ' ἐμοῦ**: The conjunction καὶ means "and." The article ὁ is the nominative singular masculine form, meaning "the." The word μὴ is a negative particle, meaning "not." The verb συνάγων is the present participle of συνάγω, "to gather." The preposition μετ' means "with." And the pronoun ἐμοῦ is the genitive singular form of ἐγώ, meaning "me." The phrase can be translated as "and the one who does not gather with me."

- **σκορπίζει**: The verb σκορπίζει is the third person singular present indicative of σκορπίζω, "to scatter." It means "he/she/it scatters."

The literal translation of the verse is: "The one who is not with me is against me, and the one who does not gather with me scatters."