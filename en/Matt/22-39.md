---
version: 1
---
- **Δευτέρα δὲ ὁμοία αὐτῇ**: The word Δευτέρα is the feminine singular nominative form of δεύτερος, meaning "second." The word ὁμοία is the feminine singular nominative form of ὅμοιος, meaning "similar" or "like." The word αὐτῇ is the feminine singular dative form of αὐτός, meaning "to her" or "for her." The phrase thus means "similar to her" or "like her."

- **Ἀγαπήσεις τὸν πλησίον σου ὡς σεαυτόν**: The verb Ἀγαπήσεις is the second person singular future indicative of ἀγαπάω, meaning "you will love." The article τὸν is the accusative singular form of ὁ, meaning "the." The noun πλησίον is the accusative singular form of πλησίος, meaning "neighbor." The pronoun σου is the genitive singular form of σύ, meaning "your." The word ὡς is a conjunction meaning "as" or "like." The pronoun σεαυτόν is the accusative singular form of σεαυτός, meaning "yourself." The phrase thus means "You will love your neighbor as yourself."

The entire verse, Matthew 22:39, is translated as: "And a second is like it: 'You shall love your neighbor as yourself.'" This verse is part of Jesus' response to the question of what the greatest commandment is. He first mentions the commandment to love God with all your heart, soul, and mind, and then adds that the second greatest commandment is to love your neighbor as yourself.