---
version: 1
---
- **περὶ δὲ τὴν ἑνδεκάτην**: The preposition περὶ means "about" or "concerning." The article τὴν is the accusative singular feminine form of the definite article ὁ, meaning "the." The adjective ἑνδεκάτην is the accusative singular feminine form of ἑνδέκατος, meaning "eleventh." The phrase means "about the eleventh."

- **ἐξελθὼν εὗρεν ἄλλους ἑστῶτας**: The participle ἐξελθὼν is the nominative singular masculine form of the aorist participle ἐξέρχομαι, meaning "going out." The verb εὗρεν is the third person singular aorist indicative of εὑρίσκω, meaning "he found." The adjective ἄλλους is the accusative plural masculine form of ἄλλος, meaning "other." The verb ἑστῶτας is the accusative plural masculine form of the perfect participle ἵστημι, meaning "standing." The phrase means "going out, he found other [people] standing."

- **καὶ λέγει αὐτοῖς**: The conjunction καὶ means "and." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The pronoun αὐτοῖς is the dative plural form of αὐτός, meaning "to them." The phrase means "and he says to them."

- **Τί ὧδε ἑστήκατε ὅλην τὴν ἡμέραν ἀργοί;**: The pronoun Τί means "what." The adverb ὧδε means "here." The verb ἑστήκατε is the second person plural perfect indicative of ἵστημι, meaning "you have stood." The adjective ὅλην is the accusative singular feminine form of ὅλος, meaning "whole" or "entire." The article τὴν is the accusative singular feminine form of the definite article ὁ, meaning "the." The noun ἡμέραν is the accusative singular feminine form of ἡμέρα, meaning "day." The adjective ἀργοί is the nominative plural masculine form of ἀργός, meaning "idle" or "lazy." The phrase means "What are you standing here the whole day idle?"

The entire verse can be translated as: "And about the eleventh hour he went out and found others standing, and he said to them, 'Why are you standing here the whole day idle?'"