---
version: 1
---
- **καὶ ἐλθὼν**: The conjunction καὶ means "and" and the participle ἐλθὼν is the nominative singular masculine form of the aorist active participle of ἔρχομαι, meaning "having come." The phrase ἐλθὼν καὶ together mean "having come and."

- **πάλιν εὗρεν αὐτοὺς**: The adverb πάλιν means "again" and the verb εὗρεν is the third person singular aorist indicative active form of εὑρίσκω, meaning "he found." The pronoun αὐτοὺς is the accusative plural form of αὐτός, meaning "them." The phrase πάλιν εὗρεν αὐτοὺς means "he found them again."

- **καθεύδοντας**: The participle καθεύδοντας is the accusative plural masculine form of the present active participle of καθεύδω, meaning "sleeping." The phrase καθεύδοντας means "sleeping."

- **ἦσαν γὰρ αὐτῶν οἱ ὀφθαλμοὶ βεβαρημένοι**: The verb ἦσαν is the third person plural imperfect indicative active form of εἰμί, meaning "they were." The conjunction γὰρ means "for" and the pronoun αὐτῶν is the genitive plural form of αὐτός, meaning "their." The article οἱ is the nominative plural form of ὁ, meaning "the." The noun ὀφθαλμοὶ is the nominative plural form of ὀφθαλμός, meaning "eyes." The participle βεβαρημένοι is the nominative plural masculine form of the perfect passive participle of βαρέω, meaning "burdened" or "heavy." The phrase ἦσαν γὰρ αὐτῶν οἱ ὀφθαλμοὶ βεβαρημένοι means "for their eyes were heavy."

The literal translation of the verse is: "And having come again, he found them sleeping, for their eyes were heavy."