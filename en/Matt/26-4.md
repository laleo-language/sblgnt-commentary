---
version: 1
---
- **συνεβουλεύσαντο**: The verb συνεβουλεύσαντο is the third person plural aorist middle indicative of συμβουλεύω, which means "to counsel" or "to plot." The prefix συν- indicates that the action is done together, meaning "they counseled together" or "they plotted together."

- **ἵνα**: This is a subordinating conjunction that introduces a purpose clause. It is often translated as "so that" or "in order that." Here, it introduces the purpose of their counsel or plot.

- **τὸν Ἰησοῦν**: The article τὸν is the accusative singular masculine form of the definite article ὁ, which means "the." The noun Ἰησοῦν is the accusative singular of Ἰησοῦς, which is the name "Jesus." So, this phrase means "Jesus."

- **δόλῳ**: The noun δόλος is the instrumental dative singular form, meaning "by deceit" or "by trickery." It describes the means by which they planned to seize Jesus.

- **κρατήσωσιν**: The verb κρατήσωσιν is the third person plural aorist active subjunctive of κρατέω, which means "to seize" or "to arrest." The subjunctive mood indicates that the action is uncertain or potential.

- **καὶ ἀποκτείνωσιν**: The conjunction καὶ is translated as "and." The verb ἀποκτείνωσιν is the third person plural aorist active subjunctive of ἀποκτείνω, which means "to kill" or "to murder." Like the previous verb, it is also in the subjunctive mood, indicating uncertainty or potential.

The literal translation of this verse would be: "And they counseled together so that they might seize Jesus by deceit and kill him."

This verse recounts the plot of certain individuals to arrest Jesus secretly and put him to death. It highlights the malicious intent of these individuals.