---
version: 1
---
- **καὶ ἐποίησαν οἱ μαθηταὶ**: The conjunction καὶ means "and." The verb ἐποίησαν is the third person plural aorist indicative of ποιέω, meaning "they did" or "they made." The noun οἱ μαθηταὶ is the nominative plural of μαθητής, which means "disciples" or "students." The phrase thus means "and the disciples did."

- **ὡς συνέταξεν αὐτοῖς ὁ Ἰησοῦς**: The adverb ὡς means "as" or "according to." The verb συνέταξεν is the third person singular aorist indicative of συντάσσω, meaning "he arranged" or "he ordered." The pronoun αὐτοῖς is the dative plural of αὐτός, which means "to them." The definite article ὁ indicates that the noun Ἰησοῦς is in the nominative case. The phrase can be translated as "as Jesus arranged for them."

- **καὶ ἡτοίμασαν τὸ πάσχα**: The conjunction καὶ means "and." The verb ἡτοίμασαν is the third person plural aorist indicative of ἑτοιμάζω, meaning "they prepared" or "they made ready." The noun τὸ πάσχα is the accusative singular of πάσχα, which means "Passover." The phrase can be translated as "and they prepared the Passover."

The literal translation of the entire verse is: "And the disciples did as Jesus arranged for them, and they prepared the Passover."