---
version: 1
---
- **καὶ προσελθὼν**: The conjunction καὶ means "and," and the participle προσελθὼν is the nominative masculine singular form of the verb προσέρχομαι, meaning "approaching" or "coming to." The phrase means "and approaching."

- **ὁ τὰ πέντε τάλαντα λαβὼν**: The definite article ὁ is the nominative masculine singular form, indicating "the." The noun τάλαντα is the accusative plural form of τάλαντον, meaning "talent" (a unit of currency). The participle λαβὼν is the nominative masculine singular form of the verb λαμβάνω, meaning "taking" or "receiving." The phrase means "having taken the five talents."

- **προσήνεγκεν ἄλλα πέντε τάλαντα**: The verb προσήνεγκεν is the third person singular aorist indicative active form of the verb προσφέρω, meaning "he brought" or "he presented." The adjective ἄλλα is the accusative neuter plural form of ἄλλος, meaning "other" or "another." The noun πέντε is the accusative feminine singular form of πέντε, meaning "five." The noun τάλαντα is the accusative plural form of τάλαντον, meaning "talent." The phrase means "he brought another five talents."

- **λέγων**: The participle λέγων is the nominative masculine singular form of the verb λέγω, meaning "saying." The phrase means "saying."

- **Κύριε, πέντε τάλαντά μοι παρέδωκας**: The noun Κύριε is the vocative masculine singular form of Κύριος, meaning "Lord" or "Master." The noun πέντε is the accusative feminine singular form of πέντε, meaning "five." The noun τάλαντα is the accusative plural form of τάλαντον, meaning "talent." The pronoun μοι is the dative singular form of ἐγώ, meaning "to me." The verb παρέδωκας is the second person singular aorist indicative active form of the verb παραδίδωμι, meaning "you handed over" or "you delivered." The phrase means "Lord, you handed over to me five talents."

- **ἴδε ἄλλα πέντε τάλαντα**: The interjection ἴδε means "behold" or "look." The adjective ἄλλα is the accusative neuter plural form of ἄλλος, meaning "other" or "another." The noun πέντε is the accusative feminine singular form of πέντε, meaning "five." The noun τάλαντα is the accusative plural form of τάλαντον, meaning "talent." The phrase means "behold, another five talents."

- **ἐκέρδησα**: The verb ἐκέρδησα is the first person singular aorist indicative active form of the verb κερδαίνω, meaning "I gained" or "I earned." The phrase means "I gained."

The entire verse, translated literally, is: "And approaching, he having taken the five talents, he brought another five talents, saying, 'Lord, you handed over to me five talents; behold, another five talents I gained.'"