---
version: 1
---
- **οὕτως ἔσονται**: The word οὕτως means "in this way" or "thus." The verb ἔσονται is the third person plural future indicative of εἰμί, meaning "they will be." The phrase οὕτως ἔσονται means "thus they will be."

- **οἱ ἔσχατοι πρῶτοι**: The article οἱ is the nominative plural masculine form of ὁ, meaning "the." The adjective ἔσχατοι is the nominative plural masculine form of ἔσχατος, meaning "last" or "final." The adjective πρῶτοι is the nominative plural masculine form of πρῶτος, meaning "first." The phrase οἱ ἔσχατοι πρῶτοι means "the last will be first."

- **καὶ οἱ πρῶτοι ἔσχατοι**: The conjunction καὶ means "and." The article οἱ is the nominative plural masculine form of ὁ, meaning "the." The adjective πρῶτοι is the nominative plural masculine form of πρῶτος, meaning "first." The adjective ἔσχατοι is the nominative plural masculine form of ἔσχατος, meaning "last" or "final." The phrase καὶ οἱ πρῶτοι ἔσχατοι means "and the first will be last."

The verse as a whole is translated as: "In this way, the last will be first and the first will be last."