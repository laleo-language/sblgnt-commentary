---
version: 1
---
- **ἐν γὰρ τῇ ἀναστάσει**: The word ἐν is a preposition meaning "in." The article τῇ is the feminine dative singular form of the definite article ὁ, meaning "the." The noun ἀναστάσει is the dative singular form of ἀνάστασις, meaning "resurrection." The phrase thus means "in the resurrection."

- **οὔτε γαμοῦσιν οὔτε γαμίζονται**: The word οὔτε is a conjunction meaning "neither." The verb γαμοῦσιν is the third person plural present indicative active form of γαμέω, meaning "they marry." The verb γαμίζονται is the third person plural present indicative middle form of γαμίζω, meaning "they are given in marriage." The phrase thus means "neither do they marry nor are they given in marriage."

- **ἀλλʼ ὡς ἄγγελοι θεοῦ ἐν τῷ οὐρανῷ εἰσιν**: The word ἀλλʼ is a conjunction meaning "but" or "rather." The adverb ὡς means "like" or "as." The noun ἄγγελοι is the nominative plural form of ἄγγελος, meaning "angels." The genitive θεοῦ is the genitive singular form of θεός, meaning "God." The preposition ἐν is a preposition meaning "in." The article τῷ is the masculine dative singular form of the definite article ὁ, meaning "the." The noun οὐρανῷ is the dative singular form of οὐρανός, meaning "heaven." The verb εἰσιν is the third person plural present indicative active form of εἰμί, meaning "they are." The phrase thus means "but they are like angels of God in heaven."

The literal translation of Matthew 22:30 is: "For in the resurrection, neither do they marry nor are they given in marriage, but they are like angels of God in heaven."