---
version: 1
---
- **λέγει αὐτῷ ὁ Ἰησοῦς**: The verb λέγει is the third person singular present indicative of λέγω, which means "he says." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The name ὁ Ἰησοῦς is "Jesus." The phrase thus means "Jesus says to him."

- **Σὺ εἶπας**: The pronoun Σὺ is the second person singular pronoun, meaning "you." The verb εἶπας is the second person singular aorist indicative of λέγω, meaning "you said." The phrase thus means "you said."

- **πλὴν λέγω ὑμῖν**: The adverb πλὴν means "but" or "except." The verb λέγω is the first person singular present indicative of λέγω, meaning "I say." The pronoun ὑμῖν is the dative plural of ὑμεῖς, meaning "to you." The phrase thus means "but I say to you."

- **ἀπʼ ἄρτι ὄψεσθε**: The preposition ἀπʼ means "from" or "since." The adverb ἄρτι means "now" or "from now on." The verb ὄψεσθε is the second person plural future indicative of ὁράω, meaning "you will see." The phrase thus means "from now on, you will see."

- **τὸν υἱὸν τοῦ ἀνθρώπου καθήμενον ἐκ δεξιῶν τῆς δυνάμεως**: The article τὸν is the accusative singular of ὁ, meaning "the." The noun υἱὸν is the accusative singular of υἱός, meaning "son." The preposition καθήμενον is the present participle accusative singular of κάθημαι, meaning "sitting." The preposition ἐκ means "from." The genitive δεξιῶν is the genitive plural of δεξιός, meaning "right." The noun τῆς δυνάμεως is the genitive singular of δύναμις, meaning "power." The phrase thus means "the son of man sitting at the right hand of power."

- **καὶ ἐρχόμενον ἐπὶ τῶν νεφελῶν τοῦ οὐρανοῦ**: The conjunction καὶ means "and." The participle ἐρχόμενον is the present participle accusative singular of ἔρχομαι, meaning "coming." The preposition ἐπὶ means "on" or "upon." The article τῶν is the genitive plural of ὁ, meaning "the." The noun νεφελῶν is the genitive plural of νεφέλη, meaning "cloud." The genitive τοῦ οὐρανοῦ is the genitive singular of οὐρανός, meaning "heaven." The phrase thus means "and coming on the clouds of heaven."

- The literal translation of the verse is: "Jesus says to him, 'You said. But I say to you, from now on you will see the Son of Man sitting at the right hand of power and coming on the clouds of heaven.'"