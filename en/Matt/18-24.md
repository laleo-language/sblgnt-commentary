---
version: 1
---
- **ἀρξαμένου δὲ αὐτοῦ συναίρειν**: The word ἀρξαμένου is the genitive singular masculine of ἄρχω, which means "to begin." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, meaning "he" or "him." The verb συναίρειν is the present active participle of συναίρω, which means "to seize" or "to take hold of." The phrase thus means "when he began to seize."

- **προσηνέχθη αὐτῷ εἷς ὀφειλέτης μυρίων ταλάντων**: The verb προσηνέχθη is the third person singular aorist passive indicative of προσφέρω, which means "to bring" or "to present." The pronoun αὐτῷ is the dative singular masculine of αὐτός. The noun ὀφειλέτης is the nominative singular masculine of ὀφειλέτης, meaning "debtor." The adjective μυρίων is the genitive plural neuter of μύριος, which means "ten thousand." The noun ταλάντων is the genitive plural neuter of τάλαντον, which refers to a unit of weight or money. The phrase thus means "a debtor of ten thousand talents was brought to him."

- The entire verse, translated literally, is: "When he began to seize, a debtor of ten thousand talents was brought to him."