---
version: 1
---
- **οἳ οὐκ ἐξ αἱμάτων**: The relative pronoun οἳ is declined in the nominative plural, referring to a group of people. The negative particle οὐκ negates the following verb. The preposition ἐξ is followed by the genitive case αἱμάτων, which means "of blood." The phrase can be translated as "who were not born of blood."

- **οὐδὲ ἐκ θελήματος σαρκὸς**: The negative particle οὐδὲ negates the following prepositional phrase. The preposition ἐκ is followed by the genitive case θελήματος, which means "of the will." The noun σαρκὸς is declined in the genitive case, meaning "of flesh." The phrase can be translated as "nor of the will of the flesh."

- **οὐδὲ ἐκ θελήματος ἀνδρὸς**: The negative particle οὐδὲ negates the following prepositional phrase. The preposition ἐκ is followed by the genitive case θελήματος, which means "of the will." The noun ἀνδρὸς is declined in the genitive case, meaning "of man." The phrase can be translated as "nor of the will of man."

- **ἀλλʼ ἐκ θεοῦ ἐγεννήθησαν**: The conjunction ἀλλʼ means "but" and introduces a contrast. The preposition ἐκ is followed by the genitive case θεοῦ, which means "of God." The verb ἐγεννήθησαν is the third person plural aorist passive indicative of γεννάω, meaning "they were born." The phrase can be translated as "but they were born of God."

The literal translation of the verse is: "Who were not born of blood, nor of the will of the flesh, nor of the will of man, but they were born of God."

In this verse, John is describing the birth of those who become children of God. He emphasizes that their birth is not a result of physical descent (blood), human desire (will of the flesh), or human decision (will of man), but rather it is a divine birth, being born of God.