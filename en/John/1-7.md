---
version: 1
---
- **οὗτος ἦλθεν εἰς μαρτυρίαν**: The word οὗτος is a demonstrative pronoun meaning "this." The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "he came." The preposition εἰς means "into" or "to," and the noun μαρτυρίαν means "testimony" or "witness." The phrase can be translated as "This one came into testimony" or "He came to bear witness."

- **ἵνα μαρτυρήσῃ περὶ τοῦ φωτός**: The conjunction ἵνα introduces a purpose clause, indicating the reason for the action in the main clause. The verb μαρτυρήσῃ is the third person singular aorist subjunctive of μαρτυρέω, meaning "he might testify" or "he may bear witness." The preposition περὶ means "about" or "concerning," and the article τοῦ indicates the genitive case of the noun φως, meaning "light." The phrase can be translated as "so that he might testify about the light" or "in order to bear witness concerning the light."

- **ἵνα πάντες πιστεύσωσιν διʼ αὐτοῦ**: Again, the conjunction ἵνα introduces a purpose clause. The verb πιστεύσωσιν is the third person plural aorist subjunctive of πιστεύω, meaning "they might believe" or "they may have faith." The preposition διʼ means "through" or "by means of," and the pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "him" or "it." The phrase can be translated as "so that all might believe through him" or "in order that everyone may have faith by means of him."

The entire verse can be translated as: "This one came into testimony, so that he might testify about the light, in order that all might believe through him."