---
version: 1
---
- **ἐποίησαν οὖν αὐτῷ δεῖπνον ἐκεῖ**: The verb ἐποίησαν is the third person plural aorist indicative of ποιέω, meaning "they made" or "they prepared." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The noun δεῖπνον is the accusative singular of δεῖπνον, which means "dinner" or "meal." The phrase thus means "so they made a dinner for him."

- **καὶ ἡ Μάρθα διηκόνει**: The conjunction καὶ means "and." The article ἡ is the nominative singular feminine definite article, meaning "the." The name Μάρθα is the nominative singular of Μάρθα, which is the name "Martha." The verb διηκόνει is the third person singular imperfect indicative of διακονέω, meaning "she was serving" or "she was ministering." The phrase thus means "and Martha was serving."

- **ὁ δὲ Λάζαρος εἷς ἦν ἐκ τῶν ἀνακειμένων σὺν αὐτῷ**: The article ὁ is the nominative singular masculine definite article, meaning "the." The name Λάζαρος is the nominative singular of Λάζαρος, which is the name "Lazarus." The adjective εἷς is the nominative singular masculine form of εἷς, meaning "one." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was." The preposition ἐκ is followed by the genitive case, so τῶν is the genitive plural of ὁ, meaning "of the." The verb ἀνακειμένων is the present participle genitive plural masculine of ἀνάκειμαι, meaning "reclining." The preposition σὺν is followed by the dative case, so αὐτῷ is the dative singular of αὐτός, meaning "with him." The phrase thus means "and Lazarus was one of those reclining with him."

The literal translation of John 12:2 is: "So they made a dinner for him, and Martha was serving, and Lazarus was one of those reclining with him."