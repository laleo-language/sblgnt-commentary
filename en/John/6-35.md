---
version: 1
---
- **Εἶπεν αὐτοῖς ὁ Ἰησοῦς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said" or "he spoke." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The noun ὁ Ἰησοῦς is the nominative singular of Ἰησοῦς, which is the name "Jesus." The phrase thus means "Jesus said to them."

- **Ἐγώ εἰμι ὁ ἄρτος τῆς ζωῆς**: The pronoun Ἐγώ is the first person singular pronoun meaning "I." The verb εἰμι is the first person singular present indicative of εἰμί, which means "I am." The article ὁ is the definite article meaning "the." The noun ἄρτος is the nominative singular of ἄρτος, which means "bread." The genitive noun τῆς ζωῆς is the genitive singular of ζωή, which means "of life." The phrase thus means "I am the bread of life."

- **ὁ ἐρχόμενος πρὸς ἐμὲ οὐ μὴ πεινάσῃ**: The article ὁ is the definite article meaning "the." The participle ἐρχόμενος is the present middle participle nominative singular of ἔρχομαι, which means "coming." The preposition πρὸς means "towards" or "to." The pronoun ἐμὲ is the accusative singular of ἐγώ, meaning "me." The negative particle οὐ means "not." The verb πεινάσῃ is the third person singular aorist subjunctive of πεινάω, which means "he may hunger." The phrase thus means "the one coming to me will not hunger."

- **καὶ ὁ πιστεύων εἰς ἐμὲ οὐ μὴ διψήσει πώποτε**: The conjunction καὶ means "and." The article ὁ is the definite article meaning "the." The participle πιστεύων is the present active participle nominative singular of πιστεύω, which means "believing" or "having faith." The preposition εἰς means "into" or "to." The pronoun ἐμὲ is the accusative singular of ἐγώ, meaning "me." The negative particle οὐ means "not." The verb διψήσει is the third person singular aorist subjunctive of διψάω, which means "he may thirst." The adverb πώποτε means "ever" or "at any time." The phrase thus means "and the one believing in me will never thirst."

The literal translation of John 6:35 is "Jesus said to them, 'I am the bread of life; the one coming to me will not hunger, and the one believing in me will never thirst.'"