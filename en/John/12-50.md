---
version: 1
---
- **καὶ οἶδα**: The conjunction καὶ means "and" and the verb οἶδα is the first person singular present indicative of οἶδα, meaning "I know." The phrase thus means "And I know."
- **ὅτι ἡ ἐντολὴ αὐτοῦ ζωὴ αἰώνιός ἐστιν**: The conjunction ὅτι introduces a clause and means "that." The noun ἡ ἐντολὴ is the nominative singular of ἐντολή, which means "commandment" or "instruction." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The noun ζωὴ is the nominative singular of ζωή, which means "life." The adjective αἰώνιός is the nominative singular of αἰώνιος, meaning "eternal." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The phrase thus means "that his commandment is eternal life."
- **ἃ οὖν ἐγὼ λαλῶ**: The relative pronoun ἃ is the accusative neuter plural of ὅς, meaning "which" or "what." The adverb οὖν means "therefore" or "so." The pronoun ἐγὼ is the first person singular pronoun, meaning "I." The verb λαλῶ is the first person singular present indicative of λαλέω, meaning "I speak." The phrase thus means "what I speak therefore."
- **καθὼς εἴρηκέν μοι ὁ πατήρ**: The adverb καθὼς means "just as" or "according to." The verb εἴρηκέν is the third person singular perfect indicative of λέγω, meaning "he has said." The pronoun μοι is the dative singular of ἐγώ, meaning "to me." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The noun πατήρ is the nominative singular masculine of πατήρ, meaning "father." The phrase thus means "just as the Father has said to me."
- **οὕτως λαλῶ**: The adverb οὕτως means "thus" or "in this way." The verb λαλῶ is the first person singular present indicative of λαλέω, meaning "I speak." The phrase thus means "I speak in this way."

Literal translation: "And I know that his commandment is eternal life. What I speak, therefore, just as the Father has said to me, I speak in this way."

In this verse, Jesus is expressing his knowledge and understanding that his commandment is eternal life. He speaks in accordance with what the Father has told him, conveying the importance and truth of his words.