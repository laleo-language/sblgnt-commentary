---
version: 1
---
- **ἐκ τοῦ ὄχλου δὲ πολλοὶ**: The preposition ἐκ means "out of" or "from." The article τοῦ is the genitive singular masculine of the definite article ὁ, meaning "the." The noun ὄχλου is the genitive singular of ὄχλος, which means "crowd" or "multitude." The adverb δὲ is a conjunction meaning "but" or "and." The adjective πολλοὶ is the nominative plural masculine of πολλός, meaning "many." The phrase thus means "but many from the crowd."

- **ἐπίστευσαν εἰς αὐτόν**: The verb ἐπίστευσαν is the third person plural aorist indicative active of πιστεύω, meaning "they believed." The preposition εἰς means "into" or "to." The pronoun αὐτόν is the accusative singular masculine of αὐτός, meaning "him." The phrase thus means "they believed in him."

- **καὶ ἔλεγον**: The conjunction καὶ means "and." The verb ἔλεγον is the third person plural imperfect indicative active of λέγω, meaning "they were saying." The phrase thus means "and they were saying."

- **Ὁ χριστὸς ὅταν ἔλθῃ**: The article Ὁ is the nominative singular masculine of ὁ, meaning "the." The noun χριστὸς is the nominative singular masculine of Χριστός, meaning "Christ." The conjunction ὅταν means "when." The verb ἔλθῃ is the third person singular aorist subjunctive active of ἔρχομαι, meaning "he comes." The phrase thus means "The Christ when he comes."

- **μὴ πλείονα σημεῖα ποιήσει**: The negation μὴ means "not." The adjective πλείονα is the accusative plural neuter of πολύς, meaning "more." The noun σημεῖα is the accusative plural neuter of σημεῖον, meaning "signs." The verb ποιήσει is the third person singular future indicative active of ποιέω, meaning "he will do." The phrase thus means "he will not do more signs."

- **ὧν οὗτος ἐποίησεν**: The pronoun ὧν is the genitive plural neuter of ὅς, meaning "which" or "that." The pronoun οὗτος is the nominative singular masculine of οὗτος, meaning "this." The verb ἐποίησεν is the third person singular aorist indicative active of ποιέω, meaning "he did." The phrase thus means "which this one did."

The literal translation of John 7:31 is: "But many from the crowd believed in him, and they were saying, 'When the Christ comes, will he not do more signs than this one did?'"