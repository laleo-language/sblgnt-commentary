---
version: 1
---
- **αὐτὸς**: This is a pronoun that means "he himself." It is in the nominative case, singular number, and masculine gender.
- **γὰρ**: This is a conjunction that means "for" or "because." It is used to provide a reason or explanation for what was previously stated.
- **Ἰησοῦς**: This is a proper noun that means "Jesus." It is in the nominative case, singular number, and masculine gender.
- **ἐμαρτύρησεν**: This is a verb that means "he testified" or "he bore witness." It is in the third person singular, aorist tense, indicative mood, and active voice.
- **ὅτι**: This is a conjunction that means "that." It is used to introduce a subordinate clause.
- **προφήτης**: This is a noun that means "prophet." It is in the nominative case, singular number, and masculine gender.
- **ἐν**: This is a preposition that means "in" or "among."
- **τῇ**: This is the definite article that means "the." It agrees with the following noun in case, number, and gender.
- **ἰδίᾳ**: This is an adjective that means "own" or "personal." It is in the dative case, singular number, and feminine gender.
- **πατρίδι**: This is a noun that means "homeland" or "country." It is in the dative case, singular number, and feminine gender.
- **τιμὴν**: This is a noun that means "honor" or "respect." It is in the accusative case, singular number, and feminine gender.
- **οὐκ**: This is an adverb that means "not."
- **ἔχει**: This is a verb that means "he has" or "he possesses." It is in the third person singular, present tense, indicative mood, and active voice.

The phrase "αὐτὸς γὰρ Ἰησοῦς ἐμαρτύρησεν" means "for Jesus himself testified." The phrase "ὅτι προφήτης ἐν τῇ ἰδίᾳ πατρίδι τιμὴν οὐκ ἔχει" means "that a prophet does not have honor in his own country."

Literal translation: "For Jesus himself testified that a prophet does not have honor in his own country."