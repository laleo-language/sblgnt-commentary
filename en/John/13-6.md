---
version: 1
---
- **ἔρχεται οὖν**: The verb ἔρχεται is the third person singular present indicative middle of ἔρχομαι, meaning "he comes" or "he is coming." The word οὖν is an adverb that means "therefore" or "so." The phrase thus means "therefore he comes" or "so he comes."

- **πρὸς Σίμωνα Πέτρον**: The word πρὸς is a preposition that means "to" or "towards." Σίμωνα is the accusative singular of Σίμων, which is the name "Simon." Πέτρον is the accusative singular of Πέτρος, which is the name "Peter." The phrase means "to Simon Peter" or "towards Simon Peter."

- **λέγει αὐτῷ**: The verb λέγει is the third person singular present indicative active of λέγω, meaning "he says" or "he is saying." The word αὐτῷ is a pronoun that means "to him." The phrase means "he says to him."

- **Κύριε, σύ μου νίπτεις τοὺς πόδας**: Κύριε is a vocative form of Κύριος, which means "Lord." Σύ is the nominative singular pronoun, meaning "you." Μου is the genitive singular pronoun, meaning "my." Νίπτεις is the second person singular present indicative active of νίπτω, meaning "you wash." Τοὺς πόδας is the accusative plural of πούς, meaning "the feet." The phrase means "Lord, you are washing my feet."

The literal translation of the entire verse is: "Therefore he comes to Simon Peter. He says to him, 'Lord, you are washing my feet?'"