---
version: 1
---
- **καθὼς γινώσκει με ὁ πατὴρ**: The word καθὼς is a conjunction that means "just as" or "in the same way." The verb γινώσκει is the third person singular present indicative of γινώσκω, which means "he knows" or "he understands." The pronoun με is the accusative singular form of ἐγώ, meaning "me." The article ὁ is the definite article, meaning "the." And the noun πατὴρ is the nominative singular form of πατήρ, which means "father." The phrase thus means "just as the Father knows me."

- **κἀγὼ γινώσκω τὸν πατέρα**: The word κἀγὼ is a conjunction that combines καί (and) with ἐγώ (I). The verb γινώσκω is the first person singular present indicative of γινώσκω, meaning "I know" or "I understand." The article τὸν is the accusative singular form of ὁ, meaning "the." And the noun πατέρα is the accusative singular form of πατήρ, which means "father." The phrase thus means "and I know the Father."

- **καὶ τὴν ψυχήν μου τίθημι ὑπὲρ τῶν προβάτων**: The conjunction καί means "and." The article τὴν is the accusative singular form of ὁ, meaning "the." The noun ψυχήν is the accusative singular form of ψυχή, which means "life" or "soul." The pronoun μου is the genitive singular form of ἐγώ, meaning "my." The verb τίθημι is the first person singular present indicative of τίθημι, meaning "I lay down" or "I give up." The preposition ὑπὲρ means "for" or "on behalf of." And the article τῶν is the genitive plural form of ὁ, meaning "the." The noun προβάτων is the genitive plural form of πρόβατον, which means "sheep." The phrase thus means "and I lay down my life for the sheep."

The entire verse can be translated as: "Just as the Father knows me and I know the Father, and I lay down my life for the sheep."