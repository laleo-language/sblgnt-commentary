---
version: 1
---
- **καὶ εἶπεν αὐτῷ**: The word καὶ is a conjunction meaning "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "and he said to him."

- **Ὕπαγε νίψαι εἰς τὴν κολυμβήθραν τοῦ Σιλωάμ**: The verb Ὕπαγε is the second person singular present imperative of ὑπάγω, meaning "go." The verb νίψαι is the second person singular aorist imperative of νίπτω, meaning "wash." The preposition εἰς means "into." The article τὴν is the accusative singular feminine of ὁ, meaning "the." The noun κολυμβήθραν is the accusative singular of κολυμβήθρα, meaning "pool." The genitive τοῦ is the genitive singular of ὁ, meaning "of the." The noun Σιλωάμ is the genitive singular of Σιλωάμ, meaning "Siloam." The phrase thus means "Go, wash in the pool of Siloam."

- **(ὃ ἑρμηνεύεται Ἀπεσταλμένος)**: The relative pronoun ὃ is the nominative singular neuter of ὅς, meaning "which." The verb ἑρμηνεύεται is the third person singular present indicative middle/passive of ἑρμηνεύω, meaning "it is translated" or "it means." The verb form is middle/passive because it refers to the action of the word being translated. The verb could also be understood as a genitive absolute construction, meaning "when it is translated." The noun Ἀπεσταλμένος is the nominative singular masculine of Ἀπεσταλμένος, meaning "sent one." The phrase thus means "(which is translated/sent one)."

- **ἀπῆλθεν οὖν**: The verb ἀπῆλθεν is the third person singular aorist indicative of ἀπέρχομαι, meaning "he went away" or "he departed." The adverb οὖν means "therefore" or "so." The phrase thus means "he went away, therefore."

- **καὶ ἐνίψατο**: The word καὶ is a conjunction meaning "and." The verb ἐνίψατο is the third person singular aorist middle deponent indicative of νίπτω, meaning "he washed." The phrase thus means "and he washed."

- **καὶ ἦλθεν βλέπων**: The word καὶ is a conjunction meaning "and." The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "he came." The participle βλέπων is the nominative singular masculine present active of βλέπω, meaning "seeing" or "looking." The phrase thus means "and he came, looking."

**Literal Translation**: And he said to him, "Go, wash in the pool of Siloam" (which is translated/sent one). So he went away and washed, and came back seeing.

The verse describes an interaction between Jesus and a blind man. Jesus tells the man to go and wash in the pool of Siloam, and the man follows Jesus' instructions, washes, and then returns with his sight restored.