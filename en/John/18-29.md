---
version: 1
---
- **ἐξῆλθεν οὖν ὁ Πιλᾶτος**: The verb ἐξῆλθεν is the third person singular aorist indicative of ἐξέρχομαι, meaning "he went out." The subject Πιλᾶτος is the nominative singular form of Πιλᾶτος, which is the name "Pilate." The phrase means "Pilate went out."

- **ἔξω πρὸς αὐτοὺς**: The word ἔξω is an adverb meaning "out." The preposition πρὸς means "to" or "towards." The pronoun αὐτοὺς is the accusative plural form of αὐτός, meaning "them." The phrase means "out to them" or "towards them."

- **καὶ φησίν**: The conjunction καὶ means "and." The verb φησίν is the third person singular present indicative of φημί, meaning "he says." The phrase means "and he says."

- **Τίνα κατηγορίαν φέρετε**: The pronoun Τίνα is the accusative singular form of τίς, meaning "who" or "what." The noun κατηγορίαν is the accusative singular of κατηγορία, which means "accusation" or "charge." The verb φέρετε is the second person plural present indicative of φέρω, meaning "you bring." The phrase means "What accusation do you bring?"

- **κατὰ τοῦ ἀνθρώπου τούτου**: The preposition κατὰ means "against" or "concerning." The article τοῦ is the genitive singular form of ὁ, meaning "the." The noun ἀνθρώπου is the genitive singular of ἄνθρωπος, meaning "man" or "person." The pronoun τούτου is the genitive singular form of οὗτος, meaning "this." The phrase means "against this man."

The verse can be translated as: "Pilate went out to them and he says, 'What accusation do you bring against this man?'"