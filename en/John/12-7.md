---
version: 1
---
- **εἶπεν οὖν ὁ Ἰησοῦς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The word οὖν is a conjunction meaning "therefore" or "so." The noun ὁ Ἰησοῦς is the nominative singular of Ἰησοῦς, which means "Jesus." The phrase thus means "Therefore Jesus said."

- **Ἄφες αὐτήν**: The verb Ἄφες is the second person singular present imperative of ἀφίημι, which means "let (her) go" or "allow (her)." The pronoun αὐτήν is the accusative singular feminine of αὐτός, which means "her" in this context. The phrase thus means "Let her go."

- **ἵνα εἰς τὴν ἡμέραν τοῦ ἐνταφιασμοῦ μου τηρήσῃ αὐτό**: The conjunction ἵνα introduces a purpose clause, meaning "in order that" or "so that." The preposition εἰς means "for" or "towards." The article τὴν is the accusative singular feminine of ὁ, which means "the." The noun ἡμέραν is the accusative singular feminine of ἡμέρα, which means "day." The genitive phrase τοῦ ἐνταφιασμοῦ μου means "of my burial." The verb τηρήσῃ is the third person singular aorist subjunctive of τηρέω, which means "she may keep" or "she may preserve." The pronoun αὐτό is the accusative singular neuter of αὐτός, which means "it" in this context. The phrase thus means "so that she may keep it for the day of my burial."

Putting it all together, the literal translation of John 12:7 is "Therefore Jesus said, 'Let her go, so that she may keep it for the day of my burial.'"