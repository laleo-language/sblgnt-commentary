---
version: 1
---
- **μὴ δοκεῖτε**: The word μὴ is a particle that negates the verb that follows it. The verb δοκεῖτε is the second person plural present indicative of δοκέω, which means "to think" or "to suppose." The phrase thus means "Do not think."

- **ὅτι ἐγὼ κατηγορήσω ὑμῶν πρὸς τὸν πατέρα**: The word ὅτι is a conjunction that introduces a clause. The verb κατηγορήσω is the first person singular future indicative of κατηγορέω, which means "to accuse" or "to bring charges against." The pronoun ὑμῶν is the genitive plural of ὑμεῖς, which means "you." The noun πρὸς τὸν πατέρα is a prepositional phrase that means "against the Father." The phrase thus means "that I will accuse you before the Father."

- **ἔστιν ὁ κατηγορῶν ὑμῶν Μωϋσῆς**: The verb ἔστιν is the third person singular present indicative of εἰμί, which means "he is." The participle κατηγορῶν is the nominative singular masculine present active participle of κατηγορέω, which means "accusing." The pronoun ὑμῶν is the genitive plural of ὑμεῖς, which means "you." The noun Μωϋσῆς is the nominative singular of Μωϋσῆς, which means "Moses." The phrase thus means "Moses, who is accusing you."

- **εἰς ὃν ὑμεῖς ἠλπίκατε**: The preposition εἰς means "to" or "towards." The pronoun ὃν is the accusative singular masculine of ὅς, which means "whom." The pronoun ὑμεῖς is the nominative plural of ὑμεῖς, which means "you." The verb ἠλπίκατε is the second person plural perfect indicative of ἐλπίζω, which means "to hope." The phrase thus means "whom you have hoped for."

The entire verse translates to: "Do not think that I will accuse you before the Father; there is one who accuses you, Moses, in whom you have hoped."