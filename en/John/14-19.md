---
version: 1
---
- **ἔτι μικρὸν**: The word ἔτι means "yet" or "still," and μικρὸν is the accusative singular of μικρός, meaning "small" or "little." The phrase ἔτι μικρὸν can be translated as "a little while longer."

- **καὶ ὁ κόσμος με οὐκέτι θεωρεῖ**: The word καὶ is a conjunction meaning "and," and ὁ κόσμος is the subject of the sentence, meaning "the world." The word με is the accusative singular of ἐγώ, meaning "me." The verb θεωρεῖ is the third person singular present indicative of θεωρέω, meaning "he sees" or "he perceives." The phrase καὶ ὁ κόσμος με οὐκέτι θεωρεῖ can be translated as "and the world no longer sees me."

- **ὑμεῖς δὲ θεωρεῖτέ με**: The word ὑμεῖς is the subject of the sentence, meaning "you." The verb θεωρεῖτέ is the second person plural present indicative of θεωρέω, meaning "you see" or "you perceive." The word με is the accusative singular of ἐγώ, meaning "me." The phrase ὑμεῖς δὲ θεωρεῖτέ με can be translated as "but you see me."

- **ὅτι ἐγὼ ζῶ καὶ ὑμεῖς ζήσετε**: The word ὅτι is a conjunction meaning "because" or "that." The word ἐγώ is the subject of the sentence, meaning "I." The verb ζῶ is the first person singular present indicative of ζάω, meaning "I live." The word καὶ is a conjunction meaning "and," and ὑμεῖς is the subject of the sentence, meaning "you." The verb ζήσετε is the second person plural future indicative of ζάω, meaning "you will live." The phrase ὅτι ἐγὼ ζῶ καὶ ὑμεῖς ζήσετε can be translated as "because I live, you will live."

Literal translation: "A little while longer and the world no longer sees me, but you see me because I live and you will live."