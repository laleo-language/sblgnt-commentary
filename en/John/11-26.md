---
version: 1
---
- **καὶ πᾶς ὁ ζῶν**: The word καὶ is a conjunction meaning "and." The word πᾶς is an adjective meaning "all" or "every." The article ὁ is the definite article meaning "the." The participle ζῶν is the nominative singular masculine present participle of ζάω, meaning "living." So this phrase means "and everyone who is living."

- **καὶ πιστεύων εἰς ἐμὲ**: The word καὶ is a conjunction meaning "and." The participle πιστεύων is the nominative singular masculine present participle of πιστεύω, meaning "believing" or "having faith." The preposition εἰς means "into" or "to." The pronoun ἐμὲ is the accusative singular first person pronoun, meaning "me." So this phrase means "and everyone who is believing in me."

- **οὐ μὴ ἀποθάνῃ εἰς τὸν αἰῶνα**: The word οὐ is the negation particle meaning "not." The particle μὴ is also a negation particle, adding emphasis to the negation. The verb ἀποθάνῃ is the third person singular aorist subjunctive of ἀποθνήσκω, meaning "to die." The preposition εἰς means "into" or "to." The article τὸν is the definite article meaning "the." The noun αἰῶνα is the accusative singular of αἰών, meaning "age" or "eternity." So this phrase means "will not die into the age."

- **πιστεύεις τοῦτο**: The verb πιστεύεις is the second person singular present indicative of πιστεύω, meaning "you believe" or "you have faith." The pronoun τοῦτο is the accusative singular neuter pronoun, meaning "this." So this phrase means "do you believe this?"

The literal translation of John 11:26 is: "And everyone who is living and believing in me will not die into the age. Do you believe this?"