---
version: 1
---
- **ὁ πιστεύων ἐν αὐτῷ**: The article ὁ, meaning "the," is modifying the participle πιστεύων, which is the present active nominative singular masculine form of πιστεύω, meaning "to believe." The preposition ἐν means "in." The pronoun αὐτῷ is the dative singular masculine form of αὐτός, meaning "him" or "it." The phrase thus means "the one believing in him" or "the one who believes in him."

- **ἵνα πᾶς ὁ πιστεύων ἔχῃ ζωὴν αἰώνιον**: The conjunction ἵνα introduces a purpose clause. The word πᾶς is an adjective meaning "every" or "all." The phrase ὁ πιστεύων ἔχῃ is a subjunctive verb construction expressing a present tense action. The verb ἔχῃ is the third person singular present subjunctive active form of ἔχω, meaning "to have." The noun ζωὴν is the accusative singular form of ζωή, meaning "life." The adjective αἰώνιον means "eternal" or "everlasting." The phrase can be translated as "so that everyone who believes may have eternal life."

- **ἵνα πᾶς ὁ πιστεύων ἔχῃ ζωὴν αἰώνιον**: "So that everyone who believes in him may have eternal life."

In this verse, Jesus is speaking and explaining the purpose of his coming. He states that the purpose is for everyone who believes in him to have eternal life. The phrase "the one who believes in him" emphasizes the importance of faith in Jesus for obtaining eternal life.