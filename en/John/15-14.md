---
version: 1
---
- **ὑμεῖς φίλοι μού ἐστε**: The pronoun ὑμεῖς means "you" and is the subject of the sentence. The noun φίλοι is the nominative plural of φίλος, which means "friends." The possessive pronoun μού means "my" and modifies φίλοι. The verb ἐστε is the second person plural present indicative of εἰμί, meaning "you are." The phrase thus means "you are my friends."

- **ἐὰν ποιῆτε ⸀ἃ ἐγὼ ἐντέλλομαι ὑμῖν**: The conjunction ἐὰν means "if." The verb ποιῆτε is the second person plural present subjunctive of ποιέω, meaning "you do" or "you make." The relative pronoun ἃ means "what" or "that which." The pronoun ἐγὼ means "I" and is the subject of the verb ἐντέλλομαι, which is the first person singular present indicative middle/passive of ἐντέλλομαι, meaning "I command" or "I instruct." The pronoun ὑμῖν means "to you" and is the indirect object of the verb. The phrase can be translated as "if you do what I command you."

The literal translation of the verse is: "You are my friends if you do what I command you."