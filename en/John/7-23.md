---
version: 1
---
- **εἰ περιτομὴν λαμβάνει ἄνθρωπος**: The word εἰ is a conjunction meaning "if." The noun περιτομὴν is the accusative singular of περιτομή, which means "circumcision." The verb λαμβάνει is the third person singular present indicative of λαμβάνω, meaning "he takes" or "he receives." The noun ἄνθρωπος is the nominative singular of ἄνθρωπος, which means "man" or "person." The phrase thus means "if a person receives circumcision."

- **ἐν σαββάτῳ ἵνα μὴ λυθῇ ὁ νόμος Μωϋσέως**: The phrase ἐν σαββάτῳ means "on the Sabbath." The conjunction ἵνα introduces a purpose clause, meaning "so that." The verb λυθῇ is the third person singular aorist subjunctive of λύω, meaning "it may not be broken" or "it may not be violated." The definite article ὁ indicates that νόμος is the subject of the verb, and it means "the law." The genitive Μωϋσέως indicates possession, meaning "of Moses." The phrase thus means "on the Sabbath, so that the law of Moses may not be violated."

- **ἐμοὶ χολᾶτε**: The word ἐμοὶ is the dative singular of ἐγώ, which means "to me" or "for me." The verb χολᾶτε is the second person plural present indicative of χολάω, which means "you are angry" or "you are indignant." The phrase thus means "you are angry with me."

- **ὅτι ὅλον ἄνθρωπον ὑγιῆ ἐποίησα ἐν σαββάτῳ**: The conjunction ὅτι introduces a subordinate clause, meaning "because" or "that." The adjective ὅλον is the accusative singular of ὅλος, which means "whole" or "entire." The noun ἄνθρωπον is the accusative singular of ἄνθρωπος, which means "man" or "person." The adjective ὑγιῆ is the accusative singular of ὑγιής, which means "healthy" or "whole." The verb ἐποίησα is the first person singular aorist indicative of ποιέω, meaning "I made" or "I healed." The phrase thus means "because I made the whole person healthy on the Sabbath."

The literal translation of the entire verse is: "If a person receives circumcision on the Sabbath, so that the law of Moses may not be violated, you are angry with me because I made the whole person healthy on the Sabbath?"