---
version: 1
---
- **καὶ γνώσεσθε τὴν ἀλήθειαν**: The word γνώσεσθε is the second person plural future indicative of γιγνώσκω, meaning "you will know" or "you will understand." The noun ἀλήθειαν is the accusative singular of ἀλήθεια, which means "truth." The phrase thus means "and you will know the truth."

- **καὶ ἡ ἀλήθεια ἐλευθερώσει ὑμᾶς**: The noun ἀλήθεια is again used here, meaning "truth." The verb ἐλευθερώσει is the third person singular future indicative of ἐλευθερόω, meaning "will set free" or "will liberate." The pronoun ὑμᾶς is the accusative plural of ὑμεῖς, which means "you." The phrase thus means "and the truth will set you free."

The literal translation of John 8:32 is "And you will know the truth, and the truth will set you free."