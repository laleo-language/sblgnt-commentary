---
version: 1
---
- **ἀπῆλθεν ὁ ἄνθρωπος**: The word ἀπῆλθεν is the third person singular aorist indicative of ἀπέρχομαι, which means "he went away" or "he departed." The article ὁ is the nominative singular masculine article, meaning "the." The noun ἄνθρωπος is the nominative singular masculine noun, meaning "man." The phrase thus means "the man went away."

- **καὶ ⸀ἀνήγγειλεν τοῖς Ἰουδαίοις**: The conjunction καὶ means "and." The verb ἀνήγγειλεν is the third person singular aorist indicative of ἀναγγέλλω, meaning "he announced" or "he reported." The article τοῖς is the dative plural masculine article, meaning "to the." The noun Ἰουδαίοις is the dative plural masculine noun, meaning "Jews." The phrase thus means "and he announced to the Jews."

- **ὅτι Ἰησοῦς ἐστιν ὁ ποιήσας αὐτὸν ὑγιῆ**: The conjunction ὅτι means "that." The noun Ἰησοῦς is the nominative singular masculine noun, meaning "Jesus." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The article ὁ is the nominative singular masculine article, meaning "the." The participle ποιήσας is the nominative singular masculine aorist active participle of ποιέω, meaning "having made" or "having healed." The pronoun αὐτὸν is the accusative singular masculine pronoun, meaning "him." The adjective ὑγιῆ is the accusative singular masculine adjective, meaning "healthy." The phrase thus means "that Jesus is the one who healed him."

Overall, the verse reads: "The man went away and announced to the Jews that Jesus is the one who healed him."

The verse is not syntactically ambiguous.