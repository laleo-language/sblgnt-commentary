---
version: 1
---
- **ὑπόδειγμα γὰρ ἔδωκα ὑμῖν**: The noun ὑπόδειγμα is the accusative singular of ὑπόδειγμα, which means "example" or "pattern." The verb ἔδωκα is the first person singular aorist indicative of δίδωμι, meaning "I gave." The phrase thus means "I gave you an example."

- **ἵνα καθὼς ἐγὼ ἐποίησα ὑμῖν καὶ ὑμεῖς ποιῆτε**: The conjunction ἵνα introduces a purpose clause, indicating the reason for giving the example. The phrase καθὼς ἐγὼ ἐποίησα means "as I did" or "just as I did." The verb ποιῆτε is the second person plural present subjunctive of ποιέω, meaning "you do" or "you make." The phrase thus means "so that you may do as I did."

The literal translation of the verse is: "For I gave you an example, so that just as I did, you may do also."