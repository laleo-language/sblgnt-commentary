---
version: 1
---
- **κἀγὼ**: The word κἀγὼ is a contraction of καὶ ἐγώ. The word καὶ means "and" and ἐγώ means "I". So the phrase κἀγὼ means "and I".

- **τὴν δόξαν**: The noun δόξα means "glory" and is in the accusative case. The definite article τὴν indicates that it is a specific glory. So the phrase τὴν δόξαν means "the glory".

- **ἣν δέδωκάς μοι**: The relative pronoun ἣν means "which" or "that". The verb δέδωκάς is the second person singular aorist indicative of δίδωμι, meaning "you have given". The pronoun μοι is the dative form of ἐγώ, meaning "to me". So the phrase ἣν δέδωκάς μοι means "which you have given to me".

- **δέδωκα αὐτοῖς**: The verb δέδωκα is the first person singular perfect indicative of δίδωμι, meaning "I have given". The pronoun αὐτοῖς is the dative form of αὐτός, meaning "to them". So the phrase δέδωκα αὐτοῖς means "I have given to them".

- **ἵνα ὦσιν ἓν**: The conjunction ἵνα introduces a purpose clause and means "so that". The verb ὦσιν is the third person plural present subjunctive of εἰμί, meaning "they may be". The adjective ἓν means "one". So the phrase ἵνα ὦσιν ἓν means "so that they may be one".

- **καθὼς ἡμεῖς ἕν**: The adverb καθὼς means "just as" or "according to". The pronoun ἡμεῖς means "we". The adjective ἕν means "one". So the phrase καθὼς ἡμεῖς ἕν means "just as we are one".

The entire verse can be literally translated as: "And I have given them the glory which you have given to me, so that they may be one, just as we are one."