---
version: 1
---
- **Ἐκ τούτου**: The preposition ἐκ means "from" and is followed by the genitive case, so τούτου is the genitive singular of οὗτος, meaning "this." The phrase Ἐκ τούτου can be translated as "from this."

- **πολλοὶ**: This is the nominative plural form of πολύς, meaning "many." It functions as the subject of the verb.

- **ἐκ τῶν μαθητῶν αὐτοῦ**: The preposition ἐκ again means "from," and is followed by the genitive case. The noun μαθητῶν is the genitive plural form of μαθητής, meaning "disciple." The pronoun αὐτοῦ is the genitive singular form of αὐτός, meaning "his" (referring to Jesus). The phrase ἐκ τῶν μαθητῶν αὐτοῦ can be translated as "from his disciples."

- **ἀπῆλθον**: This is the third person plural aorist indicative active form of ἀπέρχομαι, meaning "they went away."

- **εἰς τὰ ὀπίσω**: The preposition εἰς means "to" or "toward," and is followed by the accusative case. The article τὰ is the accusative plural form of the definite article ὁ, meaning "the." The noun ὀπίσω means "behind" or "back." The phrase εἰς τὰ ὀπίσω can be translated as "to the back" or "behind."

- **καὶ οὐκέτι μετʼ αὐτοῦ περιεπάτουν**: The conjunction καὶ means "and." The adverb οὐκέτι means "no longer" or "not anymore." The preposition μετʼ means "with" and is followed by the genitive case. The pronoun αὐτοῦ is the genitive singular form of αὐτός, meaning "his" (referring to Jesus). The verb περιεπάτουν is the third person plural imperfect indicative active form of περιπατέω, meaning "they were walking." The phrase καὶ οὐκέτι μετʼ αὐτοῦ περιεπάτουν can be translated as "and they were no longer walking with him."

The literal translation of John 6:66 is: "From this, many of his disciples went away to the back and were no longer walking with him."