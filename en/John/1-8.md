---
version: 1
---
- **οὐκ ἦν**: The word οὐκ is the negative particle meaning "not." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he/she/it was." The phrase οὐκ ἦν together means "he was not."

- **ἐκεῖνος τὸ φῶς**: The word ἐκεῖνος is a pronoun meaning "that" or "he." The article τὸ is the neuter singular nominative of ὁ, meaning "the." The noun φῶς is the neuter singular nominative of φῶς, meaning "light." The phrase ἐκεῖνος τὸ φῶς together means "that light."

- **ἀλλʼ ἵνα μαρτυρήσῃ περὶ τοῦ φωτός**: The word ἀλλʼ is a conjunction meaning "but" or "except." The conjunction ἵνα introduces a purpose clause, meaning "in order that" or "so that." The verb μαρτυρήσῃ is the third person singular aorist subjunctive of μαρτυρέω, meaning "he may testify." The preposition περὶ means "about" or "concerning." The article τοῦ is the masculine singular genitive of ὁ, meaning "the." The noun φωτός is the masculine singular genitive of φῶς, meaning "light." The phrase ἵνα μαρτυρήσῃ περὶ τοῦ φωτός together means "so that he may testify about the light."

The literal translation of John 1:8 is: "He was not that light, but [he came] in order that he may testify about the light."

In this verse, John is distinguishing between "that light" (referring to the true light, which is Jesus) and someone else who came to testify about the light. The syntax is clear and there are no ambiguities.