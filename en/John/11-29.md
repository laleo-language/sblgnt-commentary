---
version: 1
---
- **ἐκείνη** is the nominative singular feminine form of the demonstrative pronoun
  ἐκεῖνος, meaning "that" or "she." It refers to Martha, who is the subject of the
  sentence.
- **δὲ** is a conjunction that can be translated as "but" or "and." In this context, it
  serves to connect the previous phrase with the following one.
- **ὡς** is a subordinating conjunction that can be translated as "when" or "as." It
  introduces a subordinate clause that describes the action of Martha.
- **ἤκουσεν** is the third person singular aorist indicative active form of the verb
  ἀκούω, meaning "to hear." It indicates that Martha heard something.
- **ἠγέρθη** is the third person singular aorist passive form of the verb ἐγείρω, meaning
  "to rise" or "to get up." It describes the action of Martha getting up.
- **ταχὺ** is an adverb that means "quickly" or "immediately." It modifies the verb
  ἠγέρθη and emphasizes the swiftness of Martha's response.
- **καὶ** is a conjunction that can be translated as "and." It connects the previous
  phrase with the following one.
- **ἤρχετο** is the third person singular imperfect indicative middle form of the verb
  ἔρχομαι, meaning "to come." It indicates that Martha was coming towards Jesus.
- **πρὸς αὐτόν** is a prepositional phrase composed of the preposition πρός, meaning
  "towards," and the pronoun αὐτός, meaning "him." It indicates the direction of Martha's
  movement.

Literal translation: "But when she heard, she quickly got up and was coming towards him."

The verse describes Martha's immediate response upon hearing that Jesus had arrived. Her quick action demonstrates her eagerness to meet Jesus.