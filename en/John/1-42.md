---
version: 1
---
- **ἤγαγεν αὐτὸν πρὸς τὸν Ἰησοῦν**: The verb ἤγαγεν is the third person singular aorist indicative active of ἄγω, meaning "he brought" or "he led." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, meaning "him." The preposition πρὸς means "to" or "toward." The article τὸν is the accusative singular masculine of ὁ, meaning "the." The name Ἰησοῦν is the accusative singular of Ἰησοῦς, meaning "Jesus." The phrase thus means "he brought him to Jesus."

- **ἐμβλέψας αὐτῷ ὁ Ἰησοῦς εἶπεν**: The participle ἐμβλέψας is the nominative singular masculine of ἐμβλέπων, meaning "looking" or "gazing." The pronoun αὐτῷ is the dative singular masculine of αὐτός, meaning "to him." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The name Ἰησοῦς is the nominative singular of Ἰησοῦς, meaning "Jesus." The verb εἶπεν is the third person singular aorist indicative active of λέγω, meaning "he said." The phrase thus means "Jesus, looking at him, said."

- **Σὺ εἶ Σίμων ὁ υἱὸς Ἰωάννου**: The pronoun Σὺ is the nominative singular of σύ, meaning "you." The verb εἶ is the second person singular present indicative active of εἰμί, meaning "you are." The name Σίμων is the nominative singular of Σίμων, meaning "Simon." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The name ὁ υἱὸς Ἰωάννου means "the son of John." The phrase thus means "You are Simon, the son of John."

- **σὺ κληθήσῃ Κηφᾶς (ὃ ἑρμηνεύεται Πέτρος)**: The pronoun σὺ is the nominative singular of σύ, meaning "you." The verb κληθήσῃ is the second person singular future passive indicative of καλέω, meaning "you will be called." The name Κηφᾶς is the nominative singular of Κηφᾶς, which is a transliteration of the Aramaic name Cephas, meaning "rock" or "stone." The phrase ὃ ἑρμηνεύεται Πέτρος means "which is translated Peter." The phrase thus means "You will be called Cephas (which is translated Peter)."

The literal translation of John 1:42 is: "He brought him to Jesus. Jesus, looking at him, said, 'You are Simon, the son of John. You will be called Cephas (which is translated Peter)."