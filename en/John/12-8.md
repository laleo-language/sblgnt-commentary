---
version: 1
---
- **τοὺς πτωχοὺς**: The word πτωχοὺς is the accusative plural of πτωχός, which means "poor." The article τοὺς indicates that it is a specific group of poor people. The phrase thus means "the poor."

- **γὰρ**: The word γὰρ is a conjunction that introduces a reason or explanation. It can be translated as "for" or "because."

- **πάντοτε**: The word πάντοτε is an adverb that means "always" or "at all times."

- **ἔχετε**: The word ἔχετε is the second person plural present indicative of ἔχω, which means "you have."

- **μεθʼ ἑαυτῶν**: The word μεθʼ is a preposition that means "with." The reflexive pronoun ἑαυτῶν means "yourselves." The phrase thus means "with yourselves."

- **ἐμὲ δὲ**: The word ἐμὲ is the accusative singular of ἐγώ, which means "me" or "I." The particle δὲ is a conjunction that contrasts or adds information. It can be translated as "but" or "and."

- **οὐ πάντοτε ἔχετε**: The word οὐ is a negation particle that means "not." The phrase πάντοτε ἔχετε is the same as mentioned earlier, meaning "you always have." The phrase thus means "you do not always have."

The sentence as a whole can be translated as: "For you always have the poor with you, but you do not always have me."

In this verse, Jesus is responding to Judas Iscariot's objection to Mary anointing Jesus' feet with expensive perfume. Jesus acknowledges that there will always be poor people, but he reminds them that he will not always be physically present with them.