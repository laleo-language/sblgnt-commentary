---
version: 1
---
- **καὶ ἐκπορεύσονται**: The word καὶ is a conjunction meaning "and." The verb ἐκπορεύσονται is the third person plural future indicative of ἐκπορεύομαι, which means "they will come out" or "they will go forth."

- **οἱ τὰ ἀγαθὰ ποιήσαντες**: The article οἱ is the nominative plural of ὁ, which means "the." The word τὰ is the accusative neuter plural of ὁ, which means "the." The adjective ἀγαθὰ is the accusative neuter plural of ἀγαθός, which means "good." The participle ποιήσαντες is the nominative masculine plural aorist active participle of ποιέω, which means "having done" or "having made."

- **εἰς ἀνάστασιν ζωῆς**: The preposition εἰς means "to" or "for." The noun ἀνάστασιν is the accusative singular of ἀνάστασις, which means "resurrection." The noun ζωῆς is the genitive singular of ζωή, which means "life."

- **οἱ δὲ τὰ φαῦλα πράξαντες**: The article οἱ is the nominative plural of ὁ, which means "the." The word δὲ is a conjunction meaning "but." The word τὰ is the accusative neuter plural of ὁ, which means "the." The adjective φαῦλα is the accusative neuter plural of φαῦλος, which means "evil" or "wicked." The participle πράξαντες is the nominative masculine plural aorist active participle of πράσσω, which means "having done" or "having practiced."

- **εἰς ἀνάστασιν κρίσεως**: The preposition εἰς means "to" or "for." The noun ἀνάστασιν is the accusative singular of ἀνάστασις, which means "resurrection." The noun κρίσεως is the genitive singular of κρίσις, which means "judgment."

The literal translation of John 5:29 is: "And they will come out, those having done good to the resurrection of life, but those having done evil to the resurrection of judgment."