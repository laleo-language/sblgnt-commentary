---
version: 1
---
- **ὁ ὀπίσω μου ἐρχόμενος**: The article ὁ is the masculine singular nominative form, meaning "the." The word ὀπίσω is an adverb meaning "behind" or "after." The pronoun μου is the genitive singular form of ἐγώ, meaning "me" or "my." The participle ἐρχόμενος is the present middle (or passive) participle in the nominative singular form of ἔρχομαι, meaning "coming" or "who is coming." The phrase thus means "the one who is coming behind me."

- **οὗ οὐκ εἰμὶ ἄξιος**: The pronoun οὗ is the genitive singular form of ὅς, meaning "of whom" or "whose." The word οὐκ is the negative particle, meaning "not." The verb εἰμὶ is the first person singular present indicative form of εἰμί, meaning "I am." The adjective ἄξιος is the nominative singular form, meaning "worthy" or "deserving." The phrase thus means "of whom I am not worthy."

- **ἵνα λύσω αὐτοῦ τὸν ἱμάντα τοῦ ὑποδήματος**: The conjunction ἵνα introduces a purpose clause, meaning "so that" or "in order to." The verb λύσω is the first person singular future indicative form of λύω, meaning "I may untie" or "I may loosen." The pronoun αὐτοῦ is the genitive singular form of αὐτός, meaning "his" or "of him." The article τὸν is the masculine singular accusative form, meaning "the." The noun ἱμάντα is the accusative singular form of ἱμάς, meaning "strap" or "thong." The article τοῦ is the masculine singular genitive form, meaning "of the." The noun ὑποδήματος is the genitive singular form of ὑπόδημα, meaning "sandal" or "shoe." The phrase thus means "in order to untie his sandal strap."

The literal translation of the entire verse is: "The one who is coming behind me, of whom I am not worthy to untie his sandal strap."