---
version: 1
---
- **εἶπαν οὖν αὐτῷ**: The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "they said to him."

- **Τίς εἶ**: The word Τίς is the nominative singular of τίς, meaning "who." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The phrase thus means "who are you?"

- **ἵνα ἀπόκρισιν δῶμεν τοῖς πέμψασιν ἡμᾶς**: The word ἵνα is a subordinating conjunction meaning "so that" or "in order that." The noun ἀπόκρισιν is the accusative singular of ἀπόκρισις, meaning "answer" or "response." The verb δῶμεν is the first person plural aorist subjunctive of δίδωμι, meaning "we may give." The article τοῖς is the dative plural of ὁ, meaning "to the." The verb πέμψασιν is the third person plural aorist active participle of πέμπω, meaning "they sent." The pronoun ἡμᾶς is the accusative plural of ἐγώ, meaning "us." The phrase thus means "so that we may give an answer to those who sent us."

- **τί λέγεις περὶ σεαυτοῦ**: The word τί is the accusative singular of τίς, meaning "what." The verb λέγεις is the second person singular present indicative of λέγω, meaning "you say." The pronoun περὶ is a preposition meaning "about" or "concerning." The pronoun σεαυτοῦ is the genitive singular reflexive pronoun of σύ, meaning "yourself." The phrase thus means "what do you say about yourself?"

The literal translation of the verse is: "They said to him, 'Who are you?' so that we may give an answer to those who sent us. What do you say about yourself?"