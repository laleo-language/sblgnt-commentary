---
version: 1
---
- **ἀπεκρίθησαν**: The verb ἀπεκρίθησαν is the third person plural aorist indicative of ἀποκρίνομαι, meaning "they answered." 

- **καὶ εἶπαν αὐτῷ**: The conjunction καὶ connects the previous verb with the following verb. The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said." The pronoun αὐτῷ means "to him" and refers to someone in the context.

- **Εἰ μὴ ἦν οὗτος κακὸν ποιῶν**: The conjunction Εἰ introduces a conditional clause, meaning "if." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was." The word οὗτος means "this" and refers to someone in the context. The phrase κακὸν ποιῶν is a present participle phrase, with κακὸν meaning "evil" and ποιῶν meaning "doing." The phrase can be translated as "doing evil."

- **οὐκ ἄν σοι παρεδώκαμεν αὐτόν**: The word οὐκ is a negation, meaning "not." The verb ἄν is an enclitic particle that indicates possibility or uncertainty. The pronoun σοι means "to you" and refers to someone in the context. The verb παρεδώκαμεν is the first person plural aorist indicative of παραδίδωμι, meaning "we handed over." The pronoun αὐτόν means "him" and refers to someone in the context.

Literal translation: They answered and said to him, "If this man were not doing evil, we would not have handed him over to you."