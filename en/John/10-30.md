---
version: 1
---
- **ἐγὼ**: The word ἐγὼ is the first person singular pronoun, meaning "I" or "me."
- **καὶ**: The word καὶ is a conjunction, meaning "and."
- **ὁ πατὴρ**: The phrase ὁ πατὴρ consists of the definite article ὁ, meaning "the," and the noun πατὴρ, meaning "father."
- **ἕν**: The word ἕν is an adjective meaning "one" or "the same."
- **ἐσμεν**: The verb ἐσμεν is the first person plural present indicative of the verb εἰμί, meaning "we are."

The phrase "ἐγὼ καὶ ὁ πατὴρ" means "I and the Father." The word ἕν emphasizes the unity or oneness between "I" and "the Father." The verb ἐσμεν confirms this unity and means "we are." 

Literal translation: "I and the Father are one."