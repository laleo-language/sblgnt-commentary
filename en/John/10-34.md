---
version: 1
---
- **ἀπεκρίθη αὐτοῖς ὁ Ἰησοῦς**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, meaning "he answered." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The name Ἰησοῦς is the nominative singular of Ἰησοῦς, meaning "Jesus." The phrase thus means "Jesus answered them."

- **Οὐκ ἔστιν γεγραμμένον ἐν τῷ νόμῳ ὑμῶν**: The adverb Οὐκ means "not." The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "is." The adjective γεγραμμένον is the neuter singular perfect passive participle of γράφω, meaning "written." The preposition ἐν means "in." The article τῷ is the dative singular masculine of ὁ, meaning "the." The noun νόμῳ is the dative singular of νόμος, meaning "law." The possessive pronoun ὑμῶν is the genitive plural of σύ, meaning "your." The phrase thus means "It is not written in your law."

- **ὅτι Ἐγὼ εἶπα**: The conjunction ὅτι means "that" or "because." The pronoun Ἐγὼ is the first person singular pronoun, meaning "I." The verb εἶπα is the first person singular aorist indicative of λέγω, meaning "I said." The phrase thus means "that I said."

- **Θεοί ἐστε**: The noun Θεοί is the nominative plural of θεός, meaning "gods." The verb ἐστε is the second person plural present indicative of εἰμί, meaning "you are." The phrase thus means "you are gods."

Literal translation: Jesus answered them, "Is it not written in your law that I said, 'You are gods'?"