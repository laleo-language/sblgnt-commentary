---
version: 1
---
- **εἰδὼς δὲ ὁ Ἰησοῦς**: The word εἰδὼς is the nominative masculine singular participle of οἶδα, meaning "knowing" or "being aware." The word δὲ is a conjunction that can be translated as "but" or "and." The name Ἰησοῦς is the nominative masculine singular form of Ἰησοῦς, which means "Jesus." The phrase thus means "But Jesus, knowing."

- **ἐν ἑαυτῷ**: The preposition ἐν means "in." The reflexive pronoun ἑαυτῷ means "himself" or "in himself." The phrase can be translated as "in himself."

- **ὅτι γογγύζουσιν περὶ τούτου οἱ μαθηταὶ αὐτοῦ**: The conjunction ὅτι is used to introduce a direct quotation and can be translated as "that." The verb γογγύζουσιν is the present active indicative third person plural of γογγύζω, meaning "they grumble" or "they murmur." The preposition περὶ means "about" or "concerning." The pronoun τούτου is the genitive neuter singular form of οὗτος, meaning "this." The article οἱ is the nominative masculine plural definite article and can be translated as "the." The noun μαθηταὶ is the nominative masculine plural form of μαθητής, which means "disciples" or "followers." The pronoun αὐτοῦ is the genitive masculine singular form of αὐτός, meaning "his." The phrase can be translated as "that his disciples grumble about this."

- **εἶπεν αὐτοῖς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said" or "he spoke." The pronoun αὐτοῖς is the dative plural form of αὐτός, meaning "to them." The phrase means "he said to them."

- **Τοῦτο ὑμᾶς σκανδαλίζει**: The pronoun Τοῦτο is the accusative neuter singular form of οὗτος, meaning "this." The pronoun ὑμᾶς is the accusative plural form of σύ, meaning "you." The verb σκανδαλίζει is the present active indicative third person singular of σκανδαλίζω, meaning "it scandalizes" or "it offends." The phrase means "Does this offend you?"

In this verse, Jesus is aware that his disciples are grumbling about something and he asks them if it offends them. 

Literal translation: "But Jesus, knowing in himself that his disciples grumble about this, said to them, 'Does this offend you?'"