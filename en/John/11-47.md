---
version: 1
---
- **συνήγαγον οὖν οἱ ἀρχιερεῖς καὶ οἱ Φαρισαῖοι συνέδριον**: The verb συνήγαγον is the third person plural aorist indicative of συνάγω, meaning "they gathered." The noun οἱ ἀρχιερεῖς is the nominative plural of ἀρχιερεύς, meaning "the high priests." The conjunction οὖν means "therefore" or "so." The coordinating conjunction καὶ means "and." The noun οἱ Φαρισαῖοι is the nominative plural of Φαρισαῖος, meaning "the Pharisees." The noun συνέδριον is the accusative singular of συνέδριον, which can mean "council" or "assembly." The phrase thus means "So the high priests and the Pharisees gathered the council."

- **καὶ ἔλεγον**: The conjunction καὶ means "and." The verb ἔλεγον is the third person plural imperfect indicative of λέγω, meaning "they were saying." The phrase thus means "and they were saying."

- **Τί ποιοῦμεν**: The interrogative pronoun Τί means "what." The verb ποιοῦμεν is the first person plural present indicative of ποιέω, meaning "we do" or "we are doing." The phrase thus means "What are we doing?"

- **ὅτι οὗτος ὁ ἄνθρωπος πολλὰ ⸂ποιεῖ σημεῖα⸃**: The conjunction ὅτι can mean "that" or "because." The demonstrative pronoun οὗτος means "this." The article ὁ is the definite article meaning "the." The noun ἄνθρωπος is the nominative singular of ἄνθρωπος, meaning "man" or "person." The adjective πολλὰ is the accusative neuter plural of πολύς, meaning "many." The verb ποιεῖ is the third person singular present indicative of ποιέω, meaning "he does" or "he is doing." The noun σημεῖα is the accusative neuter plural of σημεῖον, meaning "signs." The phrase thus means "that this man is doing many signs."

- The literal translation of the entire verse is: "So the high priests and the Pharisees gathered the council, and they were saying, 'What are we doing, because this man is doing many signs?'"