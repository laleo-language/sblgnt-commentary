---
version: 1
---
- **εἰρήκει δὲ ὁ Ἰησοῦς**: The verb εἰρήκει is the third person singular imperfect indicative of λέγω, meaning "he was saying" or "he said." The subject of the verb is ὁ Ἰησοῦς, which means "Jesus." The phrase thus means "Jesus was saying."

- **περὶ τοῦ θανάτου αὐτοῦ**: The preposition περὶ means "about" or "concerning." The genitive noun τοῦ θανάτου means "of the death." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "about his death."

- **ἐκεῖνοι δὲ ἔδοξαν**: The pronoun ἐκεῖνοι is the nominative plural of ἐκεῖνος, meaning "they." The verb ἔδοξαν is the third person plural aorist indicative of δοκέω, meaning "they thought" or "they believed." The phrase thus means "they believed."

- **ὅτι περὶ τῆς κοιμήσεως τοῦ ὕπνου λέγει**: The conjunction ὅτι introduces a subordinate clause and means "that." The preposition περὶ means "about" or "concerning." The genitive noun τῆς κοιμήσεως means "of the sleep." The genitive noun τοῦ ὕπνου means "of the sleep." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says" or "he is saying." The phrase thus means "that he is saying about the sleep."

**Literal Translation:** Jesus was saying about his death. They believed that he is talking about sleep. 

In this verse, Jesus is speaking about his upcoming death, but those listening misunderstood his reference to "sleep" as a metaphor for death.