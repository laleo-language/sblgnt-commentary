---
version: 1
---
- **καὶ δείπνου γινομένου**: The word δείπνου is the genitive singular of δεῖπνον, which means "dinner" or "meal." The participle γινομένου is the genitive singular masculine of γίνομαι, meaning "taking place" or "being prepared." The phrase καὶ δείπνου γινομένου can be translated as "and while dinner was taking place."

- **τοῦ διαβόλου ἤδη βεβληκότος εἰς τὴν καρδίαν**: The article τοῦ indicates that διαβόλου is in the genitive case, and ἤδη is an adverb meaning "already." The participle βεβληκότος is the genitive singular masculine of βάλλω, meaning "having thrown" or "having put." The preposition εἰς is followed by the accusative case, and τὴν καρδίαν is the accusative singular of καρδία, which means "heart." The phrase τοῦ διαβόλου ἤδη βεβληκότος εἰς τὴν καρδίαν can be translated as "the devil having already put into the heart."

- **ἵνα παραδοῖ αὐτὸν Ἰούδας Σίμωνος Ἰσκαριώτου**: The conjunction ἵνα introduces a purpose clause. The verb παραδοῖ is the third person singular aorist subjunctive of παραδίδωμι, meaning "he may betray" or "he may hand over." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." The name Ἰούδας is in the nominative singular and is the subject of the verb. The genitive Σίμωνος indicates the father's name, Simon. The genitive Ἰσκαριώτου indicates the place of origin or affiliation, Iscariot. The phrase ἵνα παραδοῖ αὐτὸν Ἰούδας Σίμωνος Ἰσκαριώτου can be translated as "so that Judas, son of Simon Iscariot, may betray him."

- The literal translation of the entire verse is: "And while dinner was taking place, the devil having already put into the heart so that Judas, son of Simon Iscariot, may betray him."