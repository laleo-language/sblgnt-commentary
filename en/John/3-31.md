---
version: 1
---
- **Ὁ ἄνωθεν ἐρχόμενος**: The word ἄνωθεν is an adverb meaning "from above" or "from on top." The participle ἐρχόμενος is the present middle or passive participle of ἔρχομαι, which means "coming" or "going." So this phrase can be translated as "the one coming from above."

- **ἐπάνω πάντων ἐστίν**: The word ἐπάνω is a preposition meaning "above" or "over." The word πάντων is the genitive plural of πᾶς, which means "all" or "every." The verb ἐστίν is the third person singular present indicative of εἰμί, which means "he is" or "it is." So this phrase can be translated as "he is above all."

- **ὁ ὢν ἐκ τῆς γῆς**: The word ὁ is the definite article meaning "the." The verb ὢν is the present participle of εἰμί, which means "being" or "existing." The preposition ἐκ means "from." The article τῆς is the genitive singular feminine of ὁ, which means "the." The noun γῆς is the genitive singular of γῆ, which means "earth" or "land." This phrase can be translated as "the one being from the earth."

- **ἐκ τῆς γῆς ἐστιν**: This phrase is similar to the previous one, but without the article. So it can be translated as "he is from the earth."

- **καὶ ἐκ τῆς γῆς λαλεῖ**: The conjunction καὶ means "and." The preposition ἐκ means "from." The article τῆς is the genitive singular feminine of ὁ, which means "the." The noun γῆς is the genitive singular of γῆ, which means "earth" or "land." The verb λαλεῖ is the third person singular present indicative of λαλέω, which means "he speaks" or "he talks." So this phrase can be translated as "and he speaks from the earth."

- **ὁ ἐκ τοῦ οὐρανοῦ ἐρχόμενος**: The word ὁ is the definite article meaning "the." The preposition ἐκ means "from." The article τοῦ is the genitive singular masculine of ὁ, which means "the." The noun οὐρανοῦ is the genitive singular of οὐρανός, which means "heaven" or "sky." The participle ἐρχόμενος is the present middle or passive participle of ἔρχομαι, which means "coming" or "going." So this phrase can be translated as "the one coming from heaven."

- **ἐπάνω πάντων ἐστίν**: This phrase is the same as the second one we encountered, and it can be translated as "he is above all."

In this verse, we see a parallel structure with two contrasting phrases: "the one coming from above" (referring to Jesus) and "the one being from the earth" (referring to humans). The repetition of the phrase "he is above all" emphasizes the superiority of the one coming from above. The verse can be translated as: "The one coming from above is above all. The one being from the earth is from the earth and speaks from the earth. The one coming from heaven is above all."