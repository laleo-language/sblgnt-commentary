---
version: 1
---
- **καὶ εἰσῆλθεν εἰς τὸ πραιτώριον πάλιν**: The word καὶ is a conjunction meaning "and." The verb εἰσῆλθεν is the third person singular aorist indicative of εἰσέρχομαι, meaning "he entered." The preposition εἰς means "into." The article τὸ is the nominative singular neuter of ὁ, meaning "the." The noun πραιτώριον is the nominative singular neuter of πραιτώριον, which means "praetorium" or "governor's residence." The adverb πάλιν means "again." The phrase thus means "And he entered into the praetorium again."

- **καὶ λέγει τῷ Ἰησοῦ**: The word καὶ is a conjunction meaning "and." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The article τῷ is the dative singular masculine of ὁ, meaning "to the." The noun Ἰησοῦς is the nominative singular masculine of Ἰησοῦς, which means "Jesus." The phrase thus means "And he says to Jesus."

- **Πόθεν εἶ σύ**: The noun Πόθεν is an interrogative pronoun meaning "from where." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The pronoun σύ is the second person singular pronoun, meaning "you." The phrase thus means "From where are you?"

- **ὁ δὲ Ἰησοῦς ἀπόκρισιν οὐκ ἔδωκεν αὐτῷ**: The article ὁ is the nominative singular masculine of ὁ, meaning "the." The noun Ἰησοῦς is the nominative singular masculine of Ἰησοῦς, which means "Jesus." The noun ἀπόκρισιν is the accusative singular feminine of ἀπόκρισις, meaning "answer" or "response." The adverb οὐκ means "not." The verb ἔδωκεν is the third person singular aorist indicative of δίδωμι, meaning "he gave." The pronoun αὐτῷ is the dative singular masculine of αὐτός, meaning "to him." The phrase thus means "But Jesus did not give an answer to him."

The literal translation of John 19:9 is: "And he entered into the praetorium again, and he says to Jesus, 'From where are you?' But Jesus did not give him an answer."