---
version: 1
---
- **ἐγείρεται**: The verb ἐγείρεται is the third person singular present middle indicative of ἐγείρω, which means "to rise" or "to get up." The subject of the verb is implied to be Jesus.

- **ἐκ τοῦ δείπνου**: The prepositional phrase ἐκ τοῦ δείπνου consists of the preposition ἐκ, meaning "from," and the noun τὸ δεῖπνον, which means "the meal" or "the supper." The phrase can be translated as "from the meal" or "from the supper."

- **καὶ τίθησιν τὰ ἱμάτια**: The conjunction καὶ connects the previous phrase with the following one. The verb τίθησιν is the third person singular present active indicative of τίθημι, which means "to place" or "to put." The direct object of the verb is τὰ ἱμάτια, which means "the clothes" or "the garments." The phrase can be translated as "and he puts the clothes."

- **καὶ λαβὼν λέντιον διέζωσεν ἑαυτόν**: The conjunction καὶ connects the previous phrase with the following one. The participle λαβὼν is the aorist active nominative singular masculine of λαμβάνω, which means "having taken." The direct object of the participle is λέντιον, which means "a towel" or "a towel for drying." The verb διέζωσεν is the third person singular aorist active indicative of διζώννυμι, which means "he girded" or "he tied around." The reflexive pronoun ἑαυτόν means "himself." The phrase can be translated as "and having taken a towel, he girded himself."

- **ἐγείρεται ἐκ τοῦ δείπνου καὶ τίθησιν τὰ ἱμάτια καὶ λαβὼν λέντιον διέζωσεν ἑαυτόν**: Putting all the phrases together, the sentence can be translated as "He rises from the meal and puts the clothes and, having taken a towel, he girds himself."

The verse can be translated as "He rises from the meal and puts the clothes and, having taken a towel, he girds himself."