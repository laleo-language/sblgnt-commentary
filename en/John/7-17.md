---
version: 1
---
- **ἐάν τις θέλῃ**: The word ἐάν is a conjunction meaning "if." The word τις is an indefinite pronoun meaning "anyone" or "someone." The verb θέλῃ is the third person singular present subjunctive of θέλω, meaning "to want" or "to desire." The phrase thus means "if anyone wants."

- **τὸ θέλημα αὐτοῦ**: The article τὸ is the nominative neuter singular of ὁ, meaning "the." The noun θέλημα is the accusative neuter singular of θέλημα, meaning "will" or "desire." The pronoun αὐτοῦ is the genitive masculine singular of αὐτός, meaning "his" or "his own." The phrase thus means "his will."

- **ποιεῖν**: The verb ποιεῖν is the present infinitive of ποιέω, meaning "to do" or "to make." The phrase thus means "to do."

- **γνώσεται περὶ τῆς διδαχῆς**: The verb γνώσεται is the third person singular future indicative of γινώσκω, meaning "he will know" or "he will understand." The preposition περὶ means "about" or "concerning." The article τῆς is the genitive feminine singular of ὁ, meaning "the." The noun διδαχῆς is the genitive feminine singular of διδαχή, meaning "teaching" or "doctrine." The phrase thus means "he will know about the teaching."

- **πότερον ἐκ τοῦ θεοῦ ἐστιν ἢ ἐγὼ ἀπʼ ἐμαυτοῦ λαλῶ**: The adverb πότερον means "whether" or "either." The preposition ἐκ means "from" or "out of." The article τοῦ is the genitive masculine singular of ὁ, meaning "the." The noun θεοῦ is the genitive masculine singular of θεός, meaning "God." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is" or "it is." The pronoun ἢ means "or." The pronoun ἐγὼ is the first person singular pronoun meaning "I." The preposition ἀπʼ means "from" or "by." The pronoun ἐμαυτοῦ is the genitive masculine singular reflexive pronoun meaning "myself." The verb λαλῶ is the first person singular present indicative of λαλέω, meaning "I speak." The phrase thus means "whether it is from God or I speak from myself."

- **ἐάν τις θέλῃ τὸ θέλημα αὐτοῦ ποιεῖν, γνώσεται περὶ τῆς διδαχῆς πότερον ἐκ τοῦ θεοῦ ἐστιν ἢ ἐγὼ ἀπʼ ἐμαυτοῦ λαλῶ**: If anyone wants to do his will, he will understand about the teaching, whether it is from God or I speak from myself.

The verse presents a conditional statement, suggesting that if someone desires to do God's will, they will come to understand the truth of the teaching. It also introduces the possibility that the teaching may come from God or from the speaker themselves.