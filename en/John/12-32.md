---
version: 1
---
- **κἀγὼ**: The word κἀγὼ is a contraction of καὶ ἐγώ, meaning "and I." It is a conjunction that connects the subject of the sentence, "I," with the verb that follows.
- **ἐὰν**: The word ἐὰν is a conjunction that means "if" or "when."
- **ὑψωθῶ**: The verb ὑψόω is a first person singular aorist subjunctive passive form of ὑψόω, meaning "I am lifted up" or "I am exalted."
- **ἐκ**: The preposition ἐκ means "out of" or "from."
- **τῆς γῆς**: The article τῆς is the genitive singular feminine form of the definite article ὁ, meaning "the." The noun γῆς is the genitive singular form of γῆ, meaning "earth" or "ground." Together, they form the phrase "out of the earth" or "from the earth."
- **πάντας**: The word πάντας is the accusative plural masculine form of πᾶς, meaning "all" or "everyone."
- **ἑλκύσω**: The verb ἑλκύω is a first person singular future indicative active form of ἑλκύω, meaning "I will draw" or "I will attract."
- **πρὸς**: The preposition πρὸς means "towards" or "to."
- **ἐμαυτόν**: The pronoun ἐμαυτόν is the accusative singular form of ἐμαυτός, meaning "myself."

Literal translation: "And I, if I am lifted up from the earth, will draw all to myself."

This verse is not syntactically ambiguous.