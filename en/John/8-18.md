---
version: 1
---
- **ἐγώ εἰμι**: The pronoun ἐγώ means "I" and the verb εἰμι means "am." So this phrase means "I am."
- **ὁ μαρτυρῶν**: The article ὁ indicates that the noun μαρτυρῶν is in the nominative case and singular number. The noun μαρτυρῶν is the present participle of μαρτυρέω, which means "to bear witness" or "to testify." So this phrase means "the one who bears witness."
- **περὶ ἐμαυτοῦ**: The preposition περὶ means "about" and the reflexive pronoun ἐμαυτοῦ means "myself." So this phrase means "about myself."
- **καὶ μαρτυρεῖ περὶ ἐμοῦ**: The conjunction καὶ connects the previous phrase with this one. The verb μαρτυρεῖ is the third person singular present indicative of μαρτυρέω, meaning "he/she/it bears witness" or "he/she/it testifies." So this phrase means "and he bears witness about me."
- **ὁ πέμψας με πατήρ**: The article ὁ indicates that the noun πέμψας is in the nominative case and singular number. The noun πέμψας is the aorist active participle of πέμπω, which means "to send." The pronoun με is the accusative singular form of ἐγώ, meaning "me." The noun πατήρ means "father." So this phrase means "the one who sent me, the Father."

Literal translation: "I am the one who bears witness about myself, and the Father who sent me also bears witness about me."

The verse as a whole emphasizes the testimony of Jesus about himself and the testimony of the Father about Jesus.