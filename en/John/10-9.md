---
version: 1
---
- **ἐγώ εἰμι ἡ θύρα**: The word ἐγώ is the first person singular pronoun, meaning "I." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "I am." The article ἡ is the nominative singular feminine definite article, meaning "the." The noun θύρα is the nominative singular feminine noun, meaning "door." The phrase thus means "I am the door."

- **διʼ ἐμοῦ**: The preposition διά takes the genitive case, so ἐμοῦ is the genitive singular form of ἐγώ. The phrase means "through me."

- **ἐάν τις εἰσέλθῃ**: The conjunction ἐάν introduces a conditional clause. The indefinite pronoun τις is the nominative singular form, meaning "anyone" or "someone." The verb εἰσέρχομαι is the third person singular aorist subjunctive, meaning "he/she/it enters." The phrase thus means "if anyone enters."

- **σωθήσεται**: The verb σῴζω is the third person singular future indicative, meaning "he/she/it will be saved."

- **καὶ εἰσελεύσεται**: The conjunction καί means "and." The verb εἰσέρχομαι is the third person singular future indicative, meaning "he/she/it will enter."

- **καὶ ἐξελεύσεται**: The conjunction καί means "and." The verb ἐξέρχομαι is the third person singular future indicative, meaning "he/she/it will go out."

- **καὶ νομὴν εὑρήσει**: The conjunction καί means "and." The noun νομή is the accusative singular feminine noun, meaning "pasture" or "feeding." The verb εὑρίσκω is the third person singular future indicative, meaning "he/she/it will find." The phrase thus means "and he/she/it will find pasture."

The literal translation of the entire verse is: "I am the door; through me if anyone enters, he will be saved and will go in and out and find pasture."