---
version: 1
---
- **ἀπεκρίθη αὐτῷ Φίλιππος**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, meaning "he answered." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The name Φίλιππος is the nominative singular of Φίλιππος, which means "Philip." The phrase thus means "Philip answered him."

- **Διακοσίων δηναρίων ἄρτοι**: The word Διακοσίων is the genitive plural of Διακόσιοι, meaning "two hundred." The word δηναρίων is the genitive plural of δηνάριον, meaning "denarius" (a Roman silver coin). The noun ἄρτοι is the nominative plural of ἄρτος, meaning "bread." The phrase means "two hundred denarii of bread."

- **οὐκ ἀρκοῦσιν αὐτοῖς**: The word οὐκ is the negative particle, meaning "not." The verb ἀρκοῦσιν is the third person plural present indicative of ἀρκέω, meaning "they are sufficient" or "it is enough." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "for them." The phrase means "it is not enough for them."

- **ἵνα ἕκαστος βραχύ τι λάβῃ**: The conjunction ἵνα introduces a purpose clause, meaning "so that" or "in order that." The pronoun ἕκαστος is the nominative singular of ἕκαστος, meaning "each one." The adjective βραχύ means "a little" or "a small amount." The pronoun τι is an indefinite pronoun, meaning "something." The verb λάβῃ is the third person singular aorist subjunctive of λαμβάνω, meaning "he may receive." The phrase means "so that each one may receive a little."

- **ἀπεκρίθη αὐτῷ Φίλιππος· Διακοσίων δηναρίων ἄρτοι οὐκ ἀρκοῦσιν αὐτοῖς ἵνα ἕκαστος βραχύ τι λάβῃ**: Philip answered him, "Two hundred denarii of bread are not sufficient for them, so that each one may receive a little."

In this verse, Philip responds to Jesus' question about how to feed the large crowd of people. Philip states that two hundred denarii of bread would not be enough to feed everyone, so each person would only receive a small amount.