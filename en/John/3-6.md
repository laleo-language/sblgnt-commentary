---
version: 1
---
- **τὸ γεγεννημένον ἐκ τῆς σαρκὸς**: The article τὸ indicates that this is a neuter singular noun, and the word γεγεννημένον is the perfect participle of γεννάω, meaning "born" or "begotten." The preposition ἐκ means "from" or "out of," and the genitive τῆς σαρκὸς means "of the flesh." The phrase can be translated as "that which has been born of the flesh."

- **σάρξ ἐστιν**: The noun σάρξ means "flesh," and the verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is" or "exists." The phrase can be translated as "it is flesh."

- **καὶ τὸ γεγεννημένον ἐκ τοῦ πνεύματος**: The conjunction καὶ means "and." The article τὸ indicates that this is a neuter singular noun, and the word γεγεννημένον is again the perfect participle of γεννάω, meaning "born" or "begotten." The preposition ἐκ means "from" or "out of," and the genitive τοῦ πνεύματος means "of the Spirit." The phrase can be translated as "that which has been born of the Spirit."

- **πνεῦμά ἐστιν**: The noun πνεῦμα means "Spirit," and the verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is" or "exists." The phrase can be translated as "it is Spirit."

Literal translation: "That which has been born of the flesh is flesh, and that which has been born of the Spirit is Spirit."

In this verse, Jesus is speaking to Nicodemus about the necessity of being born again in order to enter the kingdom of God. He explains that there are two types of birth: one that is physical, "born of the flesh," and one that is spiritual, "born of the Spirit." The phrase highlights the distinction between the physical and spiritual realms, emphasizing the need for a spiritual rebirth in order to have a new life in God's kingdom.