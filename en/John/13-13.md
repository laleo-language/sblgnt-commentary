---
version: 1
---
- **ὑμεῖς φωνεῖτέ με**: The word ὑμεῖς is the nominative plural of σύ, which means "you" (plural). The verb φωνεῖτέ is the second person plural present indicative of φωνέω, meaning "you call" or "you say." The pronoun με is the accusative singular of ἐγώ, which means "me." The phrase thus means "you call me."

- **Ὁ διδάσκαλος καὶ Ὁ κύριος**: The article Ὁ before διδάσκαλος and κύριος indicates that both words are being referred to as specific objects or persons. The noun διδάσκαλος means "teacher" and the noun κύριος means "Lord." The phrase thus means "The teacher and the Lord."

- **καὶ καλῶς λέγετε**: The conjunction καὶ means "and." The adverb καλῶς means "well." The verb λέγετε is the second person plural present indicative of λέγω, meaning "you say" or "you speak." The phrase thus means "and you say well."

- **εἰμὶ γάρ**: The verb εἰμὶ is the first person singular present indicative of εἰμί, meaning "I am." The particle γάρ is used to provide a reason or explanation. The phrase thus means "for I am."

The literal translation of the entire verse would be: "You call me, the teacher and the Lord, and you say well, for I am."