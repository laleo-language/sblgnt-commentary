---
version: 1
---
- **Ἰωάννης μαρτυρεῖ**: The noun Ἰωάννης is the nominative singular of Ἰωάννης, which means "John." The verb μαρτυρεῖ is the third person singular present indicative active of μαρτυρέω, meaning "he testifies." The phrase thus means "John testifies."

- **περὶ αὐτοῦ**: The preposition περὶ means "about" or "concerning." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, meaning "him." The phrase thus means "about him."

- **καὶ κέκραγεν λέγων**: The conjunction καὶ means "and." The verb κέκραγεν is the third person singular perfect indicative active of κράζω, meaning "he has cried out." The participle λέγων is the present active nominative masculine singular of λέγω, meaning "saying." The phrase thus means "and he has cried out, saying."

- **Οὗτος ἦν ὃν εἶπον**: The pronoun Οὗτος is the nominative singular masculine of οὗτος, meaning "this." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was." The relative pronoun ὃν is the accusative singular masculine of ὅς, meaning "whom." The verb εἶπον is the first person singular aorist indicative active of λέγω, meaning "I said." The phrase thus means "This was whom I said."

- **Ὁ ὀπίσω μου ἐρχόμενος ἔμπροσθέν μου γέγονεν**: The article Ὁ is the nominative singular masculine of ὁ, meaning "the." The adverb ὀπίσω means "behind." The pronoun μου is the genitive singular of ἐγώ, meaning "me." The participle ἐρχόμενος is the present middle nominative masculine singular of ἔρχομαι, meaning "coming." The preposition ἔμπροσθεν means "before" or "in front of." The pronoun μου is the genitive singular of ἐγώ, meaning "me." The verb γέγονεν is the third person singular perfect indicative active of γίνομαι, meaning "he has become." The phrase thus means "The one coming behind me has become before me."

- **ὅτι πρῶτός μου ἦν**: The conjunction ὅτι means "because" or "that." The adjective πρῶτός is the nominative singular masculine of πρῶτος, meaning "first." The pronoun μου is the genitive singular of ἐγώ, meaning "my." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was." The phrase thus means "because he was before me."

The literal translation of John 1:15 is: "John testifies about him, and he has cried out, saying, 'This was whom I said, the one coming behind me has become before me, because he was before me.'"