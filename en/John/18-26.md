---
version: 1
---
- **εἷς ἐκ τῶν δούλων τοῦ ἀρχιερέως**: The word εἷς is the masculine nominative singular of εἷς, meaning "one." The phrase ἐκ τῶν δούλων τοῦ ἀρχιερέως consists of the preposition ἐκ, meaning "from," followed by the genitive plural of δοῦλος, meaning "servant," and the genitive singular of ἀρχιερεύς, meaning "high priest." The phrase thus means "one of the servants of the high priest."

- **συγγενὴς ὢν οὗ ἀπέκοψεν Πέτρος τὸ ὠτίον**: The word συγγενὴς is the masculine nominative singular of συγγενής, meaning "relative" or "kinsman." The participle ὢν is the present active nominative singular of εἰμί, meaning "being." The relative pronoun οὗ is in the genitive singular and refers to the high priest. The verb ἀπέκοψεν is the third person singular aorist indicative active of ἀποκόπτω, meaning "he cut off." The noun τὸ ὠτίον is the accusative singular of ὠτίον, meaning "ear." The phrase thus means "being a relative of the one whose ear Peter cut off."

- **Οὐκ ἐγώ σε εἶδον ἐν τῷ κήπῳ μετʼ αὐτοῦ**: The word Οὐκ is the negative particle meaning "not." The pronoun ἐγώ is the first person singular pronoun meaning "I." The verb εἶδον is the first person singular aorist indicative active of ὁράω, meaning "I saw." The pronoun σε is the accusative singular of σύ, meaning "you." The preposition ἐν means "in" and is followed by the dative singular of the noun τῷ κήπῳ, meaning "the garden." The preposition μετʼ means "with" and is followed by the genitive singular of the pronoun αὐτοῦ, meaning "him." The phrase thus means "I did not see you in the garden with him."

The literal translation of the verse is: "One of the servants of the high priest, being a relative of the one whose ear Peter cut off, said, 'Did I not see you in the garden with him?'"