---
version: 1
---
- **Καὶ**: This is a coordinating conjunction meaning "and."
- **τῇ ἡμέρᾳ τῇ τρίτῃ**: The word τῇ is the dative singular of the definite article ὁ, meaning "the." The noun ἡμέρᾳ is the dative singular of ἡμέρα, meaning "day." The adjective τρίτῃ is the feminine dative singular of τρίτος, meaning "third." The phrase means "on the third day."
- **γάμος**: This is a noun meaning "wedding."
- **ἐγένετο**: This is the third person singular aorist indicative of γίνομαι, meaning "he/she/it happened" or "he/she/it became."
- **ἐν Κανὰ τῆς Γαλιλαίας**: The preposition ἐν means "in." Κανὰ is the genitive singular of Κανά, which is a place name. The genitive τῆς Γαλιλαίας means "of Galilee." The phrase means "in Cana of Galilee."
- **καὶ ἦν ἡ μήτηρ τοῦ Ἰησοῦ ἐκεῖ**: The conjunction καὶ means "and." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he/she/it was." The article ἡ is the nominative singular of ὁ, meaning "the." The noun μήτηρ is the nominative singular of μήτηρ, meaning "mother." The genitive τοῦ Ἰησοῦ means "of Jesus." The adverb ἐκεῖ means "there." The phrase means "and the mother of Jesus was there."

Literal translation: "And on the third day, there was a wedding in Cana of Galilee, and the mother of Jesus was there."