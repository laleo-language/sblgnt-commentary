---
version: 1
---
- **ἐκεῖνος** is the nominative singular masculine form of the pronoun ἐκεῖνος, meaning "that one" or "he."
- **ἦν** is the third person singular imperfect indicative form of the verb εἰμί, meaning "he was."
- **ὁ λύχνος** is the nominative singular masculine form of the noun λύχνος, meaning "lamp."
- **ὁ καιόμενος** is the nominative singular masculine form of the participle καίω, meaning "burning" or "being lit."
- **καὶ** is a coordinating conjunction, meaning "and."
- **φαίνων** is the nominative singular masculine form of the participle φαίνω, meaning "shining" or "giving light."
- **ὑμεῖς** is the nominative plural form of the pronoun σύ, meaning "you."
- **δὲ** is a coordinating conjunction, meaning "but" or "and."
- **ἠθελήσατε** is the second person plural aorist indicative form of the verb θέλω, meaning "you wanted" or "you desired."
- **ἀγαλλιαθῆναι** is the aorist infinitive form of the verb ἀγαλλιάω, meaning "to rejoice" or "to be glad."
- **πρὸς** is a preposition meaning "towards" or "in the presence of."
- **ὥραν** is the accusative singular feminine form of the noun ὥρα, meaning "hour."
- **ἐν** is a preposition meaning "in" or "within."
- **τῷ φωτὶ** is the dative singular neuter form of the noun φῶς, meaning "light."
- **αὐτοῦ** is the genitive singular masculine form of the pronoun αὐτός, meaning "his."

Literal translation: "That one was the burning and shining lamp, but you desired to rejoice for a time in his light."

In this verse, Jesus is described as the "burning and shining lamp," highlighting his role as the source of illumination and guidance. The phrase "you desired to rejoice for a time in his light" contrasts the temporary fascination of the people with Jesus' light to the enduring significance of his ministry.