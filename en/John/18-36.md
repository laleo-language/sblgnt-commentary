---
version: 1
---
- **ἀπεκρίθη Ἰησοῦς**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, meaning "he answered." The noun Ἰησοῦς is the nominative singular of Ἰησοῦς, which means "Jesus." The phrase thus means "Jesus answered."

- **Ἡ βασιλεία ἡ ἐμὴ οὐκ ἔστιν ἐκ τοῦ κόσμου τούτου**: The article ἡ indicates that βασιλεία is a noun in the nominative case, feminine gender, and singular number. The adjective ἡ ἐμὴ agrees with βασιλεία in gender, case, and number. The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "is." The preposition ἐκ takes the genitive case, so τοῦ κόσμου is in the genitive case, masculine gender, and singular number. The adjective τούτου agrees with κόσμου in gender, case, and number. The phrase can be translated as "My kingdom is not from this world."

- **εἰ ἐκ τοῦ κόσμου τούτου ἦν ἡ βασιλεία ἡ ἐμή**: The conjunction εἰ introduces a conditional clause, meaning "if." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "was." The noun ἡ βασιλεία and the adjective ἡ ἐμή are the same as in the previous phrase. The phrase can be translated as "If my kingdom was from this world."

- **οἱ ὑπηρέται οἱ ἐμοὶ ἠγωνίζοντο ἄν**: The article οἱ indicates that ὑπηρέται is a noun in the nominative case, masculine gender, and plural number. The adjective οἱ ἐμοὶ agrees with ὑπηρέται in gender, case, and number. The verb ἠγωνίζοντο is the third person plural imperfect indicative middle of ἀγωνίζομαι, meaning "they were fighting" or "they struggled." The particle ἄν is used with the imperfect tense to indicate a hypothetical or potential action. The phrase can be translated as "My servants would have fought."

- **ἵνα μὴ παραδοθῶ τοῖς Ἰουδαίοις**: The conjunction ἵνα introduces a purpose clause, meaning "so that." The verb παραδοθῶ is the first person singular aorist passive subjunctive of παραδίδωμι, meaning "I may be handed over" or "I may be delivered." The definite article τοῖς indicates that Ἰουδαίοις is a noun in the dative case, masculine gender, and plural number. The phrase can be translated as "So that I may not be handed over to the Jews."

- **νῦν δὲ ἡ βασιλεία ἡ ἐμὴ οὐκ ἔστιν ἐντεῦθεν**: The adverb νῦν means "now." The conjunction δὲ indicates a contrast, meaning "but." The noun ἡ βασιλεία and the adjective ἡ ἐμὴ are the same as in the previous phrases. The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "is." The adverb ἐντεῦθεν means "from here" or "from this place." The phrase can be translated as "But now my kingdom is not from here."

The literal translation of the entire verse is: "Jesus answered, 'My kingdom is not from this world. If my kingdom was from this world, my servants would have fought so that I may not be handed over to the Jews. But now my kingdom is not from here."