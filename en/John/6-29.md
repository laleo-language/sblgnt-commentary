---
version: 1
---
- **ὁ Ἰησοῦς**: The article ὁ is the nominative singular masculine form, indicating that it is referring to a specific person. The word Ἰησοῦς is the nominative singular form of Ἰησοῦς, which means "Jesus." The phrase thus means "Jesus."

- **καὶ εἶπεν αὐτοῖς**: The conjunction καὶ means "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτοῖς is the dative plural form of αὐτός, meaning "to them." The phrase thus means "and he said to them."

- **Τοῦτό ἐστιν τὸ ἔργον τοῦ θεοῦ**: The pronoun Τοῦτό is the nominative singular neuter form of οὗτος, meaning "this." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The noun τὸ ἔργον is the accusative singular form of ἔργον, meaning "work." The noun τοῦ θεοῦ is the genitive singular form of θεός, meaning "of God." The phrase thus means "This is the work of God."

- **ἵνα πιστεύητε εἰς ὃν ἀπέστειλεν ἐκεῖνος**: The conjunction ἵνα introduces a purpose clause. The verb πιστεύητε is the second person plural present subjunctive of πιστεύω, meaning "you believe." The preposition εἰς means "into" or "to." The pronoun ὃν is the accusative singular masculine form of ὅς, meaning "whom." The verb ἀπέστειλεν is the third person singular aorist indicative of ἀποστέλλω, meaning "he sent." The pronoun ἐκεῖνος is the nominative singular masculine form of ἐκεῖνος, meaning "he." The phrase thus means "that you believe in him whom he sent."

The entire verse can be literally translated as: "Jesus answered and said to them, 'This is the work of God, that you believe in him whom he sent.'"