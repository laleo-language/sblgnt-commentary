---
version: 1
---
- **ταῦτα λελάληκα ὑμῖν**: The word ταῦτα is the accusative plural of οὗτος, which means "these" or "these things." The verb λελάληκα is the first person singular perfect indicative of λαλέω, meaning "I have spoken" or "I have said." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The phrase thus means "I have spoken these things to you."

- **ἵνα ἡ χαρὰ ἡ ἐμὴ ἐν ὑμῖν ⸀ᾖ**: The word ἵνα is a subordinating conjunction that introduces a purpose clause, meaning "in order that" or "so that." The article ἡ with the noun χαρὰ indicates that it is a specific joy, and ἡ ἐμὴ means "my." The preposition ἐν means "in" or "among." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The verb ⸀ᾖ is the third person singular present subjunctive of εἰμί, meaning "may be" or "might be." The phrase can be translated as "so that my joy may be in you."

- **καὶ ἡ χαρὰ ὑμῶν πληρωθῇ**: The conjunction καὶ means "and." The article ἡ with the noun χαρὰ indicates that it is a specific joy. The pronoun ὑμῶν is the genitive plural of σύ, meaning "of you" or "your." The verb πληρωθῇ is the third person singular aorist passive subjunctive of πληρόω, meaning "may be filled" or "might be fulfilled." The phrase can be translated as "and your joy may be fulfilled."

The entire verse can be translated as: "I have spoken these things to you so that my joy may be in you and your joy may be fulfilled."