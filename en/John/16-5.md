---
version: 1
---
- **νῦν δὲ**: The word νῦν is an adverb that means "now." The word δὲ is a coordinating conjunction that means "but" or "and." Together, the phrase means "but now."

- **ὑπάγω**: The verb ὑπάγω is the first person singular present indicative active of ὑπάγω, which means "I go" or "I am going." 

- **πρὸς τὸν πέμψαντά με**: The word πρὸς is a preposition that means "to" or "towards." The article τὸν is the accusative singular masculine definite article, indicating that πέμψαντά is a direct object. The verb πέμψαντά is the aorist active participle of πέμπω, which means "to send." The pronoun με is the accusative singular form of ἐγώ, meaning "me." Together, the phrase means "to the one who sent me."

- **καὶ οὐδεὶς ἐξ ὑμῶν ἐρωτᾷ με**: The word καὶ is a coordinating conjunction that means "and." The word οὐδεὶς is a pronoun that means "no one" or "nobody." The word ἐξ is a preposition that means "from" or "out of." The pronoun ὑμῶν is the genitive plural form of σύ, meaning "you." The verb ἐρωτᾷ is the third person singular present indicative active of ἐρωτάω, which means "he/she/it asks" or "he/she/it is asking." The pronoun με is the accusative singular form of ἐγώ, meaning "me." Together, the phrase means "and no one from among you asks me."

- **Ποῦ ὑπάγεις**: The word Ποῦ is an adverb that means "where." The verb ὑπάγεις is the second person singular present indicative active of ὑπάγω, which means "you go" or "you are going." Together, the phrase means "where are you going?"

The literal translation of the verse is: "But now I am going to the one who sent me, and no one from among you asks me, 'Where are you going?'"