---
version: 1
---
- **ἐζήτουν οὖν πάλιν αὐτὸν πιάσαι**: The verb ἐζήτουν is the third person plural imperfect indicative of ζητέω, meaning "they were seeking" or "they were looking for." The particle οὖν is a conjunction that can be translated as "therefore" or "so." The word πάλιν is an adverb meaning "again" or "once more." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." The verb πιάσαι is the aorist infinitive of πιάζω, meaning "to seize" or "to arrest." The phrase thus means "So they were seeking to seize him again."

- **καὶ ἐξῆλθεν ἐκ τῆς χειρὸς αὐτῶν**: The conjunction καὶ means "and." The verb ἐξῆλθεν is the third person singular aorist indicative of ἐξέρχομαι, meaning "he went out" or "he left." The preposition ἐκ indicates motion from a place and can be translated as "from." The article τῆς is the genitive singular feminine definite article, indicating that the noun it modifies is feminine. The noun χειρὸς is the genitive singular of χείρ, meaning "hand." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "And he went out from their hand."

The literal translation of John 10:39 is: "So they were seeking to seize him again, and he went out from their hand."