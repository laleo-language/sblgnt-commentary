---
version: 1
---
- **δόξαν**: This is the accusative singular of δόξα, which means "glory" or "honor."
- **παρὰ**: This is a preposition that means "from" or "beside."
- **ἀνθρώπων**: This is the genitive plural of ἄνθρωπος, which means "man" or "human."
- **οὐ**: This is a negation particle meaning "not."
- **λαμβάνω**: This is the first person singular present indicative of λαμβάνω, which means "I receive" or "I take."

Putting it all together, the phrase can be translated as "I do not receive glory from men."

The entire verse, with a literal translation, is: "I do not receive glory from men." This verse is part of a larger passage in which Jesus is speaking to the Jews, confronting their unbelief and their reliance on their own human traditions rather than recognizing the truth and authority of Jesus as the Son of God.