---
version: 1
---
- **ἐληλακότες**: This is the participle form of the verb ἔρχομαι, which means "to go" or "to come." The participle ἐληλακότες is in the nominative plural masculine form, and it means "having gone" or "having come." It describes the subject of the sentence.
- **οὖν**: This is an adverb that is often translated as "therefore" or "so." It is used to indicate a logical consequence or inference from what has been stated before.
- **ὡς**: This is a conjunction that can be translated as "as" or "like." It is used here to introduce a comparison.
- **σταδίους εἴκοσι πέντε ἢ τριάκοντα**: This is a noun phrase that consists of the noun σταδίους (nominative plural of στάδιον, meaning "stadion" or a unit of distance), followed by the numbers εἴκοσι πέντε (twenty-five) and τριάκοντα (thirty). So the phrase means "twenty-five or thirty stadia."
- **θεωροῦσιν**: This is the present indicative active form of the verb θεωρέω, which means "to look at" or "to observe." It is in the third person plural, indicating that the subject is a group of people.
- **τὸν Ἰησοῦν περιπατοῦντα**: This is a noun phrase that consists of the article τὸν (accusative singular masculine form of ὁ, meaning "the"), followed by the name Ἰησοῦν (accusative singular of Ἰησοῦς, meaning "Jesus"), and the participle περιπατοῦντα (accusative singular masculine form of περιπατέω, meaning "walking"). So the phrase means "they see Jesus walking."
- **ἐπὶ τῆς θαλάσσης**: This is a prepositional phrase that consists of the preposition ἐπὶ (meaning "on" or "upon") and the noun θαλάσσης (genitive singular of θάλασσα, meaning "sea"). So the phrase means "on the sea."
- **καὶ ἐγγὺς τοῦ πλοίου γινόμενον**: This is a conjunction καὶ (meaning "and"), followed by the adverb ἐγγὺς (meaning "near" or "close"), the article τοῦ (genitive singular masculine form of ὁ, meaning "the"), the noun πλοίου (genitive singular of πλοῖον, meaning "boat" or "ship"), and the present participle γινόμενον (accusative singular masculine form of γίνομαι, meaning "being"). So the phrase means "and being near the boat."

The sentence can be translated as: "So when they had rowed about twenty-five or thirty stadia, they saw Jesus walking on the sea and coming near the boat, and they were afraid."