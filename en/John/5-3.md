---
version: 1
---
- **ἐν ταύταις κατέκειτο**: The preposition ἐν means "in" and is followed by the feminine plural dative ταύταις, which means "these." The verb κατέκειτο is the third person singular imperfect indicative of κατάκειμαι, meaning "he was lying down" or "he was reclining." The phrase thus means "he was lying down in these."

- **πλῆθος τῶν ἀσθενούντων, τυφλῶν, χωλῶν, ξηρῶν**: The noun πλῆθος means "a crowd" or "a multitude." The genitive τῶν ἀσθενούντων, τυφλῶν, χωλῶν, ξηρῶν describes the crowd and consists of four participles: ἀσθενούντων (from ἀσθενέω, "to be sick"), τυφλῶν (from τυφλός, "blind"), χωλῶν (from χωλός, "lame"), and ξηρῶν (from ξηρός, "withered" or "dried up"). The phrase thus means "a crowd of the sick, blind, lame, and withered."

The literal translation of John 5:3 is: "He was lying down in these. A crowd of the sick, blind, lame, and withered."