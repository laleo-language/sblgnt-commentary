---
version: 1
---
- **διὰ τοῦτο**: The phrase διὰ τοῦτο consists of the preposition διὰ, meaning "through" or "because of," and the demonstrative pronoun τοῦτο, meaning "this." Together, the phrase means "because of this" or "for this reason."

- **καὶ ὑπήντησεν αὐτῷ ὁ ὄχλος**: The conjunction καὶ means "and." The verb ὑπήντησεν is the third person singular aorist indicative of ὑπαντάω, meaning "he met" or "he encountered." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him" or "for him." The definite article ὁ indicates that ὄχλος is the subject of the verb. The noun ὄχλος means "crowd" or "multitude." The phrase can be translated as "and the crowd met him."

- **ὅτι ἤκουσαν τοῦτο αὐτὸν πεποιηκέναι τὸ σημεῖον**: The conjunction ὅτι can introduce a clause and means "that." The verb ἤκουσαν is the third person plural aorist indicative of ἀκούω, meaning "they heard." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." The verb πεποιηκέναι is the perfect infinitive of ποιέω, meaning "to do" or "to make." The definite article τὸ indicates that σημεῖον is the direct object of the verb. The noun σημεῖον means "sign" or "miracle." The phrase can be translated as "because they heard that he had done the sign."

The literal translation of the entire verse is: "Because of this, the crowd also met him, because they heard that he had done this sign."