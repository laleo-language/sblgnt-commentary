---
version: 1
---
- **εἶπον οὖν ὑμῖν**: The word εἶπον is the first person singular aorist indicative active of λέγω, meaning "I said." The pronoun οὖν means "therefore" or "so." The pronoun ὑμῖν is the second person plural dative of σύ, meaning "to you." The phrase thus means "So I said to you."

- **ὅτι ἀποθανεῖσθε ἐν ταῖς ἁμαρτίαις ὑμῶν**: The conjunction ὅτι introduces a clause that functions as the direct object of the verb εἶπον. The verb ἀποθανεῖσθε is the second person plural future passive indicative of ἀποθνῄσκω, meaning "you will die." The preposition ἐν means "in." The article ταῖς is the plural feminine dative of ὁ, which means "the." The noun ἁμαρτίαις is the plural dative of ἁμαρτία, meaning "sins." The pronoun ὑμῶν is the second person plural genitive of σύ, meaning "your." The phrase thus means "that you will die in your sins."

- **ἐὰν γὰρ μὴ πιστεύσητε ὅτι ἐγώ εἰμι**: The conjunction ἐὰν introduces a conditional clause. The verb πιστεύσητε is the second person plural aorist active subjunctive of πιστεύω, meaning "you believe." The pronoun ὅτι introduces a clause that functions as the direct object of πιστεύσητε. The pronoun ἐγώ means "I" and the verb εἰμι is the first person singular present indicative of εἰμί, meaning "am." The phrase thus means "if you do not believe that I am."

- **ἀποθανεῖσθε ἐν ταῖς ἁμαρτίαις ὑμῶν**: Same as the previous phrase explained above.

The literal translation of John 8:24 is: "So I said to you that you will die in your sins; if you do not believe that I am, you will die in your sins."