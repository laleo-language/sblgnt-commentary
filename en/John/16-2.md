---
version: 1
---
- **ἀποσυναγώγους ποιήσουσιν ὑμᾶς**: The word ἀποσυναγώγους is the accusative
  plural of ἀποσυνάγωγος, which means "expellers from the synagogue." The verb
  ποιήσουσιν is the third person plural future indicative of ποιέω, meaning "they
  will make." The pronoun ὑμᾶς is the second person plural accusative pronoun,
  meaning "you." The phrase thus means "they will make you expellers from the
  synagogue."

- **ἀλλʼ ἔρχεται ὥρα**: The word ἀλλʼ is a conjunction that means "but." The verb
  ἔρχεται is the third person singular present indicative of ἔρχομαι, meaning
  "he/she/it is coming." The noun ὥρα is in the nominative singular, which means
  "hour" or "time." The phrase thus means "but an hour is coming."

- **ἵνα πᾶς ὁ ἀποκτείνας ὑμᾶς δόξῃ λατρείαν προσφέρειν τῷ θεῷ**: The word ἵνα is a
  conjunction that means "so that" or "in order that." The pronoun πᾶς is in the
  nominative singular, meaning "everyone" or "all." The article ὁ is the definite
  article, meaning "the." The verb ἀποκτείνας is the nominative singular
  participle of ἀποκτείνω, meaning "the one who kills." The pronoun ὑμᾶς is the
  second person plural accusative pronoun, meaning "you." The verb δόξῃ is the
  third person singular aorist subjunctive of δοξάζω, meaning "he/she/it may
  glorify." The noun λατρείαν is the accusative singular of λατρεία, meaning
  "worship" or "service." The verb προσφέρειν is the present infinitive of
  προσφέρω, meaning "to offer." The article τῷ is the definite article, meaning
  "to the." The noun θεῷ is in the dative singular, meaning "God." The phrase
  thus means "so that everyone who kills you may think he is offering worship to
  God."

The entire verse, John 16:2, can be literally translated as: "They will make you expellers from the synagogue, but an hour is coming so that everyone who kills you may think he is offering worship to God."