---
version: 1
---
- **καὶ ἡμεῖς πεπιστεύκαμεν**: The word καὶ is a conjunction meaning "and." The pronoun ἡμεῖς is the first person plural of ἐγώ, meaning "we." The verb πεπιστεύκαμεν is the first person plural perfect indicative of πιστεύω, meaning "we have believed." The phrase thus means "and we have believed."

- **καὶ ἐγνώκαμεν**: The word καὶ is a conjunction meaning "and." The pronoun ἡμεῖς is the first person plural of ἐγώ, meaning "we." The verb ἐγνώκαμεν is the first person plural perfect indicative of γινώσκω, meaning "we have known." The phrase thus means "and we have known."

- **ὅτι σὺ εἶ ὁ ἅγιος τοῦ θεοῦ**: The conjunction ὅτι introduces indirect speech and can be translated as "that." The pronoun σὺ is the second person singular of σύ, meaning "you." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The article ὁ is the nominative singular masculine definite article, meaning "the." The adjective ἅγιος is the nominative singular masculine of ἅγιος, meaning "holy." The genitive τοῦ is the genitive singular masculine definite article, meaning "of the." The noun θεοῦ is the genitive singular masculine of θεός, meaning "God." The phrase thus means "that you are the holy one of God."

The literal translation of John 6:69 is: "And we have believed and we have known that you are the holy one of God."