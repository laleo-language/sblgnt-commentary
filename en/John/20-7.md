---
version: 1
---
- **καὶ τὸ σουδάριον**: The word σουδάριον is the accusative singular of σουδάριον, which means "sweat cloth" or "linen cloth." The article τὸ indicates that it is a specific cloth being referred to. The phrase thus means "and the cloth."

- **ὃ ἦν ἐπὶ τῆς κεφαλῆς αὐτοῦ**: The relative pronoun ὃ refers back to the cloth mentioned earlier. The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was." The preposition ἐπὶ means "on" or "upon," and the noun τῆς κεφαλῆς means "the head." The pronoun αὐτοῦ means "his." The phrase thus means "which was on his head."

- **οὐ μετὰ τῶν ὀθονίων κείμενον**: The negation οὐ means "not." The preposition μετὰ means "with." The article τῶν is the genitive plural of ὁ, indicating possession. The noun ὀθονίων is the genitive plural of ὀθόνιον, meaning "linen wrappings" or "burial cloths." The participle κείμενον is the present passive accusative singular masculine of κεῖμαι, meaning "lying" or "placed." The phrase thus means "not with the burial cloths lying."

- **ἀλλὰ χωρὶς ἐντετυλιγμένον εἰς ἕνα τόπον**: The conjunction ἀλλὰ means "but." The adverb χωρὶς means "apart from" or "separate from." The participle ἐντετυλιγμένον is the perfect passive accusative singular neuter of ἐντυλίσσω, meaning "wrapped" or "folded." The preposition εἰς means "into" or "to." The numeral ἕνα means "one." The noun τόπον means "place." The phrase thus means "but folded up in a separate place."

- **καὶ τὸ σουδάριον, ὃ ἦν ἐπὶ τῆς κεφαλῆς αὐτοῦ, οὐ μετὰ τῶν ὀθονίων κείμενον ἀλλὰ χωρὶς ἐντετυλιγμένον εἰς ἕνα τόπον**: And the cloth, which was on his head, not lying with the burial cloths, but folded up in a separate place.

The verse describes the scene after Jesus' resurrection, where the cloth that had been on his head was found separate from the burial cloths, folded up in a different place.