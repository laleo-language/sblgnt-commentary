---
version: 1
---
- **ὡς δὲ ἐνεπλήσθησαν**: The word ὡς is a conjunction meaning "when" or "as." The verb ἐνεπλήσθησαν is the third person plural aorist passive indicative of ἐμπίμπλημι, which means "to fill." The phrase thus means "when they were filled."

- **λέγει τοῖς μαθηταῖς αὐτοῦ**: The verb λέγει is the third person singular present active indicative of λέγω, meaning "he says." The noun μαθηταῖς is the dative plural of μαθητής, which means "disciples" or "students." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "he says to his disciples."

- **Συναγάγετε τὰ περισσεύσαντα κλάσματα**: The verb Συναγάγετε is the second person plural aorist active imperative of συνάγω, which means "gather" or "collect." The article τὰ is the accusative plural of ὁ, which is the definite article "the." The participle περισσεύσαντα is the accusative plural neuter participle of περισσεύω, which means "to be left over" or "to abound." The noun κλάσματα is the accusative plural of κλάσμα, which means "fragments" or "broken pieces." The phrase thus means "gather the fragments that are left over."

- **ἵνα μή τι ἀπόληται**: The conjunction ἵνα introduces a purpose clause and means "so that" or "in order that." The negation μή is used with the subjunctive verb to express a negative purpose and means "lest" or "so that...not." The pronoun τι is the accusative singular of τις, meaning "some" or "any." The verb ἀπόληται is the third person singular aorist middle subjunctive of ἀπόλλυμι, which means "to be destroyed" or "to be lost." The phrase thus means "so that nothing may be lost."

Literal translation: "When they were filled, he says to his disciples, 'Gather the fragments that are left over, so that nothing may be lost.'"