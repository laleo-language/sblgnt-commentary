---
version: 1
---
- **λέγει αὐτῷ Θωμᾶς**: The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The name Θωμᾶς is the nominative singular of Θωμᾶς, which is the Greek form of "Thomas." The phrase thus means "Thomas says to him."

- **Κύριε**: The word Κύριε is the vocative singular of Κύριος, meaning "Lord." This is a direct address to Jesus.

- **οὐκ οἴδαμεν**: The verb οἴδαμεν is the first person plural present indicative of οἶδα, meaning "we know" or "we understand." The negation οὐκ is added before the verb to indicate "we do not know."

- **ποῦ ὑπάγεις**: The word ποῦ is an adverb meaning "where." The verb ὑπάγεις is the second person singular present indicative of ὑπάγω, meaning "you are going." The phrase thus means "where are you going."

- **πῶς δυνάμεθα τὴν ὁδὸν εἰδέναι**: The word πῶς is an adverb meaning "how." The verb δυνάμεθα is the first person plural present indicative of δύναμαι, meaning "we are able" or "we can." The article τὴν is the accusative singular feminine of ὁ, meaning "the." The noun ὁδὸν is the accusative singular of ὁδός, meaning "way" or "road." The verb εἰδέναι is the present infinitive of οἶδα, meaning "to know." The phrase thus means "how can we know the way."

- ⸀πῶς ⸂δυνάμεθα τὴν ὁδὸν εἰδέναι⸃;: This phrase repeats the previous phrase "πῶς δυνάμεθα τὴν ὁδὸν εἰδέναι" for emphasis, with the addition of quotation marks (⸂ and ⸃) to indicate Thomas' words. The phrase is asking a question.

Literal translation: Thomas says to him, "Lord, we do not know where you are going. How can we know the way?"

In this verse, Thomas is expressing his confusion and lack of understanding about where Jesus is going and how they can know the way.