---
version: 1
---
- **ἀπεκρίθη Ἰησοῦς**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, meaning "he answered." The noun Ἰησοῦς is the nominative singular of Ἰησοῦς, which means "Jesus." The phrase thus means "Jesus answered."

- **καὶ εἶπεν αὐτοῖς**: The conjunction καὶ means "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτοῖς is the dative plural of αὐτός, which means "to them." The phrase thus means "and he said to them."

- **Ἓν ἔργον ἐποίησα**: The numeral adjective Ἓν means "one." The noun ἔργον is the accusative singular of ἔργον, which means "work." The verb ἐποίησα is the first person singular aorist indicative of ποιέω, meaning "I did" or "I made." The phrase thus means "I did one work."

- **καὶ πάντες θαυμάζετε**: The conjunction καὶ means "and." The noun πάντες is the nominative plural of πᾶς, which means "all." The verb θαυμάζετε is the second person plural present indicative of θαυμάζω, meaning "you marvel" or "you wonder." The phrase thus means "and all of you marvel" or "and all of you wonder."

Literal translation: "Jesus answered and said to them, 'I did one work and all of you marvel.'"