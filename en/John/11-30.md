---
version: 1
---
- **οὔπω δὲ**: The word οὔπω means "not yet" or "still not." The word δὲ is a conjunction that can be translated as "but" or "and." The phrase οὔπω δὲ means "but not yet."

- **ἐληλύθει ὁ Ἰησοῦς εἰς τὴν κώμην**: The verb ἐληλύθει is the imperfect indicative of ἔρχομαι, meaning "he was coming" or "he had come." The subject of the verb is ὁ Ἰησοῦς, which means "Jesus." The preposition εἰς means "into" or "to." The noun τὴν κώμην means "the village." The phrase ἐληλύθει ὁ Ἰησοῦς εἰς τὴν κώμην means "Jesus was coming into the village."

- **ἀλλʼ ἦν ἔτι ἐν τῷ τόπῳ**: The word ἀλλʼ is a conjunction that can be translated as "but" or "however." The verb ἦν is the imperfect indicative of εἰμί, meaning "he was." The adverb ἔτι means "still" or "yet." The preposition ἐν means "in." The noun τῷ τόπῳ means "the place." The phrase ἀλλʼ ἦν ἔτι ἐν τῷ τόπῳ means "but he was still in the place."

- **ὅπου ὑπήντησεν αὐτῷ ἡ Μάρθα**: The relative pronoun ὅπου means "where." The verb ὑπήντησεν is the aorist indicative of ὑπαντάω, meaning "she met" or "she encountered." The pronoun αὐτῷ means "him." The article ἡ is the feminine singular nominative form of ὁ, meaning "the." The proper noun Μάρθα refers to the character Martha. The phrase ὅπου ὑπήντησεν αὐτῷ ἡ Μάρθα means "where Martha met him."

- The literal translation of John 11:30 is: "But Jesus had not yet come into the village, but he was still in the place where Martha met him."