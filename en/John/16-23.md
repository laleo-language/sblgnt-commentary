---
version: 1
---
- **καὶ ἐν ἐκείνῃ τῇ ἡμέρᾳ**: The conjunction καὶ means "and." The preposition ἐν means "in." The demonstrative pronoun ἐκείνῃ is the dative singular feminine form of ἐκεῖνος, meaning "that" or "those." The article τῇ is the dative singular feminine form of ὁ, meaning "the." The noun ἡμέρᾳ is the dative singular feminine form of ἡμέρα, meaning "day." The phrase thus means "and on that day."

- **ἐμὲ οὐκ ἐρωτήσετε οὐδέν**: The pronoun ἐμὲ is the accusative singular form of ἐγώ, meaning "me." The adverb οὐκ means "not." The verb ἐρωτήσετε is the second person plural future indicative form of ἐρωτάω, meaning "you will ask." The pronoun οὐδέν is the accusative singular neuter form of οὐδείς, meaning "nothing." The phrase thus means "you will not ask me anything."

- **ἀμὴν ἀμὴν λέγω ὑμῖν**: The word ἀμὴν is a transliteration of the Hebrew word אָמֵן, which means "truly" or "amen." The verb λέγω is the first person singular present indicative form of λέγω, meaning "I say." The pronoun ὑμῖν is the dative plural form of σύ, meaning "you." The phrase is an emphatic way of saying "truly, truly I say to you."

- **ἄν τι αἰτήσητε τὸν πατέρα**: The conjunction ἄν is a particle that indicates potentiality or contingency. The pronoun τι is an indefinite pronoun meaning "something." The verb αἰτήσητε is the second person plural aorist subjunctive form of αἰτέω, meaning "you ask." The article τὸν is the accusative singular masculine form of ὁ, meaning "the." The noun πατέρα is the accusative singular masculine form of πατήρ, meaning "father." The phrase thus means "if you ask something the father."

- **δώσει ὑμῖν ἐν τῷ ὀνόματί μου**: The verb δώσει is the third person singular future indicative form of δίδωμι, meaning "he will give." The pronoun ὑμῖν is the dative plural form of σύ, meaning "to you." The preposition ἐν means "in." The article τῷ is the dative singular masculine form of ὁ, meaning "the." The noun ὀνόματί is the dative singular neuter form of ὄνομα, meaning "name." The pronoun μου is the genitive singular form of ἐγώ, meaning "my." The phrase thus means "he will give to you in my name."

- **καὶ ἐν ἐκείνῃ τῇ ἡμέρᾳ ἐμὲ οὐκ ἐρωτήσετε οὐδέν, ἀμὴν ἀμὴν λέγω ὑμῖν, ἄν τι αἰτήσητε τὸν πατέρα δώσει ὑμῖν ἐν τῷ ὀνόματί μου**: And on that day you will not ask me anything, truly, truly I say to you, if you ask something the father will give to you in my name.

The phrase in this verse is relatively straightforward. It speaks of a future time when the disciples will not need to ask Jesus anything because they will have direct access to the Father. Jesus assures them that if they ask the Father for something in his name, the Father will give it to them.