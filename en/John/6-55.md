---
version: 1
---
- **ἡ γὰρ σάρξ μου**: The article ἡ is the nominative singular feminine form of ὁ, which means "the." The noun σάρξ is the nominative singular form of σάρξ, which means "flesh." The pronoun μου is the genitive singular form of ἐγώ, meaning "my" or "of me." The phrase thus means "the flesh of mine."

- **ἀληθής ἐστι βρῶσις**: The adjective ἀληθής is the nominative singular form of ἀληθής, which means "true." The verb ἐστι is the third person singular present indicative form of εἰμί, meaning "is." The noun βρῶσις is the nominative singular form of βρῶσις, which means "food." The phrase thus means "the true food is."

- **καὶ τὸ αἷμά μου**: The conjunction καὶ means "and." The article τὸ is the accusative singular neuter form of ὁ, which means "the." The noun αἷμα is the accusative singular form of αἷμα, which means "blood." The pronoun μου is the genitive singular form of ἐγώ, meaning "my" or "of me." The phrase thus means "and the blood of mine."

- **ἀληθής ἐστι πόσις**: The adjective ἀληθής is the nominative singular form of ἀληθής, which means "true." The verb ἐστι is the third person singular present indicative form of εἰμί, meaning "is." The noun πόσις is the nominative singular form of πόσις, which means "drink." The phrase thus means "the true drink is."

The entire verse is translated as: "For my flesh is true food, and my blood is true drink."