---
version: 1
---
- **ταῦτα εἰποῦσα**: The word ταῦτα is the accusative neuter plural of οὗτος, meaning "these" or "this." The participle εἰποῦσα is the nominative feminine singular of λέγω, meaning "saying" or "having said." The phrase thus means "having said these things."

- **ἐστράφη εἰς τὰ ὀπίσω**: The verb ἐστράφη is the third person singular aorist passive indicative of στρέφω, meaning "she turned." The preposition εἰς means "towards" or "to," and the article τὰ is the accusative neuter plural of ὁ, meaning "the." The noun ὀπίσω means "behind" or "back." The phrase thus means "she turned towards the back."

- **καὶ θεωρεῖ τὸν Ἰησοῦν ἑστῶτα**: The conjunction καὶ means "and." The verb θεωρεῖ is the third person singular present active indicative of θεωρέω, meaning "she sees" or "she looks." The article τὸν is the accusative masculine singular of ὁ, meaning "the." The name Ἰησοῦν is the accusative masculine singular of Ἰησοῦς, meaning "Jesus." The participle ἑστῶτα is the accusative masculine singular of ἵστημι, meaning "standing." The phrase thus means "and she sees Jesus standing."

- **καὶ οὐκ ᾔδει ὅτι Ἰησοῦς ἐστιν**: The conjunction καὶ means "and." The adverb οὐκ means "not." The verb ᾔδει is the third person singular pluperfect indicative of οἶδα, meaning "she knew" or "she understood." The conjunction ὅτι means "that." The name Ἰησοῦς is the subject of the verb. The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The phrase thus means "and she did not know that it is Jesus."

The literal translation of John 20:14 is: "Having said these things, she turned towards the back and she sees Jesus standing, and she did not know that it is Jesus."