---
version: 1
---
- **εἷς δέ τις**: The word εἷς is the nominative singular of εἷς, which means "one." The word δέ is a conjunction meaning "but" or "and." The word τις is the nominative singular of τις, which means "someone" or "a certain one." The phrase εἷς δέ τις means "but someone" or "and a certain one."

- **ἐξ αὐτῶν**: The word ἐξ is a preposition meaning "out of" or "from." The word αὐτῶν is the genitive plural of αὐτός, which means "they" or "them." The phrase ἐξ αὐτῶν means "out of them" or "from them."

- **Καϊάφας**: The word Καϊάφας is the nominative singular of Καϊάφας, which is a proper noun referring to Caiaphas, the high priest.

- **ἀρχιερεὺς**: The word ἀρχιερεὺς is the nominative singular of ἀρχιερεύς, which means "high priest."

- **ὢν τοῦ ἐνιαυτοῦ ἐκείνου**: The word ὢν is the present participle of εἰμί, which means "being" or "is." The word τοῦ is the genitive singular of ὁ, which means "the." The word ἐνιαυτοῦ is the genitive singular of ἐνιαύτος, which means "year." The word ἐκείνου is the genitive singular of ἐκεῖνος, which means "that" or "that one." The phrase ὢν τοῦ ἐνιαυτοῦ ἐκείνου means "being of that year" or "in that year."

- **εἶπεν αὐτοῖς**: The word εἶπεν is the third person singular aorist indicative of λέγω, which means "he said" or "he spoke." The word αὐτοῖς is the dative plural of αὐτός, which means "to them." The phrase εἶπεν αὐτοῖς means "he said to them."

- **Ὑμεῖς οὐκ οἴδατε οὐδέν**: The word Ὑμεῖς is the nominative plural of σύ, which means "you." The word οὐκ is the negative particle meaning "not." The word οἴδατε is the second person plural present indicative of οἶδα, which means "you know." The word οὐδέν is the accusative singular of οὐδείς, which means "nothing." The phrase Ὑμεῖς οὐκ οἴδατε οὐδέν means "you do not know anything."

The sentence "εἷς δέ τις ἐξ αὐτῶν Καϊάφας, ἀρχιερεὺς ὢν τοῦ ἐνιαυτοῦ ἐκείνου, εἶπεν αὐτοῖς· Ὑμεῖς οὐκ οἴδατε οὐδέν" can be translated as "But someone out of them, Caiaphas, being the high priest of that year, said to them, 'You do not know anything.'"

In this verse, Caiaphas, who was the high priest of that year, speaks to a group of people and tells them that they do not know anything.