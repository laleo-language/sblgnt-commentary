---
version: 1
---
- **λέγει αὐτῇ Ἰησοῦς**: The verb λέγει is the third person singular present indicative active of λέγω, meaning "he/she/it says." The pronoun αὐτῇ is the dative singular of αὐτός, meaning "to her." And Ἰησοῦς is the nominative singular of Ἰησοῦς, which means "Jesus." The phrase thus means "Jesus says to her."

- **Μαριάμ**: Μαριάμ is the nominative singular of Μαρία, which means "Mary." This is a direct address to Mary.

- **στραφεῖσα ἐκείνη**: The verb στραφεῖσα is the nominative singular feminine aorist passive participle of στρέφω, meaning "having turned around." The pronoun ἐκείνη is the nominative singular feminine pronoun meaning "she." The phrase thus means "she, having turned around."

- **λέγει αὐτῷ Ἑβραϊστί**: The verb λέγει is the third person singular present indicative active of λέγω, meaning "he/she/it says." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." And Ἑβραϊστί is an adverb derived from Ἑβραϊκός, meaning "in Hebrew." The phrase thus means "she says to him in Hebrew."

- **Ραββουνι (ὃ λέγεται Διδάσκαλε)**: Ραββουνι is a transliteration of the Hebrew word רַבּוֹנִי (rabbōnī), which means "my teacher." The phrase ὃ λέγεται is a relative clause that means "which is called." So the phrase Ραββουνι (ὃ λέγεται Διδάσκαλε) means "Rabbouni (which is called Teacher)."

Literal translation: Jesus says to her, "Mary." She, having turned around, says to him in Hebrew, "Rabbouni (which is called Teacher)."