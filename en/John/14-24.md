---
version: 1
---
- **ὁ μὴ ἀγαπῶν με**: The article ὁ is the masculine singular nominative form, which means "the." The negation μὴ negates the verb that follows. The verb ἀγαπῶν is the present active participle of ἀγαπάω, which means "loving." The pronoun με is the accusative singular form of ἐγώ, which means "me." So this phrase means "the one who does not love me."

- **τοὺς λόγους μου**: The article τοὺς is the masculine plural accusative form, which means "the." The noun λόγους is the plural accusative form of λόγος, which means "words." The possessive pronoun μου is the genitive singular form of ἐγώ, which means "my." So this phrase means "my words."

- **οὐ τηρεῖ**: The negation οὐ negates the verb that follows. The verb τηρεῖ is the third person singular present indicative form of τηρέω, which means "he keeps" or "he observes." So this phrase means "he does not keep" or "he does not observe."

- **καὶ ὁ λόγος ὃν ἀκούετε**: The conjunction καὶ means "and." The definite article ὁ is the masculine singular nominative form, which means "the." The noun λόγος is the singular nominative form, which means "word." The relative pronoun ὃν is the accusative singular form, which means "which." The verb ἀκούετε is the second person plural present indicative form of ἀκούω, which means "you hear." So this phrase means "and the word which you hear."

- **οὐκ ἔστιν ἐμὸς**: The negation οὐκ negates the verb that follows. The verb ἔστιν is the third person singular present indicative form of εἰμί, which means "is." The possessive pronoun ἐμὸς is the nominative singular form of ἐγώ, which means "my." So this phrase means "it is not mine."

- **ἀλλὰ τοῦ πέμψαντός με πατρός**: The conjunction ἀλλὰ means "but." The definite article τοῦ is the masculine singular genitive form, which means "the." The verb πέμψαντός is the aorist active participle of πέμπω, which means "sending." The pronoun με is the accusative singular form of ἐγώ, which means "me." The noun πατρός is the genitive singular form of πατήρ, which means "father." So this phrase means "but of the one who sent me, the Father."

The literal translation of John 14:24 is: "The one who does not love me does not keep my words. And the word which you hear is not mine, but of the one who sent me, the Father."

In this verse, Jesus is speaking to his disciples and explaining that those who do not love him will not keep or obey his words. He emphasizes that the words he speaks are not his own, but come from the Father who sent him. This passage highlights the importance of loving and obeying Jesus' teachings as a sign of true discipleship.