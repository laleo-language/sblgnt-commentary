---
version: 1
---
- **λέγει αὐτοῖς Σίμων Πέτρος**: The verb λέγει is the third person singular present indicative of λέγω, meaning "he says" or "he is saying." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The name Σίμων is the nominative form of Simon, and Πέτρος is the nominative form of Peter. The phrase thus means "Simon Peter says to them."

- **Ὑπάγω ἁλιεύειν**: The verb Ὑπάγω is the first person singular present indicative of ὑπάγω, meaning "I am going" or "I go." The infinitive ἁλιεύειν is the present infinitive of ἁλιεύω, meaning "to fish." The phrase thus means "I am going to fish."

- **λέγουσιν αὐτῷ**: The verb λέγουσιν is the third person plural present indicative of λέγω, meaning "they say" or "they are saying." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "they say to him."

- **Ἐρχόμεθα καὶ ἡμεῖς σὺν σοί**: The verb Ἐρχόμεθα is the first person plural present indicative of ἔρχομαι, meaning "we come" or "we are coming." The pronoun ἡμεῖς is the nominative plural form of ἐγώ, meaning "we." The preposition σὺν means "with" and the pronoun σοί is the dative singular form of σύ, meaning "you." The phrase thus means "we are coming with you."

- **ἐξῆλθον καὶ ἐνέβησαν εἰς τὸ πλοῖον**: The verb ἐξῆλθον is the third person plural aorist indicative of ἐξέρχομαι, meaning "they went out." The verb ἐνέβησαν is the third person plural aorist indicative of ἐμβαίνω, meaning "they got into." The preposition εἰς means "into" and the noun τὸ πλοῖον means "the boat." The phrase thus means "they went out and got into the boat."

- **καὶ ἐν ἐκείνῃ τῇ νυκτὶ ἐπίασαν οὐδέν**: The conjunction καὶ means "and." The preposition ἐν means "in" and the demonstrative pronoun ἐκείνῃ means "that" or "those." The article τῇ is the dative singular feminine form of the definite article, meaning "the." The noun νυκτὶ means "night." The verb ἐπίασαν is the third person plural aorist indicative of ἐπιάζω, meaning "they caught." The pronoun οὐδέν is the accusative singular of οὐδείς, meaning "nothing." The phrase thus means "and in that night they caught nothing."

The literal translation of the entire verse is: "Simon Peter says to them, 'I am going to fish.' They say to him, 'We are coming with you.' They went out and got into the boat, and in that night they caught nothing."