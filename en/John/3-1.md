---
version: 1
---
- **Ἦν δὲ ἄνθρωπος ἐκ τῶν Φαρισαίων**: The verb Ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was" or "there was." The noun ἄνθρωπος is the nominative singular of ἄνθρωπος, which means "man" or "person." The preposition ἐκ means "from," and the article τῶν is the genitive plural of ὁ, meaning "the." The noun Φαρισαίων is the genitive plural of Φαρισαῖος, which means "Pharisee." The phrase thus means "Now there was a man from the Pharisees."

- **Νικόδημος ὄνομα αὐτῷ**: The noun Νικόδημος is the nominative singular of Νικόδημος, which is a personal name. The noun ὄνομα is the accusative singular of ὄνομα, meaning "name." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him" or "for him." The phrase thus means "His name was Nicodemus."

- **ἄρχων τῶν Ἰουδαίων**: The noun ἄρχων is the nominative singular of ἄρχων, which means "ruler" or "leader." The article τῶν is the genitive plural of ὁ, meaning "the." The noun Ἰουδαίων is the genitive plural of Ἰουδαῖος, which means "Jew." The phrase thus means "a ruler of the Jews."

The literal translation of John 3:1 is: "Now there was a man from the Pharisees, Nicodemus by name, a ruler of the Jews."