---
version: 1
---
- **ἀπεκρίθη Νικόδημος**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, which means "he answered." Νικόδημος is the nominative singular of Νικόδημος, which is a name. The phrase thus means "Nicodemus answered."

- **καὶ εἶπεν αὐτῷ**: The conjunction καὶ means "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said." The pronoun αὐτῷ is the dative singular of αὐτός, which means "to him." The phrase thus means "and he said to him."

- **Πῶς δύναται**: The adverb Πῶς means "how." The verb δύναται is the third person singular present indicative of δύναμαι, which means "he is able" or "he can." The phrase thus means "How is he able?"

- **ταῦτα γενέσθαι**: The pronoun ταῦτα is the accusative plural of οὗτος, which means "these." The verb γενέσθαι is the aorist infinitive of γίνομαι, which means "to happen" or "to become." The phrase thus means "for these things to happen."

The literal translation of John 3:9 is:
"Nicodemus answered and said to him, 'How is he able for these things to happen?'"