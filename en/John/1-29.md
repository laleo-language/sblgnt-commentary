---
version: 1
---
- **Τῇ ἐπαύριον**: The word Τῇ is the dative singular of the definite article ὁ, which means "the." The word ἐπαύριον is the accusative singular of ἐπαύριος, which means "on the next day." The phrase thus means "on the next day."

- **βλέπει τὸν Ἰησοῦν ἐρχόμενον πρὸς αὐτόν**: The verb βλέπει is the third person singular present indicative of βλέπω, which means "he sees." The word τὸν is the accusative singular of the definite article ὁ, which means "the." The word Ἰησοῦν is the accusative singular of Ἰησοῦς, which means "Jesus." The word ἐρχόμενον is the present participle of ἔρχομαι, which means "coming." The word πρὸς is a preposition meaning "towards" or "to." The word αὐτόν is the accusative singular of αὐτός, which means "him." The phrase thus means "he sees Jesus coming towards him."

- **καὶ λέγει**: The word καὶ is a conjunction meaning "and." The verb λέγει is the third person singular present indicative of λέγω, which means "he says." The phrase thus means "and he says."

- **Ἴδε ὁ ἀμνὸς τοῦ θεοῦ**: The word Ἴδε is an interjection meaning "behold" or "look." The word ὁ is the nominative singular of the definite article ὁ, which means "the." The word ἀμνὸς is the nominative singular of ἀμνός, which means "lamb." The word τοῦ is the genitive singular of the definite article ὁ, which means "of the." The word θεοῦ is the genitive singular of θεός, which means "God." The phrase thus means "Behold, the lamb of God."

- **ὁ αἴρων τὴν ἁμαρτίαν τοῦ κόσμου**: The word ὁ is the nominative singular of the definite article ὁ, which means "the." The word αἴρων is the present participle of αἴρω, which means "taking away" or "removing." The word τὴν is the accusative singular of the definite article ὁ, which means "the." The word ἁμαρτίαν is the accusative singular of ἁμαρτία, which means "sin." The word τοῦ is the genitive singular of the definite article ὁ, which means "of the." The word κόσμου is the genitive singular of κόσμος, which means "world." The phrase thus means "the one taking away the sin of the world."

The literal translation of John 1:29 would be: "On the next day he sees Jesus coming towards him, and he says, 'Behold, the lamb of God, the one taking away the sin of the world."