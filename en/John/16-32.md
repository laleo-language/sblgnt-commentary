---
version: 1
---
- **ἰδοὺ ἔρχεται ὥρα**: The word ἰδοὺ is an interjection that is often used to draw attention to something. Here, it can be translated as "behold" or "look." The verb ἔρχεται is the third person singular present indicative of ἔρχομαι, meaning "he is coming" or "he comes." The noun ὥρα means "hour" or "time." So the phrase can be translated as "behold, the hour is coming."

- **καὶ ἐλήλυθεν ἵνα σκορπισθῆτε**: The conjunction καὶ means "and." The verb ἐλήλυθεν is the third person singular perfect indicative of ἔρχομαι, meaning "he has come" or "he came." The conjunction ἵνα introduces a purpose clause and can be translated as "so that" or "in order that." The verb σκορπισθῆτε is the second person plural aorist subjunctive passive of σκορπίζω, meaning "you may be scattered" or "you may be dispersed." So the phrase can be translated as "and he has come so that you may be scattered."

- **ἕκαστος εἰς τὰ ἴδια**: The pronoun ἕκαστος means "each one" or "everyone." The preposition εἰς means "into" or "to." The article τὰ is the accusative plural of the definite article, and the adjective ἴδια is the accusative plural of ἴδιος, meaning "own" or "personal." So the phrase can be translated as "each one to his own."

- **κἀμὲ μόνον ἀφῆτε**: The conjunction κἀμὲ is a contraction of καὶ ἐμέ, meaning "and me." The adverb μόνον means "only" or "alone." The verb ἀφῆτε is the second person plural aorist subjunctive of ἀφίημι, meaning "you may leave" or "you may abandon." So the phrase can be translated as "and you leave only me."

- **καὶ οὐκ εἰμὶ μόνος, ὅτι ὁ πατὴρ μετʼ ἐμοῦ ἐστιν**: The conjunction καὶ means "and." The adverb οὐκ means "not." The verb εἰμὶ is the first person singular present indicative of εἰμί, meaning "I am." The adverb μόνος means "alone" or "only." The conjunction ὅτι introduces a reason clause and can be translated as "because" or "since." The article ὁ is the nominative singular of the definite article, and the noun πατὴρ means "father." The preposition μετʼ means "with." The pronoun ἐμοῦ means "me" or "my." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is" or "it is." So the phrase can be translated as "and I am not alone, because the Father is with me."

The literal translation of John 16:32 is: "Behold, the hour is coming and has come so that you may be scattered each one to his own, and you leave only me. And I am not alone, because the Father is with me."