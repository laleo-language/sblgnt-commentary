---
version: 1
---
- **Ἐγώ εἰμι**: The pronoun Ἐγώ is the first person singular pronoun, meaning "I." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "I am." This phrase means "I am."

- **ὁ ποιμὴν ὁ καλός**: The article ὁ is the masculine singular nominative article, meaning "the." The noun ποιμὴν is the masculine singular nominative of ποιμήν, meaning "shepherd." The adjective καλός is the masculine singular nominative of καλός, meaning "good" or "beautiful." This phrase means "the good shepherd."

- **ὁ ποιμὴν ὁ καλὸς**: This is a repetition of the previous phrase, emphasizing that the shepherd is good.

- **τὴν ψυχὴν αὐτοῦ τίθησιν**: The article τὴν is the feminine singular accusative article, meaning "the." The noun ψυχὴν is the feminine singular accusative of ψυχή, meaning "soul" or "life." The pronoun αὐτοῦ is the third person singular genitive pronoun, meaning "his." The verb τίθησιν is the third person singular present indicative of τίθημι, meaning "he lays" or "he places." This phrase means "he lays down his life."

- **ὑπὲρ τῶν προβάτων**: The preposition ὑπὲρ means "for" or "on behalf of." The article τῶν is the neuter plural genitive article, meaning "the." The noun προβάτων is the neuter plural genitive of πρόβατον, meaning "sheep." This phrase means "for the sheep."

The sentence can be translated as follows: "I am the good shepherd; the good shepherd lays down his life for the sheep."

In this verse, Jesus identifies himself as the good shepherd who is willing to sacrifice his own life for the sake of the sheep. This metaphor highlights his role as the caring and protective leader of his followers.