---
version: 1
---
- **τοῦτο δὲ ἔλεγεν**: The word τοῦτο is the nominative singular neuter pronoun meaning "this." The particle δὲ is often used to indicate a transition or contrast and can be translated as "but" or "and." The verb ἔλεγεν is the third person singular imperfect indicative of λέγω, meaning "he was saying." The phrase thus means "But he was saying this."

- **σημαίνων**: The participle σημαίνων is the nominative singular masculine present active participle of σημαίνω, meaning "signifying" or "indicating." Participles are verbal adjectives that can modify a noun or a verb. In this case, it modifies the subject of the previous phrase, "he." The phrase can be translated as "signifying."

- **ποίῳ θανάτῳ**: The word ποίῳ is the dative singular masculine pronoun meaning "by what" or "in what manner." The noun θανάτῳ is the dative singular masculine form of θάνατος, meaning "death." In Greek, the dative case is often used to indicate the means or manner by which something happens. The phrase can be translated as "by what death" or "in what manner of death."

- **ἤμελλεν ἀποθνῄσκειν**: The verb ἤμελλεν is the third person singular imperfect indicative of μέλλω, meaning "he was about to" or "he was going to." The infinitive ἀποθνῄσκειν is the present active infinitive of ἀποθνῄσκω, meaning "to die." The phrase can be translated as "he was about to die."

The literal translation of the entire verse is: "But he was saying this, signifying by what death he was about to die."