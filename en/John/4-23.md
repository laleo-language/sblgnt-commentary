---
version: 1
---
- **ἀλλὰ ἔρχεται ὥρα καὶ νῦν ἐστιν**: The word ἀλλὰ means "but" or "however." The verb ἔρχεται is the third person singular present indicative of ἔρχομαι, meaning "he/she/it is coming." The noun ὥρα means "hour" or "time." The adjective νῦν means "now." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he/she/it is." The phrase thus means "But an hour is coming and is now here."

- **ὅτε οἱ ἀληθινοὶ προσκυνηταὶ προσκυνήσουσιν τῷ πατρὶ**: The word ὅτε means "when" or "at the time when." The adjective ἀληθινοὶ means "true" or "genuine." The noun προσκυνηταὶ means "worshipers" or "those who worship." The verb προσκυνήσουσιν is the third person plural future indicative of προσκυνέω, meaning "they will worship." The definite article τῷ indicates that πατρὶ is in the dative case, meaning "to the father." The phrase thus means "when the true worshipers will worship the Father."

- **ἐν πνεύματι καὶ ἀληθείᾳ**: The preposition ἐν means "in" or "by." The noun πνεῦμα means "spirit" or "breath." The conjunction καὶ means "and." The noun ἀλήθεια means "truth." The phrase thus means "in spirit and truth."

- **καὶ γὰρ ὁ πατὴρ τοιούτους ζητεῖ τοὺς προσκυνοῦντας αὐτόν**: The conjunction καὶ means "and." The adverb γὰρ means "for" or "indeed." The definite article ὁ indicates that πατὴρ is in the nominative case, meaning "the Father." The adjective τοιούτους means "such" or "those." The verb ζητεῖ is the third person singular present indicative of ζητέω, meaning "he/she/it seeks" or "he/she/it searches for." The definite article τοὺς indicates that προσκυνοῦντας is in the accusative case, meaning "the ones who worship." The pronoun αὐτόν means "him." The phrase thus means "For indeed the Father seeks such worshipers."

- The entire verse can be literally translated as: "But an hour is coming and is now here, when the true worshipers will worship the Father in spirit and truth, for indeed the Father seeks such worshipers."