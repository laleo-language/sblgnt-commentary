---
version: 1
---
- **ὅτι μισθωτός ἐστιν**: The word ὅτι is a conjunction that introduces a statement. The word μισθωτός is an adjective in the nominative masculine singular form, derived from μισθός which means "wages" or "hire." It is used here to describe the subject of the sentence. The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is" or "it is." The phrase thus means "because he is a hired worker."

- **καὶ οὐ μέλει αὐτῷ περὶ τῶν προβάτων**: The word καὶ is a conjunction meaning "and." The word οὐ is a negation particle meaning "not." The verb μέλει is the third person singular present indicative of μέλω, meaning "it matters" or "it concerns." The pronoun αὐτῷ is the dative masculine singular form of αὐτός, meaning "to him." The preposition περὶ means "about" or "concerning." The article τῶν is the genitive plural form of ὁ, meaning "the." The noun προβάτων is the genitive plural form of πρόβατον, meaning "sheep." The phrase thus means "and it does not matter to him about the sheep."

Literal translation: "because he is a hired worker and it does not matter to him about the sheep."

In John 10:13, Jesus is speaking about the hired worker who does not have a personal investment in the sheep and does not care for their well-being. This verse is part of a larger discourse where Jesus contrasts himself as the Good Shepherd who knows his own and is willing to lay down his life for them.