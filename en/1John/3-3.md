---
version: 2
---
- **καὶ πᾶς**: καὶ is a conjunction meaning "and", and πᾶς is an adjective meaning "every" or "all". So together, καὶ πᾶς means "and everyone".

- **ὁ ἔχων**: ὁ is the definite article meaning "the", and ἔχων is the present active participle of ἔχω, meaning "having". So ὁ ἔχων means "the one having".

- **τὴν ἐλπίδα ταύτην**: τὴν is the definite article meaning "the", and ἐλπίδα is the accusative singular of ἐλπίς, meaning "hope". Ταύτην is a demonstrative pronoun meaning "this". So together, τὴν ἐλπίδα ταύτην means "this hope".

- **ἐπʼ αὐτῷ**: ἐπί is a preposition meaning "on" or "upon", and αὐτῷ is the pronoun meaning "himself". So together, ἐπʼ αὐτῷ means "on himself".

- **ἁγνίζει**: ἁγνίζει is the present active indicative of ἁγνίζω, meaning "he purifies".

- **ἑαυτὸν**: ἑαυτὸν is the pronoun meaning "himself".

- **καθὼς**: καθὼς is a conjunction meaning "just as" or "in the same way".

- **ἐκεῖνος**: ἐκεῖνος is a demonstrative pronoun meaning "he" or "that one".

- **ἁγνός ἐστιν**: ἁγνός is an adjective meaning "pure" or "holy", and ἐστιν is the present indicative of εἰμί, meaning "he is". So ἁγνός ἐστιν means "he is pure".

Putting it all together, the literal translation of the verse is: "And everyone who has this hope on himself purifies himself just as he is pure."