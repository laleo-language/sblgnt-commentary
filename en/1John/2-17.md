---
version: 2
---
καὶ ὁ κόσμος παράγεται: The word κόσμος is the nominative singular of κόσμος,
which means "world." The verb παράγεται is the third person singular present
indicative middle/passive of παράγω, which means "it is passing away" or "it is
coming to an end." The phrase thus means "and the world is passing away."

καὶ ἡ ἐπιθυμία αὐτοῦ: The word ἐπιθυμία is the nominative singular of
ἐπιθυμία, which means "desire" or "lust." The pronoun αὐτοῦ is the genitive
singular of αὐτός, which means "his" or "its." The phrase thus means "and its
desire."

ὁ δὲ ποιῶν τὸ θέλημα τοῦ θεοῦ: The article ὁ is the nominative singular of ὁ, which
means "the." The participle ποιῶν is the nominative singular masculine present
active participle of ποιέω, which means "doing" or "performing." The article τὸ is
the accusative singular of ὁ. The noun θέλημα is the accusative singular of
θέλημα, which means "will" or "desire." The genitive τοῦ θεοῦ is the genitive
singular of θεός, which means "God." The phrase thus means "but the one who does
the will of God."

μένει εἰς τὸν αἰῶνα: The verb μένει is the third person singular present
indicative active of μένω, which means "he/she/it remains" or "he/she/it
abides." The preposition εἰς means "into" or "to." The article τὸν is the accusative
singular of ὁ. The noun αἰῶνα is the accusative singular of αἰών, which means
"age" or "eternity." The phrase thus means "he/she/it remains into the age" or "he/she/it abides forever."

The literal translation of the entire verse is: "And the world is passing away
and its desire, but the one who does the will of God remains into the age."