---
version: 2
---
- **καὶ αὐτὸς ἱλασμός ἐστιν**: The word καὶ is a conjunction meaning "and." The word αὐτὸς is a pronoun meaning "he himself." The noun ἱλασμός is in the nominative singular and means "propitiation" or "atonement." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The phrase thus means "and he himself is the propitiation."

- **περὶ τῶν ἁμαρτιῶν ἡμῶν**: The preposition περὶ means "concerning" or "about." The article τῶν is the genitive plural of ὁ and means "of the." The noun ἁμαρτιῶν is in the genitive plural and means "sins." The pronoun ἡμῶν is the genitive plural of ἐγώ and means "our." The phrase thus means "concerning our sins."

- **οὐ περὶ τῶν ἡμετέρων δὲ μόνον**: The negation οὐ means "not." The preposition περὶ means "concerning" or "about." The article τῶν is the genitive plural of ὁ and means "of the." The pronoun ἡμετέρων is the genitive plural of ἐγώ and means "our." The adverb δὲ means "but" or "however." The adverb μόνον means "only" or "alone." The phrase thus means "not concerning our own sins only."

- **ἀλλὰ καὶ περὶ ὅλου τοῦ κόσμου**: The conjunction ἀλλὰ means "but" or "rather." The conjunction καὶ means "and." The preposition περὶ means "concerning" or "about." The pronoun ὅλου is the genitive singular of ὅλος and means "the whole." The article τοῦ is the genitive singular of ὁ and means "of the." The noun κόσμου is in the genitive singular and means "world." The phrase thus means "but also concerning the whole world."

- The literal translation of the entire verse is: "And he himself is the propitiation concerning our sins, not concerning our own sins only, but also concerning the whole world."