---
version: 2
---
- **καὶ ταῦτα γράφομεν**: The word καὶ is a conjunction meaning "and." The word ταῦτα is the accusative plural of οὗτος, which means "these." The verb γράφομεν is the first person plural present indicative of γράφω, meaning "we write." The phrase thus means "And we write these."
- **ἡμεῖς ἵνα**: The word ἡμεῖς is the nominative plural of ἐγώ, which means "we." The word ἵνα is a conjunction meaning "so that" or "in order that."
- **ἡ χαρὰ ἡμῶν**: The word χαρὰ is the nominative singular of χαρά, which means "joy" or "happiness." The word ἡμῶν is the genitive plural of ἐγώ, which means "our." The phrase thus means "our joy."
- **ᾖ πεπληρωμένη**: The verb ᾖ is the third person singular present subjunctive of εἰμί, meaning "may be" or "might be." The verb πεπληρωμένη is the perfect passive participle, nominative singular feminine, of πληρόω, meaning "filled" or "completed." The phrase thus means "may our joy be filled."

Overall, the sentence can be translated as: "And we write these so that our joy may be filled."