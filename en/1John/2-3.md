---
version: 2
---
**Καὶ ἐν τούτῳ**: The word Καὶ is a conjunction meaning "and." The phrase ἐν τούτῳ consists of the preposition ἐν, which means "in," and the demonstrative pronoun τούτῳ, which means "this." The phrase can be translated as "and in this."

**γινώσκομεν ὅτι ἐγνώκαμεν αὐτόν**: The verb γινώσκομεν is the first person plural present indicative of γινώσκω, which means "we know" or "we understand." The word ὅτι is a conjunction meaning "that." The verb ἐγνώκαμεν is the first person plural perfect indicative of γινώσκω, meaning "we have known" or "we have understood." The pronoun αὐτόν is the accusative singular of αὐτός, which means "him" or "it." The phrase can be translated as "We know that we have known him."

**ἐὰν τὰς ἐντολὰς αὐτοῦ τηρῶμεν**: The conjunction ἐὰν introduces a conditional clause and can be translated as "if." The word τὰς is the accusative plural of the article ὁ, which means "the." The word ἐντολὰς is the accusative plural of ἐντολή, which means "commandments" or "instructions." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his" or "its." The verb τηρῶμεν is the first person plural present subjunctive of τηρέω, which means "we keep" or "we observe." The phrase can be translated as "if we keep his commandments."

**The entire verse**: "And in this we know that we have known him, if we keep his commandments."