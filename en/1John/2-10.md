---
version: 2
---
- **ὁ ἀγαπῶν τὸν ἀδελφὸν αὐτοῦ**: The article ὁ indicates that the noun ἀγαπῶν is in the nominative case and is functioning as the subject of the sentence. The verb ἀγαπῶν is the present participle of ἀγαπάω, meaning "loving." The noun τὸν ἀδελφὸν is in the accusative case and is the direct object of the verb ἀγαπῶν. The pronoun αὐτοῦ is a genitive singular form of αὐτός, meaning "his." So the literal translation of this phrase is "the one loving his brother."

- **ἐν τῷ φωτὶ μένει**: The preposition ἐν indicates that the noun τῷ φωτί is in the dative case and is functioning as the location or sphere in which the action takes place. The noun φωτί is in the singular form and means "light." The verb μένει is the third person singular present indicative of μένω, meaning "to abide" or "to remain." So the literal translation of this phrase is "he abides in the light."

- **καὶ σκάνδαλον ἐν αὐτῷ οὐκ ἔστιν**: The conjunction καὶ connects this phrase to the previous one. The noun σκάνδαλον is in the nominative case and is the subject of the verb ἔστιν. The preposition ἐν indicates that the noun αὐτῷ is in the dative case and is functioning as the location in which the action takes place. The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "to be." The negation οὐκ indicates that the verb is negated. So the literal translation of this phrase is "and there is no stumbling block in him."

- The entire verse can be literally translated as: "The one loving his brother abides in the light, and there is no stumbling block in him."