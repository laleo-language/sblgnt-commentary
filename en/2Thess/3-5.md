---
version: 1
---
- **ὁ δὲ κύριος**: The article ὁ indicates that this is a definite noun phrase. The noun κύριος means "Lord" or "master." It is in the nominative case, singular number, and masculine gender. The phrase thus means "the Lord."

- **κατευθύναι ὑμῶν**: The verb κατευθύναι is the third person singular aorist active infinitive of the verb κατευθύνω, meaning "to direct" or "to guide." It is in the accusative case and agrees with the noun κύριος. The pronoun ὑμῶν is the genitive plural of ὑμεῖς, meaning "you." The phrase can be translated as "to direct/guide you."

- **τὰς καρδίας**: The article τὰς indicates that this is a definite noun phrase. The noun καρδίας means "hearts." It is in the accusative case, plural number, and feminine gender. The phrase can be translated as "the hearts."

- **εἰς τὴν ἀγάπην τοῦ θεοῦ**: The preposition εἰς means "into" or "towards." The article τὴν indicates that this is a definite noun phrase. The noun ἀγάπην means "love." It is in the accusative case, singular number, and feminine gender. The genitive phrase τοῦ θεοῦ means "of God." The phrase can be translated as "into the love of God."

- **καὶ εἰς τὴν ὑπομονὴν τοῦ Χριστοῦ**: The conjunction καὶ means "and." The article τὴν indicates that this is a definite noun phrase. The noun ὑπομονὴν means "endurance" or "perseverance." It is in the accusative case, singular number, and feminine gender. The genitive phrase τοῦ Χριστοῦ means "of Christ." The phrase can be translated as "and into the endurance of Christ."

The literal translation of 2 Thessalonians 3:5 is: "But the Lord to direct your hearts into the love of God and into the endurance of Christ."

This verse speaks of the Lord directing the hearts of the readers towards the love of God and the endurance of Christ. It emphasizes the importance of having a heart filled with love for God and being able to endure challenges and trials with the same endurance that Christ displayed.