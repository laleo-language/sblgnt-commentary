---
version: 1
---
- **εἴπερ δίκαιον**: The word εἴπερ is a conjunction that means "if indeed" or "if truly." The word δίκαιον is the accusative singular of δίκαιος, which means "just" or "righteous." The phrase can be translated as "if indeed it is just."

- **παρὰ θεῷ ἀνταποδοῦναι**: The preposition παρὰ means "from" or "by." The noun θεῷ is the dative singular of θεός, which means "God." The verb ἀνταποδοῦναι is the aorist infinitive of ἀνταποδίδωμι, which means "to repay" or "to recompense." The phrase can be translated as "to repay from God."

- **τοῖς θλίβουσιν ὑμᾶς**: The article τοῖς is the dative plural of the definite article ὁ, "the." The verb θλίβουσιν is the present active participle of θλίβω, which means "to afflict" or "to oppress." The pronoun ὑμᾶς is the accusative plural of σύ, which means "you." The phrase can be translated as "to those who afflict you."

- **θλῖψιν**: The noun θλῖψις is the accusative singular of θλῖψις, which means "affliction" or "tribulation." It completes the infinitive ἀνταποδοῦναι and can be translated as "affliction."

The entire verse can be translated as "if indeed it is just for God to repay affliction to those who afflict you."