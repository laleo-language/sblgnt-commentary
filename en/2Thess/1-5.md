---
version: 1
---
- **ἔνδειγμα τῆς δικαίας κρίσεως τοῦ θεοῦ**: The noun ἔνδειγμα is the nominative singular of ἔνδειγμα, which means "example" or "proof." The genitive singular τῆς δικαίας κρίσεως is a genitive of description and can be translated as "of the righteous judgment." The genitive τοῦ θεοῦ is a genitive of possession and means "of God." The phrase thus means "an example of God's righteous judgment."

- **εἰς τὸ καταξιωθῆναι ὑμᾶς τῆς βασιλείας τοῦ θεοῦ**: The preposition εἰς can be translated as "for" or "in order to." The infinitive καταξιωθῆναι is the aorist passive infinitive of καταξιόω, meaning "to deem worthy" or "to consider worthy." The pronoun ὑμᾶς is the accusative plural of ὑμεῖς, which means "you." The genitive τῆς βασιλείας is a genitive of possession and means "of the kingdom." The genitive τοῦ θεοῦ is a genitive of possession and means "of God." The phrase can be translated as "in order to be deemed worthy of the kingdom of God."

- **ὑπὲρ ἧς καὶ πάσχετε**: The preposition ὑπὲρ can be translated as "for" or "on behalf of." The relative pronoun ἧς is the genitive singular of ὅς, which means "which" or "that." The verb πάσχετε is the second person plural present indicative of πάσχω, meaning "you suffer" or "you endure." The phrase can be translated as "for which (kingdom) you also suffer."

The literal translation of the entire verse is: "an example of God's righteous judgment, in order to be deemed worthy of the kingdom of God, for which you also suffer."