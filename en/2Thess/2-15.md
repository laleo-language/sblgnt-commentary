---
version: 1
---
- **ἄρα οὖν**: The conjunction ἄρα means "therefore" or "so" and οὖν means "then" or "so." Together, they emphasize the logical connection between the previous statement and what follows. So we can translate this phrase as "therefore, then."

- **ἀδελφοί**: This is the vocative plural of ἀδελφός, meaning "brothers" or "brothers and sisters." It is used here as an address to the recipients of the letter, so we can translate it as "brothers and sisters" or simply "fellow believers."

- **στήκετε**: This is the second person plural present imperative of ἵστημι, meaning "to stand." It is used here as a command or exhortation to "stand firm" or "remain steadfast." So we can translate it as "stand firm" or "stay strong."

- **καὶ κρατεῖτε**: The conjunction καὶ means "and" and κρατεῖτε is the second person plural present imperative of κρατέω, meaning "to hold" or "to keep." So this phrase is a continuation of the exhortation to "stand firm" and it means "and keep" or "and hold fast."

- **τὰς παραδόσεις ἃς ἐδιδάχθητε**: The article τὰς is the accusative plural feminine form of ὁ, meaning "the." The noun παραδόσεις is the accusative plural form of παράδοσις, meaning "traditions" or "teachings." The relative pronoun ἃς is the accusative plural feminine form of ὅς, meaning "which" or "that." The verb ἐδιδάχθητε is the second person plural aorist passive indicative of διδάσκω, meaning "to teach" or "to instruct." So this phrase means "the traditions which you were taught" or "the teachings that were taught to you."

- **εἴτε διὰ λόγου εἴτε διʼ ἐπιστολῆς ἡμῶν**: The conjunctions εἴτε...εἴτε mean "whether...or" and introduce two options. The preposition διὰ means "through" or "by means of." The noun λόγου is the genitive singular form of λόγος, meaning "word" or "message." The noun ἐπιστολῆς is the genitive singular form of ἐπιστολή, meaning "letter." The pronoun ἡμῶν is the genitive plural form of ἐγώ, meaning "our." So this phrase means "whether through word or through our letter" or "whether by word or by our letter."

Putting it all together, we can translate the verse as follows: "Therefore, then, brothers and sisters, stand firm and keep the traditions which you were taught, whether by word or by our letter."

This verse is not syntactically ambiguous.