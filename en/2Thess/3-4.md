---
version: 1
---
- **πεποίθαμεν δὲ ἐν κυρίῳ**: The verb πεποίθαμεν is the first person plural perfect indicative of πείθω, meaning "we have believed" or "we have confidence." The preposition ἐν means "in" and takes the dative case, so κυρίῳ is the dative singular of κύριος, meaning "the Lord." The phrase thus means "we have confidence in the Lord."

- **ἐφʼ ὑμᾶς**: The preposition ἐπί means "on" or "upon" and takes the accusative case, so ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase thus means "upon you."

- **ὅτι ἃ παραγγέλλομεν καὶ ποιεῖτε καὶ ποιήσετε**: The pronoun ὅτι means "that" and introduces a subordinate clause. The verb παραγγέλλομεν is the first person plural present indicative of παραγγέλλω, meaning "we command" or "we instruct." The verb ποιεῖτε is the second person plural present indicative of ποιέω, meaning "you do" or "you are doing." The verb ποιήσετε is the second person plural future indicative of ποιέω, meaning "you will do" or "you will be doing." The phrase thus means "that which we command and you are doing and you will do."

Literal translation: "But we have confidence in the Lord upon you, that which we command and you are doing and you will do."