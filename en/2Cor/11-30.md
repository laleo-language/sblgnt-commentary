---
version: 1
---
- **Εἰ καυχᾶσθαι δεῖ**: The word εἰ is a conjunction that means "if." The verb καυχᾶσθαι is the present infinitive middle/passive of καυχάομαι, which means "to boast" or "to glory." The verb δεῖ is the third person singular present indicative of δέω, which means "it is necessary" or "it is fitting." The phrase thus means "if it is necessary to boast."

- **τὰ τῆς ἀσθενείας μου**: The article τὰ is the neuter plural nominative of the definite article, meaning "the." The noun τῆς ἀσθενείας is the genitive singular of ἀσθένεια, which means "weakness" or "infirmity." The pronoun μου is the first person singular genitive pronoun, meaning "my." The phrase thus means "the things of my weakness" or "my weaknesses."

- **καυχήσομαι**: The verb καυχήσομαι is the first person singular future indicative of καυχάομαι, meaning "I will boast" or "I will glory." 

The entire verse is translated as: "If it is necessary to boast, I will boast of my weaknesses."