---
version: 1
---
- **καὶ ἔσομαι ὑμῖν εἰς πατέρα**: The word καὶ is a coordinating conjunction meaning "and." The verb ἔσομαι is the first person singular future indicative of εἰμί, meaning "I will be." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The preposition εἰς means "into" or "for." The noun πατέρα is the accusative singular of πατήρ, meaning "father." The phrase thus means "and I will be a father to you."

- **καὶ ὑμεῖς ἔσεσθέ μοι εἰς υἱοὺς καὶ θυγατέρας**: The word καὶ is a coordinating conjunction meaning "and." The pronoun ὑμεῖς is the nominative plural of σύ, meaning "you." The verb ἔσεσθέ is the second person plural future indicative of εἰμί, meaning "you will be." The pronoun μοι is the dative singular of ἐγώ, meaning "to me." The preposition εἰς means "into" or "for." The noun υἱοὺς is the accusative plural of υἱός, meaning "sons." The conjunction καὶ is again used to mean "and." The noun θυγατέρας is the accusative plural of θυγάτηρ, meaning "daughters." The phrase thus means "and you will be sons and daughters to me."

- **λέγει κύριος παντοκράτωρ**: The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The noun κύριος is the nominative singular of κύριος, meaning "Lord." The noun παντοκράτωρ is the nominative singular of παντοκράτωρ, meaning "Almighty." The phrase thus means "says the Lord Almighty."

Literal translation: "And I will be a father to you, and you will be sons and daughters to me, says the Lord Almighty."