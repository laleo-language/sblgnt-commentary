---
version: 1
---
- **Ἑαυτοὺς πειράζετε**: The word Ἑαυτοὺς is the accusative plural reflexive pronoun, meaning "yourselves." The verb πειράζετε is the second person plural present indicative of πειράζω, meaning "you test" or "you examine." The phrase thus means "You test yourselves."

- **εἰ ἐστὲ ἐν τῇ πίστει**: The word εἰ is a conjunction meaning "if." The verb ἐστὲ is the second person plural present indicative of εἰμί, meaning "you are." The prepositional phrase ἐν τῇ πίστει means "in the faith." The phrase thus means "If you are in the faith."

- **ἑαυτοὺς δοκιμάζετε**: The word ἑαυτοὺς is the accusative plural reflexive pronoun, meaning "yourselves." The verb δοκιμάζετε is the second person plural present indicative of δοκιμάζω, meaning "you examine" or "you test." The phrase thus means "You test yourselves."

- **ἢ οὐκ ἐπιγινώσκετε ἑαυτοὺς**: The word ἢ is a conjunction meaning "or." The verb ἐπιγινώσκετε is the second person plural present indicative of ἐπιγινώσκω, meaning "you know" or "you recognize." The phrase thus means "Or do you not recognize yourselves."

- **ὅτι Ἰησοῦς Χριστὸς ἐν ὑμῖν**: The word ὅτι is a conjunction meaning "that." The proper noun Ἰησοῦς Χριστὸς means "Jesus Christ." The preposition ἐν means "in." The pronoun ὑμῖν is the second person plural pronoun, meaning "you." The phrase thus means "that Jesus Christ is in you."

- **εἰ μήτι ἀδόκιμοί ἐστε**: The word εἰ is a conjunction meaning "if." The adverb μήτι is used for emphasis and can be translated as "surely" or "indeed." The adjective ἀδόκιμοί is the nominative plural of ἀδόκιμος, meaning "unapproved" or "rejected." The verb ἐστε is the second person plural present indicative of εἰμί, meaning "you are." The phrase thus means "Surely you are not unapproved."

The entire verse can be translated as: "Test yourselves to see if you are in the faith; examine yourselves. Or do you not recognize yourselves, that Jesus Christ is in you? Surely you are not unapproved."