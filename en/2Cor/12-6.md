---
version: 1
---
- **ἐὰν θελήσω καυχήσασθαι**: The word ἐὰν is a conjunction meaning "if." The verb θελήσω is the first person singular aorist subjunctive of θέλω, meaning "I want" or "I desire." The verb καυχήσασθαι is the first person singular aorist middle subjunctive of καυχάομαι, meaning "to boast" or "to glory." The phrase thus means "if I want to boast."

- **οὐκ ἔσομαι ἄφρων**: The word οὐκ is a negative particle meaning "not." The verb ἔσομαι is the first person singular future indicative of εἰμί, meaning "I will be." The adjective ἄφρων is the nominative singular masculine of ἄφρων, meaning "foolish" or "senseless." The phrase thus means "I will not be foolish."

- **ἀλήθειαν γὰρ ἐρῶ**: The noun ἀλήθειαν is the accusative singular of ἀλήθεια, meaning "truth." The conjunction γὰρ is a particle meaning "for." The verb ἐρῶ is the first person singular future indicative of λέγω, meaning "I will say." The phrase thus means "for I will speak the truth."

- **φείδομαι δέ**: The verb φείδομαι is the first person singular present indicative of φείδομαι, meaning "I spare" or "I refrain." The conjunction δέ is a particle meaning "but" or "and." The phrase thus means "but I spare" or "and I refrain."

- **μή τις εἰς ἐμὲ λογίσηται**: The particle μή is a negative particle meaning "not." The pronoun τις is the nominative singular masculine of τις, meaning "someone" or "anyone." The preposition εἰς is a preposition meaning "into" or "to." The pronoun ἐμὲ is the accusative singular first person pronoun, meaning "me." The verb λογίσηται is the third person singular aorist middle subjunctive of λογίζομαι, meaning "to consider" or "to reckon." The phrase thus means "let no one consider me."

- **ὑπὲρ ὃ βλέπει με ἢ ἀκούει**: The preposition ὑπὲρ is a preposition meaning "above" or "beyond." The pronoun ὃ is the accusative singular neuter of ὅς, meaning "what" or "that which." The verb βλέπει is the third person singular present indicative of βλέπω, meaning "he sees." The pronoun με is the accusative singular first person pronoun, meaning "me." The conjunction ἢ is a conjunction meaning "or." The verb ἀκούει is the third person singular present indicative of ἀκούω, meaning "he hears." The phrase thus means "above what he sees in me or hears."

- **τι ἐξ ἐμοῦ**: The pronoun τι is the accusative singular neuter of τις, meaning "something" or "anything." The preposition ἐξ is a preposition meaning "out of" or "from." The pronoun ἐμοῦ is the genitive singular first person pronoun, meaning "of me" or "from me." The phrase thus means "something from me."

The entire verse, 2Cor 12:6, can be literally translated as: "For if I want to boast, I will not be foolish, for I will speak the truth; but I spare, so that no one may consider me above what he sees in me or hears something from me."