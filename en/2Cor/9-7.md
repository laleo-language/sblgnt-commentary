---
version: 1
---
- **ἕκαστος καθὼς προῄρηται τῇ καρδίᾳ**: The word ἕκαστος is an adjective meaning "each" or "every." The word καθὼς is a conjunction meaning "as" or "just as." The verb προῄρηται is the third person singular aorist middle indicative of προερῶ, which means "to purpose" or "to decide." The dative noun τῇ καρδίᾳ means "in the heart." So the phrase can be translated as "each one as he purposes in his heart."

- **μὴ ἐκ λύπης ἢ ἐξ ἀνάγκης**: The word μὴ is a negative particle meaning "not." The preposition ἐκ means "out of" or "from." The noun λύπης means "sorrow" or "grief." The conjunction ἢ is used to give alternatives and means "or." The preposition ἐξ also means "out of" or "from." The noun ἀνάγκης means "necessity" or "constraint." So the phrase can be translated as "not out of sorrow or out of necessity."

- **ἱλαρὸν γὰρ δότην ἀγαπᾷ ὁ θεός**: The adjective ἱλαρὸν means "cheerful" or "joyful." The noun δότην means "giver." The verb ἀγαπᾷ is the third person singular present indicative of ἀγαπάω, which means "to love." The article ὁ is the definite article "the" and the noun θεός means "God." So the phrase can be translated as "for God loves a cheerful giver."

Literal translation: "Each one as he purposes in his heart, not out of sorrow or out of necessity; for God loves a cheerful giver."

In this verse, Paul is encouraging the Corinthians to give generously, but not out of a sense of obligation or compulsion. He emphasizes that each person should give according to their own decision and with a joyful and willing heart, because God loves those who give cheerfully.