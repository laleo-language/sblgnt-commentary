---
version: 1
---
- **διάκονοι Χριστοῦ εἰσιν**: The word διάκονοι is the nominative plural of διάκονος, which means "servants" or "ministers." Χριστοῦ is the genitive singular of Χριστός, meaning "of Christ." The phrase εἰσιν is the third person plural present indicative of εἰμί, which means "they are." The phrase thus means "they are servants of Christ."

- **παραφρονῶν λαλῶ, ὑπὲρ ἐγώ**: The word παραφρονῶν is the present active participle nominative singular of παραφρονέω, meaning "I am beside myself" or "I am out of my mind." The verb λαλῶ is the first person singular present indicative of λαλέω, meaning "I speak" or "I talk." The phrase ὑπὲρ ἐγώ means "for myself" or "about myself." The phrase thus means "I am speaking while beside myself, for myself."

- **ἐν κόποις περισσοτέρως, ἐν φυλακαῖς περισσοτέρως, ἐν πληγαῖς ὑπερβαλλόντως, ἐν θανάτοις πολλάκις**: 
  - The phrase ἐν κόποις is the preposition ἐν followed by the dative plural of κόπος, meaning "in labors" or "in toils."
  - The word περισσοτέρως is the adverb form of περισσός, meaning "more" or "excessively." It modifies κόποις and emphasizes "in more labors."
  - The phrase ἐν φυλακαῖς is the preposition ἐν followed by the dative plural of φυλακή, meaning "in prisons."
  - The word περισσοτέρως is again used as an adverb to modify φυλακαῖς, emphasizing "in more prisons."
  - The phrase ἐν πληγαῖς is the preposition ἐν followed by the dative plural of πληγή, meaning "in beatings" or "in blows."
  - The word ὑπερβαλλόντως is the adverb form of ὑπερβάλλω, meaning "exceedingly" or "beyond measure." It modifies πληγαῖς and emphasizes "in exceedingly more beatings."
  - The phrase ἐν θανάτοις is the preposition ἐν followed by the dative plural of θάνατος, meaning "in deaths."
  - The word πολλάκις is an adverb meaning "many times" or "often." It modifies θανάτοις and emphasizes "in many deaths."
  
  The entire phrase means "in more labors, in more prisons, in exceedingly more beatings, in many deaths."

The literal translation of the verse is "Are they servants of Christ? I speak as if insane, I more so; in labors more abundantly, in prisons more abundantly, in beatings exceedingly, in deaths often."