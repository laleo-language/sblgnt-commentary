---
version: 1
---
- **Ἔχοντες δὲ τὸ αὐτὸ πνεῦμα τῆς πίστεως**: The participle Ἔχοντες is the nominative plural of ἔχω, meaning "having." The article τὸ is the neuter singular accusative of ὁ, meaning "the." The adjective αὐτὸ is the neuter singular accusative of αὐτός, meaning "the same." The noun πνεῦμα is the neuter singular accusative of πνεῦμα, meaning "spirit." The genitive τῆς πίστεως is the genitive singular of πίστις, meaning "faith." The phrase thus means "having the same spirit of faith."

- **κατὰ τὸ γεγραμμένον**: The preposition κατὰ means "according to" or "in accordance with." The article τὸ is the neuter singular accusative of ὁ, meaning "the." The verb γεγραμμένον is the perfect passive participle of γράφω, meaning "written." The phrase thus means "according to what is written."

- **Ἐπίστευσα, διὸ ἐλάλησα**: The verb Ἐπίστευσα is the first person singular aorist indicative of πιστεύω, meaning "I believed." The conjunction διὸ means "therefore" or "for this reason." The verb ἐλάλησα is the first person singular aorist indicative of λαλέω, meaning "I spoke." The phrase thus means "I believed, therefore I spoke."

- **καὶ ἡμεῖς πιστεύομεν, διὸ καὶ λαλοῦμεν**: The conjunction καὶ means "and." The pronoun ἡμεῖς is the first person plural nominative of ἐγώ, meaning "we." The verb πιστεύομεν is the first person plural present indicative of πιστεύω, meaning "we believe." The conjunction διὸ means "therefore" or "for this reason." The verb λαλοῦμεν is the first person plural present indicative of λαλέω, meaning "we speak." The phrase thus means "and we believe, therefore we speak."

- The literal translation of the entire verse is: "Having the same spirit of faith, according to what is written, 'I believed, therefore I spoke,' we also believe, therefore we also speak."