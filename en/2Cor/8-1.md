---
version: 1
---
- **Γνωρίζομεν δὲ ὑμῖν**: The verb γνωρίζομεν is the first person plural present indicative of γνωρίζω, meaning "we make known" or "we inform." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The phrase thus means "We inform you."

- **ἀδελφοί**: The noun ἀδελφοί is the nominative plural of ἀδελφός, meaning "brothers" or "brothers and sisters." It is used here as an address to the recipients of the letter, indicating a sense of unity and familial relationship. 

- **τὴν χάριν τοῦ θεοῦ τὴν δεδομένην**: The article τὴν is the accusative singular feminine definite article. The noun χάριν is the accusative singular of χάρις, meaning "grace" or "favor." The genitive τοῦ θεοῦ indicates that the grace is from God. The participle δεδομένην is the accusative singular feminine perfect participle of δίδωμι, meaning "given." The phrase thus means "the grace of God that has been given."

- **ἐν ταῖς ἐκκλησίαις τῆς Μακεδονίας**: The preposition ἐν means "in" or "among." The article ταῖς is the dative plural feminine definite article. The noun ἐκκλησίαις is the dative plural of ἐκκλησία, meaning "churches." The genitive τῆς Μακεδονίας indicates that the churches are from Macedonia. The phrase thus means "in the churches of Macedonia."

Literal translation: "We inform you, brothers, about the grace of God that has been given in the churches of Macedonia."