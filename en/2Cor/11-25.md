---
version: 1
---
- **τρὶς ἐραβδίσθην**: The word τρὶς is an adverb meaning "three times." The verb ἐραβδίσθην is the first person singular aorist passive indicative of ἐραβδίζω, which means "I was beaten with rods." The phrase thus means "I was beaten with rods three times."

- **ἅπαξ ἐλιθάσθην**: The word ἅπαξ is an adverb meaning "once." The verb ἐλιθάσθην is the first person singular aorist passive indicative of λιθάζω, which means "I was stoned." The phrase thus means "I was stoned once."

- **τρὶς ἐναυάγησα**: The word τρὶς is an adverb meaning "three times." The verb ἐναυάγησα is the first person singular aorist indicative of ἐναυάγησα, which means "I was shipwrecked." The phrase thus means "I was shipwrecked three times."

- **νυχθήμερον ἐν τῷ βυθῷ πεποίηκα**: The word νυχθήμερον is an adverb meaning "a night and a day" or "day and night." The prepositional phrase ἐν τῷ βυθῷ means "in the deep" or "in the abyss." The verb πεποίηκα is the first person singular perfect indicative of ποιέω, which means "I have made" or "I have become." The phrase thus means "I have spent a night and a day in the deep."

The entire verse, 2 Corinthians 11:25, can be translated as: "Three times I was beaten with rods, once I was stoned, three times I was shipwrecked, a night and a day I have spent in the deep."