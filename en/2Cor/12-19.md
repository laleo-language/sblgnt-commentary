---
version: 1
---
- **Πάλαι δοκεῖτε**: The word Πάλαι is an adverb meaning "formerly" or "previously." The verb δοκεῖτε is the second person plural present indicative of δοκέω, meaning "you think" or "you suppose." The phrase thus means "You used to think" or "You previously supposed."

- **ὅτι ὑμῖν ἀπολογούμεθα**: The word ὅτι is a conjunction meaning "that." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The verb ἀπολογούμεθα is the first person plural present indicative middle/passive of ἀπολογέομαι, meaning "we defend" or "we make a defense." The phrase thus means "that we are defending to you" or "that we are making a defense to you."

- **κατέναντι θεοῦ ἐν Χριστῷ λαλοῦμεν**: The word κατέναντι is a preposition meaning "in the presence of" or "before." The noun θεοῦ is the genitive singular of θεός, meaning "God." The preposition ἐν is a preposition meaning "in" or "by means of." The noun Χριστῷ is the dative singular of Χριστός, meaning "Christ." The verb λαλοῦμεν is the first person plural present indicative of λαλέω, meaning "we speak" or "we talk." The phrase thus means "we speak in the presence of God in Christ."

- **τὰ δὲ πάντα**: The word τὰ is the nominative neuter plural article meaning "the." The word δὲ is a conjunction meaning "but" or "and." The word πάντα is the accusative neuter plural of πᾶς, meaning "all" or "everything." The phrase thus means "but everything" or "and everything."

- **ἀγαπητοί**: The word ἀγαπητοί is the vocative masculine plural of ἀγαπητός, meaning "beloved" or "dear ones." It is an address to the recipients of the letter.

- **ὑπὲρ τῆς ὑμῶν οἰκοδομῆς**: The preposition ὑπὲρ is a preposition meaning "for" or "in behalf of." The article τῆς is the genitive feminine singular article meaning "of the." The pronoun ὑμῶν is the genitive plural of σύ, meaning "your." The noun οἰκοδομῆς is the genitive singular of οἰκοδομή, meaning "building" or "edification." The phrase thus means "for the building up of your."

The literal translation of the entire verse is: "You used to think that we are defending to you? We speak in the presence of God in Christ. But everything, beloved ones, for the building up of your."