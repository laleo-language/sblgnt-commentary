---
version: 1
---
- **ἀσπάσασθε ἀλλήλους**: The verb ἀσπάσασθε is the second person plural aorist middle imperative of ἀσπάζομαι, meaning "greet" or "welcome." The pronoun ἀλλήλους is the accusative plural form of ἀλλήλων, which means "one another" or "each other." The phrase thus means "Greet one another."

- **ἐν ἁγίῳ φιλήματι**: The preposition ἐν means "in" or "with." The adjective ἁγίῳ is the dative singular form of ἅγιος, meaning "holy" or "sacred." The noun φιλήματι is the dative singular form of φίλημα, which means "kiss." The phrase can be translated as "with a holy kiss."

- **ἀσπάζονται ὑμᾶς οἱ ἅγιοι πάντες**: The verb ἀσπάζονται is the third person plural present middle indicative of ἀσπάζομαι, meaning "greet" or "welcome." The pronoun ὑμᾶς is the accusative plural form of ὑμεῖς, which means "you" (plural). The article οἱ is the nominative plural form of ὁ, which means "the." The adjective ἅγιοι is the nominative plural form of ἅγιος, meaning "holy" or "sacred." The adjective πάντες is the nominative plural form of πᾶς, meaning "all" or "every." The phrase can be translated as "All the holy ones greet you."

The literal translation of 2 Corinthians 13:12 is: "Greet one another with a holy kiss. All the holy ones greet you."