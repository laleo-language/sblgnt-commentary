---
version: 1
---
- **Αἱ ἐπιστολαὶ μέν**: The word Αἱ is the feminine nominative plural definite article, meaning "the." The noun ἐπιστολαὶ is the nominative plural form of ἐπιστολή, which means "letters." The particle μέν is used here to introduce a contrast, and can be translated as "on the one hand" or "indeed." So this phrase means "The letters, indeed..."

- **φησίν**: The verb φησίν is the third person singular present indicative of φημί, which means "he says" or "he is saying." This word introduces a direct quotation and can be translated as "he says."

- **βαρεῖαι καὶ ἰσχυραί**: The adjective βαρεῖαι is the nominative feminine plural form of βαρύς, meaning "heavy" or "weighty." The adjective ἰσχυραί is the nominative feminine plural form of ἰσχυρός, meaning "strong" or "powerful." These adjectives describe the letters. So this phrase means "heavy and strong."

- **ἡ δὲ παρουσία τοῦ σώματος**: The definite article ἡ is the feminine nominative singular, meaning "the." The noun παρουσία is the nominative singular form of παρουσία, meaning "presence" or "appearance." The genitive noun τοῦ σώματος is the genitive singular form of σῶμα, meaning "body." This phrase means "the presence of the body."

- **ἀσθενὴς καὶ ὁ λόγος ἐξουθενημένος**: The adjective ἀσθενὴς is the nominative feminine singular form of ἀσθενής, meaning "weak" or "feeble." The definite article ὁ is the masculine nominative singular, meaning "the." The noun λόγος is the nominative singular form of λόγος, meaning "word" or "speech." The participle ἐξουθενημένος is the masculine nominative singular present passive participle of ἐξουθενέω, meaning "to despise" or "to treat with contempt." These words describe the presence of the body. So this phrase means "the presence of the body weak and the word despised."

The literal translation of the entire verse is: "The letters, indeed...he says, are weighty and strong, but the presence of the body is weak and the word despised."