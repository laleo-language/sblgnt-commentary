---
version: 1
---
- **καὶ πᾶν ὕψωμα ἐπαιρόμενον**: The word καὶ is a conjunction meaning "and." The noun πᾶν is the nominative singular of πᾶς, which means "every" or "all." The noun ὕψωμα is the accusative singular of ὕψωμα, which means "high thing" or "high thought." The verb ἐπαιρόμενον is the present participle middle/passive accusative singular of ἐπαίρω, which means "to lift up" or "to exalt." The phrase can be translated as "and every high thing being lifted up."

- **κατὰ τῆς γνώσεως τοῦ θεοῦ**: The word κατὰ is a preposition meaning "against" or "according to." The article τῆς is the feminine genitive singular of ὁ, which means "the." The noun γνώσεως is the genitive singular of γνῶσις, which means "knowledge." The article τοῦ is the masculine genitive singular of ὁ. The noun θεοῦ is the genitive singular of θεός, which means "God." The phrase can be translated as "against the knowledge of God."

- **καὶ αἰχμαλωτίζοντες πᾶν νόημα**: The word καὶ is a conjunction meaning "and." The verb αἰχμαλωτίζοντες is the present participle active nominative plural of αἰχμαλωτίζω, which means "to take captive" or "to bring into captivity." The noun πᾶν is the accusative singular of πᾶς, which means "every" or "all." The noun νόημα is the accusative singular of νόημα, which means "thought" or "mind." The phrase can be translated as "and taking captive every thought."

- **εἰς τὴν ὑπακοὴν τοῦ Χριστοῦ**: The preposition εἰς is a preposition meaning "to" or "for." The article τὴν is the feminine accusative singular of ὁ. The noun ὑπακοὴν is the accusative singular of ὑπακοή, which means "obedience." The article τοῦ is the masculine genitive singular of ὁ. The noun Χριστοῦ is the genitive singular of Χριστός, which means "Christ." The phrase can be translated as "for the obedience of Christ."

The entire verse can be translated as "and every high thing being lifted up against the knowledge of God, and taking captive every thought for the obedience of Christ."