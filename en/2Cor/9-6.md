---
version: 1
---
- **Τοῦτο δέ**: The word τοῦτο is a neuter singular pronoun meaning "this." The particle δέ is a conjunction meaning "but" or "and." The phrase thus means "But this."

- **ὁ σπείρων φειδομένως φειδομένως καὶ θερίσει**: The article ὁ is the masculine singular nominative form of the definite article, meaning "the." The noun σπείρων is the present active participle of σπείρω, meaning "sowing." The adverb φειδομένως is the present active participle of φειδομαι, meaning "sparingly" or "with restraint." The conjunction καὶ is a coordinating conjunction meaning "and." The verb θερίσει is the third person singular future indicative of θερίζω, meaning "he will reap." The phrase thus means "The one who sows sparingly, he will also reap."

- **καὶ ὁ σπείρων ἐπʼ εὐλογίαις ἐπʼ εὐλογίαις καὶ θερίσει**: The conjunction καὶ is a coordinating conjunction meaning "and." The article ὁ is the masculine singular nominative form of the definite article, meaning "the." The noun σπείρων is the present active participle of σπείρω, meaning "sowing." The preposition ἐπʼ is a contraction of ἐπί meaning "on" or "upon." The noun εὐλογίαις is the dative plural form of εὐλογία, meaning "blessing" or "generosity." The phrase is repeated for emphasis. The verb θερίσει is the third person singular future indicative of θερίζω, meaning "he will reap." The phrase thus means "And the one who sows upon blessings, he will also reap."

The literal translation of the entire verse is: "But this, the one who sows sparingly, he will also reap, and the one who sows upon blessings, he will also reap."