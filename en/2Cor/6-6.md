---
version: 1
---
- **ἐν ἁγνότητι**: The word ἁγνότητι is the dative singular of ἁγνότης, which means "purity" or "chastity." The preposition ἐν means "in" or "with." The phrase thus means "in purity" or "with purity."

- **ἐν γνώσει**: The word γνώσει is the dative singular of γνῶσις, which means "knowledge" or "understanding." The preposition ἐν means "in" or "with." The phrase thus means "in knowledge" or "with knowledge."

- **ἐν μακροθυμίᾳ**: The word μακροθυμίᾳ is the dative singular of μακροθυμία, which means "patience" or "long-suffering." The preposition ἐν means "in" or "with." The phrase thus means "in patience" or "with patience."

- **ἐν χρηστότητι**: The word χρηστότητι is the dative singular of χρηστότης, which means "goodness" or "kindness." The preposition ἐν means "in" or "with." The phrase thus means "in goodness" or "with goodness."

- **ἐν πνεύματι ἁγίῳ**: The word πνεύματι is the dative singular of πνεῦμα, which means "spirit." The adjective ἁγίῳ is the dative singular masculine/neuter of ἅγιος, which means "holy" or "sacred." The preposition ἐν means "in" or "with." The phrase thus means "in the Holy Spirit" or "with the Holy Spirit."

- **ἐν ἀγάπῃ ἀνυποκρίτῳ**: The word ἀγάπῃ is the dative singular of ἀγάπη, which means "love." The adjective ἀνυποκρίτῳ is the dative singular masculine/neuter of ἀνυπόκριτος, which means "unfeigned" or "sincere." The preposition ἐν means "in" or "with." The phrase thus means "in sincere love" or "with sincere love."

The entire verse, 2 Corinthians 6:6, can be translated as: "in purity, in knowledge, in patience, in goodness, in the Holy Spirit, in sincere love."