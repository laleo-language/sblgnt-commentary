---
version: 1
---
- **καθὼς γέγραπται**: The word καθὼς is a conjunction meaning "just as" or "according to." The verb γέγραπται is the third person singular perfect indicative of γράφω, meaning "it is written." The phrase thus means "just as it is written."
- **Ἐσκόρπισεν**: The verb Ἐσκόρπισεν is the third person singular aorist indicative of σκορπίζω, which means "he scattered" or "he dispersed."
- **ἔδωκεν τοῖς πένησιν**: The verb ἔδωκεν is the third person singular aorist indicative of δίδωμι, meaning "he gave." The noun τοῖς πένησιν is the dative plural of πένης, which means "the poor" or "the needy." The phrase thus means "he gave to the poor."
- **ἡ δικαιοσύνη αὐτοῦ μένει εἰς τὸν αἰῶνα**: The noun phrase ἡ δικαιοσύνη αὐτοῦ means "his righteousness." The verb μένει is the third person singular present indicative of μένω, meaning "it remains" or "it abides." The prepositional phrase εἰς τὸν αἰῶνα means "forever" or "for eternity." The phrase thus means "his righteousness remains forever."

Literal translation: "Just as it is written: He scattered, he gave to the poor, his righteousness remains forever."

In this verse, Paul is quoting from Psalm 112:9 to illustrate the principle of generosity and God's faithfulness in providing for the needs of the poor.