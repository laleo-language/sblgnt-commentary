---
version: 1
---
- **συνεπέμψαμεν δὲ αὐτοῖς**: The verb συνεπέμψαμεν is the first person plural aorist indicative active of συνεπιμέμφω, which means "to send together with" or "to accompany." The pronoun αὐτοῖς is the dative plural of αὐτός, which means "to them." The phrase thus means "we sent together with them."

- **τὸν ἀδελφὸν ἡμῶν**: The article τὸν is the accusative singular masculine form of ὁ, which means "the." The noun ἀδελφὸν is the accusative singular masculine form of ἀδελφός, which means "brother." The pronoun ἡμῶν is the genitive plural of ἐγώ, which means "our." The phrase thus means "our brother."

- **ὃν ἐδοκιμάσαμεν ἐν πολλοῖς πολλάκις**: The pronoun ὃν is the accusative singular masculine form of ὅς, which means "whom" or "which." The verb ἐδοκιμάσαμεν is the first person plural aorist indicative active of δοκιμάζω, which means "to test" or "to prove." The adverb πολλάκις means "many times." The phrase πολλοῖς πολλάκις functions as an adverbial phrase modifying the verb ἐδοκιμάσαμεν and means "in many ways many times." The phrase thus means "whom we have tested in many ways many times."

- **σπουδαῖον ὄντα**: The adjective σπουδαῖον is the accusative singular masculine form of σπουδαῖος, which means "diligent" or "earnest." The participle ὄντα is the accusative singular masculine form of the present active participle of εἰμί, which means "being" or "existing." The phrase thus means "being diligent."

- **νυνὶ δὲ πολὺ σπουδαιότερον**: The adverb νυνὶ means "now." The conjunction δὲ means "but" or "however." The adjective πολὺ is the neuter accusative singular form of πολύς, which means "much" or "many." The comparative adjective σπουδαιότερον is the neuter accusative singular comparative form of σπουδαῖος, which means "more diligent" or "more earnest." The phrase thus means "but now much more diligent."

- **πεποιθήσει πολλῇ τῇ εἰς ὑμᾶς**: The verb πεποιθήσει is the third person singular future indicative active of πείθω, which means "to trust" or "to have confidence in." The noun πολλῇ is the dative singular feminine form of πολύς, which means "much" or "many." The article τῇ is the dative singular feminine form of ὁ, which means "the." The preposition εἰς means "into" or "to." The pronoun ὑμᾶς is the accusative plural of σύ, which means "you." The phrase thus means "with much confidence in you."

The entire verse can be translated as: "But we sent with them our brother, whom we have tested in many ways many times, being diligent. But now much more diligent with great confidence in you."