---
version: 1
---
- **Καὶ ὅτε εἶδον αὐτόν**: The word Καὶ is a conjunction meaning "and." The word ὅτε is a temporal conjunction meaning "when." The verb εἶδον is the first person singular aorist indicative of ὁράω, meaning "I saw." The pronoun αὐτόν is the accusative singular of αὐτός, meaning "him." The phrase thus means "And when I saw him."

- **ἔπεσα πρὸς τοὺς πόδας αὐτοῦ**: The verb ἔπεσα is the first person singular aorist indicative of πίπτω, meaning "I fell." The preposition πρὸς is a preposition that can mean "to" or "toward." The article τοὺς is the accusative plural of ὁ, meaning "the." The noun πόδας is the accusative plural of πούς, meaning "feet." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "I fell at his feet."

- **ὡς νεκρός**: The word ὡς is a comparative conjunction meaning "as" or "like." The adjective νεκρός is the nominative singular masculine of νεκρός, meaning "dead." The phrase thus means "like a dead person."

- **καὶ ἔθηκεν τὴν δεξιὰν αὐτοῦ ἐπʼ ἐμὲ λέγων**: The conjunction καὶ means "and." The verb ἔθηκεν is the third person singular aorist indicative of τίθημι, meaning "he placed" or "he laid." The article τὴν is the accusative singular feminine of ὁ, meaning "the." The adjective δεξιὰν is the accusative singular feminine of δεξιός, meaning "right." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The preposition ἐπʼ is a preposition that can mean "on" or "upon." The pronoun ἐμὲ is the accusative singular of ἐγώ, meaning "me." The verb λέγων is the present participle nominative singular masculine of λέγω, meaning "saying." The phrase thus means "And he placed his right hand on me, saying."

- **Μὴ φοβοῦ**: The word Μὴ is a negation particle meaning "do not" or "not." The verb φοβοῦ is the second person singular present imperative of φοβέομαι, meaning "fear" or "be afraid." The phrase thus means "Do not fear."

- **ἐγώ εἰμι ὁ πρῶτος καὶ ὁ ἔσχατος**: The pronoun ἐγώ is the first person singular pronoun meaning "I." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "I am." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The adjective πρῶτος is the nominative singular masculine of πρῶτος, meaning "first." The conjunction καὶ means "and." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The adjective ἔσχατος is the nominative singular masculine of ἔσχατος, meaning "last." The phrase thus means "I am the first and the last."

- **καὶ**: The conjunction καὶ means "and."

- **ἐγώ εἰμι ὁ ζῶν**: The pronoun ἐγώ is the first person singular pronoun meaning "I." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "I am." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The adjective ζῶν is the nominative singular masculine of ζῶν, meaning "living."

- **καὶ**: The conjunction καὶ means "and."

- **ἐγενόμην νεκρὸς καὶ ἰδοὺ ζῶν εἰμι εἰς τοὺς αἰῶνας τῶν αἰώνων**: The verb ἐγενόμην is the first person singular aorist indicative of γίνομαι, meaning "I became." The adjective νεκρὸς is the nominative singular masculine of νεκρός, meaning "dead." The conjunction καὶ means "and." The interjection ἰδοὺ means "behold" or "look." The verb ζῶν is the nominative singular masculine present participle of ζῶ, meaning "living." The pronoun εἰμι is the first person singular present indicative of εἰμί, meaning "I am." The preposition εἰς is a preposition that can mean "to" or "into." The article τοὺς is the accusative plural of ὁ, meaning "the." The noun αἰῶνας is the accusative plural of αἰών, meaning "ages" or "eternity." The article τῶν is the genitive plural of ὁ, meaning "of the." The noun αἰώνων is the genitive plural of αἰών, meaning "ages" or "eternity." The phrase thus means "And I became dead, and behold, I am living forever and ever."

The literal translation of the verse is: "And when I saw him, I fell at his feet like a dead person. And he placed his right hand on me, saying, 'Do not fear. I am the first and the last, and I am the living one. I became dead, and behold, I am living forever and ever."