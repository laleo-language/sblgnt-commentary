---
version: 1
---
- **ὃς ἐμαρτύρησεν τὸν λόγον τοῦ θεοῦ καὶ τὴν μαρτυρίαν Ἰησοῦ Χριστοῦ**: The word ὃς is the nominative singular masculine of ὅς, which means "who" or "he who." The verb ἐμαρτύρησεν is the third person singular aorist indicative of μαρτυρέω, meaning "he testified" or "he bore witness." The accusative noun phrase τὸν λόγον τοῦ θεοῦ means "the word of God." The conjunction καὶ means "and." The accusative noun phrase τὴν μαρτυρίαν Ἰησοῦ Χριστοῦ means "the testimony of Jesus Christ." 

- **ὅσα εἶδεν**: The relative pronoun ὅσα is the accusative neuter plural of ὅσος, which means "as many as" or "all that." The verb εἶδεν is the third person singular aorist indicative of ὁράω, meaning "he saw." The phrase thus means "all that he saw."

The entire verse can be translated as: "who testified to the word of God and the testimony of Jesus Christ, as many things as he saw."