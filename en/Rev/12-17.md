---
version: 1
---
- **καὶ ὠργίσθη ὁ δράκων**: The conjunction καὶ means "and." The verb ὠργίσθη is the third person singular aorist indicative passive of ὀργίζω, meaning "he was angry." The article ὁ is the nominative singular masculine definite article, meaning "the." The noun δράκων is the nominative singular masculine form of δράκων, which means "dragon." The phrase thus means "and the dragon was angry."

- **ἐπὶ τῇ γυναικί**: The preposition ἐπὶ means "upon" or "on." The article τῇ is the dative singular feminine definite article, meaning "the." The noun γυναικί is the dative singular feminine form of γυνή, which means "woman." The phrase thus means "upon the woman."

- **καὶ ἀπῆλθεν**: The conjunction καὶ means "and." The verb ἀπῆλθεν is the third person singular aorist indicative active of ἀπέρχομαι, meaning "he went away" or "he departed."

- **ποιῆσαι πόλεμον**: The infinitive ποιῆσαι is the aorist infinitive of ποιέω, meaning "to make" or "to do." The noun πόλεμον is the accusative singular masculine form of πόλεμος, which means "war." The phrase thus means "to make war."

- **μετὰ τῶν λοιπῶν τοῦ σπέρματος αὐτῆς**: The preposition μετὰ means "with." The article τῶν is the genitive plural masculine definite article, meaning "the." The adjective λοιπῶν is the genitive plural masculine form of λοιπός, which means "remaining" or "rest." The noun σπέρματος is the genitive singular neuter form of σπέρμα, which means "seed" or "descendants." The pronoun αὐτῆς is the genitive singular feminine pronoun, meaning "her." The phrase thus means "with the rest of her descendants."

- **τῶν τηρούντων τὰς ἐντολὰς τοῦ θεοῦ καὶ ἐχόντων τὴν μαρτυρίαν Ἰησοῦ**: The article τῶν is the genitive plural masculine definite article, meaning "the." The verb τηρούντων is the present participle genitive plural masculine form of τηρέω, meaning "keeping" or "observing." The noun τὰς is the accusative plural feminine definite article, meaning "the." The noun ἐντολὰς is the accusative plural feminine form of ἐντολή, which means "commandments." The noun θεοῦ is the genitive singular masculine form of θεός, which means "God." The conjunction καὶ means "and." The verb ἐχόντων is the present participle genitive plural masculine form of ἔχω, meaning "having." The article τὴν is the accusative singular feminine definite article, meaning "the." The noun μαρτυρίαν is the accusative singular feminine form of μαρτυρία, which means "testimony." The noun Ἰησοῦ is the genitive singular masculine form of Ἰησοῦς, which means "Jesus." The phrase thus means "those who keep the commandments of God and have the testimony of Jesus."

The literal translation of the entire verse is: "And the dragon was angry upon the woman, and he went away to make war with the rest of her descendants, those who keep the commandments of God and have the testimony of Jesus."