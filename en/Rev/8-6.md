---
version: 1
---
- **Καὶ οἱ ἑπτὰ ἄγγελοι**: The word οἱ is the definite article in the nominative plural, meaning "the." The adjective ἑπτὰ is the cardinal number seven. The noun ἄγγελοι is the nominative plural of ἄγγελος, which means "angel." The phrase thus means "the seven angels."

- **οἱ ἔχοντες τὰς ἑπτὰ σάλπιγγας**: The word οἱ is again the definite article in the nominative plural, meaning "the." The verb ἔχοντες is the present participle of ἔχω, meaning "having." The noun σάλπιγγας is the accusative plural of σάλπιγξ, which means "trumpet." The phrase thus means "the ones having the seven trumpets."

- **ἡτοίμασαν**: The verb ἡτοίμασαν is the third person plural aorist indicative active of ἑτοιμάζω, meaning "they prepared."

- **αὑτοὺς**: The pronoun αὑτοὺς is the accusative plural of αὐτός, which means "themselves."

- **ἵνα σαλπίσωσιν**: The conjunction ἵνα introduces a purpose clause, meaning "so that." The verb σαλπίσωσιν is the third person plural aorist subjunctive active of σαλπίζω, meaning "they might blow the trumpet."

Literal Translation: "And the seven angels, the ones having the seven trumpets, prepared themselves so that they might blow the trumpet."