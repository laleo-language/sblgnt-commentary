---
version: 1
---
- **οὕτως ἔχεις**: The word οὕτως is an adverb meaning "in this way" or "thus." The verb ἔχεις is the second person singular present indicative of ἔχω, meaning "you have." The phrase οὕτως ἔχεις can be translated as "you have in this way" or "you have thus."

- **καὶ σὺ κρατοῦντας**: The word καὶ is a coordinating conjunction meaning "and." The pronoun σὺ is the second person singular pronoun meaning "you." The verb κρατοῦντας is the present active participle of κρατέω, meaning "you are holding" or "you are keeping." The phrase καὶ σὺ κρατοῦντας can be translated as "and you, holding" or "and you, keeping."

- **τὴν ⸀διδαχὴν Νικολαϊτῶν**: The article τὴν is the accusative singular feminine definite article, indicating that the noun it modifies is the direct object of the sentence. The noun διδαχὴν is the accusative singular of διδαχή, meaning "teaching" or "doctrine." The genitive plural Νικολαϊτῶν is the genitive form of Νικολαΐτης, meaning "Nicolaitans." The phrase τὴν διδαχὴν Νικολαϊτῶν can be translated as "the teaching of the Nicolaitans" or "the doctrine of the Nicolaitans."

- **ὁμοίως**: The adverb ὁμοίως means "likewise" or "in the same way." It modifies the verb κρατοῦντας and emphasizes that the action of holding or keeping applies to both the previous subject (you) and the teaching of the Nicolaitans. The phrase ὁμοίως can be translated as "likewise" or "in the same way."

The literal translation of the entire verse is: "You have in this way and you, holding the teaching of the Nicolaitans likewise."