---
version: 1
---
- **Καὶ ὁ τρίτος ἐξέχεεν τὴν φιάλην αὐτοῦ**: The phrase ὁ τρίτος is the nominative singular of ὁ τρίτος, meaning "the third." The verb ἐξέχεεν is the third person singular imperfect indicative of ἐκχέω, meaning "he poured out." The noun phrase τὴν φιάλην is the accusative singular of ἡ φιάλη, meaning "the bowl" or "the vial." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "And the third one poured out his bowl."

- **εἰς τοὺς ποταμοὺς καὶ τὰς πηγὰς τῶν ὑδάτων**: The preposition εἰς means "into." The noun phrase τοὺς ποταμοὺς is the accusative plural of ὁ ποταμός, meaning "the rivers." The conjunction καὶ means "and." The noun phrase τὰς πηγὰς is the accusative plural of ἡ πηγή, meaning "the springs." The article τῶν is the genitive plural of ὁ, indicating possession. The noun ὑδάτων is the genitive plural of τὸ ὕδωρ, meaning "of the waters." The phrase thus means "into the rivers and the springs of the waters."

- **καὶ ἐγένετο αἷμα**: The conjunction καὶ means "and." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it became" or "it happened." The noun αἷμα is the accusative singular of τὸ αἷμα, meaning "blood." The phrase thus means "and it became blood."

Literal translation: "And the third one poured out his bowl into the rivers and the springs of the waters, and it became blood."

In this verse, we see the third angel pouring out his bowl into the rivers and springs of water, causing them to become blood.