---
version: 1
---
- **ἀπὸ μακρόθεν**: The preposition ἀπὸ means "from" and is followed by the adverb μακρόθεν, which means "far off" or "at a distance." The phrase thus means "from far off" or "from a distance."

- **ἑστηκότες**: This is the present participle of ἵστημι, which means "to stand." The participle is in the masculine plural nominative case, agreeing with an implied subject. The phrase can be translated as "standing."

- **διὰ τὸν φόβον τοῦ βασανισμοῦ αὐτῆς**: The preposition διὰ means "through" or "because of." The noun φόβον is the accusative singular of φόβος, meaning "fear" or "terror." The genitive τοῦ βασανισμοῦ is the genitive singular of βασανισμός, meaning "torment" or "punishment." The pronoun αὐτῆς is the genitive singular feminine of αὐτός, meaning "her." The phrase can be translated as "because of the fear of her torment."

- **λέγοντες**: This is the present participle of λέγω, which means "to say" or "to speak." The participle is in the masculine plural nominative case, agreeing with the implied subject. The phrase can be translated as "saying."

- **Οὐαὶ οὐαί**: These are interjections expressing grief or lamentation. They are often translated as "woe" or "alas."

- **ἡ πόλις ἡ μεγάλη, Βαβυλὼν ἡ πόλις ἡ ἰσχυρά**: The article ἡ is the feminine singular nominative form, indicating that the following noun is the subject of the sentence. The noun πόλις means "city." The adjective μεγάλη means "great" or "large." The name Βαβυλὼν is the Greek form of Babylon. The adjective ἰσχυρά means "strong" or "mighty." The phrase can be translated as "the great city, Babylon the strong city."

- **ὅτι μιᾷ ὥρᾳ ἦλθεν ἡ κρίσις σου**: The conjunction ὅτι introduces a subordinate clause and means "because" or "that." The numeral μιᾷ is the genitive singular feminine of εἷς, meaning "one." The noun ὥρᾳ is the dative singular of ὥρα, meaning "hour." The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "to come." The noun κρίσις means "judgment" or "decision." The pronoun σου is the genitive singular second person pronoun, meaning "your." The phrase can be translated as "because in one hour came your judgment."

The verse can be translated as: "Standing from far off because of the fear of her torment, saying, 'Woe, woe, the great city, Babylon the strong city, because in one hour came your judgment.'"