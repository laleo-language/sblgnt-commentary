---
version: 1
---
- **Καὶ εἶδον ἄλλο θηρίον**: The word Καὶ is a conjunction meaning "and." The verb εἶδον is the first person singular aorist indicative of ὁράω, meaning "I saw." The word ἄλλο is an adjective meaning "another" or "different." The noun θηρίον means "beast" or "animal." The phrase thus means "And I saw another beast."

- **ἀναβαῖνον ἐκ τῆς γῆς**: The verb ἀναβαῖνον is the present participle of ἀναβαίνω, meaning "ascending" or "coming up." The preposition ἐκ means "from" or "out of." The article τῆς is the genitive singular feminine form of the definite article, indicating "the." The noun γῆς means "earth" or "land." The phrase can be translated as "ascending from the earth."

- **καὶ εἶχεν κέρατα δύο ὅμοια ἀρνίῳ**: The conjunction καὶ means "and." The verb εἶχεν is the third person singular imperfect indicative of ἔχω, meaning "he had." The noun κέρατα means "horns." The adjective δύο means "two." The adjective ὅμοια means "similar" or "like." The definite article ἀρνίῳ is the dative singular neuter form of the definite article, indicating "to the lamb." The phrase can be translated as "And it had two horns like a lamb."

- **καὶ ἐλάλει ὡς δράκων**: The conjunction καὶ means "and." The verb ἐλάλει is the third person singular imperfect indicative of λαλέω, meaning "he spoke" or "he talked." The adverb ὡς means "like" or "as." The noun δράκων means "dragon" or "serpent." The phrase can be translated as "And he spoke like a dragon."

Overall, the verse describes John seeing another beast that was coming up from the earth. This beast had two horns like a lamb and spoke like a dragon.

Literal translation: "And I saw another beast ascending from the earth, and it had two horns like a lamb, and it spoke like a dragon."