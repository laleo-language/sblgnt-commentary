---
version: 1
---
- **ἔξω οἱ κύνες**: The word ἔξω means "outside" or "out." The word οἱ is the definite article in the nominative plural, indicating that it is modifying a plural noun. The noun κύνες means "dogs." The phrase thus means "outside (are) the dogs."

- **καὶ οἱ φάρμακοι**: The word καὶ is a conjunction meaning "and." The word οἱ is the definite article in the nominative plural. The noun φάρμακοι means "sorcerers" or "those who practice magic." The phrase thus means "and the sorcerers."

- **καὶ οἱ πόρνοι**: The word καὶ is a conjunction meaning "and." The word οἱ is the definite article in the nominative plural. The noun πόρνοι means "those who engage in sexual immorality" or "the sexually immoral." The phrase thus means "and the sexually immoral."

- **καὶ οἱ φονεῖς**: The word καὶ is a conjunction meaning "and." The word οἱ is the definite article in the nominative plural. The noun φονεῖς means "murderers." The phrase thus means "and the murderers."

- **καὶ οἱ εἰδωλολάτραι**: The word καὶ is a conjunction meaning "and." The word οἱ is the definite article in the nominative plural. The noun εἰδωλολάτραι means "idolaters" or "those who worship idols." The phrase thus means "and the idolaters."

- **καὶ πᾶς φιλῶν καὶ ποιῶν ψεῦδος**: The word καὶ is a conjunction meaning "and." The adjective πᾶς means "all" or "everyone." The participle φιλῶν is from the verb φιλέω, which means "to love." The participle ποιῶν is from the verb ποιέω, which means "to do" or "to practice." The noun ψεῦδος means "falsehood" or "lying." The phrase thus means "and everyone who loves and practices falsehood."

The literal translation of the entire verse is: "Outside (are) the dogs and the sorcerers and the sexually immoral and the murderers and the idolaters and everyone who loves and practices falsehood."