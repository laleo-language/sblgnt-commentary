---
version: 1
---
- **καὶ ἤκουσα τοῦ θυσιαστηρίου λέγοντος**: The word καὶ is a conjunction meaning "and." The verb ἤκουσα is the first person singular aorist indicative of ἀκούω, meaning "I heard." The genitive noun τοῦ θυσιαστηρίου means "of the altar." The participle λέγοντος is the genitive singular masculine present active participle of λέγω, meaning "saying." The phrase thus means "and I heard the altar saying."

- **Ναί, κύριε, ὁ θεός, ὁ παντοκράτωρ**: The word Ναί is an adverb meaning "yes" or "indeed." The vocative noun Κύριε means "Lord." The noun ὁ θεός means "the God." The noun ὁ παντοκράτωρ means "the Almighty." The phrase can be translated as "Yes, Lord, the God, the Almighty."

- **ἀληθιναὶ καὶ δίκαιαι αἱ κρίσεις σου**: The adjective ἀληθιναὶ means "true." The conjunction καὶ means "and." The adjective δίκαιαι means "just" or "righteous." The feminine noun αἱ κρίσεις means "the judgments." The pronoun σου means "your." The phrase can be translated as "Your judgments are true and just."

The entire verse can be translated as: "And I heard the altar saying, 'Yes, Lord, the God, the Almighty, your judgments are true and just.'"