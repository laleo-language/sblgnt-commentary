---
version: 1
---
- **καὶ οἱ βασιλεῖς τῆς γῆς**: The article οἱ is the plural nominative form, indicating that there are multiple kings. The noun βασιλεῖς is the plural nominative form of βασιλεύς, which means "kings." The prepositional phrase τῆς γῆς means "of the earth." So this phrase translates to "and the kings of the earth."

- **καὶ οἱ μεγιστᾶνες**: The article οἱ is again the plural nominative form, indicating multiple magnates. The noun μεγιστᾶνες is the plural nominative form of μεγιστάνης, which means "magnates" or "great ones." This phrase translates to "and the magnates."

- **καὶ οἱ χιλίαρχοι**: The article οἱ is once again the plural nominative form, indicating multiple chiliarchs. The noun χιλίαρχοι is the plural nominative form of χιλίαρχος, which means "chiliarchs" or "commanders of a thousand." This phrase translates to "and the chiliarchs."

- **καὶ οἱ πλούσιοι**: The article οἱ is again the plural nominative form, indicating multiple rich people. The noun πλούσιοι is the plural nominative form of πλούσιος, which means "rich." This phrase translates to "and the rich."

- **καὶ οἱ ἰσχυροὶ**: The article οἱ is once again the plural nominative form, indicating multiple strong people. The adjective ἰσχυροὶ is the plural nominative form of ἰσχυρός, which means "strong." This phrase translates to "and the strong."

- **καὶ πᾶς δοῦλος καὶ ἐλεύθερος**: The adjective πᾶς is the singular nominative form, meaning "every" or "all." The noun δοῦλος is the singular nominative form of δοῦλος, which means "slave" or "servant." The conjunction καὶ connects δοῦλος with ἐλεύθερος, which is the singular nominative form of ἐλεύθερος, meaning "free." So this phrase translates to "every slave and free person."

- **ἔκρυψαν ἑαυτοὺς εἰς τὰ σπήλαια καὶ εἰς τὰς πέτρας τῶν ὀρέων**: The verb ἔκρυψαν is the third person plural aorist indicative of κρύπτω, which means "to hide." The reflexive pronoun ἑαυτοὺς means "themselves." The prepositional phrase εἰς τὰ σπήλαια means "into the caves." The conjunction καὶ connects εἰς τὰ σπήλαια with εἰς τὰς πέτρας, which means "and into the rocks." The noun πέτρας is the plural accusative form of πέτρα, which means "rock." The genitive phrase τῶν ὀρέων means "of the mountains." So this phrase translates to "they hid themselves in the caves and in the rocks of the mountains."

The literal translation of the entire verse is: "And the kings of the earth, and the magnates, and the chiliarchs, and the rich, and the strong, and every slave and free person, hid themselves in the caves and in the rocks of the mountains."