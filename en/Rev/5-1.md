---
version: 1
---
- **καὶ εἶδον**: The conjunction καὶ means "and." The verb εἶδον is the first person singular aorist indicative of ὁράω, meaning "I saw." The phrase thus means "and I saw."
- **ἐπὶ τὴν δεξιὰν τοῦ καθημένου ἐπὶ τοῦ θρόνου**: The preposition ἐπὶ means "on" or "upon." The article τὴν is the accusative singular feminine form of ὁ, which means "the." The adjective δεξιὰν is the accusative singular feminine form of δεξιός, which means "right." The genitive masculine form of καθημένου is καθημένου, which means "sitting." The preposition ἐπὶ is repeated with the article τοῦ, meaning "on the." The genitive singular neuter form of θρόνου is θρόνου, which means "throne." The phrase thus means "on the right of the one sitting on the throne."
- **βιβλίον γεγραμμένον ἔσωθεν καὶ ὄπισθεν**: The noun βιβλίον is the accusative singular form of βιβλίον, which means "book." The participle γεγραμμένον is the accusative singular neuter form of γράφω, which means "written." The adverbs ἔσωθεν and ὄπισθεν mean "inside" and "back" respectively. The phrase thus means "a book written inside and on the back."
- **κατεσφραγισμένον σφραγῖσιν ἑπτά**: The verb κατεσφραγισμένον is the accusative singular neuter form of κατασφραγίζω, which means "sealed." The noun σφραγῖσιν is the dative plural form of σφραγίς, which means "seal." The numeral ἑπτά means "seven." The phrase thus means "sealed with seven seals."

A literal translation of the verse would be: "And I saw on the right of the one sitting on the throne a book written inside and on the back, sealed with seven seals."