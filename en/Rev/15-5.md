---
version: 1
---
- **Καὶ μετὰ ταῦτα εἶδον**: The word Καὶ is a coordinating conjunction that means "and." The word μετὰ is a preposition that means "after" or "following." The word ταῦτα is a demonstrative pronoun that means "these" or "those." The verb εἶδον is the first person singular aorist indicative of ὁράω, meaning "I saw." The phrase thus means "And after these things I saw."

- **καὶ ἠνοίγη ὁ ναὸς τῆς σκηνῆς τοῦ μαρτυρίου**: The word καὶ is again a coordinating conjunction meaning "and." The verb ἠνοίγη is the third person singular aorist passive indicative of ἀνοίγω, meaning "it was opened." The article ὁ is the definite article meaning "the." The noun ναὸς is the nominative singular of ναός, which means "temple." The genitive τῆς σκηνῆς modifies ναὸς and means "of the tabernacle." The genitive τοῦ μαρτυρίου also modifies ναὸς and means "of the testimony." The phrase can be translated as "And the temple of the tabernacle of the testimony was opened."

- **ἐν τῷ οὐρανῷ**: The preposition ἐν means "in" or "within." The article τῷ is the definite article meaning "the." The noun οὐρανῷ is the dative singular of οὐρανός, which means "heaven." The phrase can be translated as "in the heaven."

The entire verse can be translated as "And after these things I saw, and the temple of the tabernacle of the testimony was opened in the heaven."