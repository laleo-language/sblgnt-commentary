---
version: 1
---
- **καὶ κυκλόθεν τοῦ θρόνου**: The word κυκλόθεν means "around" or "surrounding," and it is an adverb here. The noun θρόνου is in the genitive case, which indicates possession or source, and it means "of the throne." So this phrase means "and around the throne."

- **θρόνοι εἴκοσι τέσσαρες**: The noun θρόνοι is in the nominative case, which indicates the subject of the sentence, and it means "thrones." The adjective εἴκοσι τέσσαρες means "twenty-four." So this phrase means "twenty-four thrones."

- **καὶ ἐπὶ τοὺς θρόνους εἴκοσι τέσσαρας**: The preposition ἐπὶ means "on" or "upon." The article τοὺς is the accusative plural form of the definite article, and it means "the." The noun θρόνους is in the accusative case, which indicates the direct object of the preposition, and it means "thrones." The adjective εἴκοσι τέσσαρας means "twenty-four." So this phrase means "and on the twenty-four thrones."

- **πρεσβυτέρους καθημένους**: The noun πρεσβυτέρους is in the accusative plural form, and it means "elders" or "presbyters." The participle καθημένους is in the accusative plural form and it is derived from the verb κάθημαι, which means "to sit." So this phrase means "sitting elders."

- **περιβεβλημένους ἐν ἱματίοις λευκοῖς**: The participle περιβεβλημένους is in the accusative plural form and it is derived from the verb περιβάλλω, which means "to clothe" or "to put on." The preposition ἐν means "in" or "with." The noun ἱματίοις is in the dative plural form and it means "garments" or "robes." The adjective λευκοῖς means "white." So this phrase means "clothed in white garments."

- **καὶ ἐπὶ τὰς κεφαλὰς αὐτῶν στεφάνους χρυσοῦς**: The preposition ἐπὶ means "on" or "upon." The article τὰς is the accusative plural form of the definite article, and it means "the." The noun κεφαλὰς is in the accusative plural form, which indicates the direct object of the preposition, and it means "heads." The noun στεφάνους is in the accusative plural form and it means "crowns." The adjective χρυσοῦς means "golden." So this phrase means "and on their heads golden crowns."

The literal translation of Revelation 4:4 is: "And around the throne, twenty-four thrones. And on the twenty-four thrones, sitting elders, clothed in white garments, and on their heads golden crowns."