---
version: 1
---
- **καὶ ἤκουσα**: The word καὶ is a conjunction meaning "and." The verb ἤκουσα is the first person singular aorist indicative of ἀκούω, meaning "I heard." The phrase thus means "and I heard."

- **τοῦ ἀγγέλου τῶν ὑδάτων**: The word τοῦ is the genitive singular of the definite article ὁ, meaning "the." The noun ἀγγέλου is the genitive singular of ἄγγελος, meaning "angel." The genitive τῶν ὑδάτων is a genitive of possession, indicating that the angel has a connection to the waters. The phrase thus means "of the angel of the waters."

- **λέγοντος**: The word λέγοντος is the genitive singular present participle of λέγω, meaning "saying." The participle is in agreement with the noun ἀγγέλου and indicates that the angel is currently speaking. The phrase thus means "saying."

- **Δίκαιος εἶ, ὁ ὢν καὶ ὁ ἦν, ὁ ὅσιος**: The word Δίκαιος is the nominative singular of δίκαιος, meaning "righteous." The pronoun εἶ is the second person singular present indicative of εἰμί, meaning "you are." The article ὁ is the nominative singular of the definite article ὁ, meaning "the." The participles ὢν and ἦν are the present and imperfect participles of εἰμί, meaning "being" and "was." The adjective ὅσιος is the nominative singular of ὅσιος, meaning "holy." The phrase thus means "You are righteous, the one who is and was, the holy one."

- **ὅτι ταῦτα ἔκρινας**: The conjunction ὅτι introduces a statement and can be translated as "because" or "that." The pronoun ταῦτα is the accusative plural of οὗτος, meaning "these" or "this." The verb ἔκρινας is the second person singular aorist indicative of κρίνω, meaning "you judged." The phrase thus means "because you judged these."

The literal translation of the entire verse is: "And I heard the angel of the waters saying, 'You are righteous, the one who is and was, the holy one, because you judged these.'"