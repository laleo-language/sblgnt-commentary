---
version: 1
---
- **τῷ ἀγγέλῳ τῆς ἐν Σμύρνῃ ἐκκλησίας**: The word τῷ is the dative singular of the definite article ὁ, which means "the." The noun ἀγγέλῳ is the dative singular of ἄγγελος, meaning "angel." The genitive phrase τῆς ἐν Σμύρνῃ ἐκκλησίας modifies ἀγγέλῳ and means "of the church in Smyrna." 

- **γράψον**: The verb γράψον is the second person singular aorist imperative of γράφω, meaning "write." The command is addressed to the speaker, which suggests that the speaker is the one who should write to the angel of the church in Smyrna.

- **Τάδε λέγει ὁ πρῶτος καὶ ὁ ἔσχατος**: The phrase Τάδε λέγει means "Thus says." The article ὁ is the nominative singular of ὁ, which means "the." The adjective πρῶτος is the nominative singular of πρῶτος, meaning "first." The conjunction καὶ means "and." The adjective ἔσχατος is the nominative singular of ἔσχατος, meaning "last." This phrase describes the subject of the following verb.

- **ὃς ἐγένετο νεκρὸς καὶ ἔζησεν**: The relative pronoun ὃς is the nominative singular of ὅς, which means "who" or "which." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "he became." The adjective νεκρὸς is the nominative singular of νεκρός, meaning "dead." The conjunction καὶ means "and." The verb ἔζησεν is the third person singular aorist indicative of ζάω, meaning "he lived." This phrase describes the subject of the previous verb.

The literal translation of the verse is: "And write to the angel of the church in Smyrna: Thus says the first and the last, who became dead and lived."