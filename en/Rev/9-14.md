---
version: 1
---
- **λέγοντα τῷ ἕκτῳ ἀγγέλῳ**: The participle λέγοντα is the accusative singular masculine form of λέγω, meaning "saying" or "who says." It is modifying the noun ἀγγέλῳ, which is the dative singular masculine form of ἄγγελος, meaning "angel." The phrase thus means "saying to the sixth angel."

- **ὁ ἔχων τὴν σάλπιγγα**: The article ὁ is the masculine singular nominative form meaning "the." The participle ἔχων is the masculine singular nominative form of ἔχω, meaning "having." It is modifying the noun σάλπιγγα, which is the accusative singular feminine form of σάλπιγξ, meaning "trumpet." The phrase thus means "the one having the trumpet."

- **Λῦσον τοὺς τέσσαρας ἀγγέλους**: The verb Λῦσον is the second person singular aorist imperative of λύω, meaning "release" or "unbind." The article τοὺς is the masculine plural accusative form meaning "the." The adjective τέσσαρας is the feminine plural accusative form of τέσσαρες, meaning "four." The noun ἀγγέλους is the masculine plural accusative form of ἄγγελος, meaning "angels." The phrase thus means "Release the four angels."

- **τοὺς δεδεμένους ἐπὶ τῷ ποταμῷ τῷ μεγάλῳ Εὐφράτῃ**: The article τοὺς is the masculine plural accusative form meaning "the." The participle δεδεμένους is the masculine plural accusative form of δέω, meaning "bound" or "tied." It is modifying the noun ἀγγέλους. The preposition ἐπὶ means "on" or "upon." The article τῷ is the masculine singular dative form meaning "the." The noun ποταμῷ is the masculine singular dative form of ποταμός, meaning "river." The adjective μεγάλῳ is the masculine singular dative form of μέγας, meaning "great" or "large." The noun Εὐφράτῃ is the masculine singular dative form of Εὐφράτης, referring to the Euphrates River. The phrase thus means "the bound angels on the great river Euphrates."

Literal translation: "saying to the sixth angel, the one having the trumpet: Release the four angels, the bound ones on the great river Euphrates."

In this verse, John sees a vision of an angel speaking to another angel. The second angel is described as having a trumpet. The first angel instructs the second angel to release four other angels who are bound near the Euphrates River.