---
version: 1
---
- **καὶ ἔκραξεν φωνῇ μεγάλῃ**: The conjunction καὶ means "and." The verb ἔκραξεν is the third person singular aorist indicative of κράζω, meaning "he cried out." The noun φωνῇ is the dative singular of φωνή, which means "voice" or "sound." The adjective μεγάλῃ is the dative singular feminine of μέγας, which means "great" or "loud." So this phrase means "and he cried out with a loud voice."

- **ὥσπερ λέων μυκᾶται**: The phrase ὥσπερ λέων means "like a lion." The verb μυκᾶται is the third person singular present middle or passive indicative of μυκάομαι, which means "he roars" or "he bellows." So this phrase means "he roared like a lion."

- **καὶ ὅτε ἔκραξεν**: The conjunction καὶ means "and." The conjunction ὅτε means "when" or "as." The verb ἔκραξεν is the third person singular aorist indicative of κράζω, meaning "he cried out." So this phrase means "and when he cried out."

- **ἐλάλησαν αἱ ἑπτὰ βρονταὶ τὰς ἑαυτῶν φωνάς**: The verb ἐλάλησαν is the third person plural aorist indicative of λαλέω, meaning "they spoke" or "they uttered." The article αἱ is the nominative plural feminine definite article, meaning "the." The numeral ἑπτὰ is the nominative plural of ἑπτά, meaning "seven." The noun βρονταὶ is the nominative plural of βροντή, which means "thunder." The article τὰς is the accusative plural feminine definite article, meaning "the." The reflexive pronoun ἑαυτῶν means "their own." The noun φωνάς is the accusative plural of φωνή, which means "voice" or "sound." So this phrase means "the seven thunders uttered their own voices."

The entire verse translates to: "And he cried out with a loud voice, like a lion roars. And when he cried out, the seven thunders uttered their own voices."