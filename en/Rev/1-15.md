---
version: 1
---
- **καὶ οἱ πόδες αὐτοῦ**: The word καὶ is a conjunction meaning "and." The word οἱ is the nominative plural definite article, meaning "the." The word πόδες is the nominative plural of πούς, meaning "feet." The word αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "and his feet."

- **ὅμοιοι χαλκολιβάνῳ**: The word ὅμοιοι is an adjective in the nominative plural, meaning "like" or "similar." The word χαλκολιβάνῳ is the dative singular of χαλκολίβανος, a compound word meaning "fine brass." The phrase thus means "like fine brass."

- **ὡς ἐν καμίνῳ πεπυρωμένης**: The word ὡς is a preposition meaning "like" or "as." The word ἐν is a preposition meaning "in." The word καμίνῳ is the dative singular of κάμινος, meaning "furnace." The participle πεπυρωμένης is the nominative singular feminine of πυρόω, meaning "burned." The phrase thus means "like in a burned furnace."

- **καὶ ἡ φωνὴ αὐτοῦ**: The word καὶ is a conjunction meaning "and." The word ἡ is the nominative singular definite article, meaning "the." The word φωνὴ is the nominative singular of φωνή, meaning "voice." The word αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "and his voice."

- **ὡς φωνὴ ὑδάτων πολλῶν**: The word ὡς is a preposition meaning "like" or "as." The word φωνὴ is the nominative singular of φωνή, meaning "voice." The word ὑδάτων is the genitive plural of ὕδωρ, meaning "water." The adjective πολλῶν is the genitive plural masculine/feminine of πολύς, meaning "many." The phrase thus means "like the voice of many waters."

The literal translation of the verse is: "And his feet like fine brass, as in a burned furnace, and his voice like the sound of many waters."