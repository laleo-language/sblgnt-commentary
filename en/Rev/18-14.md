---
version: 1
---
- **καὶ ἡ ὀπώρα σου**: The word καὶ is a conjunction meaning "and." The noun ὀπώρα is the nominative singular of ὀπώρα, which means "harvest" or "fruit." The pronoun σου is the genitive singular of σύ, which means "your." The phrase thus means "and your harvest."

- **τῆς ἐπιθυμίας τῆς ψυχῆς**: The article τῆς is the genitive singular feminine of ὁ, which means "the." The noun ἐπιθυμίας is the genitive singular of ἐπιθυμία, which means "desire" or "longing." The article τῆς is the genitive singular feminine of ὁ. The noun ψυχῆς is the genitive singular of ψυχή, which means "soul" or "life." The phrase thus means "of the desire of the soul."

- **ἀπῆλθεν ἀπὸ σοῦ**: The verb ἀπῆλθεν is the third person singular aorist indicative of ἀπέρχομαι, which means "I went away" or "I departed." The preposition ἀπὸ means "from." The pronoun σοῦ is the genitive singular of σύ, which means "you." The phrase thus means "it went away from you."

- **καὶ πάντα τὰ λιπαρὰ καὶ τὰ λαμπρὰ**: The conjunction καὶ means "and." The adjective πάντα is the accusative plural neuter of πᾶς, which means "all" or "every." The article τὰ is the nominative/accusative plural neuter of ὁ. The adjective λιπαρὰ is the nominative/accusative plural neuter of λιπαρός, which means "fat" or "rich." The conjunction καὶ means "and." The article τὰ is the nominative/accusative plural neuter of ὁ. The adjective λαμπρὰ is the nominative/accusative plural neuter of λαμπρός, which means "bright" or "splendid." The phrase thus means "and all the fat and the splendid."

- **ἀπώλετο ἀπὸ σοῦ**: The verb ἀπώλετο is the third person singular aorist middle indicative of ἀπόλλυμι, which means "I was destroyed" or "I perished." The preposition ἀπὸ means "from." The pronoun σοῦ is the genitive singular of σύ, which means "you." The phrase thus means "it was destroyed from you."

- **καὶ οὐκέτι οὐ μὴ αὐτὰ εὑρήσουσιν**: The conjunction καὶ means "and." The adverb οὐκέτι means "no longer" or "no more." The negative particle οὐ means "not." The particle μὴ is used with the indicative verb εὑρήσουσιν to indicate a negative statement. The pronoun αὐτὰ is the accusative plural neuter of αὐτός, which means "they" or "them." The verb εὑρήσουσιν is the third person plural future indicative of εὑρίσκω, which means "they will find." The phrase thus means "and they will no longer find them."

The literal translation of Revelation 18:14 is: "And your harvest of the desire of the soul went away from you, and all the fat and the splendid were destroyed from you, and they will no longer find them."