---
version: 1
---
- **καὶ ἐξαλείψει πᾶν δάκρυον**: The word καὶ is a conjunction that means "and." The verb ἐξαλείψει is the third person singular future indicative of ἐξαλείφω, which means "to wipe out" or "to eliminate." The noun πᾶν is the accusative singular of πᾶς, meaning "every" or "all." And the noun δάκρυον is the accusative singular of δάκρυ, which means "tear." The phrase thus means "and he will wipe away every tear."

- **ἐκ τῶν ὀφθαλμῶν αὐτῶν**: The preposition ἐκ means "from" or "out of." The article τῶν is the genitive plural of ὁ, meaning "the." The noun ὀφθαλμῶν is the genitive plural of ὀφθαλμός, which means "eye." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "from their eyes."

- **καὶ ὁ θάνατος οὐκ ἔσται ἔτι**: The conjunction καὶ means "and." The article ὁ is the nominative singular of ὁ, meaning "the." The noun θάνατος is the nominative singular of θάνατος, which means "death." The verb ἔσται is the third person singular future indicative of εἰμί, meaning "to be." The adverb ἔτι means "anymore" or "still." The phrase thus means "and death will no longer be."

- **οὔτε πένθος οὔτε κραυγὴ οὔτε πόνος οὐκ ἔσται**: The conjunction οὔτε means "neither" or "nor." The noun πένθος is the nominative singular of πένθος, meaning "mourning" or "grief." The noun κραυγὴ is the nominative singular of κραυγή, which means "cry" or "shout." The noun πόνος is the nominative singular of πόνος, meaning "pain" or "suffering." The verb ἔσται is the third person singular future indicative of εἰμί, meaning "to be." The phrase thus means "neither mourning nor crying nor pain will be."

- **τὰ πρῶτα ἀπῆλθαν**: The article τὰ is the nominative plural of ὁ, meaning "the." The adjective πρῶτα is the nominative plural of πρῶτος, meaning "first." The verb ἀπῆλθαν is the third person plural aorist indicative of ἀπέρχομαι, meaning "to go away" or "to depart." The phrase thus means "the first things have gone away."

Literal translation: "And he will wipe away every tear from their eyes, and death will no longer be, neither mourning nor crying nor pain will be, the first things have gone away."

In this verse, John describes a vision of the future where God will bring an end to all sorrow and suffering. He says that God will wipe away every tear from the eyes of his people, and death will no longer exist. There will be no more mourning, crying, or pain. All of these things will be eliminated, and the first things will have passed away. This vision gives hope and comfort to those who believe in God's promise of a new and perfect creation.