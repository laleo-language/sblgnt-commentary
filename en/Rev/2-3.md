---
version: 1
---
- **καὶ ὑπομονὴν ἔχεις**: The word καὶ is a conjunction meaning "and." The noun ὑπομονὴν is the accusative singular of ὑπομονή, which means "endurance" or "perseverance." The verb ἔχεις is the second person singular present indicative of ἔχω, meaning "you have." The phrase thus means "and you have endurance."

- **καὶ ἐβάστασας διὰ τὸ ὄνομά μου**: The word καὶ is a conjunction meaning "and." The verb ἐβάστασας is the second person singular aorist indicative of βαστάζω, meaning "you have borne" or "you have carried." The preposition διὰ means "through" or "because of." The article τὸ is the accusative singular neuter of ὁ, which means "the." The noun ὄνομά is the accusative singular of ὄνομα, which means "name." The pronoun μου is the genitive singular of ἐγώ, meaning "my" or "mine." The phrase thus means "and you have borne because of my name."

- **καὶ οὐ κεκοπίακες**: The word καὶ is a conjunction meaning "and." The adverb οὐ is a negation meaning "not." The verb κεκοπίακες is the second person singular perfect indicative of κοπιάω, meaning "you have labored" or "you have toiled." The phrase thus means "and you have not labored."

The sentence as a whole means "And you have endurance, and you have borne because of my name, and you have not labored."