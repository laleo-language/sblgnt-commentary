---
version: 1
---
- **ὅτι ἀληθιναὶ καὶ δίκαιαι αἱ κρίσεις αὐτοῦ**: The phrase ὅτι ἀληθιναὶ καὶ δίκαιαι αἱ κρίσεις αὐτοῦ consists of a relative pronoun ὅτι, meaning "because," followed by the adjectives ἀληθιναὶ (true) and δίκαιαι (just), modifying the noun κρίσεις (judgments), and the possessive pronoun αὐτοῦ (his). The phrase means "because his judgments are true and just."

- **ὅτι ἔκρινεν τὴν πόρνην τὴν μεγάλην**: The phrase ὅτι ἔκρινεν τὴν πόρνην τὴν μεγάλην consists of a relative pronoun ὅτι, meaning "because," followed by the verb ἔκρινεν (judged) in the third person singular aorist indicative, and the noun πόρνην (prostitute) and its modifier τὴν μεγάλην (the great). The phrase means "because he judged the great prostitute."

- **ἥτις ⸀ἔφθειρεν τὴν γῆν ἐν τῇ πορνείᾳ αὐτῆς**: The phrase ἥτις ἔφθειρεν τὴν γῆν ἐν τῇ πορνείᾳ αὐτῆς consists of a relative pronoun ἥτις (who/which) introducing a subordinate clause modifying the noun πόρνην (prostitute), followed by the verb ἔφθειρεν (corrupted) in the third person singular aorist indicative, and the noun γῆν (earth), and its modifier τῇ πορνείᾳ αὐτῆς (in her prostitution). The phrase means "who corrupted the earth with her prostitution."

- **καὶ ἐξεδίκησεν τὸ αἷμα τῶν δούλων αὐτοῦ ἐκ χειρὸς αὐτῆς**: The phrase καὶ ἐξεδίκησεν τὸ αἷμα τῶν δούλων αὐτοῦ ἐκ χειρὸς αὐτῆς consists of the conjunction καὶ (and), followed by the verb ἐξεδίκησεν (avenged) in the third person singular aorist indicative, the noun αἷμα (blood), the genitive plural τῶν δούλων (of the servants), the possessive pronoun αὐτοῦ (his), the preposition ἐκ (from), and the noun χειρὸς (hand) and its modifier αὐτῆς (her). The phrase means "and he avenged the blood of his servants from her hand."

The literal translation of the verse is: "Because his judgments are true and just, because he judged the great prostitute who corrupted the earth with her prostitution, and he avenged the blood of his servants from her hand."