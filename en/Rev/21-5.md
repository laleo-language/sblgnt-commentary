---
version: 1
---
- **Καὶ εἶπεν ὁ καθήμενος ἐπὶ τῷ θρόνῳ**: The word Καὶ is a conjunction meaning "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The article ὁ is the masculine nominative singular form of ὁ, which means "the." The participle καθήμενος is the masculine nominative singular form of καθήμαι, meaning "sitting." The preposition ἐπὶ means "on" and takes the dative case. The article τῷ is the masculine dative singular form of ὁ. The noun θρόνῳ is the masculine dative singular form of θρόνος, meaning "throne." The phrase thus means "And the one sitting on the throne said."

- **Ἰδοὺ καινὰ ποιῶ πάντα**: The interjection Ἰδοὺ means "behold" or "look." The adjective καινὰ is the accusative neuter plural form of καινός, meaning "new." The verb ποιῶ is the first person singular present indicative of ποιέω, meaning "I make" or "I do." The noun πάντα is the accusative neuter plural form of πᾶς, meaning "all" or "everything." The phrase thus means "Behold, I am making all things new."

- **καὶ λέγει**: The conjunction καὶ means "and." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The phrase thus means "And he says."

- **Γράψον**: The verb Γράψον is the second person singular aorist imperative of γράφω, meaning "write." The phrase thus means "Write."

- **ὅτι οὗτοι οἱ λόγοι πιστοὶ καὶ ἀληθινοί εἰσιν**: The conjunction ὅτι means "that." The demonstrative pronoun οὗτοι is the nominative masculine plural form of οὗτος, meaning "these." The article οἱ is the masculine nominative plural form of ὁ. The noun λόγοι is the masculine nominative plural form of λόγος, meaning "words." The adjective πιστοὶ is the nominative masculine plural form of πιστός, meaning "faithful" or "trustworthy." The conjunction καὶ means "and." The adjective ἀληθινοί is the nominative masculine plural form of ἀληθινός, meaning "true" or "genuine." The verb εἰσιν is the third person plural present indicative of εἰμί, meaning "they are." The phrase thus means "that these words are faithful and true."

The literal translation of the verse is: "And the one sitting on the throne said: Behold, I am making all things new. And he says: Write, that these words are faithful and true."