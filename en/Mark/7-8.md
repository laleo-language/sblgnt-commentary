---
version: 1
---
- **ἀφέντες τὴν ἐντολὴν τοῦ θεοῦ**: The word ἀφέντες is the nominative plural participle of ἀφίημι, meaning "letting go" or "neglecting." The article τὴν is the accusative singular of ὁ, meaning "the." The noun ἐντολή is the accusative singular of ἐντολή, which means "commandment" or "instruction." The genitive τοῦ θεοῦ means "of God." The phrase can be translated as "neglecting the commandment of God."

- **κρατεῖτε τὴν παράδοσιν τῶν ἀνθρώπων**: The verb κρατεῖτε is the second person plural present indicative of κρατέω, meaning "you hold" or "you keep." The article τὴν is the accusative singular of ὁ, meaning "the." The noun παράδοσις is the accusative singular of παράδοσις, which means "tradition" or "teaching." The genitive τῶν ἀνθρώπων means "of men" or "of humans." The phrase can be translated as "you hold the tradition of men."

The literal translation of Mark 7:8 is: "Neglecting the commandment of God, you hold the tradition of men."