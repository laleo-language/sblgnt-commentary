---
version: 1
---
- **δεδώκει δὲ**: The verb δεδώκει is the third person singular imperfect indicative of δίδωμι, meaning "he was giving" or "he had given." The particle δὲ is a conjunction that can mean "but" or "and" and is often used to connect clauses.

- **ὁ παραδιδοὺς αὐτὸν**: The article ὁ is the nominative singular masculine definite article, meaning "the." The participle παραδιδοὺς is the nominative singular masculine present active participle of παραδίδωμι, meaning "the one betraying." The pronoun αὐτὸν is the accusative singular masculine pronoun, meaning "him." So the phrase means "the one betraying him."

- **σύσσημον αὐτοῖς λέγων**: The noun σύσσημον is the accusative singular neuter of σύσσημος, meaning "a sign" or "a signal." The pronoun αὐτοῖς is the dative plural masculine pronoun, meaning "to them." The verb λέγων is the present active participle nominative singular masculine of λέγω, meaning "saying." So the phrase means "saying a sign to them."

- **Ὃν ἂν φιλήσω αὐτός ἐστιν**: The pronoun Ὃν is the accusative singular masculine pronoun, meaning "whom." The verb φιλήσω is the first person singular future indicative of φιλέω, meaning "I will kiss." The pronoun αὐτός is the nominative singular masculine pronoun, meaning "he." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." So the phrase means "whom I will kiss, he is."

- **κρατήσατε αὐτὸν καὶ ἀπάγετε ἀσφαλῶς**: The verb κρατήσατε is the second person plural aorist imperative of κρατέω, meaning "you seize" or "you arrest." The pronoun αὐτὸν is the accusative singular masculine pronoun, meaning "him." The verb ἀπάγετε is the second person plural present imperative of ἀπάγω, meaning "you lead away." The adverb ἀσφαλῶς means "safely" or "securely." So the phrase means "seize him and lead him away safely."

The literal translation of the entire verse is: "But the one betraying him had given them a sign, saying, 'Whom I will kiss, he is; seize him and lead him away safely.'"