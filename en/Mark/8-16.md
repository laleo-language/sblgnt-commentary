---
version: 1
---
- **καὶ διελογίζοντο**: The word καὶ is a conjunction meaning "and." The verb διελογίζοντο is the third person plural imperfect indicative middle of διαλογίζομαι, which means "they were reasoning" or "they were discussing." The subject of the verb is not explicitly stated, but it can be inferred to be the disciples based on the context.

- **πρὸς ἀλλήλους**: The preposition πρὸς means "to" or "towards." The noun ἀλλήλους is the accusative plural of ἀλλήλων, which means "one another" or "each other." So together, this phrase means "to one another" or "among themselves."

- **ὅτι ἄρτους οὐκ ἔχουσιν**: The conjunction ὅτι introduces a subordinate clause and can be translated as "that." The noun ἄρτους is the accusative plural of ἄρτος, which means "loaves of bread." The verb ἔχουσιν is the third person plural present indicative active of ἔχω, which means "they have." So this phrase means "that they do not have loaves of bread."

The literal translation of the entire verse would be: "And they were reasoning among themselves that they do not have loaves of bread."