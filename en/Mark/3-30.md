---
version: 1
---
- **ὅτι**: This is a conjunction meaning "because" or "that." It introduces a statement that provides the reason or explanation for something.
- **ἔλεγον**: This is the third person plural imperfect indicative of the verb λέγω, meaning "they were saying" or "they were saying." The imperfect tense indicates an ongoing or repeated action in the past.
- **Πνεῦμα**: This is the nominative singular of the noun πνεῦμα, meaning "spirit." In this context, it refers to an evil or unclean spirit.
- **ἀκάθαρτον**: This is the accusative singular of the adjective ἀκάθαρτος, meaning "unclean" or "impure." It describes the nature of the spirit.
- **ἔχει**: This is the third person singular present indicative of the verb ἔχω, meaning "he/she/it has" or "he/she/it possesses." It indicates the state or condition of the spirit.

Literal translation: "because they were saying, 'He has an unclean spirit.'"

This verse is part of a larger narrative where some people were accusing Jesus of having an unclean spirit. The phrase in question explains what they were saying about Jesus.