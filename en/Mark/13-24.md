---
version: 1
---
- **Ἀλλὰ**: The word Ἀλλὰ means "but" or "however."

- **ἐν ἐκείναις ταῖς ἡμέραις**: The preposition ἐν means "in." The demonstrative pronoun ἐκείναις means "those" or "those ones." The article ταῖς is the feminine dative plural form of the definite article ὁ, which means "the." The noun ἡμέραις is the feminine dative plural form of ἡμέρα, which means "day." The phrase thus means "in those days."

- **μετὰ τὴν θλῖψιν ἐκείνην**: The preposition μετὰ means "after" or "following." The article τὴν is the feminine accusative singular form of the definite article ὁ. The noun θλῖψιν is the accusative singular form of θλῖψις, which means "tribulation" or "affliction." The demonstrative pronoun ἐκείνην means "that" or "that one." The phrase thus means "after that tribulation."

- **ὁ ἥλιος σκοτισθήσεται**: The article ὁ is the masculine nominative singular form of the definite article ὁ. The noun ἥλιος is the masculine nominative singular form of ἥλιος, which means "sun." The verb σκοτισθήσεται is the third person singular future passive indicative form of σκοτίζω, which means "it will be darkened." The phrase thus means "the sun will be darkened."

- **καὶ ἡ σελήνη οὐ δώσει τὸ φέγγος αὐτῆς**: The conjunction καὶ means "and." The article ἡ is the feminine nominative singular form of the definite article ὁ. The noun σελήνη is the feminine nominative singular form of σελήνη, which means "moon." The verb δώσει is the third person singular future active indicative form of δίδωμι, which means "it will give." The article τὸ is the neuter accusative singular form of the definite article ὁ. The noun φέγγος is the neuter accusative singular form of φέγγος, which means "brightness" or "light." The pronoun αὐτῆς is the genitive singular form of αὐτός, which means "its." The phrase thus means "and the moon will not give its brightness."

In this verse, Jesus is speaking about the signs that will accompany the end times. He states that in those days, after the tribulation, the sun will be darkened and the moon will not give its brightness. This imagery is symbolic and conveys the idea of cosmic disturbances and the dramatic nature of the events that will occur.