---
version: 1
---
- **ὁ δὲ ἀποκριθεὶς αὐτοῖς λέγει**: The definite article ὁ is the nominative singular masculine form of ὁ, meaning "the." The conjunction δὲ is used here to indicate a transition or contrast, and can be translated as "and" or "but." The participle ἀποκριθεὶς is the nominative singular masculine form of ἀποκρίνομαι, meaning "having answered" or "responding." The verb λέγει is the third person singular present indicative form of λέγω, meaning "he says" or "he is saying." The phrase thus means "and having answered them, he says."

- **Ὦ γενεὰ ἄπιστος**: The interjection Ὦ is used to express strong emotion or to call attention to something, and can be translated as "O" or "Oh." The adjective γενεὰ is the vocative singular form of γενεά, meaning "generation." The adjective ἄπιστος is the vocative singular masculine form of ἄπιστος, meaning "unbelieving" or "faithless." The phrase thus means "O unbelieving generation."

- **ἕως πότε πρὸς ὑμᾶς ἔσομαι**: The adverb ἕως means "until" or "how long." The interrogative pronoun πότε means "when" or "how long." The preposition πρὸς means "to" or "toward." The personal pronoun ὑμᾶς is the accusative plural form of ὑμεῖς, meaning "you." The verb ἔσομαι is the first person singular future indicative form of εἰμί, meaning "I will be." The phrase thus means "how long will I be with you?"

- **ἕως πότε ἀνέξομαι ὑμῶν**: The adverb ἕως means "until" or "how long." The interrogative pronoun πότε means "when" or "how long." The verb ἀνέξομαι is the first person singular future middle indicative form of ἀνέχομαι, meaning "I will endure" or "I will bear." The personal pronoun ὑμῶν is the genitive plural form of ὑμεῖς, meaning "you." The phrase thus means "how long will I endure from you?"

- **φέρετε αὐτὸν πρός με**: The imperative verb φέρετε is the second person plural present imperative form of φέρω, meaning "bring." The personal pronoun αὐτὸν is the accusative singular masculine form of αὐτός, meaning "him." The preposition πρός means "to" or "toward." The personal pronoun με is the accusative singular form of ἐγώ, meaning "me." The phrase thus means "bring him to me."

The literal translation of Mark 9:19 is: "And having answered them, he says, 'O unbelieving generation, how long will I be with you? How long will I endure from you? Bring him to me.'"