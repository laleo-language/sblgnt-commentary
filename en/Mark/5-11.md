---
version: 1
---
- **ἦν δὲ**: The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he/she/it was." The particle δὲ is a conjunction that can be translated as "but" or "and." Together, the phrase means "but there was."
- **ἐκεῖ**: This is an adverb meaning "there."
- **πρὸς τῷ ὄρει**: The preposition πρὸς means "to" or "towards." The definite article τῷ is the dative singular of ὁ, meaning "the." The noun ὄρος means "mountain." Together, the phrase means "to the mountain."
- **ἀγέλη χοίρων μεγάλη βοσκομένη**: The noun ἀγέλη means "herd" or "flock." The adjective μεγάλη means "great" or "large." The participle βοσκομένη is the present middle/passive participle of βόσκω, meaning "being grazed" or "being pastured." Together, the phrase means "a large herd of pigs being pastured."

Literal translation: "But there was a large herd of pigs being pastured on the mountain."