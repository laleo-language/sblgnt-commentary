---
version: 1
---
- **καὶ ἀποστέλλει δύο τῶν μαθητῶν αὐτοῦ**: The conjunction καὶ means "and" and connects the phrase to the previous one. The verb ἀποστέλλει is the third person singular present indicative of ἀποστέλλω, which means "he sends." The number δύο is the accusative plural of δύο, meaning "two." The preposition τῶν means "of" and the noun μαθητῶν is the genitive plural of μαθητής, which means "disciples" or "students." The pronoun αὐτοῦ means "his." The phrase thus means "And he sends two of his disciples."

- **καὶ λέγει αὐτοῖς**: The conjunction καὶ means "and" and connects the phrase to the previous one. The verb λέγει is the third person singular present indicative of λέγω, which means "he says." The pronoun αὐτοῖς means "to them." The phrase means "And he says to them."

- **Ὑπάγετε εἰς τὴν πόλιν**: The verb Ὑπάγετε is the second person plural present imperative of ὑπάγω, which means "you go" or "you go away." The preposition εἰς means "to" and the article τὴν is the accusative singular feminine definite article, meaning "the." The noun πόλιν is the accusative singular feminine of πόλις, which means "city." The phrase means "Go to the city."

- **καὶ ἀπαντήσει ὑμῖν ἄνθρωπος κεράμιον ὕδατος βαστάζων**: The conjunction καὶ means "and" and connects the phrase to the previous one. The verb ἀπαντήσει is the third person singular future indicative of ἀπαντάω, which means "he will meet" or "he will encounter." The pronoun ὑμῖν means "you." The noun ἄνθρωπος is the nominative singular masculine of ἄνθρωπος, which means "man" or "person." The noun κεράμιον is the accusative singular neuter of κεράμιον, which means "jar" or "pitcher." The genitive absolute construction ὕδατος βαστάζων consists of the genitive noun ὕδατος, meaning "of water," and the present participle βαστάζων, meaning "carrying." The phrase means "And a man carrying a jar of water will meet you."

- **ἀκολουθήσατε αὐτῷ**: The verb ἀκολουθήσατε is the second person plural aorist imperative of ἀκολουθέω, which means "you follow." The pronoun αὐτῷ means "him." The phrase means "Follow him."

**Literal translation of the verse**: And he sends two of his disciples and says to them, "Go to the city, and a man carrying a jar of water will meet you. Follow him."