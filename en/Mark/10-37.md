---
version: 1
---
- **οἱ δὲ εἶπαν αὐτῷ**: The phrase οἱ δὲ is composed of the article οἱ, meaning "the," and the conjunction δὲ, meaning "but" or "and." It is used here to introduce a new topic or to transition to a new speaker. The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "but they said to him."

- **Δὸς ἡμῖν**: The verb Δὸς is the second person singular present imperative of δίδωμι, meaning "give." The pronoun ἡμῖν is the dative plural of ἐγώ, meaning "us." The phrase thus means "Give us."

- **ἵνα εἷς ⸂σου ἐκ δεξιῶν⸃ καὶ εἷς ἐξ ⸀ἀριστερῶν**: The conjunction ἵνα introduces a purpose clause and can be translated as "so that" or "in order to." The adjective εἷς is the nominative masculine singular form of εἷς, meaning "one." The pronoun σου is the genitive singular of σύ, meaning "your." The prepositional phrase ἐκ δεξιῶν consists of the preposition ἐκ, meaning "from," and the accusative plural of δεξιός, meaning "right." The prepositional phrase ἐξ ἀριστερῶν consists of the preposition ἐξ, meaning "from," and the genitive plural of ἀριστερός, meaning "left." The phrase thus means "so that one of us may be at your right and one at your left."

- **καθίσωμεν ἐν τῇ δόξῃ σου**: The verb καθίσωμεν is the first person plural aorist subjunctive of καθίζω, meaning "we may sit." The preposition ἐν means "in" or "among." The article τῇ is the dative singular feminine form of ὁ, meaning "the." The noun δόξῃ is the dative singular feminine of δόξα, meaning "glory." The pronoun σου is the genitive singular of σύ, meaning "your." The phrase thus means "let us sit in your glory."

- **The literal translation of the entire verse**: But they said to him, "Give us, so that one of us may be at your right and one at your left, and let us sit in your glory."