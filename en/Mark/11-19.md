---
version: 1
---
- **Καὶ ὅταν ὀψὲ ἐγένετο**: The conjunction καὶ means "and." The word ὅταν is a subordinating conjunction that introduces a temporal clause and means "when." The adverb ὀψὲ means "late" or "in the evening." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "he/she/it happened" or "it became." The phrase thus means "And when it became late."

- **ἐξεπορεύοντο ἔξω τῆς πόλεως**: The verb ἐξεπορεύοντο is the third person plural imperfect indicative middle/passive of ἐξέρχομαι, which means "they were going out" or "they were departing." The preposition ἔξω means "out." The article τῆς is the genitive singular of ὁ, which means "the." The noun πόλεως is the genitive singular of πόλις, meaning "city." The phrase thus means "they were going out of the city."

The literal translation of the verse is: "And when it became late, they were going out of the city."