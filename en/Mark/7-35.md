---
version: 1
---
- **καὶ ἠνοίγησαν αὐτοῦ αἱ ἀκοαί**: The word καὶ is a conjunction meaning "and." The verb ἠνοίγησαν is the third person plural aorist passive indicative of ἀνοίγω, meaning "they were opened." The noun αἱ ἀκοαί is the nominative plural of ἀκοή, meaning "hearing" or "ears." The phrase thus means "and his ears were opened."

- **καὶ ἐλύθη ὁ δεσμὸς τῆς γλώσσης αὐτοῦ**: The word καὶ is again a conjunction meaning "and." The verb ἐλύθη is the third person singular aorist passive indicative of λύω, meaning "it was loosed" or "it was untied." The noun ὁ δεσμὸς is the nominative singular of δεσμός, meaning "bond" or "fetter." The genitive τῆς γλώσσης is the genitive singular of γλῶσσα, meaning "tongue." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase can be translated as "and the bond of his tongue was loosed."

- **καὶ ἐλάλει ὀρθῶς**: The word καὶ is once again a conjunction meaning "and." The verb ἐλάλει is the third person singular imperfect indicative of λαλέω, meaning "he was speaking" or "he was talking." The adverb ὀρθῶς means "correctly" or "rightly." The phrase can be translated as "and he was speaking correctly."

The entire verse can be translated as "And his ears were opened, and the bond of his tongue was loosed, and he was speaking correctly."