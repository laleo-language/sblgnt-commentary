---
version: 1
---
- **ὁ δὲ εἶπεν αὐτοῖς**: The word ὁ is the definite article, meaning "the." The word δὲ is a conjunction meaning "but" or "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "but he said to them."
- **Τί θέλετε**: The word Τί is an interrogative pronoun meaning "what." The verb θέλετε is the second person plural present indicative of θέλω, meaning "you want." The phrase thus means "what do you want."
- **ποιήσω ὑμῖν**: The word ποιήσω is the first person singular future indicative of ποιέω, meaning "I will do." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The phrase thus means "I will do for you."

The literal translation of the entire verse is: "But he said to them, 'What do you want me to do for you?'"