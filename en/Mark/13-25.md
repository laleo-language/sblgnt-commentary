---
version: 1
---
- **καὶ οἱ ἀστέρες**: The word οἱ is the definite article in the nominative plural masculine, indicating "the." The noun ἀστέρες is the nominative plural masculine of ἀστήρ, meaning "stars." The phrase means "and the stars."

- **ἔσονται ἐκ τοῦ οὐρανοῦ πίπτοντες**: The verb ἔσονται is the third person plural future indicative of εἰμί, meaning "they will be." The preposition ἐκ means "from." The definite article τοῦ is the genitive singular masculine of ὁ, indicating "the." The noun οὐρανοῦ is the genitive singular masculine of οὐρανός, meaning "heaven" or "sky." The participle πίπτοντες is the nominative plural masculine present active participle of πίπτω, meaning "falling." The phrase can be translated as "falling from the sky."

- **καὶ αἱ δυνάμεις αἱ ἐν τοῖς οὐρανοῖς σαλευθήσονται**: The conjunction καὶ means "and." The definite article αἱ is the nominative plural feminine of ὁ, indicating "the." The noun δυνάμεις is the nominative plural feminine of δύναμις, meaning "powers" or "forces." The adjective ἐν means "in." The definite article τοῖς is the dative plural masculine of ὁ, indicating "the." The noun οὐρανοῖς is the dative plural masculine of οὐρανός, meaning "heavens" or "skies." The verb σαλευθήσονται is the third person plural future indicative passive of σαλεύω, meaning "they will be shaken." The phrase can be translated as "and the powers in the heavens will be shaken."

The literal translation of Mark 13:25 is: "And the stars will be falling from the sky, and the powers in the heavens will be shaken."