---
version: 1
---
- **οἱ δὲ εἶπαν αὐτοῖς**: The word οἱ is the nominative plural of ὁ, which is the definite article meaning "the." The word δὲ is a conjunction used to connect clauses or phrases, often translated as "but" or "and." The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase can be translated as "but they said to them."

- **καθὼς ⸀εἶπεν ὁ Ἰησοῦς**: The word καθὼς is a conjunction meaning "as" or "just as." The word εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The article ὁ is the definite article meaning "the." The name Ἰησοῦς refers to Jesus. The phrase can be translated as "just as Jesus said."

- **καὶ ἀφῆκαν αὐτούς**: The word καὶ is a conjunction meaning "and." The verb ἀφῆκαν is the third person plural aorist indicative of ἀφίημι, meaning "they let go" or "they released." The pronoun αὐτούς is the accusative plural of αὐτός, meaning "them." The phrase can be translated as "and they let them go."

The entire verse can be translated as: "But they said to them, just as Jesus said, and they let them go."