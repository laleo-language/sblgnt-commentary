---
version: 1
---
- **ἀμὴν λέγω ὑμῖν**: The phrase ἀμὴν λέγω ὑμῖν consists of the particle ἀμήν, which is an Aramaic word meaning "truly" or "amen," and the verb λέγω, which means "I say." So the phrase means "truly I say to you."

- **ὃς ἂν μὴ δέξηται τὴν βασιλείαν τοῦ θεοῦ ὡς παιδίον**: This phrase is more complex. The relative pronoun ὃς means "whoever" or "who." The particle ἄν is a marker of potentiality or contingency. The verb δέξηται is the third person singular aorist subjunctive of δέχομαι, meaning "to receive." The noun βασιλείαν is the accusative singular of βασιλεία, which means "kingdom." The genitive τοῦ θεοῦ means "of God," and the noun παιδίον means "child." So, the phrase can be translated as "whoever does not receive the kingdom of God like a child."

- **οὐ μὴ εἰσέλθῃ εἰς αὐτήν**: The phrase οὐ μὴ is a double negative that signifies strong negation, so it can be translated as "will not." The verb εἰσέλθῃ is the third person singular aorist subjunctive of εἰσέρχομαι, meaning "to enter." The preposition εἰς means "into," and the pronoun αὐτήν refers back to the βασιλείαν, "kingdom." Therefore, the phrase means "will not enter into it."

Putting it all together, the literal translation of Mark 10:15 is: "Truly I say to you, whoever does not receive the kingdom of God like a child will not enter into it."

In this verse, there are no ambiguous syntax or complex grammar constructions. The main focus is on understanding the meaning of the words and their arrangement to form a coherent sentence.