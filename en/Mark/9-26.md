---
version: 1
---
- **καὶ κράξας καὶ πολλὰ σπαράξας**: The word κράξας is the aorist active participle masculine nominative singular of κράζω, meaning "having cried out." The word πολλὰ is the neuter plural accusative of πολύς, meaning "many" or "much." The word σπαράξας is the aorist active participle masculine nominative singular of σπαράσσω, meaning "having convulsed" or "having torn apart." The phrase thus means "having cried out and having convulsed."

- **ἐξῆλθεν**: The word ἐξῆλθεν is the third person singular aorist indicative of ἐξέρχομαι, meaning "he went out."

- **καὶ ἐγένετο ὡσεὶ νεκρὸς**: The word ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "he became." The word ὡσεὶ is an adverb meaning "as" or "like." The word νεκρὸς is the masculine singular nominative of νεκρός, meaning "dead." The phrase thus means "and he became as dead."

- **ὥστε τοὺς πολλοὺς λέγειν ὅτι ἀπέθανεν**: The word ὥστε is a conjunction meaning "so that" or "with the result that." The word τοὺς πολλοὺς is the masculine plural accusative of πολύς, meaning "many" or "much." The word λέγειν is the present active infinitive of λέγω, meaning "to say." The word ὅτι is a conjunction meaning "that." The word ἀπέθανεν is the third person singular aorist indicative of ἀποθνῄσκω, meaning "he died." The phrase thus means "so that many were saying that he died."

The literal translation of the verse would be: "And having cried out and convulsed much, he went out; and he became as dead, so that many were saying that he died."