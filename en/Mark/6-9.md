---
version: 1
---
- **ἀλλὰ ὑποδεδεμένους σανδάλια**: The word ἀλλὰ means "but" or "instead." The participle ὑποδεδεμένους is the accusative plural masculine of ὑποδέω, meaning "to tie" or "to bind." The noun σανδάλια is the accusative plural of σανδάλιον, which means "sandal." The phrase thus means "but having tied sandals."

- **καὶ μὴ ἐνδύσησθε δύο χιτῶνας**: The conjunction καὶ means "and." The verb ἐνδύσησθε is the second person plural aorist middle subjunctive of ἐνδύω, meaning "to put on" or "to wear." The negation μὴ is used to express "not." The noun δύο is the accusative plural of δύο, meaning "two." The noun χιτῶνας is the accusative plural of χιτών, which means "tunic" or "undergarment." The phrase thus means "and do not put on two tunics."

The literal translation of Mark 6:9 is: "But having tied sandals, and do not put on two tunics."