---
version: 1
---
- **καὶ οὐκ ἀφῆκεν οὐδένα**: The word καὶ is a conjunction that means "and." The word οὐκ is the negative particle, meaning "not." The verb ἀφῆκεν is the third person singular aorist indicative of ἀφίημι, meaning "he allowed" or "he let." The word οὐδένα is the accusative singular of οὐδείς, which means "no one" or "none." The phrase thus means "And he did not allow anyone."

- **μετʼ αὐτοῦ**: The word μετʼ is a preposition that means "with." The word αὐτοῦ is the genitive singular of αὐτός, meaning "himself" or "him." The phrase thus means "with him."

- **συνακολουθῆσαι**: The verb συνακολουθῆσαι is the aorist infinitive of συνακολουθέω, meaning "to follow along with." The phrase thus means "to follow along with him."

- **εἰ μὴ**: The phrase εἰ μὴ is a conjunction that means "except" or "unless."

- **τὸν Πέτρον καὶ Ἰάκωβον καὶ Ἰωάννην τὸν ἀδελφὸν Ἰακώβου**: The word τὸν is the accusative singular of ὁ, meaning "the." The names Πέτρον, Ἰάκωβον, and Ἰωάννην are in the accusative singular form. The word καὶ is a conjunction that means "and." The word ἀδελφὸν is the accusative singular of ἀδελφός, meaning "brother." The name Ἰακώβου is in the genitive singular form. The phrase thus means "Peter and James and John, the brother of James."

- The literal translation of the entire verse is "And he did not allow anyone to follow along with him, except Peter and James and John, the brother of James."