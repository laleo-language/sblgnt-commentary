---
version: 1
---
- **καὶ γνοὺς**: The conjunction καὶ means "and," connecting this phrase to what came before. The participle γνοὺς is the nominative singular masculine of γινώσκω, meaning "knowing" or "having recognized." The phrase thus means "And having recognized."

- **ἀπὸ τοῦ κεντυρίωνος**: The preposition ἀπὸ means "from" or "by." The article τοῦ is the genitive singular masculine of ὁ, meaning "the." The noun κεντυρίων is the genitive singular of κεντυρίων, which means "centurion" or "officer." The phrase can be translated as "from the centurion" or "by the centurion."

- **ἐδωρήσατο τὸ πτῶμα τῷ Ἰωσήφ**: The verb ἐδωρήσατο is the third person singular aorist middle indicative of δωρέομαι, meaning "he gave" or "he granted." The article τὸ is the accusative singular neuter of ὁ, meaning "the." The noun πτῶμα is the accusative singular neuter of πτῶμα, which means "body" or "corpse." The dative noun τῷ is the dative singular masculine of ὁ, meaning "to the." The name Ἰωσήφ is the dative singular masculine of Ἰωσήφ, which is a proper name. The phrase can be translated as "he gave the body to Joseph" or "he granted the corpse to Joseph."

The literal translation of the verse is: "And having recognized, he gave the body to Joseph."