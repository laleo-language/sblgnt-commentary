---
version: 1
---
- **Καὶ ἔρχονται εἰς χωρίον**: The word Καὶ is a conjunction meaning "and." The verb ἔρχονται is the third person plural present indicative middle of ἔρχομαι, meaning "they come." The preposition εἰς means "into" or "to," and the noun χωρίον is the accusative singular of χωρίον, meaning "place" or "area." The phrase thus means "And they come into a place."

- **οὗ τὸ ὄνομα Γεθσημανί**: The relative pronoun οὗ is in the genitive singular masculine/neuter, and it refers back to the noun χωρίον. The article τὸ is the nominative/accusative singular neuter of ὁ, meaning "the." The noun ὄνομα is the nominative/accusative singular neuter of ὄνομα, meaning "name." The proper noun Γεθσημανί is the genitive singular of Γεθσημανί, which is the transliteration of the Hebrew word גַּת־שְׁמָנִי (Gath-shemani) meaning "oil press," referring to a garden near Jerusalem. The phrase thus means "the name of which is Gethsemane."

- **καὶ λέγει τοῖς μαθηταῖς αὐτοῦ**: The conjunction καὶ means "and." The verb λέγει is the third person singular present indicative active of λέγω, meaning "he says." The article τοῖς is the dative plural masculine/neuter of ὁ, meaning "to the." The noun μαθηταῖς is the dative plural masculine/neuter of μαθητής, meaning "disciples" or "students." The possessive pronoun αὐτοῦ is the genitive singular masculine of αὐτός, meaning "his." The phrase thus means "And he says to his disciples."

- **Καθίσατε ὧδε ἕως προσεύξωμαι**: The verb Καθίσατε is the second person plural aorist imperative middle of καθίζω, meaning "sit down." The adverb ὧδε means "here." The conjunction ἕως means "until." The verb προσεύξωμαι is the first person singular aorist subjunctive middle of προσεύχομαι, meaning "I pray." The phrase thus means "Sit here until I pray."

- **Καὶ ἔρχονται εἰς χωρίον οὗ τὸ ὄνομα Γεθσημανί, καὶ λέγει τοῖς μαθηταῖς αὐτοῦ· Καθίσατε ὧδε ἕως προσεύξωμαι**: And they come into a place called Gethsemane, and he says to his disciples, "Sit here until I pray."

In Mark 14:32, we see Jesus and his disciples going into a place called Gethsemane. Jesus then instructs his disciples to sit and wait for him while he goes to pray.