---
version: 1
---
- **καὶ ἐπηρώτησεν αὐτὸν ὁ Πιλᾶτος**: The word καὶ is a conjunction meaning "and." The verb ἐπηρώτησεν is the third person singular aorist indicative of ἐπερωτάω, meaning "he asked." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The name Πιλᾶτος is the nominative singular of Πιλᾶτος, meaning "Pilate." The phrase thus means "And Pilate asked him."

- **Σὺ εἶ ὁ βασιλεὺς τῶν Ἰουδαίων**: The pronoun Σὺ is the second person singular nominative of σύ, meaning "you." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The noun βασιλεὺς is the nominative singular masculine of βασιλεύς, meaning "king." The preposition τῶν is the genitive plural of ὁ, meaning "of." The noun Ἰουδαίων is the genitive plural of Ἰουδαῖος, meaning "Jews." The phrase thus means "You are the king of the Jews."

- **ὁ δὲ ἀποκριθεὶς αὐτῷ λέγει**: The article ὁ is the nominative singular masculine of ὁ, meaning "the." The conjunction δὲ is a coordinating conjunction meaning "and" or "but." The verb ἀποκριθεὶς is the nominative singular masculine aorist participle of ἀποκρίνομαι, meaning "he answered." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The phrase thus means "But he answered him."

- **Σὺ λέγεις**: The pronoun Σὺ is the second person singular nominative of σύ, meaning "you." The verb λέγεις is the second person singular present indicative of λέγω, meaning "you say." The phrase thus means "You say."

Literal Translation: And Pilate asked him, "Are you the king of the Jews?" But he answered him, "You say."