---
version: 1
---
- **ὁ δὲ Ἰησοῦς εἶπεν**: The word ὁ is the definite article, indicating that it is referring to a specific person. The word δὲ is a conjunction that can be translated as "but" or "and." The name Ἰησοῦς is the nominative singular of Ἰησοῦς, which means "Jesus." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "But Jesus said."

- **Ἐγώ εἰμι**: The word Ἐγώ is the first person singular pronoun, meaning "I." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "am." The phrase thus means "I am."

- **καὶ ὄψεσθε τὸν υἱὸν τοῦ ἀνθρώπου**: The word καὶ is a conjunction that can be translated as "and." The verb ὄψεσθε is the second person plural future indicative of ὁράω, meaning "you will see." The article τὸν is the accusative singular of ὁ, indicating that it is referring to a specific person. The word υἱὸν is the accusative singular of υἱός, which means "son." The article τοῦ is the genitive singular of ὁ. The noun ἀνθρώπου is the genitive singular of ἄνθρωπος, which means "man" or "human." The phrase thus means "and you will see the son of man."

- **ἐκ δεξιῶν καθήμενον τῆς δυνάμεως**: The preposition ἐκ is translated as "from" or "out of." The adjective δεξιῶν is the genitive plural of δεξιός, meaning "right." The participle καθήμενον is the accusative singular masculine of κάθημαι, meaning "sitting." The article τῆς is the genitive singular of ὁ. The noun δυνάμεως is the genitive singular of δύναμις, meaning "power." The phrase thus means "sitting at the right hand of power."

- **καὶ ἐρχόμενον μετὰ τῶν νεφελῶν τοῦ οὐρανοῦ**: The word καὶ is a conjunction that can be translated as "and." The participle ἐρχόμενον is the accusative singular masculine of ἔρχομαι, meaning "coming." The preposition μετὰ is translated as "with." The article τῶν is the genitive plural of ὁ. The noun νεφελῶν is the genitive plural of νεφέλη, meaning "cloud." The article τοῦ is the genitive singular of ὁ. The noun οὐρανοῦ is the genitive singular of οὐρανός, meaning "heaven." The phrase thus means "and coming with the clouds of heaven."

The entire verse can be translated as: "But Jesus said, 'I am, and you will see the son of man sitting at the right hand of power and coming with the clouds of heaven.'"