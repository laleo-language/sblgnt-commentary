---
version: 1
---
- **καὶ οὐδὲ οὕτως**: The conjunction καὶ means "and." The adverb οὐδὲ means "not even" or "neither." The adverb οὕτως means "in this way" or "thus." The phrase can be translated as "and not even in this way" or "and not even like this."

- **ἴση ἦν ἡ μαρτυρία**: The adjective ἴση is the nominative singular feminine form of ἴσος, which means "equal" or "the same." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "was." The noun ἡ μαρτυρία is the nominative singular form of μαρτυρία, which means "testimony" or "witness." The phrase can be translated as "the testimony was not the same" or "the testimony was not equal."

- **αὐτῶν**: The pronoun αὐτῶν is the genitive plural form of αὐτός, which means "their" or "of them." It refers back to an implied antecedent. The phrase can be translated as "of them."

The entire verse can be translated as "And not even in this way was their testimony the same." 

In this verse, the phrase "καὶ οὐδὲ οὕτως" emphasizes that the testimony was not equal or the same. The pronoun "αὐτῶν" indicates that the testimony referred to is that of others.