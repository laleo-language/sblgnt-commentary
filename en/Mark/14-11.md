---
version: 1
---
- **οἱ δὲ ἀκούσαντες**: The word οἱ is the nominative plural article, meaning "the." The word δὲ is a conjunction that can be translated as "and" or "but." The participle ἀκούσαντες is the nominative plural masculine aorist active participle of ἀκούω, meaning "having heard." The phrase thus means "and the ones having heard."

- **ἐχάρησαν**: The verb ἐχάρησαν is the third person plural aorist indicative passive of χαίρω, meaning "they rejoiced."

- **καὶ ἐπηγγείλαντο αὐτῷ**: The word καὶ is a conjunction that can be translated as "and" or "but." The verb ἐπηγγείλαντο is the third person plural aorist middle deponent indicative of ἐπαγγέλλομαι, meaning "they promised." The pronoun αὐτῷ is the dative singular masculine of αὐτός, meaning "to him." The phrase thus means "and they promised to him."

- **ἀργύριον δοῦναι**: The noun ἀργύριον is the accusative singular of ἀργύριον, meaning "money." The infinitive δοῦναι is the aorist infinitive of δίδωμι, meaning "to give." The phrase thus means "to give money."

- **καὶ ἐζήτει πῶς ⸂αὐτὸν εὐκαίρως παραδοῖ⸃**: The word καὶ is a conjunction that can be translated as "and" or "but." The verb ἐζήτει is the third person singular imperfect indicative active of ζητέω, meaning "he was seeking." The adverb πῶς means "how." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, meaning "him." The adverb εὐκαίρως means "opportunely" or "conveniently." The verb παραδοῖ is the third person singular aorist subjunctive active of παραδίδωμι, meaning "he might deliver." The phrase thus means "and he was seeking how he might deliver him opportunely."

The literal translation of Mark 14:11 is: "And the ones having heard rejoiced and they promised to him to give money. And he was seeking how he might deliver him opportunely."