---
version: 1
---
- **καὶ ἐδίδασκεν αὐτοὺς**: The conjunction καὶ means "and." The verb ἐδίδασκεν is the third person singular imperfect indicative of διδάσκω, meaning "he was teaching." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The phrase thus means "And he was teaching them."

- **ἐν παραβολαῖς πολλά**: The preposition ἐν means "in." The noun παραβολαῖς is the dative plural of παραβολή, meaning "parables." The adjective πολλά is the neuter plural form of πολύς, meaning "many" or "a lot." The phrase thus means "in many parables."

- **καὶ ἔλεγεν αὐτοῖς**: The conjunction καὶ means "and." The verb ἔλεγεν is the third person singular imperfect indicative of λέγω, meaning "he was saying." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "And he was saying to them."

- **ἐν τῇ διδαχῇ αὐτοῦ**: The preposition ἐν means "in." The definite article τῇ is the feminine singular form of the article ὁ, meaning "the." The noun διδαχῇ is the dative singular of διδαχή, meaning "teaching" or "instruction." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "in his teaching."

The literal translation of Mark 4:2 is: "And he was teaching them in many parables, and he was saying to them in his teaching."