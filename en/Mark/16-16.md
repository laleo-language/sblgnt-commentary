---
version: 1
---
- **ὁ πιστεύσας**: The definite article ὁ ("the") modifies the participle πιστεύσας, which is the nominative singular masculine form of πιστεύω, meaning "to believe." The phrase ὁ πιστεύσας can be translated as "the one who believes" or "the believer."
- **καὶ βαπτισθεὶς**: The conjunction καί connects the previous phrase to this one. The participle βαπτισθεὶς is the nominative singular masculine form of βαπτίζω, meaning "to baptize." The phrase καὶ βαπτισθεὶς can be translated as "and having been baptized."
- **σωθήσεται**: This is the third person singular future indicative form of σῴζω, meaning "to be saved." It is used here in a passive voice, indicating that the subject is the one who will be saved. The phrase σωθήσεται can be translated as "he will be saved."
- **ὁ δὲ ἀπιστήσας**: The definite article ὁ ("the") modifies the participle ἀπιστήσας, which is the nominative singular masculine form of ἀπιστέω, meaning "to disbelieve" or "to be faithless." The phrase ὁ δὲ ἀπιστήσας can be translated as "but the one who disbelieves" or "but the unbeliever."
- **κατακριθήσεται**: This is the third person singular future indicative form of κατακρίνω, meaning "to condemn." It is used here in a passive voice, indicating that the subject is the one who will be condemned. The phrase κατακριθήσεται can be translated as "he will be condemned."

Literal translation: "The one who believes and has been baptized will be saved, but the one who disbelieves will be condemned."

In this verse, the phrases "the one who believes and has been baptized" and "the one who disbelieves" are contrasted with each other. The first phrase describes the condition for salvation, emphasizing the importance of faith and baptism. The second phrase describes the consequence for those who do not believe. This verse conveys the message that belief and baptism are essential for salvation.