---
version: 1
---
- **καὶ λέγει αὐτῷ**: The conjunction καὶ means "and," and the verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The pronoun αὐτῷ is the dative singular form of αὐτός, meaning "to him." The phrase thus means "And he says to him."

- **Ὅρα μηδενὶ μηδὲν εἴπῃς**: The verb Ὅρα is an imperative form of ὁράω, meaning "see" or "look." The word μηδενὶ is the dative singular form of μηδείς, meaning "to no one" or "to nobody." The verb μηδὲν is the accusative singular neuter form of μηδείς, meaning "nothing." The verb εἴπῃς is the second person singular aorist subjunctive of λέγω, meaning "you say." The phrase can be translated as "See that you say nothing to anyone."

- **ἀλλὰ ὕπαγε**: The conjunction ἀλλὰ means "but," and the verb ὕπαγε is the second person singular present imperative of ὑπάγω, meaning "go." The phrase can be translated as "But go."

- **σεαυτὸν δεῖξον**: The pronoun σεαυτὸν is the accusative singular form of σεαυτός, meaning "yourself." The verb δεῖξον is the second person singular aorist imperative of δείκνυμι, meaning "show." The phrase can be translated as "Show yourself."

- **τῷ ἱερεῖ**: The article τῷ is the dative singular form of the definite article, meaning "to the." The noun ἱερεῖ is the dative singular form of ἱερεύς, meaning "priest." The phrase can be translated as "to the priest."

- **καὶ προσένεγκε περὶ τοῦ καθαρισμοῦ σου**: The conjunction καὶ means "and." The verb προσένεγκε is the second person singular aorist imperative of προσφέρω, meaning "bring" or "offer." The preposition περὶ means "concerning" or "about." The noun καθαρισμοῦ is the genitive singular form of καθαρισμός, meaning "cleansing" or "purification." The pronoun σου is the genitive singular form of σύ, meaning "your." The phrase can be translated as "And bring about your cleansing."

- **ἃ προσέταξεν Μωϋσῆς εἰς μαρτύριον αὐτοῖς**: The relative pronoun ἃ is the accusative plural neuter form of ὅς, meaning "which" or "what." The verb προσέταξεν is the third person singular aorist indicative of προστάσσω, meaning "he commanded" or "he ordered." The noun Μωϋσῆς is the nominative singular form of Μωϋσῆς, meaning "Moses." The preposition εἰς means "for" or "to." The noun μαρτύριον is the accusative singular form of μαρτύριον, meaning "testimony" or "evidence." The pronoun αὐτοῖς is the dative plural form of αὐτός, meaning "to them." The phrase can be translated as "which Moses commanded as a testimony to them."

The literal translation of the entire verse is: "And he says to him, 'See that you say nothing to anyone, but go, show yourself to the priest and bring about your cleansing, which Moses commanded as a testimony to them.'"