---
version: 1
---
- **ἰδὼν δὲ ὁ Ἰησοῦς**: The word ἰδὼν is the nominative masculine singular participle of ὁράω, meaning "seeing" or "having seen." The word δὲ is a conjunction meaning "and" or "but." The noun ὁ Ἰησοῦς is the nominative masculine singular form of Ἰησοῦς, meaning "Jesus." The phrase means "Jesus, having seen."

- **ὅτι ἐπισυντρέχει ὄχλος**: The word ὅτι is a conjunction meaning "that" or "because." The verb ἐπισυντρέχω is the third person singular present indicative of ἐπισυντρέχω, meaning "to come running together" or "to gather." The noun ὄχλος is the nominative masculine singular form of ὄχλος, meaning "crowd." The phrase means "because a crowd is coming running together."

- **ἐπετίμησεν τῷ πνεύματι τῷ ἀκαθάρτῳ**: The verb ἐπιτιμάω is the third person singular aorist indicative of ἐπιτιμάω, meaning "to rebuke" or "to admonish." The article τῷ is the dative masculine singular form of ὁ, meaning "the." The noun πνεῦμα is the dative neuter singular form of πνεῦμα, meaning "spirit." The adjective ἀκαθάρτος is the dative masculine singular form of ἀκάθαρτος, meaning "unclean." The phrase means "he rebuked the unclean spirit."

- **λέγων αὐτῷ**: The verb λέγων is the nominative masculine singular present participle of λέγω, meaning "saying." The pronoun αὐτῷ is the dative masculine singular form of αὐτός, meaning "to him." The phrase means "saying to him."

- **Τὸ ἄλαλον καὶ κωφὸν πνεῦμα**: The article Τὸ is the nominative neuter singular form of ὁ, meaning "the." The adjective ἄλαλον is the accusative neuter singular form of ἄλαλος, meaning "mute" or "speechless." The adjective κωφὸν is the accusative neuter singular form of κωφός, meaning "deaf" or "dumb." The noun πνεῦμα is the accusative neuter singular form of πνεῦμα, meaning "spirit." The phrase means "the mute and deaf spirit."

- **ἐγὼ ἐπιτάσσω σοι**: The pronoun ἐγὼ is the first person singular nominative form of ἐγώ, meaning "I." The verb ἐπιτάσσω is the first person singular present indicative of ἐπιτάσσω, meaning "I command" or "I order." The pronoun σοι is the dative second person singular form of σύ, meaning "to you." The phrase means "I command you."

- **ἔξελθε ἐξ αὐτοῦ καὶ μηκέτι εἰσέλθῃς εἰς αὐτόν**: The verb ἔξελθε is the second person singular aorist imperative of ἐξέρχομαι, meaning "go out" or "come out." The preposition ἐξ is a preposition meaning "out of" or "from." The pronoun αὐτοῦ is the genitive masculine singular form of αὐτός, meaning "of him." The conjunction καὶ is a conjunction meaning "and." The adverb μηκέτι is an adverb meaning "no longer" or "not anymore." The verb εἰσέρχομαι is the second person singular present subjunctive of εἰσέρχομαι, meaning "go into" or "enter." The preposition εἰς is a preposition meaning "into." The pronoun αὐτόν is the accusative masculine singular form of αὐτός, meaning "him." The phrase means "Go out from him and no longer enter into him."

The literal translation of Mark 9:25 is: "And Jesus, having seen that a crowd is coming running together, rebuked the unclean spirit, saying to him, 'You mute and deaf spirit, I command you, go out from him and no longer enter into him.'"