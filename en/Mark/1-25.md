---
version: 1
---
- **καὶ ἐπετίμησεν αὐτῷ**: The conjunction καὶ means "and." The verb ἐπετίμησεν is the third person singular aorist indicative of ἐπιτιμάω, meaning "he rebuked" or "he scolded." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "And he rebuked him."

- **ὁ Ἰησοῦς λέγων**: The article ὁ is the definite article, meaning "the." The noun Ἰησοῦς is the nominative singular of Ἰησοῦς, meaning "Jesus." The verb λέγων is the present participle of λέγω, meaning "saying" or "speaking." The phrase thus means "Jesus, saying."

- **Φιμώθητι**: The verb Φιμώθητι is the second person singular aorist imperative passive of φιμόω, meaning "be silent" or "be muzzled." The phrase thus means "Be silent."

- **καὶ ἔξελθε ἐξ αὐτοῦ**: The conjunction καὶ means "and." The verb ἔξελθε is the second person singular aorist imperative of ἐξέρχομαι, meaning "go out" or "come out." The preposition ἐξ means "out of" and the pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "him." The phrase thus means "And come out of him."

The literal translation of Mark 1:25 is: "And he rebuked him, Jesus saying, 'Be silent and come out of him.'"