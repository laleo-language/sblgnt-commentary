---
version: 1
---
- **καὶ ἐναγκαλισάμενος αὐτὰ**: The conjunction καὶ means "and." The verb ἐναγκαλισάμενος is aorist middle participle of ἐναγκαλίζομαι, which means "to embrace" or "to take into one's arms." The pronoun αὐτὰ is the accusative plural of αὐτός, meaning "them." The phrase thus means "and having embraced them."

- **κατευλόγει τιθεὶς τὰς χεῖρας ἐπʼ αὐτά**: The verb κατευλόγει is the imperfect indicative of κατευλογέω, which means "to bless." The participle τιθεὶς is the nominative masculine singular of τίθημι, meaning "placing" or "setting." The article τὰς is the accusative plural feminine form of ὁ, meaning "the." The noun χεῖρας is the accusative plural of χείρ, meaning "hands." The preposition ἐπʼ means "upon" or "on." The pronoun αὐτά is the accusative plural of αὐτός, meaning "them." The phrase thus means "he blessed them, placing his hands on them."

The literal translation of Mark 10:16 is "And having embraced them, he blessed them, placing his hands on them."