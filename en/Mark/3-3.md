---
version: 1
---
- **τῷ ἀνθρώπῳ**: The word τῷ is the dative singular of ὁ, which means "the." The word ἀνθρώπῳ is the dative singular of ἄνθρωπος, which means "man." The phrase thus means "to the man."

- **τῷ τὴν χεῖρα ἔχοντι ξηράν**: The word τῷ is the dative singular of ὁ, which means "the." The word τὴν is the accusative singular of ὁ, which means "the." The word χεῖρα is the accusative singular of χείρ, which means "hand." The word ἔχοντι is the present participle of ἔχω, which means "having." The word ξηράν is the accusative singular of ξηρός, which means "withered." The phrase thus means "to the man having the withered hand."

- **Ἔγειρε εἰς τὸ μέσον**: The word Ἔγειρε is the second person singular present imperative of ἐγείρω, which means "rise" or "get up." The word εἰς is a preposition that can mean "into" or "to." The word τὸ is the accusative singular of ὁ, which means "the." The word μέσον is the accusative singular of μέσος, which means "middle." The phrase thus means "Rise into the middle."

The literal translation of the verse is: "And he says to the man with the withered hand, 'Rise into the middle.'"