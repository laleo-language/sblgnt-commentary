---
version: 1
---
- **καὶ εἶπεν**: The conjunction καὶ means "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said." The phrase thus means "and he said."
- **τοῖς μαθηταῖς αὐτοῦ**: The article τοῖς is the masculine dative plural form of the definite article ὁ, which means "the." The noun μαθηταῖς is the dative plural of μαθητής, which means "disciples" or "students." The pronoun αὐτοῦ is the genitive singular of αὐτός, which means "his" or "his own." The phrase thus means "to his disciples."
- **ἵνα πλοιάριον προσκαρτερῇ αὐτῷ**: The conjunction ἵνα introduces a purpose clause and is often translated as "so that" or "in order to." The noun πλοιάριον is the accusative singular of πλοιάριον, which means "boat" or "small boat." The verb προσκαρτερῇ is the third person singular present subjunctive of προσκαρτερέω, which means "to wait for" or "to stay with." The pronoun αὐτῷ is the dative singular of αὐτός. The phrase thus means "so that a boat would wait for him."
- **διὰ τὸν ὄχλον**: The preposition διὰ means "through" or "because of." The article τὸν is the masculine accusative singular form of ὁ. The noun ὄχλον is the accusative singular of ὄχλος, which means "crowd" or "multitude." The phrase thus means "because of the crowd."
- **ἵνα μὴ θλίβωσιν αὐτόν**: The conjunction ἵνα introduces a negative purpose clause. The verb θλίβωσιν is the third person plural aorist subjunctive of θλίβω, which means "to press" or "to crowd." The pronoun αὐτόν is the accusative singular of αὐτός. The phrase thus means "so that they would not crush him."

Literal translation: "And he said to his disciples that a boat should wait for him because of the crowd, so that they would not crush him."

The verse describes Jesus instructing his disciples to have a boat ready for him due to the large crowd that gathered around him. The boat would serve as a means of protection against the crowd, preventing them from pressing in so closely that they would harm Jesus.