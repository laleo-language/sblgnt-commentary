---
version: 1
---
- **ἕνεκεν τούτου**: The word ἕνεκεν is a preposition that means "because of" or "on account of." The noun τούτου is the genitive singular of οὗτος, meaning "this." The phrase thus means "because of this."

- **καταλείψει ἄνθρωπος τὸν πατέρα αὐτοῦ καὶ τὴν μητέρα**: The verb καταλείψει is the third person singular future indicative of καταλείπω, meaning "he will leave behind." The noun ἄνθρωπος is the accusative singular of ἄνθρωπος, meaning "man" or "person." The article τὸν is the accusative singular of ὁ, meaning "the." The noun πατέρα is the accusative singular of πατήρ, meaning "father." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The conjunction καὶ means "and." The article τὴν is the accusative singular of ὁ. The noun μητέρα is the accusative singular of μήτηρ, meaning "mother." The phrase thus means "a man will leave behind his father and mother."

- **καὶ προσκολληθήσεται πρὸς τὴν γυναῖκα αὐτοῦ**: The conjunction καὶ means "and." The verb προσκολληθήσεται is the third person singular future indicative passive of προσκολλάω, meaning "he will be joined to" or "he will be united with." The preposition πρὸς means "to" or "toward." The article τὴν is the accusative singular of ὁ. The noun γυναῖκα is the accusative singular of γυνή, meaning "wife." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase thus means "and he will be joined to his wife."

The sentence can be literally translated as: "Because of this, a man will leave behind his father and mother, and he will be joined to his wife."

In this verse, there are no ambiguous grammatical constructions.