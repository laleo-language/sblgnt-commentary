---
version: 1
---
- **Καὶ ἀκούσαντες**: The conjunction καὶ means "and." The participle ἀκούσαντες is the nominative plural masculine of ἀκούω, meaning "having heard." The phrase thus means "And having heard."

- **οἱ δέκα**: The article οἱ is the nominative plural masculine of ὁ, meaning "the." The number δέκα means "ten." The phrase thus means "the ten."

- **ἤρξαντο ἀγανακτεῖν**: The verb ἤρξαντο is the third person plural aorist middle indicative of ἄρχομαι, meaning "they began." The infinitive ἀγανακτεῖν is the present infinitive of ἀγανακτέω, meaning "to be indignant." The phrase thus means "they began to be indignant."

- **περὶ Ἰακώβου καὶ Ἰωάννου**: The preposition περὶ means "about" or "concerning." The genitive Ἰακώβου and Ἰωάννου are the genitive singular masculine of Ἰάκωβος and Ἰωάννης, which are proper names. The phrase thus means "about James and John."

- **Καὶ ἀκούσαντες οἱ δέκα ἤρξαντο ἀγανακτεῖν περὶ Ἰακώβου καὶ Ἰωάννου**: Putting it all together, the sentence means "And when the ten heard, they began to be indignant about James and John."

The literal translation of Mark 10:41 is "And having heard, the ten began to be indignant about James and John."