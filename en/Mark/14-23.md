---
version: 1
---
- **καὶ λαβὼν ποτήριον**: The word καὶ is a conjunction meaning "and." The participle λαβὼν is the nominative singular masculine form of λαμβάνω, which means "taking." The noun ποτήριον is the accusative singular of ποτήριον, meaning "cup." The phrase thus means "and taking a cup."

- **εὐχαριστήσας**: This is the nominative singular masculine participle of εὐχαριστέω, which means "giving thanks." The phrase εὐχαριστήσας ἔδωκεν αὐτοῖς can be understood as "having given thanks, he gave to them."

- **ἔδωκεν αὐτοῖς**: The verb ἔδωκεν is the third person singular aorist indicative of δίδωμι, meaning "he gave." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase means "he gave to them."

- **καὶ ἔπιον ἐξ αὐτοῦ πάντες**: The word καὶ is a conjunction meaning "and." The verb ἔπιον is the third person plural aorist indicative of πίνω, meaning "they drank." The preposition ἐξ means "from." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "of it." The adjective πάντες is the nominative plural of πᾶς, meaning "all." The phrase can be understood as "and they all drank from it."

The entire verse can be literally translated as: "And taking a cup, having given thanks, he gave it to them, and they all drank from it."