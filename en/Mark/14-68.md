---
version: 1
---
- **ὁ δὲ ἠρνήσατο λέγων**: The word ὁ is the nominative singular masculine definite article, meaning "the." The word δὲ is a conjunction that can be translated as "but" or "and." The verb ἠρνήσατο is the third person singular aorist middle indicative of ἀρνέομαι, meaning "he denied." The participle λέγων is the nominative singular masculine present active participle of λέγω, meaning "saying." The phrase thus means "But he denied, saying."

- **Οὔτε οἶδα οὔτε ἐπίσταμαι σὺ τί λέγεις**: The word Οὔτε is a conjunction that can be translated as "neither" or "nor." The verb οἶδα is the first person singular present indicative of εἴδω, meaning "I know." The verb ἐπίσταμαι is the first person singular present indicative middle deponent of ἐπίσταμαι, meaning "I understand" or "I know." The pronoun σὺ is the second person singular nominative pronoun, meaning "you." The word τί is an interrogative pronoun meaning "what." The verb λέγεις is the second person singular present indicative of λέγω, meaning "you say." The phrase thus means "Neither do I know nor do I understand what you are saying."

- **καὶ ἐξῆλθεν ἔξω εἰς τὸ προαύλιον**: The conjunction καὶ can be translated as "and." The verb ἐξῆλθεν is the third person singular aorist indicative of ἐξέρχομαι, meaning "he went out." The adverb ἔξω means "outside." The preposition εἰς means "into." The article τὸ is the accusative singular neuter definite article, meaning "the." The noun προαύλιον is the accusative singular neuter of προαύλιον, meaning "courtyard." The phrase thus means "And he went out outside into the courtyard."

- **καὶ ἀλέκτωρ ἐφώνησεν**: The conjunction καὶ can be translated as "and." The noun ἀλέκτωρ is the nominative singular masculine noun, meaning "rooster" or "cock." The verb ἐφώνησεν is the third person singular aorist indicative of φωνέω, meaning "he crowed." The phrase thus means "And a rooster crowed."

The literal translation of the verse is: "But he denied, saying, 'Neither do I know nor do I understand what you are saying.' And he went out outside into the courtyard. And a rooster crowed."