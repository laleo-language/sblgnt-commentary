---
version: 1
---
- **καὶ εὐθὺς**: The conjunction καὶ means "and" and is used here to connect the phrase to what came before it. The adverb εὐθὺς means "immediately" or "right away." So this phrase means "and immediately."
- **ἀφέντες τὰ δίκτυα**: The participle ἀφέντες is the nominative plural of ἀφίημι, meaning "leaving" or "abandoning." The article τὰ is the accusative plural of ὁ, meaning "the." The noun δίκτυα is the accusative plural of δίκτυον, which means "nets." So this phrase means "leaving the nets."
- **ἠκολούθησαν αὐτῷ**: The verb ἠκολούθησαν is the third person plural aorist indicative of ἀκολουθέω, meaning "they followed." The pronoun αὐτῷ is the dative singular of αὐτός, which means "him." So this phrase means "they followed him."

Literal translation: "And immediately, leaving the nets, they followed him."

In this verse, the syntax is straightforward and there are no ambiguous constructions. The disciples of Jesus immediately abandoned their nets and followed him.