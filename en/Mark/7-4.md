---
version: 1
---
- **καὶ ἀπʼ ἀγορᾶς**: The conjunction καὶ means "and" and connects this phrase to the previous one. The preposition ἀπό means "from." The noun ἀγορά is the genitive singular form of ἀγορά, which means "marketplace." The phrase thus means "and from the marketplace."

- **ἐὰν μὴ βαπτίσωνται**: The conjunction ἐὰν means "if" or "unless." The verb βαπτίσωνται is the third person plural aorist subjunctive middle/passive of βαπτίζω, which means "to baptize." The phrase can be translated as "if they do not baptize themselves" or "unless they are baptized."

- **οὐκ ἐσθίουσιν**: The adverb οὐκ negates the verb ἐσθίουσιν, which is the third person plural present indicative active of ἐσθίω, meaning "to eat." The phrase means "they do not eat."

- **καὶ ἄλλα πολλά**: The conjunction καὶ means "and" and connects this phrase to the previous one. The adjective πολλά is the neuter plural form of πολύς, which means "many" or "much." The phrase means "and many other things."

- **ἐστιν ἃ παρέλαβον κρατεῖν**: The verb ἐστιν is the third person singular present indicative active of εἰμί, which means "to be." The relative pronoun ἃ is the accusative neuter plural form of ὅς, which means "which" or "what." The verb παρέλαβον is the first person singular aorist indicative active of παραλαμβάνω, meaning "I received" or "I took." The verb κρατεῖν is the present infinitive of κρατέω, meaning "to hold" or "to possess." The phrase means "there are things which I received to hold."

- **βαπτισμοὺς ποτηρίων καὶ ξεστῶν καὶ χαλκίων καὶ κλινῶν**: The noun βαπτισμοὺς is the accusative plural form of βαπτισμός, which means "washings" or "baptisms." The nouns ποτηρίων, ξεστῶν, χαλκίων, and κλινῶν are all genitive plural forms of ποτήριον (cup), ξέστης (pitcher), χαλκίον (bronze vessel), and κλίνη (bed) respectively. The phrase means "washings of cups and pitchers and bronze vessels and beds."

- **καὶ κλινῶν**: The conjunction καὶ means "and" and connects this phrase to the previous one. The noun κλινῶν is the genitive plural form of κλίνη, which means "bed." The phrase means "and beds."

- *Literal translation of the verse:* "And from the marketplace, if they do not baptize themselves, they do not eat; and many other things there are which I received to hold, washings of cups and pitchers and bronze vessels and beds."