---
version: 1
---
- **καὶ ⸀εὐθὺς**: The conjunction καὶ means "and." The adverb εὐθύς means "immediately" or "right away." So this phrase means "and immediately."

- **ἐπιγνοὺς ὁ Ἰησοῦς τῷ πνεύματι αὐτοῦ**: The participle ἐπιγνοὺς is the nominative singular masculine form of ἐπιγινώσκω, which means "to recognize" or "to know." The article ὁ is the definite article "the," referring to Jesus. The noun πνεῦμα is the accusative singular form of πνεῦμα, which means "spirit." The pronoun αὐτοῦ is the genitive singular form of αὐτός, which means "his." The phrase can be translated as "Jesus, having recognized by his spirit."

- **ὅτι ⸀οὕτως διαλογίζονται ἐν ἑαυτοῖς**: The conjunction ὅτι means "that." The adverb οὕτως means "in this way" or "like this." The verb διαλογίζονται is the third person plural present middle indicative form of διαλογίζομαι, which means "to reason" or "to think." The preposition ἐν means "in." The reflexive pronoun ἑαυτοῖς is the dative plural form of ἑαυτός, which means "themselves." The phrase can be translated as "that they reason in this way among themselves."

- **λέγει αὐτοῖς**: The verb λέγει is the third person singular present active indicative form of λέγω, which means "to say." The pronoun αὐτοῖς is the dative plural form of αὐτός, which means "to them." The phrase can be translated as "he says to them."

- **Τί ταῦτα διαλογίζεσθε ἐν ταῖς καρδίαις ὑμῶν**: The pronoun Τί means "why" or "what." The pronoun ταῦτα means "these things." The verb διαλογίζεσθε is the second person plural present middle indicative form of διαλογίζομαι, which means "to reason" or "to think." The preposition ἐν means "in." The definite article ταῖς is the dative plural form of ὁ, which means "the." The noun καρδίαις is the dative plural form of καρδία, which means "hearts." The pronoun ὑμῶν is the genitive plural form of σύ, which means "your." The phrase can be translated as "Why do you reason about these things in your hearts?"

The literal translation of the entire verse is: "And immediately Jesus, having recognized by his spirit that they reason in this way among themselves, says to them, 'Why do you reason about these things in your hearts?'"