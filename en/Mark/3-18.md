---
version: 1
---
- **καὶ Ἀνδρέαν**: The word καὶ is a coordinating conjunction meaning "and." The noun Ἀνδρέαν is the accusative singular of Ἀνδρέας, which means "Andrew." The phrase means "and Andrew."

- **καὶ Φίλιππον**: The word καὶ is again a coordinating conjunction meaning "and." The noun Φίλιππον is the accusative singular of Φίλιππος, which means "Philip." The phrase means "and Philip."

- **καὶ Βαρθολομαῖον**: The word καὶ is once again a coordinating conjunction meaning "and." The noun Βαρθολομαῖον is the accusative singular of Βαρθολομαῖος, which means "Bartholomew." The phrase means "and Bartholomew."

- **καὶ Μαθθαῖον**: The word καὶ is still a coordinating conjunction meaning "and." The noun Μαθθαῖον is the accusative singular of Μαθθαῖος, which means "Matthew." The phrase means "and Matthew."

- **καὶ Θωμᾶν**: The word καὶ is again a coordinating conjunction meaning "and." The noun Θωμᾶν is the accusative singular of Θωμᾶς, which means "Thomas." The phrase means "and Thomas."

- **καὶ Ἰάκωβον**: The word καὶ is once again a coordinating conjunction meaning "and." The noun Ἰάκωβον is the accusative singular of Ἰάκωβος, which means "James." The phrase means "and James."

- **τὸν τοῦ Ἁλφαίου**: The article τὸν is the accusative singular masculine form of ὁ, which means "the." The genitive singular form of Ἁλφαῖος is Ἁλφαίου, which means "of Alphaeus." The phrase means "the son of Alphaeus."

- **καὶ Θαδδαῖον**: The word καὶ is still a coordinating conjunction meaning "and." The noun Θαδδαῖον is the accusative singular of Θαδδαῖος, which means "Thaddaeus." The phrase means "and Thaddaeus."

- **καὶ Σίμωνα**: The word καὶ is again a coordinating conjunction meaning "and." The noun Σίμωνα is the accusative singular of Σίμων, which means "Simon." The phrase means "and Simon."

- **τὸν Καναναῖον**: The article τὸν is still the accusative singular masculine form of ὁ, which means "the." The noun Καναναῖον is the accusative singular of Καναναῖος, which means "Cananaean." The phrase means "the Cananaean."

The entire verse, Mark 3:18, says "and Andrew, and Philip, and Bartholomew, and Matthew, and Thomas, and James the son of Alphaeus, and Thaddaeus, and Simon the Cananaean."