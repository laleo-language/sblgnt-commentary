---
version: 1
---
- **ὁ δὲ Ἰησοῦς εἶπεν αὐτῷ**: The article ὁ indicates that the noun Ἰησοῦς is in the nominative case and is the subject of the sentence. The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase can be translated as "But Jesus said to him."

- **Τί με λέγεις ἀγαθόν**: The interrogative pronoun Τί means "what." The pronoun με is the accusative singular of ἐγώ, meaning "me." The verb λέγεις is the second person singular present indicative of λέγω, meaning "you say." The adjective ἀγαθόν is in the accusative singular to agree with με, and it means "good." The phrase can be translated as "Why do you call me good?"

- **οὐδεὶς ἀγαθὸς εἰ μὴ εἷς ὁ θεός**: The indefinite pronoun οὐδεὶς means "no one" or "nobody." The adjective ἀγαθὸς is in the nominative singular to agree with οὐδεὶς, and it means "good." The verb εἰ is the third person singular present indicative of εἰμί, meaning "is." The conjunction μὴ indicates negation. The noun εἷς is in the nominative singular and means "one." The article ὁ indicates that the noun θεός is in the nominative case and means "God." The phrase can be translated as "No one is good except one, God."

The entire verse can be translated as "But Jesus said to him, 'Why do you call me good? No one is good except one, God.'"