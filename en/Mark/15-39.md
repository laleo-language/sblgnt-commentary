---
version: 1
---
- **ἰδὼν δὲ**: The word ἰδὼν is the nominative singular masculine participle of ὁράω, which means "seeing." The conjunction δὲ means "but" or "and." The phrase thus means "but seeing" or "and seeing."

- **ὁ κεντυρίων ὁ παρεστηκὼς**: The word ὁ κεντυρίων is the nominative singular masculine form of κεντυρίων, which means "centurion." The article ὁ means "the." The word ὁ παρεστηκὼς is the nominative singular masculine participle of παρίστημι, which means "standing." The phrase thus means "the centurion, the one standing."

- **ἐξ ἐναντίας αὐτοῦ**: The word ἐξ is a preposition that means "out of" or "from." The word ἐναντίας is the genitive singular feminine form of ἐναντίος, which means "opposite" or "in front of." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, which means "himself." The phrase thus means "out of opposite him" or "from in front of him."

- **ὅτι οὕτως ἐξέπνευσεν**: The word ὅτι is a conjunction that means "that" or "because." The adverb οὕτως means "thus" or "in this way." The verb ἐξέπνευσεν is the third person singular aorist indicative of ἐκπνέω, which means "to breathe out" or "to expire." The phrase thus means "because he thus breathed out" or "because he thus expired."

- **εἶπεν**: The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said."

- **Ἀληθῶς**: The adverb Ἀληθῶς means "truly" or "indeed."

- **οὗτος ὁ ἄνθρωπος**: The pronoun οὗτος means "this." The article ὁ means "the." The noun ἄνθρωπος means "man" or "person." The phrase thus means "this man" or "this person."

- **υἱὸς θεοῦ ἦν**: The noun υἱὸς means "son." The noun θεοῦ means "of God" or "God." The verb ἦν is the third person singular imperfect indicative of εἰμί, which means "he was." The phrase thus means "he was the son of God."

The literal translation of the entire verse is: "But seeing the centurion, the one standing opposite him, that he thus breathed out, he said, 'Truly this man was the son of God.'"