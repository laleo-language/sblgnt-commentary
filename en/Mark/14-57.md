---
version: 1
---
- **καί τινες ἀναστάντες**: The word καί is a conjunction that means "and." The word τινες is an indefinite pronoun that means "some." The verb ἀναστάντες is the nominative plural masculine participle of ἀνίστημι, which means "to stand up" or "to rise." The phrase thus means "and some, having stood up."

- **ἐψευδομαρτύρουν**: The verb ἐψευδομαρτύρουν is the third person plural imperfect indicative of ψευδομαρτυρέω, which means "to bear false witness" or "to give false testimony." The phrase thus means "they were bearing false witness."

- **κατʼ αὐτοῦ**: The word κατʼ is a preposition that means "against" or "according to." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, which means "himself" or "him." The phrase thus means "against him" or "according to him."

- **λέγοντες**: The verb λέγοντες is the nominative plural masculine participle of λέγω, which means "to say" or "to speak." The phrase thus means "saying."

The literal translation of the verse would be: "And some, having stood up, were bearing false witness against him, saying."