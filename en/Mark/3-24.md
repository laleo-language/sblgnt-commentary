---
version: 1
---
- **καὶ ἐὰν βασιλεία ἐφʼ ἑαυτὴν μερισθῇ**: The conjunction καὶ means "and." The conditional clause ἐὰν introduces a hypothetical condition. The noun βασιλεία is the nominative singular of βασιλεία, meaning "kingdom." The preposition ἐφʼ is a contraction of ἐπί and ἐγώ, meaning "upon oneself" or "on its own." The reflexive pronoun ἑαυτὴν is the accusative singular of ἑαυτός, meaning "itself." The verb μερισθῇ is the third person singular aorist passive subjunctive of μερίζω, meaning "to be divided." The phrase thus means "And if a kingdom is divided against itself."

- **οὐ δύναται σταθῆναι ἡ βασιλεία ἐκείνη**: The negation οὐ means "not." The verb δύναται is the third person singular present indicative of δύναμαι, meaning "it is able" or "it can." The verb σταθῆναι is the aorist infinitive of ἵστημι, meaning "to stand." The article ἡ is the nominative singular feminine form of ὁ, meaning "the." The noun βασιλεία is the nominative singular of βασιλεία, meaning "kingdom." The demonstrative pronoun ἐκείνη is the nominative singular feminine form of ἐκεῖνος, meaning "that." The phrase thus means "it cannot stand, that kingdom."

The literal translation of Mark 3:24 is "And if a kingdom is divided against itself, it cannot stand, that kingdom."