---
version: 1
---
- **Καὶ νεανίσκος τις**: The word Καὶ is a conjunction meaning "and." The noun νεανίσκος is in the nominative singular and means "young man" or "youth." The word τις is an indefinite pronoun meaning "a certain" or "someone." So the phrase means "And a certain young man."

- **συνηκολούθει αὐτῷ**: The verb συνηκολούθει is in the imperfect indicative active third person singular form of συνακολουθέω, which means "he was following." The pronoun αὐτῷ is in the dative singular and means "to him" or "with him." So the phrase means "He was following with him."

- **περιβεβλημένος σινδόνα ἐπὶ γυμνοῦ**: The participle περιβεβλημένος is in the nominative singular masculine form of περιβάλλω, meaning "having wrapped." The noun σινδόνα is in the accusative singular and means "a linen cloth" or "a sheet." The preposition ἐπὶ means "on" or "upon." The noun γυμνοῦ is in the genitive singular and means "naked." So the phrase means "Having wrapped a linen cloth around (himself) on (his) naked (body)."

- **καὶ κρατοῦσιν αὐτόν**: The conjunction καὶ means "and." The verb κρατοῦσιν is in the present indicative active third person plural form of κρατέω, meaning "they were holding" or "they were seizing." The pronoun αὐτόν is in the accusative singular and means "him." So the phrase means "And they were holding him."

Literal translation: "And a certain young man was following with him, having wrapped a linen cloth around (his) naked (body), and they were holding him."

In this verse from Mark, we see a description of a young man who was following Jesus. The young man had wrapped a linen cloth around his naked body, and he was being held by others.