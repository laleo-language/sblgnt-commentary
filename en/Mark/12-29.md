---
version: 1
---
- **ἀπεκρίθη ὁ Ἰησοῦς**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, which means "he answered." The subject of the verb is ὁ Ἰησοῦς, meaning "Jesus." The phrase thus means "Jesus answered."

- **ὅτι Πρώτη ἐστίν**: The conjunction ὅτι means "that" and introduces a direct statement. The adjective Πρώτη is the feminine nominative singular of Πρῶτος, which means "first." The verb ἐστίν is the third person singular present indicative of εἰμί, meaning "is." The phrase thus means "that she is first."

- **Ἄκουε, Ἰσραήλ**: The verb Ἄκουε is the second person singular present imperative of ἀκούω, which means "listen" or "hear." The noun Ἰσραήλ is the vocative form of Ἰσραήλ, which means "Israel." The phrase thus means "Listen, Israel."

- **κύριος ὁ θεὸς ἡμῶν κύριος εἷς ἐστιν**: The noun κύριος is the nominative singular of κύριος, which means "Lord." The article ὁ indicates that it is definite. The noun θεὸς is the nominative singular of θεός, which means "God." The article ὁ indicates that it is definite. The possessive pronoun ἡμῶν means "our." The noun κύριος is repeated, followed by the adjective εἷς, which means "one." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The phrase thus means "The Lord, the God of us, is one."

- The literal translation of the entire verse is: "Jesus answered that she is first. Listen, Israel, the Lord our God is one."