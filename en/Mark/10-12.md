---
version: 1
---
- **καὶ**: This is a conjunction meaning "and."
- **ἐὰν**: This is a conjunction meaning "if" or "whenever."
- **αὐτὴ**: This is a pronoun in the nominative singular feminine form of αὐτός, meaning "she" or "her."
- **ἀπολύσασα**: This is a participle in the nominative singular feminine form of ἀπολύω, meaning "having divorced" or "having put away."
- **τὸν ἄνδρα**: This is a noun phrase consisting of the accusative singular masculine form of ὁ ἀνήρ, meaning "the man" or "her husband."
- **αὐτῆς**: This is a pronoun in the genitive singular feminine form of αὐτός, meaning "her."
- **γαμήσῃ**: This is a verb in the aorist subjunctive third person singular form of γαμέω, meaning "she might marry" or "she might wed."
- **ἄλλον**: This is an adjective in the accusative singular masculine form of ἄλλος, meaning "another."
- **μοιχᾶται**: This is a verb in the present indicative third person singular form of μοιχάομαι, meaning "she commits adultery."

The phrase "καὶ ἐὰν αὐτὴ ἀπολύσασα τὸν ἄνδρα αὐτῆς γαμήσῃ ἄλλον" can be translated as "and if she, having divorced her husband, marries another."

The entire verse can be translated as: "And if she, having divorced her husband, marries another, she commits adultery."