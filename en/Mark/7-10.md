---
version: 1
---
- **Μωϋσῆς γὰρ εἶπεν**: The word Μωϋσῆς is the nominative singular of Μωϋσῆς, which means "Moses." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "For Moses said."

- **Τίμα τὸν πατέρα σου καὶ τὴν μητέρα σου**: The word Τίμα is the second person singular present imperative of τιμάω, which means "honor." The article τὸν is the accusative singular form of ὁ, meaning "the." The noun πατέρα is the accusative singular form of πατήρ, which means "father." The conjunction καὶ means "and." The article τὴν is the accusative singular form of ὁ, meaning "the." The noun μητέρα is the accusative singular form of μήτηρ, which means "mother." The phrase thus means "Honor your father and your mother."

- **καί**: The conjunction καί means "and."

- **Ὁ κακολογῶν πατέρα ἢ μητέρα θανάτῳ τελευτάτω**: The article Ὁ is the nominative singular form of ὁ, meaning "the." The participle κακολογῶν is the nominative singular masculine present active participle of κακολογέω, which means "speaking evil of." The noun πατέρα is the accusative singular form of πατήρ, which means "father." The conjunction ἢ means "or." The noun μητέρα is the accusative singular form of μήτηρ, which means "mother." The noun θανάτῳ is the dative singular form of θάνατος, which means "death." The verb τελευτάτω is the third person singular present imperative of τελευτάω, which means "let him die." The phrase thus means "Let the one speaking evil of father or mother die."

The entire verse can be literally translated as: "For Moses said, 'Honor your father and your mother,' and 'Let the one speaking evil of father or mother die.'"

In this verse, there are no complex grammar constructions or ambiguities. It is a straightforward command from Moses to honor one's parents and a warning that those who speak evil of their parents should face severe consequences.