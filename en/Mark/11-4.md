---
version: 1
---
- **καὶ ἀπῆλθον**: The word καὶ is a conjunction meaning "and." The verb ἀπῆλθον is the third person plural aorist indicative of ἀπέρχομαι, which means "they went." The phrase means "and they went."

- **καὶ εὗρον**: The word καὶ is a conjunction meaning "and." The verb εὗρον is the third person plural aorist indicative of εὑρίσκω, which means "they found." The phrase means "and they found."

- **πῶλον δεδεμένον**: The noun πῶλον is the accusative singular of πῶλος, which means "colt" or "young horse." The adjective δεδεμένον is the accusative singular masculine of δεδεμένος, which means "bound" or "tied up." The phrase means "a colt that was bound."

- **πρὸς θύραν ἔξω**: The preposition πρὸς means "to" or "towards." The noun θύραν is the accusative singular of θύρα, which means "door." The adverb ἔξω means "outside." The phrase means "to a door outside."

- **ἐπὶ τοῦ ἀμφόδου**: The preposition ἐπὶ means "on" or "upon." The article τοῦ is the genitive singular masculine of ὁ, which means "the." The noun ἀμφόδος is the genitive singular of ἀμφόδος, which means "path" or "way." The phrase means "on the path."

- **καὶ λύουσιν αὐτόν**: The word καὶ is a conjunction meaning "and." The verb λύουσιν is the third person plural present indicative of λύω, which means "they untie" or "they release." The pronoun αὐτόν is the accusative singular masculine of αὐτός, which means "him" or "it." The phrase means "and they untie him."

Literal translation: "And they went and they found a colt that was bound to a door outside on the path, and they untie him."

The verse describes the actions of a group of people who went and found a colt that was tied up to a door outside on the path, and then they untied the colt.