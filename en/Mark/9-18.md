---
version: 1
---
- **καὶ ὅπου ἐὰν αὐτὸν καταλάβῃ**: The conjunction καὶ means "and." The word ὅπου means "where." The verb καταλάβῃ is the third person singular aorist subjunctive of καταλαμβάνω, which means "to seize" or "to overpower." The phrase thus means "and wherever it seizes him."

- **ῥήσσει αὐτόν**: The verb ῥήσσει is the third person singular present indicative of ῥήσσω, which means "to tear apart" or "to rend." The pronoun αὐτόν is the accusative singular masculine of αὐτός, meaning "him." The phrase thus means "it tears him apart."

- **καὶ ἀφρίζει καὶ τρίζει τοὺς ὀδόντας**: The conjunction καὶ means "and." The verb ἀφρίζει is the third person singular present indicative of ἀφρίζω, which means "to foam." The verb τρίζει is the third person singular present indicative of τρίζω, meaning "to grind" or "to gnash." The article τοὺς is the accusative plural masculine of ὁ, meaning "the." The noun ὀδόντας is the accusative plural masculine of ὀδούς, which means "teeth." The phrase thus means "and he foams and grinds his teeth."

- **καὶ ξηραίνεται**: The conjunction καὶ means "and." The verb ξηραίνεται is the third person singular present indicative of ξηραίνω, which means "to dry up" or "to wither." The phrase thus means "and he becomes dry."

- **καὶ εἶπα τοῖς μαθηταῖς σου**: The conjunction καὶ means "and." The verb εἶπα is the first person singular aorist indicative of λέγω, meaning "I said." The article τοῖς is the dative plural masculine of ὁ, meaning "the." The noun μαθηταῖς is the dative plural feminine of μαθητής, which means "disciples." The pronoun σου is the genitive singular of σύ, meaning "your." The phrase thus means "and I said to your disciples."

- **ἵνα αὐτὸ ἐκβάλωσιν**: The conjunction ἵνα introduces a purpose clause and means "so that" or "in order that." The pronoun αὐτὸ is the accusative singular neuter of αὐτό, meaning "it." The verb ἐκβάλωσιν is the third person plural aorist subjunctive of ἐκβάλλω, which means "to cast out." The phrase thus means "so that they may cast it out."

- **καὶ οὐκ ἴσχυσαν**: The conjunction καὶ means "and." The adverb οὐκ negates the verb. The verb ἴσχυσαν is the third person plural aorist indicative of ἰσχύω, meaning "they were able" or "they had the power." The phrase thus means "and they were not able."

The literal translation of the verse is: "And wherever it seizes him, it tears him apart, and he foams and grinds his teeth and becomes dry; and I said to your disciples so that they may cast it out, and they were not able."