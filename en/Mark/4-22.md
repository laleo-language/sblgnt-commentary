---
version: 1
---
- **οὐ γάρ ἐστιν κρυπτὸν**: The word οὐ is a negation, meaning "not." The word γάρ is a conjunction, meaning "for" or "because." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The adjective κρυπτὸν is the accusative singular neuter form of κρυπτός, meaning "hidden" or "secret." The phrase thus means "For nothing is hidden."

- **ἐὰν μὴ ἵνα φανερωθῇ**: The conjunction ἐὰν introduces a conditional clause, meaning "if." The conjunction μὴ is a negation, meaning "not." The verb φανερωθῇ is the third person singular aorist passive subjunctive of φανερόω, meaning "to be revealed" or "to be made known." The infinitive ἵνα introduces a purpose clause, meaning "in order that." The phrase thus means "if it is not in order to be revealed."

- **οὐδὲ ἐγένετο ἀπόκρυφον**: The word οὐδὲ is a conjunction, meaning "nor" or "neither." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it became." The adjective ἀπόκρυφον is the accusative singular neuter form of ἀπόκρυφος, meaning "hidden" or "concealed." The phrase thus means "Nor did it become hidden."

- **ἵνα ἔλθῃ εἰς φανερόν**: The conjunction ἵνα introduces a purpose clause, meaning "in order that." The verb ἔλθῃ is the third person singular aorist subjunctive of ἔρχομαι, meaning "to come." The preposition εἰς means "into" or "to." The noun φανερόν is the accusative singular neuter form of φανερός, meaning "visible" or "manifest." The phrase thus means "in order that it may come into the open."

The entire verse can be literally translated as: "For nothing is hidden that will not be revealed, nor did it become hidden, but in order that it may come into the open."