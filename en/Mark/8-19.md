---
version: 1
---
- **ὅτε τοὺς πέντε ἄρτους ἔκλασα**: The word ὅτε means "when" and introduces a subordinate clause. The word τοὺς is the accusative plural of the definite article ὁ, meaning "the." The noun πέντε means "five," and the accusative plural form is used here to indicate that there are five loaves. The verb ἔκλασα is the first person singular aorist indicative active of the verb κλάω, meaning "I broke." So this phrase means "when I broke the five loaves."

- **εἰς τοὺς πεντακισχιλίους**: The word εἰς means "into" or "to." The article τοὺς is the accusative plural of ὁ, meaning "the." The adjective πεντακισχίλιοι means "five thousand." So this phrase means "into the five thousand."

- **πόσους κοφίνους κλασμάτων πλήρεις**: The word πόσους means "how many." The noun κοφίνος means "basket." The genitive plural form κλασμάτων is a genitive absolute construction, which means it functions independently of the main clause and is translated as "of broken pieces." The noun πλήρεις is the second person singular present indicative active of the verb πληρόω, meaning "you fill." So this phrase means "how many baskets of broken pieces do you fill?"

- **ἤρατε**: The verb ἤρατε is the second person plural aorist indicative active of αἴρω, meaning "you took." It is used here in the sense of "you collected" or "you gathered." 

- **λέγουσιν αὐτῷ**: The verb λέγουσιν is the third person plural present indicative active of λέγω, meaning "they say." The pronoun αὐτῷ means "to him" or "to Jesus."

- **Δώδεκα**: The word Δώδεκα means "twelve."

The verse can be literally translated as: "When I broke the five loaves for the five thousand, how many baskets of broken pieces do you fill?" They said to him, "Twelve."

This verse describes the scene where Jesus miraculously feeds a large crowd with only five loaves of bread and two fish. The phrase "how many baskets of broken pieces do you fill?" emphasizes the abundance of food that was left over after everyone had eaten. The response of "Twelve" indicates that there were twelve baskets filled with leftovers.