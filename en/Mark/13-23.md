---
version: 1
---
- **ὑμεῖς δὲ βλέπετε**: The pronoun ὑμεῖς is the second person plural, meaning "you all." The verb βλέπετε is the second person plural present imperative of βλέπω, meaning "you all see" or "you all watch." The phrase is a command, telling the listeners to pay attention or be watchful.

- **προείρηκα ὑμῖν**: The verb προείρηκα is the first person singular perfect indicative of προλέγω, meaning "I have foretold" or "I have told beforehand." The pronoun ὑμῖν is the dative plural of ὑμεῖς, meaning "to you all." The phrase means "I have foretold to you all."

- **πάντα**: The word πάντα is the accusative plural of πᾶς, meaning "all" or "everything." It is used here as the direct object of the verb προείρηκα. The phrase means "all things" or "everything."

The entire verse can be translated as: "But you all watch; I have foretold everything to you all."