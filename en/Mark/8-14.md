---
version: 1
---
- **Καὶ ἐπελάθοντο λαβεῖν ἄρτους**: The conjunction καὶ means "and." The verb ἐπελάθοντο is the third person plural aorist middle indicative of ἐπιλανθάνομαι, which means "they forgot." The infinitive λαβεῖν is the aorist infinitive of λαμβάνω, meaning "to take." The noun ἄρτους is the accusative plural of ἄρτος, which means "bread." The phrase thus means "And they forgot to take bread."

- **καὶ εἰ μὴ ἕνα ἄρτον οὐκ εἶχον μεθʼ ἑαυτῶν ἐν τῷ πλοίῳ**: The conjunction καὶ means "and." The conjunction εἰ means "if." The particle μὴ is a negation particle, meaning "not." The number εἷς is the masculine singular form of the cardinal number εἷς, meaning "one." The noun ἄρτον is the accusative singular of ἄρτος, which means "bread." The verb εἶχον is the third person plural imperfect indicative of ἔχω, meaning "they had." The preposition μεθʼ means "with." The reflexive pronoun ἑαυτῶν means "themselves." The preposition ἐν means "in." The article τῷ is the masculine singular dative form of the definite article ὁ, meaning "the." The noun πλοίῳ is the dative singular of πλοῖον, which means "boat." The phrase thus means "And if they did not have one bread with themselves in the boat."

The literal translation of Mark 8:14 is: "And they forgot to take bread, and if they did not have one bread with themselves in the boat."