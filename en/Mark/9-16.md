---
version: 1
---
- **καὶ ἐπηρώτησεν αὐτούς**: The conjunction καὶ means "and" and connects this phrase to the previous one. The verb ἐπηρώτησεν is the third person singular aorist indicative of ἐπερωτάω, meaning "he asked." The pronoun αὐτούς is the accusative plural of αὐτός, meaning "them." The phrase can be translated as "and he asked them."

- **Τί συζητεῖτε**: The interrogative pronoun Τί means "what." The verb συζητεῖτε is the second person plural present indicative of συζητέω, meaning "you discuss" or "you argue." The phrase can be translated as "What are you discussing?"

- **πρὸς αὑτούς**: The preposition πρός means "to" or "towards." The pronoun αὑτούς is the accusative plural of αὐτός, meaning "yourselves" or "each other." The phrase can be translated as "among yourselves" or "with each other."

Literal translation: "And he asked them, 'What are you discussing among yourselves?'"