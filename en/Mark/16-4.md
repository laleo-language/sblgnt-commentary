---
version: 1
---
- **καὶ ἀναβλέψασαι**: The conjunction καὶ means "and" and ἀναβλέψασαι is the aorist participle of ἀναβλέπω, which means "to look up" or "to gaze." The phrase καὶ ἀναβλέψασαι translates to "and having looked up."

- **θεωροῦσιν**: The verb θεωροῦσιν is the present indicative third person plural of θεωρέω, meaning "they see" or "they observe." The phrase θεωροῦσιν translates to "they see."

- **ὅτι ἀποκεκύλισται ὁ λίθος**: The conjunction ὅτι introduces a subordinate clause and can mean "that" or "because." The verb ἀποκεκύλισται is the perfect indicative passive third person singular of ἀποκυλίω, which means "to be rolled away." The article ὁ before λίθος indicates that it is a specific stone. The phrase ὅτι ἀποκεκύλισται ὁ λίθος translates to "that the stone has been rolled away."

- **ἦν γὰρ μέγας σφόδρα**: The verb ἦν is the imperfect indicative third person singular of εἰμί, meaning "he was" or "it was." The adverb μέγας means "great" or "big," and the adverb σφόδρα means "exceedingly" or "very." The phrase ἦν γὰρ μέγας σφόδρα translates to "for it was very big."

The literal translation of Mark 16:4 is:

"And having looked up, they see that the stone has been rolled away, for it was very big."