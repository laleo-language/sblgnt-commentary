---
version: 1
---
- **καὶ εἰσελθοῦσαι εἰς τὸ μνημεῖον**: The conjunction καὶ means "and." The participle εἰσελθοῦσαι is the nominative plural feminine form of the verb εἰσέρχομαι, meaning "having entered" or "entering." The preposition εἰς means "into." The article τὸ is the nominative singular neuter form of the definite article, meaning "the." The noun μνημεῖον is the accusative singular form of μνημεῖον, which means "tomb" or "monument." The phrase thus means "and having entered into the tomb."

- **εἶδον νεανίσκον καθήμενον**: The verb εἶδον is the first person singular aorist indicative of ὁράω, meaning "I saw." The noun νεανίσκον is the accusative singular form of νεανίσκος, which means "young man" or "youth." The participle καθήμενον is the accusative singular masculine form of the verb κάθημαι, meaning "sitting." The phrase thus means "I saw a young man sitting."

- **ἐν τοῖς δεξιοῖς περιβεβλημένον στολὴν λευκήν**: The preposition ἐν means "in" or "on." The article τοῖς is the dative plural masculine form of the definite article, meaning "the." The adjective δεξιοῖς is the dative plural masculine form of δεξιός, which means "right." The participle περιβεβλημένον is the accusative singular masculine form of the verb περιβάλλω, meaning "having put on" or "wearing." The noun στολὴν is the accusative singular form of στολή, which means "garment" or "robe." The adjective λευκήν is the accusative singular feminine form of λευκός, meaning "white." The phrase thus means "wearing a white garment on his right side."

- **καὶ ἐξεθαμβήθησαν**: The conjunction καὶ means "and." The verb ἐξεθαμβήθησαν is the third person plural aorist passive indicative of ἐκθαμβέω, meaning "they were amazed" or "they were astonished." The phrase thus means "and they were amazed."

Literal translation: "And having entered into the tomb, I saw a young man sitting, wearing a white garment on his right side, and they were amazed."