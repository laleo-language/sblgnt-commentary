---
version: 1
---
- **καὶ ἐὰν οἰκία ἐφʼ ἑαυτὴν μερισθῇ**: The conjunction καὶ means "and." The conjunction ἐὰν introduces a conditional clause and can be translated as "if" or "when." The noun οἰκία is in the nominative singular, meaning "house." The preposition ἐφʼ means "upon" or "on." The reflexive pronoun ἑαυτὴν is in the accusative singular and means "itself." The verb μερισθῇ is the third person singular aorist passive subjunctive of μερίζω, meaning "to be divided." The phrase can be translated as "and if a house is divided against itself."

- **οὐ δυνήσεται ἡ οἰκία ἐκείνη σταθῆναι**: The negation οὐ means "not." The verb δυνήσεται is the third person singular future indicative of δύναμαι, meaning "to be able" or "to have power." The definite article ἡ is in the nominative singular and means "the." The noun οἰκία is in the nominative singular, meaning "house." The demonstrative pronoun ἐκείνη is in the nominative singular and means "that" or "the." The verb σταθῆναι is the aorist infinitive of ἵστημι, meaning "to stand." The phrase can be translated as "the house will not be able to stand."

The literal translation of Mark 3:25 is: "And if a house is divided against itself, that house will not be able to stand."