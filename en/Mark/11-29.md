---
version: 1
---
- **ὁ δὲ ⸀Ἰησοῦς εἶπεν αὐτοῖς**: The definite article ὁ indicates that the noun Ἰησοῦς is being referred to as a specific person, so it means "Jesus." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτοῖς is the dative plural form of αὐτός, meaning "to them." So this phrase means "But Jesus said to them."

- **Ἐπερωτήσω ⸀ὑμᾶς ἕνα λόγον**: The verb Ἐπερωτήσω is the first person singular future indicative of ἐπερωτάω, meaning "I will ask." The pronoun ὑμᾶς is the accusative plural form of σύ, meaning "you." The noun λόγον is in the accusative singular form of λόγος, meaning "a word" or "a question." So this phrase means "I will ask you a question."

- **καὶ ἀποκρίθητέ μοι**: The conjunction καὶ means "and." The verb ἀποκρίθητέ is the second person plural aorist imperative of ἀποκρίνομαι, meaning "you answer." The pronoun μοι is the dative singular form of ἐγώ, meaning "to me." So this phrase means "And you answer me."

- **καὶ ἐρῶ ὑμῖν ἐν ποίᾳ ἐξουσίᾳ ταῦτα ποιῶ**: The conjunction καὶ means "and." The verb ἐρῶ is the first person singular future indicative of λέγω, meaning "I will say." The pronoun ὑμῖν is the dative plural form of σύ, meaning "to you." The preposition ἐν means "in." The interrogative pronoun ποίᾳ means "by what" or "in what." The noun ἐξουσίᾳ is in the dative singular form of ἐξουσία, meaning "authority." The demonstrative pronoun ταῦτα means "these" or "those." The verb ποιῶ is the present indicative of ποιέω, meaning "I do" or "I make." So this phrase means "And I will say to you, by what authority I do these things."

- The literal translation of this verse is: "But Jesus said to them, 'I will ask you a question. And you answer me. And I will say to you, by what authority I do these things.'"