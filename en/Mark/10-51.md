---
version: 1
---
- **καὶ ἀποκριθεὶς αὐτῷ ὁ Ἰησοῦς εἶπεν**: The word καὶ is a conjunction meaning "and." The verb ἀποκριθεὶς is the nominative singular participle of ἀποκρίνομαι, meaning "having answered." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The noun ὁ Ἰησοῦς is the nominative singular of Ἰησοῦς, meaning "Jesus." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "And Jesus, having answered him, said."

- **Τί σοι θέλεις ποιήσω**: The pronoun Τί is the accusative singular of τίς, meaning "what." The pronoun σοι is the dative singular of σύ, meaning "to you." The verb θέλεις is the second person singular present indicative of θέλω, meaning "you want." The verb ποιήσω is the first person singular future indicative of ποιέω, meaning "I will do." The phrase thus means "What do you want me to do for you?"

- **ὁ δὲ τυφλὸς εἶπεν αὐτῷ**: The article ὁ is the nominative singular of ὁ, meaning "the." The noun δὲ is a conjunction meaning "and." The adjective τυφλὸς is the nominative singular of τυφλός, meaning "blind." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "And the blind man said to him."

- **Ραββουνι, ἵνα ἀναβλέψω**: The noun Ραββουνι is a transliteration of the Hebrew word for "my master" or "my teacher." The conjunction ἵνα is a subordinating conjunction meaning "so that" or "in order that." The verb ἀναβλέψω is the first person singular aorist subjunctive of ἀναβλέπω, meaning "I may see again." The phrase thus means "Master, so that I may see again."

The literal translation of Mark 10:51 is: "And Jesus, having answered him, said, 'What do you want me to do for you?' And the blind man said to him, 'Master, so that I may see again.'"