---
version: 1
---
- **οὕτως, λέγω ὑμῖν**: The word οὕτως means "thus" or "in this way." The verb λέγω is the first person singular present indicative of λέγω, meaning "I say" or "I am saying." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The phrase thus means "I say to you in this way."

- **γίνεται χαρὰ ἐνώπιον τῶν ἀγγέλων τοῦ θεοῦ**: The verb γίνεται is the third person singular present indicative of γίνομαι, meaning "it happens" or "it becomes." The noun χαρὰ is the nominative singular of χαρά, meaning "joy" or "rejoicing." The preposition ἐνώπιον means "before" or "in the presence of." The genitive plural τῶν ἀγγέλων τοῦ θεοῦ means "of the angels of God." The phrase thus means "there is joy before the angels of God."

- **ἐπὶ ἑνὶ ἁμαρτωλῷ μετανοοῦντι**: The preposition ἐπὶ means "over" or "concerning." The cardinal number ἑνὶ means "one." The noun ἁμαρτωλῷ is the dative singular of ἁμαρτωλός, meaning "sinner." The participle μετανοοῦντι is the present active dative singular of μετανοέω, meaning "repenting" or "turning back." The phrase thus means "concerning one sinner who is repenting."

The literal translation of Luke 15:10 is "Thus, I say to you, there is joy before the angels of God over one sinner who is repenting."