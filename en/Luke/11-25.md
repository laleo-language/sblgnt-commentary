---
version: 1
---
- **καὶ ἐλθὸν**: The word καὶ is a conjunction meaning "and." The verb ἐλθὸν is the aorist participle of ἔρχομαι, "to come." The phrase means "and coming."

- **εὑρίσκει**: The verb εὑρίσκει is the present indicative active third person singular of εὑρίσκω, "to find." The phrase means "he finds."

- **σεσαρωμένον**: The word σεσαρωμένον is the perfect passive participle accusative singular of σαρόω, "to sweep." The phrase means "swept."

- **καὶ κεκοσμημένον**: The word καὶ is a conjunction meaning "and." The verb κεκοσμημένον is the perfect passive participle accusative singular of κοσμέω, "to adorn" or "to arrange." The phrase means "and adorned" or "and arranged."

The literal translation of the verse is: "And coming, he finds it swept and adorned."