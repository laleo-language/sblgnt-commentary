---
version: 1
---
- **Σίμωνα**: The word Σίμωνα is the accusative singular of Σίμων, which is the name "Simon."
- **ὃν**: The word ὃν is the accusative singular masculine of ὅς, ἥ, ὅ, which is a relative pronoun meaning "whom" or "which."
- **καὶ**: The word καὶ is a conjunction meaning "and."
- **ὠνόμασεν**: The word ὠνόμασεν is the third person singular aorist indicative active of ὀνομάζω, which means "he named" or "he called."
- **Πέτρον**: The word Πέτρον is the accusative singular of Πέτρος, which is the name "Peter."
- **καὶ**: The word καὶ is a conjunction meaning "and."
- **Ἀνδρέαν**: The word Ἀνδρέαν is the accusative singular of Ἀνδρέας, which is the name "Andrew."
- **τὸν**: The word τὸν is the accusative singular masculine of ὁ, ἡ, τό, which is the definite article meaning "the."
- **ἀδελφὸν**: The word ἀδελφὸν is the accusative singular of ἀδελφός, which means "brother."
- **αὐτοῦ**: The word αὐτοῦ is the genitive singular masculine of αὐτός, which means "his."
- **καὶ**: The word καὶ is a conjunction meaning "and."
- **Ἰάκωβον**: The word Ἰάκωβον is the accusative singular of Ἰάκωβος, which is the name "James."
- **καὶ**: The word καὶ is a conjunction meaning "and."
- **Ἰωάννην**: The word Ἰωάννην is the accusative singular of Ἰωάννης, which is the name "John."
- **καὶ**: The word καὶ is a conjunction meaning "and."
- **Φίλιππον**: The word Φίλιππον is the accusative singular of Φίλιππος, which is the name "Philip."
- **καὶ**: The word καὶ is a conjunction meaning "and."
- **Βαρθολομαῖον**: The word Βαρθολομαῖον is the accusative singular of Βαρθολομαῖος, which is the name "Bartholomew."

Literal translation: "Simon whom he also named Peter and Andrew his brother and James and John and Philip and Bartholomew."

In this verse, we see a list of names of Jesus' disciples. Jesus named Simon "Peter," and Simon's brother Andrew is also mentioned. James, John, Philip, and Bartholomew are also listed as his disciples.