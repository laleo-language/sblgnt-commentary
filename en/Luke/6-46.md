---
version: 1
---
- **Τί δέ με καλεῖτε**: The word Τί is the nominative singular of τίς, which means "what." The word δέ is a conjunction that can be translated as "but" or "and." The pronoun με is the accusative singular of ἐγώ, meaning "me." The verb καλεῖτε is the second person plural present indicative of καλέω, which means "you call" or "you address." The phrase can be translated as "But why do you call me?"

- **Κύριε κύριε**: The word Κύριε is the vocative singular of κύριος, which means "Lord" or "Master." In this phrase, it is repeated twice for emphasis. The phrase can be translated as "Lord, Lord."

- **καὶ οὐ ποιεῖτε ἃ λέγω**: The conjunction καὶ means "and." The adverb οὐ is a negation that can be translated as "not." The verb ποιεῖτε is the second person plural present indicative of ποιέω, meaning "you do" or "you perform." The relative pronoun ἃ is the accusative plural neuter of ὅς, which means "what" or "which." The verb λέγω is the first person singular present indicative of λέγω, meaning "I say" or "I speak." The phrase can be translated as "And you are not doing what I say."

The literal translation of the entire verse is: "But why do you call me, Lord, Lord, and you are not doing what I say?"