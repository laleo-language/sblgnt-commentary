---
version: 1
---
- **Ἀναστὰς δὲ ἀπὸ τῆς συναγωγῆς**: The word Ἀναστὰς is the nominative singular masculine of ἀνίστημι, which means "he arose" or "he got up." The conjunction δὲ connects this phrase with the previous one and can be translated as "and." The preposition ἀπὸ means "from." The article τῆς is the genitive singular feminine of ὁ, which means "the." The noun συναγωγῆς is the genitive singular of συναγωγή, which means "synagogue." The phrase thus means "And he arose from the synagogue."

- **εἰσῆλθεν εἰς τὴν οἰκίαν Σίμωνος**: The verb εἰσῆλθεν is the third person singular aorist indicative of εἰσέρχομαι, which means "he entered" or "he went into." The preposition εἰς means "into." The article τὴν is the accusative singular feminine of ὁ. The noun οἰκίαν is the accusative singular of οἰκία, which means "house." The genitive Σίμωνος indicates possession and means "of Simon." The phrase thus means "He entered into Simon's house."

- **πενθερὰ δὲ τοῦ Σίμωνος**: The noun πενθερὰ is the nominative singular feminine of πενθερά, which means "mother-in-law." The conjunction δὲ connects this phrase with the previous one and can be translated as "and." The genitive τοῦ is the genitive singular masculine of ὁ. The noun Σίμωνος is the genitive singular of Σίμων, which is the name "Simon." The phrase thus means "And Simon's mother-in-law."

- **ἦν συνεχομένη πυρετῷ μεγάλῳ**: The verb ἦν is the third person singular imperfect indicative of εἰμί, which means "she was." The participle συνεχομένη is the present middle/passive nominative singular feminine of σέχω, which means "to have" or "to hold." The dative noun πυρετῷ is the dative singular of πυρετός, which means "fever." The adjective μεγάλῳ is the dative singular masculine of μέγας, which means "great" or "large." The phrase thus means "She was being held by a great fever."

- **καὶ ἠρώτησαν αὐτὸν περὶ αὐτῆς**: The conjunction καὶ connects this phrase with the previous one and can be translated as "and." The verb ἠρώτησαν is the third person plural aorist indicative of ἐρωτάω, which means "they asked." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, which means "him." The preposition περὶ means "about." The pronoun αὐτῆς is the genitive singular feminine of αὐτός, which means "her." The phrase thus means "And they asked him about her."

The literal translation of Luke 4:38 is: "And he arose from the synagogue and entered into Simon's house. And Simon's mother-in-law was being held by a great fever, and they asked him about her."