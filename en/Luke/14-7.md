---
version: 1
---
- **Ἔλεγεν δὲ**: The verb ἔλεγεν is the third person singular imperfect indicative of λέγω, meaning "he was saying" or "he said." The particle δὲ is a conjunction that can be translated as "and" or "but." The phrase thus means "But he was saying."

- **πρὸς τοὺς κεκλημένους**: The preposition πρὸς can be translated as "to" or "towards." The definite article τοὺς is the accusative plural of ὁ, meaning "the." The verb κεκλημένους is the perfect passive participle of καλέω, meaning "called" or "invited." The phrase can be translated as "to the invited guests."

- **παραβολήν**: The noun παραβολήν is the accusative singular of παραβολή, meaning "parable" or "illustration." The phrase can be translated as "a parable."

- **ἐπέχων**: The verb ἐπέχων is the participle of ἔχω, meaning "having" or "holding." The phrase can be translated as "having."

- **πῶς τὰς πρωτοκλισίας ἐξελέγοντο**: The adverb πῶς can be translated as "how" or "in what way." The definite article τὰς is the accusative plural of ὁ, meaning "the." The noun πρωτοκλισίας is the accusative plural of πρωτοκλισία, meaning "chief seats" or "places of honor." The verb ἐξελέγοντο is the imperfect middle indicative of ἐκλέγομαι, meaning "they were choosing" or "they were selecting." The phrase can be translated as "how the chief seats were being chosen."

- **λέγων πρὸς αὐτούς**: The participle λέγων is the present active participle of λέγω, meaning "saying" or "speaking." The preposition πρὸς can be translated as "to" or "towards." The pronoun αὐτούς is the accusative plural of αὐτός, meaning "them." The phrase can be translated as "saying to them."

The literal translation of the entire verse would be: "But he was saying to the invited guests a parable, having how the chief seats were being chosen, saying to them."