---
version: 1
---
- **Ἦν δὲ ἐκεῖ ἀγέλη χοίρων ἱκανῶν**: The word Ἦν is the third person singular imperfect indicative of εἰμί, meaning "he/she/it was." The word δὲ is a conjunction meaning "and" or "but." The word ἐκεῖ is an adverb meaning "there." The word ἀγέλη is the nominative singular of ἀγέλη, which means "herd" or "flock." The word χοίρων is the genitive plural of χοίρος, meaning "pig" or "swine." The word ἱκανῶν is the genitive plural of ἱκανός, meaning "many" or "a large number." The phrase thus means "And there was a large herd of pigs."

- **⸀βοσκομένη ἐν τῷ ὄρει**: The word ⸀βοσκομένη is the nominative singular feminine participle of βόσκω, meaning "grazing" or "feeding." The word ἐν is a preposition meaning "in" or "on." The word τῷ is the dative singular masculine/feminine/neuter definite article, meaning "the." The word ὄρει is the dative singular masculine/neuter of ὄρος, meaning "mountain." The phrase thus means "grazing on the mountain."

- **καὶ ⸀παρεκάλεσαν αὐτὸν**: The word καὶ is a conjunction meaning "and" or "but." The word ⸀παρεκάλεσαν is the third person plural aorist indicative of παρακαλέω, meaning "they begged" or "they implored." The word αὐτὸν is the accusative singular masculine pronoun, meaning "him." The phrase thus means "And they begged him."

- **ἵνα ἐπιτρέψῃ αὐτοῖς εἰς ἐκείνους εἰσελθεῖν**: The word ἵνα is a conjunction meaning "that" or "so that." The word ἐπιτρέψῃ is the third person singular aorist subjunctive of ἐπιτρέπω, meaning "he might permit" or "he might allow." The word αὐτοῖς is the dative plural masculine/neuter pronoun, meaning "them." The word εἰς is a preposition meaning "into" or "to." The word ἐκείνους is the accusative plural masculine pronoun, meaning "those." The word εἰσελθεῖν is the aorist infinitive of εἰσέρχομαι, meaning "to enter." The phrase thus means "that he might permit them to enter into those."

- **καὶ ἐπέτρεψεν αὐτοῖς**: The word καὶ is a conjunction meaning "and" or "but." The word ἐπέτρεψεν is the third person singular aorist indicative of ἐπιτρέπω, meaning "he permitted" or "he allowed." The word αὐτοῖς is the dative plural masculine/neuter pronoun, meaning "them." The phrase thus means "And he permitted them."

The literal translation of Luke 8:32 is: "And there was a large herd of pigs grazing on the mountain; and they begged him that he might permit them to enter into those; and he permitted them."