---
version: 1
---
- **καὶ εἶπεν ὁ δοῦλος**: The word καὶ is a conjunction meaning "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The article ὁ is the masculine singular nominative form, meaning "the." The noun δοῦλος is the masculine singular nominative form, meaning "servant." The phrase thus means "and the servant said."

- **Κύριε, γέγονεν ὃ ἐπέταξας**: The word Κύριε is the vocative form of κύριος, meaning "Lord." The verb γέγονεν is the third person singular perfect indicative of γίνομαι, meaning "it has happened" or "it has become." The pronoun ὃ is the neuter singular accusative form, meaning "what." The verb ἐπέταξας is the second person singular aorist indicative of ἐπιτάσσω, meaning "you commanded." The phrase thus means "Lord, what you commanded has happened."

- **καὶ ἔτι τόπος ἐστίν**: The word καὶ is a conjunction meaning "and." The adverb ἔτι means "still" or "yet." The noun τόπος is the masculine singular nominative form, meaning "place." The verb ἐστίν is the third person singular present indicative of εἰμί, meaning "it is." The phrase thus means "and there is still place."

The literal translation of Luke 14:22 is: "And the servant said, 'Lord, what you commanded has happened, and there is still place.'"