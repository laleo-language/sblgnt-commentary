---
version: 1
---
- **παραγενόμενοι δὲ πρὸς αὐτὸν οἱ ἄνδρες**: The participle παραγενόμενοι is the
  nominative masculine plural of παραγίνομαι, meaning "having come" or "having
  arrived." The preposition πρὸς means "to" or "toward," and αὐτὸν is the accusative
  singular of αὐτός, meaning "him." The noun οἱ ἄνδρες means "the men." The phrase
  thus means "the men having come to him."

- **εἶπαν· Ἰωάννης ὁ βαπτιστὴς**: The verb εἶπαν is the third person plural aorist
  indicative of λέγω, meaning "they said." The noun Ἰωάννης is the nominative
  singular of Ἰωάννης, which is the Greek name "John." The phrase thus means
  "they said, 'John the Baptist.'"

- **ἀπέστειλεν ἡμᾶς πρὸς σὲ λέγων**: The verb ἀπέστειλεν is the third person singular
  aorist indicative of ἀποστέλλω, meaning "he sent." The pronoun ἡμᾶς is the
  accusative plural of ἐγώ, meaning "us." The preposition πρὸς means "to" or
  "toward," and σὲ is the accusative singular of σύ, meaning "you." The present
  participle λέγων is the nominative masculine singular of λέγω, meaning "saying."
  The phrase thus means "he sent us to you, saying."

- **Σὺ εἶ ὁ ἐρχόμενος ἢ ἄλλον προσδοκῶμεν**: The pronoun Σὺ means "you." The verb εἶ
  is the second person singular present indicative of εἰμί, meaning "you are." The
  article ὁ is the nominative singular masculine definite article, meaning "the."
  The participle ἐρχόμενος is the nominative masculine singular of ἔρχομαι, meaning
  "coming." The conjunction ἢ means "or." The pronoun ἄλλον is the accusative
  singular of ἄλλος, meaning "another." The verb προσδοκῶμεν is the first person
  plural present indicative of προσδοκάω, meaning "we are expecting." The phrase
  thus means "Are you the coming one, or should we expect another?"

The entire verse can be translated as: "When the men came to him, they said, 'John the Baptist sent us to you, saying, 'Are you the coming one, or should we expect another?'"