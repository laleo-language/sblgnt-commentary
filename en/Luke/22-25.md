---
version: 1
---
- **ὁ δὲ εἶπεν αὐτοῖς**: The article ὁ is the nominative singular masculine of ὁ, which means "the." The conjunction δὲ indicates a contrast or continuation of thought and can be translated as "but" or "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "but he said to them."

- **Οἱ βασιλεῖς τῶν ἐθνῶν κυριεύουσιν αὐτῶν**: The article Οἱ is the nominative plural masculine of ὁ, which means "the." The noun βασιλεῖς is the nominative plural of βασιλεύς, meaning "kings." The preposition τῶν is the genitive plural of ὁ, meaning "of." The noun ἐθνῶν is the genitive plural of ἔθνος, meaning "nations" or "Gentiles." The verb κυριεύουσιν is the third person plural present indicative of κυριεύω, meaning "they rule." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "the kings of the nations rule over them."

- **καὶ οἱ ἐξουσιάζοντες αὐτῶν εὐεργέται καλοῦνται**: The conjunction καὶ means "and." The article οἱ is the nominative plural masculine of ὁ, which means "the." The verb ἐξουσιάζοντες is the present active participle nominative plural masculine of ἐξουσιάζω, meaning "those who have authority" or "those who exercise authority." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The noun εὐεργέται is the nominative plural masculine of εὐεργέτης, meaning "benefactors." The verb καλοῦνται is the third person plural present indicative passive of καλέω, meaning "they are called." The phrase thus means "and those who exercise authority over them are called benefactors."

The sentence as a whole can be translated as: "But he said to them, 'The kings of the nations rule over them, and those who exercise authority over them are called benefactors.'"

In this verse, Jesus is contrasting the way rulers in the world exercise authority with the way his disciples should act as leaders. He is pointing out that true leadership is not about exerting power and dominance, but about serving others and doing good for them.