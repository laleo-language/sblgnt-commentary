---
version: 1
---
- **Καὶ εἶπεν Μαριάμ**: The word Καὶ is a conjunction meaning "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he/she said." The name Μαριάμ refers to Mary. The phrase thus means "And Mary said."

- **Μεγαλύνει ἡ ψυχή μου τὸν κύριον**: The verb Μεγαλύνει is the third person singular present indicative of μεγαλύνω, meaning "he/she magnifies." The article ἡ is the feminine singular nominative definite article, meaning "the." The noun ψυχή is the feminine singular nominative of ψυχή, meaning "soul." The pronoun μου is the first person singular genitive pronoun, meaning "my." The article τὸν is the masculine singular accusative definite article, meaning "the." The noun κύριον is the masculine singular accusative of κύριος, meaning "Lord." The phrase thus means "My soul magnifies the Lord."

- The entire verse translates to: "And Mary said, 'My soul magnifies the Lord.'"