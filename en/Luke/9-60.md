---
version: 1
---
- **εἶπεν δὲ αὐτῷ**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase means "he said to him."

- **Ἄφες τοὺς νεκροὺς θάψαι τοὺς ἑαυτῶν νεκρούς**: The verb Ἄφες is the second person singular present imperative of ἀφίημι, meaning "allow" or "let." The article τοὺς is the accusative plural of ὁ, meaning "the." The noun νεκροὺς is the accusative plural of νεκρός, meaning "dead." The infinitive θάψαι is the aorist infinitive of θάπτω, meaning "to bury." The article τοὺς is again the accusative plural of ὁ. The pronoun ἑαυτῶν is the genitive plural of ἑαυτοῦ, meaning "their own." The phrase means "allow the dead to bury their own dead."

- **σὺ δὲ ἀπελθὼν διάγγελλε τὴν βασιλείαν τοῦ θεοῦ**: The pronoun σὺ is the second person singular pronoun, meaning "you." The conjunction δὲ is a connective particle, often translated as "but" or "and." The participle ἀπελθὼν is the nominative singular masculine aorist active participle of ἀπέρχομαι, meaning "having gone." The verb διάγγελλε is the second person singular present imperative of διαγγέλλω, meaning "proclaim" or "declare." The article τὴν is the accusative singular feminine of ὁ, meaning "the." The noun βασιλείαν is the accusative singular of βασιλεία, meaning "kingdom." The article τοῦ is the genitive singular masculine of ὁ. The noun θεοῦ is the genitive singular of θεός, meaning "God." The phrase means "but you, having gone, proclaim the kingdom of God."

The literal translation of Luke 9:60 is: "But he said to him, 'Allow the dead to bury their own dead, but you, having gone, proclaim the kingdom of God.'"