---
version: 1
---
- **αὐτὸς δὲ εἶπεν**: The word αὐτὸς is the pronoun meaning "he himself" or "he." The word δὲ is a conjunction that can be translated as "but" or "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said." The phrase thus means "he himself said."

- **Μενοῦν μακάριοι**: The word Μενοῦν is an adverb that means "indeed" or "truly." The adjective μακάριοι is the nominative plural of μακάριος, which means "blessed" or "happy." The phrase can be translated as "indeed blessed" or "truly happy."

- **οἱ ἀκούοντες τὸν λόγον τοῦ θεοῦ**: The word οἱ is the definite article in its nominative plural form, meaning "the." The participle ἀκούοντες is the nominative plural present active participle of ἀκούω, which means "hearing" or "listening." The noun λόγον is the accusative singular of λόγος, which means "word" or "message." The genitive τοῦ θεοῦ is the genitive singular of θεός, meaning "of God." The phrase can be translated as "those who hear the word of God."

- **καὶ φυλάσσοντες**: The conjunction καὶ means "and." The participle φυλάσσοντες is the nominative plural present active participle of φυλάσσω, which means "guarding" or "keeping." The phrase can be translated as "and keeping."

The entire verse can be translated as "He himself said, 'Truly blessed are those who hear the word of God and keep it.'"