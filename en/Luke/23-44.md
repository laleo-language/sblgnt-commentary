---
version: 1
---
- **Καὶ ἦν**: The conjunction καὶ means "and," and the verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he was." The phrase thus means "And he was."

- **ἤδη ὡσεὶ ὥρα ἕκτη**: The adverb ἤδη means "already," and the adverb ὡσεὶ means "about" or "approximately." The noun ὥρα is in the nominative singular and means "hour." The adjective ἕκτη is in the nominative singular feminine form of ἕκτος, meaning "sixth." The phrase thus means "It was already about the sixth hour."

- **καὶ σκότος ἐγένετο**: The conjunction καὶ means "and," and the noun σκότος is in the nominative singular form meaning "darkness." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it became." The phrase thus means "And darkness came."

- **ἐφʼ ὅλην τὴν γῆν**: The preposition ἐφʼ means "on" or "over," and the article τὴν is the accusative singular feminine form of ὁ, meaning "the." The noun γῆν is in the accusative singular form meaning "earth" or "land." The phrase thus means "over the whole earth."

- **ἕως ὥρας ἐνάτης**: The preposition ἕως means "until," and the noun ὥρας is in the genitive singular form meaning "hour." The adjective ἐνάτης is in the genitive singular feminine form of ἐνάτος, meaning "ninth." The phrase thus means "until the ninth hour."

Literal translation: And he was already about the sixth hour, and darkness came over the whole earth until the ninth hour.