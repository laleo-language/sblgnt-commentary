---
version: 1
---
- **ὅτε δὲ ὁ υἱός σου οὗτος**: The word ὅτε is a conjunction meaning "when." The word δὲ is a conjunction meaning "but" or "and." The word υἱός is the nominative singular form of υἱός, which means "son." The word σου is the genitive singular form of σύ, which means "your." The word οὗτος is a pronoun meaning "this." The phrase thus means "But when this son of yours."

- **ὁ καταφαγών σου τὸν βίον**: The word ὁ is the definite article meaning "the." The word καταφαγών is the nominative singular masculine form of καταφαγών, which is the aorist active participle of κατεσθίω, meaning "to devour" or "to eat up." The word σου is the genitive singular form of σύ, which means "your." The word τὸν is the accusative singular masculine form of ὁ, meaning "the." The word βίον is the accusative singular form of βίος, which means "livelihood" or "property." The phrase thus means "the one who devoured your livelihood."

- **μετὰ πορνῶν ἦλθεν**: The word μετὰ is a preposition meaning "with." The word πορνῶν is the genitive plural form of πόρνη, which means "prostitutes." The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "he came." The phrase thus means "he came with prostitutes."

- **ἔθυσας αὐτῷ τὸν σιτευτὸν μόσχον**: The verb ἔθυσας is the second person singular aorist indicative of θύω, meaning "you sacrificed." The word αὐτῷ is the dative singular form of αὐτός, which means "to him." The word τὸν is the accusative singular masculine form of ὁ, meaning "the." The word σιτευτὸν is the accusative singular masculine form of σιτευτός, which means "fattened" or "fatted." The word μόσχον is the accusative singular form of μόσχος, which means "calf." The phrase thus means "you sacrificed to him the fattened calf."

- The literal translation of the entire verse is: "But when this son of yours, the one who devoured your livelihood, came with prostitutes, you sacrificed to him the fattened calf."