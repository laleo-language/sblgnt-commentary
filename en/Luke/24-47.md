---
version: 1
---
- **καὶ κηρυχθῆναι**: The word κηρυχθῆναι is the aorist passive infinitive of κηρύσσω, which means "to proclaim" or "to preach." The phrase καὶ κηρυχθῆναι means "and to be proclaimed."

- **ἐπὶ τῷ ὀνόματι αὐτοῦ**: The word ἐπὶ is a preposition meaning "on" or "upon." The article τῷ is the dative singular masculine form of the definite article ὁ, meaning "the." The noun ὀνόματι is the dative singular form of ὄνομα, which means "name." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, meaning "his." The phrase ἐπὶ τῷ ὀνόματι αὐτοῦ means "in his name."

- **μετάνοιαν**: The word μετάνοιαν is the accusative singular form of μετάνοια, which means "repentance." 

- **καὶ ἄφεσιν ἁμαρτιῶν**: The word καὶ is a conjunction meaning "and." The noun ἄφεσιν is the accusative singular form of ἄφεσις, which means "forgiveness." The noun ἁμαρτιῶν is the genitive plural form of ἁμαρτία, which means "sins." The phrase καὶ ἄφεσιν ἁμαρτιῶν means "and forgiveness of sins."

- **εἰς πάντα τὰ ἔθνη**: The word εἰς is a preposition meaning "to" or "for." The adjective πάντα is the accusative plural form of πᾶς, which means "all" or "every." The noun τὰ ἔθνη is the accusative plural form of ἔθνος, which means "nations" or "Gentiles." The phrase εἰς πάντα τὰ ἔθνη means "to all the nations."

- **ἀρξάμενοι ἀπὸ Ἰερουσαλήμ**: The verb ἀρξάμενοι is the aorist middle participle nominative plural masculine form of ἄρχω, which means "to begin" or "to start." The preposition ἀπὸ is a preposition meaning "from." The noun Ἰερουσαλήμ is the genitive singular form of Ἰερουσαλήμ, which means "Jerusalem." The phrase ἀρξάμενοι ἀπὸ Ἰερουσαλήμ means "beginning from Jerusalem."

- Literal Translation: "And to be proclaimed in his name repentance and forgiveness of sins to all the nations—beginning from Jerusalem."