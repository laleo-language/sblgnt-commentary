---
version: 1
---
- **ἀλλʼ**: This is a conjunction meaning "but" or "however."
- **ὅταν**: This is a subordinating conjunction meaning "whenever" or "when."
- **δοχὴν**: This is the accusative singular form of the noun δοχή, which means "a reception" or "a banquet."
- **ποιῇς**: This is the second person singular present subjunctive form of the verb ποιέω, which means "to make" or "to do."
- **κάλει**: This is the third person singular present imperative form of the verb καλέω, which means "to call" or "to invite."
- **πτωχούς**: This is the accusative plural form of the adjective πτωχός, which means "poor."
- **ἀναπείρους**: This is the accusative plural form of the adjective ἀναπήρος, which means "lame" or "crippled."
- **χωλούς**: This is the accusative plural form of the adjective χωλός, which means "lame" or "crippled."
- **τυφλούς**: This is the accusative plural form of the adjective τυφλός, which means "blind."

Literal translation: "But whenever you make a reception, call the poor, the crippled, the lame, the blind."

The verse is not syntactically ambiguous. 

Literal translation of the entire verse: "But whenever you make a reception, call the poor, the crippled, the lame, the blind."