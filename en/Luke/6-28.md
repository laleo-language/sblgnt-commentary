---
version: 1
---
- **εὐλογεῖτε τοὺς καταρωμένους ⸀ὑμᾶς**: The verb εὐλογεῖτε is the second person plural present imperative of εὐλογέω, which means "bless." The noun τοὺς καταρωμένους is the accusative plural of καταρώμενος, which means "cursed" or "reviled." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." So this phrase can be translated as "bless those who curse you."

- **προσεύχεσθε ⸀περὶ τῶν ἐπηρεαζόντων ὑμᾶς**: The verb προσεύχεσθε is the second person plural present imperative of προσεύχομαι, which means "pray." The preposition περὶ means "for" or "about." The article τῶν is the genitive plural of ὁ, meaning "the." The participle ἐπηρεαζόντων is the genitive plural masculine present active participle of ἐπηρεάζω, which means "to mistreat" or "to persecute." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." So this phrase can be translated as "pray for those who mistreat you."

The literal translation of Luke 6:28 is "Bless those who curse you, pray for those who mistreat you."