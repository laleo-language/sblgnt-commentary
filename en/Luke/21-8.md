---
version: 1
---
- **ὁ δὲ εἶπεν**: The word ὁ is the definite article in the nominative singular masculine, meaning "the." The word δὲ is a conjunction that can be translated as "and" or "but" depending on the context; here it is translated as "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "And he said."

- **Βλέπετε μὴ πλανηθῆτε**: The verb Βλέπετε is the second person plural present imperative of βλέπω, meaning "you see" or "you watch." The word μὴ is a negative particle that can be translated as "not" or "do not." The verb πλανηθῆτε is the second person plural aorist passive subjunctive of πλανάω, meaning "you may be deceived" or "you may go astray." The phrase thus means "See that you do not be deceived."

- **πολλοὶ γὰρ ἐλεύσονται**: The word πολλοὶ is the nominative plural of πολλός, meaning "many." The word γὰρ is a conjunction that can be translated as "for" or "because." The verb ἐλεύσονται is the third person plural future indicative of ἔρχομαι, meaning "they will come." The phrase thus means "For many will come."

- **ἐπὶ τῷ ὀνόματί μου λέγοντες**: The preposition ἐπὶ means "on" or "upon." The article τῷ is the definite article in the dative singular masculine, meaning "the." The noun ὀνόματί is the dative singular of ὄνομα, meaning "name." The participle λέγοντες is the present active participle in the nominative plural masculine of λέγω, meaning "saying" or "declaring." The phrase thus means "on my name, saying."

- **Ἐγώ εἰμι καί**: The pronoun Ἐγώ means "I." The verb εἰμι is the first person singular present indicative of εἰμί, meaning "am." The conjunction καί means "and." The phrase thus means "I am, and."

- **Ὁ καιρὸς ἤγγικεν**: The article Ὁ is the definite article in the nominative singular masculine, meaning "the." The noun καιρὸς is the nominative singular of καιρός, meaning "time" or "season." The verb ἤγγικεν is the third person singular perfect indicative of ἐγγίζω, meaning "has come" or "has approached." The phrase thus means "The time has come."

- **μὴ πορευθῆτε ὀπίσω αὐτῶν**: The word μὴ is a negative particle that can be translated as "not" or "do not." The verb πορευθῆτε is the second person plural aorist passive subjunctive of πορεύομαι, meaning "you may go" or "you may walk." The adverb ὀπίσω means "behind." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "them." The phrase thus means "Do not go behind them."

The sentence as a whole can be translated as "And he said, 'See that you do not be deceived, for many will come on my name, saying, 'I am,' and 'The time has come,' do not go behind them."