---
version: 1
---
- **εἶπεν δὲ πρὸς ἕτερον**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The preposition πρὸς means "to" or "toward," and ἕτερον is the accusative singular of ἕτερος, meaning "another." The phrase thus means "he said to another."

- **Ἀκολούθει μοι**: The verb Ἀκολούθει is the imperative second person singular of ἀκολουθέω, meaning "follow." The pronoun μοι is the dative singular of ἐγώ, meaning "me." The phrase thus means "follow me."

- **ὁ δὲ εἶπεν**: The article ὁ is the nominative singular masculine definite article, meaning "the." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "but he said."

- **Κύριε, ἐπίτρεψόν μοι**: The noun Κύριε is the vocative singular of κύριος, meaning "Lord." The verb ἐπίτρεψόν is the second person singular aorist imperative of ἐπιτρέπω, meaning "permit" or "allow." The pronoun μοι is the dative singular of ἐγώ, meaning "me." The phrase thus means "Lord, permit me."

- **ἀπελθόντι πρῶτον**: The verb ἀπελθόντι is the aorist participle masculine singular dative of ἀπέρχομαι, meaning "going away." The adverb πρῶτον means "first." The phrase thus means "going away first."

- **θάψαι τὸν πατέρα μου**: The verb θάψαι is the aorist infinitive of θάπτω, meaning "to bury." The article τὸν is the accusative singular masculine definite article, meaning "the." The noun πατέρα is the accusative singular masculine of πατήρ, meaning "father." The pronoun μου is the genitive singular of ἐγώ, meaning "my." The phrase thus means "to bury my father."

The literal translation of the verse is: "He said to another, 'Follow me.' But he said, 'Lord, permit me going away first to bury my father.'"