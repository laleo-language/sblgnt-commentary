---
version: 1
---
- **τοῦ Ἰωανὰν**: The word τοῦ is the genitive singular of the article ὁ, "the." The noun Ἰωανὰν is the accusative singular of Ἰωάννης, which means "John." The phrase thus means "the John."

- **τοῦ Ῥησὰ**: The word τοῦ is the genitive singular of the article ὁ, "the." The noun Ῥησὰ is the accusative singular of Ῥησά, which is a variant form of Ῥησσα, a name. The phrase thus means "the Rhesa."

- **τοῦ Ζοροβαβὲλ**: The word τοῦ is the genitive singular of the article ὁ, "the." The noun Ζοροβαβέλ is the accusative singular of Ζοροβαβέλ, which is a transliteration of the Hebrew name Zerubbabel. The phrase thus means "the Zerubbabel."

- **τοῦ Σαλαθιὴλ**: The word τοῦ is the genitive singular of the article ὁ, "the." The noun Σαλαθιήλ is the accusative singular of Σαλαθιήλ, which is a transliteration of the Hebrew name Shealtiel. The phrase thus means "the Shealtiel."

- **τοῦ Νηρὶ**: The word τοῦ is the genitive singular of the article ὁ, "the." The noun Νηρί is the accusative singular of Νηρί, which is a transliteration of the Hebrew name Neri. The phrase thus means "the Neri."

The literal translation of the entire verse is: "the John, the Rhesa, the Zerubbabel, the Shealtiel, the Neri."