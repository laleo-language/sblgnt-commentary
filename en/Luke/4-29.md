---
version: 1
---
- **καὶ ἀναστάντες**: The conjunction καὶ means "and." The participle ἀναστάντες is the nominative plural masculine of ἀνίστημι, meaning "they stood up" or "they rose up." The phrase thus means "and standing up."

- **ἐξέβαλον αὐτὸν ἔξω τῆς πόλεως**: The verb ἐξέβαλον is the first person plural aorist indicative of ἐκβάλλω, meaning "we cast out" or "we expelled." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, meaning "him." The preposition ἔξω means "outside." The genitive τῆς πόλεως means "of the city." The phrase thus means "we cast him out of the city."

- **καὶ ἤγαγον αὐτὸν ἕως ὀφρύος τοῦ ὄρους**: The conjunction καὶ means "and." The verb ἤγαγον is the first person plural aorist indicative of ἄγω, meaning "we led" or "we brought." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, meaning "him." The preposition ἕως means "until." The genitive ὀφρύος means "brow" or "edge." The genitive τοῦ ὄρους means "of the mountain." The phrase thus means "we led him until the brow of the mountain."

- **ἐφʼ οὗ ἡ πόλις ⸂ᾠκοδόμητο αὐτῶν, ὥστε⸃ κατακρημνίσαι αὐτόν**: The preposition ἐφʼ means "on" or "upon." The relative pronoun οὗ means "where." The verb ᾠκοδόμητο is the third person singular aorist indicative middle of οἰκοδομέω, meaning "it was built" or "it was constructed." The pronoun αὐτῶν is the genitive plural masculine of αὐτός, meaning "their." The conjunction ὥστε introduces a result clause and means "so that." The verb κατακρημνίσαι is the aorist infinitive of κατακρημνίζω, meaning "to hurl down" or "to cast down." The pronoun αὐτόν is the accusative singular masculine of αὐτός, meaning "him." The phrase thus means "on which their city was built, so that they could cast him down."

- **καὶ ἀναστάντες ἐξέβαλον αὐτὸν ἔξω τῆς πόλεως, καὶ ἤγαγον αὐτὸν ἕως ὀφρύος τοῦ ὄρους ἐφʼ οὗ ἡ πόλις ᾠκοδόμητο αὐτῶν, ὥστε κατακρημνίσαι αὐτόν**: And standing up, they cast him out of the city, and led him until the brow of the mountain on which their city was built, so that they could cast him down.

In this verse, we see that after Jesus spoke in the synagogue, the people in his hometown became angry and tried to harm him. They stood up, expelled him from the city, and led him to the edge of a mountain, where their city was built. They wanted to cast him down from the mountain.