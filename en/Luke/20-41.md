---
version: 1
---
- **Εἶπεν δὲ πρὸς αὐτούς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said." The preposition πρὸς means "to" or "towards." The pronoun αὐτούς is the accusative plural of αὐτός, meaning "them." The phrase thus means "He said to them."

- **Πῶς λέγουσιν**: The adverb πῶς means "how." The verb λέγουσιν is the third person plural present indicative of λέγω, which means "they say." The phrase thus means "How do they say."

- **τὸν χριστὸν εἶναι Δαυὶδ υἱόν**: The article τὸν is the accusative singular of ὁ, meaning "the." The noun χριστὸν is the accusative singular of Χριστός, which means "Christ." The verb εἶναι is the present infinitive of εἰμί, meaning "to be." The noun Δαυὶδ is the accusative singular of Δαυίδ, which means "David." The noun υἱόν is the accusative singular of υἱός, meaning "son." The phrase thus means "that the Christ is the son of David."

- The entire verse can be literally translated as: "He said to them, 'How do they say that the Christ is the son of David?'"