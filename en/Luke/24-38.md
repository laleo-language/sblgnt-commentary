---
version: 1
---
- **Τί τεταραγμένοι ἐστέ**: The word τί is the nominative singular neuter of the interrogative pronoun τίς, meaning "what." The verb τεταραγμένοι is the nominative plural masculine of the perfect passive participle of ταράσσω, meaning "to disturb" or "to trouble." The verb ἐστέ is the second person plural present indicative of εἰμί, meaning "you are." The phrase thus means "Why are you troubled?"

- **καὶ διὰ τί διαλογισμοὶ ἀναβαίνουσιν**: The phrase καὶ διὰ τί means "and why." The noun διαλογισμοὶ is the nominative plural masculine of διαλογισμός, which means "thoughts" or "reasonings." The verb ἀναβαίνουσιν is the third person plural present indicative of ἀναβαίνω, meaning "to arise" or "to come up." The phrase thus means "and why do thoughts arise?"

- **ἐν τῇ καρδίᾳ ὑμῶν**: The preposition ἐν means "in." The noun καρδίᾳ is the dative singular feminine of καρδία, meaning "heart." The pronoun ὑμῶν is the genitive plural of ὑμεῖς, meaning "you." The phrase thus means "in your hearts."

The literal translation of Luke 24:38 is: "And he said to them, 'Why are you troubled, and why do thoughts arise in your hearts?'"