---
version: 1
---
- **καὶ αὐτὸς φωνήσας εἶπεν**: The word αὐτὸς is the pronoun meaning "he himself." The participle φωνήσας is from the verb φωνέω, which means "to call out" or "to cry out." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "And he himself, calling out, said."

- **Πάτερ Ἀβραάμ**: The word Πάτερ is the vocative singular of πατήρ, which means "father." The name Ἀβραάμ is a proper noun, referring to Abraham. The phrase means "Father Abraham."

- **ἐλέησόν με**: The verb ἐλέησόν is the second person singular aorist imperative of ἐλεέω, meaning "have mercy" or "be merciful." The pronoun με is the accusative singular of ἐγώ, meaning "me." The phrase means "Have mercy on me."

- **καὶ πέμψον Λάζαρον**: The verb πέμψον is the second person singular aorist imperative of πέμπω, meaning "send." The noun Λάζαρον is the accusative singular of Λάζαρος, referring to the character Lazarus. The phrase means "And send Lazarus."

- **ἵνα βάψῃ τὸ ἄκρον τοῦ δακτύλου αὐτοῦ ὕδατος**: The word ἵνα is a conjunction introducing a purpose clause, meaning "so that." The verb βάψῃ is the third person singular aorist subjunctive of βάπτω, meaning "he might dip" or "he might moisten." The article τὸ is the accusative singular neuter form of the definite article, meaning "the." The noun ἄκρον is the accusative singular of ἄκρος, which means "end" or "tip." The genitive τοῦ δακτύλου is the genitive singular of δάκτυλος, meaning "finger." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The noun ὕδατος is the genitive singular of ὕδωρ, meaning "water." The phrase means "so that he might dip the tip of his finger in water."

- **καὶ καταψύξῃ τὴν γλῶσσάν μου**: The verb καταψύξῃ is the third person singular aorist subjunctive of καταψύχω, meaning "he might cool down" or "he might refresh." The article τὴν is the accusative singular feminine form of the definite article, meaning "the." The noun γλῶσσαν is the accusative singular of γλῶσσα, meaning "tongue." The pronoun μου is the genitive singular of ἐγώ, meaning "my." The phrase means "so that he might cool down my tongue."

- **ὅτι ὀδυνῶμαι ἐν τῇ φλογὶ ταύτῃ**: The conjunction ὅτι introduces a dependent clause, meaning "because" or "since." The verb ὀδυνῶμαι is the first person singular present indicative middle/passive of ὀδυνάω, meaning "I am in pain" or "I suffer." The preposition ἐν means "in." The definite article τῇ is the dative singular feminine form of the definite article, meaning "the." The noun φλογὶ is the dative singular of φλόξ, meaning "flame." The demonstrative pronoun ταύτῃ is the dative singular feminine form of οὗτος, meaning "this." The phrase means "because I am in pain in this flame."

The literal translation of the entire verse is: "And he himself, calling out, said: Father Abraham, have mercy on me and send Lazarus so that he might dip the tip of his finger in water and cool down my tongue, because I am in pain in this flame."