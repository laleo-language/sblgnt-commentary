---
version: 1
---
- **⸀αὕτη ἀπογραφὴ πρώτη ἐγένετο**: The word ⸀αὕτη is the nominative singular feminine form of αὕτη, which means "this." The noun ἀπογραφὴ is the nominative singular of ἀπογραφή, which means "registration" or "census." The adjective πρώτη is the feminine singular form of πρῶτος, meaning "first." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it became." The phrase thus means "This was the first registration."

- **ἡγεμονεύοντος τῆς Συρίας Κυρηνίου**: The participle ἡγεμονεύοντος is the genitive singular masculine form of ἡγεμονεύω, which means "governing" or "ruling." The genitive τῆς Συρίας is the genitive singular feminine form of Συρία, meaning "Syria." The genitive Κυρηνίου is the genitive singular masculine form of Κύρηνος, meaning "Cyrenius" or "Quirinius." The phrase thus means "during the governing of Syria by Cyrenius."

The entire verse can be literally translated as: "This was the first registration that took place while Cyrenius was governing Syria."