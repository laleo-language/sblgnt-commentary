---
version: 1
---
- **εἶπεν δὲ αὐτοῖς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "he said to them."

- **Ἐθεώρουν τὸν Σατανᾶν**: The verb Ἐθεώρουν is the imperfect indicative of θεωρέω, meaning "they saw" or "they were seeing." The noun τὸν Σατανᾶν is the accusative singular of Σατανᾶς, meaning "Satan." The phrase thus means "they were seeing Satan."

- **ὡς ἀστραπὴν ἐκ τοῦ οὐρανοῦ πεσόντα**: The word ὡς is a preposition meaning "like" or "as." The noun ἀστραπὴν is the accusative singular of ἀστραπή, meaning "lightning" or "flash." The prepositional phrase ἐκ τοῦ οὐρανοῦ means "from heaven." The participle πεσόντα is the accusative singular masculine aorist active participle of πίπτω, meaning "falling." The phrase thus means "like a flash of lightning falling from heaven."

- **Ἐθεώρουν τὸν Σατανᾶν ὡς ἀστραπὴν ἐκ τοῦ οὐρανοῦ πεσόντα**: This entire phrase functions as the direct object of the verb Ἐθεώρουν. It means "they were seeing Satan like a flash of lightning falling from heaven."

Literal translation: "He said to them, 'I saw Satan falling like a flash of lightning from heaven.'"