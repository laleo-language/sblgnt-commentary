---
version: 1
---
- **ἐκεῖ ἔσται ὁ κλαυθμὸς καὶ ὁ βρυγμὸς τῶν ὀδόντων**: The word ἐκεῖ means "there." The verb ἔσται is the third person singular future indicative of εἰμί, meaning "will be." The noun phrase ὁ κλαυθμὸς καὶ ὁ βρυγμὸς τῶν ὀδόντων consists of the noun ὁ κλαυθμός meaning "weeping" or "wailing," the conjunction καὶ meaning "and," the noun ὁ βρυγμός meaning "grinding," and the genitive plural τῶν ὀδόντων meaning "of the teeth." The phrase thus means "there will be weeping and grinding of teeth."

- **ὅταν ⸀ὄψησθε Ἀβραὰμ καὶ Ἰσαὰκ καὶ Ἰακὼβ καὶ πάντας τοὺς προφήτας**: The subordinating conjunction ὅταν means "when" and introduces a subordinate clause. The verb ὄψησθε is the second person plural aorist middle subjunctive of ὁράω, meaning "you see" or "you will see." The noun phrase Ἀβραὰμ καὶ Ἰσαὰκ καὶ Ἰακὼβ καὶ πάντας τοὺς προφήτας consists of the proper names Ἀβραὰμ, Ἰσαὰκ, Ἰακώβ, and the noun πάντας τοὺς προφήτας meaning "all the prophets." The phrase thus means "when you see Abraham and Isaac and Jacob and all the prophets."

- **ἐν τῇ βασιλείᾳ τοῦ θεοῦ**: The preposition ἐν means "in" and the noun phrase τῇ βασιλείᾳ τοῦ θεοῦ consists of the definite article τῇ meaning "the," the noun βασιλεία meaning "kingdom," and the genitive singular τοῦ θεοῦ meaning "of God." The phrase thus means "in the kingdom of God."

- **ὑμᾶς δὲ ἐκβαλλομένους ἔξω**: The pronoun ὑμᾶς means "you" and the conjunction δὲ means "but" or "and." The present participle ἐκβαλλομένους is declined from the verb ἐκβάλλω, meaning "to cast out" or "to drive out." The adverb ἔξω means "out." The phrase thus means "but you being cast out outside."

The entire verse could be literally translated as "There will be weeping and grinding of teeth when you see Abraham and Isaac and Jacob and all the prophets in the kingdom of God, but you being cast out outside."