---
version: 1
---
- **ταύτην δὲ θυγατέρα Ἀβραὰμ οὖσαν**: The word ταύτην is the accusative singular feminine of οὗτος, which means "this." The word θυγατέρα is the accusative singular feminine of θυγάτηρ, which means "daughter." The phrase Ἀβραὰμ οὖσαν means "being of Abraham." So the phrase means "this daughter being of Abraham."

- **ἣν ἔδησεν ὁ Σατανᾶς**: The word ἣν is the accusative singular feminine of ὅς, which means "whom" or "which." The verb ἔδησεν is the third person singular aorist indicative of δέω, which means "he bound" or "he tied." The phrase ὁ Σατανᾶς means "the Satan." So the phrase means "whom the Satan bound."

- **ἰδοὺ δέκα καὶ ὀκτὼ ἔτη**: The word ἰδοὺ is an interjection that indicates surprise or emphasis, often translated as "behold" or "look." The words δέκα καὶ ὀκτὼ are the numbers ten and eight, meaning "eighteen." The word ἔτη is the accusative plural of ἔτος, which means "years." So the phrase means "Look, eighteen years."

- **οὐκ ἔδει λυθῆναι ἀπὸ τοῦ δεσμοῦ τούτου**: The word οὐκ is a negative particle that means "not." The verb ἔδει is the third person singular imperfect indicative of δέω, which means "it was necessary" or "it should have been." The infinitive λυθῆναι is the aorist passive infinitive of λύω, meaning "to be loosed" or "to be released." The phrase ἀπὸ τοῦ δεσμοῦ τούτου means "from this bond." So the phrase means "It was not necessary to be loosed from this bond."

- **τῇ ἡμέρᾳ τοῦ σαββάτου**: The word τῇ is the dative singular feminine of ὁ, which means "on the." The noun ἡμέρᾳ is the dative singular feminine of ἡμέρα, which means "day." The genitive τοῦ σαββάτου means "of the Sabbath." So the phrase means "on the day of the Sabbath."

The sentence can be translated as: "But should not this daughter, being of Abraham, whom the Satan bound, behold, eighteen years, it was not necessary to be loosed from this bond on the day of the Sabbath?"

This verse tells the story of a woman who had been bound by Satan for eighteen years and was not able to be loosed from this bondage on the Sabbath day.