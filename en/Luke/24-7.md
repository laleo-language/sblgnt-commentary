---
version: 1
---
- **λέγων**: The word λέγων is the present active participle of λέγω, which means "saying" or "he said." It agrees with the noun it modifies in gender, number, and case. 

- **τὸν υἱὸν τοῦ ἀνθρώπου**: The article τὸν indicates that the noun following it is in the accusative case. The noun υἱὸν means "son" and is in the accusative singular form. The genitive phrase τοῦ ἀνθρώπου means "of the man" or "of humanity." So together, the phrase means "the son of man."

- **ὅτι δεῖ**: The word ὅτι means "that" and introduces a clause that explains or gives the content of what is being said. The verb δεῖ is the third person singular present indicative of δεῖ, which means "it is necessary." So the phrase means "that it is necessary."

- **παραδοθῆναι εἰς χεῖρας ἀνθρώπων ἁμαρτωλῶν**: The verb παραδοθῆναι is the aorist passive infinitive of παραδίδωμι, which means "to be handed over" or "to be delivered." The preposition εἰς means "into" or "to." The noun χεῖρας means "hands" and is in the accusative plural form. The genitive phrase ἀνθρώπων ἁμαρτωλῶν means "of sinful men" or "of sinners." So together, the phrase means "to be handed over into the hands of sinful men."

- **καὶ σταυρωθῆναι**: The conjunction καὶ means "and." The verb σταυρωθῆναι is the aorist passive infinitive of σταυρόω, which means "to be crucified." So the phrase means "and to be crucified."

- **καὶ τῇ τρίτῃ ἡμέρᾳ ἀναστῆναι**: The conjunction καὶ means "and." The article τῇ indicates that the following noun is in the dative case. The noun τρίτῃ means "third" and is in the dative singular form. The genitive phrase ἡμέρᾳ means "of day" or "day." The verb ἀναστῆναι is the aorist infinitive of ἀνίστημι, which means "to rise" or "to be raised." So the phrase means "and on the third day to rise."

- **The literal translation**: saying the Son of Man that it is necessary to be handed over into the hands of sinful men and to be crucified and on the third day to rise.

In this verse, Jesus is speaking and explaining to his disciples that it is necessary for the Son of Man to be handed over to sinful men, crucified, and then rise on the third day.