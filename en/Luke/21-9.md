---
version: 1
---
- **ὅταν δὲ ἀκούσητε πολέμους καὶ ἀκαταστασίας**: The word ὅταν is a conjunction meaning "when." The verb ἀκούσητε is the second person plural aorist subjunctive of ἀκούω, meaning "you hear." The noun πολέμους is the accusative plural of πόλεμος, meaning "wars." The noun ἀκαταστασίας is the accusative plural of ἀκαταστασία, which means "disturbances" or "upheavals." The phrase thus means "when you hear of wars and disturbances."

- **μὴ πτοηθῆτε**: The word μὴ is a negative particle meaning "not." The verb πτοηθῆτε is the second person plural aorist subjunctive passive of πτοέω, meaning "you be frightened" or "you be alarmed." The phrase thus means "do not be frightened."

- **δεῖ γὰρ ταῦτα γενέσθαι πρῶτον**: The word δεῖ is a verb meaning "it is necessary." The adverb γὰρ is a conjunction meaning "for." The noun ταῦτα is the accusative plural of οὗτος, meaning "these things." The verb γενέσθαι is the aorist infinitive of γίνομαι, meaning "to happen" or "to occur." The adverb πρῶτον is an adverb meaning "first" or "before." The phrase thus means "for it is necessary for these things to happen first."

- **ἀλλʼ οὐκ εὐθέως τὸ τέλος**: The conjunction ἀλλά is a contrastive conjunction meaning "but" or "however." The adverb οὐκ is a negative particle meaning "not." The adverb εὐθέως is an adverb meaning "immediately" or "at once." The article τὸ is the nominative neuter singular of ὁ, meaning "the." The noun τέλος is the nominative neuter singular of τέλος, meaning "end" or "completion." The phrase thus means "but not immediately the end."

Literal translation: "When you hear of wars and disturbances, do not be frightened; for it is necessary for these things to happen first, but not immediately the end."

In this verse, Jesus is warning his disciples about the signs that will precede the end times. He tells them that they will hear of wars and disturbances, but they should not be alarmed because these things must happen before the end comes. The end will not come immediately, but there will be a sequence of events leading up to it.