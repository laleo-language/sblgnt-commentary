---
version: 1
---
- **ἀποκριθεὶς δὲ ὁ Ἰησοῦς εἶπεν**: The word ἀποκριθεὶς is the nominative masculine singular participle of ἀποκρίνομαι, which means "having answered." The article ὁ is the nominative masculine singular definite article, meaning "the." The noun Ἰησοῦς is the nominative masculine singular form of Ἰησοῦς, which means "Jesus." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "Having answered, Jesus said."

- **Οὐχὶ οἱ δέκα ἐκαθαρίσθησαν**: The word Οὐχὶ is an interrogative particle that expects a negative answer, meaning "did not." The article οἱ is the nominative masculine plural definite article, meaning "the." The number δέκα is the nominative masculine plural form of δέκα, which means "ten." The verb ἐκαθαρίσθησαν is the third person plural aorist passive indicative of καθαρίζω, meaning "they were cleansed." The phrase thus means "Did not the ten (people) get cleansed?"

- **οἱ δὲ ἐννέα ποῦ**: The article οἱ is the nominative masculine plural definite article, meaning "the." The number δὲ is the nominative masculine plural form of δέ, which means "but" or "on the other hand." The number ἐννέα is the nominative masculine plural form of ἐννέα, which means "nine." The word ποῦ is an interrogative adverb, meaning "where." The phrase thus means "But where are the nine (people)?"

The verse can be translated as: "Having answered, Jesus said, 'Did not the ten (people) get cleansed? But where are the nine (people)?'"