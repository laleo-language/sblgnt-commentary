---
version: 1
---
- **λέγω ὑμῖν**: The verb λέγω is the first person singular present indicative of λέγω, meaning "I say" or "I speak." The pronoun ὑμῖν is the second person plural dative of σύ, meaning "to you." The phrase thus means "I say to you."

- **μείζων ἐν γεννητοῖς γυναικῶν**: The adjective μείζων is the nominative singular masculine form of μέγας, meaning "greater" or "bigger." The preposition ἐν means "among" or "in." The noun γεννητοῖς is the dative plural of γεννητός, meaning "born" or "created." The noun γυναικῶν is the genitive plural of γυνή, meaning "woman" or "wife." The phrase can be translated as "greater among those born of women."

- **Ἰωάννου οὐδείς ἐστιν**: The noun Ἰωάννου is the genitive singular of Ἰωάννης, which is the name "John." The pronoun οὐδείς is the nominative singular masculine form of οὐδείς, meaning "no one" or "none." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is" or "exists." The phrase can be translated as "no one is John."

- **ὁ δὲ μικρότερος ἐν τῇ βασιλείᾳ τοῦ θεοῦ μείζων αὐτοῦ ἐστιν**: The article ὁ is the nominative singular masculine form of ὁ, meaning "the." The conjunction δὲ means "but" or "and." The adjective μικρότερος is the nominative singular masculine form of μικρός, meaning "smaller" or "lesser." The preposition ἐν means "in." The article τῇ is the dative singular feminine form of ὁ, meaning "the." The noun βασιλείᾳ is the dative singular feminine of βασιλεία, meaning "kingdom." The genitive pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, meaning "his." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is" or "exists." The phrase can be translated as "but the smaller (one) in the kingdom of God is greater than him."

The literal translation of Luke 7:28 is: "I say to you, among those born of women, no one is greater than John. But the smaller one in the kingdom of God is greater than him."