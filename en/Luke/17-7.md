---
version: 1
---
- **Τίς δὲ ἐξ ὑμῶν δοῦλον ἔχων**: The word τίς is the nominative singular of τίς, which means "who." The word δὲ is a conjunction that can be translated as "but" or "and." The preposition ἐξ takes the genitive case, so ὑμῶν is the genitive plural of σύ, which means "of you." The participle ἔχων is the nominative singular masculine present active participle of ἔχω, meaning "having." The noun δοῦλον is the accusative singular of δοῦλος, which means "servant." So the phrase means "But who among you having a servant."

- **ἀροτριῶντα ἢ ποιμαίνοντα**: The participle ἀροτριῶντα is the accusative singular masculine present active participle of ἀροτριάω, meaning "plowing." The participle ποιμαίνοντα is the accusative singular masculine present active participle of ποιμαίνω, meaning "shepherding." So the phrase means "who is plowing or shepherding."

- **ὃς εἰσελθόντι ἐκ τοῦ ἀγροῦ**: The relative pronoun ὃς is the nominative singular masculine of ὅς, which means "who." The participle εἰσελθόντι is the dative singular masculine aorist active participle of εἰσέρχομαι, meaning "entering." The preposition ἐκ takes the genitive case, so τοῦ ἀγροῦ is the genitive singular of ἀγρός, which means "field." So the phrase means "who, upon entering from the field."

- **ἐρεῖ αὐτῷ**: The verb ἐρεῖ is the third person singular future indicative of λέγω, meaning "he will say." The pronoun αὐτῷ is the dative singular of αὐτός, which means "to him." So the phrase means "he will say to him."

- **Εὐθέως παρελθὼν ἀνάπεσε**: The adverb Εὐθέως means "immediately" or "straightaway." The participle παρελθὼν is the nominative singular masculine aorist active participle of παρέρχομαι, meaning "passing by." The verb ἀνάπεσε is the third person singular aorist indicative of ἀναπίπτω, meaning "he sat down." So the phrase means "Immediately, having passed by, he sat down."

- **τίς δὲ ἐξ ὑμῶν δοῦλον ἔχων ἀροτριῶντα ἢ ποιμαίνοντα, ὃς εἰσελθόντι ἐκ τοῦ ἀγροῦ ἐρεῖ αὐτῷ· Εὐθέως παρελθὼν ἀνάπεσε**: "But who among you, having a servant who is plowing or shepherding, when he comes in from the field, will say to him, 'Immediately, come and sit down'?"

The phrase is not ambiguous in terms of its grammar, but the context might require some interpretation to fully understand the situation being described.