---
version: 1
---
- **πᾶς ὁ ἐρχόμενος πρός με**: The word πᾶς is an adjective meaning "every" or "all." The article ὁ is the definite article meaning "the." The participle ἐρχόμενος is the present middle or passive participle of the verb ἔρχομαι, meaning "coming." The preposition πρός means "to" or "towards" and the pronoun με means "me." The phrase thus means "everyone who is coming to me."

- **καὶ ἀκούων μου τῶν λόγων**: The conjunction καὶ means "and." The participle ἀκούων is the present active participle of the verb ἀκούω, meaning "hearing." The genitive pronoun μου means "my" and the genitive noun τῶν λόγων means "of the words." The phrase thus means "and hearing my words."

- **καὶ ποιῶν αὐτούς**: The conjunction καὶ means "and." The participle ποιῶν is the present active participle of the verb ποιέω, meaning "doing" or "making." The pronoun αὐτούς means "them." The phrase thus means "and doing them."

- **ὑποδείξω ὑμῖν τίνι ἐστὶν ὅμοιος**: The verb ὑποδείξω is the future active indicative of the verb ὑποδείκνυμι, meaning "I will show" or "I will demonstrate." The pronoun ὑμῖν means "to you." The interrogative pronoun τίνι means "to whom" or "to what." The verb ἐστὶν is the present active indicative of the verb εἰμί, meaning "is." The adjective ὅμοιος means "similar" or "like." The phrase thus means "I will show you to whom he is similar."

- The entire verse can be literally translated as "Everyone who is coming to me and hearing my words and doing them, I will show you to whom he is similar."