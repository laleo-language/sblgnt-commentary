---
version: 1
---
- **Ταῦτα ἃ θεωρεῖτε**: The word Ταῦτα is the nominative neuter plural of οὗτος, meaning "these." The relative pronoun ἃ is the accusative neuter plural of ὅς, meaning "which" or "that." The verb θεωρεῖτε is the second person plural present indicative of θεωρέω, meaning "you see" or "you observe." The phrase thus means "these things which you see."

- **ἐλεύσονται ἡμέραι**: The verb ἐλεύσονται is the third person plural future indicative of ἔρχομαι, meaning "they will come." The noun ἡμέραι is the nominative plural of ἡμέρα, meaning "days." The phrase thus means "days will come."

- **ἐν αἷς οὐκ ἀφεθήσεται λίθος ἐπὶ λίθῳ ὃς οὐ καταλυθήσεται**: The preposition ἐν means "in" or "during." The relative pronoun αἷς is the dative feminine plural of ὅς. The verb ἀφεθήσεται is the third person singular future passive indicative of ἀφίημι, meaning "it will not be left" or "it will not remain." The noun λίθος is the nominative singular of λίθος, meaning "stone." The preposition ἐπὶ means "on" or "upon." The pronoun λίθῳ is the dative singular of λίθος. The pronoun ὃς is the nominative singular masculine of ὅς. The verb καταλυθήσεται is the third person singular future passive indicative of καταλύω, meaning "it will be destroyed" or "it will be demolished." The phrase thus means "in which days a stone will not be left on top of a stone which will not be destroyed."

The literal translation of Luke 21:6 is: "These things which you see, days will come in which a stone will not be left on top of a stone which will not be destroyed."