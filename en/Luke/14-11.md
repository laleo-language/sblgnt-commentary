---
version: 1
---
- **ὅτι**: This is a conjunction that introduces a subordinate clause. It can be translated as "because" or "that."
- **πᾶς ὁ ὑψῶν ἑαυτὸν**: The word πᾶς is an adjective that means "all" or "everyone." The article ὁ is the definite article, meaning "the." The verb ὑψῶν is the present participle of ὑψόω, meaning "to exalt" or "to lift up." The reflexive pronoun ἑαυτὸν means "himself." So the phrase can be translated as "everyone who exalts himself."
- **ταπεινωθήσεται**: This is a verb in the future passive indicative form of ταπεινόω, meaning "to humble" or "to lower oneself." It is third person singular, so it can be translated as "he will be humbled."

- **καὶ**: This is a conjunction that means "and."
- **ὁ ταπεινῶν ἑαυτὸν**: The article ὁ is the definite article. The verb ταπεινῶν is the present participle of ταπεινόω. So the phrase can be translated as "the one who humbles himself."
- **ὑψωθήσεται**: This is a verb in the future passive indicative form of ὑψόω. It is third person singular, so it can be translated as "he will be exalted."

Putting it all together, the verse can be translated as: "For everyone who exalts himself will be humbled, and the one who humbles himself will be exalted."

This verse teaches a principle of humility: those who exalt themselves will be humbled, but those who humble themselves will be exalted. The syntax of the verse is clear and there is no ambiguity in the grammar.