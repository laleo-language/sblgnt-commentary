---
version: 1
---
- **καὶ ἰδοὺ ἄνδρες δύο**: The conjunction καὶ means "and." The word ἰδοὺ is an interjection that is often translated as "behold" or "look." The noun ἄνδρες is the nominative plural of ἀνήρ, which means "men" or "people." The adjective δύο is the nominative plural of δύο, which means "two." The phrase can be translated as "and behold, two men."

- **συνελάλουν αὐτῷ**: The verb συνελάλουν is the third person plural imperfect indicative active of συνελαλέω, which means "they were talking together." The pronoun αὐτῷ is the dative singular of αὐτός, which means "to him" or "to them." The phrase can be translated as "they were talking together to him."

- **οἵτινες ἦσαν Μωϋσῆς καὶ Ἠλίας**: The pronoun οἵτινες is the nominative plural of οἵτις, which means "who" or "which." The verb ἦσαν is the third person plural imperfect indicative active of εἰμί, meaning "they were." The proper nouns Μωϋσῆς and Ἠλίας are the names Moses and Elijah. The phrase can be translated as "who were Moses and Elijah."

- The literal translation of the verse is: "And behold, two men were talking together with him, who were Moses and Elijah."

In this verse, we see that two men, identified as Moses and Elijah, were talking together with Jesus. This encounter is often referred to as the Transfiguration, where Jesus was revealed in his glorified state and was seen conversing with these two significant figures from Jewish history.