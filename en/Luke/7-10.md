---
version: 1
---
- **καὶ ὑποστρέψαντες**: The verb ὑποστρέφω is the aorist active participle, nominative plural, of ὑποστρέφω, meaning "to return." The participle form here functions as the subject of the sentence. The phrase means "and returning."

- **εἰς τὸν οἶκον**: The preposition εἰς means "into" or "to." The article τὸν is the accusative singular masculine form of the definite article ὁ, meaning "the." The noun οἶκον means "house." The phrase means "to the house."

- **οἱ πεμφθέντες**: The article οἱ is the nominative plural masculine form of the definite article ὁ, meaning "the." The verb πέμπω is the aorist passive participle, nominative plural, of πέμπω, meaning "to send." The participle form here functions as the subject of the sentence. The phrase means "the ones who were sent."

- **εὗρον**: The verb εὑρίσκω is the aorist active indicative, first person plural, of εὑρίσκω, meaning "to find." The phrase means "we found."

- **τὸν δοῦλον ὑγιαίνοντα**: The article τὸν is the accusative singular masculine form of the definite article ὁ, meaning "the." The noun δοῦλον means "slave." The participle ὑγιαίνοντα is the present active participle, accusative singular masculine, of ὑγιαίνω, meaning "to be in good health." The phrase means "the slave being in good health."

The verse can be literally translated as: "And returning to the house, the ones who were sent found the slave being in good health."

In this verse, there are no ambiguous phrases or complex grammar constructions. The sentence is straightforward and easy to understand.