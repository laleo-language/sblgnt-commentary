---
version: 1
---
- **κἂν μὲν ποιήσῃ καρπὸν εἰς τὸ μέλλον**: The word κἂν is a contraction of καὶ ἐὰν, which means "and if" or "if." The verb ποιήσῃ is the third person singular aorist subjunctive of ποιέω, meaning "he/she/it may produce." The noun καρπὸν is the accusative singular of καρπός, which means "fruit." The prepositional phrase εἰς τὸ μέλλον means "for the future" or "for the coming time."

- **εἰ δὲ μήγε**: The word εἰ is a conjunction that means "if." The adverb δὲ is a coordinating conjunction that often functions as "but" or "and." The adverb μήγε is a particle used to emphasize a negative statement, often translated as "surely not" or "certainly not."

- **ἐκκόψεις αὐτήν**: The verb ἐκκόψεις is the second person singular future indicative of ἐκκόπτω, meaning "you will cut off." The pronoun αὐτήν is the accusative singular feminine pronoun, which means "it" or "her." The phrase thus means "you will cut it off."

The literal translation of the entire verse is: "And if it may produce fruit for the future— but if not, you will cut it off."