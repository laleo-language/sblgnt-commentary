---
version: 1
---
- **καὶ ἦν αὐτῷ**: The word καὶ is a conjunction meaning "and." The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "he/she/it was." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to/for him." The phrase thus means "and it was to him."

- **κεχρηματισμένον ὑπὸ τοῦ πνεύματος τοῦ ἁγίου**: The verb κεχρηματισμένον is the perfect passive participle accusative singular of χρηματίζω, meaning "having been informed." The preposition ὑπὸ means "by." The article τοῦ is the genitive singular masculine of ὁ, meaning "the." The noun πνεῦμα is the nominative singular neuter of πνεῦμα, meaning "spirit." The adjective ἅγιος is the genitive singular masculine of ἅγιος, meaning "holy." The phrase thus means "having been informed by the Holy Spirit."

- **μὴ ἰδεῖν θάνατον**: The particle μὴ is a negative particle. The verb ἰδεῖν is the aorist infinitive of ὁράω, meaning "to see." The noun θάνατος is the accusative singular of θάνατος, meaning "death." The phrase thus means "not to see death."

- **πρὶν ἢ ἂν ἴδῃ τὸν χριστὸν κυρίου**: The preposition πρὶν means "before." The conjunction ἢ is a disjunctive conjunction meaning "or." The particle ἂν is a particle used with subjunctive verbs to indicate contingency or possibility. The verb ἴδῃ is the aorist subjunctive third person singular of ὁράω, meaning "he/she/it might see." The article τὸν is the accusative singular masculine of ὁ, meaning "the." The noun χριστός is the accusative singular of χριστός, meaning "Christ." The noun κύριος is the genitive singular masculine of κύριος, meaning "Lord." The phrase thus means "before he might see the Christ of the Lord."

- **καὶ ἦν αὐτῷ κεχρηματισμένον ὑπὸ τοῦ πνεύματος τοῦ ἁγίου μὴ ἰδεῖν θάνατον πρὶν ἢ ἂν ἴδῃ τὸν χριστὸν κυρίου**: The phrase combines all the previous phrases and means "and it was to him having been informed by the Holy Spirit not to see death before he might see the Christ of the Lord."

The verse is saying that it had been revealed to someone, by the Holy Spirit, that he would not die before seeing the Christ of the Lord.