---
version: 1
---
- **Ἀναβλέψας δὲ εἶδεν**: The participle Ἀναβλέψας is the nominative singular masculine form of the verb ἀναβλέπω, meaning "to look up" or "to look intently." The verb εἶδεν is the third person singular aorist indicative of ὁράω, meaning "he saw." The phrase thus means "Looking up, he saw."

- **τοὺς βάλλοντας εἰς τὸ γαζοφυλάκιον τὰ δῶρα αὐτῶν**: The article τοὺς is the accusative plural masculine form of ὁ, meaning "the." The participle βάλλοντας is the accusative plural masculine form of the verb βάλλω, meaning "throwing" or "casting." The preposition εἰς means "into." The noun γαζοφυλάκιον is the accusative singular neuter form of γαζοφυλάκιον, meaning "treasury" or "storeroom." The article τὰ is the accusative plural neuter form of ὁ, meaning "the." The noun δῶρα is the accusative plural neuter form of δῶρον, meaning "gifts." The pronoun αὐτῶν is the genitive plural masculine form of αὐτός, meaning "their." The phrase thus means "the ones throwing into the treasury their gifts."

- **πλουσίους**: The adjective πλουσίους is the accusative plural masculine form of πλούσιος, meaning "rich." The word functions as an adjective modifying the noun τοὺς βάλλοντας. The phrase thus means "the ones throwing into the treasury their rich gifts."

The literal translation of the verse is: "Looking up, he saw the ones throwing into the treasury their rich gifts."