---
version: 1
---
- **λυόντων δὲ αὐτῶν τὸν πῶλον**: The participle λυόντων is the genitive plural of λύω, which means "to untie" or "to loosen." It is in the genitive plural form because it agrees with the genitive plural noun αὐτῶν, which means "of them" or "their." The noun πῶλον is the accusative singular of πῶλος, which means "colt" or "young donkey." The phrase can be translated as "while they were untying the colt."

- **εἶπαν οἱ κύριοι αὐτοῦ πρὸς αὐτούς**: The verb εἶπαν is the third person plural aorist indicative of λέγω, which means "they said." The noun οἱ κύριοι is the nominative plural of κύριος, which means "lords" or "owners." The genitive singular pronoun αὐτοῦ means "of him" or "his." The preposition πρὸς means "to" or "towards." The pronoun αὐτούς is the accusative plural form of αὐτός, which means "them." The phrase can be translated as "his owners said to them."

- **Τί λύετε τὸν πῶλον**: The interrogative pronoun Τί means "what." The verb λύετε is the second person plural present indicative of λύω, which means "you are untying." The accusative singular article τὸν is used before the noun πῶλον to indicate that it is a specific colt being referred to. The phrase can be translated as "What are you untying the colt for?"

The literal translation of the verse is: "While they were untying the colt, his owners said to them, 'What are you untying the colt for?'"