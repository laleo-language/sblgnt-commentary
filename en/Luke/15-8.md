---
version: 1
---
- **ἢ τίς γυνὴ δραχμὰς ἔχουσα δέκα**: The word ἢ is a conjunction meaning "or." The word τίς is an indefinite pronoun meaning "who" or "someone." The noun γυνὴ is the nominative singular of γυνή, meaning "woman." The noun δραχμὰς is the accusative plural of δραχμή, meaning "drachmas." The participle ἔχουσα is the nominative singular feminine of ἔχω, meaning "having." The number δέκα means "ten." The phrase thus means "Or what woman, having ten drachmas..."

- **ἐὰν ἀπολέσῃ δραχμὴν μίαν**: The conjunction ἐὰν introduces a conditional clause and means "if." The verb ἀπολέσῃ is the third person singular aorist subjunctive of ἀπόλλυμι, meaning "she loses." The noun δραχμὴν is the accusative singular of δραχμή. The adjective μίαν is the accusative singular feminine of εἷς, meaning "one." The phrase thus means "if she loses one drachma..."

- **οὐχὶ ἅπτει λύχνον καὶ σαροῖ τὴν οἰκίαν**: The word οὐχὶ is a particle meaning "does she not." The verb ἅπτει is the third person singular present indicative of ἅπτω, meaning "she lights." The noun λύχνον is the accusative singular of λύχνος, meaning "lamp." The verb σαροῖ is the third person singular present subjunctive of σαίρω, meaning "she sweeps." The article τὴν is the accusative singular feminine of the definite article ὁ. The noun οἰκίαν is the accusative singular of οἰκία, meaning "house." The phrase thus means "does she not light a lamp and sweep the house..."

- **καὶ ζητεῖ ἐπιμελῶς ἕως οὗ εὕρῃ**: The conjunction καὶ means "and." The verb ζητεῖ is the third person singular present indicative of ζητέω, meaning "she seeks." The adverb ἐπιμελῶς means "carefully" or "diligently." The conjunction ἕως introduces a temporal clause and means "until." The verb εὕρῃ is the third person singular aorist subjunctive of εὑρίσκω, meaning "she finds." The phrase thus means "and she diligently seeks until she finds."

The entire verse, translated literally, would be: "Or what woman, having ten drachmas, if she loses one drachma, does she not light a lamp and sweep the house and seek diligently until she finds?"