---
version: 1
---
- **ἐπηρώτα δὲ αὐτὸν**: The verb ἐπηρώτα is the imperfect indicative active, third person singular of ἐπερωτάω, which means "to ask." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, which means "him." The phrase thus means "he asked him."

- **ἐν λόγοις ἱκανοῖς**: The preposition ἐν means "in." The noun λόγοις is the dative plural of λόγος, which means "words." The adjective ἱκανοῖς is the dative plural masculine of ἱκανός, which means "sufficient" or "adequate." The phrase thus means "with sufficient words" or "with enough words."

- **αὐτὸς δὲ οὐδὲν ἀπεκρίνατο αὐτῷ**: The pronoun αὐτὸς is the nominative singular masculine of αὐτός, which can mean "he" or "himself" depending on the context. The adverb οὐδὲν means "nothing." The verb ἀπεκρίνατο is the imperfect indicative middle, third person singular of ἀποκρίνομαι, which means "to answer" or "to reply." The pronoun αὐτῷ is the dative singular masculine of αὐτός, meaning "to him." The phrase thus means "he himself answered him nothing."

- **ἐπηρώτα δὲ αὐτὸν ἐν λόγοις ἱκανοῖς· αὐτὸς δὲ οὐδὲν ἀπεκρίνατο αὐτῷ**: The entire verse can be translated as "He asked him with enough words, but he himself answered him nothing."

In this verse, we see that someone is asking another person a question using sufficient words, but the person being asked does not reply at all.