---
version: 1
---
- **καὶ προσελθὼν ἥψατο τῆς σοροῦ**: The conjunction καὶ means "and." The participle προσελθὼν is the nominative singular masculine of προσέρχομαι, which means "approaching" or "coming near." The verb ἥψατο is the third person singular aorist middle indicative of ἅπτομαι, meaning "he touched." The article τῆς is the genitive singular feminine of ὁ, which means "the." The noun σοροῦ is the genitive singular feminine of σορός, which means "bier" or "coffin." The phrase thus means "And approaching, he touched the bier."

- **οἱ δὲ βαστάζοντες ἔστησαν**: The article οἱ is the nominative plural masculine of ὁ, which means "the." The verb βαστάζοντες is the nominative plural masculine present participle of βαστάζω, meaning "carrying" or "bearing." The verb ἔστησαν is the third person plural aorist indicative of ἵστημι, which means "they stood." The phrase thus means "And those who were carrying stood."

- **καὶ εἶπεν**: The conjunction καὶ means "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "And he said."

- **Νεανίσκε, σοὶ λέγω, ἐγέρθητι**: The noun Νεανίσκε is the vocative singular masculine of νεανίσκος, which means "young man." The pronoun σοὶ is the dative singular second person pronoun, which means "to you." The verb λέγω is the first person singular present indicative of λέγω, meaning "I say." The verb ἐγέρθητι is the second person singular aorist imperative passive of ἐγείρω, which means "be raised" or "get up." The phrase thus means "Young man, I say to you, get up."

- The literal translation of the entire verse is: "And approaching, he touched the bier, and those who were carrying stood, and he said, 'Young man, I say to you, get up.'"