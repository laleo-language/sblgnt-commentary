---
version: 1
---
- **καὶ πρὸς αὐτοὺς εἶπεν**: The conjunction καὶ means "and." The preposition πρὸς means "to" or "towards." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "and he said to them."

- **Τίνος ὑμῶν υἱὸς ἢ βοῦς**: The interrogative pronoun Τίνος means "whose." The pronoun ὑμῶν is the genitive plural of σύ, meaning "your." The noun υἱὸς means "son." The conjunction ἢ means "or." The noun βοῦς means "ox." The phrase thus means "Whose son or ox."

- **εἰς φρέαρ πεσεῖται**: The preposition εἰς means "into." The noun φρέαρ means "well" or "cistern." The verb πεσεῖται is the third person singular future indicative of πίπτω, meaning "he will fall." The phrase thus means "will fall into a well."

- **καὶ οὐκ εὐθέως ἀνασπάσει αὐτὸν**: The conjunction καὶ means "and." The adverb οὐκ means "not." The adverb εὐθέως means "immediately" or "right away." The verb ἀνασπάσει is the third person singular future indicative of ἀνασπάω, meaning "he will lift up." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "it" or "him." The phrase thus means "and he will not immediately lift it up."

- **ἐν ἡμέρᾳ τοῦ σαββάτου**: The preposition ἐν means "on" or "in." The noun ἡμέρᾳ means "day." The article τοῦ is the genitive singular of ὁ, meaning "the." The noun σαββάτου means "Sabbath." The phrase thus means "on the Sabbath day."

The literal translation of Luke 14:5 is: "And he said to them, Whose son or ox will fall into a well, and he will not immediately lift it up on the Sabbath day?"