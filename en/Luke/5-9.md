---
version: 1
---
- **θάμβος γὰρ περιέσχεν αὐτὸν**: The noun θάμβος means "astonishment" or "amazement." The verb περιέσχεν is the third person singular aorist indicative of περιέχω, which means "to surround" or "to enclose." The phrase thus means "astonishment surrounded him."

- **καὶ πάντας τοὺς σὺν αὐτῷ**: The conjunction καὶ means "and." The adjective πάντας is the accusative plural of πᾶς, which means "all" or "every." The definite article τοὺς is the accusative plural of ὁ, which means "the." The preposition σὺν means "with." The pronoun αὐτῷ is the dative singular of αὐτός, which means "himself" or "him." The phrase thus means "and all those with him."

- **ἐπὶ τῇ ἄγρᾳ τῶν ἰχθύων**: The preposition ἐπὶ means "on" or "at." The definite article τῇ is the dative singular of ὁ. The noun ἄγρᾳ means "shore" or "beach." The genitive plural τῶν is the genitive plural of ὁ. The noun ἰχθύων means "fish." The phrase thus means "on the shore of the fish."

- **ὧν συνέλαβον**: The relative pronoun ὧν is the genitive plural of ὅς, ἥ, ὅ. The verb συνέλαβον is the third person plural aorist indicative of συλλαμβάνω, which means "to catch" or "to seize." The phrase thus means "of which they caught."

The literal translation of the verse is: "For astonishment surrounded him and all those with him on the shore of the fish, of which they caught."