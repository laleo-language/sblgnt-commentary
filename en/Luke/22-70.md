---
version: 1
---
- **εἶπαν δὲ πάντες**: The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said." The word πάντες is the nominative plural of πᾶς, which means "all" or "everyone." The phrase thus means "they all said."

- **Σὺ οὖν εἶ ὁ υἱὸς τοῦ θεοῦ**: The word Σὺ is the nominative singular of σύ, which means "you." The word οὖν is an adverb meaning "therefore" or "so." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The phrase ὁ υἱὸς τοῦ θεοῦ means "the son of God." The phrase thus means "So you are the son of God."

- **ὁ δὲ πρὸς αὐτοὺς ἔφη**: The word ὁ is the nominative singular of the definite article, meaning "the." The word δὲ is a conjunction meaning "but" or "and." The word πρὸς is a preposition meaning "to" or "towards." The word αὐτοὺς is the accusative plural of αὐτός, meaning "them." The verb ἔφη is the third person singular imperfect indicative of φημί, meaning "he said." The phrase thus means "But he said to them."

- **Ὑμεῖς λέγετε ὅτι ἐγώ εἰμι**: The word Ὑμεῖς is the nominative plural of σύ, meaning "you." The verb λέγετε is the second person plural present indicative of λέγω, meaning "you say." The word ὅτι is a conjunction meaning "that." The pronoun ἐγώ is the first person singular nominative pronoun, meaning "I." The verb εἰμί is the first person singular present indicative of εἰμί, meaning "I am." The phrase thus means "You say that I am."

The literal translation of the entire verse is: "They all said, 'So you are the son of God?' But he said to them, 'You say that I am.'"