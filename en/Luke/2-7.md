---
version: 1
---
- **καὶ ἔτεκεν τὸν υἱὸν αὐτῆς τὸν πρωτότοκον**:
  - καὶ: a conjunction meaning "and"
  - ἔτεκεν: a verb, the third person singular aorist indicative active of τίκτω, meaning "she gave birth"
  - τὸν υἱὸν: a noun phrase consisting of the article τόν, the accusative singular of υἱός meaning "the son"
  - αὐτῆς: a pronoun, the genitive singular feminine of αὐτός meaning "her"
  - τὸν πρωτότοκον: a noun phrase consisting of the article τόν, the accusative singular of πρωτότοκος meaning "the firstborn"
- **καὶ ἐσπαργάνωσεν αὐτὸν**:
  - καὶ: a conjunction meaning "and"
  - ἐσπαργάνωσεν: a verb, the third person singular aorist indicative active of σπαργανόω, meaning "she wrapped"
  - αὐτὸν: a pronoun, the accusative singular masculine of αὐτός meaning "him"
- **καὶ ἀνέκλινεν αὐτὸν ἐν φάτνῃ**:
  - καὶ: a conjunction meaning "and"
  - ἀνέκλινεν: a verb, the third person singular aorist indicative active of ἀνακλίνω, meaning "she laid down"
  - αὐτὸν: a pronoun, the accusative singular masculine of αὐτός meaning "him"
  - ἐν φάτνῃ: a prepositional phrase consisting of the preposition ἐν meaning "in" and the noun φάτνη meaning "manger"
- **διότι οὐκ ἦν αὐτοῖς τόπος ἐν τῷ καταλύματι**:
  - διότι: a conjunction meaning "because"
  - οὐκ: an adverb meaning "not"
  - ἦν: a verb, the third person singular imperfect indicative active of εἰμί, meaning "there was"
  - αὐτοῖς: a pronoun, the dative plural masculine of αὐτός meaning "to them"
  - τόπος: a noun meaning "place"
  - ἐν τῷ καταλύματι: a prepositional phrase consisting of the preposition ἐν meaning "in" and the noun κατάλυμα meaning "lodging place"

Literal translation: "And she gave birth to her firstborn son, and she wrapped him and laid him in a manger, because there was no place for them in the inn."

This verse describes the birth of Jesus and the circumstances surrounding it. Mary gave birth to her firstborn son and then wrapped him in swaddling clothes and laid him in a manger. This was because there was no room for them in the inn.