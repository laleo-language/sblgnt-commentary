---
version: 1
---
- **ἐν μιᾷ τῶν ἡμερῶν**: The preposition ἐν means "in" and the noun μία is the feminine form of εἷς, meaning "one." The genitive τῶν ἡμερῶν means "of the days." So this phrase can be translated as "on one of the days."

- **διδάσκοντος αὐτοῦ τὸν λαὸν**: The participle διδάσκοντος is the genitive masculine singular form of διδάσκω, meaning "teaching." The pronoun αὐτοῦ means "his" (referring to Jesus). The accusative τὸν λαόν means "the people." So this phrase can be translated as "while he was teaching the people."

- **ἐν τῷ ἱερῷ**: The preposition ἐν means "in" and the noun τῷ ἱερῷ means "the temple." So this phrase can be translated as "in the temple."

- **καὶ εὐαγγελιζομένου**: The conjunction καὶ means "and" and the participle εὐαγγελιζομένου is the genitive masculine singular form of εὐαγγελίζομαι, meaning "proclaiming the good news." So this phrase can be translated as "and while he was proclaiming the good news."

- **ἐπέστησαν οἱ ἀρχιερεῖς καὶ οἱ γραμματεῖς σὺν τοῖς πρεσβυτέροις**: The verb ἐπέστησαν is the third person plural aorist indicative of ἐφίστημι, meaning "they approached" or "they came." The article οἱ before ἀρχιερεῖς means "the" and the noun ἀρχιερεῖς means "chief priests." The conjunction καὶ means "and" and the article οἱ before γραμματεῖς means "the" and the noun γραμματεῖς means "scribes." The preposition σὺν means "with" and the article τοῖς before πρεσβυτέροις means "the" and the noun πρεσβύτεροις means "elders." So this phrase can be translated as "the chief priests and the scribes came with the elders."

The entire verse can be translated as: "And it happened on one of the days, while he was teaching the people in the temple and proclaiming the good news, the chief priests and the scribes came with the elders."