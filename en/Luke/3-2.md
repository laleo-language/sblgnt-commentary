---
version: 1
---
- **ἐπὶ ἀρχιερέως Ἅννα καὶ Καϊάφα**: The preposition ἐπὶ means "upon" or "during." The noun ἀρχιερέως is the genitive singular of ἀρχιερεύς, meaning "high priest." The names Ἅννα and Καϊάφα are in the genitive case, indicating possession or association. The phrase can be translated as "during the high priesthood of Annas and Caiaphas."

- **ἐγένετο ῥῆμα θεοῦ**: The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it came to pass." The noun ῥῆμα is the nominative singular of ῥῆμα, which can mean "word" or "message." The genitive θεοῦ means "of God." The phrase can be translated as "the word of God came."

- **ἐπὶ Ἰωάννην τὸν Ζαχαρίου υἱὸν**: The preposition ἐπὶ means "upon" or "to." The noun Ἰωάννην is the accusative singular of Ἰωάννης, which is the Greek form of "John." The article τὸν is the accusative singular masculine definite article, indicating that Ἰωάννην is the direct object of the verb. The name Ζαχαρίου is in the genitive case, indicating possession or association. The noun υἱὸν is the accusative singular of υἱός, meaning "son." The phrase can be translated as "to John the son of Zechariah."

- **ἐν τῇ ἐρήμῳ**: The preposition ἐν means "in" or "at." The article τῇ is the dative singular feminine definite article, indicating that ἐρήμῳ is in the dative case. The noun ἐρήμῳ is the dative singular of ἔρημος, meaning "wilderness" or "desert." The phrase can be translated as "in the wilderness."

Literal translation: "During the high priesthood of Annas and Caiaphas, the word of God came to John the son of Zechariah in the wilderness."

In this verse, we see a description of the historical context in which the word of God came to John the Baptist. The phrase "during the high priesthood of Annas and Caiaphas" provides a specific timeframe for when this event occurred. The phrase "the word of God came to John the son of Zechariah" indicates that John received a message or revelation from God. Finally, the phrase "in the wilderness" tells us the location where John was when this happened.