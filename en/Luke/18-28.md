---
version: 1
---
- **Εἶπεν δὲ ὁ Πέτρος**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The article ὁ is the nominative singular masculine definite article, meaning "the." The name Πέτρος is the nominative singular masculine form of Πέτρος, meaning "Peter." The phrase thus means "Peter said."

- **Ἰδοὺ ἡμεῖς**: The word Ἰδοὺ is an interjection which means "behold" or "look." The pronoun ἡμεῖς is the nominative plural first person pronoun, meaning "we." The phrase thus means "Behold, we."

- **ἀφέντες τὰ ἴδια**: The participle ἀφέντες is the nominative plural masculine aorist participle of ἀφίημι, meaning "having left" or "having abandoned." The article τὰ is the accusative plural neuter definite article, meaning "the." The adjective ἴδια is the accusative plural neuter form of ἴδιος, meaning "own." The phrase thus means "having left their own."

- **ἠκολουθήσαμέν σοι**: The verb ἠκολουθήσαμεν is the first person plural aorist indicative of ἀκολουθέω, meaning "we followed." The pronoun σοι is the dative singular second person pronoun, meaning "to you." The phrase thus means "we followed you."

The literal translation of the verse is "Peter said, 'Behold, we, having left our own, followed you.'"