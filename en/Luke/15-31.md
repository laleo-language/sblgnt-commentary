---
version: 1
---
- **ὁ δὲ εἶπεν αὐτῷ**: The article ὁ is the nominative singular masculine form of ὁ, which means "the." The conjunction δὲ means "but" or "and." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτῷ is the dative singular masculine form of αὐτός, which means "to him." The phrase thus means "But he said to him."

- **Τέκνον**: This is a vocative singular neuter form of τέκνον, which means "child." It is used as an address, similar to saying "my child" or "son."

- **σὺ πάντοτε μετʼ ἐμοῦ εἶ**: The pronoun σὺ is the nominative singular second person form of σύ, which means "you." The adverb πάντοτε means "always" or "at all times." The preposition μετʼ means "with." The pronoun ἐμοῦ is the genitive singular first person form of ἐγώ, which means "me" or "I." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The phrase thus means "You are always with me."

- **καὶ πάντα τὰ ἐμὰ σά ἐστιν**: The conjunction καὶ means "and." The pronoun πάντα is the accusative plural neuter form of πᾶς, which means "all" or "everything." The article τὰ is the accusative plural neuter form of ὁ, meaning "the." The pronoun ἐμὰ is the accusative plural neuter form of ἐμός, which means "my." The pronoun σά is the accusative singular second person form of σύ, meaning "you." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is" or "are." The phrase thus means "And all my things are yours."

The literal translation of Luke 15:31 is: "But he said to him, 'Child, you are always with me, and all my things are yours.'"