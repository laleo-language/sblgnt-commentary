---
version: 1
---
- **Προσέφερον δὲ αὐτῷ**: The verb προσέφερον is the imperfect indicative active, third person plural of προσφέρω, meaning "they were bringing" or "they were presenting." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him" or "to Jesus." The phrase thus means "they were bringing to him."

- **καὶ τὰ βρέφη**: The conjunction καὶ means "and," and the article τὰ is the nominative/accusative neuter plural of ὁ, meaning "the." The noun βρέφη is the nominative/accusative neuter plural of βρέφος, meaning "infants" or "babies." The phrase thus means "and the infants."

- **ἵνα αὐτῶν ἅπτηται**: The conjunction ἵνα introduces a purpose clause, meaning "so that." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "of them" or "their." The verb ἅπτηται is the present subjunctive middle/passive, third person singular of ἅπτομαι, meaning "he might touch" or "he might lay hands on." The phrase thus means "so that he might touch them" or "so that he might lay hands on them."

- **ἰδόντες δὲ οἱ μαθηταὶ**: The participle ἰδόντες is the aorist active nominative masculine plural of ὁράω, meaning "seeing." The conjunction δὲ means "and" or "but." The definite article οἱ is the nominative masculine plural of ὁ, meaning "the." The noun μαθηταὶ is the nominative masculine plural of μαθητής, meaning "disciples" or "students." The phrase thus means "but the disciples, seeing."

- **ἐπετίμων αὐτοῖς**: The verb ἐπετίμων is the imperfect indicative active, third person plural of ἐπιτιμάω, meaning "they were rebuking" or "they were scolding." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "they were rebuking them."

Literal translation: "They were bringing to him also the infants so that he might touch them, but the disciples, seeing, were rebuking them."

The verse describes a scene where people were bringing infants to Jesus, hoping that he would touch them. However, the disciples were rebuking those who were bringing the infants.