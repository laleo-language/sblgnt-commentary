---
version: 1
---
- **καὶ αὐτοὶ**: The word καὶ is a conjunction meaning "and." The word αὐτοὶ is the nominative plural masculine pronoun, which means "they" or "themselves." The phrase means "and they."

- **ὡμίλουν πρὸς ἀλλήλους**: The verb ὡμίλουν is the imperfect indicative of ὁμιλέω, which means "they were talking" or "they were conversing." The preposition πρὸς means "to" or "towards." The pronoun ἀλλήλους is the accusative plural of ἀλλήλων, which means "one another" or "each other." The phrase means "they were talking to one another."

- **περὶ πάντων τῶν συμβεβηκότων τούτων**: The preposition περὶ means "about" or "concerning." The word πάντων is the genitive plural of πᾶς, which means "all" or "every." The definite article τῶν is the genitive plural of ὁ, which means "the." The participle συμβεβηκότων is the genitive plural masculine of the perfect participle συμβαίνω, which means "having happened" or "having occurred." The pronoun τούτων is the genitive plural of οὗτος, which means "these." The phrase means "about all the things that had happened."

The literal translation of the verse is: "And they were talking to one another about all the things that had happened."