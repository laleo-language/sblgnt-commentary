---
version: 1
---
- **καὶ ἐταράχθη Ζαχαρίας ἰδών**: The conjunction καὶ means "and". The verb ἐταράχθη is the third person singular aorist passive indicative of ταράσσω, meaning "he was troubled" or "he was disturbed". The name Ζαχαρίας is the nominative singular of Ζαχαρίας, which is the Greek equivalent of "Zechariah". The participle ἰδών is the nominative singular masculine of ὁράω, meaning "seeing" or "having seen". The phrase can be translated as "And Zechariah was troubled, seeing..."

- **καὶ φόβος ἐπέπεσεν ἐπʼ αὐτόν**: The conjunction καὶ means "and". The noun φόβος is the nominative singular of φόβος, which means "fear" or "terror". The verb ἐπέπεσεν is the third person singular aorist active indicative of ἐπιπίπτω, meaning "it fell upon" or "it came upon". The preposition ἐπʼ means "upon" or "on". The pronoun αὐτόν is the accusative singular masculine of αὐτός, meaning "him" or "himself". The phrase can be translated as "And fear fell upon him."

The literal translation of Luke 1:12 is "And Zechariah was troubled, seeing, and fear fell upon him."