---
version: 1
---
- **ἦλθεν γὰρ**: The verb ἦλθεν is the third person singular aorist indicative of ἔρχομαι, meaning "he came." The particle γὰρ is a conjunction that introduces a reason or explanation, and can be translated as "for" or "because." The phrase thus means "for he came."

- **ὁ υἱὸς τοῦ ἀνθρώπου**: The article ὁ is the nominative singular masculine of the definite article, meaning "the." The noun υἱὸς is the nominative singular masculine of υἱός, meaning "son." The preposition τοῦ is the genitive singular of the definite article, meaning "of the." The noun ἀνθρώπου is the genitive singular masculine of ἄνθρωπος, meaning "man." The phrase together means "the son of man."

- **ζητῆσαι καὶ σῶσαι**: The infinitive ζητῆσαι is the aorist infinitive of ζητέω, meaning "to seek." The conjunction καὶ means "and." The infinitive σῶσαι is the aorist infinitive of σῴζω, meaning "to save." The phrase means "to seek and to save."

- **τὸ ἀπολωλός**: The article τὸ is the accusative singular neuter of the definite article, meaning "the." The participle ἀπολωλός is the nominative singular neuter of ἀπολωλώς, meaning "lost." The phrase means "the lost."

Literal translation: "For the son of man came to seek and to save the lost."