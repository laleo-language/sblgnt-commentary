---
version: 1
---
- **ὁ δὲ θεὸς**: The word θεὸς is the nominative singular of θεός, which means "God." The article ὁ is the definite article, indicating that this is "the God."

- **οὐ μὴ ποιήσῃ**: The word οὐ is the negative particle, meaning "not." The verb ποιήσῃ is the third person singular aorist subjunctive of ποιέω, meaning "to do" or "to make." The phrase οὐ μὴ ποιήσῃ thus means "will not do."

- **τὴν ἐκδίκησιν**: The word ἐκδίκησιν is the accusative singular of ἐκδίκησις, which means "vengeance" or "justice." The article τὴν is the definite article, indicating that this is "the vengeance."

- **τῶν ἐκλεκτῶν αὐτοῦ**: The word ἐκλεκτῶν is the genitive plural of ἐκλεκτός, which means "chosen" or "elect." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his" or "of him." The phrase τῶν ἐκλεκτῶν αὐτοῦ means "of his chosen ones."

- **τῶν βοώντων αὐτῷ ἡμέρας καὶ νυκτός**: The word βοώντων is the genitive plural present participle of βοάω, meaning "to cry out" or "to call." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase ἡμέρας καὶ νυκτός means "day and night." The phrase τῶν βοώντων αὐτῷ ἡμέρας καὶ νυκτός thus means "of those who cry out to him day and night."

- **καὶ μακροθυμεῖ ἐπʼ αὐτοῖς**: The verb μακροθυμεῖ is the third person singular present indicative of μακροθυμέω, meaning "to be patient" or "to endure." The preposition ἐπʼ is a contraction of ἐπί and the pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase καὶ μακροθυμεῖ ἐπʼ αὐτοῖς means "and he is patient with them."

The literal translation of Luke 18:7 is: "But God will not do the vengeance of his chosen ones who cry out to him day and night, and he is patient with them."