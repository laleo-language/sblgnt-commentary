---
version: 1
---
- **Καὶ Ἰησοῦς**: The word Καὶ is a conjunction meaning "and." The name Ἰησοῦς is the nominative singular form of Ἰησοῦς, which is the Greek equivalent of the name Jesus. The phrase means "And Jesus."

- **προέκοπτεν**: The verb προέκοπτεν is the third person singular imperfect indicative of προκόπτω, meaning "to make progress" or "to advance." The imperfect tense indicates an ongoing action in the past. The phrase means "was making progress."

- **σοφίᾳ καὶ ἡλικίᾳ**: The word σοφίᾳ is the dative singular form of σοφία, which means "wisdom." The word ἡλικίᾳ is the dative singular form of ἡλικία, which means "age" or "stature." The phrase means "in wisdom and age."

- **καὶ χάριτι**: The word χάριτι is the dative singular form of χάρις, which means "grace" or "favor." The phrase means "and favor."

- **παρὰ θεῷ καὶ ἀνθρώποις**: The word παρὰ is a preposition meaning "from" or "by." The word θεῷ is the dative singular form of θεός, which means "God." The word ἀνθρώποις is the dative plural form of ἄνθρωπος, which means "people" or "humans." The phrase means "from God and people."

The literal translation of the verse is: "And Jesus was making progress in wisdom and age, and favor from God and people."