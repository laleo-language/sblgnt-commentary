---
version: 1
---
- **μακάριος ὁ δοῦλος ἐκεῖνος**: The word μακάριος is an adjective meaning "blessed" or "happy." It agrees with the noun δοῦλος, which means "servant" or "slave." The pronoun ἐκεῖνος means "that" or "that one" and refers back to the servant mentioned earlier. So the phrase can be translated as "blessed is that servant."

- **ὃν ἐλθὼν ὁ κύριος αὐτοῦ εὑρήσει**: The word ὃν is a relative pronoun meaning "whom" and refers to the servant. The verb ἐλθὼν is the aorist participle of ἔρχομαι, meaning "having come" or "when he comes." The subject of the verb is ὁ κύριος, which means "the master" or "the lord." The possessive pronoun αὐτοῦ means "his." The verb εὑρήσει is the future indicative of εὑρίσκω, meaning "he will find." So the phrase can be translated as "whom his master, when he comes, will find."

- **ποιοῦντα οὕτως**: The participle ποιοῦντα is the present active participle of ποιέω, meaning "doing" or "performing." It agrees with the servant mentioned earlier. The adverb οὕτως means "in this way" or "thus." So the phrase can be translated as "doing in this way."

The entire verse can be translated as "Blessed is that servant whom his master, when he comes, will find doing in this way."