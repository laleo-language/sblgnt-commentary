---
version: 1
---
- **καὶ φωνὴ ἐγένετο ἐκ τῆς νεφέλης**: The word φωνὴ is the nominative singular of φωνή, which means "voice." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it became." The preposition ἐκ means "from" and the noun νεφέλης is the genitive singular of νεφέλη, which means "cloud." The phrase thus means "And a voice came from the cloud."

- **λέγουσα**: The participle λέγουσα is the nominative singular feminine present active participle of λέγω, meaning "saying." It agrees with the noun φωνὴ and modifies it. The participle indicates that the voice is actively speaking.

- **Οὗτός ἐστιν ὁ υἱός μου ὁ ἐκλελεγμένος**: The word Οὗτός is the nominative singular masculine of οὗτος, which means "this." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The noun υἱός is the nominative singular of υἱός, which means "son." The pronoun μου is the genitive singular of ἐγώ, meaning "my." The adjective ἐκλελεγμένος is the nominative singular masculine perfect passive participle of ἐκλέγομαι, meaning "chosen" or "elect." The phrase thus means "This is my chosen son."

- **αὐτοῦ ἀκούετε**: The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The verb ἀκούετε is the second person plural present indicative of ἀκούω, meaning "you hear" or "you listen." The phrase thus means "Listen to him."

Literal translation: "And a voice came from the cloud saying, 'This is my chosen son, listen to him.'"

In this verse, we see Jesus being transfigured before Peter, John, and James. A voice comes from the cloud and declares that Jesus is the chosen son of God, instructing the disciples to listen to him.