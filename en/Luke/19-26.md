---
version: 1
---
- **λέγω ὑμῖν**: The verb λέγω is the first person singular present indicative of λέγω, meaning "I say." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The phrase thus means "I say to you."

- **ὅτι παντὶ τῷ ἔχοντι δοθήσεται**: The conjunction ὅτι introduces a subordinate clause and does not have a direct translation in English. The pronoun παντὶ is the dative singular of πᾶς, meaning "to every" or "to everyone." The article τῷ is the dative singular of ὁ, meaning "the." The participle ἔχοντι is the present active participle of ἔχω, meaning "having." The verb δοθήσεται is the third person singular future passive indicative of δίδωμι, meaning "it will be given." The phrase thus means "it will be given to everyone who has."

- **ἀπὸ δὲ τοῦ μὴ ἔχοντος καὶ ὃ ἔχει ἀρθήσεται**: The preposition ἀπὸ means "from." The conjunction δὲ is used to indicate contrast and can be translated as "but" or "however." The article τοῦ is the genitive singular of ὁ, meaning "the." The participle μὴ ἔχοντος is the present active participle of ἔχω with the negative particle μὴ, meaning "not having." The conjunction καὶ means "and." The pronoun ὃ is the accusative singular of ὅς, meaning "what" or "that which." The verb ἔχει is the third person singular present active indicative of ἔχω, meaning "he has." The verb ἀρθήσεται is the third person singular future passive indicative of αἴρω, meaning "it will be taken away." The phrase thus means "but from the one who does not have, even what he has will be taken away."

The verse as a whole can be translated as: "I say to you that to everyone who has, it will be given, but from the one who does not have, even what he has will be taken away."