---
version: 1
---
- **τότε**: This is an adverb meaning "then" or "at that time."
- **οἱ ἐν τῇ Ἰουδαίᾳ**: "The ones in Judea." 
- **φευγέτωσαν**: This is the third person plural aorist imperative of φεύγω, meaning "let them flee" or "let them run away."
- **εἰς τὰ ὄρη**: "to the mountains."
- **καὶ οἱ ἐν μέσῳ αὐτῆς**: "and the ones in the midst of it."
- **ἐκχωρείτωσαν**: This is the third person plural present imperative of ἐκχωρέω, meaning "let them depart" or "let them go out."
- **καὶ οἱ ἐν ταῖς χώραις**: "and the ones in the countryside."
- **μὴ εἰσερχέσθωσαν εἰς αὐτήν**: "let them not enter into it."

Literal translation: "Then let the ones in Judea flee to the mountains, and let the ones in the midst of it depart, and let the ones in the countryside not enter into it."

This verse is an instruction given by Jesus to his disciples regarding what they should do when they see the signs of the impending destruction of Jerusalem. The imperative verbs indicate commands or instructions, and the prepositional phrases indicate the destinations or actions to be taken. The sentence is not grammatically ambiguous.