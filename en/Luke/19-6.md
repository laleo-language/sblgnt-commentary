---
version: 1
---
- **καὶ σπεύσας κατέβη**: The word σπεύσας is the nominative singular masculine participle of σπεύδω, meaning "hurrying" or "rushing." The verb κατέβη is the third person singular aorist indicative of καταβαίνω, which means "he came down." The phrase thus means "hurrying, he came down."

- **καὶ ὑπεδέξατο αὐτὸν χαίρων**: The verb ὑπεδέξατο is the third person singular aorist middle indicative of ὑποδέχομαι, meaning "he received" or "he welcomed." The pronoun αὐτὸν is the accusative singular masculine form of αὐτός, meaning "him." The participle χαίρων is the nominative singular masculine form of χαίρω, meaning "rejoicing" or "being glad." The phrase can be translated as "he welcomed him, rejoicing."

The literal translation of Luke 19:6 is "Hurrying, he came down and welcomed him, rejoicing."