---
version: 1
---
- **καὶ τῇδε ἦν ἀδελφὴ καλουμένη Μαριάμ**: The word καὶ is a conjunction meaning "and." The word τῇδε is a demonstrative pronoun meaning "this" or "this one." The word ἦν is the third person singular imperfect indicative of εἰμί, meaning "he/she/it was." The word ἀδελφὴ is the nominative singular of ἀδελφή, meaning "sister." The word καλουμένη is the feminine nominative singular participle of καλέω, meaning "called" or "named." The name Μαριάμ is a proper noun meaning "Mary." The phrase thus means "And there was a sister called Mary."

- **ἣ καὶ παρακαθεσθεῖσα πρὸς τοὺς πόδας τοῦ Ἰησοῦ**: The word ἣ is a relative pronoun meaning "who" or "whom." The word καὶ is a conjunction meaning "and." The word παρακαθεσθεῖσα is the feminine nominative singular participle of παρακαθίζω, meaning "sitting beside" or "reclining." The word πρὸς is a preposition meaning "to" or "towards." The word τοὺς πόδας is the accusative plural of ὁ πούς, meaning "the feet." The word τοῦ Ἰησοῦ is the genitive singular of Ἰησοῦς, meaning "of Jesus." The phrase thus means "who also was sitting beside the feet of Jesus."

- **ἤκουεν τὸν λόγον αὐτοῦ**: The word ἤκουεν is the third person singular imperfect indicative of ἀκούω, meaning "he/she/it was listening" or "he/she/it was hearing." The word τὸν λόγον is the accusative singular of ὁ λόγος, meaning "the word" or "the message." The word αὐτοῦ is the genitive singular of αὐτός, meaning "his" or "his own." The phrase thus means "she was listening to his word."

- **καὶ τῇδε ἦν ἀδελφὴ καλουμένη Μαριάμ, ἣ καὶ παρακαθεσθεῖσα πρὸς τοὺς πόδας τοῦ Ἰησοῦ ἤκουεν τὸν λόγον αὐτοῦ**: And there was a sister called Mary, who also was sitting beside the feet of Jesus, listening to his word.

In this verse, we learn about Mary, who is described as the sister of someone. Mary is sitting beside Jesus and listening to his word.