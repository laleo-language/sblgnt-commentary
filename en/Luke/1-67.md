---
version: 1
---
- **Καὶ Ζαχαρίας ὁ πατὴρ αὐτοῦ**: The word Καὶ means "and." Ζαχαρίας is the nominative singular form of the name Ζαχαρίας, which is the Greek equivalent of the Hebrew name Zechariah. ὁ is the definite article "the" in the masculine singular form. πατὴρ is the nominative singular form of πατήρ, which means "father." αὐτοῦ is the genitive singular form of αὐτός, meaning "his." So this phrase translates to "And Zechariah his father."

- **ἐπλήσθη πνεύματος ἁγίου**: The verb ἐπλήσθη is the third person singular aorist passive indicative of πίμπλημι, which means "to be filled." πνεύματος is the genitive singular form of πνεῦμα, which means "spirit." ἁγίου is the genitive singular form of ἅγιος, which means "holy." So this phrase translates to "was filled with the Holy Spirit."

- **καὶ ἐπροφήτευσεν λέγων**: The conjunction καὶ means "and." ἐπροφήτευσεν is the third person singular aorist indicative active of προφητεύω, meaning "he prophesied." λέγων is the present participle active nominative singular form of λέγω, which means "saying." So this phrase translates to "and he prophesied, saying."

The entire verse translates to: "And Zechariah his father was filled with the Holy Spirit and prophesied, saying,"

In this verse, we see Zechariah, the father of someone, being filled with the Holy Spirit and prophesying.