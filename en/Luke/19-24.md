---
version: 1
---
- **καὶ τοῖς παρεστῶσιν εἶπεν**: The conjunction καὶ means "and." The article τοῖς is the dative plural of ὁ, which means "the." The participle παρεστῶσιν is the dative plural of παρίστημι, meaning "to stand beside" or "to be present." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase thus means "and he said to those who were present."

- **Ἄρατε ἀπʼ αὐτοῦ τὴν μνᾶν**: The verb Ἄρατε is the second person plural imperative of αἴρω, meaning "take" or "pick up." The preposition ἀπʼ means "from." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, meaning "his" or "of him." The article τὴν is the accusative singular feminine of ὁ, meaning "the." The noun μνᾶν is the accusative singular of μνᾶ, which was a unit of currency. The phrase thus means "Take from him the mina."

- **καὶ δότε τῷ τὰς δέκα μνᾶς ἔχοντι**: The conjunction καὶ means "and." The verb δότε is the second person plural imperative of δίδωμι, meaning "give." The article τῷ is the dative singular masculine of ὁ, meaning "to the." The article τὰς is the accusative plural feminine of ὁ. The noun δέκα is the cardinal number "ten." The noun μνᾶς is the accusative plural of μνᾶ. The participle ἔχοντι is the dative singular masculine of ἔχω, meaning "having" or "possessing." The phrase thus means "and give it to the one who has the ten minas."

The syntax of this verse is not ambiguous.

The literal translation of this verse is: "And he said to those who were present, 'Take from him the mina and give it to the one who has the ten minas.'"