---
version: 1
---
- **γενόμενος δὲ ἐπὶ τοῦ τόπου**: The participle γενόμενος is the nominative singular masculine form of γίνομαι, which means "having come" or "having arrived." The preposition ἐπὶ means "upon" or "on," and τοῦ τόπου is the genitive singular form of τόπος, which means "place." The phrase thus means "having come upon the place" or "having arrived at the place."

- **εἶπεν αὐτοῖς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, which means "he said." The pronoun αὐτοῖς is the dative plural form of αὐτός, meaning "to them." The phrase means "he said to them."

- **Προσεύχεσθε μὴ εἰσελθεῖν εἰς πειρασμόν**: The verb προσεύχεσθε is the second person plural present middle/passive imperative of προσεύχομαι, meaning "pray." The particle μὴ is a negative particle, meaning "not." The infinitive εἰσελθεῖν is the aorist infinitive of εἰσέρχομαι, which means "to enter." The preposition εἰς means "into," and πειρασμόν is the accusative singular form of πειρασμός, meaning "temptation." The phrase therefore means "Pray not to enter into temptation."

The literal translation of Luke 22:40 is: "Having come upon the place, he said to them, 'Pray not to enter into temptation.'"