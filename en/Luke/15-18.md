---
version: 1
---
- **ἀναστὰς**: The word ἀναστὰς is the nominative singular masculine participle of ἀνίστημι, meaning "having risen" or "having stood up." It is functioning here as a temporal participle, indicating the action that precedes the main verb.

- **πορεύσομαι**: The word πορεύσομαι is the first person singular future indicative of πορεύομαι, which means "I will go" or "I will journey."

- **πρὸς τὸν πατέρα μου**: The phrase πρὸς τὸν πατέρα μου consists of the preposition πρὸς, meaning "to" or "toward," followed by the accusative singular masculine article τὸν and the noun πατέρα, meaning "father," in the accusative singular. The pronoun μου means "my." The phrase can be translated as "to my father."

- **καὶ ἐρῶ αὐτῷ**: The conjunction καὶ means "and." The verb ἐρῶ is the first person singular future indicative of λέγω, meaning "I will say" or "I will speak." The pronoun αὐτῷ means "to him." The phrase can be translated as "and I will say to him."

- **Πάτερ**: The word Πάτερ means "Father." It is used here as a direct address.

- **ἥμαρτον εἰς τὸν οὐρανὸν καὶ ἐνώπιόν σου**: The phrase ἥμαρτον εἰς τὸν οὐρανὸν καὶ ἐνώπιόν σου consists of the verb ἥμαρτον, which is the first person singular aorist indicative of ἁμαρτάνω, meaning "I have sinned" or "I sinned," followed by the preposition εἰς, meaning "to," the accusative singular masculine article τὸν, the noun οὐρανὸν, meaning "heaven," in the accusative singular, the conjunction καὶ meaning "and," and the preposition ἐνώπιόν, meaning "before" or "in the presence of," and the pronoun σου, meaning "you." The phrase can be translated as "I have sinned against heaven and before you."

Literal translation: "Having risen, I will go to my father, and I will say to him, 'Father, I have sinned against heaven and before you.'"