---
version: 1
---
- **πλὴν δεῖ με**: The word πλὴν means "except" or "but." The verb δεῖ is the third person singular present indicative of δέω, which means "it is necessary." The pronoun με is the accusative form of ἐγώ, meaning "me." The phrase thus means "but it is necessary for me."

- **σήμερον καὶ αὔριον καὶ τῇ ἐχομένῃ**: The word σήμερον means "today," αὔριον means "tomorrow," and τῇ ἐχομένῃ means "the next day." These are all adverbs indicating time. The phrase thus means "today and tomorrow and the next day."

- **πορεύεσθαι**: This is the present middle infinitive of πορεύομαι, meaning "to go" or "to journey."

- **ὅτι οὐκ ἐνδέχεται προφήτην ἀπολέσθαι ἔξω Ἰερουσαλήμ**: The word ὅτι means "because" or "that." The verb οὐκ ἐνδέχεται is the third person singular present indicative of ἐνδύω, which means "it is possible" or "it is able." The noun προφήτην is the accusative form of προφήτης, meaning "prophet." The verb ἀπολέσθαι is the present middle infinitive of ἀπόλλυμι, meaning "to destroy" or "to perish." The preposition ἔξω means "outside" or "out of." The noun Ἰερουσαλήμ is the accusative form of Ἱερουσαλήμ, meaning "Jerusalem." The phrase thus means "because it is not possible for a prophet to perish outside Jerusalem."

The literal translation of the verse is: "But it is necessary for me to go today and tomorrow and the next day, because it is not possible for a prophet to perish outside Jerusalem."