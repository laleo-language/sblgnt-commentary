---
version: 1
---
- **ὅταν δὲ ⸀εἰσφέρωσιν ὑμᾶς**: The word ὅταν is a conjunction that means "when." The verb εἰσφέρωσιν is the third person plural aorist subjunctive of εἰσφέρω, which means "to bring in" or "to lead in." The pronoun ὑμᾶς is the accusative plural of ὑμείς, which means "you." The phrase thus means "when they bring you in."

- **ἐπὶ τὰς συναγωγὰς καὶ τὰς ἀρχὰς καὶ τὰς ἐξουσίας**: The preposition ἐπὶ means "to" or "into." The noun συναγωγὰς is the accusative plural of συναγωγή, which means "synagogues." The noun ἀρχὰς is the accusative plural of ἀρχή, which means "rulers" or "authorities." The noun ἐξουσίας is the accusative plural of ἐξουσία, which means "powers" or "authorities." The phrase thus means "to the synagogues and the rulers and the authorities."

- **μὴ ⸀μεριμνήσητε**: The negative particle μὴ indicates a prohibition, meaning "do not." The verb μεριμνήσητε is the second person plural aorist subjunctive of μεριμνάω, which means "to be anxious" or "to worry." The phrase thus means "do not be anxious."

- **πῶς ⸂ἢ τί⸃ ἀπολογήσησθε ἢ τί εἴπητε**: The interrogative adverb πῶς means "how." The conjunction ἢ is used here to present alternatives and can be translated as "or." The pronoun τί is the accusative singular of τίς, which means "what." The verb ἀπολογήσησθε is the second person plural future middle indicative of ἀπολογέομαι, which means "to defend oneself" or "to make a defense." The verb εἴπητε is the second person plural aorist active subjunctive of λέγω, which means "to say." The phrase thus means "how or what you will defend yourselves or what you will say."

The literal translation of Luke 12:11 is:
"When they bring you in to the synagogues and the rulers and the authorities, do not be anxious about how or what you will defend yourselves or what you will say."