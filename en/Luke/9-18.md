---
version: 1
---
- **Καὶ ἐγένετο**: The word Καὶ is a conjunction meaning "and." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it occurred."

- **ἐν τῷ εἶναι αὐτὸν προσευχόμενον**: The preposition ἐν means "in" or "at." The article τῷ is the dative singular of ὁ, which means "the." The noun εἶναι is the present active infinitive of εἰμί, meaning "to be." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "himself." The verb προσευχόμενον is the present middle participle of προσεύχομαι, meaning "praying." 

- **κατὰ μόνας**: The preposition κατὰ means "according to" or "in accordance with." The adjective μόνας is the accusative plural of μόνος, meaning "alone" or "by themselves."

- **συνῆσαν αὐτῷ οἱ μαθηταί**: The verb συνῆσαν is the third person plural aorist indicative of συνίημι, meaning "they understood" or "they perceived." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The article οἱ is the nominative plural of ὁ, which means "the." The noun μαθηταί is the nominative plural of μαθητής, meaning "disciples."

- **καὶ ἐπηρώτησεν αὐτοὺς λέγων**: The conjunction καὶ means "and." The verb ἐπηρώτησεν is the third person singular aorist indicative of ἐπερωτάω, meaning "he asked." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The participle λέγων is the present active participle of λέγω, meaning "saying."

- **Τίνα με οἱ ὄχλοι λέγουσιν εἶναι**: The pronoun Τίνα is the accusative singular of τίς, meaning "who" or "whom." The pronoun με is the accusative singular of ἐγώ, meaning "me." The article οἱ is the nominative plural of ὁ, which means "the." The noun ὄχλοι is the nominative plural of ὄχλος, meaning "crowds" or "multitudes." The verb λέγουσιν is the third person plural present indicative of λέγω, meaning "they say." The infinitive εἶναι is the present active infinitive of εἰμί, meaning "to be."

The literal translation of Luke 9:18 is: "And it happened, as he was praying alone, his disciples were with him, and he asked them, saying, 'Who do the crowds say that I am?'"