---
version: 1
---
- **καὶ οὐκ ἴσχυσαν ἐπιλαβέσθαι**: The conjunction καὶ means "and." The word οὐκ is the negation particle, meaning "not." The verb ἴσχυσαν is the third person plural aorist indicative of ἰσχύω, meaning "they were able" or "they had power." The infinitive ἐπιλαβέσθαι is the aorist infinitive of ἐπιλαμβάνομαι, which means "to take hold of" or "to grasp." The phrase thus means "and they were not able to take hold of."

- **τοῦ ῥήματος ἐναντίον τοῦ λαοῦ**: The article τοῦ indicates that the following noun is in the genitive case. The noun ῥῆμα means "word" or "message." The preposition ἐναντίον means "before" or "in the presence of." The preposition τοῦ indicates that the following noun is in the genitive case. The noun λαός means "people" or "crowd." The phrase thus means "the word in the presence of the people."

- **καὶ θαυμάσαντες**: The conjunction καὶ means "and." The participle θαυμάσαντες is the nominative plural masculine aorist active participle of θαυμάζω, meaning "having marveled" or "having wondered."

- **ἐπὶ τῇ ἀποκρίσει αὐτοῦ**: The preposition ἐπὶ means "at" or "upon." The article τῇ indicates that the following noun is in the dative case. The noun ἀπόκρισις means "answer" or "response." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, meaning "his" or "of him." The phrase thus means "at his answer."

- **ἐσίγησαν**: The verb ἐσίγησαν is the third person plural aorist indicative of σιγάω, meaning "they became silent" or "they were silent."

The literal translation of Luke 20:26 is: "And they were not able to take hold of the word in the presence of the people, and having marveled at his answer, they became silent."