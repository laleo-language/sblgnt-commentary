---
version: 1
---
- **πλὴν Τύρῳ καὶ Σιδῶνι**: The word πλὴν is a conjunction meaning "except" or "besides." Τύρῳ and Σιδῶνι are both dative singular forms of Τύρος and Σιδών, which are the names of cities. The phrase means "except for Tyre and Sidon."

- **ἀνεκτότερον ἔσται**: The word ἀνεκτότερον is the comparative form of ἀνεκτός, which means "more bearable" or "more tolerable." The verb ἔσται is the third person singular future indicative form of εἰμί, meaning "it will be." The phrase means "it will be more bearable."

- **ἐν τῇ κρίσει**: The word ἐν is a preposition meaning "in." Τῇ is the feminine dative singular form of the definite article, meaning "the." Κρίσει is the dative singular form of κρίσις, which means "judgment" or "sentence." The phrase means "in the judgment."

- **ἢ ὑμῖν**: The word ἢ is a conjunction meaning "than." Ὑμῖν is the second person plural pronoun, meaning "you." The phrase means "than you."

The literal translation of the verse is: "Except for Tyre and Sidon, it will be more bearable in the judgment than for you."