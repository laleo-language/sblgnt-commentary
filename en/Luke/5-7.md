---
version: 1
---
- **καὶ κατένευσαν**: The word καὶ is a conjunction meaning "and." The verb κατένευσαν is the third person plural aorist indicative of κατανεύω, meaning "they signaled" or "they motioned."

- **τοῖς μετόχοις**: The word τοῖς is the dative plural of the definite article ὁ, meaning "the." The noun μετόχοις is the dative plural of μέτοχος, which means "partners" or "companions."

- **ἐν τῷ ἑτέρῳ πλοίῳ**: The word ἐν is a preposition meaning "in" or "on." The article τῷ is the dative singular of ὁ. The adjective ἑτέρῳ is the dative singular masculine of ἕτερος, meaning "other" or "another." The noun πλοίῳ is the dative singular of πλοῖον, which means "boat" or "ship." The phrase means "in the other boat."

- **τοῦ ἐλθόντας συλλαβέσθαι αὐτοῖς**: The word τοῦ is the genitive singular of the definite article ὁ. The participle ἐλθόντας is the accusative plural masculine aorist active of ἔρχομαι, meaning "having come." The infinitive συλλαβέσθαι is the aorist middle infinitive of συλλαμβάνω, meaning "to catch" or "to take hold of." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase means "to catch them who had come."

- **καὶ ἦλθον**: The word καὶ is a conjunction meaning "and." The verb ἦλθον is the third person plural aorist indicative of ἔρχομαι, meaning "they came."

- **καὶ ἔπλησαν**: The word καὶ is a conjunction meaning "and." The verb ἔπλησαν is the third person plural aorist indicative of πίμπλημι, meaning "they filled."

- **ἀμφότερα τὰ πλοῖα**: The adjective ἀμφότερα is the neuter plural of ἀμφότερος, meaning "both." The article τὰ is the accusative plural of ὁ. The noun πλοῖα is the accusative plural of πλοῖον, which means "boats" or "ships." The phrase means "both the boats."

- **ὥστε βυθίζεσθαι αὐτά**: The conjunction ὥστε introduces a result clause and can be translated as "so that." The verb βυθίζεσθαι is the present middle infinitive of βυθίζω, meaning "to sink" or "to submerge." The pronoun αὐτά is the accusative plural of αὐτός, meaning "them." The phrase means "so that they were sinking."

The literal translation of Luke 5:7 is: "And they signaled to the partners in the other boat to come and help them. And they came, and they filled both the boats, so that they were sinking."