---
version: 1
---
- **καὶ ἀποκριθεὶς ὁ Ἰησοῦς**: The conjunction καὶ means "and." The participle ἀποκριθεὶς is the nominative masculine singular form of the verb ἀποκρίνομαι, which means "answered." The definite article ὁ is the nominative masculine singular form of the article ὁ, which means "the." The noun Ἰησοῦς is the nominative masculine singular form of Ἰησοῦς, which means "Jesus." The phrase thus means "And Jesus, answering," or "And Jesus, in response."

- **εἶπεν πρὸς αὐτούς**: The verb εἶπεν is the third person singular aorist indicative form of λέγω, which means "he said." The preposition πρὸς means "to" or "towards." The pronoun αὐτούς is the accusative plural form of αὐτός, which means "them." The phrase thus means "he said to them."

- **Οὐ χρείαν ἔχουσιν**: The negation Οὐ means "not." The noun χρείαν is the accusative singular form of χρεία, which means "need" or "necessity." The verb ἔχουσιν is the third person plural present indicative form of ἔχω, which means "they have." The phrase thus means "They do not have need" or "They do not need."

- **οἱ ὑγιαίνοντες**: The definite article οἱ is the nominative masculine plural form of ὁ, which means "the." The noun ὑγιαίνοντες is the nominative masculine plural form of ὑγιαίνω, which means "those who are healthy" or "the healthy ones." The phrase thus means "the healthy ones."

- **ἰατροῦ**: The noun ἰατροῦ is the genitive singular form of ἰατρός, which means "doctor" or "physician." It is in the genitive case, which indicates possession or association. The phrase thus means "of a doctor" or "of a physician."

- **ἀλλὰ οἱ κακῶς ἔχοντες**: The conjunction ἀλλὰ means "but" or "however." The definite article οἱ is the nominative masculine plural form of ὁ, which means "the." The adjective κακῶς is the adverbial form of κακός, which means "sick" or "ill." The verb ἔχοντες is the nominative masculine plural form of ἔχω, which means "they have." The phrase thus means "but the sick ones" or "but those who are sick."

The verse can be translated as: "And Jesus, answering, said to them, 'They do not need doctors, but those who are sick do.'"