---
version: 1
---
- **Ἄνθρωπος δέ τις ἦν πλούσιος**: The word Ἄνθρωπος is the nominative singular of ἄνθρωπος, which means "man" or "person." The word δέ is a conjunction that means "but" or "and." The word τις is an indefinite pronoun that means "someone" or "a certain." The verb ἦν is the third person singular imperfect indicative of εἰμί, which means "he was." The adjective πλούσιος is in the nominative singular to agree with ἄνθρωπος, and it means "rich." The phrase thus means "But there was a certain rich man."

- **καὶ ἐνεδιδύσκετο πορφύραν καὶ βύσσον εὐφραινόμενος καθʼ ἡμέραν λαμπρῶς**: The conjunction καὶ means "and." The verb ἐνεδιδύσκετο is the third person singular imperfect indicative middle/passive of ἐνδιδύσκω, which means "he was dressed in." The noun πορφύραν is the accusative singular of πορφύρα, which means "purple cloth." The noun βύσσον is the accusative singular of βύσσος, which means "fine linen." The participle εὐφραινόμενος is in the nominative singular masculine present middle participle of εὐφραίνω, which means "rejoicing" or "being glad." The prepositional phrase καθʼ ἡμέραν means "every day." The adverb λαμπρῶς means "splendidly" or "in a splendid manner." The phrase thus means "And he was dressed in purple cloth and fine linen, rejoicing every day splendidly."

- **Ἄνθρωπος δέ τις ἦν πλούσιος, καὶ ἐνεδιδύσκετο πορφύραν καὶ βύσσον εὐφραινόμενος καθʼ ἡμέραν λαμπρῶς**: But there was a certain rich man who was dressed in purple cloth and fine linen, rejoicing every day splendidly.

In this verse, Luke describes a rich man who was dressed luxuriously and enjoyed his wealth every day.