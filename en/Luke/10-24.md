---
version: 1
---
- **λέγω γὰρ ὑμῖν**: The verb λέγω is the first person singular present indicative of λέγω, meaning "I say" or "I am saying." The word γὰρ is a conjunction that means "for" or "because." The pronoun ὑμῖν is the second person plural pronoun, meaning "to you." The phrase thus means "for I say to you."

- **ὅτι πολλοὶ προφῆται καὶ βασιλεῖς ἠθέλησαν ἰδεῖν**: The word ὅτι is a conjunction that means "that." The adjective πολλοὶ is the nominative plural form of πολλός, meaning "many." The noun προφῆται is the nominative plural form of προφήτης, meaning "prophets." The conjunction καὶ means "and." The noun βασιλεῖς is the nominative plural form of βασιλεύς, meaning "kings." The verb ἠθέλησαν is the third person plural aorist indicative of θέλω, meaning "they wanted." The infinitive ἰδεῖν is the aorist infinitive of εἶδον, meaning "to see." The phrase thus means "that many prophets and kings wanted to see."

- **ἃ ὑμεῖς βλέπετε καὶ οὐκ εἶδαν**: The relative pronoun ἃ is the accusative neuter plural form of ὅς, meaning "which" or "what." The pronoun ὑμεῖς is the second person plural pronoun, meaning "you." The verb βλέπετε is the second person plural present indicative of βλέπω, meaning "you see." The conjunction καὶ means "and." The adverb οὐκ means "not." The verb εἶδαν is the third person plural aorist indicative of ὁράω, meaning "they saw." The phrase thus means "which you see and they did not see."

- **καὶ ἀκοῦσαι ἃ ἀκούετε καὶ οὐκ ἤκουσαν**: The conjunction καὶ means "and." The verb ἀκοῦσαι is the aorist infinitive of ἀκούω, meaning "to hear." The pronoun ἃ is the accusative neuter plural form of ὅς, meaning "which" or "what." The verb ἀκούετε is the second person plural present indicative of ἀκούω, meaning "you hear." The adverb οὐκ means "not." The verb ἤκουσαν is the third person plural aorist indicative of ἀκούω, meaning "they heard." The phrase thus means "and to hear what you hear and they did not hear."

The verse can be literally translated as: "For I say to you that many prophets and kings wanted to see what you see and did not see, and to hear what you hear and did not hear."