---
version: 1
---
- **καὶ ἤρξαντο οἱ συνανακείμενοι λέγειν**: The word καὶ is a conjunction meaning "and". The verb ἤρξαντο is the third person plural aorist middle indicative of ἄρχω, which means "they began". The article οἱ is the nominative plural of ὁ, meaning "the". The participle συνανακείμενοι is the nominative plural masculine present middle participle of συνανάκειμαι, meaning "those reclining together" or "those dining together". The verb λέγειν is the present active infinitive of λέγω, meaning "to say". The phrase thus means "And those reclining together began to say".

- **ἐν ἑαυτοῖς**: The preposition ἐν means "in". The reflexive pronoun ἑαυτοῖς is the dative plural of ἑαυτός, meaning "themselves". The phrase thus means "among themselves".

- **Τίς οὗτός ἐστιν**: The interrogative pronoun Τίς means "who". The demonstrative pronoun οὗτός means "this". The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is". The phrase thus means "Who is this?".

- **ὃς καὶ ἁμαρτίας ἀφίησιν**: The relative pronoun ὃς means "who" or "that". The conjunction καὶ means "also". The noun ἁμαρτίας is the genitive singular of ἁμαρτία, meaning "sin". The verb ἀφίησιν is the third person singular present indicative of ἀφίημι, meaning "he forgives". The phrase thus means "who also forgives sins".

The literal translation of the entire verse is: "And those reclining together began to say among themselves, 'Who is this who also forgives sins?'"