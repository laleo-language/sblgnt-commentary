---
version: 1
---
- **ὅστις ἦν διὰ στάσιν τινὰ γενομένην ἐν τῇ πόλει**: The word ὅστις is the
  nominative singular masculine form of ὅστις, which means "who" or "which."
  The verb ἦν is the third person singular imperfect indicative of εἰμί,
  meaning "he was." The preposition διὰ means "through" or "because of." The
  noun στάσιν is the accusative singular of στάσις, which means "strife" or
  "dispute." The adjective τινὰ is the accusative singular of τις, which means
  "certain" or "some." The participle γενομένην is the accusative singular
  feminine form of γίνομαι, meaning "having happened" or "having occurred."
  The preposition ἐν means "in" or "among." The noun τῇ πόλει is the dative
  singular of πόλις, which means "city." The phrase thus means "who was
  because of a certain dispute that happened in the city."

- **καὶ φόνον βληθεὶς ἐν τῇ φυλακῇ**: The conjunction καὶ means "and." The noun
  φόνον is the accusative singular of φόνος, which means "murder" or "killing."
  The participle βληθεὶς is the nominative singular masculine form of βάλλω,
  meaning "having been thrown" or "having been cast." The preposition ἐν means
  "in" or "within." The noun τῇ φυλακῇ is the dative singular of φυλακή, which
  means "prison." The phrase thus means "and having been thrown into the
  prison."

- The entire verse can be translated as: "He was because of a certain dispute
  that happened in the city, and having been thrown into the prison."