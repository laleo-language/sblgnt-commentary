---
version: 1
---
- **Τῇ δὲ Ἐλισάβετ**: The word Τῇ is the dative singular form of the definite article ὁ, meaning "the." The name Ἐλισάβετ is declined in the dative case, which is commonly used to indicate the indirect object of a verb. The phrase thus means "To Elizabeth."

- **ἐπλήσθη ὁ χρόνος**: The verb ἐπλήσθη is the third person singular aorist passive indicative of πίμπλημι, which means "to be filled." The noun χρόνος is the nominative singular form of χρόνος, meaning "time" or "period." The phrase thus means "The time was fulfilled."

- **τοῦ τεκεῖν αὐτήν**: The word τοῦ is the genitive singular form of the definite article ὁ, meaning "of the." The verb τεκεῖν is the present active infinitive of τίκτω, which means "to give birth." The pronoun αὐτήν is the accusative singular form of αὐτός, meaning "her." The phrase thus means "of giving birth to her."

- **καὶ ἐγέννησεν υἱόν**: The conjunction καὶ means "and." The verb ἐγέννησεν is the third person singular aorist active indicative of γεννάω, meaning "to give birth." The noun υἱόν is the accusative singular form of υἱός, meaning "son." The phrase thus means "And she gave birth to a son."

The literal translation of the entire verse is: "To Elizabeth, the time was fulfilled of giving birth to her, and she gave birth to a son."