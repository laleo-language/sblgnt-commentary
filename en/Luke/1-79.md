---
version: 1
---
- **ἐπιφᾶναι τοῖς ἐν σκότει καὶ σκιᾷ θανάτου καθημένοις**: The word ἐπιφᾶναι is the aorist infinitive of ἐπιφαίνω, which means "to appear." The dative plural τοῖς ἐν σκότει καὶ σκιᾷ θανάτου καθημένοις consists of the preposition ἐν followed by the dative plural of ἐν σκότει καὶ σκιᾷ θανάτου καθημένοις, which means "those who sit in darkness and in the shadow of death." The phrase thus means "to appear to those who sit in darkness and in the shadow of death."

- **τοῦ κατευθῦναι τοὺς πόδας ἡμῶν εἰς ὁδὸν εἰρήνης**: The genitive τοῦ κατευθῦναι is the articular infinitive of κατευθύνω, meaning "to make straight" or "to direct." The accusative plural τοὺς πόδας is the accusative plural of ὁ πούς, which means "the foot." The genitive ἡμῶν is the genitive plural of ἐγώ, meaning "our." The accusative singular εἰς ὁδὸν εἰρήνης consists of the preposition εἰς followed by the accusative singular of ὁδός, which means "the way," and the genitive singular of εἰρήνη, which means "peace." The phrase thus means "to make straight our feet into the way of peace."

The literal translation of Luke 1:79 is: "to appear to those who sit in darkness and in the shadow of death, to make straight our feet into the way of peace."