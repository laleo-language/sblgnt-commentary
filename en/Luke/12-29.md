---
version: 1
---
- **καὶ ὑμεῖς**: The word καὶ is a conjunction meaning "and." The pronoun ὑμεῖς is the nominative plural form of σύ, which means "you." The phrase thus means "and you."

- **μὴ ζητεῖτε**: The word μὴ is a negative particle meaning "not." The verb ζητεῖτε is the second person plural present imperative of ζητέω, meaning "you seek" or "you look for." The phrase thus means "do not seek."

- **τί φάγητε**: The word τί is an interrogative pronoun meaning "what." The verb φάγητε is the second person plural aorist subjunctive of ἐσθίω, meaning "you eat." The phrase thus means "what you should eat."

- **καὶ τί πίητε**: The word καὶ is a conjunction meaning "and." The word τί is an interrogative pronoun meaning "what." The verb πίητε is the second person plural aorist subjunctive of πίνω, meaning "you drink." The phrase thus means "and what you should drink."

- **καὶ μὴ μετεωρίζεσθε**: The word καὶ is a conjunction meaning "and." The negative particle μὴ is followed by the verb μετεωρίζεσθε, which is the second person plural present imperative middle/passive of μετεωρίζομαι, meaning "you are anxious" or "you worry." The phrase thus means "and do not be anxious."

In this verse, Jesus is instructing his disciples not to be anxious about what they will eat or drink. The literal translation of the verse is: "And you, do not seek what you should eat and what you should drink, and do not be anxious."