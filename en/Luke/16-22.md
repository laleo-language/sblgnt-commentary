---
version: 1
---
- **ἐγένετο δὲ ἀποθανεῖν τὸν πτωχὸν**: The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it came to pass." The infinitive ἀποθανεῖν is the aorist infinitive of ἀποθνῄσκω, "to die." The accusative article τὸν marks πτωχὸν as the direct object of the infinitive, and it means "the poor man." The phrase thus means "it happened for the poor man to die."

- **καὶ ἀπενεχθῆναι αὐτὸν ὑπὸ τῶν ἀγγέλων εἰς τὸν κόλπον Ἀβραάμ**: The conjunction καὶ means "and." The verb ἀπενεχθῆναι is the aorist passive infinitive of ἀποφέρω, meaning "to carry away" or "to bring." The accusative pronoun αὐτὸν refers back to the πτωχὸν mentioned earlier, and it means "him." The preposition ὑπὸ means "by" or "under," and it is followed by the genitive article τῶν and the noun ἀγγέλων, which means "angels." The preposition εἰς means "into" or "to," and it is followed by the accusative article τὸν and the noun κόλπον, meaning "bosom" or "chest." The noun Ἀβραάμ is in the genitive case and refers to Abraham. The phrase thus means "and he was carried away by the angels into Abraham's bosom."

- **ἀπέθανεν δὲ καὶ ὁ πλούσιος καὶ ἐτάφη**: The verb ἀπέθανεν is the third person singular aorist indicative of ἀποθνῄσκω, "to die." The conjunction δὲ means "and" or "but." The article ὁ marks πλούσιος as the subject of the sentence and it means "the rich man." The verb ἐτάφη is the third person singular aorist passive indicative of θάπτω, meaning "he was buried." The phrase thus means "and the rich man also died and was buried."

The literal translation of Luke 16:22 is: "And it happened for the poor man to die and he was carried away by the angels into Abraham's bosom; and the rich man also died and was buried."