---
version: 1
---
- **Καὶ παρατηρήσαντες**: The word παρατηρήσαντες is the nominative masculine plural participle of παρατηρέω, which means "to watch" or "to observe." The participle form indicates that the action of watching or observing is happening at the same time as the main verb. The phrase thus means "and having watched" or "and observing."

- **ἀπέστειλαν**: The word ἀπέστειλαν is the third person plural aorist indicative of ἀποστέλλω, which means "to send." The verb form indicates that the action of sending has been completed in the past. The phrase thus means "they sent."

- **ἐγκαθέτους**: The word ἐγκαθέτους is the accusative masculine plural form of ἐγκαθέτης, which means "spy" or "informant." The phrase thus means "spies" or "informants."

- **ὑποκρινομένους ἑαυτοὺς δικαίους εἶναι**: The word ὑποκρινομένους is the accusative masculine plural present participle of ὑποκρίνομαι, which means "to pretend" or "to act." The participle form indicates that the action of pretending or acting is happening at the same time as the main verb. The phrase ἑαυτοὺς δικαίους εἶναι means "to be righteous." The phrase thus means "pretending to be righteous" or "acting as if they were righteous."

- **ἵνα ἐπιλάβωνται αὐτοῦ λόγου**: The word ἵνα is a subordinating conjunction that introduces a purpose clause. The clause that follows ἵνα is ἐπιλάβωνται αὐτοῦ λόγου. The verb ἐπιλάβωνται is the third person plural aorist subjunctive of ἐπιλαμβάνομαι, which means "to seize" or "to take hold of." The phrase αὐτοῦ λόγου means "his word" or "what he says." The phrase thus means "in order to catch him in his words" or "so that they could find something to accuse him of."

- **ὥστε παραδοῦναι αὐτὸν τῇ ἀρχῇ καὶ τῇ ἐξουσίᾳ τοῦ ἡγεμόνος**: The word ὥστε is a conjunction that introduces a result clause. The clause that follows ὥστε is παραδοῦναι αὐτὸν τῇ ἀρχῇ καὶ τῇ ἐξουσίᾳ τοῦ ἡγεμόνος. The verb παραδοῦναι is the aorist infinitive of παραδίδωμι, which means "to hand over" or "to deliver." The phrase αὐτὸν τῇ ἀρχῇ καὶ τῇ ἐξουσίᾳ τοῦ ἡγεμόνος means "him to the authority and power of the governor." The phrase thus means "so that they could hand him over to the authority and power of the governor."

Literal translation: "And having watched, they sent spies who pretended to be righteous, in order to catch him in his words, so that they could find something to accuse him of, resulting in handing him over to the authority and power of the governor."