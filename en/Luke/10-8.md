---
version: 1
---
- **καὶ εἰς ἣν ἂν πόλιν εἰσέρχησθε**: The conjunction καὶ means "and." The preposition εἰς means "into" or "to." The relative pronoun ἣν is the accusative singular of ὅς, meaning "which" or "whom." The particle ἂν indicates potentiality or uncertainty. The noun πόλιν is the accusative singular of πόλις, which means "city." The verb εἰσέρχησθε is the second person plural present middle indicative of εἰσέρχομαι, meaning "you enter." The phrase means "and into whichever city you enter."

- **καὶ δέχωνται ὑμᾶς**: The conjunction καὶ means "and." The verb δέχωνται is the third person plural present middle indicative of δέχομαι, meaning "they receive." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase means "and they receive you."

- **ἐσθίετε τὰ παρατιθέμενα ὑμῖν**: The verb ἐσθίετε is the second person plural present indicative of ἐσθίω, meaning "you eat." The article τὰ is the accusative plural of ὁ, meaning "the." The verb παρατιθέμενα is the accusative plural middle participle of παρατίθημι, meaning "being set before" or "presented." The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The phrase means "you eat the things that are set before you."

The sentence can be translated as: "And whichever city you enter, and they receive you, eat the things that are set before you."

This verse is not syntactically ambiguous.