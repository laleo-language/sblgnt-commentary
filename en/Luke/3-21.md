---
version: 1
---
- **Ἐγένετο δὲ**: The word Ἐγένετο is the third person singular aorist indicative of γίνομαι, which means "it happened" or "it occurred." The word δὲ is a conjunction that typically means "but" or "and," but here it is used to transition to a new event in the narrative. The phrase thus means "And it happened."

- **ἐν τῷ βαπτισθῆναι ἅπαντα τὸν λαὸν**: The word ἐν is a preposition that means "in" or "at." The phrase τῷ βαπτισθῆναι is the aorist infinitive of the verb βαπτίζω, which means "to baptize." The word ἅπαντα is the accusative singular of ἅπας, which means "all" or "every." The phrase τὸν λαὸν is the accusative singular of λαός, which means "the people." The phrase thus means "when all the people were baptized."

- **καὶ Ἰησοῦ βαπτισθέντος καὶ προσευχομένου**: The word καὶ is a conjunction that means "and." The word Ἰησοῦ is the genitive singular of Ἰησοῦς, which means "Jesus." The word βαπτισθέντος is the genitive singular masculine of the aorist passive participle of βαπτίζω, which means "having been baptized." The word προσευχομένου is the genitive singular masculine of the present middle participle of προσεύχομαι, which means "praying." The phrase thus means "and Jesus, having been baptized and praying."

- **ἀνεῳχθῆναι τὸν οὐρανόν**: The word ἀνεῳχθῆναι is the aorist passive infinitive of ἀνοίγω, which means "to open." The word τὸν οὐρανόν is the accusative singular of οὐρανός, which means "heaven" or "the sky." The phrase thus means "for the heaven to be opened."

The literal translation of Luke 3:21 is "And it happened, when all the people were baptized, and Jesus, having been baptized and praying, the heaven was opened."