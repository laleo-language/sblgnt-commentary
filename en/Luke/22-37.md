---
version: 1
---
- **λέγω γὰρ ὑμῖν**: The verb λέγω is the first person singular present indicative of λέγω, meaning "I say" or "I tell." The word γὰρ is a conjunction meaning "for" or "because." The pronoun ὑμῖν is the dative plural of σύ, meaning "you." The phrase means "For I tell you."

- **ὅτι τοῦτο τὸ γεγραμμένον**: The conjunction ὅτι introduces a subordinate clause and is often translated as "that." The pronoun τοῦτο means "this." The article τὸ is the neuter singular nominative of ὁ, meaning "the." The participle γεγραμμένον is the neuter singular perfect passive participle of γράφω, meaning "written." The phrase means "that which is written."

- **δεῖ τελεσθῆναι**: The verb δεῖ is a third person singular present indicative of δεῖ, meaning "it is necessary." The infinitive τελεσθῆναι is the aorist passive infinitive of τελέω, meaning "to be fulfilled" or "to be accomplished." The phrase means "it is necessary to be fulfilled."

- **ἐν ἐμοί**: The preposition ἐν means "in" or "by." The pronoun ἐμοί is the first person singular dative of ἐγώ, meaning "me" or "in me." The phrase means "in me."

- **τό**: The article τό is the neuter singular accusative of ὁ, meaning "the."

- **Καὶ μετὰ ἀνόμων ἐλογίσθη**: The conjunction Καὶ means "and." The preposition μετὰ means "with." The adjective ἀνόμων is the genitive plural of ἄνομος, meaning "lawless" or "criminal." The verb ἐλογίσθη is the third person singular aorist passive indicative of λογίζομαι, meaning "he was counted" or "he was reckoned." The phrase means "And he was counted with lawless ones."

- **καὶ γὰρ τὸ περὶ ἐμοῦ τέλος ἔχει**: The conjunction καὶ means "and." The adverb γὰρ means "for" or "because." The article τὸ is the neuter singular nominative of ὁ, meaning "the." The preposition περὶ means "about" or "concerning." The pronoun ἐμοῦ is the first person singular genitive of ἐγώ, meaning "me" or "my." The noun τέλος is the neuter singular nominative of τέλος, meaning "end" or "goal." The verb ἔχει is the third person singular present indicative of ἔχω, meaning "he has" or "it has." The phrase means "For the end concerning me has."

The entire verse can be literally translated as: "For I tell you that which is written must be fulfilled in me, even he was counted with lawless ones, for the end concerning me has."