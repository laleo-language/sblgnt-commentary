---
version: 1
---
- **εἶπεν δὲ αὐτῷ**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "he said to him."

- **Ὀρθῶς ἀπεκρίθης**: The adverb Ὀρθῶς means "rightly" or "correctly." The verb ἀπεκρίθης is the second person singular aorist indicative of ἀποκρίνομαι, meaning "you answered." The phrase thus means "you answered rightly."

- **τοῦτο ποίει**: The pronoun τοῦτο is the accusative singular of οὗτος, meaning "this." The verb ποίει is the second person singular present imperative of ποιέω, meaning "do" or "make." The phrase thus means "do this."

- **καὶ ζήσῃ**: The conjunction καὶ means "and." The verb ζήσῃ is the second person singular future indicative of ζάω, meaning "you will live." The phrase thus means "and you will live."

Literal translation: He said to him, "You answered rightly. Do this and you will live."

In this verse, Jesus is responding to a lawyer who asked him how to inherit eternal life. Jesus commends the lawyer for his answer and instructs him to do what is right in order to have eternal life.