---
version: 1
---
- **τινὲς δὲ τῶν Φαρισαίων**: The word τινὲς is the nominative plural of τις, meaning "some." The word δὲ is a conjunction meaning "but" or "and." The genitive plural τῶν Φαρισαίων means "of the Pharisees." The phrase thus means "some of the Pharisees."

- **εἶπαν**: The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said."

- **Τί ποιεῖτε**: The word Τί is the accusative singular of τίς, meaning "what." The verb ποιεῖτε is the second person plural present indicative of ποιέω, meaning "you do." The phrase thus means "What are you doing?"

- **ὃ οὐκ ἔξεστιν**: The relative pronoun ὃ is the accusative singular of ὅς, meaning "which" or "that." The verb ἔξεστιν is the third person singular present indicative of ἔξεστιν, meaning "it is allowed" or "it is permissible." The phrase thus means "which is not allowed."

- **τοῖς σάββασιν**: The word τοῖς is the dative plural of ὁ, meaning "to" or "for." The noun σάββασιν is the dative plural of σάββατον, meaning "Sabbath." The phrase thus means "on the Sabbaths."

The literal translation of the verse is: "And some of the Pharisees said, 'What are you doing which is not allowed on the Sabbaths?'"