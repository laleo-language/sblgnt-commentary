---
version: 1
---
- **καὶ αὐτὸς παρήγγειλεν αὐτῷ**: The word αὐτὸς is the nominative singular masculine pronoun, meaning "he himself." The verb παρήγγειλεν is the third person singular aorist indicative of παραγγέλλω, meaning "he commanded." The pronoun αὐτῷ is the dative singular masculine pronoun, meaning "to him." The phrase thus means "he himself commanded him."

- **μηδενὶ εἰπεῖν**: The word μηδενὶ is the dative singular masculine pronoun, meaning "to no one." The infinitive εἰπεῖν is the aorist infinitive of λέγω, meaning "to say." The phrase thus means "to say to no one."

- **ἀλλὰ ἀπελθὼν δεῖξον σεαυτὸν τῷ ἱερεῖ**: The word ἀλλὰ is a conjunction meaning "but." The participle ἀπελθὼν is the aorist active participle of ἀπέρχομαι, meaning "having gone." The verb δεῖξον is the second person singular aorist imperative of δείκνυμι, meaning "show." The pronoun σεαυτὸν is the accusative singular reflexive pronoun, meaning "yourself." The dative article τῷ is the dative singular masculine article, meaning "to the." The noun ἱερεῖ is the dative singular masculine noun, meaning "priest." The phrase thus means "but having gone, show yourself to the priest."

- **καὶ προσένεγκε περὶ τοῦ καθαρισμοῦ σου**: The conjunction καὶ means "and." The verb προσένεγκε is the second person singular aorist imperative of προσφέρω, meaning "bring" or "offer." The preposition περὶ means "concerning" or "about." The genitive noun τοῦ καθαρισμοῦ is the genitive singular masculine noun, meaning "of the cleansing." The pronoun σου is the genitive singular second person pronoun, meaning "your." The phrase thus means "and bring about your cleansing."

- **καθὼς προσέταξεν Μωϋσῆς εἰς μαρτύριον αὐτοῖς**: The conjunction καθὼς means "just as" or "according to what." The verb προσέταξεν is the third person singular aorist indicative of προστάσσω, meaning "he commanded." The noun Μωϋσῆς is the nominative singular masculine noun, meaning "Moses." The preposition εἰς means "for" or "to." The accusative noun μαρτύριον is the accusative singular neuter noun, meaning "testimony" or "witness." The pronoun αὐτοῖς is the dative plural masculine pronoun, meaning "to them." The phrase thus means "just as Moses commanded for a testimony to them."

The literal translation of the entire verse would be: "And he himself commanded him to say to no one, but having gone, show yourself to the priest and bring about your cleansing, just as Moses commanded for a testimony to them."