---
version: 1
---
- **οὐ γὰρ θελήματι ἀνθρώπου ἠνέχθη**: The word οὐ is a negation, meaning "not." The noun θελήματι is the dative singular of θέλημα, which means "will" or "desire." The noun ἀνθρώπου is the genitive singular of ἄνθρωπος, meaning "man" or "human." The verb ἠνέχθη is the third person singular aorist passive indicative of φέρω, which means "was brought" or "was carried." The phrase thus means "For prophecy was not brought by human will."

- **προφητεία ποτέ**: The noun προφητεία is the nominative singular of προφητεία, which means "prophecy." The adverb ποτέ means "at any time" or "ever." The phrase thus means "prophecy at any time."

- **ἀλλὰ ὑπὸ πνεύματος ἁγίου φερόμενοι**: The conjunction ἀλλὰ means "but." The preposition ὑπὸ means "by" or "under." The noun πνεύματος is the genitive singular of πνεῦμα, meaning "spirit." The adjective ἁγίου is the genitive singular of ἅγιος, which means "holy." The participle φερόμενοι is the nominative plural masculine of φέρω, meaning "being carried" or "being borne." The phrase thus means "but being carried by the Holy Spirit."

- **ἐλάλησαν ἀπὸ θεοῦ ἄνθρωποι**: The verb ἐλάλησαν is the third person plural aorist indicative of λαλέω, which means "they spoke" or "they uttered." The preposition ἀπὸ means "from." The noun θεοῦ is the genitive singular of θεός, meaning "God." The noun ἄνθρωποι is the nominative plural of ἄνθρωπος, meaning "men" or "human beings." The phrase thus means "men spoke from God."

The entire verse can be translated as: "For prophecy was not brought by human will, but men spoke from God, being carried by the Holy Spirit."

This verse highlights the divine origin of prophecy, emphasizing that it is not the result of human desire or effort, but rather a message spoken by men who were carried or inspired by the Holy Spirit.