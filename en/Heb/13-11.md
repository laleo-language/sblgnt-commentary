---
version: 1
---
- **ὧν γὰρ εἰσφέρεται ζῴων τὸ αἷμα**: The word ὧν is the genitive plural of ὅς, ἥ, ὅ, which means "of which" or "whose." The verb εἰσφέρεται is the third person singular present indicative middle/passive of εἰσφέρω, meaning "it is being brought in" or "it is being offered." The noun ζῴων is the genitive plural of ζῷον, which means "animal." The phrase τὸ αἷμα is the accusative singular of αἷμα, meaning "the blood." So the phrase means "of which the blood of animals is being brought in."

- **περὶ ἁμαρτίας**: The preposition περὶ means "concerning" or "about." The noun ἁμαρτίας is the genitive singular of ἁμαρτία, which means "sin." The phrase means "concerning sin."

- **εἰς τὰ ἅγια**: The preposition εἰς means "into" or "to." The article τὰ is the accusative neuter plural of ὁ, ἡ, τό, meaning "the." The adjective ἅγια is the accusative neuter plural of ἅγιος, which means "holy" or "sacred." The phrase means "into the holy (place)."

- **διὰ τοῦ ἀρχιερέως**: The preposition διὰ means "through" or "by means of." The article τοῦ is the genitive masculine singular of ὁ, ἡ, τό, meaning "the." The noun ἀρχιερέως is the genitive masculine singular of ἀρχιερεύς, which means "high priest." The phrase means "through the high priest."

- **τούτων τὰ σώματα κατακαίεται**: The pronoun τούτων is the genitive plural of οὗτος, αὕτη, τοῦτο, which means "these." The noun τὰ σώματα is the accusative neuter plural of σῶμα, meaning "the bodies." The verb κατακαίεται is the third person singular present indicative middle/passive of κατακαίω, which means "they are being burned" or "they are being consumed." The phrase means "the bodies of these (animals) are being burned."

- **ἔξω τῆς παρεμβολῆς**: The adverb ἔξω means "outside" or "out." The article τῆς is the genitive feminine singular of ὁ, ἡ, τό, meaning "the." The noun παρεμβολῆς is the genitive feminine singular of παρεμβολή, which means "encampment" or "camp." The phrase means "outside the camp."

The literal translation of the entire verse is: "For of which the blood of animals is being brought in concerning sin into the holy (place) through the high priest, the bodies of these (animals) are being burned outside the camp."