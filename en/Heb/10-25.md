---
version: 1
---
- **μὴ ἐγκαταλείποντες τὴν ἐπισυναγωγὴν ἑαυτῶν**: The word μὴ is a particle that indicates negation. The participle ἐγκαταλείποντες is the nominative plural present active participle of ἐγκαταλείπω, which means "to abandon" or "to forsake." The article τὴν is the accusative singular of the definite article ὁ, meaning "the." The noun ἐπισυναγωγὴν is the accusative singular of ἐπισυναγωγή, which means "gathering" or "assembly." The possessive pronoun ἑαυτῶν is the genitive plural of ἑαυτοῦ, meaning "their own." The phrase thus means "not abandoning their own gathering."

- **καθὼς ἔθος τισίν**: The word καθὼς is a conjunction that means "just as" or "according to." The noun ἔθος is the nominative singular of ἔθος, which means "custom" or "habit." The pronoun τισίν is the dative plural of τις, which means "some" or "certain." The phrase thus means "according to the custom of some."

- **ἀλλὰ παρακαλοῦντες**: The word ἀλλὰ is a conjunction that means "but" or "instead." The participle παρακαλοῦντες is the nominative plural present active participle of παρακαλέω, which means "to exhort" or "to encourage." The phrase thus means "but exhorting."

- **καὶ τοσούτῳ μᾶλλον ὅσῳ βλέπετε ἐγγίζουσαν τὴν ἡμέραν**: The conjunction καὶ means "and." The adverb τοσούτῳ means "so much" or "to such an extent." The adverb μᾶλλον means "more" or "rather." The relative pronoun ὅσῳ is the dative singular of ὅσος, which means "as much as" or "as many as." The verb βλέπετε is the second person plural present indicative of βλέπω, which means "you see" or "you perceive." The present active participle ἐγγίζουσαν is the accusative singular feminine present active participle of ἐγγίζω, which means "approaching" or "drawing near." The article τὴν is the accusative singular of the definite article ὁ, meaning "the." The noun ἡμέραν is the accusative singular of ἡμέρα, which means "day." The phrase thus means "and so much more as you see the day approaching."

The literal translation of Hebrews 10:25 is: "Not abandoning their own gathering, just as the custom of some, but exhorting and so much more as you see the day approaching."