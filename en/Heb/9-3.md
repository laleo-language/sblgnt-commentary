---
version: 1
---
- **μετὰ δὲ τὸ δεύτερον καταπέτασμα**: The preposition μετὰ means "after" or "beyond." The article τὸ is the nominative singular neuter of the definite article ὁ, meaning "the." The adjective δεύτερον is the accusative singular neuter of the ordinal number δεύτερος, meaning "second." The noun καταπέτασμα is the accusative singular neuter of καταπέτασμα, which means "veil" or "curtain." So this phrase means "after the second veil."

- **σκηνὴ ἡ λεγομένη Ἅγια Ἁγίων**: The noun σκηνὴ is the nominative singular feminine of σκηνή, meaning "tabernacle" or "tent." The article ἡ is the nominative singular feminine of the definite article ὁ. The participle λεγομένη is the nominative singular feminine of the present passive participle λέγω, meaning "called" or "named." The adjective Ἅγια is the nominative plural neuter of ἅγιος, meaning "holy" or "sacred." The noun Ἁγίων is the genitive plural neuter of ἅγιος. So this phrase means "the tent that is called Holy of Holies."

- The literal translation of the verse is "And after the second veil, the tent that is called Holy of Holies,".