---
version: 1
---
- **εἰ γὰρ αὐτοὺς Ἰησοῦς κατέπαυσεν**: The word εἰ is a conjunction that means "if." The word γὰρ is a conjunction that means "for" or "because." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The name Ἰησοῦς is the accusative singular of Ἰησοῦς, which is the Greek form of "Jesus." The verb κατέπαυσεν is the third person singular aorist indicative active of καταπαύω, meaning "he rested" or "he caused to rest." The phrase thus means "For if Jesus had caused them to rest."

- **οὐκ ἂν περὶ ἄλλης ἐλάλει μετὰ ταῦτα ἡμέρας**: The word οὐκ is an adverb that means "not." The verb ἐλάλει is the third person singular imperfect indicative active of λαλέω, which means "he was speaking" or "he was talking." The pronoun περὶ is a preposition that means "about" or "concerning." The adjective ἄλλης is the genitive singular of ἄλλος, meaning "another." The noun ἡμέρας is the accusative plural of ἡμέρα, meaning "day." The phrase thus means "he would not have spoken about another day after these."

The phrase "εἰ γὰρ αὐτοὺς Ἰησοῦς κατέπαυσεν, οὐκ ἂν περὶ ἄλλης ἐλάλει μετὰ ταῦτα ἡμέρας" can be translated as "For if Jesus had caused them to rest, he would not have spoken about another day after these."

This verse is discussing the rest that Jesus provides and how it relates to the concept of a future day. It is explaining that if Jesus had already brought true rest, there would be no need for further discussion about another day of rest.