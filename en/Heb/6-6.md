---
version: 1
---
- **καὶ παραπεσόντας**: The conjunction καὶ means "and." The participle παραπεσόντας is the accusative plural masculine participle of παραπίπτω, meaning "having fallen away" or "having apostatized." The phrase thus means "and having fallen away."

- **πάλιν ἀνακαινίζειν εἰς μετάνοιαν**: The adverb πάλιν means "again." The infinitive ἀνακαινίζειν is the present infinitive of ἀνακαινίζω, meaning "to renew." The preposition εἰς means "for" or "unto." The noun μετάνοιαν is the accusative singular of μετάνοια, which means "repentance." The phrase thus means "to renew unto repentance."

- **ἀνασταυροῦντας ἑαυτοῖς τὸν υἱὸν τοῦ θεοῦ**: The participle ἀνασταυροῦντας is the accusative plural masculine participle of ἀνασταυρόω, meaning "crucifying." The reflexive pronoun ἑαυτοῖς means "themselves." The article τὸν is the accusative singular masculine article, and it modifies the noun υἱὸν, which means "son." The genitive τοῦ θεοῦ means "of God." The phrase thus means "crucifying themselves the Son of God."

- **καὶ παραδειγματίζοντας**: The conjunction καὶ means "and." The participle παραδειγματίζοντας is the accusative plural masculine participle of παραδειγματίζω, meaning "making an example of" or "exposing." The phrase thus means "and making an example of."

Literal translation: "And having fallen away, to renew unto repentance, crucifying themselves the Son of God and making an example of."