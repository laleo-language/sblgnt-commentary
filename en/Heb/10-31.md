---
version: 1
---
- **φοβερὸν τὸ ἐμπεσεῖν εἰς χεῖρας θεοῦ ζῶντος**: The adjective φοβερὸν means "fearful" or "terrible." The article τὸ is the neuter singular definite article, meaning "the." The verb ἐμπεσεῖν is the aorist infinitive of ἐμπίπτω, which means "to fall into" or "to come upon." The preposition εἰς means "into." The noun χεῖρας is the accusative plural of χείρ, which means "hand." The genitive θεοῦ is the genitive singular of θεός, meaning "God." The participle ζῶντος is the genitive singular masculine present participle of ζάω, meaning "living." The phrase thus means "the fearful thing of falling into the hands of the living God."

The syntax of this sentence is not ambiguous.

**Literal translation:**
"The fearful thing of falling into the hands of the living God."