---
version: 1
---
- **ἐν ᾧ γὰρ πέπονθεν**: The preposition ἐν means "in" and here it is followed by the relative pronoun ᾧ, which means "in which" or "in whom." The verb πέπονθεν is the third person singular perfect indicative active of πάσχω, meaning "he has suffered" or "he has experienced." So this phrase can be translated as "in which he has suffered" or "in whom he has suffered."

- **αὐτὸς πειρασθείς**: The word αὐτὸς is the nominative singular masculine pronoun meaning "he himself." The participle πειρασθείς is the nominative singular masculine aorist passive participle of πειράζω, meaning "having been tempted." So this phrase can be translated as "he himself, having been tempted."

- **δύναται τοῖς πειραζομένοις βοηθῆσαι**: The verb δύναται is the third person singular present indicative middle/passive of δύναμαι, meaning "he is able" or "he can." The noun πειραζομένοις is the dative plural masculine participle of πειράζω, meaning "those being tempted" or "those who are being tested." The verb βοηθῆσαι is the aorist infinitive of βοηθέω, meaning "to help" or "to assist." So this phrase can be translated as "he is able to help those who are being tempted."

The literal translation of the entire verse is: "For in which he has suffered himself, he is able to help those who are being tempted."