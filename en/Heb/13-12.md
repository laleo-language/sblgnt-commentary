---
version: 1
---
- **διὸ**: This is a conjunction that means "therefore" or "for this reason."
- **καὶ**: This is a conjunction that means "and."
- **Ἰησοῦς**: This is the nominative singular form of Ἰησοῦς, which means "Jesus."
- **ἵνα**: This is a conjunction that introduces a purpose clause and can be translated as "so that" or "in order to."
- **ἁγιάσῃ**: This is the third person singular aorist subjunctive of ἁγιάζω, which means "to sanctify" or "to make holy."
- **διὰ**: This is a preposition that means "through" or "by means of."
- **τοῦ**: This is the genitive singular form of the article ὁ, which means "the."
- **ἰδίου**: This is the genitive singular form of ἴδιος, which means "one's own" or "personal."
- **αἵματος**: This is the genitive singular form of αἷμα, which means "blood."
- **τὸν**: This is the accusative singular form of the article ὁ.
- **λαόν**: This is the accusative singular form of λαός, which means "people" or "nation."
- **ἔξω**: This is an adverb that means "outside" or "out."
- **τῆς**: This is the genitive singular form of the article ὁ.
- **πύλης**: This is the genitive singular form of πύλη, which means "gate" or "door."
- **ἔπαθεν**: This is the third person singular aorist indicative of πάσχω, which means "to suffer" or "to experience."

Literal translation: "Therefore Jesus also, in order to sanctify the people through his own blood, suffered outside the gate."

The verse is not syntactically ambiguous.