---
version: 1
---
- **χωρὶς δὲ πάσης ἀντιλογίας**: The word χωρὶς is an adverb meaning "apart from" or "without." The word δὲ is a conjunction meaning "but" or "and." The phrase πάσης ἀντιλογίας consists of the genitive singular of πᾶς, meaning "every" or "all," and the genitive singular of ἀντιλογία, meaning "dispute" or "opposition." The phrase thus means "apart from all dispute."

- **τὸ ἔλαττον**: The article τὸ is the neuter singular nominative of ὁ, meaning "the." The word ἔλαττον is the neuter singular comparative of ἐλάσσων, meaning "less" or "smaller." The phrase thus means "the smaller" or "the lesser."

- **ὑπὸ τοῦ κρείττονος**: The preposition ὑπὸ means "by" or "under." The article τοῦ is the genitive singular masculine of ὁ. The word κρείττονος is the genitive singular masculine of κρείσσων, meaning "greater" or "superior." The phrase thus means "by the greater" or "under the superior."

- **εὐλογεῖται**: The verb εὐλογεῖται is the third person singular present passive indicative of εὐλογέω, meaning "he is blessed" or "he is praised." 

The phrase "χωρὶς δὲ πάσης ἀντιλογίας" modifies "τὸ ἔλαττον," indicating that the lesser is being mentioned without any dispute. "ὑπὸ τοῦ κρείττονος" describes how the lesser is being blessed or praised, as it is under the superior. 

Literal translation: "But without any dispute, the lesser is blessed by the greater."