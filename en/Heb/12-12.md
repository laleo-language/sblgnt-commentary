---
version: 1
---
- **Διὸ**: This is a conjunction meaning "therefore" or "for this reason."
- **τὰς παρειμένας χεῖρας**: The article τὰς is the accusative plural feminine article, and the noun χεῖρας is the accusative plural feminine form of χείρ, which means "hands." The adjective παρειμένας is the accusative plural feminine form of παρειμένος, which means "weak" or "feeble." The phrase thus means "the weak hands."
- **καὶ τὰ παραλελυμένα γόνατα**: The conjunction καὶ means "and." The article τὰ is the accusative plural neuter article, and the noun γόνατα is the accusative plural neuter form of γόνυ, which means "knees." The adjective παραλελυμένα is the accusative plural neuter form of παραλελυμένος, which means "weak" or "paralyzed." The phrase thus means "the weak knees."
- **ἀνορθώσατε**: This is the second person plural aorist imperative form of ἀνορθόω, which means "to straighten up" or "to make straight." The verb is addressing the readers and is translated as "straighten up."

Literal translation: "Therefore, straighten up the weak hands and the weak knees."