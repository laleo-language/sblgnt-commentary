---
version: 1
---
- **ὅταν δὲ πάλιν εἰσαγάγῃ τὸν πρωτότοκον εἰς τὴν οἰκουμένην**: The conjunction ὅταν introduces a temporal clause and means "when." The verb εἰσαγάγῃ is the third person singular aorist subjunctive of εἰσάγω, meaning "he brings in" or "he introduces." The article τὸν is the accusative singular masculine form, modifying the noun πρωτότοκον meaning "firstborn." The preposition εἰς means "into." The noun οἰκουμένην is the accusative singular of οἰκουμένη, which can mean "world" or "inhabited earth."

- **λέγει**: The verb λέγει is the third person singular present indicative form of λέγω, meaning "he says."

- **Καὶ προσκυνησάτωσαν αὐτῷ πάντες ἄγγελοι θεοῦ**: The conjunction Καὶ means "and." The verb προσκυνησάτωσαν is the third person plural aorist imperative form of προσκυνέω, meaning "let them worship." The pronoun αὐτῷ is the dative singular masculine form, meaning "to him." The adjective πάντες is the nominative plural masculine form of πᾶς, meaning "all." The noun ἄγγελοι is the nominative plural masculine form of ἄγγελος, meaning "angels." The genitive θεοῦ expresses possession and means "of God."

The phrase can be translated as: "When he brings the firstborn into the world, he says, 'And let all the angels of God worship him.'"

The literal translation of the verse would be: "But when again he brings the firstborn into the inhabited earth, he says, 'And let all the angels of God worship him.'"