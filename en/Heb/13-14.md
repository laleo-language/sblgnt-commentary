---
version: 1
---
- **οὐ γὰρ ἔχομεν ὧδε μένουσαν πόλιν**: The word οὐ is the negative particle, meaning "not." The verb ἔχομεν is the first person plural present indicative of ἔχω, meaning "we have" or "we possess." The word ὧδε means "here." The participle μένουσαν is the accusative singular feminine present participle of μένω, meaning "remaining" or "staying." The noun πόλιν is the accusative singular feminine of πόλις, which means "city." The phrase can be translated as "For we do not have here a remaining city."

- **ἀλλὰ τὴν μέλλουσαν ἐπιζητοῦμεν**: The word ἀλλὰ means "but." The article τὴν is the accusative singular feminine definite article, meaning "the." The participle μέλλουσαν is the accusative singular feminine present participle of μέλλω, meaning "about to be" or "future." The verb ἐπιζητοῦμεν is the first person plural present indicative of ἐπιζητέω, meaning "we seek" or "we desire." The phrase can be translated as "but we seek the future one."

- **οὐ γὰρ ἔχομεν ὧδε μένουσαν πόλιν, ἀλλὰ τὴν μέλλουσαν ἐπιζητοῦμεν**: Putting it all together, the literal translation of the verse is "For we do not have here a remaining city, but we seek the future one."

In this verse, the author of Hebrews contrasts the present reality of not having a permanent city with the future hope of a city to come. The syntax in this verse is straightforward and not ambiguous.