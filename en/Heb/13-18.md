---
version: 1
---
- **Προσεύχεσθε περὶ ἡμῶν**: The verb προσεύχεσθε is the present imperative middle form of προσεύχομαι, meaning "pray." The preposition περὶ means "for" or "about." The pronoun ἡμῶν is the genitive plural form of ἐγώ, meaning "us" or "we." The phrase thus means "Pray for us."

- **πειθόμεθα γὰρ ὅτι καλὴν συνείδησιν ἔχομεν**: The verb πειθόμεθα is the present indicative middle form of πείθομαι, meaning "we are persuaded" or "we trust." The conjunction γὰρ means "for" or "because." The pronoun ὅτι introduces a complement clause and means "that." The adjective καλὴν is the accusative singular feminine form of καλός, meaning "good" or "beautiful." The noun συνείδησιν is the accusative singular form of συνείδησις, meaning "conscience." The verb ἔχομεν is the present indicative active form of ἔχω, meaning "we have." The phrase can be translated as "For we trust that we have a good conscience."

- **ἐν πᾶσιν καλῶς θέλοντες ἀναστρέφεσθαι**: The preposition ἐν means "in" or "with." The adjective πᾶσιν is the dative plural form of πᾶς, meaning "all" or "every." The adverb καλῶς means "well" or "in a good way." The verb θέλοντες is the present participle active form of θέλω, meaning "desiring" or "wanting." The verb ἀναστρέφεσθαι is the present infinitive middle form of ἀναστρέφομαι, meaning "to conduct oneself" or "to live." The phrase can be translated as "desiring to live well in all things."

Literal translation: "Pray for us, for we trust that we have a good conscience, desiring to live well in all things."