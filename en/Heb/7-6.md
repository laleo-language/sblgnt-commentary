---
version: 1
---
- **ὁ δὲ μὴ γενεαλογούμενος ἐξ αὐτῶν**: The definite article ὁ indicates that this is a noun phrase. The word δὲ is a conjunction meaning "but" or "and." The verb γενεαλογούμενος is the present middle participle of γενεαλογέω, which means "to trace genealogy" or "to be recorded in a genealogy." The preposition ἐξ here means "from" or "out of." The pronoun αὐτῶν is the genitive plural form of αὐτός, meaning "of them" or "from them." So this phrase can be translated as "but the one not being traced in their genealogy."

- **δεδεκάτωκεν**: This is the third person singular aorist indicative of δεκατόω, which means "to tithe" or "to give a tenth." The subject of this verb is not explicitly stated, but it can be inferred to be the one mentioned in the previous phrase, "the one not being traced in their genealogy." So the phrase can be translated as "he has tithed."

- **Ἀβραάμ**: This is the accusative singular form of Ἀβραάμ, which is the name "Abraham." It is the direct object of the verb δεδεκάτωκεν. So this phrase can be translated as "Abraham."

- **καὶ τὸν ἔχοντα τὰς ἐπαγγελίας**: The conjunction καὶ means "and." The article τὸν indicates that this is a noun phrase. The verb ἔχοντα is the present active participle of ἔχω, which means "to have" or "to possess." The accusative plural form of ἐπαγγελίας is the direct object of the verb ἔχοντα, and it means "promises." So this phrase can be translated as "and the one having the promises."

- **εὐλόγηκεν**: This is the third person singular aorist indicative of εὐλογέω, which means "to bless" or "to praise." The subject of this verb is not explicitly stated, but it can be inferred to be the same as the subject of the previous verb, "he has tithed." So this phrase can be translated as "he has blessed."

Putting it all together, the literal translation of the verse is: "But the one not being traced in their genealogy has tithed to Abraham, and he has blessed the one having the promises."