---
version: 1
---
- **Εἶχε μὲν οὖν ἡ πρώτη δικαιώματα λατρείας**: The verb Εἶχε is the third person singular imperfect indicative of ἔχω, meaning "he/she/it had." The phrase μὲν οὖν is a combination of two particles that often occur together to indicate a transition or continuation in the discourse. The article ἡ is the nominative singular feminine form of ὁ, meaning "the." The adjective πρώτη is the nominative singular feminine form of πρῶτος, meaning "first." The noun δικαιώματα is the accusative plural of δικαίωμα, which means "ordinances" or "regulations." The noun λατρείας is the genitive singular of λατρεία, meaning "worship" or "service." The phrase thus means "The first ordinances of worship."

- **τό τε ἅγιον κοσμικόν**: The article τό is the accusative singular neuter form of ὁ. The conjunction τε is used to connect two nouns or noun phrases and can be translated as "and." The adjective ἅγιον is the accusative singular neuter form of ἅγιος, meaning "holy" or "sacred." The adjective κοσμικόν is the accusative singular neuter form of κοσμικός, meaning "worldly" or "earthly." The phrase thus means "and the holy earthly thing."

The literal translation of the entire verse is: "Now the first ordinances of worship had both the holy earthly thing."