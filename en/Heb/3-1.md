---
version: 1
---
- **Ὅθεν**: This is an adverb meaning "from where" or "therefore."

- **ἀδελφοὶ ἅγιοι**: The word ἀδελφοὶ is the nominative plural form of ἀδελφός, which means "brothers" or "brothers and sisters." The word ἅγιοι is the nominative plural form of ἅγιος, which means "holy." So together, this phrase means "holy brothers."

- **κλήσεως ἐπουρανίου μέτοχοι**: The word κλήσεως is the genitive singular form of κλῆσις, which means "calling" or "invitation." The word ἐπουρανίου is the genitive singular form of ἐπουράνιος, which means "heavenly" or "celestial." The word μέτοχοι is the nominative plural form of μέτοχος, which means "partakers" or "participants." So together, this phrase means "participants of the heavenly calling."

- **κατανοήσατε**: This is the second person plural aorist imperative form of κατανοέω, which means "understand" or "comprehend." So this verb is a command to the readers to understand or comprehend something.

- **τὸν ἀπόστολον καὶ ἀρχιερέα τῆς ὁμολογίας ἡμῶν Ἰησοῦν**: The word τὸν is the accusative singular form of the definite article ὁ, which means "the." The word ἀπόστολον is the accusative singular form of ἀπόστολος, which means "apostle." The word καὶ is a conjunction meaning "and." The word ἀρχιερέα is the accusative singular form of ἀρχιερεύς, which means "high priest." The word τῆς is the genitive singular form of the definite article ὁ. The word ὁμολογίας is the genitive singular form of ὁμολογία, which means "confession" or "profession." The word ἡμῶν is the genitive plural form of ἐγώ, which means "our." The word Ἰησοῦν is the accusative singular form of Ἰησοῦς, which means "Jesus." So this phrase means "the apostle and high priest of our confession, Jesus."

The literal translation of the entire verse is: "Therefore, holy brothers, participants of the heavenly calling, understand the apostle and high priest of our confession, Jesus."