---
version: 1
---
- **ὁλοκαυτώματα**: This is the nominative plural of ὁλοκαύτωμα, which means "burnt offerings." It refers to the sacrifices that were completely burned on the altar.
- **καὶ**: This is a conjunction that means "and."
- **περὶ**: This is a preposition that means "concerning" or "about."
- **ἁμαρτίας**: This is the genitive singular of ἁμαρτία, which means "sin."
- **οὐκ**: This is an adverb that means "not."
- **εὐδόκησας**: This is the second person singular aorist indicative of εὐδοκέω, which means "to be pleased" or "to take pleasure in."

The phrase "ὁλοκαυτώματα καὶ περὶ ἁμαρτίας" can be translated as "burnt offerings and concerning sin." The phrase "οὐκ εὐδόκησας" can be translated as "you were not pleased" or "you did not take pleasure in."

Literal translation: "You did not take pleasure in burnt offerings and concerning sin."

In this verse, the author is discussing the ineffectiveness of burnt offerings and sacrifices for sin. The phrase highlights that God was not pleased with these offerings and they did not fully address the problem of sin.