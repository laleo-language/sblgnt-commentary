---
version: 1
---
- **πρὸς τίνα δὲ τῶν ἀγγέλων**: The preposition πρὸς means "to" or "towards." The interrogative pronoun τίνα means "who" or "which one." The genitive article τῶν indicates possession or relationship. The noun ἀγγέλων is the genitive plural of ἄγγελος, which means "angel." The phrase can be translated as "to which one of the angels."

- **εἴρηκέν ποτε**: The verb εἴρηκέν is the third person singular perfect indicative active of λέγω, which means "he has said" or "he said." The adverb ποτε means "once" or "at some point." The phrase can be translated as "he has said once."

- **Κάθου ἐκ δεξιῶν μου**: The verb Κάθου is the second person singular present imperative middle/passive of κάθημαι, which means "sit." The preposition ἐκ means "from." The genitive noun δεξιῶν is the genitive plural of δεξιός, which means "right." The pronoun μου means "my." The phrase can be translated as "Sit at my right."

- **ἕως ἂν θῶ τοὺς ἐχθρούς σου ὑποπόδιον τῶν ποδῶν σου**: The conjunction ἕως means "until." The conjunction ἂν indicates a future contingency. The verb θῶ is the first person singular aorist subjunctive active of τίθημι, which means "I put" or "I place." The definite article τοὺς is the accusative plural of ὁ, which means "the." The noun ἐχθρούς is the accusative plural of ἐχθρός, which means "enemy." The pronoun σου means "your." The noun ὑποπόδιον is the accusative singular of ὑποπόδιον, which means "footstool." The definite article τῶν is the genitive plural of ὁ. The noun ποδῶν is the genitive plural of πούς, which means "foot." The phrase can be translated as "until I place your enemies as a footstool for your feet."

The literal translation of Hebrews 1:13 is: "But to which one of the angels has He ever said, 'Sit at My right hand, until I make Your enemies a footstool for Your feet?'"