---
version: 1
---
- **Ἰησοῦς Χριστὸς**: The word Ἰησοῦς is the nominative singular form of Ἰησοῦς, which is the name "Jesus." The word Χριστὸς is the nominative singular form of Χριστός, which means "Christ." The phrase means "Jesus Christ."

- **ἐχθὲς καὶ σήμερον**: The word ἐχθὲς is an adverb meaning "yesterday." The word σήμερον is an adverb meaning "today." The phrase means "yesterday and today."

- **ὁ αὐτός**: The word ὁ is the definite article in the nominative singular form, meaning "the." The word αὐτός is a pronoun in the nominative singular form, meaning "he himself." The phrase means "the same."

- **καὶ εἰς τοὺς αἰῶνας**: The word καὶ is a conjunction meaning "and." The word εἰς is a preposition meaning "to" or "for." The word τοὺς is the definite article in the accusative plural form, meaning "the." The word αἰῶνας is the accusative plural form of αἰών, meaning "ages" or "eternity." The phrase means "and for the ages" or "and forever."

The entire verse could be literally translated as "Jesus Christ yesterday and today the same, and for the ages."