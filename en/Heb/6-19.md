---
version: 1
---
- **ἣν ὡς ἄγκυραν ἔχομεν τῆς ψυχῆς**: The word ἣν is the accusative singular feminine of ὅς, ἥ, ὅ, which means "which" or "whom." The word ὡς is a conjunction meaning "as" or "like." The noun ἄγκυραν is the accusative singular of ἄγκυρα, which means "anchor." The verb ἔχομεν is the first person plural present indicative of ἔχω, meaning "we have" or "we hold." The genitive singular noun τῆς ψυχῆς means "of the soul." So the phrase means "which we have as an anchor of the soul."

- **ἀσφαλῆ τε καὶ βεβαίαν**: The adjective ἀσφαλῆ is the accusative singular neuter of ἀσφαλής, meaning "secure" or "firm." The adjective βεβαίαν is the accusative singular feminine of βεβαῖος, meaning "steadfast" or "sure." The conjunction τε is used to connect the two adjectives, and it can be translated as "and" or "both." So the phrase means "secure and steadfast."

- **καὶ εἰσερχομένην εἰς τὸ ἐσώτερον τοῦ καταπετάσματος**: The conjunction καὶ means "and." The present participle εἰσερχομένην is the accusative singular feminine of εἰσέρχομαι, meaning "entering" or "going into." The preposition εἰς means "into." The article τὸ is the accusative singular neuter of ὁ, ἡ, τό, and it functions as a definite article. The noun ἐσώτερον is the accusative singular neuter of ἔσωτερος, meaning "inner" or "inside." The genitive singular noun τοῦ καταπετάσματος means "of the curtain." So the phrase means "and entering into the inner (part) of the curtain."

- **ἣν ὡς ἄγκυραν ἔχομεν τῆς ψυχῆς, ἀσφαλῆ τε καὶ βεβαίαν καὶ εἰσερχομένην εἰς τὸ ἐσώτερον τοῦ καταπετάσματος**: The verse can be translated as "which we have as an anchor of the soul, secure and steadfast and entering into the inner part of the curtain."

The phrase in this verse describes the anchor of the soul that believers have. It is secure, steadfast, and it enters into the inner part of the curtain. This imagery suggests that the anchor provides stability and access to the presence of God.