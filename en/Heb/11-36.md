---
version: 1
---
- **ἕτεροι δὲ ἐμπαιγμῶν καὶ μαστίγων πεῖραν ἔλαβον**: The word ἕτεροι is the nominative plural of ἕτερος, which means "others." The conjunction δὲ is used to indicate a contrast or continuation, and can be translated as "but" or "and." The genitive plural ἐμπαιγμῶν is derived from the noun ἐμπαίγμα, meaning "mocking" or "ridicule." The genitive plural μαστίγων is derived from the noun μάστιξ, meaning "whip" or "scourge." The accusative singular πεῖραν is the direct object of the verb ἔλαβον, which is the first person plural aorist indicative of λαμβάνω, meaning "I received" or "I took." The phrase thus means "others experienced mockings and floggings."

- **ἔτι δὲ δεσμῶν καὶ φυλακῆς**: The adverb ἔτι means "still" or "yet." The genitive plural δεσμῶν is derived from the noun δεσμός, meaning "bond" or "chain." The genitive singular φυλακῆς is derived from the noun φυλακή, meaning "imprisonment" or "custody." The phrase can be translated as "still in bonds and in prison."

The entire verse can be translated as: "Others experienced mockings and floggings, and even chains and imprisonment."