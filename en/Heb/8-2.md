---
version: 1
---
- **τῶν ἁγίων λειτουργὸς**: The word τῶν is the genitive plural of the article ὁ, meaning "of the." The noun ἁγίων is the genitive plural of ἅγιος, which means "holy ones" or "saints." The noun λειτουργὸς is in the nominative singular and means "minister" or "servant." The phrase thus means "the minister of the holy ones."

- **καὶ τῆς σκηνῆς τῆς ἀληθινῆς**: The word καὶ is a conjunction meaning "and." The noun τῆς is the genitive singular of the article ὁ. The noun σκηνῆς is in the genitive singular and means "tent" or "tabernacle." The adjective ἀληθινῆς is also in the genitive singular and means "true" or "genuine." The phrase thus means "and of the true tent."

- **ἣν ἔπηξεν ὁ κύριος**: The pronoun ἣν is the accusative singular feminine of ὅς, meaning "which" or "whom." The verb ἔπηξεν is the third person singular aorist indicative of πήγνυμι, meaning "he set up" or "he pitched." The noun ὁ κύριος is in the nominative singular and means "the Lord." The phrase thus means "which the Lord set up."

- **οὐκ ἄνθρωπος**: The word οὐκ is an adverb meaning "not." The noun ἄνθρωπος is in the nominative singular and means "man" or "human." The phrase thus means "not man."

Literal translation: "the minister of the holy ones and of the true tent, which the Lord set up, not man."

In Hebrews 8:2, the author is describing Jesus as the minister or servant of the holy ones and of the true tabernacle, which the Lord himself set up and is not of human origin.