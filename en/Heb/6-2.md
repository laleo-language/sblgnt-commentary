---
version: 1
---
- **βαπτισμῶν**: This is the genitive plural of the noun βάπτισμα, which means "baptism." The genitive case indicates that it is a partitive genitive, so it can be translated as "of baptisms."

- **διδαχὴν**: This is the accusative singular of the noun διδαχή, which means "teaching" or "instruction." Here it is modified by the genitive noun βαπτισμῶν, so it can be translated as "teaching of baptisms."

- **ἐπιθέσεώς**: This is the genitive singular of the noun ἐπίθεσις, which means "laying on" or "imposition." It is also modified by the genitive noun βαπτισμῶν, so it can be translated as "laying on of hands."

- **τε**: This is a conjunction that connects the previous two phrases, indicating that they are both part of a larger list. It can be translated as "and."

- **χειρῶν**: This is the genitive plural of the noun χείρ, which means "hand." It is modified by the genitive noun βαπτισμῶν, so it can be translated as "of hands."

- **ἀναστάσεώς**: This is the genitive singular of the noun ἀνάστασις, which means "resurrection." It is modified by the genitive noun βαπτισμῶν, so it can be translated as "of resurrections."

- **τε**: This is another conjunction that connects the previous phrase with the next one, indicating that they are all part of the same list. It can also be translated as "and."

- **νεκρῶν**: This is the genitive plural of the noun νεκρός, which means "dead." It is modified by the genitive noun βαπτισμῶν, so it can be translated as "of the dead."

- **κρίματος**: This is the genitive singular of the noun κρίμα, which means "judgment." It is also modified by the genitive noun βαπτισμῶν, so it can be translated as "of judgment."

- **αἰωνίου**: This is the genitive singular of the adjective αἰώνιος, which means "eternal" or "everlasting." It is modifying the genitive noun κρίματος, so it can be translated as "eternal" or "of eternal judgment."

Literal translation: "teaching of baptisms, laying on of hands, resurrection of the dead, and eternal judgment."

In Hebrews 6:2, the author is listing various teachings or doctrines that were foundational to the early Christian community. These include the teachings about baptisms, the laying on of hands, the resurrection of the dead, and eternal judgment. These teachings would have been important for the believers to understand and embrace as part of their faith.