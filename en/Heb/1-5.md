---
version: 1
---
- **Τίνι γὰρ εἶπέν ποτε τῶν ἀγγέλων**: The word Τίνι is the dative singular of τίς, which means "who" or "to whom." The verb εἶπέν is the third person singular aorist indicative of λέγω, meaning "he said." The noun ποτε is an adverb meaning "once" or "at some time." The genitive plural τῶν ἀγγέλων means "of the angels." The phrase thus means "To whom did he ever say to the angels?"

- **Υἱός μου εἶ σύ**: The noun Υἱός is the nominative singular of υἱός, meaning "son." The pronoun σύ is the second person singular pronoun, meaning "you." The verb εἶ is the second person singular present indicative of εἰμί, meaning "you are." The phrase thus means "You are my son."

- **ἐγὼ σήμερον γεγέννηκά σε**: The pronoun ἐγὼ is the first person singular pronoun, meaning "I." The adverb σήμερον means "today." The verb γεγέννηκά is the first person singular perfect indicative of γεννάω, meaning "I have begotten" or "I have brought forth." The pronoun σε is the accusative singular pronoun, meaning "you." The phrase thus means "I have begotten you today."

- **καὶ πάλιν· Ἐγὼ ἔσομαι αὐτῷ εἰς πατέρα, καὶ αὐτὸς ἔσται μοι εἰς υἱόν**: The conjunction καὶ means "and." The adverb πάλιν means "again" or "once more." The pronoun Ἐγὼ is the first person singular pronoun, meaning "I." The verb ἔσομαι is the first person singular future indicative of εἰμί, meaning "I will be." The preposition αὐτῷ is the dative singular of αὐτός, meaning "to him." The preposition εἰς is used with the accusative case and can mean "into" or "to." The noun πατέρα is the accusative singular of πατήρ, meaning "father." The conjunction καὶ means "and." The pronoun αὐτὸς is the nominative singular of αὐτός, meaning "he." The verb ἔσται is the third person singular future indicative of εἰμί, meaning "he will be." The pronoun μοι is the dative singular pronoun, meaning "to me." The preposition εἰς is used with the accusative case and can mean "into" or "to." The noun υἱόν is the accusative singular of υἱός, meaning "son." The phrase thus means "And again, I will be to him a father, and he will be to me a son."

The literal translation of Hebrews 1:5 is: "For to whom did he ever say to the angels, 'You are my son, today I have begotten you,' and again, 'I will be to him a father, and he will be to me a son'?"

The verse is not syntactically ambiguous.