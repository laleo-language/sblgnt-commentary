---
version: 1
---
- **γῆ γὰρ**: The word γῆ is the nominative singular of γῆ, which means "earth" or "land." The word γὰρ is a conjunction meaning "for" or "because." The phrase means "for the earth."

- **ἡ πιοῦσα**: The word πιοῦσα is the nominative singular feminine participle of πίνω, which means "to drink." The article ἡ indicates that it is a definite noun phrase. The phrase means "the one drinking."

- **τὸν ἐπʼ αὐτῆς ἐρχόμενον πολλάκις**: The word ἐρχόμενον is the accusative singular masculine participle of ἔρχομαι, which means "to come." The preposition ἐπʼ with the genitive αὐτῆς means "upon it" or "on it." The adverb πολλάκις means "many times" or "often." The phrase means "the one coming upon it many times."

- **ὑετόν**: The word ὑετόν is the accusative singular of ὑετός, which means "rain." It is the direct object of the verb ἔρχομαι. The phrase means "the rain."

- **καὶ τίκτουσα βοτάνην εὔθετον**: The word τίκτουσα is the nominative singular feminine participle of τίκτω, which means "to bear" or "to give birth to." The article ἡ indicates that it is a definite noun phrase. The noun βοτάνην is the accusative singular of βοτάνη, which means "herb" or "plant." The adjective εὔθετον means "fitting" or "suitable." The phrase means "the one bearing suitable plants."

- **ἐκείνοις**: The word ἐκείνοις is the dative plural of ἐκεῖνος, which means "those" or "to those." It is the indirect object of the verb τίκτω. The phrase means "to those."

- **διʼ οὓς**: The phrase διʼ οὓς consists of the preposition διά followed by the accusative plural of ὅς, which means "through whom" or "because of whom." The phrase means "through whom."

- **καὶ γεωργεῖται**: The word γεωργεῖται is the third person singular present indicative middle of γεωργέω, which means "to cultivate" or "to work the land." The phrase means "and it is cultivated."

- **μεταλαμβάνει εὐλογίας ἀπὸ τοῦ θεοῦ**: The word μεταλαμβάνει is the third person singular present indicative active of μεταλαμβάνω, which means "to receive" or "to partake of." The noun εὐλογίας is the genitive singular of εὐλογία, which means "blessing" or "benefit." The preposition ἀπὸ with the genitive τοῦ θεοῦ means "from God." The phrase means "it receives blessings from God."

- The literal translation of the entire verse is: "For the earth, the one drinking the rain coming upon it many times, and the one bearing suitable plants to those through whom it is cultivated, receives blessings from God."