---
version: 1
---
- **πίστει**: This is the dative singular of the noun πίστις, which means "faith" or "trust." The dative case here indicates the means or manner by which the action is performed. The word πίστει is translated as "by faith."

- **παρῴκησεν**: This is the third person singular aorist indicative active form of the verb παροικέω, which means "to sojourn" or "to dwell as a stranger." The subject of the verb is not explicitly stated, but it is implied to be Abraham based on the context. The verb παρῴκησεν is translated as "he sojourned."

- **εἰς γῆν τῆς ἐπαγγελίας**: The prepositional phrase εἰς γῆν means "into the land." The noun γῆν is the accusative singular form of γῆ, which means "land" or "earth." The genitive phrase τῆς ἐπαγγελίας means "of the promise." The noun ἐπαγγελίας is the genitive singular of ἐπαγγελία, which means "promise." This phrase is translated as "into the land of the promise."

- **ὡς ἀλλοτρίαν**: The adverb ὡς means "as" or "like." The adjective ἀλλοτρίαν means "foreign" or "alien." This phrase is translated as "like a foreign land."

- **ἐν σκηναῖς**: The prepositional phrase ἐν σκηναῖς means "in tents." The noun σκηναῖς is the dative plural form of σκηνή, which means "tent." This phrase is translated as "in tents."

- **κατοικήσας**: This is the nominative singular masculine participle form of the verb κατοικέω, which means "to dwell" or "to live." The participle form κατοικήσας agrees with the subject Abraham. This verb is translated as "having dwelt" or "having lived."

- **μετὰ Ἰσαὰκ καὶ Ἰακὼβ**: The prepositional phrase μετὰ means "with." The proper nouns Ἰσαὰκ and Ἰακὼβ refer to Isaac and Jacob, the sons of Abraham. This phrase is translated as "with Isaac and Jacob."

- **τῶν συγκληρονόμων τῆς ἐπαγγελίας τῆς αὐτῆς**: The genitive phrase τῶν συγκληρονόμων means "of the fellow heirs." The noun συγκληρονόμων is the genitive plural form of συγκληρονόμος, which means "fellow heir." The genitive phrase τῆς ἐπαγγελίας means "of the promise." The possessive pronoun αὐτῆς means "their." This phrase is translated as "of the fellow heirs of the same promise."

- The entire verse is translated as: "By faith he sojourned in the land of the promise, as in a foreign land, dwelling in tents with Isaac and Jacob, fellow heirs of the same promise."