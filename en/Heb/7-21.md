---
version: 1
---
- **ὁ δὲ μετὰ ὁρκωμοσίας**: The article ὁ is the nominative singular masculine form of the definite article, meaning "the." The conjunction δὲ means "but" or "and." The preposition μετὰ means "with" or "after." The noun ὁρκωμοσίας is the genitive singular of ὁρκωμοσία, which means "oath." The phrase thus means "but with an oath."

- **διὰ τοῦ λέγοντος**: The preposition διὰ means "through" or "by means of." The definite article τοῦ is the genitive singular masculine form of the definite article. The participle λέγοντος is the genitive singular masculine present active participle of λέγω, meaning "saying" or "the one who says." The phrase can be translated as "through the one who says."

- **πρὸς αὐτόν**: The preposition πρὸς means "to" or "towards." The pronoun αὐτόν is the accusative singular masculine form of αὐτός, meaning "him." The phrase can be translated as "to him."

- **Ὤμοσεν κύριος**: The verb Ὤμοσεν is the third person singular aorist indicative active of ὀμνύω, meaning "he swore" or "the Lord swore." The noun κύριος is the nominative singular masculine form of κύριος, meaning "Lord." The phrase means "the Lord swore."

- **καὶ οὐ μεταμεληθήσεται**: The conjunction καὶ means "and." The adverb οὐ means "not." The verb μεταμεληθήσεται is the third person singular future indicative middle form of μεταμέλομαι, meaning "he will change his mind" or "he will not repent." The phrase can be translated as "and he will not change his mind."

- **Σὺ ἱερεὺς εἰς τὸν ⸀αἰῶνα**: The pronoun Σὺ is the nominative singular second person pronoun, meaning "you." The noun ἱερεὺς is the nominative singular masculine form of ἱερεύς, meaning "priest." The preposition εἰς means "for" or "to." The article τὸν is the accusative singular masculine form of the definite article. The noun αἰῶνα is the accusative singular of αἰών, meaning "age" or "eternity." The phrase means "you are a priest forever."

The literal translation of the entire verse is: "But with an oath by the one who says to him, 'The Lord swore and he will not change his mind, you are a priest forever.'"