---
version: 1
---
- **οὐδὲ διʼ αἵματος τράγων καὶ μόσχων**: The conjunction οὐδὲ means "nor" or "not even." The preposition διʼ means "through" or "by means of." The word αἵματος is the genitive singular of αἷμα, meaning "blood." The nouns τράγων and μόσχων are in the genitive plural and mean "goats" and "calves" respectively. So this phrase can be translated as "not even through the blood of goats and calves."

- **διὰ δὲ τοῦ ἰδίου αἵματος**: The preposition διὰ appears again, meaning "through" or "by means of." The article τοῦ is the genitive singular of ὁ, "the." The pronoun ἰδίου is the genitive singular of ἴδιος, meaning "his own." And once again, we have the word αἷμα meaning "blood." So this phrase can be translated as "but through his own blood."

- **εἰσῆλθεν ἐφάπαξ εἰς τὰ ἅγια**: The verb εἰσῆλθεν is the third person singular aorist indicative of εἰσέρχομαι, meaning "he entered" or "he went into." The adverb ἐφάπαξ means "once" or "once for all." The preposition εἰς means "into" or "to." And the article τὰ is the accusative plural of ὁ, meaning "the." The noun ἅγια is the accusative plural of ἅγιος, meaning "holy things" or "sanctuaries." So this phrase can be translated as "he entered once for all into the holy things."

- **αἰωνίαν λύτρωσιν εὑράμενος**: The adjective αἰωνίαν is the accusative singular of αἰώνιος, meaning "eternal" or "everlasting." The noun λύτρωσιν is the accusative singular of λύτρωσις, meaning "redemption" or "deliverance." The verb εὑράμενος is the nominative singular middle participle of εὑρίσκω, meaning "having found" or "having obtained." So this phrase can be translated as "having found eternal redemption."

Putting it all together, a literal translation of the verse would be: "Not even through the blood of goats and calves, but through his own blood, he entered once for all into the holy things, having found eternal redemption."