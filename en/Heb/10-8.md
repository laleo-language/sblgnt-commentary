---
version: 1
---
- **ἀνώτερον λέγων**: The word ἀνώτερον is a comparative adverb derived from the adjective ἀνώτερος, meaning "higher" or "superior." The participle λέγων is the present active nominative masculine singular form of the verb λέγω, meaning "saying" or "speaking." The phrase thus means "saying something higher" or "speaking something superior."

- **ὅτι Θυσίας καὶ προσφορὰς καὶ ὁλοκαυτώματα καὶ περὶ ἁμαρτίας οὐκ ἠθέλησας οὐδὲ εὐδόκησας**: This is a complex phrase with multiple components. The word Θυσίας is the genitive plural of θυσία, which means "sacrifices." The word προσφορὰς is the genitive plural of προσφορά, meaning "offerings." The word ὁλοκαυτώματα is the accusative plural of ὁλοκαύτωμα, which means "burnt offerings." The word περὶ ἁμαρτίας is a prepositional phrase meaning "for sins." The verb οὐκ ἠθέλησας is the second person singular aorist indicative of θέλω, meaning "you did not want." The verb οὐδὲ εὐδόκησας is the second person singular aorist indicative of εὐδοκέω, meaning "you did not take pleasure." The phrase can be translated as "you did not want sacrifices and offerings and burnt offerings for sins, nor did you take pleasure."

- **αἵτινες κατὰ νόμον προσφέρονται**: The word αἵτινες is a relative pronoun meaning "which" or "who." The phrase κατὰ νόμον is a prepositional phrase meaning "according to the law." The verb προσφέρονται is the third person plural present indicative passive form of προσφέρω, meaning "they are offered." The phrase can be translated as "which are offered according to the law."

The literal translation of the entire verse is: "Saying something higher, that you did not want sacrifices and offerings and burnt offerings for sins, nor did you take pleasure, which are offered according to the law."