---
version: 1
---
- **Οὗτος γὰρ ὁ Μελχισέδεκ**: The word Οὗτος is the nominative singular masculine form of οὗτος, which means "this." The word γὰρ is a conjunction meaning "for." The name Μελχισέδεκ refers to Melchizedek, a figure mentioned in the Old Testament. The phrase thus means "For this Melchizedek."

- **βασιλεὺς Σαλήμ**: The word βασιλεὺς is the nominative singular masculine form of βασιλεύς, which means "king." The name Σαλήμ refers to Salem, a place mentioned in the Old Testament. The phrase means "King of Salem."

- **ἱερεὺς τοῦ θεοῦ τοῦ ὑψίστου**: The word ἱερεὺς is the nominative singular masculine form of ἱερεύς, which means "priest." The phrase τοῦ θεοῦ τοῦ ὑψίστου means "of God the Most High." The phrase thus means "Priest of God the Most High."

- **ὁ συναντήσας Ἀβραὰμ ὑποστρέφοντι ἀπὸ τῆς κοπῆς τῶν βασιλέων**: The word ὁ is the nominative singular masculine form of ὁ, which means "the." The word συναντήσας is the nominative singular masculine participle of συναντάω, which means "having met." The name Ἀβραὰμ refers to Abraham, a prominent figure in the Old Testament. The word ὑποστρέφοντι is the dative singular masculine form of ὑποστρέφω, which means "returning." The word ἀπὸ is a preposition meaning "from." The phrase τῆς κοπῆς τῶν βασιλέων means "from the slaughter of the kings." The phrase thus means "the one who met Abraham returning from the slaughter of the kings."

- **καὶ εὐλογήσας αὐτόν**: The word καὶ is a conjunction meaning "and." The word εὐλογήσας is the nominative singular masculine participle of εὐλογέω, which means "having blessed." The pronoun αὐτόν means "him." The phrase thus means "and having blessed him."

Literal translation: "For this Melchizedek, king of Salem, priest of God the Most High, the one who met Abraham returning from the slaughter of the kings and blessed him."