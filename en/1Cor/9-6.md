---
version: 1
---
- **ἢ μόνος ἐγὼ καὶ Βαρναβᾶς**: The word μόνος is an adjective meaning "alone" or "only." The pronoun ἐγὼ is the first person singular pronoun, meaning "I." The name Βαρναβᾶς is in the nominative case, meaning it is the subject of the sentence. Together, the phrase means "Is it only I and Barnabas?"

- **οὐκ ἔχομεν ἐξουσίαν**: The word οὐκ is a negative particle meaning "not." The verb ἔχομεν is the first person plural present indicative of ἔχω, meaning "we have." The noun ἐξουσίαν is in the accusative case and means "authority" or "power." The phrase means "We do not have authority."

- **μὴ ἐργάζεσθαι**: The word μὴ is a negative particle meaning "not." The verb ἐργάζεσθαι is the present middle infinitive of ἐργάζομαι, meaning "to work" or "to labor." The phrase means "not to work."

Putting it all together, the literal translation of the verse is: "Is it only I and Barnabas who do not have the authority not to work?"