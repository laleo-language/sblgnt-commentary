---
version: 1
---
- **πᾶς ἀνὴρ**: The word πᾶς is an adjective meaning "every" or "all". The noun ἀνὴρ means "man" or "husband". So this phrase means "every man" or "every husband".

- **προσευχόμενος ἢ προφητεύων**: The participle προσευχόμενος is from the verb προσεύχομαι, meaning "to pray". The participle προφητεύων is from the verb προφητεύω, meaning "to prophesy". These two participles are connected by the conjunction ἢ, which means "or". So this phrase means "praying or prophesying".

- **κατὰ κεφαλῆς ἔχων**: The prepositional phrase κατὰ κεφαλῆς means "over the head" or "on the head". The participle ἔχων is from the verb ἔχω, meaning "to have" or "to hold". So this phrase means "having over the head" or "having on the head".

- **καταισχύνει τὴν κεφαλὴν αὐτοῦ**: The verb καταισχύνει is the third person singular present indicative of καταισχύνω, meaning "to dishonor" or "to shame". The noun κεφαλὴν means "head". The pronoun αὐτοῦ means "his". So this phrase means "he dishonors his head".

In this verse, Paul is addressing the issue of men praying or prophesying with their heads covered, which was culturally inappropriate in the Corinthian context. The phrase "every man praying or prophesying with his head covered" is used to describe the action that dishonors his head.

Literal translation: "Every man praying or prophesying with his head covered dishonors his head."