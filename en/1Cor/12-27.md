---
version: 1
---
- **Ὑμεῖς δέ**: The pronoun Ὑμεῖς is the nominative plural of σύ, which means "you." The conjunction δέ means "but" or "and." The phrase thus means "But you."

- **ἐστε σῶμα Χριστοῦ**: The verb ἐστε is the second person plural present indicative of εἰμί, meaning "you are." The noun σῶμα is the nominative singular of σῶμα, which means "body." The genitive Χριστοῦ is the genitive singular of Χριστός, which means "of Christ." The phrase thus means "you are the body of Christ."

- **καὶ μέλη ἐκ μέρους**: The conjunction καί means "and." The noun μέλη is the nominative plural of μέλος, which means "members." The preposition ἐκ means "from" or "out of." The genitive μέρους is the genitive singular of μέρος, which means "part." The phrase thus means "and members from a part."

The literal translation of the entire verse is: "But you are the body of Christ and members from a part."