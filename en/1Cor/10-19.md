---
version: 1
---
- **τί οὖν φημι;**: The word τί is the accusative singular of τίς, which means "what." The verb φημι is the first person singular present indicative of φημί, meaning "I say" or "I affirm." The phrase thus means "What then do I say?"

- **ὅτι εἰδωλόθυτόν τί ἐστιν**: The word ὅτι is a conjunction meaning "that." The noun εἰδωλόθυτόν is the accusative singular of εἰδωλόθυτος, which means "meat sacrificed to idols." The word τί is the accusative singular of τίς, which means "what." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "it is." The phrase thus means "that meat sacrificed to idols is what?"

- **ἢ ὅτι εἴδωλόν τί ἐστιν**: The word ἢ is a conjunction meaning "or." The noun εἴδωλόν is the accusative singular of εἴδωλον, which means "an idol." The word τί is the accusative singular of τίς, which means "what." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "it is." The phrase thus means "or that an idol is what?"

The entire verse can be translated as: "What then do I say? That meat sacrificed to idols is what? Or that an idol is what?"