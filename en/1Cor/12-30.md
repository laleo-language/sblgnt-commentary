---
version: 1
---
- **μὴ πάντες χαρίσματα ἔχουσιν ἰαμάτων**: The word μὴ is a particle that is often used to form a question. The word πάντες is the nominative plural form of πᾶς, which means "all." The noun χαρίσματα is the accusative plural of χάρισμα, which means "spiritual gifts" or "graces." The verb ἔχουσιν is the third person plural present indicative of ἔχω, meaning "they have." The noun ἰαμάτων is the genitive plural of ἴαμα, which means "healings" or "cures." The phrase thus means "Do all have the gifts of healings?"

- **μὴ πάντες γλώσσαις λαλοῦσιν**: The word μὴ is again used to form a question. The word πάντες is the nominative plural form of πᾶς. The noun γλώσσαις is the dative plural of γλῶσσα, which means "tongues" or "languages." The verb λαλοῦσιν is the third person plural present indicative of λαλέω, meaning "they speak." The phrase thus means "Do all speak in tongues?"

- **μὴ πάντες διερμηνεύουσιν**: The word μὴ is again used to form a question. The word πάντες is the nominative plural form of πᾶς. The verb διερμηνεύουσιν is the third person plural present indicative of διερμηνεύω, which means "they interpret" or "they translate." The phrase thus means "Do all interpret?"

Literal translation: "Do all have the gifts of healings? Do all speak in tongues? Do all interpret?"