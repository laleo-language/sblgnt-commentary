---
version: 1
---
- **ὁ δὲ κολλώμενος**: The article ὁ is the nominative singular masculine form of the definite article, meaning "the." The verb κολλώμενος is the present participle middle/passive form of κολλάω, meaning "to be joined to" or "to be united with." The participle is masculine singular, so it agrees with the subject of the sentence. The phrase thus means "the one who is joined to" or "the one who is united with."

- **τῷ κυρίῳ**: The article τῷ is the dative singular masculine form of the definite article, meaning "to the." The noun κυρίῳ is the dative singular masculine form of κύριος, meaning "Lord" or "master." The phrase means "to the Lord."

- **ἓν πνεῦμά ἐστιν**: The adjective ἓν is the nominative/accusative neuter singular form of εἷς, meaning "one." The noun πνεῦμά is the nominative/accusative neuter singular form of πνεῦμα, meaning "spirit." The verb ἐστιν is the third person singular present indicative form of εἰμί, meaning "is." The phrase means "is one spirit."

The phrase "ὁ δὲ κολλώμενος τῷ κυρίῳ ἓν πνεῦμά ἐστιν" can be translated as "the one who is joined to the Lord is one spirit."

The entire verse can be translated as "But the one who is joined to the Lord is one spirit."