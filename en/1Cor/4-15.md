---
version: 1
---
- **ἐὰν γὰρ μυρίους παιδαγωγοὺς ἔχητε ἐν Χριστῷ**: The word ἐὰν is a conjunction meaning "if." The word γὰρ is a conjunction meaning "for" or "because." The adjective μυρίους is the accusative plural masculine of μύριος, which means "countless" or "innumerable." The noun παιδαγωγοὺς is the accusative plural masculine of παιδαγωγός, which means "tutors" or "guardians." The verb ἔχητε is the second person plural present subjunctive of ἔχω, meaning "you have." The preposition ἐν means "in." The noun Χριστῷ is the dative singular masculine of Χριστός, which means "Christ." The phrase thus means "For if you have countless tutors in Christ."

- **ἀλλʼ οὐ πολλοὺς πατέρας**: The conjunction ἀλλʼ means "but" or "rather." The adverb οὐ means "not." The adjective πολλοὺς is the accusative plural masculine of πολύς, which means "many" or "much." The noun πατέρας is the accusative plural masculine of πατήρ, which means "fathers." The phrase thus means "but not many fathers."

- **ἐν γὰρ Χριστῷ Ἰησοῦ διὰ τοῦ εὐαγγελίου**: The preposition ἐν means "in." The conjunction γὰρ means "for" or "because." The noun Χριστῷ is the dative singular masculine of Χριστός, which means "Christ." The noun Ἰησοῦ is the genitive singular masculine of Ἰησοῦς, which means "Jesus." The preposition διὰ means "through" or "by means of." The article τοῦ is the genitive singular masculine of ὁ, which means "the." The noun εὐαγγελίου is the genitive singular neuter of εὐαγγέλιον, which means "gospel" or "good news." The phrase thus means "For in Christ Jesus through the gospel."

- **ἐγὼ ὑμᾶς ἐγέννησα**: The pronoun ἐγὼ means "I." The pronoun ὑμᾶς means "you." The verb ἐγέννησα is the first person singular aorist indicative of γεννάω, which means "I have begotten" or "I have fathered." The phrase thus means "I have begotten you."

The entire verse is translated as: "For if you have countless tutors in Christ, but not many fathers; for in Christ Jesus through the gospel I have begotten you."