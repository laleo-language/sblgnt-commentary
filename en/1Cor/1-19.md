---
version: 1
---
**γέγραπται γάρ**: The verb γέγραπται is the 3rd person singular perfect indicative of γράφω, meaning "it has been written." The word γάρ is a conjunction that means "for" or "because." So this phrase means "for it has been written."

**Ἀπολῶ τὴν σοφίαν τῶν σοφῶν**: The verb Ἀπολῶ is the 1st person singular future indicative of ἀπόλλυμι, meaning "I will destroy" or "I will bring to nothing." The noun σοφίαν is the accusative singular of σοφία, which means "wisdom." The article τὴν is the accusative singular of ὁ, "the." The genitive plural τῶν σοφῶν means "of the wise." So this phrase means "I will destroy the wisdom of the wise."

**καὶ τὴν σύνεσιν τῶν συνετῶν ἀθετήσω**: The conjunction καὶ means "and." The noun σύνεσιν is the accusative singular of σύνεσις, which means "understanding" or "knowledge." The article τὴν is the accusative singular of ὁ, "the." The genitive plural τῶν συνετῶν means "of the intelligent." The verb ἀθετήσω is the 1st person singular future indicative of ἀθετέω, meaning "I will set aside" or "I will disregard." So this phrase means "and I will set aside the understanding of the intelligent."

The literal translation of the entire verse is: "For it has been written, 'I will destroy the wisdom of the wise, and I will set aside the understanding of the intelligent.'"