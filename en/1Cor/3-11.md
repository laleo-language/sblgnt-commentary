---
version: 1
---
- **θεμέλιον γὰρ**: The word θεμέλιον is the accusative singular of θεμέλιος, which means "foundation." The conjunction γὰρ means "for" or "because." The phrase thus means "for the foundation."

- **ἄλλον οὐδεὶς δύναται θεῖναι**: The word ἄλλον is the accusative singular of ἄλλος, meaning "another." The word οὐδεὶς is the nominative singular of οὐδείς, which means "no one" or "nobody." The verb δύναται is the third person singular present indicative of δύναμαι, meaning "is able" or "can." The infinitive θεῖναι is the present infinitive of τίθημι, meaning "to lay" or "to set." The phrase thus means "no one is able to lay."

- **παρὰ τὸν κείμενον**: The preposition παρὰ means "beside" or "with." The article τὸν is the accusative singular masculine form of ὁ, which means "the." The participle κείμενον is the present participle of κείμαι, meaning "being laid" or "being set." The phrase thus means "beside the one being laid" or "beside the one being set."

- **ὅς ἐστιν Ἰησοῦς Χριστός**: The relative pronoun ὅς is the nominative singular masculine form of ὅς, which means "who" or "which." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The noun Ἰησοῦς is the nominative singular of Ἰησοῦς, which is the name "Jesus." The noun Χριστός is the nominative singular of Χριστός, which means "Christ." The phrase thus means "who is Jesus Christ."

- The literal translation of 1 Corinthians 3:11 is: "For no one is able to lay another foundation beside the one being laid, which is Jesus Christ."