---
version: 1
---
- **δεῖ γὰρ αὐτὸν βασιλεύειν**: The verb δεῖ is a third person singular present indicative of δέω, meaning "it is necessary" or "it must." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." The verb βασιλεύειν is the present infinitive of βασιλεύω, meaning "to reign" or "to be king." The phrase thus means "for it is necessary for him to reign."

- **ἄχρι οὗ θῇ πάντας τοὺς ἐχθροὺς ὑπὸ τοὺς πόδας αὐτοῦ**: The preposition ἄχρι means "until" or "up to." The pronoun οὗ is the genitive singular of ὅς, meaning "of whom" or "of which." The verb θῇ is the third person singular aorist subjunctive of τίθημι, meaning "he puts" or "he places." The adjective πάντας is the accusative plural of πᾶς, meaning "all" or "every." The noun τοὺς ἐχθροὺς is the accusative plural of ἐχθρός, meaning "enemies." The preposition ὑπὸ means "under" or "beneath." The definite article τοὺς is the accusative plural of ὁ, meaning "the." The noun πόδας is the accusative plural of πούς, meaning "feet." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase can be translated as "until he puts all the enemies under his feet."

The entire verse can be translated as: "For it is necessary for him to reign until he puts all the enemies under his feet."