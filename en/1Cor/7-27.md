---
version: 1
---
- **δέδεσαι γυναικί**: The word δέδεσαι is the second person singular perfect passive indicative of δέω, which means "to be bound" or "to be married." The noun γυναικί is the dative singular of γυνή, which means "woman" or "wife." The phrase thus means "Are you bound to a woman?"

- **μὴ ζήτει λύσιν**: The word μὴ is a negative particle that means "do not." The verb ζήτει is the second person singular present active imperative of ζητέω, which means "to seek." The noun λύσιν is the accusative singular of λύσις, which means "release" or "divorce." The phrase thus means "Do not seek divorce."

- **λέλυσαι ἀπὸ γυναικός**: The word λέλυσαι is the second person singular perfect active indicative of λύω, which means "to release" or "to divorce." The preposition ἀπὸ means "from." The noun γυναικός is the genitive singular of γυνή. The phrase thus means "You have divorced from your wife."

- **μὴ ζήτει γυναῖκα**: The word μὴ is a negative particle that means "do not." The verb ζήτει is the second person singular present active imperative of ζητέω, which means "to seek." The noun γυναῖκα is the accusative singular of γυνή. The phrase thus means "Do not seek a wife."

Literal translation: "Are you bound to a woman? Do not seek divorce. You have divorced from your wife? Do not seek a wife."