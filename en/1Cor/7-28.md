---
version: 1
---
- **ἐὰν δὲ καὶ γαμήσῃς**: The word ἐὰν is a conjunction meaning "if." The verb γαμήσῃς is the second person singular aorist subjunctive of γαμέω, which means "to marry." The phrase thus means "if you marry."

- **οὐχ ἥμαρτες**: The word οὐχ is a negation meaning "did not." The verb ἥμαρτες is the second person singular aorist indicative of ἁμαρτάνω, which means "to sin." The phrase thus means "you did not sin."

- **καὶ ἐὰν γήμῃ ἡ παρθένος**: The word καὶ is a conjunction meaning "and." The verb γήμῃ is the third person singular aorist subjunctive of γαμέω, which means "to marry." The article ἡ is the feminine nominative singular form of ὁ, which means "the." The noun παρθένος is the nominative singular form of παρθένος, which means "virgin." The phrase thus means "and if the virgin marries."

- **οὐχ ἥμαρτεν**: The word οὐχ is a negation meaning "did not." The verb ἥμαρτεν is the third person singular aorist indicative of ἁμαρτάνω, which means "to sin." The phrase thus means "she did not sin."

- **θλῖψιν δὲ τῇ σαρκὶ ἕξουσιν οἱ τοιοῦτοι**: The noun θλῖψιν is the accusative singular form of θλῖψις, which means "affliction" or "trouble." The conjunction δὲ connects this phrase to the previous ones and can be translated as "but" or "and." The article τῇ is the feminine dative singular form of ὁ, which means "the." The noun σαρκὶ is the dative singular form of σάρξ, which means "flesh." The verb ἕξουσιν is the third person plural future indicative of ἔχω, which means "they will have." The pronoun οἱ is the nominative plural form of ὁ, which means "those." The adjective τοιοῦτοι is the nominative plural form of τοιοῦτος, which means "such." The phrase thus means "but those who marry will have trouble in the flesh."

- **ἐγὼ δὲ ὑμῶν φείδομαι**: The pronoun ἐγὼ is the first person singular form of ἐγώ, which means "I." The conjunction δὲ connects this phrase to the previous one and can be translated as "but" or "and." The pronoun ὑμῶν is the genitive plural form of σύ, which means "you." The verb φείδομαι is the first person singular present indicative of φείδομαι, which means "I spare" or "I pity." The phrase thus means "but I spare you."

The literal translation of 1 Corinthians 7:28 is: "But if you marry, you did not sin. And if the virgin marries, she did not sin. But those who marry will have trouble in the flesh. But I spare you."