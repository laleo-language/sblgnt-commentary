---
version: 1
---
- **οὐκ ἀσχημονεῖ**: The word οὐκ is the negative particle, meaning "not." The verb ἀσχημονεῖ is the third person singular present indicative of ἀσχημονέω, which means "to act unbecomingly" or "to behave inappropriately." The phrase thus means "it does not act unbecomingly."

- **οὐ ζητεῖ τὰ ἑαυτῆς**: The word οὐ is the negative particle, meaning "not." The verb ζητεῖ is the third person singular present indicative of ζητέω, which means "to seek" or "to desire." The phrase τὰ ἑαυτῆς means "its own." So, the phrase οὐ ζητεῖ τὰ ἑαυτῆς means "it does not seek its own."

- **οὐ παροξύνεται**: The word οὐ is the negative particle, meaning "not." The verb παροξύνεται is the third person singular present indicative of παροξύνω, which means "to provoke" or "to irritate." The phrase thus means "it is not provoked."

- **οὐ λογίζεται τὸ κακόν**: The word οὐ is the negative particle, meaning "not." The verb λογίζεται is the third person singular present indicative of λογίζομαι, which means "to account" or "to reckon." The phrase τὸ κακόν means "evil" or "wrong." So, the phrase οὐ λογίζεται τὸ κακόν means "it does not take into account wrongdoing."

The literal translation of 1 Corinthians 13:5 is: "it does not act unbecomingly, it does not seek its own, it is not provoked, it does not take into account wrongdoing."