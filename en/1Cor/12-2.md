---
version: 1
---
- **οἴδατε**: The verb οἶδατε is the second person plural present indicative of οἶδα, meaning "you know." 
- **ὅτι**: The conjunction ὅτι means "that" and introduces a subordinate clause. 
- **ὅτε**: The conjunction ὅτε means "when." 
- **ἔθνη**: The noun ἔθνη is the nominative plural of ἔθνος, meaning "nations" or "Gentiles." 
- **ἦτε**: The verb ἦτε is the second person plural imperfect indicative of εἰμί, meaning "you were." 
- **πρὸς**: The preposition πρὸς means "toward" or "with." 
- **τὰ εἴδωλα**: The article τὰ is the accusative plural neuter of ὁ, meaning "the." The noun εἴδωλα is the accusative plural of εἴδωλον, meaning "idols." 
- **τὰ ἄφωνα**: The article τὰ is the accusative plural neuter of ὁ, meaning "the." The adjective ἄφωνα is the accusative plural neuter of ἄφωνος, meaning "voiceless" or "dumb." 
- **ὡς ἂν ἤγεσθε**: The word ὡς is an adverb meaning "as" or "like." The adverb ἂν is used here to indicate contingency or possibility. The verb ἤγεσθε is the second person plural imperfect indicative middle/passive of ἄγω, meaning "you were led" or "you were carried away." 
- **ἀπαγόμενοι**: The verb ἀπαγόμενοι is the nominative plural middle/passive participle of ἀπάγω, meaning "being carried away" or "being led."

Literal translation: "You know that when you were Gentiles, you were led away to the voiceless idols as you were being carried away."

In this verse, Paul is reminding the Corinthians that before they became believers, they were led astray by idols, which were voiceless and powerless. The phrase "πρὸς τὰ εἴδωλα τὰ ἄφωνα" (toward the voiceless idols) describes the direction or association of the Corinthians with these idols. The phrase "ὡς ἂν ἤγεσθε ἀπαγόμενοι" (as you were being carried away) further explains the manner in which they were led astray.