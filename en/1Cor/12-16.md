---
version: 1
---
- **καὶ ἐὰν εἴπῃ τὸ οὖς**: The word καὶ is a conjunction that means "and." The verb εἴπῃ is the third person singular aorist subjunctive of λέγω, meaning "he may say" or "he says." The article τὸ is the neuter singular accusative of ὁ, which means "the." The noun οὖς is the neuter singular accusative of οὖς, which means "ear." The phrase thus means "and if the ear says."

- **Ὅτι οὐκ εἰμὶ ὀφθαλμός**: The conjunction Ὅτι means "that." The word οὐκ is an adverb that means "not." The verb εἰμὶ is the first person singular present indicative of εἰμί, meaning "I am." The noun ὀφθαλμός is the masculine singular nominative of ὀφθαλμός, which means "eye." The phrase thus means "that I am not an eye."

- **οὐκ εἰμὶ ἐκ τοῦ σώματος**: The word οὐκ is an adverb that means "not." The verb εἰμὶ is the first person singular present indicative of εἰμί, meaning "I am." The preposition ἐκ means "from" or "out of." The article τοῦ is the neuter singular genitive of ὁ, which means "the." The noun σώματος is the neuter singular genitive of σῶμα, which means "body." The phrase thus means "I am not from the body."

- **οὐ παρὰ τοῦτο οὐκ ἔστιν ἐκ τοῦ σώματος**: The word οὐ is an adverb that means "not." The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "it is." The preposition παρὰ means "beside" or "apart from." The article τοῦτο is the neuter singular nominative of οὗτος, which means "this." The preposition ἐκ means "from" or "out of." The article τοῦ is the neuter singular genitive of ὁ, which means "the." The noun σώματος is the neuter singular genitive of σῶμα, which means "body." The phrase thus means "not beside this, it is not from the body."

The literal translation of the entire verse is: "And if the ear says, 'I am not an eye,' I am not from the body, not beside this, it is not from the body."