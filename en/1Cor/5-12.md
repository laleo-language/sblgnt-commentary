---
version: 1
---
- **τί γάρ μοι τοὺς ἔξω κρίνειν;**: The word τί is the accusative singular of τίς, which means "who" or "what." The word γάρ is a particle that introduces a reason or explanation, often translated as "for" or "because." The pronoun μοι is the dative singular form of ἐγώ, meaning "to me" or "for me." The phrase τοὺς ἔξω is composed of the article τοὺς, the accusative plural form of ὁ, meaning "the," and the adverb ἔξω, meaning "outside" or "externally." The verb κρίνειν is the present infinitive of κρίνω, meaning "to judge" or "to condemn." The phrase thus means "For what is it to me to judge those outside?" or "Why should I judge those outside?"

- **οὐχὶ τοὺς ἔσω ὑμεῖς κρίνετε**: The word οὐχὶ is a particle that expresses negation, often translated as "not." The pronoun ὑμεῖς is the nominative plural form of σύ, meaning "you." The phrase τοὺς ἔσω is composed of the article τοὺς, the accusative plural form of ὁ, meaning "the," and the adverb ἔσω, meaning "inside" or "internally." The verb κρίνετε is the present indicative second person plural form of κρίνω, meaning "you judge" or "you condemn." The phrase thus means "Do you not judge those inside?" or "Shouldn't you judge those inside?"

The verse can be literally translated as: "For what is it to me to judge those outside? Do you not judge those inside?"