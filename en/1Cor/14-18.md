---
version: 1
---
- εὐχαριστῶ τῷ θεῷ: The verb εὐχαριστῶ is the first person singular present indicative of εὐχαριστέω, meaning "I give thanks" or "I am thankful." The dative noun τῷ θεῷ means "to God." The phrase thus means "I give thanks to God."

- πάντων ὑμῶν μᾶλλον: The adjective πάντων is the genitive plural of πᾶς, meaning "all" or "every." The pronoun ὑμῶν is the genitive plural of σύ, meaning "you." The adverb μᾶλλον means "more" or "rather." The phrase can be translated as "of all of you, rather" or "more than all of you."

- γλώσσαις λαλῶ: The noun γλώσσαις is the dative plural of γλῶσσα, meaning "tongues" or "languages." The verb λαλῶ is the first person singular present indicative of λαλέω, meaning "I speak" or "I talk." The phrase thus means "I speak in tongues" or "I speak in languages."

The literal translation of the entire verse is: "I give thanks to God, I speak in tongues more than all of you."