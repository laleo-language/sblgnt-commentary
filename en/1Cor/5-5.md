---
version: 1
---
- **παραδοῦναι τὸν τοιοῦτον τῷ Σατανᾷ**: The verb παραδοῦναι is the present infinitive of παραδίδωμι, meaning "to deliver" or "to hand over." The article τὸν is the accusative singular masculine definite article, indicating that the following noun is the direct object of the verb. The adjective τοιοῦτον means "such" or "this kind of." The dative noun τῷ Σατανᾷ means "to Satan." The phrase can be translated as "to deliver such a one to Satan."

- **εἰς ὄλεθρον τῆς σαρκός**: The preposition εἰς means "to" or "for." The noun ὄλεθρον means "destruction" or "ruin." The genitive noun τῆς σαρκός means "of the flesh." The phrase can be translated as "for the destruction of the flesh."

- **ἵνα τὸ πνεῦμα σωθῇ**: The conjunction ἵνα introduces a purpose clause, indicating the intended outcome of the action. The article τὸ is the nominative singular neuter definite article, indicating that the following noun is the subject of the verb. The noun πνεῦμα means "spirit." The verb σωθῇ is the third person singular aorist subjunctive passive of σῴζω, meaning "to save" or "to preserve." The phrase can be translated as "so that the spirit may be saved."

- **ἐν τῇ ἡμέρᾳ τοῦ κυρίου**: The preposition ἐν means "in" or "on." The article τῇ is the dative singular feminine definite article, indicating that the following noun is in the dative case. The noun ἡμέρᾳ means "day." The genitive noun τοῦ κυρίου means "of the Lord." The phrase can be translated as "in the day of the Lord."

The literal translation of the entire verse is: "To deliver such a one to Satan for the destruction of the flesh, so that the spirit may be saved in the day of the Lord."