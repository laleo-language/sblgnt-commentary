---
version: 1
---
- **τῇ ἐκκλησίᾳ τοῦ θεοῦ**: The noun ἐκκλησίᾳ is the dative singular of ἐκκλησία, which means "church" or "assembly." The genitive τοῦ θεοῦ indicates possession, so the phrase means "to the church of God."

- **ἡγιασμένοις ἐν Χριστῷ Ἰησοῦ**: The participle ἡγιασμένοις is the dative plural masculine of ἁγιάζω, which means "to sanctify" or "to make holy." It is modifying the noun ἐκκλησίᾳ. The prepositional phrase ἐν Χριστῷ Ἰησοῦ indicates location and means "in Christ Jesus," so the phrase means "to the sanctified in Christ Jesus."

- **τῇ οὔσῃ ἐν Κορίνθῳ**: The noun οὔσῃ is the dative singular feminine of εἰμί, which means "to be." It is modifying the noun ἐκκλησίᾳ. The prepositional phrase ἐν Κορίνθῳ indicates location and means "in Corinth," so the phrase means "to the [church] being in Corinth."

- **κλητοῖς ἁγίοις**: The participle κλητοῖς is the dative plural masculine of καλέω, which means "to call" or "to summon." It is modifying the noun ἐκκλησίᾳ. The adjective ἁγίοις is the dative plural masculine of ἅγιος, which means "holy" or "saints." So the phrase means "to the called saints."

- **σὺν πᾶσιν τοῖς ἐπικαλουμένοις τὸ ὄνομα τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ**: The preposition σὺν means "with." The article τοῖς is the dative plural masculine of ὁ, which means "the." The participle ἐπικαλουμένοις is the dative plural masculine of ἐπικαλέω, which means "to call upon" or "to invoke." The noun ὄνομα is the accusative singular of ὄνομα, which means "name." The genitive τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ indicates possession and means "of our Lord Jesus Christ." So the phrase means "with all those who call upon the name of our Lord Jesus Christ."

- **ἐν παντὶ τόπῳ αὐτῶν καὶ ἡμῶν**: The preposition ἐν means "in." The adjective παντὶ is the dative singular neuter of πᾶς, which means "all" or "every." The noun τόπῳ is the dative singular masculine of τόπος, which means "place" or "location." The pronouns αὐτῶν and ἡμῶν mean "their" and "our" respectively. So the phrase means "in every place of theirs and ours."

Literal translation: "To the church of God, to the sanctified in Christ Jesus, to the [church] being in Corinth, to the called saints, with all those who call upon the name of our Lord Jesus Christ, in every place of theirs and ours."