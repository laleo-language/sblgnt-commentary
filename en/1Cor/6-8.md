---
version: 1
---
- **ἀλλὰ ὑμεῖς**: The word ἀλλὰ is a conjunction meaning "but." The pronoun ὑμεῖς is the second person plural pronoun, meaning "you." The phrase thus means "but you."

- **ἀδικεῖτε καὶ ἀποστερεῖτε**: The verb ἀδικεῖτε is the second person plural present indicative of ἀδικέω, meaning "you are wronging" or "you are treating unjustly." The verb ἀποστερεῖτε is the second person plural present indicative of ἀποστερέω, meaning "you are defrauding" or "you are depriving." The phrase thus means "you are wronging and defrauding."

- **καὶ ⸀τοῦτο ἀδελφούς**: The conjunction καὶ means "and." The pronoun τοῦτο is the accusative singular of οὗτος, meaning "this." The noun ἀδελφούς is the accusative plural of ἀδελφός, meaning "brothers" or "brethren." The phrase thus means "and this (you are wronging) the brothers."

The literal translation of the entire verse is: "But you are wronging and defrauding, and this the brothers."