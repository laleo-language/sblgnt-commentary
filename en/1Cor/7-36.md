---
version: 1
---
- **Εἰ δέ τις ἀσχημονεῖν**: The phrase Εἰ δέ τις is composed of the conditional particle Εἰ, meaning "if," and the indefinite pronoun δέ τις, meaning "someone" or "anyone." The verb ἀσχημονεῖν is the present infinitive of ἀσχημονέω, which means "to behave improperly" or "to act indecently." The phrase thus means "if someone behaves improperly."

- **ἐπὶ τὴν παρθένον αὐτοῦ**: The preposition ἐπὶ means "upon" or "towards." The article τὴν is the accusative singular feminine form of the definite article ὁ, meaning "the." The noun παρθένον is the accusative singular form of παρθένος, which means "virgin" or "unmarried woman." The possessive pronoun αὐτοῦ means "his" or "her." The phrase thus means "upon his virgin" or "towards his unmarried woman."

- **νομίζει ἐὰν ᾖ ὑπέρακμος**: The verb νομίζει is the third person singular present indicative of νομίζω, which means "to consider" or "to think." The conjunction ἐὰν means "if." The verb ᾖ is the third person singular present subjunctive of εἰμί, meaning "to be." The adjective ὑπέρακμος means "ripe" or "in full bloom." The phrase thus means "he thinks if she is ripe" or "he considers if she is in full bloom."

- **καὶ οὕτως ὀφείλει γίνεσθαι**: The conjunction καὶ means "and." The adverb οὕτως means "thus" or "in this way." The verb ὀφείλει is the third person singular present indicative of ὀφείλω, which means "to owe" or "to be obliged." The verb γίνεσθαι is the present infinitive of γίνομαι, meaning "to become" or "to be." The phrase thus means "and thus he is obliged to become" or "and in this way he must become."

- **ὃ θέλει ποιείτω**: The pronoun ὃ is the accusative singular neuter form of ὅς, meaning "what" or "which." The verb θέλει is the third person singular present indicative of θέλω, which means "to want" or "to desire." The verb ποιείτω is the third person singular present imperative of ποιέω, meaning "to do" or "to make." The phrase thus means "let him do what he wants" or "let him make what he desires."

- **οὐχ ἁμαρτάνει**: The adverb οὐχ means "not." The verb ἁμαρτάνει is the third person singular present indicative of ἁμαρτάνω, which means "to sin" or "to make a mistake." The phrase thus means "he is not sinning" or "he is not making a mistake."

- **γαμείτωσαν**: The verb γαμείτωσαν is the third person plural present imperative of γαμέω, which means "to marry." The phrase thus means "let them marry."

Literal translation: "If someone behaves improperly towards his virgin and he thinks if she is ripe, and thus he is obliged to become, let him do what he wants, he is not sinning; let them marry."