---
version: 1
---
- **βρῶμα δὲ ἡμᾶς**: The word βρῶμα is the accusative singular of βρῶμα, which means "food." The pronoun ἡμᾶς is the accusative plural of ἐγώ, meaning "us." The phrase thus means "food for us."

- **οὐ ⸀παραστήσει τῷ θεῷ**: The word οὐ is the negative particle, meaning "not." The verb παραστήσει is the third person singular future indicative of παρίστημι, meaning "will present" or "will offer." The dative article τῷ indicates the indirect object, meaning "to the." The noun θεῷ is the dative singular of θεός, meaning "God." The phrase thus means "will not present to God."

- **οὔτε ⸂γὰρ ἐὰν φάγωμεν, περισσεύομεν**: The word οὔτε is a conjunction meaning "neither." The conjunction γὰρ is used to provide an explanation or reason, and can be translated as "for." The verb φάγωμεν is the first person plural aorist subjunctive of ἐσθίω, meaning "we eat." The verb περισσεύομεν is the first person plural present indicative of περισσεύω, meaning "we have an abundance." The phrase thus means "for neither if we eat, do we have an abundance."

- **οὔτε ἐὰν μὴ φάγωμεν, ὑστερούμεθα**: The word οὔτε is a conjunction meaning "neither." The conjunction ἐὰν introduces a condition and can be translated as "if." The verb φάγωμεν is the first person plural aorist subjunctive of ἐσθίω, meaning "we eat." The verb ὑστερούμεθα is the first person plural present indicative of ὑστερέω, meaning "we lack" or "we are in need." The phrase thus means "neither if we do not eat, do we lack."

- **βρῶμα δὲ ἡμᾶς οὐ ⸀παραστήσει τῷ θεῷ· οὔτε ⸂γὰρ ἐὰν φάγωμεν, περισσεύομεν, οὔτε ἐὰν μὴ φάγωμεν, ὑστερούμεθα⸃**: The entire verse can be translated as "But food will not bring us closer to God; neither if we eat, do we have an abundance, nor if we do not eat, do we lack."