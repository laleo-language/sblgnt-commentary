---
version: 1
---
- **ἐγὼ τοίνυν**: The pronoun ἐγὼ is the first person singular pronoun, meaning "I." The adverb τοίνυν means "therefore" or "so." The phrase ἐγὼ τοίνυν translates to "I therefore."
- **οὕτως τρέχω**: The adverb οὕτως means "thus" or "in this way." The verb τρέχω is the first person singular present indicative of τρέχω, meaning "I run." The phrase οὕτως τρέχω translates to "I run in this way."
- **ὡς οὐκ ἀδήλως**: The conjunction ὡς means "as" or "like." The adverb οὐκ means "not." The adjective ἀδήλως means "uncertain" or "in an unclear manner." The phrase ὡς οὐκ ἀδήλως translates to "not uncertainly" or "not in an unclear manner."
- **οὕτως πυκτεύω**: The adverb οὕτως means "thus" or "in this way." The verb πυκτεύω is the first person singular present indicative of πυκτεύω, meaning "I box" or "I fight." The phrase οὕτως πυκτεύω translates to "I fight in this way."
- **ὡς οὐκ ἀέρα δέρων**: The conjunction ὡς means "as" or "like." The adverb οὐκ means "not." The noun ἀήρ (genitive: ἀέρος) means "air." The verb δέρων is the present participle of δέρω, meaning "striking" or "beating." The phrase ὡς οὐκ ἀέρα δέρων translates to "not striking the air."

Literal translation: "I therefore run in this way, not uncertainly; I fight in this way, not beating the air."

The verse as a whole emphasizes the focused and purposeful nature of the speaker's actions, both in running and fighting. The phrases highlight the intention to avoid ambiguity and aimlessness in these endeavors.