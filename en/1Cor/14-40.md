---
version: 1
---
- **πάντα**: This is the neuter plural form of πᾶς, which means "all" or "everything."
- **δὲ**: This is a conjunction that can be translated as "but" or "and."
- **εὐσχημόνως**: This is the adverbial form of εὐσχημοσύνη, which means "properly" or "decently."
- **καὶ**: This is a conjunction that can be translated as "and."
- **κατὰ τάξιν**: This is a prepositional phrase that means "in order" or "according to order."
- **γινέσθω**: This is the third person singular present imperative middle form of γίνομαι, which means "let it happen" or "let it be done."

The phrase "πάντα δὲ εὐσχημόνως καὶ κατὰ τάξιν" can be translated as "but everything should be done properly and in order." 

In this verse, the apostle Paul is giving instructions to the Corinthians about how to conduct themselves in the assembly. He emphasizes the importance of maintaining order and decency in their worship services. The phrase is a command, urging them to ensure that everything that takes place in the assembly is done with propriety and in an orderly manner. 

The literal translation of the verse is "but everything should be done properly and in order."