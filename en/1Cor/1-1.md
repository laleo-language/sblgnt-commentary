---
version: 1
---
- **Παῦλος** is the nominative singular form of Παῦλος, which is the name "Paul."
- **κλητὸς** is the nominative singular form of κλητός, which means "called" or "chosen."
- **ἀπόστολος** is the nominative singular form of ἀπόστολος, which means "apostle."
- **Χριστοῦ Ἰησοῦ** is a genitive phrase that means "of Christ Jesus." Χριστοῦ is the genitive singular form of Χριστός, which means "Christ." Ἰησοῦ is the genitive singular form of Ἰησοῦς, which means "Jesus."
- **διὰ θελήματος θεοῦ** is a prepositional phrase that means "by the will of God." διὰ is a preposition meaning "by" or "through." θελήματος is the genitive singular form of θέλημα, which means "will." θεοῦ is the genitive singular form of θεός, which means "God."
- **καὶ Σωσθένης ὁ ἀδελφὸς** is a phrase that means "and Sosthenes the brother." καὶ is a conjunction meaning "and." Σωσθένης is the nominative singular form of Σωσθένης, which is the name "Sosthenes." ὁ is the definite article meaning "the." ἀδελφὸς is the nominative singular form of ἀδελφός, which means "brother."

Literal translation: "Paul, called apostle of Christ Jesus by the will of God, and Sosthenes the brother."