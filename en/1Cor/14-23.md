---
version: 1
---
- **ἐὰν οὖν συνέλθῃ ἡ ἐκκλησία ὅλη**: The phrase ἐὰν οὖν introduces a conditional statement, meaning "if then." The verb συνέλθῃ is the third person singular aorist subjunctive of συνέρχομαι, which means "to come together" or "to assemble." The noun ἐκκλησία is the nominative singular of ἐκκλησία, which means "church" or "assembly." The adjective ὅλη is the nominative singular feminine form of ὅλος, meaning "whole" or "entire." The phrase thus means "if then the whole church comes together."

- **ἐπὶ τὸ αὐτὸ καὶ πάντες λαλῶσιν γλώσσαις**: The prepositional phrase ἐπὶ τὸ αὐτὸ means "in the same place" or "at the same time." The conjunction καὶ means "and." The verb λαλῶσιν is the third person plural present subjunctive of λαλέω, which means "to speak" or "to talk." The noun γλώσσαις is the dative plural of γλῶσσα, which means "tongues" or "languages." The phrase thus means "and all speak in tongues."

- **εἰσέλθωσιν δὲ ἰδιῶται ἢ ἄπιστοι**: The verb εἰσέλθωσιν is the third person plural aorist subjunctive of εἰσέρχομαι, which means "to enter" or "to come in." The noun ἰδιῶται is the nominative plural of ἰδιώτης, which means "laymen" or "ordinary people." The conjunction δὲ means "but" or "on the other hand." The adjective ἄπιστοι is the nominative plural masculine form of ἄπιστος, meaning "unbelievers" or "non-Christians." The phrase thus means "but if unbelievers or non-Christians enter."

- **οὐκ ἐροῦσιν ὅτι μαίνεσθε**: The adverb οὐκ negates the verb ἐροῦσιν, which is the third person plural future indicative of λέγω, meaning "to say" or "to speak." The conjunction ὅτι introduces a subordinate clause and means "that." The verb μαίνεσθε is the second person plural present indicative of μαίνομαι, which means "to be mad" or "to be insane." The phrase thus means "they will not say that you are mad."

The sentence as a whole can be literally translated as: "If then the whole church comes together in the same place and all speak in tongues, but unbelievers or non-Christians enter, they will not say that you are mad."