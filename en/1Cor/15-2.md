---
version: 1
---
- **διʼ οὗ καὶ σῴζεσθε**: The word διʼ is a preposition meaning "through" or "by means of". The word οὗ is the genitive singular of ὅς, which means "who" or "which". The phrase διʼ οὗ can be translated as "through whom" or "by means of whom". The verb σῴζεσθε is the present passive indicative of σῴζω, meaning "you are being saved" or "you are being rescued". So the phrase can be translated as "through whom you are being saved".

- **τίνι λόγῳ εὐηγγελισάμην ὑμῖν**: The word τίνι is the dative singular of τίς, which means "what" or "which". The word λόγῳ is the dative singular of λόγος, which means "word" or "message". The verb εὐηγγελισάμην is the first person singular aorist indicative middle of εὐαγγελίζομαι, meaning "I proclaimed" or "I preached the good news". The pronoun ὑμῖν is the dative plural of σύ, meaning "to you". So the phrase can be translated as "with what message I proclaimed to you".

- **εἰ κατέχετε**: The word εἰ is a conjunction meaning "if". The verb κατέχετε is the present indicative of κατέχω, meaning "you hold" or "you possess". So the phrase can be translated as "if you hold".

- **ἐκτὸς εἰ μὴ εἰκῇ ἐπιστεύσατε**: The word ἐκτὸς is an adverb meaning "except" or "unless". The word εἰ is a conjunction meaning "if". The particle μὴ is a negation particle meaning "not". The verb εἰκῇ is an adverb meaning "vainly" or "without cause". The verb ἐπιστεύσατε is the second person plural aorist indicative of πιστεύω, meaning "you believed" or "you trusted". So the phrase can be translated as "unless you believed in vain".

The entire verse can be translated as: "Through whom you are being saved, if you hold firmly to the message I proclaimed to you—unless you believed in vain."