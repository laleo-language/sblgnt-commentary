---
version: 1
---
- **ἀλλὰ ἐν ἐκκλησίᾳ**: The word ἀλλὰ means "but" or "instead." The preposition ἐν means "in" or "among." The noun ἐκκλησίᾳ is the dative singular of ἐκκλησία, which means "church." The phrase thus means "but in the church."

- **θέλω πέντε λόγους**: The verb θέλω is the first person singular present indicative of θέλω, which means "I want" or "I desire." The noun πέντε is the accusative plural of πέντε, which means "five." The noun λόγους is the accusative plural of λόγος, which means "word." The phrase thus means "I want five words."

- **τῷ νοΐ μου λαλῆσαι**: The definite article τῷ is the dative singular of ὁ, which means "the." The noun νοΐ is the dative singular of νοῦς, which means "mind" or "understanding." The verb λαλῆσαι is the aorist infinitive of λαλέω, which means "to speak." The phrase thus means "to speak to my understanding."

- **ἵνα καὶ ἄλλους κατηχήσω**: The conjunction ἵνα introduces a purpose clause and means "so that" or "in order to." The adverb καὶ means "also" or "even." The noun ἄλλους is the accusative plural of ἄλλος, which means "other" or "another." The verb κατηχήσω is the first person singular future indicative of κατηχέω, which means "I may instruct" or "I may teach." The phrase thus means "so that I may instruct others."

- **ἢ μυρίους λόγους ἐν γλώσσῃ**: The conjunction ἢ means "or." The adjective μυρίους is the accusative plural of μύριος, which means "countless" or "innumerable." The noun λόγους is the accusative plural of λόγος, which means "word." The preposition ἐν means "in" or "with." The noun γλώσσῃ is the dative singular of γλῶσσα, which means "tongue" or "language." The phrase thus means "or countless words in a tongue."

Literal translation: "But in the church, I want to speak five words to my understanding, so that I may instruct others, or countless words in a tongue."