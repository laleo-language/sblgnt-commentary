---
version: 1
---
- **δεῖ γὰρ**: The verb δεῖ is a third person singular present indicative of δεῖ, which means "it is necessary" or "it must." The word γὰρ is a conjunction that means "for" or "because." So the phrase δεῖ γὰρ translates to "for it is necessary."
- **τὸ φθαρτὸν τοῦτο**: The definite article τὸ is the nominative singular neuter form of the article ὁ, which means "the." The adjective φθαρτὸν is the accusative singular neuter form of the adjective φθαρτός, which means "perishable" or "mortal." The word τοῦτο is the accusative singular neuter form of the pronoun οὗτος, which means "this." So the phrase τὸ φθαρτὸν τοῦτο translates to "this perishable."
- **ἐνδύσασθαι ἀφθαρσίαν**: The verb ἐνδύσασθαι is the aorist infinitive middle form of the verb ἐνδύω, which means "to put on" or "to clothe oneself." The noun ἀφθαρσίαν is the accusative singular feminine form of the noun ἀφθαρσία, which means "incorruptibility" or "immortality." So the phrase ἐνδύσασθαι ἀφθαρσίαν translates to "to put on incorruptibility."

- **καὶ τὸ θνητὸν τοῦτο**: The conjunction καὶ means "and." The definite article τὸ is the nominative singular neuter form of the article ὁ, which means "the." The adjective θνητὸν is the accusative singular neuter form of the adjective θνητός, which means "mortal" or "subject to death." The word τοῦτο is the accusative singular neuter form of the pronoun οὗτος, which means "this." So the phrase τὸ θνητὸν τοῦτο translates to "this mortal."
- **ἐνδύσασθαι ἀθανασίαν**: The verb ἐνδύσασθαι is the aorist infinitive middle form of the verb ἐνδύω, which means "to put on" or "to clothe oneself." The noun ἀθανασίαν is the accusative singular feminine form of the noun ἀθανασία, which means "immortality" or "deathlessness." So the phrase ἐνδύσασθαι ἀθανασίαν translates to "to put on immortality."

The literal translation of the verse is: "For it is necessary for this perishable to put on incorruptibility, and for this mortal to put on immortality."