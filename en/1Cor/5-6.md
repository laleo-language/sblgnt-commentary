---
version: 1
---
- **Οὐ καλὸν τὸ καύχημα ὑμῶν**: The word Οὐ is the negative particle, meaning "not." The adjective καλὸν is the accusative singular neuter form of καλός, which means "good" or "beautiful." The noun καύχημα is the accusative singular neuter form of καύχησις, which means "boasting" or "bragging." The pronoun ὑμῶν is the genitive plural form of ὑμῶν, meaning "your." The phrase thus means "Not good, your boasting."

- **οὐκ οἴδατε ὅτι μικρὰ ζύμη**: The word οὐκ is the negative particle, meaning "not." The verb οἴδατε is the second person plural present indicative form of οἶδα, which means "you know." The conjunction ὅτι introduces a subordinate clause and can be translated as "that." The adjective μικρὰ is the nominative singular neuter form of μικρός, meaning "small" or "little." The noun ζύμη is the nominative singular feminine form of ζύμη, which means "leaven" or "yeast." The phrase thus means "You do not know that a little leaven."

- **ὅλον τὸ φύραμα ζυμοῖ**: The adjective ὅλον is the accusative singular neuter form of ὅλος, meaning "whole" or "entire." The noun φύραμα is the accusative singular neuter form of φύραμα, which means "batch" or "mass." The verb ζυμοῖ is the third person singular present subjunctive form of ζύμοω, meaning "to leaven" or "to ferment." The phrase thus means "leavens the whole batch."

The literal translation of the verse is: "Not good, your boasting. You do not know that a little leaven leavens the whole batch."