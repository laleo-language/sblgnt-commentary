---
version: 1
---
- **γάλα ὑμᾶς ἐπότισα**: The word γάλα is the accusative singular of γάλα, which means "milk." The verb ἐπότισα is the first person singular aorist indicative of ποτίζω, meaning "I gave to drink" or "I watered." The phrase thus means "I gave you milk to drink."

- **οὐ βρῶμα**: The word οὐ is the negation particle meaning "not." The noun βρῶμα is the accusative singular of βρῶμα, which means "food." The phrase thus means "not food."

- **οὔπω γὰρ ἐδύνασθε**: The word οὔπω is a compound of οὐ, meaning "not," and πώποτε, meaning "ever." The verb ἐδύνασθε is the second person plural imperfect indicative of δύναμαι, meaning "you were able" or "you could." The phrase thus means "for you were not yet able."

- **ἀλλʼ οὐδὲ ἔτι νῦν δύνασθε**: The word ἀλλʼ is a conjunction meaning "but" or "however." The word οὐδὲ is a compound of οὐ, meaning "not," and δέ, meaning "but." The adverb ἔτι means "yet" or "still." The adverb νῦν means "now." The verb δύνασθε is the second person plural present indicative of δύναμαι, meaning "you are able" or "you can." The phrase thus means "but you are not yet able now."

The literal translation of 1 Corinthians 3:2 is: "I gave you milk to drink, not food, for you were not yet able, but you are not yet able now."