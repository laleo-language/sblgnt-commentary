---
version: 1
---
- **τὰ κρυπτὰ τῆς καρδίας αὐτοῦ**: The noun phrase τὰ κρυπτὰ is the neuter plural accusative of the adjective κρυπτός, meaning "hidden." The genitive noun τῆς καρδίας is the genitive singular of καρδία, meaning "heart." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, meaning "his." The phrase thus means "the hidden things of his heart."

- **φανερὰ γίνεται**: The adjective φανερὰ is the neuter plural nominative of φανερός, meaning "visible" or "manifest." The verb γίνεται is the third person singular present indicative of γίνομαι, meaning "it happens" or "it becomes." The phrase thus means "it becomes visible."

- **καὶ οὕτως**: The conjunction καὶ means "and," and the adverb οὕτως means "thus" or "in this way." The phrase connects the previous phrase with the following one.

- **πεσὼν ἐπὶ πρόσωπον**: The participle πεσὼν is the nominative singular masculine of πίπτω, meaning "falling." The preposition ἐπὶ means "on" or "upon," and the noun πρόσωπον means "face." The phrase thus means "falling on the face."

- **προσκυνήσει τῷ θεῷ**: The verb προσκυνήσει is the third person singular future indicative of προσκυνέω, meaning "he will worship." The dative noun τῷ θεῷ is the dative singular of θεός, meaning "to God." The phrase thus means "he will worship God."

- **ἀπαγγέλλων ὅτι Ὄντως ὁ θεὸς**: The participle ἀπαγγέλλων is the nominative singular masculine of ἀπαγγέλλω, meaning "proclaiming" or "declaring." The conjunction ὅτι means "that." The adverb Ὄντως means "truly" or "indeed." The article ὁ is the nominative singular masculine of ὁ, meaning "the." The noun θεὸς is the nominative singular masculine of θεός, meaning "God." The phrase thus means "proclaiming that truly God."

- **ἐν ὑμῖν ἐστιν**: The preposition ἐν means "in." The pronoun ὑμῖν is the dative plural of σύ, meaning "you." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The phrase thus means "he is in you."

The entire verse can be literally translated as: "The hidden things of his heart become visible, and thus falling on his face, he will worship God, proclaiming that truly God is in you."