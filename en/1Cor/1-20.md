---
version: 1
---
- **ποῦ σοφός;**: The word ποῦ means "where," and here it is used as an interrogative pronoun. The noun σοφός means "wise." So this phrase translates to "Where is the wise?"

- **ποῦ γραμματεύς;**: The word γραμματεύς means "scribe" or "scholar." So this phrase translates to "Where is the scribe?"

- **ποῦ συζητητὴς τοῦ αἰῶνος τούτου;**: The word συζητητὴς means "debater" or "disputer." The genitive phrase τοῦ αἰῶνος τούτου means "of this age." So this phrase translates to "Where is the debater of this age?"

- **οὐχὶ ἐμώρανεν ὁ θεὸς τὴν σοφίαν τοῦ κόσμου;**: The word οὐχὶ means "not" or "did not." The verb ἐμώρανεν is the third person singular aorist indicative of μωραίνω, which means "to make foolish" or "to make foolishly wise." The noun σοφίαν means "wisdom." The genitive phrase τοῦ κόσμου means "of the world." So this phrase translates to "Did not God make foolish the wisdom of the world?"

The literal translation of the entire verse is: "Where is the wise? Where is the scribe? Where is the debater of this age? Did not God make foolish the wisdom of the world?"