---
version: 1
---
- **καὶ γὰρ**: καὶ is a conjunction meaning "and," and γὰρ is a conjunction meaning "for" or "indeed." Together, they indicate a continuation or further explanation of a previous point. In this case, they introduce a reason or justification for what is about to be said.
- **ἐὰν ἄδηλον φωνὴν σάλπιγξ δῷ**: This phrase consists of three parts. 
   - ἐὰν is a conjunction that introduces a conditional clause and means "if."
   - ἄδηλον is an adjective in the accusative singular form, modifying the noun φωνὴν. It means "uncertain" or "indistinct." 
   - φωνὴν is the accusative singular form of the noun φωνή, meaning "sound" or "voice."
   - σάλπιγξ is the nominative singular form of the noun σάλπιγξ, meaning "trumpet."
   - δῷ is the third person singular aorist subjunctive form of the verb δίδωμι, meaning "to give."
- **τίς παρασκευάσεται εἰς πόλεμον**: This phrase also consists of three parts.
   - τίς is a pronoun meaning "who" or "someone."
   - παρασκευάσεται is the third person singular future middle indicative form of the verb παρασκευάζω, meaning "to prepare" or "to make ready."
   - εἰς is a preposition meaning "for" or "into."
   - πόλεμον is the accusative singular form of the noun πόλεμος, meaning "war."

The phrase can be translated as "And indeed, if the trumpet gives an uncertain sound, who will prepare for battle?"

The literal translation of the verse is: "And indeed, if the trumpet gives an uncertain sound, who will prepare for battle?"