---
version: 1
---
- **καὶ ὁ λόγος μου**: The word λόγος is the nominative singular of λόγος, which means "word" or "message." The pronoun μου is the genitive singular of ἐγώ, meaning "my." The phrase thus means "and my word."

- **καὶ τὸ κήρυγμά μου**: The word κήρυγμα is the nominative singular of κήρυγμα, which means "proclamation" or "preaching." Again, the pronoun μου is the genitive singular of ἐγώ, meaning "my." The phrase thus means "and my proclamation."

- **οὐκ ἐν πειθοῖ σοφίας**: The negation οὐκ means "not." The preposition ἐν means "in" or "by means of." The noun πειθοῖ is the dative singular of πειθός, which means "persuasion" or "convincing." The genitive noun σοφίας is the genitive singular of σοφία, which means "wisdom." The phrase thus means "not in persuasive words of wisdom."

- **ἀλλʼ ἐν ἀποδείξει πνεύματος καὶ δυνάμεως**: The conjunction ἀλλʼ means "but." The preposition ἐν means "in" or "by means of." The noun ἀπόδειξις is the dative singular of ἀπόδειξις, which means "proof" or "demonstration." The genitive noun πνεύματος is the genitive singular of πνεῦμα, which means "spirit." The noun δυνάμεως is the genitive singular of δύναμις, which means "power" or "strength." The phrase thus means "but in the demonstration of the Spirit and power."

The entire verse is translated as follows: "And my message and my preaching were not with persuasive words of wisdom, but with a demonstration of the Spirit and power."