---
version: 1
---
- **καὶ οἱ κλαίοντες**: The word κλαίοντες is the present participle of κλαίω, meaning "to cry" or "to weep." The article οἱ indicates that it is plural and masculine, so the phrase means "those who are crying."

- **ὡς μὴ κλαίοντες**: The word μὴ is a negative particle, and ὡς is a comparative particle that can be translated as "as" or "like." So this phrase can be translated as "as if not crying" or "like they are not crying."

- **καὶ οἱ χαίροντες**: The word χαίροντες is the present participle of χαίρω, meaning "to rejoice" or "to be glad." The article οἱ indicates that it is plural and masculine, so the phrase means "those who are rejoicing."

- **ὡς μὴ χαίροντες**: Similar to the previous phrase, this means "as if not rejoicing" or "like they are not rejoicing."

- **καὶ οἱ ἀγοράζοντες**: The word ἀγοράζοντες is the present participle of ἀγοράζω, meaning "to buy" or "to purchase." The article οἱ indicates that it is plural and masculine, so the phrase means "those who are buying."

- **ὡς μὴ κατέχοντες**: Similar to the previous phrases, this means "as if not possessing" or "like they are not possessing."

The entire verse can be translated as: "And those who weep, as if not weeping; and those who rejoice, as if not rejoicing; and those who buy, as if not possessing."