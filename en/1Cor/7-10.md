---
version: 1
---
- **Τοῖς δὲ γεγαμηκόσιν**: The word Τοῖς is the dative plural of the definite article ὁ, meaning "the." The word δὲ is a conjunction that can be translated as "but" or "and." The adjective γεγαμηκόσιν is the dative plural of γαμηκός, which means "married." The phrase thus means "to the married ones."

- **παραγγέλλω**: This is the first person singular present indicative active of the verb παραγγέλλω, which means "I command" or "I instruct." The phrase means "I command."

- **οὐκ ἐγὼ ἀλλὰ ὁ κύριος**: The word οὐκ is an adverb that means "not." The pronoun ἐγὼ means "I" and is the first person singular personal pronoun. The conjunction ἀλλὰ means "but" or "rather." The article ὁ is the definite article meaning "the." The noun κύριος means "Lord" and refers to Jesus. The phrase means "not I, but the Lord."

- **γυναῖκα ἀπὸ ἀνδρὸς μὴ χωρισθῆναι**: The noun γυναῖκα is the accusative singular of γυνή, which means "woman" or "wife." The preposition ἀπὸ means "from" or "away from." The noun ἀνδρὸς is the genitive singular of ἀνήρ, which means "man" or "husband." The negative particle μὴ means "not." The verb χωρισθῆναι is the aorist passive infinitive of χωρίζω, which means "to separate" or "to divorce." The phrase means "that a woman should not separate from her husband."

The entire verse can be translated as: "To the married ones, I command (this), not I but the Lord, that a woman should not separate from her husband."