---
version: 1
---
- **ὅταν δὲ ἔλθῃ τὸ τέλειον**: The word ὅταν is a conjunction meaning "when." The verb ἔλθῃ is the third person singular aorist subjunctive of ἔρχομαι, meaning "to come." The article τὸ is the nominative neuter singular of ὁ, meaning "the." The noun τέλειον is the accusative neuter singular of τέλειος, meaning "perfect" or "complete." The phrase can be translated as "when the perfect comes."

- **τὸ ἐκ μέρους**: The article τὸ is the nominative neuter singular of ὁ, meaning "the." The preposition ἐκ means "from" or "out of." The noun μέρους is the genitive neuter singular of μέρος, meaning "part." The phrase can be translated as "the part."

- **καταργηθήσεται**: The verb καταργηθήσεται is the third person singular future passive indicative of καταργέω, meaning "to be abolished" or "to be done away with." The phrase can be translated as "will be abolished."

The literal translation of the entire verse is: "When the perfect comes, the part will be abolished."