---
version: 1
---
- **ἢ ἀφʼ ὑμῶν ὁ λόγος τοῦ θεοῦ ἐξῆλθεν**: The word ἢ is a conjunction meaning "or." The preposition ἀπό takes the genitive case, and here it is followed by the genitive plural pronoun ὑμῶν, which means "from you." The noun λόγος is in the nominative singular and means "word" or "message." The article ὁ is the definite article and indicates that the noun is specific. The genitive phrase τοῦ θεοῦ means "of God." The verb ἐξῆλθεν is the third person singular aorist indicative of ἐξέρχομαι, meaning "he/she/it went out" or "he/she/it came forth." So this phrase can be translated as "Or did the word of God come from you?"

- **ἢ εἰς ὑμᾶς μόνους κατήντησεν**: The conjunction ἢ is used again, meaning "or." The preposition εἰς takes the accusative case, and here it is followed by the accusative plural pronoun ὑμᾶς, which means "to you." The adjective μόνους is in the accusative plural and means "alone" or "only." The verb κατήντησεν is the third person singular aorist indicative of καταντάω, meaning "he/she/it arrived" or "he/she/it came to." So this phrase can be translated as "Or did it come only to you?"

- The literal translation of the entire verse is: "Or did the word of God come from you? Or did it come only to you?"