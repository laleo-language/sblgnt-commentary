---
version: 1
---
- **τῷ δὲ θεῷ καὶ πατρὶ ἡμῶν**: The word τῷ is the dative singular of the definite article ὁ, meaning "the." The word δὲ is a conjunction which can be translated as "and" or "but." The word θεῷ is the dative singular of θεός, meaning "God." The word καὶ is a conjunction meaning "and." The word πατρὶ is the dative singular of πατήρ, meaning "father." The word ἡμῶν is the genitive plural of ἐγώ, meaning "our." This phrase can be translated as "to our God and Father."

- **ἡ δόξα εἰς τοὺς αἰῶνας τῶν αἰώνων**: The word ἡ is the nominative singular of the definite article ὁ, meaning "the." The word δόξα is the nominative singular of δόξα, meaning "glory." The word εἰς is a preposition meaning "into" or "for." The word τοὺς is the accusative plural of the definite article ὁ, meaning "the." The word αἰῶνας is the accusative plural of αἰών, meaning "ages" or "eternity." The word τῶν is the genitive plural of the definite article ὁ, meaning "the." The word αἰώνων is the genitive plural of αἰών, meaning "ages" or "eternity." This phrase can be translated as "the glory into the ages of the ages."

- **ἀμήν**: The word ἀμήν is an exclamation meaning "truly" or "so be it."

The literal translation of the entire verse is: "To our God and Father be the glory into the ages of the ages. Truly."