---
version: 1
---
- **ἀσπάζονται ὑμᾶς**: The verb ἀσπάζονται is the third person plural present middle/passive indicative of ἀσπάζομαι, which means "they greet" or "they send greetings." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase thus means "they greet you."

- **πάντες οἱ ἅγιοι**: The adjective πάντες is the nominative plural of πᾶς, meaning "all" or "every." The article οἱ is the nominative plural of ὁ, the definite article. The noun ἅγιοι is the nominative plural of ἅγιος, which means "holy ones" or "saints." The phrase thus means "all the holy ones" or "all the saints."

- **μάλιστα δὲ οἱ ἐκ τῆς Καίσαρος οἰκίας**: The adverb μάλιστα means "especially" or "particularly." The article οἱ is the nominative plural of ὁ, the definite article. The preposition ἐκ means "from." The article τῆς is the genitive singular of ὁ, the definite article. The noun Καίσαρος is the genitive singular of Καίσαρ, which means "Caesar." The noun οἰκίας is the genitive singular of οἰκία, meaning "house" or "household." The phrase thus means "especially those from Caesar's household."

The verse can be literally translated as: "All the holy ones greet you, especially those from Caesar's household."