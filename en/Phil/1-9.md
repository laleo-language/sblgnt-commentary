---
version: 1
---
- **καὶ τοῦτο προσεύχομαι**: The word καὶ is a conjunction meaning "and." The word τοῦτο is a demonstrative pronoun meaning "this." The verb προσεύχομαι is the first person singular present middle indicative of προσεύχομαι, meaning "I pray." The phrase thus means "and I pray this."

- **ἵνα ἡ ἀγάπη ὑμῶν ἔτι μᾶλλον καὶ μᾶλλον περισσεύῃ**: The word ἵνα is a subordinating conjunction meaning "so that" or "in order that." The article ἡ is the nominative singular feminine definite article meaning "the." The noun ἀγάπη is the nominative singular feminine noun meaning "love." The pronoun ὑμῶν is the genitive plural second person pronoun meaning "your." The adverb ἔτι means "still" or "yet." The adverb μᾶλλον means "more" or "increasingly." The verb περισσεύῃ is the third person singular present active subjunctive of περισσεύω, meaning "it may abound" or "it may be abundant." The phrase thus means "so that the love of you may still more and more abound."

- **ἐν ἐπιγνώσει καὶ πάσῃ αἰσθήσει**: The preposition ἐν means "in" or "by means of." The noun ἐπιγνώσει is the dative singular feminine noun meaning "knowledge" or "understanding." The conjunction καὶ is a coordinating conjunction meaning "and." The adjective πάσῃ is the dative singular feminine form of πᾶς, meaning "every" or "all." The noun αἰσθήσει is the dative singular feminine noun meaning "sense" or "perception." The phrase thus means "in knowledge and in every sense."

The literal translation of the verse is: "And I pray this, so that the love of you may still more and more abound in knowledge and in every sense."