---
version: 1
---
- **Τούτου χάριν**: The word Τούτου is the genitive singular of οὗτος, which means "this." The noun χάριν is the accusative singular of χάρις, which means "grace" or "favor." The phrase can be translated as "for this reason" or "because of this."
- **ἐγὼ Παῦλος ὁ δέσμιος τοῦ Χριστοῦ Ἰησοῦ**: The pronoun ἐγὼ means "I" or "me." The name Παῦλος is the nominative singular of Παῦλος, which is the Greek form of "Paul." The phrase ὁ δέσμιος τοῦ Χριστοῦ Ἰησοῦ means "the prisoner of Christ Jesus." Here, ὁ is the definite article meaning "the," δέσμιος is the nominative singular of δέσμιος, which means "prisoner," and Χριστοῦ is the genitive singular of Χριστός, meaning "Christ." The name Ἰησοῦ is the genitive singular of Ἰησοῦς, which is the Greek form of "Jesus."
- **ὑπὲρ ὑμῶν τῶν ἐθνῶν**: The preposition ὑπὲρ means "for" or "on behalf of." The pronoun ὑμῶν means "you" or "your." The article τῶν is the genitive plural form of ὁ, meaning "the." The noun ἐθνῶν is the genitive plural of ἔθνος, which means "Gentiles" or "nations." The phrase can be translated as "for your sake, the Gentiles."

Literal translation: "For this reason, I Paul, the prisoner of Christ Jesus, on behalf of you, the Gentiles—"