---
version: 1
---
- **προορίσας ἡμᾶς**: The word προορίσας is the nominative singular participle of the verb προορίζω, which means "to predestine" or "to appoint beforehand." The pronoun ἡμᾶς is the accusative plural form of ἐγώ, meaning "us." So this phrase means "having predestined us."

- **εἰς υἱοθεσίαν**: The word εἰς is a preposition meaning "for" or "to." The noun υἱοθεσίαν is the accusative singular form of υἱοθεσία, which means "adoption." So this phrase means "for adoption."

- **διὰ Ἰησοῦ Χριστοῦ**: The word διὰ is a preposition meaning "through" or "by means of." The name Ἰησοῦ Χριστοῦ is the genitive form of Ἰησοῦς Χριστός, meaning "Jesus Christ." So this phrase means "through Jesus Christ."

- **εἰς αὐτόν**: The word εἰς is a preposition meaning "to" or "into." The pronoun αὐτόν is the accusative singular form of αὐτός, meaning "him." So this phrase means "into him."

- **κατὰ τὴν εὐδοκίαν τοῦ θελήματος αὐτοῦ**: The word κατὰ is a preposition meaning "according to" or "in accordance with." The noun εὐδοκίαν is the accusative singular form of εὐδοκία, which means "good pleasure" or "favorable purpose." The noun θελήματος is the genitive singular form of θέλημα, meaning "will" or "desire." The pronoun αὐτοῦ is the genitive singular form of αὐτός, meaning "his." So this phrase means "according to the good pleasure of his will."

Literal translation: "Having predestined us for adoption through Jesus Christ into him, according to the good pleasure of his will."