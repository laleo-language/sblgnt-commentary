---
version: 1
---
- **εἴ γε**: The word εἴ is a conjunction that means "if." The word γε is a particle that adds emphasis, and can be translated as "indeed" or "certainly." Together, εἴ γε can be translated as "if indeed" or "if certainly."

- **αὐτὸν ἠκούσατε**: The word αὐτὸν is the accusative singular masculine pronoun, which means "him." The verb ἠκούσατε is the second person plural aorist indicative of ἀκούω, meaning "you heard." The phrase αὐτὸν ἠκούσατε can be translated as "you indeed heard him."

- **καὶ ἐν αὐτῷ ἐδιδάχθητε**: The word καὶ is a conjunction that means "and." The word ἐν is a preposition that means "in." The pronoun αὐτῷ is the dative singular masculine pronoun, which means "him." The verb ἐδιδάχθητε is the second person plural aorist indicative passive of διδάσκω, meaning "you were taught." The phrase καὶ ἐν αὐτῷ ἐδιδάχθητε can be translated as "and in him you were taught."

- **καθώς ἐστιν ἀλήθεια ἐν τῷ Ἰησοῦ**: The word καθώς is a conjunction that means "just as" or "in the same way as." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The noun ἀλήθεια is the nominative singular of ἀλήθεια, which means "truth." The phrase ἀλήθεια ἐν τῷ Ἰησοῦ can be translated as "truth in Jesus." The entire phrase καθώς ἐστιν ἀλήθεια ἐν τῷ Ἰησοῦ can be translated as "just as there is truth in Jesus."

- The literal translation of the verse is: "If indeed you heard him and in him you were taught, just as there is truth in Jesus."