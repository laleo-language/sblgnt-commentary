---
version: 1
---
- **ὅτι**: This is a conjunction that means "because" or "since." It introduces a subordinate clause that explains the reason for what is stated in the main clause.
- **ἦτε**: This is the second person plural imperfect indicative of the verb εἰμί, which means "you were." The imperfect tense indicates an ongoing or repeated action in the past.
- **τῷ καιρῷ ἐκείνῳ**: The noun τῷ καιρῷ is the dative singular of καιρός, which means "time." The adjective ἐκείνῳ is the dative singular of ἐκεῖνος, which means "that" or "those." The phrase together means "in that time" or "at that time."
- **χωρὶς Χριστοῦ**: The preposition χωρὶς means "without." The genitive Χριστοῦ is the genitive singular of Χριστός, which means "Christ." The phrase means "without Christ."
- **ἀπηλλοτριωμένοι**: This is a participle in the nominative plural masculine form of the verb ἀπαλλοτριόω, which means "to alienate" or "to estrange." It describes the subject of the sentence (ὑμεῖς, "you") and means "being alienated."
- **τῆς πολιτείας τοῦ Ἰσραὴλ**: The genitive τῆς πολιτείας is the genitive singular of πολιτεία, which means "citizenship" or "commonwealth." The genitive τοῦ Ἰσραὴλ is the genitive singular of Ἰσραήλ, which means "Israel." The phrase means "the citizenship of Israel."
- **καὶ ξένοι τῶν διαθηκῶν τῆς ἐπαγγελίας**: The conjunction καὶ means "and." The adjective ξένοι is the nominative plural masculine form of ξένος, which means "strangers." The genitive τῶν διαθηκῶν is the genitive plural of διαθήκη, which means "covenant." The genitive τῆς ἐπαγγελίας is the genitive singular of ἐπαγγελία, which means "promise." The phrase means "and strangers to the covenants of the promise."
- **ἐλπίδα μὴ ἔχοντες**: The noun ἐλπίς is the accusative singular of ἐλπίς, which means "hope." The participle ἔχοντες is the nominative plural masculine form of the verb ἔχω, which means "having." The phrase means "not having hope."
- **καὶ ἄθεοι ἐν τῷ κόσμῳ**: The conjunction καὶ means "and." The adjective ἄθεοι is the nominative plural masculine form of ἄθεος, which means "godless" or "without God." The preposition ἐν means "in." The dative τῷ κόσμῳ is the dative singular of κόσμος, which means "world." The phrase means "and godless in the world."

Literal translation: "because you were in that time without Christ, being alienated from the citizenship of Israel and strangers to the covenants of the promise, not having hope and godless in the world."

The verse is discussing the state of the Ephesians before they came to faith in Christ. They were without Christ, separated from the privileges and blessings of being part of the people of Israel, and lacking hope. They were also described as godless in the world.