---
version: 1
---
- **ὑπεράνω πάσης ἀρχῆς**: The word ὑπεράνω is a preposition meaning "above" or "beyond." It is used with the genitive case here, modifying the noun ἀρχῆς, which means "principality" or "rule." The phrase thus means "above every principality."

- **καὶ ἐξουσίας**: The conjunction καὶ means "and." The noun ἐξουσίας means "authority" or "power." The phrase means "and authority."

- **καὶ δυνάμεως**: The conjunction καὶ means "and." The noun δυνάμεως means "power" or "strength." The phrase means "and power."

- **καὶ κυριότητος**: The conjunction καὶ means "and." The noun κυριότητος means "dominion" or "lordship." The phrase means "and dominion."

- **καὶ παντὸς ὀνόματος ὀνομαζομένου**: The conjunction καὶ means "and." The adjective παντὸς means "every" or "all." The noun ὀνόματος means "name." The participle ὀνομαζομένου is from the verb ὀνομάζω, which means "to name" or "to call." The phrase means "and every named name" or "and every name that is named."

- **οὐ μόνον ἐν τῷ αἰῶνι τούτῳ**: The phrase οὐ μόνον means "not only." The preposition ἐν means "in." The article τῷ is the definite article in the dative case, meaning "the." The noun αἰῶνι means "age" or "world." The demonstrative pronoun τούτῳ means "this." The phrase means "not only in this age."

- **ἀλλὰ καὶ ἐν τῷ μέλλοντι**: The conjunction ἀλλὰ means "but" or "rather." The conjunction καὶ means "and." The preposition ἐν means "in." The article τῷ is the definite article in the dative case, meaning "the." The noun μέλλοντι is the present participle of the verb μέλλω, which means "to be about to" or "to intend." The phrase means "but also in the coming age" or "but also in the age to come."

The literal translation of Eph 1:21 is: "above every principality and authority and power and dominion and every named name, not only in this age, but also in the age to come."