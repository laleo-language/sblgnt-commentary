---
version: 1
---
- **ὃ ἑτέραις γενεαῖς οὐκ ἐγνωρίσθη**: The word ὃ is a relative pronoun, which refers back to something mentioned earlier. The noun it refers to is not explicitly stated in this verse, but it is implied to be the mystery mentioned in the previous verses. The phrase ἑτέραις γενεαῖς is composed of the adjective ἑτέραις, meaning "other" or "different," and the noun γενεαῖς, meaning "generations" or "ages." The verb ἐγνωρίσθη is the third person singular aorist passive indicative of γνωρίζω, meaning "to make known" or "to reveal." The phrase thus means "which was not made known to other generations."

- **τοῖς υἱοῖς τῶν ἀνθρώπων**: The word τοῖς is the dative plural article, indicating that the following noun is in the dative case. The noun υἱοῖς is the dative plural of υἱός, meaning "sons" or "children." The preposition τῶν indicates possession, so υἱοῖς τῶν ἀνθρώπων means "to the sons of men" or "to the children of humans."

- **ὡς νῦν ἀπεκαλύφθη**: The word ὡς is an adverb meaning "as" or "like." The adverb νῦν means "now." The verb ἀπεκαλύφθη is the third person singular aorist passive indicative of ἀποκαλύπτω, meaning "to reveal" or "to disclose." The phrase ὡς νῦν ἀπεκαλύφθη therefore means "as it has now been revealed."

- **τοῖς ἁγίοις ἀποστόλοις αὐτοῦ καὶ προφήταις**: The word τοῖς is the dative plural article, indicating that the following nouns are in the dative case. The adjective ἁγίοις means "holy" or "saints." The noun ἀποστόλοις is the dative plural of ἀπόστολος, meaning "apostles." The conjunction καὶ means "and." The noun προφήταις is the dative plural of προφήτης, meaning "prophets." The phrase τοῖς ἁγίοις ἀποστόλοις αὐτοῦ καὶ προφήταις means "to his holy apostles and prophets."

- **ἐν πνεύματι**: The preposition ἐν means "in." The noun πνεῦμα can mean "spirit" or "wind," but in this context, it likely refers to the Holy Spirit. The phrase ἐν πνεύματι thus means "in the Spirit."

The literal translation of Ephesians 3:5 is: "which was not made known to other generations, as it has now been revealed to his holy apostles and prophets in the Spirit."

This verse highlights the revelation of the mystery that was previously unknown to earlier generations but has now been made known to the apostles and prophets through the Holy Spirit.