---
version: 1
---
- **ἐν οἷς**: The word ἐν is a preposition meaning "in" or "among." The relative pronoun οἷς is the plural dative form of ὅς, which means "who" or "which." The phrase ἐν οἷς means "in which" or "among which."

- **καὶ ἡμεῖς πάντες**: The conjunction καὶ means "and." The pronoun ἡμεῖς is the first person plural personal pronoun, meaning "we." The adjective πάντες is the nominative plural form of πᾶς, which means "all" or "every." The phrase καὶ ἡμεῖς πάντες means "and we all."

- **ἀνεστράφημέν ποτε**: The verb ἀνεστράφημέν is the first person plural aorist passive indicative of ἀναστρέφω, which means "to live" or "to conduct oneself." The adverb ποτε means "once" or "formerly." The phrase ἀνεστράφημέν ποτε means "we all once lived" or "we all formerly conducted ourselves."

- **ἐν ταῖς ἐπιθυμίαις τῆς σαρκὸς ἡμῶν**: The preposition ἐν means "in" or "among." The noun ταῖς is the feminine plural dative form of ὁ, which means "the." The noun ἐπιθυμίαις is the plural dative form of ἐπιθυμία, which means "desires" or "lusts." The genitive noun phrase τῆς σαρκὸς ἡμῶν means "of our flesh." The phrase ἐν ταῖς ἐπιθυμίαις τῆς σαρκὸς ἡμῶν means "in the desires of our flesh."

- **ποιοῦντες τὰ θελήματα τῆς σαρκὸς καὶ τῶν διανοιῶν**: The present participle ποιοῦντες is the nominative plural form of ποιέω, which means "to do" or "to make." The noun τὰ θελήματα is the neuter plural accusative form of θέλημα, which means "desires" or "wills." The genitive noun phrase τῆς σαρκὸς means "of the flesh." The genitive noun phrase τῶν διανοιῶν means "of the mind" or "of the thoughts." The phrase ποιοῦντες τὰ θελήματα τῆς σαρκὸς καὶ τῶν διανοιῶν means "doing the desires of the flesh and of the mind."

- **καὶ ἤμεθα τέκνα φύσει ὀργῆς ὡς καὶ οἱ λοιποί**: The conjunction καὶ means "and." The verb ἤμεθα is the first person plural imperfect indicative form of εἰμί, which means "to be." The noun τέκνα is the nominative plural form of τέκνον, which means "children." The noun φύσει is the dative form of φύσις, which means "nature." The genitive noun ὀργῆς means "of wrath" or "of anger." The pronoun οἱ is the nominative plural form of ὁ, which means "the." The noun λοιποί is the nominative plural form of λοιπός, which means "remaining" or "others." The phrase καὶ ἤμεθα τέκνα φύσει ὀργῆς ὡς καὶ οἱ λοιποί means "and we were by nature children of wrath, just like the rest."

The literal translation of Ephesians 2:3 is "among whom also we all once lived in the desires of our flesh, doing the desires of the flesh and of the mind, and we were by nature children of wrath, just like the rest."