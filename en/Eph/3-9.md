---
version: 1
---
- **καὶ φωτίσαι πάντας**: The verb φωτίσαι is the aorist active infinitive of φωτίζω, meaning "to enlighten" or "to illuminate." The word πάντας is the accusative plural form of πᾶς, meaning "all" or "everyone." The phrase thus means "to enlighten everyone."

- **τίς ἡ οἰκονομία τοῦ μυστηρίου**: The word τίς is the nominative singular form of τίς, meaning "what" or "which." The word οἰκονομία is the genitive singular form of οἰκονομία, meaning "administration" or "stewardship." The word μυστηρίου is the genitive singular form of μυστήριον, meaning "mystery." The phrase thus means "what is the administration of the mystery."

- **τοῦ ἀποκεκρυμμένου ἀπὸ τῶν αἰώνων**: The word ἀποκεκρυμμένου is the genitive singular form of ἀποκρύπτω, meaning "hidden" or "concealed." The word αἰώνων is the genitive plural form of αἰών, meaning "ages" or "worlds." The phrase τοῦ ἀποκεκρυμμένου ἀπὸ τῶν αἰώνων functions as an articular participle, modifying μυστηρίου. It can be translated as "the one that has been hidden from the ages."

- **ἐν τῷ θεῷ τῷ τὰ πάντα κτίσαντι**: The word θεῷ is the dative singular form of θεός, meaning "God." The word τῷ is the definite article in the dative singular form. The phrase τῷ θεῷ τῷ τὰ πάντα κτίσαντι functions as a genitive absolute, indicating the circumstances in which the action of the main verb takes place. The phrase can be translated as "in God, the one who created all things."

Literal translation: "And to enlighten everyone, what is the administration of the mystery, the one that has been hidden from the ages, in God, the one who created all things."