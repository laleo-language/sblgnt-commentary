---
version: 1
---
- **ἐν ᾧ**: The preposition ἐν means "in" and ᾧ is a relative pronoun, which means "in whom." Together, they form the phrase "in whom."

- **ἔχομεν τὴν ἀπολύτρωσιν**: The verb ἔχομεν is the first person plural present indicative of ἔχω, meaning "we have." The noun ἀπολύτρωσιν is the accusative singular of ἀπολύτρωσις, which means "redemption" or "deliverance." The phrase thus means "we have the redemption."

- **διὰ τοῦ αἵματος αὐτοῦ**: The preposition διὰ means "through" and τοῦ αἵματος is the genitive singular of αἷμα, which means "blood." The pronoun αὐτοῦ is the genitive singular of αὐτός, which means "his." Together, they form the phrase "through his blood."

- **τὴν ἄφεσιν τῶν παραπτωμάτων**: The article τὴν is the accusative singular feminine of ὁ, which means "the." The noun ἄφεσιν is the accusative singular of ἄφεσις, which means "forgiveness." The genitive plural noun τῶν παραπτωμάτων means "of sins" or "of trespasses." The phrase thus means "the forgiveness of sins."

- **κατὰ τὸ πλοῦτος τῆς χάριτος αὐτοῦ**: The preposition κατὰ means "according to" or "in accordance with." The article τὸ is the accusative singular neuter of ὁ, which means "the." The noun πλοῦτος is the accusative singular of πλοῦτος, which means "riches" or "wealth." The genitive singular noun τῆς χάριτος means "of grace." The pronoun αὐτοῦ is the genitive singular of αὐτός, which means "his." Together, they form the phrase "according to the riches of his grace."

- The literal translation of the entire verse is: "In whom we have the redemption through his blood, the forgiveness of sins, according to the riches of his grace."