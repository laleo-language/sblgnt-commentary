---
version: 1
---
- **μετʼ εὐνοίας**: The word μετʼ is a preposition meaning "with." The noun εὐνοίας is the genitive singular of εὐνοία, which means "goodwill" or "kindness." The phrase μετʼ εὐνοίας means "with goodwill."

- **δουλεύοντες**: This is the present active participle of the verb δουλεύω, which means "to serve" or "to be a slave." The participle form δουλεύοντες means "serving" or "working." 

- **ὡς τῷ κυρίῳ**: The word ὡς is a conjunction meaning "as" or "like." The article τῷ is the dative singular masculine form of the definite article ὁ, which means "the." The noun κυρίῳ is the dative singular of κύριος, which means "lord" or "master." The phrase ὡς τῷ κυρίῳ means "as to the Lord" or "as if serving the Lord."

- **καὶ οὐκ ἀνθρώποις**: The conjunction καὶ means "and." The word οὐκ is a negation meaning "not." The noun ἀνθρώποις is the dative plural of ἄνθρωπος, which means "human beings" or "people." The phrase καὶ οὐκ ἀνθρώποις means "and not to people" or "and not as if serving people."

The entire verse can be translated as: "Serving with goodwill, as if serving the Lord and not people."