---
version: 1
---
- **ἐμοὶ τῷ ἐλαχιστοτέρῳ πάντων ἁγίων ἐδόθη ἡ χάρις αὕτη**: The word ἐμοὶ is the dative singular form of the pronoun ἐγώ, meaning "to me." The word τῷ is the dative singular article, meaning "to the." The word ἐλαχιστοτέρῳ is the dative singular superlative form of the adjective ἐλάχιστος, meaning "least." The word πάντων is the genitive plural form of the adjective πᾶς, meaning "all" or "every." The word ἁγίων is the genitive plural form of the noun ἅγιος, meaning "holy ones" or "saints." The word ἐδόθη is the third person singular aorist passive indicative of the verb δίδωμι, meaning "it was given." The phrase ἡ χάρις αὕτη means "this grace."

- **τοῖς ἔθνεσιν εὐαγγελίσασθαι**: The word τοῖς is the dative plural article, meaning "to the." The word ἔθνεσιν is the dative plural form of the noun ἔθνος, meaning "nations" or "Gentiles." The infinitive εὐαγγελίσασθαι is the aorist infinitive of the verb εὐαγγελίζομαι, meaning "to preach the good news."

- **τὸ ἀνεξιχνίαστον πλοῦτος τοῦ Χριστοῦ**: The word τὸ is the accusative singular article, meaning "the." The word ἀνεξιχνίαστον is the accusative singular form of the adjective ἀνεξιχνίαστος, meaning "unsearchable" or "unfathomable." The word πλοῦτος is the genitive singular form of the noun πλοῦτος, meaning "riches" or "wealth." The word τοῦ Χριστοῦ is the genitive singular form of the noun Χριστός, meaning "of Christ."

The literal translation of Ephesians 3:8 would be: "To me, the least of all the holy ones, this grace was given—to preach the unsearchable riches of Christ to the nations."