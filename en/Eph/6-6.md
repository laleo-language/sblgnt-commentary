---
version: 1
---
- **μὴ κατʼ ὀφθαλμοδουλίαν**: The word μὴ is a negative particle meaning "not." The preposition κατά is followed by the accusative case, and it can mean "according to" or "in accordance with." The noun ὀφθαλμοδουλίαν is a compound word made up of ὀφθαλμός, meaning "eye," and δουλία, meaning "slavery" or "servitude." So together, this phrase means "not according to eye-slavery" or "not in the way of eye-service."

- **ὡς ἀνθρωπάρεσκοι**: The word ὡς is a particle that can mean "as" or "like." The adjective ἀνθρωπάρεσκοι is the nominative plural form of ἀνθρωπάρεσκος, meaning "people-pleasers" or "those who seek to please others."

- **ἀλλʼ ὡς ⸀δοῦλοι Χριστοῦ**: The word ἀλλά is a coordinating conjunction meaning "but." The word δοῦλοι is the nominative plural form of δοῦλος, meaning "slaves" or "servants." The genitive form Χριστοῦ is the genitive singular of Χριστός, meaning "Christ." So this phrase means "but as slaves of Christ."

- **ποιοῦντες τὸ θέλημα τοῦ θεοῦ**: The participle ποιοῦντες is the present active nominative plural form of ποιέω, meaning "doing" or "performing." The noun τὸ θέλημα is the accusative singular of θέλημα, meaning "will" or "desire." The genitive form τοῦ θεοῦ is the genitive singular of θεός, meaning "God." So this phrase means "doing the will of God."

- **ἐκ ψυχῆς**: The preposition ἐκ is followed by the genitive case, and it can mean "from" or "out of." The noun ψυχή is the genitive singular of ψυχή, meaning "soul" or "inner being." So this phrase means "from the soul" or "with sincerity."

Putting it all together, the verse can be translated as: "Not with eye-service as people-pleasers, but as slaves of Christ, doing the will of God from the soul."

This verse is not syntactically ambiguous.