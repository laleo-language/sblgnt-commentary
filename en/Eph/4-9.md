---
version: 1
---
- **τὸ δὲ Ἀνέβη**: The word τὸ is the neuter singular article, meaning "the." The word δὲ is a conjunction that can mean "but" or "and." The verb Ἀνέβη is the third person singular aorist indicative of ἀναβαίνω, meaning "he went up" or "he ascended." The phrase thus means "the he ascended."

- **τί ἐστιν**: The word τί is an interrogative pronoun meaning "what." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he is." The phrase thus means "what is."

- **εἰ μὴ ὅτι**: The phrase εἰ μὴ is a conjunction that can mean "unless" or "except." The word ὅτι is a conjunction that can mean "that" or "because." The phrase ὅτι καὶ can be translated as "because also" or "that also." The phrase thus means "unless that also."

- **καὶ ⸀κατέβη**: The word καὶ is a conjunction that means "and." The verb κατέβη is the third person singular aorist indicative of καταβαίνω, meaning "he went down" or "he descended." The phrase thus means "and he descended."

- **εἰς τὰ κατώτερα μέρη τῆς γῆς**: The word εἰς is a preposition meaning "into" or "to." The article τὰ is the neuter plural article, meaning "the." The word κατώτερα is the neuter plural comparative adjective of κατώτερος, meaning "lower." The noun μέρη is the neuter plural of μέρος, meaning "parts" or "regions." The noun γῆς is the genitive singular of γῆ, meaning "earth" or "land." The phrase thus means "into the lower parts of the earth."

The entire verse can be translated as: "Now, what is the meaning of 'he ascended,' if not also that he descended into the lower parts of the earth?"