---
version: 1
---
- **Ἑνὶ δὲ ἑκάστῳ ἡμῶν**: The word Ἑνὶ is the dative singular of εἷς, which means "one." The word δὲ is a conjunction that can be translated as "but" or "and." The word ἑκάστῳ is the dative singular of ἕκαστος, which means "each" or "every." The word ἡμῶν is the genitive plural of ἐγώ, which means "our." The phrase Ἑνὶ δὲ ἑκάστῳ ἡμῶν can be translated as "But to each of us" or "And to every one of us."

- **ἐδόθη ⸀ἡ χάρις**: The verb ἐδόθη is the third person singular aorist passive indicative of δίδωμι, which means "was given." The article ἡ is the nominative singular feminine form of ὁ, which means "the." The noun χάρις is the nominative singular form of χάρις, which means "grace" or "favor." The phrase ἐδόθη ἡ χάρις can be translated as "grace was given" or "favor was given."

- **κατὰ τὸ μέτρον τῆς δωρεᾶς τοῦ Χριστοῦ**: The preposition κατὰ can be translated as "according to" or "in accordance with." The article τὸ is the accusative singular neuter form of ὁ. The noun μέτρον is the accusative singular neuter form of μέτρον, which means "measure" or "standard." The genitive singular feminine form of δωρεά, which means "gift" or "donation." The article τοῦ is the genitive singular masculine form of ὁ. The noun Χριστοῦ is the genitive singular form of Χριστός, which means "Christ." The phrase κατὰ τὸ μέτρον τῆς δωρεᾶς τοῦ Χριστοῦ can be translated as "according to the measure of the gift of Christ."

- The literal translation of Ephesians 4:7 is: "But to each of us, grace was given according to the measure of the gift of Christ."