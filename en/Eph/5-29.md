---
version: 1
---
- **οὐδεὶς γάρ**: The word οὐδεὶς is the nominative singular of οὐδείς, which means "no one" or "nobody." The conjunction γάρ means "for" and is used to provide an explanation or reason. The phrase οὐδεὶς γάρ thus means "for no one."

- **ποτε τὴν ἑαυτοῦ σάρκα ἐμίσησεν**: The word ποτε is an adverb that means "ever" or "at any time." The article τὴν is the accusative singular feminine definite article, indicating that it modifies the noun σάρκα. The pronoun ἑαυτοῦ is the genitive singular of ἑαυτός, which means "himself" or "herself." The verb ἐμίσησεν is the third person singular aorist indicative of μισέω, meaning "he hated" or "he detested." The phrase τὴν ἑαυτοῦ σάρκα ἐμίσησεν thus means "he never hated his own flesh."

- **ἀλλὰ ἐκτρέφει καὶ θάλπει αὐτήν**: The conjunction ἀλλὰ means "but" and is used to contrast with the previous statement. The verb ἐκτρέφει is the third person singular present indicative of ἐκτρέφω, meaning "he nourishes" or "he cherishes." The verb θάλπει is the third person singular present indicative of θάλπω, meaning "he warms" or "he comforts." The pronoun αὐτήν is the accusative singular feminine pronoun, referring to the noun σάρκα. The phrase ἀλλὰ ἐκτρέφει καὶ θάλπει αὐτήν thus means "but he nourishes and cherishes it."

- **καθὼς καὶ ὁ Χριστὸς τὴν ἐκκλησίαν**: The conjunction καθὼς means "just as" or "as." The conjunction καὶ means "also" or "even." The article ὁ is the nominative singular masculine definite article, indicating that it modifies the noun Χριστὸς. The noun Χριστὸς means "Christ." The article τὴν is the accusative singular feminine definite article, indicating that it modifies the noun ἐκκλησίαν. The noun ἐκκλησίαν means "church." The phrase καθὼς καὶ ὁ Χριστὸς τὴν ἐκκλησίαν thus means "just as Christ also the church."

The literal translation of Ephesians 5:29 is: "For no one ever hated his own flesh, but he nourishes and cherishes it, just as Christ also the church."