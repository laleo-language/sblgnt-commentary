---
version: 1
---
- **ἐποικοδομηθέντες**: This is the nominative plural participle of ἐποικοδομέω, meaning "to be built upon" or "to be founded." It is describing the subject of the sentence.

- **ἐπὶ τῷ θεμελίῳ**: The preposition ἐπὶ means "upon" or "on." The article τῷ is the dative singular masculine form of ὁ, and it means "the." The noun θεμέλιον is the accusative singular of θεμέλιος, which means "foundation." So this phrase means "on the foundation."

- **τῶν ἀποστόλων καὶ προφητῶν**: The article τῶν is the genitive plural form of ὁ, and it means "of the." The nouns ἀπόστολος and προφήτης mean "apostle" and "prophet" respectively. So this phrase means "of the apostles and prophets."

- **ὄντος ἀκρογωνιαίου αὐτοῦ**: The participle ὄντος is the genitive singular masculine form of εἰμί, which means "being" or "existing." The adjective ἀκρογωνιαῖος means "cornerstone." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, which means "his" or "its." So this phrase means "being the cornerstone of his."

- **Χριστοῦ Ἰησοῦ**: The noun Χριστός means "Christ" and the noun Ἰησοῦς means "Jesus." This phrase means "of Christ Jesus."

The literal translation of the verse would be: "having been built upon the foundation of the apostles and prophets, being the cornerstone of his, Christ Jesus."