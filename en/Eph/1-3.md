---
version: 1
---
- **Εὐλογητὸς ὁ θεὸς καὶ πατὴρ τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ**: The word Εὐλογητὸς is the nominative singular masculine form of Εὐλογητός, which means "blessed" or "praiseworthy." The article ὁ is the definite article "the," and the noun θεὸς means "God." The conjunction καὶ means "and." The noun πατὴρ means "father." The genitive τοῦ κυρίου is the genitive singular masculine form of Κύριος, which means "Lord." The genitive ἡμῶν is the genitive plural of ἐγώ, which means "our." The proper name Ἰησοῦς is the genitive singular masculine form of Ἰησοῦς, which means "Jesus." The noun Χριστός means "Christ." The phrase thus means "Blessed is the God and Father of our Lord Jesus Christ."

- **ὁ εὐλογήσας ἡμᾶς**: The article ὁ is the definite article "the." The participle εὐλογήσας is the nominative singular masculine form of εὐλογέω, which means "blessing" or "praising." The pronoun ἡμᾶς is the accusative plural of ἐγώ, which means "us." The phrase thus means "the one who blessed us."

- **ἐν πάσῃ εὐλογίᾳ πνευματικῇ**: The preposition ἐν means "in." The adjective πάσῃ is the dative singular feminine form of πᾶς, which means "every" or "all." The noun εὐλογίᾳ is the dative singular of εὐλογία, which means "blessing." The adjective πνευματικῇ is the dative singular feminine form of πνευματικός, which means "spiritual." The phrase thus means "in every spiritual blessing."

- **ἐν τοῖς ἐπουρανίοις ἐν Χριστῷ**: The preposition ἐν means "in." The article τοῖς is the definite article "the." The adjective ἐπουράνιος is the dative plural masculine form of ἐπουράνιος, which means "heavenly" or "celestial." The preposition ἐν means "in." The noun Χριστός means "Christ." The phrase thus means "in the heavenly realms in Christ."

**Literal Translation:** Blessed is the God and Father of our Lord Jesus Christ, the one who blessed us in every spiritual blessing in the heavenly realms in Christ.

The sentence is not syntactically ambiguous.