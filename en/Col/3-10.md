---
version: 1
---
- **καὶ ἐνδυσάμενοι**: The word καὶ is a coordinating conjunction meaning "and." The participle ἐνδυσάμενοι is the nominative plural masculine present middle participle of ἐνδύω, "to put on" or "to clothe." The phrase καὶ ἐνδυσάμενοι means "and having put on."

- **τὸν νέον τὸν ἀνακαινούμενον**: The article τὸν is the accusative singular masculine form of the definite article ὁ, meaning "the." The adjective νέον is the accusative singular masculine form of νέος, which means "new." The article τὸν is repeated before the participle ἀνακαινούμενον to emphasize the noun it modifies. The participle ἀνακαινούμενον is the accusative singular masculine present passive participle of ἀνακαινίζω, "to renew." The phrase τὸν νέον τὸν ἀνακαινούμενον means "the new one who is being renewed."

- **εἰς ἐπίγνωσιν**: The preposition εἰς means "into" or "for." The noun ἐπίγνωσιν is the accusative singular of ἐπίγνωσις, which means "knowledge" or "recognition." The phrase εἰς ἐπίγνωσιν means "for knowledge."

- **κατʼ εἰκόνα τοῦ κτίσαντος αὐτόν**: The preposition κατά means "according to" or "in accordance with." The noun εἰκόνα is the accusative singular of εἰκών, which means "image" or "likeness." The article τοῦ is the genitive singular masculine form of the definite article ὁ, meaning "the." The participle κτίσαντος is the genitive singular masculine aorist participle of κτίζω, "to create" or "to make." The pronoun αὐτόν is the accusative singular masculine form of αὐτός, meaning "him." The phrase κατʼ εἰκόνα τοῦ κτίσαντος αὐτόν means "according to the image of the one who created him."

The literal translation of the entire verse is: "And having put on the new one who is being renewed for knowledge according to the image of the one who created him."