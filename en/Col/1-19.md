---
version: 1
---
- **ὅτι**: This is a conjunction that introduces a subordinate clause. It can be translated as "because" or "that."

- **ἐν αὐτῷ**: The preposition ἐν means "in" and is followed by the pronoun αὐτῷ, which is the dative singular of αὐτός, meaning "himself" or "in him."

- **εὐδόκησεν**: This is the third person singular aorist indicative active form of the verb εὐδοκέω, which means "to be well pleased" or "to take pleasure in."

- **πᾶν τὸ πλήρωμα**: The word πᾶν is the nominative neuter singular of πᾶς, meaning "all" or "every." The noun πλήρωμα is a neuter noun meaning "fullness" or "completeness." Together, the phrase πᾶν τὸ πλήρωμα can be translated as "all the fullness."

- **κατοικῆσαι**: This is the aorist infinitive active form of the verb κατοικέω, which means "to dwell" or "to reside."

Putting it all together, the phrase ὅτι ἐν αὐτῷ εὐδόκησεν πᾶν τὸ πλήρωμα κατοικῆσαι can be translated as "because in him all the fullness was pleased to dwell."

The entire verse, Colossians 1:19, can be translated as "because in him all the fullness was pleased to dwell."