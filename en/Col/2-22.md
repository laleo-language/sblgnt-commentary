---
version: 1
---
- **ἅ ἐστιν**: The word ἅ is the neuter plural accusative of ὅς, ἥ, ὅ which means "which" or "what." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The phrase ἅ ἐστιν together means "which is."

- **πάντα εἰς φθορὰν**: The word πάντα is the neuter plural accusative of πᾶς, πᾶσα, πᾶν which means "all" or "everything." The preposition εἰς means "to" or "for." The noun φθορὰν is the accusative singular of φθορά, which means "corruption" or "destruction." The phrase πάντα εἰς φθορὰν together means "all for destruction."

- **τῇ ἀποχρήσει**: The definite article τῇ is the feminine singular dative of ὁ, ἡ, τό which means "the." The noun ἀποχρήσει is the dative singular of ἀποχρῆσις, which means "use" or "consumption." The phrase τῇ ἀποχρήσει together means "to the use."

- **κατὰ τὰ ἐντάλματα καὶ διδασκαλίας**: The preposition κατὰ means "according to" or "in accordance with." The definite article τὰ is the neuter plural nominative of ὁ, ἡ, τό which means "the." The noun ἐντάλματα is the neuter plural nominative of ἐντολή, which means "commandment" or "precept." The conjunction καὶ means "and." The noun διδασκαλίας is the genitive singular of διδασκαλία, which means "teaching" or "doctrine." The phrase κατὰ τὰ ἐντάλματα καὶ διδασκαλίας together means "according to the commandments and teachings."

- **τῶν ἀνθρώπων**: The definite article τῶν is the masculine plural genitive of ὁ, ἡ, τό which means "the." The noun ἀνθρώπων is the genitive plural of ἄνθρωπος, which means "man" or "human." The phrase τῶν ἀνθρώπων together means "of men" or "of humans."

The phrase "ἅ ἐστιν πάντα εἰς φθορὰν τῇ ἀποχρήσει, κατὰ τὰ ἐντάλματα καὶ διδασκαλίας τῶν ἀνθρώπων" can be translated as "which is all for destruction by the use, according to the commandments and teachings of men."

In Colossians 2:22, Paul is warning against following human traditions and teachings that lead to corruption and destruction. He emphasizes the importance of adhering to God's commandments and teachings instead.