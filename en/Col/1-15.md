---
version: 1
---
- **ὅς ἐστιν εἰκὼν τοῦ θεοῦ τοῦ ἀοράτου**: The word ὅς is a relative pronoun in the nominative singular, meaning "who" or "which." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he/she/it is." The noun εἰκὼν is in the nominative singular, meaning "image" or "representation." The genitive τοῦ θεοῦ is the genitive singular of θεός, meaning "of God." The genitive τοῦ ἀοράτου is the genitive singular of ἀόρατος, meaning "invisible." The phrase thus means "who is the image of the invisible God."

- **πρωτότοκος πάσης κτίσεως**: The word πρωτότοκος is an adjective in the nominative singular, meaning "firstborn." The genitive πάσης is the genitive singular of πᾶς, meaning "of all" or "every." The noun κτίσεως is in the genitive singular, meaning "creation" or "creature." The phrase thus means "the firstborn of all creation."

The literal translation of the entire verse is: "Who is the image of the invisible God, the firstborn of all creation."