---
version: 1
---
- **ὁ ἀδικῶν**: The article ὁ is the masculine singular nominative form, indicating that the noun it modifies is the subject of the sentence. The participle ἀδικῶν is the masculine singular nominative present active participle of ἀδικέω, meaning "to do wrong" or "to act unjustly." The phrase thus means "the one who does wrong."
- **κομίσεται**: This is the third person singular future middle indicative form of κομίζω, which means "to receive" or "to obtain." The middle voice indicates that the subject is receiving the action. The phrase means "he will receive."
- **ὃ ἠδίκησεν**: The relative pronoun ὃ is the accusative singular neuter form, referring back to the object of the verb κομίσεται. The verb ἠδίκησεν is the third person singular aorist indicative active form of ἀδικέω. The phrase means "what he has done wrong."
- **καὶ οὐκ ἔστιν προσωπολημψία**: The conjunction καὶ means "and." The verb ἔστιν is the third person singular present indicative form of εἰμί, meaning "to be." The noun προσωπολημψία is the nominative singular form, meaning "partiality" or "favoritism." The phrase means "and there is no partiality."

Literal translation: "For the one who does wrong will receive what he has done wrong, and there is no partiality."

In this verse, Paul is emphasizing that God is just and impartial, and that those who act unjustly will face consequences for their actions. The phrase highlights the principle of divine justice and the absence of favoritism in God's judgment.