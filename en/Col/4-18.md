---
version: 1
---
- **Ὁ ἀσπασμὸς τῇ ἐμῇ χειρὶ Παύλου**: The noun ἀσπασμὸς means "greeting" or "salutation." It is in the nominative case and singular number. The definite article ὁ indicates that it is a specific greeting. The prepositional phrase τῇ ἐμῇ χειρὶ modifies ἀσπασμὸς and means "with my own hand." Παύλου is the genitive case of Παῦλος, the name "Paul." So the phrase literally means "The greeting with my own hand of Paul."

- **μνημονεύετέ μου τῶν δεσμῶν**: The verb μνημονεύετέ is the second person plural present imperative of μνημονεύω, which means "remember." The pronoun μου is the genitive case of ἐγώ, meaning "my." The genitive phrase τῶν δεσμῶν modifies μου and means "of the chains." So the phrase can be translated as "Remember my chains."

- **ἡ χάρις μεθʼ ⸀ὑμῶν**: The noun χάρις means "grace" or "favor." It is in the nominative case and singular number. The preposition μεθʼ indicates "with" or "among." The pronoun ὑμῶν is the genitive case of ὑμεῖς, meaning "you." So the phrase can be translated as "The grace with you" or "The favor among you."

**Literal translation**: The greeting with my own hand of Paul. Remember my chains. The grace with you.