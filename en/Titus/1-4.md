---
version: 1
---
- **Τίτῳ γνησίῳ τέκνῳ**: The word Τίτῳ is the dative singular of Τίτος, which is a proper noun meaning "Titus." The word γνησίῳ is the dative singular of γνήσιος, which means "genuine" or "legitimate." The word τέκνῳ is the dative singular of τέκνον, which means "child" or "son." The phrase thus means "to Titus, a genuine child."

- **κατὰ κοινὴν πίστιν**: The word κατὰ is a preposition that can mean "according to" or "in accordance with." The word κοινὴν is the accusative singular of κοινός, which means "common" or "shared." The word πίστιν is the accusative singular of πίστις, which means "faith" or "belief." The phrase can be translated as "according to the common faith" or "in accordance with the shared belief."

- **χάρις καὶ εἰρήνη ἀπὸ θεοῦ πατρὸς καὶ Χριστοῦ Ἰησοῦ τοῦ σωτῆρος ἡμῶν**: The word χάρις is the nominative singular of χάρις, which means "grace" or "favor." The word εἰρήνη is the nominative singular of εἰρήνη, which means "peace." The word ἀπὸ is a preposition that means "from." The word θεοῦ is the genitive singular of θεός, which means "God." The word πατρὸς is the genitive singular of πατήρ, which means "father." The word Χριστοῦ is the genitive singular of Χριστός, which means "Christ." The word Ἰησοῦ is the genitive singular of Ἰησοῦς, which means "Jesus." The word τοῦ is the genitive singular of the article ὁ, which means "the." The word σωτῆρος is the genitive singular of σωτήρ, which means "savior." The word ἡμῶν is the genitive plural of ἐγώ, which means "our." The phrase can be translated as "grace and peace from God the Father and Christ Jesus our Savior."

The literal translation of Titus 1:4 is "To Titus, a genuine child, according to the common faith; grace and peace from God the Father and Christ Jesus our Savior."