---
version: 1
---
- **Ταῦτα λάλει**: The word Ταῦτα is the accusative plural of οὗτος, meaning "these" or "these things." The verb λάλει is the present imperative singular of λαλέω, meaning "speak" or "say." The phrase thus means "Speak these things."

- **καὶ παρακάλει**: The conjunction καὶ connects this phrase to the previous one. The verb παρακάλει is the present imperative singular of παρακαλέω, meaning "exhort" or "encourage." The phrase means "And exhort."

- **καὶ ἔλεγχε**: The conjunction καὶ connects this phrase to the previous one. The verb ἔλεγχε is the present imperative singular of ἐλέγχω, meaning "rebuke" or "reprove." The phrase means "And rebuke."

- **μετὰ πάσης ἐπιταγῆς**: The preposition μετὰ means "with" or "in the company of." The noun πάσης is the genitive singular of πᾶς, meaning "every" or "all." The noun ἐπιταγῆς is the genitive singular of ἐπιταγή, meaning "command" or "directive." The phrase means "With every command."

- **μηδείς σου περιφρονείτω**: The word μηδείς is the nominative singular of μηδείς, meaning "no one" or "none." The pronoun σου is the genitive singular of σύ, meaning "you." The verb περιφρονείτω is the present imperative singular of περιφρονέω, meaning "despise" or "disregard." The phrase means "Let no one despise you."

The literal translation of the verse is: "Speak these things and exhort and rebuke with every command; let no one despise you."