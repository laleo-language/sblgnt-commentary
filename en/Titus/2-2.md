---
version: 1
---
- **πρεσβύτας**: This is the accusative plural of the noun πρεσβύτης, which means "elder" or "older man." The plural form indicates that it is referring to multiple elders.

- **νηφαλίους**: This is the accusative plural of the adjective νηφάλιος, which means "sober" or "temperate." It describes the state or condition of the elders.

- **εἶναι**: This is the present infinitive of the verb εἰμί, which means "to be." In this context, it serves as a verb of existence, indicating that the state or condition of being sober or temperate is what the elders should be.

- **σεμνούς**: This is the accusative plural of the adjective σεμνός, which means "dignified" or "honorable." It describes the character or demeanor of the elders.

- **σώφρονας**: This is the accusative plural of the adjective σώφρων, which means "self-controlled" or "prudent." It describes the behavior or mindset of the elders.

- **ὑγιαίνοντας**: This is the accusative plural of the participle ὑγιαίνων, which means "being healthy" or "being sound." It describes the physical and spiritual well-being of the elders.

- **τῇ πίστει, τῇ ἀγάπῃ, τῇ ὑπομονῇ**: These are three dative singular nouns, all preceded by the article τῇ (meaning "the"). The noun πίστις means "faith," ἀγάπη means "love," and ὑπομονή means "patience" or "endurance." These three nouns indicate the areas in which the elders should exhibit their soundness and health.

In summary, this verse is instructing elders to be sober, dignified, self-controlled, healthy in faith, love, and patience.

Literal translation: "Elders to be sober, dignified, self-controlled, healthy in faith, love, and patience."