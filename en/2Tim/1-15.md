---
version: 1
---
- **Οἶδας τοῦτο**: The verb Οἶδας is the second person singular present indicative of οἶδα, meaning "you know." The pronoun τοῦτο is the accusative singular neuter of οὗτος, meaning "this." The phrase thus means "You know this."

- **ὅτι ἀπεστράφησάν με πάντες οἱ ἐν τῇ Ἀσίᾳ**: The conjunction ὅτι introduces a subordinate clause. The verb ἀπεστράφησάν is the third person plural aorist indicative passive of ἀποστρέφω, meaning "they turned away." The pronoun με is the accusative singular of ἐγώ, meaning "me." The adjective πάντες is the nominative plural masculine of πᾶς, meaning "all." The article οἱ is the nominative plural masculine of ὁ, meaning "the." The preposition ἐν is used with the dative case, and the noun τῇ Ἀσίᾳ is the dative singular feminine of Ἀσία, meaning "Asia." The phrase thus means "that all those in Asia turned away from me."

- **ὧν ἐστιν Φύγελος καὶ Ἑρμογένης**: The relative pronoun ὧν is the genitive plural masculine of ὅς, meaning "who/whom." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The noun Φύγελος is the nominative singular masculine of Φύγελος, which is a personal name. The conjunction καὶ means "and." The noun Ἑρμογένης is the nominative singular masculine of Ἑρμογένης, which is also a personal name. The phrase thus means "of whom are Phygelus and Hermogenes."

The entire verse can be literally translated as: "You know this, that all those in Asia turned away from me, of whom are Phygelus and Hermogenes."