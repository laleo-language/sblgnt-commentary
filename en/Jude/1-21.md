---
version: 5
---
- **ἑαυτοὺς**: This is the reflexive pronoun ἑαυτοὺς, in the accusative plural form. It means "yourselves."
- **ἐν ἀγάπῃ θεοῦ**: The preposition ἐν means "in." The noun ἀγάπη means "love." The genitive form θεοῦ means "of God." So this phrase means "in the love of God."
- **τηρήσατε**: This is the second person plural aorist imperative of the verb τηρέω, which means "to keep" or "to guard." The imperative form here is a command, so it means "keep" or "guard."
- **προσδεχόμενοι**: This is the present middle participle of the verb προσδέχομαι, which means "to await" or "to expect." The participle form here indicates ongoing action, so it means "while expecting" or "while awaiting."
- **τὸ ἔλεος τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ**: The article τὸ followed by the noun ἔλεος means "the mercy." The genitive phrase τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ means "of our Lord Jesus Christ." So this phrase means "the mercy of our Lord Jesus Christ."
- **εἰς ζωὴν αἰώνιον**: The preposition εἰς means "into" or "for." The noun ζωὴν means "life." The adjective αἰώνιον means "eternal" or "everlasting." So this phrase means "into eternal life."

The literal translation of the entire verse is: "Yourselves in the love of God keep, while expecting the mercy of our Lord Jesus Christ into eternal life."