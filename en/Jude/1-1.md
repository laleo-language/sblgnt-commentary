---
version: 7
---
- **Ἰούδας Ἰησοῦ Χριστοῦ δοῦλος**: The word Ἰούδας is the nominative singular of Ἰούδας, which is a proper noun and means "Jude." The phrase Ἰησοῦ Χριστοῦ is in the genitive case and means "of Jesus Christ." The word δοῦλος is the nominative singular of δοῦλος, which means "servant" or "slave." So, the phrase means "Jude, a servant of Jesus Christ."

- **ἀδελφὸς δὲ Ἰακώβου**: The word ἀδελφὸς is the nominative singular of ἀδελφός, which means "brother." The word δὲ is a conjunction that means "but" or "and." The proper noun Ἰακώβου is in the genitive case and means "of James." So, the phrase means "and brother of James."

- **τοῖς ἐν θεῷ πατρὶ ἠγαπημένοις**: The word τοῖς is the dative plural of the definite article ὁ, which means "the." The prepositional phrase ἐν θεῷ means "in God." The word πατρὶ is the dative singular of πατήρ, which means "father." The verb ἠγαπημένοις is the dative plural of the perfect participle ἀγαπάω, which means "loved." So, the phrase means "to the loved ones in God the Father."

- **καὶ Ἰησοῦ Χριστῷ τετηρημένοις κλητοῖς**: The conjunction καὶ means "and." The proper noun Ἰησοῦ Χριστῷ is in the dative case and means "to Jesus Christ." The verb τετηρημένοις is the dative plural of the perfect participle τηρέω, which means "kept" or "preserved." The noun κλητοῖς is the dative plural of the participle κλητός, which means "called." So, the phrase means "and kept ones for Jesus Christ."

- **Ἰούδας Ἰησοῦ Χριστοῦ δοῦλος, ἀδελφὸς δὲ Ἰακώβου, τοῖς ἐν θεῷ πατρὶ ἠγαπημένοις καὶ Ἰησοῦ Χριστῷ τετηρημένοις κλητοῖς**: Jude introduces himself as a servant of Jesus Christ and a brother of James. He addresses his letter to the loved ones in God the Father and the kept ones for Jesus Christ.

Literal translation: "Jude, a servant of Jesus Christ and brother of James, to the loved ones in God the Father and the kept ones for Jesus Christ."