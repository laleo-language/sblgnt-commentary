---
version: 1
---
- **Τὸν μὲν πρῶτον λόγον**: The word Τὸν is the accusative singular of the definite article ὁ, meaning "the." The word μὲν is an adverb indicating contrast or comparison, often translated as "on the one hand" or "indeed." The word πρῶτον is the accusative singular of the adjective πρῶτος, meaning "first." The word λόγον is the accusative singular of the noun λόγος, meaning "word" or "message." The phrase thus means "the first word" or "the initial message."

- **ἐποιησάμην περὶ πάντων**: The verb ἐποιησάμην is the first person singular aorist indicative middle of ποιέω, meaning "I made" or "I composed." The preposition περὶ means "about" or "concerning." The word πάντων is the genitive plural of the adjective πᾶς, meaning "all" or "everything." The phrase thus means "I made about everything" or "I composed concerning all."

- **ὦ Θεόφιλε**: The word ὦ is an interjection used to address someone, often translated as "O" or "oh." The word Θεόφιλε is the vocative singular of the proper noun Θεόφιλος, meaning "Theophilus." The phrase is an address to Theophilus, meaning "O Theophilus."

- **ὧν ἤρξατο ὁ Ἰησοῦς ποιεῖν τε καὶ διδάσκειν**: The relative pronoun ὧν is the genitive plural of ὅς, meaning "of which" or "of whom." The verb ἤρξατο is the third person singular aorist indicative middle of ἄρχομαι, meaning "he began" or "he started." The definite article ὁ is the nominative singular of the article ὁ, meaning "the." The noun Ἰησοῦς is the nominative singular of the proper noun Ἰησοῦς, meaning "Jesus." The verb ποιεῖν is the present infinitive of ποιέω, meaning "to do" or "to make." The verb διδάσκειν is the present infinitive of διδάσκω, meaning "to teach." The phrase can be translated as "of which Jesus began to do and teach" or "which Jesus started doing and teaching."

- The literal translation of the verse is: "The first word I made about everything, O Theophilus, of which Jesus began to do and teach."