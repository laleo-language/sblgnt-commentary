---
version: 1
---
- **γενομένης δὲ στάσεως καὶ ζητήσεως οὐκ ὀλίγης**: The participle γενομένης is the genitive singular feminine of γίγνομαι, meaning "having taken place" or "having occurred." The noun στάσεως is the genitive singular of στάσις, which means "dispute" or "contention." The noun ζητήσεως is the genitive singular of ζήτησις, meaning "inquiry" or "question." The adjective οὐκ ὀλίγης means "not small" or "not insignificant." The phrase thus means "a not insignificant dispute and inquiry having taken place."

- **τῷ Παύλῳ καὶ τῷ Βαρναβᾷ**: The dative singular nouns τῷ Παύλῳ and τῷ Βαρναβᾷ are the forms of Παῦλος and Βαρναβᾶς, which are the names "Paul" and "Barnabas" respectively. The phrase means "to Paul and to Barnabas."

- **πρὸς αὐτοὺς**: The preposition πρὸς means "to" or "towards." The pronoun αὐτοὺς is the accusative plural form of αὐτός, meaning "them." The phrase means "to them."

- **ἔταξαν ἀναβαίνειν Παῦλον καὶ Βαρναβᾶν καί τινας ἄλλους ἐξ αὐτῶν**: The verb ἔταξαν is the third person plural aorist indicative of τάσσω, meaning "they appointed" or "they decided." The infinitive ἀναβαίνειν is the present infinitive of ἀναβαίνω, meaning "to go up" or "to go to." The accusative nouns Παῦλον and Βαρναβᾶν are the forms of Παῦλος and Βαρναβᾶς mentioned earlier. The pronoun τινας is the accusative plural form of τις, meaning "some" or "certain." The preposition ἐξ means "from" or "out of." The pronoun αὐτῶν is the genitive plural form of αὐτός, meaning "of them." The phrase means "they appointed Paul and Barnabas and some others from among them to go up."

- **πρὸς τοὺς ἀποστόλους καὶ πρεσβυτέρους**: The preposition πρὸς means "to" or "towards." The accusative nouns τοὺς ἀποστόλους and πρεσβυτέρους are the forms of ἀπόστολος and πρεσβύτερος, which mean "apostles" and "elders" respectively. The phrase means "to the apostles and elders."

- **εἰς Ἰερουσαλὴμ**: The preposition εἰς means "to" or "into." The accusative noun Ἰερουσαλὴμ is the form of Ἱεροσόλυμα, which means "Jerusalem." The phrase means "to Jerusalem."

- **περὶ τοῦ ζητήματος τούτου**: The preposition περὶ means "about" or "concerning." The genitive noun τοῦ ζητήματος is the form of ζήτημα, which means "question" or "issue." The demonstrative pronoun τούτου means "this." The phrase means "about this question."

Literal translation: "And a not insignificant dispute and inquiry having taken place, they appointed Paul and Barnabas and some others from among them to go up to them, to the apostles and elders, in Jerusalem, about this question."

This verse describes a dispute and inquiry that arose among the early Christians. To resolve the matter, Paul, Barnabas, and some others were appointed to go to Jerusalem and discuss the question with the apostles and elders there.