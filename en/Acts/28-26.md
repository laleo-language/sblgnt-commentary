---
version: 1
---
- **λέγων**: The word λέγων is the present active participle of λέγω, which means "saying" or "speaking." It is in the nominative singular masculine form.

- **Πορεύθητι**: The word Πορεύθητι is the second person singular aorist imperative of πορεύομαι, which means "go" or "proceed." It is in the second person singular form, commanding someone to go.

- **πρὸς τὸν λαὸν τοῦτον**: The phrase πρὸς τὸν λαὸν τοῦτον means "to this people." The preposition πρὸς indicates movement towards or direction. The article τὸν is the accusative singular masculine form of the definite article, meaning "the." The noun λαὸν is the accusative singular masculine form of λαός, which means "people." The word τοῦτον is the accusative singular masculine form of οὗτος, meaning "this."

- **καὶ εἰπόν**: The phrase καὶ εἰπόν means "and saying." The conjunction καὶ means "and." The verb εἰπόν is the aorist active participle of λέγω, meaning "said" or "having said." It is in the nominative singular masculine form.

- **Ἀκοῇ ἀκούσετε**: The phrase Ἀκοῇ ἀκούσετε means "with hearing you will hear." The noun Ἀκοῇ is the dative singular feminine form of ἀκοή, which means "hearing." The verb ἀκούσετε is the second person plural future indicative of ἀκούω, meaning "you will hear."

- **καὶ οὐ μὴ συνῆτε**: The phrase καὶ οὐ μὴ συνῆτε means "and you will not understand." The conjunction καὶ means "and." The adverb οὐ means "not." The particle μὴ is a negation particle used with future verbs, indicating a negative future action. The verb συνῆτε is the second person plural future indicative of συνίημι, meaning "you will understand."

- **καὶ βλέποντες βλέψετε**: The phrase καὶ βλέποντες βλέψετε means "and seeing you will see." The conjunction καὶ means "and." The participle βλέποντες is the present active participle of βλέπω, meaning "seeing." It is in the nominative plural masculine form. The verb βλέψετε is the second person plural future indicative of βλέπω, meaning "you will see."

- **καὶ οὐ μὴ ἴδητε**: The phrase καὶ οὐ μὴ ἴδητε means "and you will not perceive." The conjunction καὶ means "and." The adverb οὐ means "not." The particle μὴ is a negation particle used with future verbs, indicating a negative future action. The verb ἴδητε is the second person plural future indicative of ὁράω, meaning "you will perceive" or "you will see."

The entire verse, Acts 28:26, can be literally translated as: "saying, 'Go to this people and say, "You will hear with your ears, and you will not understand; and seeing, you will see and not perceive."'"