---
version: 1
---
- **Γενομένης δὲ ἡμέρας**: The participle γενομένης is the genitive singular feminine of γίνομαι, meaning "becoming" or "having happened." The noun ἡμέρας is the genitive singular of ἡμέρα, meaning "day." The phrase means "when the day had come."

- **ποιήσαντες συστροφὴν οἱ Ἰουδαῖοι**: The participle ποιήσαντες is the nominative plural masculine of ποιέω, meaning "having made" or "causing." The noun συστροφὴν is the accusative singular of συστροφή, meaning "conspiracy" or "plot." The article οἱ is the nominative plural masculine definite article, meaning "the." The noun Ἰουδαῖοι is the nominative plural masculine of Ἰουδαῖος, meaning "Jews." The phrase means "the Jews having made a conspiracy."

- **ἀνεθεμάτισαν ἑαυτοὺς**: The verb ἀνεθεμάτισαν is the third person plural aorist indicative of ἀναθεματίζω, meaning "they cursed" or "they swore." The reflexive pronoun ἑαυτοὺς is the accusative plural of ἑαυτός, meaning "themselves." The phrase means "they cursed themselves."

- **λέγοντες μήτε φαγεῖν μήτε πιεῖν**: The participle λέγοντες is the nominative plural masculine of λέγω, meaning "saying." The conjunction μήτε is a coordinating conjunction meaning "neither." The verb φαγεῖν is the aorist infinitive of ἐσθίω, meaning "to eat." The verb πιεῖν is the aorist infinitive of πίνω, meaning "to drink." The phrase means "saying neither to eat nor to drink."

- **ἕως οὗ ἀποκτείνωσιν τὸν Παῦλον**: The conjunction ἕως is a subordinating conjunction meaning "until." The pronoun οὗ is the genitive singular of ὅς, meaning "which" or "whom." The verb ἀποκτείνωσιν is the third person plural aorist subjunctive of ἀποκτείνω, meaning "they might kill." The article τὸν is the accusative singular masculine definite article, meaning "the." The noun Παῦλον is the accusative singular of Παῦλος, meaning "Paul." The phrase means "until they might kill Paul."

The literal translation of the entire verse is: "When the day had come, the Jews made a conspiracy, cursing themselves and saying neither to eat nor to drink until they might kill Paul."