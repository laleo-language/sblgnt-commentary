---
version: 1
---
- **ἰδόντες δὲ οἱ Ἰουδαῖοι**: The participle ἰδόντες is the nominative plural masculine of ὁράω, which means "seeing." The article οἱ is the nominative plural masculine of ὁ, which means "the." The noun Ἰουδαῖοι is the nominative plural masculine of Ἰουδαῖος, which means "Jews." The phrase thus means "the Jews, seeing."

- **τοὺς ὄχλους**: The article τοὺς is the accusative plural masculine of ὁ. The noun ὄχλους is the accusative plural masculine of ὄχλος, which means "crowds" or "multitudes." The phrase thus means "the crowds."

- **ἐπλήσθησαν ζήλου**: The verb ἐπλήσθησαν is the third person plural aorist passive indicative of πίμπλημι, which means "they were filled." The noun ζήλου is the genitive singular masculine of ζῆλος, which means "zeal." The phrase thus means "they were filled with zeal."

- **καὶ ἀντέλεγον**: The conjunction καὶ means "and." The verb ἀντέλεγον is the third person plural imperfect indicative of ἀντιλέγω, which means "they contradicted" or "they argued." The phrase thus means "and they argued."

- **τοῖς ⸀ὑπὸ Παύλου ⸀λαλουμένοις ⸀βλασφημοῦντες**: The article τοῖς is the dative plural masculine of ὁ. The preposition ὑπὸ means "by" or "under." The proper noun Παύλου is the genitive singular masculine of Παῦλος, which means "Paul." The participle λαλουμένοις is the dative plural masculine of λαλέω, which means "speaking." The participle βλασφημοῦντες is the nominative plural masculine of βλασφημέω, which means "blaspheming." The phrase can be understood as "arguing against what was being spoken by Paul" or "contradicting the words spoken by Paul."

The literal translation of Acts 13:45 is: "The Jews, seeing the crowds, were filled with zeal and argued against the words spoken by Paul, blaspheming."