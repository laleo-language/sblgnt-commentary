---
version: 1
---
- **τότε ἰδὼν**: The word τότε means "then" or "at that time." The verb ἰδὼν is the aorist participle of ὁράω, which means "to see." The phrase thus means "then seeing."

- **ὁ ἀνθύπατος**: The article ὁ is the masculine singular nominative form, indicating that the noun it modifies is also masculine singular. The noun ἀνθύπατος means "proconsul" or "governor." So the phrase means "the proconsul."

- **τὸ γεγονὸς**: The article τὸ is the neuter singular accusative form, indicating that the noun it modifies is also neuter singular and is the direct object of the verb. The noun γεγονὸς is the perfect participle of γίνομαι, which means "to become" or "to happen." The phrase thus means "the thing that had happened."

- **ἐπίστευσεν**: The verb ἐπίστευσεν is the third person singular aorist indicative of πιστεύω, which means "to believe" or "to have faith." The phrase thus means "he believed."

- **ἐκπλησσόμενος**: The verb ἐκπλησσόμενος is the present participle of ἐκπλήσσω, which means "to be amazed" or "to be astonished." The participle is in the nominative singular form, indicating that it is the subject of the verb. The phrase thus means "being amazed."

- **ἐπὶ τῇ διδαχῇ τοῦ κυρίου**: The preposition ἐπὶ means "upon" or "about." The article τῇ is the feminine singular dative form, indicating that the noun it modifies is also feminine singular and is in the dative case. The noun διδαχῇ means "teaching" or "instruction." The genitive noun τοῦ κυρίου means "of the Lord" or "of the master." The phrase thus means "upon the teaching of the Lord."

The literal translation of the entire verse is: "Then seeing, the proconsul believed, being amazed upon the teaching of the Lord."