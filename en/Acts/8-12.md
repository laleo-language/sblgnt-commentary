---
version: 1
---
- **ὅτε δὲ ἐπίστευσαν τῷ Φιλίππῳ εὐαγγελιζομένῳ**: The word ὅτε is a conjunction that means "when." The verb ἐπίστευσαν is the third person plural aorist indicative of πιστεύω, meaning "they believed." The dative noun τῷ Φιλίππῳ is the dative singular of Φίλιππος, which is a personal name. The participle εὐαγγελιζομένῳ is the dative singular masculine present middle or passive participle of εὐαγγελίζομαι, meaning "proclaiming the good news." The phrase can be translated as "when they believed Philip who was proclaiming the good news."

- **περὶ τῆς βασιλείας τοῦ θεοῦ καὶ τοῦ ὀνόματος Ἰησοῦ Χριστοῦ**: The preposition περὶ means "about" or "concerning." The genitive noun phrase τῆς βασιλείας τοῦ θεοῦ is in the genitive case, which indicates possession or source, and can be translated as "of the kingdom of God." The genitive noun phrase τοῦ ὀνόματος Ἰησοῦ Χριστοῦ is also in the genitive case and can be translated as "of the name of Jesus Christ." The phrase can be translated as "about the kingdom of God and the name of Jesus Christ."

- **ἐβαπτίζοντο ἄνδρες τε καὶ γυναῖκες**: The verb ἐβαπτίζοντο is the third person plural imperfect indicative middle of βαπτίζω, meaning "they were baptizing." The noun ἄνδρες is the nominative plural of ἀνήρ, meaning "men." The conjunction τε is used to connect the noun ἄνδρες with the following noun. The noun γυναῖκες is the nominative plural of γυνή, meaning "women." The phrase can be translated as "men and women were being baptized."

Literal translation: "When they believed Philip who was proclaiming the good news about the kingdom of God and the name of Jesus Christ, men and women were being baptized."