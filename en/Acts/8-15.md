---
version: 1
---
- **οἵτινες καταβάντες**: The word οἵτινες is a relative pronoun that means "who" or "which." It agrees with the subject of the sentence, which is implied but not stated. The verb καταβάντες is the nominative plural participle of καταβαίνω, meaning "to go down." The phrase can be translated as "who, having gone down."

- **προσηύξαντο περὶ αὐτῶν**: The verb προσηύξαντο is the third person plural aorist middle indicative of προσεύχομαι, meaning "they prayed." The preposition περὶ means "about" or "concerning." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "of them." The phrase can be translated as "they prayed about them."

- **ὅπως λάβωσιν πνεῦμα ἅγιον**: The conjunction ὅπως introduces a purpose clause and means "so that" or "in order that." The verb λάβωσιν is the third person plural aorist subjunctive of λαμβάνω, meaning "they might receive." The noun πνεῦμα is the accusative singular of πνεῦμα, meaning "spirit." The adjective ἅγιον means "holy." The phrase can be translated as "so that they might receive the Holy Spirit."

- The literal translation of the entire verse is: "who, having gone down, they prayed about them so that they might receive the Holy Spirit."