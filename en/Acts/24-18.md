---
version: 1
---
- **ἐν αἷς**: The preposition ἐν means "in" and αἷς is the plural feminine dative form of ὅς, which means "which" or "that." The phrase ἐν αἷς can be translated as "in which" or "in them."

- **εὗρόν με**: The verb εὑρίσκω is the first person singular aorist indicative of εὑρίσκω, which means "I found." The pronoun με is the accusative form of ἐγώ, meaning "me." The phrase εὗρόν με can be translated as "found me."

- **ἡγνισμένον ἐν τῷ ἱερῷ**: The verb ἡγνίζω is the perfect participle passive accusative masculine singular of ἁγνίζω, which means "I purify" or "I cleanse." The preposition ἐν means "in" and τῷ is the definite article in the dative singular form, meaning "the." The noun ἱερόν is the accusative singular form of ἱερόν, which means "temple" or "sanctuary." The phrase ἡγνισμένον ἐν τῷ ἱερῷ can be translated as "purified in the temple."

- **οὐ μετὰ ὄχλου οὐδὲ μετὰ θορύβου**: The adverb οὐ means "not." The preposition μετὰ means "with." The noun ὄχλος is the genitive singular form of ὄχλος, which means "crowd" or "multitude." The noun θόρυβος is the genitive singular form of θόρυβος, which means "uproar" or "disturbance." The phrase οὐ μετὰ ὄχλου οὐδὲ μετὰ θορύβου can be translated as "not with a crowd nor with an uproar."

The literal translation of Acts 24:18 is: "in which they found me purified in the temple, not with a crowd nor with an uproar."