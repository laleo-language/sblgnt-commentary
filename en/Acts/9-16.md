---
version: 1
---
- **ἐγὼ γὰρ**: The word ἐγὼ is the first person singular pronoun, meaning "I." The word γὰρ is a conjunction meaning "for" or "because." The phrase thus means "for I."

- **ὑποδείξω αὐτῷ**: The verb ὑποδείκνυμι is the first person singular future indicative of ὑποδείκνυμι, meaning "I will show" or "I will reveal." The pronoun αὐτῷ is the dative singular form of αὐτός, meaning "to him." The phrase thus means "I will show to him."

- **ὅσα δεῖ αὐτὸν**: The word ὅσα is a relative pronoun meaning "as many as" or "whatever." The verb δεῖ is the third person singular present indicative of δέω, meaning "it is necessary." The pronoun αὐτὸν is the accusative singular form of αὐτός, meaning "him." The phrase thus means "whatever is necessary for him."

- **ὑπὲρ τοῦ ὀνόματός μου**: The preposition ὑπὲρ means "for the sake of" or "on behalf of." The article τοῦ is the genitive singular form of ὁ, meaning "the." The noun ὀνόμα means "name." The pronoun μου is the genitive singular form of ἐγώ, meaning "my." The phrase thus means "for the sake of my name."

- **παθεῖν**: The verb παθεῖν is the aorist infinitive of πάσχω, meaning "to suffer" or "to experience." The phrase thus means "to suffer."

The entire verse can be literally translated as: "For I will show him whatever is necessary for him to suffer on behalf of my name."