---
version: 1
---
- **καὶ ἰδοὺ**: καὶ is a conjunction meaning "and," and ἰδοὺ is an interjection meaning "behold" or "look." Together, they introduce a new element or event.

- **ἐξαυτῆς**: ἐξαυτῆς is an adverb derived from αὐτός, meaning "immediately" or "straightaway."

- **τρεῖς ἄνδρες**: τρεῖς is the nominative plural of τρεῖς, meaning "three." ἄνδρες is the nominative plural of ἀνήρ, meaning "men." Together, they form the subject of the verb.

- **ἐπέστησαν**: ἐπέστησαν is the third person plural aorist indicative active of ἐφίστημι, meaning "they stood." It is the main verb of the sentence.

- **ἐπὶ τὴν οἰκίαν**: ἐπὶ is a preposition meaning "upon" or "to." τὴν is the accusative singular of ὁ, meaning "the." οἰκίαν is the accusative singular of οἰκία, meaning "house." Together, they form a prepositional phrase that indicates the location where the men stood.

- **ἐν ᾗ ⸀ἦμεν**: ἐν is a preposition meaning "in." ᾗ is the dative singular feminine of ὅς, meaning "which" or "where." ⸀ἦμεν is the first person plural imperfect indicative active of εἰμί, meaning "we were." Together, they form a relative clause that describes the location of the house.

- **ἀπεσταλμένοι ἀπὸ Καισαρείας πρός με**: ἀπεσταλμένοι is the nominative plural masculine perfect passive participle of ἀποστέλλω, meaning "sent." ἀπὸ is a preposition meaning "from." Καισαρείας is the genitive singular of Καισάρεια, meaning "Caesarea." πρός is a preposition meaning "to" or "towards." με is the accusative singular of ἐγώ, meaning "me." Together, they form a participial phrase that describes the men as being sent from Caesarea to the speaker.

Literal translation: "And behold, immediately three men stood upon the house where we were, sent from Caesarea to me."

The verse describes the sudden arrival of three men who were sent from Caesarea to the speaker's location.