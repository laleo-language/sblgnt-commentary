---
version: 1
---
- **Τότε ὑπέστρεψαν εἰς Ἰερουσαλὴμ**: The word Τότε means "then" and ὑπέστρεψαν is the third person plural aorist indicative of ὑποστρέφω, which means "they returned." The preposition εἰς means "to" and Ἰερουσαλὴμ is the accusative singular form of Ἱερουσαλήμ, which means "Jerusalem." The phrase thus means "Then they returned to Jerusalem."

- **ἀπὸ ὄρους τοῦ καλουμένου Ἐλαιῶνος**: The preposition ἀπὸ means "from" and ὄρους is the genitive singular form of ὄρος, which means "mountain." The phrase τοῦ καλουμένου is a genitive construction meaning "called" or "named." Ἐλαιῶνος is the genitive singular form of Ἐλαίων, which means "of Olives." The phrase thus means "from the mountain called Olives."

- **ὅ ἐστιν ἐγγὺς Ἰερουσαλὴμ**: The relative pronoun ὅ means "which" and ἐστιν is the third person singular present indicative of εἰμί, which means "is." The phrase ἐγγὺς is an adverb meaning "near" or "close to." The phrase thus means "which is near Jerusalem."

- **σαββάτου ἔχον ὁδόν**: The noun σαββάτου is the genitive singular form of σάββατον, which means "Sabbath." The participle ἔχον is the present active accusative singular form of ἔχω, which means "having" or "possessing." The noun ὁδόν is the accusative singular form of ὁδός, which means "way" or "road." The phrase thus means "having a Sabbath day's journey."

Overall, the verse describes the disciples returning to Jerusalem from the mountain called Olives, which is near Jerusalem, and they had a Sabbath day's journey.

Literal translation: Then they returned to Jerusalem from the mountain called Olives, which is near Jerusalem, having a Sabbath day's journey.