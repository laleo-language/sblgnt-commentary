---
version: 1
---
- **οἱ δὲ εἶπαν**: The article οἱ is the nominative plural of ὁ, meaning "the." The conjunction δὲ means "and" or "but." The verb εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said." The phrase thus means "they said."

- **Πίστευσον ἐπὶ τὸν κύριον Ἰησοῦν**: The verb Πίστευσον is the second person singular aorist imperative of πιστεύω, meaning "believe." The preposition ἐπὶ means "on" or "in." The article τὸν is the accusative singular of ὁ. The noun κύριον is the accusative singular of κύριος, meaning "Lord." The name Ἰησοῦν is the accusative singular of Ἰησοῦς, meaning "Jesus." The phrase thus means "Believe in/on the Lord Jesus."

- **καὶ σωθήσῃ**: The conjunction καὶ means "and." The verb σωθήσῃ is the second person singular future passive indicative of σῴζω, meaning "you will be saved." The phrase thus means "and you will be saved."

- **σὺ καὶ ὁ οἶκός σου**: The pronoun σὺ is the second person singular pronoun, meaning "you." The conjunction καὶ means "and." The article ὁ is the nominative singular of ὁ. The noun οἶκός is the nominative singular of οἶκος, meaning "house." The pronoun σου is the genitive singular of σύ, meaning "your." The phrase thus means "you and your house."

Literal Translation: "They said, 'Believe in/on the Lord Jesus, and you will be saved, you and your house.'"

This verse is a response to a question from a jailer who asked Paul and Silas, "Sirs, what must I do to be saved?" The phrase "Believe in/on the Lord Jesus" emphasizes that faith in Jesus is the key to salvation, and the promise of salvation extends not only to the jailer but also to his household.