---
version: 1
---
- **εἰς γὰρ χολὴν πικρίας**: The preposition εἰς means "into" or "for" and takes the accusative case. The noun χολή means "gall" or "bitterness" and is in the accusative singular. The genitive noun πικρίας means "of bitterness." So this phrase can be translated as "into the gall of bitterness."

- **καὶ σύνδεσμον ἀδικίας**: The conjunction καὶ means "and." The noun σύνδεσμος means "bond" or "fetter" and is in the accusative singular. The genitive noun ἀδικίας means "of injustice." So this phrase can be translated as "and a bond of injustice."

- **ὁρῶ σε ὄντα**: The verb ὁρῶ is the first person singular present indicative of ὁράω, meaning "I see." The pronoun σε is the accusative singular form of σύ, meaning "you." The present participle ὄντα is from the verb εἰμί, meaning "to be." So this phrase can be translated as "I see you being."

The literal translation of the verse is: "For I see you being into the gall of bitterness and a bond of injustice."