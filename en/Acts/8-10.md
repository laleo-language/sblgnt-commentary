---
version: 1
---
- **ᾧ προσεῖχον**: The word ᾧ is the dative singular form of the relative pronoun ὅς, ἥ, ὅ, which means "who, which, that." The verb προσεῖχον is the third person plural imperfect indicative active of προσέχω, meaning "they paid attention to" or "they gave heed to." The phrase thus means "to whom they all paid attention."

- **πάντες**: This is the nominative plural form of the adjective πᾶς, πᾶσα, πᾶν, meaning "all" or "everyone." It is used here to emphasize that everyone was paying attention.

- **ἀπὸ μικροῦ ἕως μεγάλου**: The preposition ἀπὸ means "from" and is followed by the genitive case. The adjective μικροῦ means "small" and the adjective μεγάλου means "great" or "big." The phrase ἀπὸ μικροῦ ἕως μεγάλου means "from small to great" or "from the least to the greatest."

- **λέγοντες**: This is the nominative plural participle present active of the verb λέγω, meaning "they were saying" or "saying."

- **Οὗτός ἐστιν ἡ Δύναμις τοῦ θεοῦ**: The pronoun Οὗτός means "this." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The noun Δύναμις means "power" or "might." The genitive τοῦ θεοῦ means "of God." The phrase Οὗτός ἐστιν ἡ Δύναμις τοῦ θεοῦ means "This is the power of God."

- **ἡ καλουμένη Μεγάλη**: The article ἡ is the feminine nominative singular form of the definite article, meaning "the." The verb καλουμένη is the feminine nominative singular participle present passive of the verb καλέω, meaning "being called" or "called." The adjective Μεγάλη means "great" or "big." The phrase ἡ καλουμένη Μεγάλη means "the one called Great."

The entire verse can be literally translated as: "To whom they all gave attention, from small to great, saying, 'This is the power of God, the one called Great.'"