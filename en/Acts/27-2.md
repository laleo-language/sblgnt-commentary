---
version: 1
---
- **ἐπιβάντες δὲ πλοίῳ Ἀδραμυττηνῷ**: The participle ἐπιβάντες is the aorist active nominative plural masculine participle of ἐπιβαίνω, meaning "having embarked" or "having gone on board." The noun πλοίῳ is the dative singular of πλοῖον, which means "ship." The adjective Ἀδραμυττηνῷ is the dative singular of Ἀδραμύττηνος, referring to the ship's destination, Adramyttium. The phrase thus means "having embarked on a ship bound for Adramyttium."

- **μέλλοντι πλεῖν**: The participle μέλλοντι is the present active dative singular masculine participle of μέλλω, meaning "about to" or "intending to." The verb πλεῖν is the present active infinitive of πλέω, meaning "to sail." The phrase can be understood as "intending to sail."

- **εἰς τοὺς κατὰ τὴν Ἀσίαν τόπους**: The preposition εἰς means "to" or "towards." The article τοὺς is the accusative plural of ὁ, "the." The preposition κατὰ means "according to" or "throughout." The article τὴν is the accusative singular of ὁ. The noun Ἀσίαν is the accusative singular of Ἀσία, referring to the region of Asia. The noun τόπους is the accusative plural of τόπος, meaning "places" or "regions." The phrase can be translated as "to the places in Asia" or "to the regions of Asia."

- **ἀνήχθημεν**: The verb ἀνήχθημεν is the first person plural aorist passive indicative of ἀνάγω, meaning "we were brought" or "we sailed." 

- **ὄντος σὺν ἡμῖν Ἀριστάρχου Μακεδόνος Θεσσαλονικέως**: The participle ὄντος is the present active genitive singular masculine participle of εἰμί, meaning "being" or "existing." The preposition σὺν means "with." The pronoun ἡμῖν is the first person plural dative of ἐγώ, meaning "us" or "to us." The genitive Ἀριστάρχου is the genitive singular of Ἀρίσταρχος, referring to a person named Aristarchus. The genitive Μακεδόνος is the genitive singular of Μακεδών, meaning "Macedonian." The genitive Θεσσαλονικέως is the genitive singular of Θεσσαλονικεύς, referring to a person from Thessalonica. The phrase can be translated as "with Aristarchus, the Macedonian from Thessalonica, being with us."

Literal translation: "Having embarked on a ship bound for Adramyttium, intending to sail to the places in Asia, we were brought, with Aristarchus, the Macedonian from Thessalonica, being with us."

The verse describes the author and his companions boarding a ship bound for Adramyttium with the intention of sailing to the regions of Asia. They were brought on the ship, and Aristarchus, a Macedonian from Thessalonica, was also with them.