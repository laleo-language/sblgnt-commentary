---
version: 1
---
- **τοῦτο δὲ αὐτοῦ λαλοῦντος**: The word τοῦτο is a neuter singular pronoun meaning "this." The word δὲ is a conjunction meaning "but" or "and." The pronoun αὐτοῦ is a genitive singular pronoun meaning "his" or "of him." The participle λαλοῦντος is the genitive singular masculine present active participle of λαλέω, meaning "speaking" or "talking." The phrase thus means "while he was speaking."

- **ἐγένετο στάσις τῶν Φαρισαίων καὶ Σαδδουκαίων**: The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "there was." The noun στάσις is the nominative singular of στάσις, meaning "a commotion" or "a disturbance." The genitive plural τῶν Φαρισαίων καὶ Σαδδουκαίων means "of the Pharisees and Sadducees." The phrase thus means "there was a commotion of the Pharisees and Sadducees."

- **καὶ ἐσχίσθη τὸ πλῆθος**: The conjunction καὶ means "and." The verb ἐσχίσθη is the third person singular aorist passive indicative of σχίζω, meaning "it was divided" or "it split." The noun τὸ πλῆθος is the nominative singular of πλῆθος, meaning "a crowd" or "a multitude." The phrase thus means "and the crowd was divided."

The entire verse can be literally translated as: "But while he was speaking, there was a commotion of the Pharisees and Sadducees, and the crowd was divided."