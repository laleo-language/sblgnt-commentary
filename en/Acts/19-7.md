---
version: 1
---
- **ἦσαν δὲ οἱ πάντες ἄνδρες**: The verb ἦσαν is the third person plural imperfect indicative of εἰμί, meaning "they were." The particle δὲ is used to add a contrasting or clarifying element, in this case meaning "but" or "and." The article οἱ is the masculine nominative plural, indicating "the." The adjective πάντες is the masculine nominative plural of πᾶς, meaning "all" or "every." And finally, the noun ἄνδρες is the masculine nominative plural of ἀνήρ, meaning "men." So the phrase can be translated as "But all the men were."

- **ὡσεὶ δώδεκα**: The adverb ὡσεί means "about" or "approximately." The number δώδεκα means "twelve." So the phrase can be translated as "about twelve."

The entire verse can be translated as: "But all the men were about twelve."