---
version: 1
---
- **Ἀπολυθέντες**: The word Ἀπολυθέντες is the nominative plural masculine participle of ἀπολύω, meaning "having been released" or "having been set free." 

- **δὲ**: The word δὲ is a conjunction that can be translated as "but" or "and" depending on the context. Here it is best translated as "and."

- **ἦλθον**: The word ἦλθον is the third person plural aorist indicative of ἔρχομαι, meaning "they came."

- **πρὸς τοὺς ἰδίους**: The word πρὸς is a preposition meaning "to" or "towards," and τοὺς ἰδίους is the accusative plural of ἴδιος, meaning "their own" or "their own people." So πρὸς τοὺς ἰδίους can be translated as "to their own people" or "to their own."

- **καὶ**: The word καὶ is a conjunction meaning "and."

- **ἀπήγγειλαν**: The word ἀπήγγειλαν is the third person plural aorist indicative of ἀπαγγέλλω, meaning "they reported" or "they announced."

- **ὅσα**: The word ὅσα is a pronoun meaning "as much as" or "all that." Here it is used as a relative pronoun and can be translated as "what" or "all that."

- **πρὸς αὐτοὺς**: The word πρὸς is a preposition meaning "to" or "towards," and αὐτοὺς is the accusative plural of αὐτός, meaning "them" or "they." So πρὸς αὐτοὺς can be translated as "to them" or "towards them."

- **οἱ ἀρχιερεῖς καὶ οἱ πρεσβύτεροι**: The words οἱ ἀρχιερεῖς mean "the chief priests" and οἱ πρεσβύτεροι mean "the elders." 

- **εἶπαν**: The word εἶπαν is the third person plural aorist indicative of λέγω, meaning "they said."

The phrase can be translated as "Having been released, they came to their own people and reported all that the chief priests and the elders had said."

The entire verse can be translated as "After they were released, they went to their own people and reported everything the chief priests and the elders had said."