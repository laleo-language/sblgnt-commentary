---
version: 1
---
- **εἶπεν δὲ Πέτρος**: The verb εἶπεν is the third person singular aorist indicative active of λέγω, meaning "he said." The conjunction δὲ is a common particle that can mean "and," "but," or "now." The noun Πέτρος is the nominative singular of Πέτρος, which means "Peter." The phrase thus means "But Peter said."

- **Ἀργύριον καὶ χρυσίον οὐχ ὑπάρχει μοι**: The nouns Ἀργύριον and χρυσίον are the accusative singular of Ἀργύριον and χρυσίον, which mean "silver" and "gold" respectively. The verb οὐχ ὑπάρχει is the third person singular present indicative active of ὑπάρχω, meaning "there is not" or "I do not have." The pronoun μοι is the dative singular of ἐγώ, meaning "to me" or "for me." The phrase thus means "I do not have silver or gold."

- **ὃ δὲ ἔχω τοῦτό σοι δίδωμι**: The pronoun ὃ is the accusative singular of ὅς, meaning "what" or "that which." The verb ἔχω is the first person singular present indicative active of ἔχω, meaning "I have." The demonstrative pronoun τοῦτό is the accusative singular of οὗτος, meaning "this." The pronoun σοι is the dative singular of σύ, meaning "to you" or "for you." The verb δίδωμι is the first person singular present indicative active of δίδωμι, meaning "I give." The phrase thus means "What I have, I give to you."

- **ἐν τῷ ὀνόματι Ἰησοῦ Χριστοῦ τοῦ Ναζωραίου περιπάτει**: The preposition ἐν means "in." The article τῷ is the dative singular masculine of ὁ, meaning "the." The noun ὀνόματι is the dative singular of ὄνομα, meaning "name." The noun Ἰησοῦ is the genitive singular of Ἰησοῦς, which means "Jesus." The noun Χριστοῦ is the genitive singular of Χριστός, meaning "Christ." The noun Ναζωραίου is the genitive singular of Ναζωραῖος, meaning "Nazarene." The verb περιπάτει is the second person singular present imperative active of περιπατέω, meaning "walk" or "go about." The phrase thus means "Walk in the name of Jesus Christ of Nazareth."

Literal translation: "But Peter said, 'I do not have silver or gold, but what I have, I give to you; in the name of Jesus Christ of Nazareth, walk.'"