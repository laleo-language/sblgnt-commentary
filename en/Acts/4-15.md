---
version: 1
---
- **κελεύσαντες δὲ αὐτοὺς**: The participle κελεύσαντες is the nominative plural masculine of κελεύω, meaning "having commanded." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The phrase thus means "having commanded them."

- **ἔξω τοῦ συνεδρίου**: The preposition ἔξω means "outside." The article τοῦ is the genitive singular masculine of ὁ, "the." The noun συνέδριον means "council" or "assembly." The phrase thus means "outside the council."

- **ἀπελθεῖν**: The infinitive ἀπελθεῖν is the aorist infinitive of ἀπέρχομαι, meaning "to go away" or "to depart."

- **συνέβαλλον**: The verb συνέβαλλον is the third person plural imperfect indicative of συνβάλλω, meaning "they were discussing" or "they were conferring."

- **πρὸς ἀλλήλους**: The preposition πρὸς means "to" or "towards." The pronoun ἀλλήλους is the accusative plural of ἀλλήλων, meaning "one another" or "each other." The phrase thus means "to one another."

Literal translation: "Having commanded them to go outside the council, they were discussing with one another."