---
version: 1
---
- **ζητούντων τε αὐτὸν ἀποκτεῖναι**: The participle ζητούντων is the genitive plural masculine of ζητέω, meaning "seeking." The word τε is a particle that connects this phrase with the following one. The pronoun αὐτὸν is the accusative singular masculine of αὐτός, meaning "him." The infinitive ἀποκτεῖναι is the present infinitive of ἀποκτείνω, meaning "to kill." The phrase thus means "they were seeking to kill him."

- **ἀνέβη φάσις τῷ χιλιάρχῳ τῆς σπείρης**: The verb ἀνέβη is the third person singular aorist indicative of ἀναβαίνω, meaning "he went up." The noun φάσις is the nominative singular of φάσις, which means "report" or "news." The dative noun τῷ χιλιάρχῳ is the dative singular masculine of χιλίαρχος, meaning "tribune" or "commander." The genitive noun τῆς σπείρης is the genitive singular feminine of σπείρα, meaning "cohort" or "military unit." The phrase thus means "a report went up to the tribune of the cohort."

- **ὅτι ὅλη συγχύννεται Ἰερουσαλήμ**: The conjunction ὅτι means "that" and introduces indirect discourse. The adjective ὅλη is the nominative singular feminine of ὅλος, meaning "whole" or "entire." The verb συγχύννεται is the third person singular present indicative of συγχύννω, meaning "is in confusion" or "is stirred up." The noun Ἰερουσαλήμ is the nominative singular of Ἰερουσαλήμ, which means "Jerusalem." The phrase thus means "that the whole Jerusalem is in confusion."

- The entire verse can be literally translated as: "While they were seeking to kill him, a report went up to the tribune of the cohort that the whole Jerusalem is in confusion."