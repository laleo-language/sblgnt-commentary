---
version: 1
---
- **ὁ δὲ ἑκατοντάρχης**: The word ὁ is the definite article, meaning "the." The word δὲ is a conjunction, meaning "and" or "but." The noun ἑκατοντάρχης means "centurion" or "commander of a hundred." The phrase thus means "but the centurion."

- **βουλόμενος διασῶσαι τὸν Παῦλον**: The verb βουλόμενος is the present middle participle of βούλομαι, meaning "wanting" or "desiring." The infinitive διασῶσαι is the aorist infinitive of διασῴζω, meaning "to save." The accusative noun τὸν Παῦλον is the accusative singular of Παῦλος, which means "Paul." The phrase thus means "wanting to save Paul."

- **ἐκώλυσεν αὐτοὺς τοῦ βουλήματος**: The verb ἐκώλυσεν is the third person singular aorist indicative of κωλύω, meaning "he prevented" or "he hindered." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The genitive noun τοῦ βουλήματος means "of the plan" or "of the intention." The phrase thus means "he prevented them from the plan."

- **ἐκέλευσέν τε τοὺς δυναμένους κολυμβᾶν**: The verb ἐκέλευσέν is the third person singular aorist indicative of κελεύω, meaning "he ordered" or "he commanded." The conjunction τε means "and." The article τοὺς is the definite article, meaning "the." The participle δυναμένους is the present middle participle of δύναμαι, meaning "able" or "capable." The verb κολυμβᾶν is the present infinitive of κολυμβάω, meaning "to swim." The phrase thus means "he ordered the ones who were able to swim."

- **ἀπορίψαντας πρώτους ἐπὶ τὴν γῆν ἐξιέναι**: The participle ἀπορίψαντας is the aorist active participle of ἀπορίπτω, meaning "having cast off" or "having thrown off." The adjective πρώτους means "first" or "foremost." The preposition ἐπὶ means "onto" or "to." The accusative noun τὴν γῆν means "the land" or "the ground." The verb ἐξιέναι is the present infinitive of ἐξέρχομαι, meaning "to go out" or "to come out." The phrase thus means "having cast off first to the land to go out."

The entire verse can be literally translated as: "But the centurion, wanting to save Paul, prevented them from the plan and ordered the ones who were able to swim to cast off first to the land to go out."