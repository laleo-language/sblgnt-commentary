---
version: 1
---
- **εἶπέν τε πρὸς αὐτούς**: The verb εἶπέν is the third person singular aorist indicative of λέγω, meaning "he said." The word τε is a conjunction that can be translated as "and" or "also." The preposition πρὸς means "to" or "towards," and the pronoun αὐτούς is the accusative plural of αὐτός, meaning "to them." The phrase means "he also said to them."

- **Ἄνδρες Ἰσραηλῖται**: The word Ἄνδρες is a noun meaning "men" or "people." The word Ἰσραηλῖται is a noun meaning "Israelites." The phrase means "Men of Israel."

- **προσέχετε ἑαυτοῖς**: The verb προσέχετε is the second person plural present imperative of προσέχω, meaning "you must pay attention." The reflexive pronoun ἑαυτοῖς means "to yourselves." The phrase means "Pay attention to yourselves."

- **ἐπὶ τοῖς ἀνθρώποις τούτοις**: The preposition ἐπὶ means "on" or "upon." The article τοῖς is the dative plural of ὁ, meaning "the." The noun ἀνθρώποις is the dative plural of ἄνθρωπος, meaning "people" or "men." The demonstrative pronoun τούτοις means "these" or "those." The phrase means "on these people."

- **τί μέλλετε πράσσειν**: The pronoun τί is an interrogative pronoun meaning "what." The verb μέλλετε is the second person plural present indicative of μέλλω, meaning "you are about to." The verb πράσσειν is the present infinitive of πράσσω, meaning "to do" or "to perform." The phrase means "What are you about to do?"

The literal translation of Acts 5:35 is: "He also said to them, 'Men of Israel, pay attention to yourselves regarding these people, what you are about to do.'"