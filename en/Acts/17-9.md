---
version: 1
---
- **καὶ λαβόντες**: The conjunction καὶ means "and" and introduces a new phrase. The participle λαβόντες is the nominative plural masculine of λαμβάνω, meaning "having taken" or "having received." The phrase thus means "and having taken."
- **τὸ ἱκανὸν**: The article τὸ is the accusative singular neuter of ὁ, meaning "the." The adjective ἱκανὸν is the accusative singular neuter of ἱκανός, meaning "sufficient" or "enough." The phrase thus means "the sufficient" or "enough."
- **παρὰ τοῦ Ἰάσονος καὶ τῶν λοιπῶν**: The preposition παρὰ means "from" or "by." The genitive masculine singular τοῦ is the genitive of ὁ, meaning "of" or "from." The proper noun Ἰάσονος is the genitive singular of Ἰάσων, meaning "Jason." The conjunction καὶ means "and" and introduces another genitive noun. The article τῶν is the genitive plural masculine of ὁ, meaning "the." The adjective λοιπῶν is the genitive plural masculine of λοιπός, meaning "remaining" or "other." The phrase thus means "from Jason and the remaining."
- **ἀπέλυσαν αὐτούς**: The verb ἀπέλυσαν is the third person plural aorist indicative of ἀπολύω, meaning "they released" or "they let go." The pronoun αὐτούς is the accusative plural of αὐτός, meaning "them." The phrase thus means "they released them."

Literal translation: "And having taken the sufficient from Jason and the remaining, they released them."

The verse is from Acts 17:9, which describes an event where certain people took what they needed from Jason and the others, and then released them.