---
version: 1
---
- **καὶ ἐλάλησαν αὐτῷ**: The conjunction καὶ means "and." The verb ἐλάλησαν is the third person plural aorist indicative of λαλέω, which means "they spoke." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "and they spoke to him."

- **τὸν λόγον τοῦ κυρίου**: The article τὸν is the accusative singular masculine of ὁ, meaning "the." The noun λόγον is the accusative singular of λόγος, which means "word" or "message." The genitive τοῦ κυρίου is the genitive singular masculine of κύριος, meaning "of the Lord." The phrase can be translated as "the word of the Lord."

- **σὺν πᾶσι τοῖς ἐν τῇ οἰκίᾳ αὐτοῦ**: The preposition σὺν means "with." The adjective πᾶσι is the dative plural masculine of πᾶς, meaning "all" or "every." The article τοῖς is the dative plural masculine of ὁ, meaning "the." The preposition ἐν means "in." The definite article τῇ is the dative singular feminine of ὁ, meaning "the." The noun οἰκίᾳ is the dative singular of οἰκία, which means "house." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "his." The phrase can be translated as "with all those in his house."

The literal translation of the verse is: "And they spoke to him the word of the Lord with all those in his house."