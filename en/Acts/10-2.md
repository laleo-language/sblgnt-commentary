---
version: 1
---
- **εὐσεβὴς καὶ φοβούμενος τὸν θεὸν**: The word εὐσεβὴς is an adjective in the nominative singular masculine form of εὐσεβής, which means "pious" or "devout." The word φοβούμενος is a present participle in the nominative singular masculine form of φοβέομαι, which means "fearing" or "revering." The phrase thus means "pious and fearing God."

- **σὺν παντὶ τῷ οἴκῳ αὐτοῦ**: The preposition σὺν means "with" and is followed by the dative case. The word παντὶ is the dative singular masculine form of πᾶς, which means "all" or "every." The word τῷ οἴκῳ is the dative singular masculine form of οἶκος, which means "house" or "household." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, which means "his." The phrase thus means "with all his household."

- **⸀ποιῶν ἐλεημοσύνας πολλὰς τῷ λαῷ**: The word ποιῶν is a present participle in the nominative singular masculine form of ποιέω, which means "doing" or "performing." The noun ἐλεημοσύνας is the accusative plural form of ἐλεημοσύνη, which means "acts of charity" or "alms." The adjective πολλὰς is the accusative plural feminine form of πολύς, which means "many" or "much." The noun τῷ λαῷ is the dative singular masculine form of λαός, which means "people" or "nation." The phrase thus means "doing many acts of charity to the people."

- **καὶ δεόμενος τοῦ θεοῦ διὰ παντός**: The conjunction καὶ means "and." The participle δεόμενος is a present participle in the nominative singular masculine form of δέομαι, which means "praying" or "making requests." The pronoun τοῦ θεοῦ is the genitive singular masculine form of θεός, which means "God." The preposition διὰ means "through" or "by means of" and is followed by the genitive case. The adverb παντός means "always" or "at all times." The phrase thus means "and praying to God always."

Literal translation: "He was pious and fearing God with all his household, doing many acts of charity to the people and praying to God always."

This verse describes a man named Cornelius, who is characterized as pious and God-fearing. He not only demonstrates his piety and reverence for God personally, but also extends it to his entire household. Cornelius is actively involved in performing acts of charity for the people and regularly prays to God.