---
version: 1
---
- **διαλεγομένου δὲ αὐτοῦ**: The participle διαλεγομένου is from the verb διαλέγομαι, meaning "to discuss" or "to reason." It is in the genitive singular masculine form, agreeing with the noun αὐτοῦ, which means "of him" or "his." The phrase διαλεγομένου δὲ αὐτοῦ can be translated as "while he was discussing" or "as he was reasoning."

- **περὶ δικαιοσύνης καὶ ἐγκρατείας καὶ τοῦ κρίματος τοῦ μέλλοντος**: This is a prepositional phrase that begins with the preposition περὶ, meaning "about" or "concerning." The genitive nouns δικαιοσύνης, ἐγκρατείας, and τοῦ κρίματος indicate the topics of the discussion. δικαιοσύνης means "righteousness," ἐγκρατείας means "self-control," and τοῦ κρίματος τοῦ μέλλοντος means "the judgment to come." The phrase περὶ δικαιοσύνης καὶ ἐγκρατείας καὶ τοῦ κρίματος τοῦ μέλλοντος can be translated as "about righteousness, self-control, and the judgment to come."

- **ἔμφοβος γενόμενος ὁ Φῆλιξ**: The adjective ἔμφοβος means "terrified" or "frightened." The participle γενόμενος is from the verb γίνομαι, meaning "to become." ὁ Φῆλιξ is the name of the person being referred to, Felix. The phrase ἔμφοβος γενόμενος ὁ Φῆλιξ can be translated as "when Felix became terrified."

- **ἀπεκρίθη**: This is the verb ἀποκρίνομαι, meaning "to answer" or "to reply." It is in the third person singular aorist indicative form. The word ἀπεκρίθη can be translated as "he answered."

- **Τὸ νῦν ἔχον πορεύου, καιρὸν δὲ μεταλαβὼν μετακαλέσομαί σε**: This is a complex statement consisting of two imperatives and a future indicative verb. 
  - Τὸ νῦν ἔχον means "now having." Τὸ νῦν is an adverb meaning "now," and ἔχον is the present active participle of ἔχω, meaning "having." 
  - πορεύου is the second person singular present imperative of πορεύομαι, meaning "go" or "depart."
  - καιρὸν δὲ μεταλαβὼν μετακαλέσομαί σε can be translated as "but when I have an opportunity, I will summon you." καιρὸν is the accusative singular of καιρός, meaning "opportunity" or "time." μεταλαβὼν is the aorist active participle of μεταλαμβάνω, meaning "to have an opportunity." μετακαλέσομαί is the first person singular future indicative of μετακαλέω, meaning "I will summon."

The literal translation of the verse is: "While he was discussing about righteousness, self-control, and the judgment to come, Felix became terrified and answered, 'For now, go, but when I have an opportunity, I will summon you.'"

In this verse, Paul is having a conversation with Felix, the governor, and discussing important topics such as righteousness, self-control, and the judgment to come. Felix becomes afraid and responds by telling Paul to go for the time being, but promises to summon him when he has the chance.