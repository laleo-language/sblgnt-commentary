---
version: 1
---
- **καὶ δεηθέντων αὐτῶν**: The conjunction καὶ means "and." The verb δεηθέντων is the genitive plural aorist participle of δέομαι, which means "to pray" or "to beg." The genitive plural αὐτῶν is a possessive pronoun meaning "their." The phrase thus means "and when they had prayed."

- **ἐσαλεύθη ὁ τόπος**: The verb ἐσαλεύθη is the third person singular aorist indicative passive of σαλεύω, which means "to shake" or "to tremble." The definite article ὁ is the nominative singular masculine of ὁ, which means "the." The noun τόπος is the nominative singular masculine of τόπος, which means "place." The phrase thus means "the place was shaken."

- **ἐν ᾧ ἦσαν συνηγμένοι**: The preposition ἐν means "in." The relative pronoun ᾧ is the dative singular masculine of ὅς, which means "which" or "in which." The verb ἦσαν is the third person plural imperfect indicative of εἰμί, which means "they were." The verb συνηγμένοι is the nominative plural perfect passive participle of συνάγω, which means "to gather" or "to assemble." The phrase thus means "in which they were gathered."

- **καὶ ἐπλήσθησαν ἅπαντες**: The conjunction καὶ means "and." The verb ἐπλήσθησαν is the third person plural aorist passive indicative of πίμπλημι, which means "to fill." The indefinite pronoun ἅπαντες is the nominative plural masculine of ἅπας, which means "all" or "everyone." The phrase thus means "and they were all filled."

- **τοῦ ἁγίου πνεύματος**: The genitive article τοῦ is the genitive singular masculine of ὁ, which means "of the." The adjective ἁγίου is the genitive singular masculine of ἅγιος, which means "holy." The noun πνεύματος is the genitive singular neuter of πνεῦμα, which means "spirit." The phrase thus means "of the Holy Spirit."

- **καὶ ἐλάλουν τὸν λόγον τοῦ θεοῦ μετὰ παρρησίας**: The conjunction καὶ means "and." The verb ἐλάλουν is the third person plural imperfect indicative of λαλέω, which means "to speak" or "to talk." The accusative article τὸν is the accusative singular masculine of ὁ, which means "the." The noun λόγον is the accusative singular masculine of λόγος, which means "word" or "message." The genitive article τοῦ is the genitive singular masculine of ὁ, which means "of the." The noun θεοῦ is the genitive singular masculine of θεός, which means "God." The preposition μετὰ means "with." The noun παρρησίας is the genitive singular feminine of παρρησία, which means "boldness" or "confidence." The phrase thus means "and they were speaking the word of God with boldness."

The literal translation of Acts 4:31 is: "And when they had prayed, the place was shaken in which they were gathered, and they were all filled with the Holy Spirit, and they were speaking the word of God with boldness."