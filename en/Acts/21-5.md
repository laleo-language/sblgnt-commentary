---
version: 1
---
- **ὅτε δὲ ἐγένετο**: The word ὅτε is a conjunction meaning "when." The word δὲ is a conjunction meaning "but" or "and." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it became." The phrase means "when it happened."

- **ἐξαρτίσαι ἡμᾶς**: The infinitive ἐξαρτίσαι is the aorist infinitive of ἐξαρτίζω, meaning "to prepare" or "to complete." The pronoun ἡμᾶς is the accusative plural of ἐγώ, meaning "us." The phrase means "to prepare us" or "to complete us."

- **τὰς ἡμέρας**: The article τὰς is the accusative plural feminine definite article, meaning "the." The noun ἡμέρας is the accusative plural feminine form of ἡμέρα, meaning "days." The phrase means "the days."

- **ἐξελθόντες**: The verb ἐξελθόντες is the nominative masculine plural aorist participle of ἐξέρχομαι, meaning "having gone out" or "going out."

- **ἐπορευόμεθα**: The verb ἐπορευόμεθα is the first person plural imperfect indicative middle of πορεύομαι, meaning "we were going" or "we were proceeding."

- **προπεμπόντων ἡμᾶς πάντων**: The participle προπεμπόντων is the genitive plural masculine present participle of προπέμπω, meaning "sending us off" or "accompanying us." The pronoun ἡμᾶς is the accusative plural of ἐγώ, meaning "us." The adjective πάντων is the genitive plural masculine form of πᾶς, meaning "all" or "every." The phrase means "of all who were sending us off" or "of all who were accompanying us."

- **σὺν γυναιξὶ καὶ τέκνοις**: The preposition σὺν means "with." The noun γυναιξὶ is the dative plural feminine form of γυνή, meaning "women" or "wives." The noun τέκνοις is the dative plural neuter form of τέκνον, meaning "children." The phrase means "with women and children."

- **ἕως ἔξω τῆς πόλεως**: The preposition ἕως means "until." The adverb ἔξω means "outside." The article τῆς is the genitive singular feminine definite article, meaning "the." The noun πόλεως is the genitive singular feminine form of πόλις, meaning "city." The phrase means "until outside the city."

- **καὶ θέντες τὰ γόνατα ἐπὶ τὸν αἰγιαλὸν**: The conjunction καὶ means "and." The verb θέντες is the nominative masculine plural aorist participle of τίθημι, meaning "having placed" or "having set." The definite article τὰ is the accusative plural neuter definite article, meaning "the." The noun γόνατα is the accusative plural neuter form of γόνυ, meaning "knees." The preposition ἐπὶ means "on" or "upon." The article τὸν is the accusative singular masculine definite article, meaning "the." The noun αἰγιαλὸν is the accusative singular masculine form of αἰγιαλός, meaning "shore" or "beach." The phrase means "and having placed the knees on the shore."

- **προσευξάμενοι**: The verb προσευξάμενοι is the nominative masculine plural aorist middle participle of προσεύχομαι, meaning "having prayed" or "having made a prayer."

The literal translation of the verse is: "But when it happened for us to complete the days, having gone out, we were proceeding with all who were sending us off, with women and children, until outside the city, and having placed the knees on the shore, having prayed."