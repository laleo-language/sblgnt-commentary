---
version: 1
---
- **ὡς οὐδὲν ὑπεστειλάμην**: The word ὡς is a conjunction meaning "as" or "like." The word οὐδὲν is the accusative neuter singular of οὐδείς, meaning "nothing." The verb ὑπεστειλάμην is the first person singular aorist middle indicative of ὑποστέλλομαι, meaning "I held back" or "I concealed." The phrase thus means "as I did not hold back" or "as I did not conceal."

- **τῶν συμφερόντων τοῦ μὴ ἀναγγεῖλαι ὑμῖν**: The word τῶν is the genitive plural of the definite article ὁ, meaning "of the." The word συμφερόντων is the genitive plural of the participle συμφέρων, meaning "beneficial" or "useful." The word τοῦ is the genitive singular of the definite article ὁ. The word μὴ is a negation particle meaning "not." The infinitive ἀναγγεῖλαι is the aorist infinitive of ἀναγγέλλω, meaning "to announce" or "to declare." The pronoun ὑμῖν is the dative plural of the second person pronoun σύ, meaning "to you." The phrase thus means "of the beneficial things not to announce to you."

- **καὶ διδάξαι ὑμᾶς δημοσίᾳ καὶ κατʼ οἴκους**: The conjunction καὶ means "and." The verb διδάξαι is the aorist infinitive of διδάσκω, meaning "to teach." The pronoun ὑμᾶς is the accusative plural of the second person pronoun σύ, meaning "you." The adverb δημοσίᾳ means "publicly" or "openly." The conjunction καὶ means "and." The preposition κατʼ is a contraction of κατά and means "according to" or "in." The noun οἶκος is the genitive singular of οἶκος, meaning "house." The phrase thus means "and to teach you publicly and according to house."

The entire verse, Acts 20:20, can be literally translated as: "How I did not hold back anything that is beneficial, so as not to announce to you and to teach you publicly and according to house."