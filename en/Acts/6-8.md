---
version: 1
---
- **Στέφανος δὲ**: Στέφανος is the nominative singular form of Στέφανος, which means "Stephen." The particle δὲ is a conjunction that can be translated as "and" or "but." The phrase thus means "But Stephen."

- **πλήρης χάριτος καὶ δυνάμεως**: The adjective πλήρης is in the nominative singular masculine form and means "full" or "filled." The noun χάρις is in the genitive singular form and means "grace" or "favor." The conjunction καὶ connects χάριτος with δυνάμεως. The noun δύναμις is in the genitive singular form and means "power" or "ability." The phrase can be translated as "full of grace and power."

- **ἐποίει τέρατα καὶ σημεῖα μεγάλα**: The verb ἐποίει is the imperfect indicative third person singular form of ποιέω, meaning "he was doing" or "he was performing." The noun τέρας is in the accusative plural form and means "wonders" or "miracles." The conjunction καὶ connects τέρατα with σημεῖα. The noun σημεῖον is in the accusative plural form and means "signs." The adjective μεγάλα is in the accusative plural form and means "great" or "mighty." The phrase can be translated as "he was doing great wonders and signs."

- **ἐν τῷ λαῷ**: The preposition ἐν means "in" or "among." The definite article τῷ is the dative singular form of ὁ, which means "the." The noun λαός is in the dative singular form and means "people" or "crowd." The phrase can be translated as "among the people."

Literal Translation: "But Stephen, full of grace and power, was doing great wonders and signs among the people."

This verse describes Stephen, who is portrayed as a man filled with grace and power. He is performing impressive miracles and signs among the people.