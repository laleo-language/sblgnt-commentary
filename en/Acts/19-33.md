---
version: 1
---
- **ἐκ δὲ τοῦ ὄχλου**: The preposition ἐκ means "from" and governs the genitive case, so δὲ τοῦ ὄχλου means "from the crowd."

- **συνεβίβασαν Ἀλέξανδρον**: The verb συνεβίβασαν is the third person plural aorist indicative of συνβιβάζω, which means "they brought together" or "they gathered." The accusative noun Ἀλέξανδρον refers to a person named Alexander. So the phrase means "they brought together Alexander."

- **προβαλόντων αὐτὸν τῶν Ἰουδαίων**: The participle προβαλόντων is the genitive plural masculine of προβάλλω, which means "having brought forward" or "having put forward." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, meaning "him." The genitive noun phrase τῶν Ἰουδαίων means "of the Jews." So the phrase means "having brought him forward of the Jews."

- **ὁ δὲ Ἀλέξανδρος κατασείσας τὴν χεῖρα**: The nominative noun phrase ὁ δὲ Ἀλέξανδρος means "but Alexander." The verb κατασείσας is the nominative singular masculine participle of κατασείω, which means "shaking." The accusative noun phrase τὴν χεῖρα means "his hand." So the phrase means "but Alexander, shaking his hand."

- **ἤθελεν ἀπολογεῖσθαι τῷ δήμῳ**: The verb ἤθελεν is the third person singular imperfect indicative of θέλω, meaning "he wanted" or "he intended." The infinitive ἀπολογεῖσθαι is the present middle/passive infinitive of ἀπολογέομαι, which means "to defend oneself" or "to make a defense." The dative noun phrase τῷ δήμῳ means "to the people." So the phrase means "he wanted to make a defense to the people."

The literal translation of Acts 19:33 is: "But from the crowd, they brought together Alexander, when the Jews had put him forward, and Alexander, shaking his hand, wanted to make a defense to the people."