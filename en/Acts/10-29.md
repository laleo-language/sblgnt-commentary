---
version: 1
---
- **διὸ καὶ ἀναντιρρήτως ἦλθον μεταπεμφθείς**: The word διὸ is a conjunction meaning "therefore" or "so." The adverb καὶ means "also" or "even." The adverb ἀναντιρρήτως is derived from the adjective ἀναντίρρητος, which means "unquestionably" or "without hesitation." The verb ἦλθον is the first person singular aorist indicative of ἔρχομαι, meaning "I came." The participle μεταπεμφθείς is derived from the verb μεταπέμπομαι, which means "I am sent for" or "I am summoned." The phrase thus means "therefore, I also came without hesitation when I was sent for."

- **πυνθάνομαι οὖν τίνι λόγῳ μετεπέμψασθέ με**: The verb πυνθάνομαι is the first person singular present indicative of πυνθάνομαι, meaning "I inquire" or "I ask." The conjunction οὖν means "therefore" or "so." The pronoun τίνι is the dative singular of the interrogative pronoun τίς, meaning "who" or "what." The noun λόγῳ is the dative singular of λόγος, which means "word" or "message." The verb μετεπέμψασθέ is the second person plural aorist imperative of μεταπέμπομαι, meaning "you sent for" or "you summoned." The pronoun με is the accusative singular of ἐγώ, meaning "me." The phrase thus means "so, I inquire by what word you sent for me."

The literal translation of Acts 10:29 is: "Therefore, I also came without hesitation when I was sent for. So, I inquire by what word you sent for me."