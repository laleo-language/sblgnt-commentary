---
version: 1
---
- **οὓς συναθροίσας**: The word οὓς is the accusative plural of ὅς, which means "who" or "whom." The verb συναθροίσας is the aorist participle of συναθροίζω, meaning "having gathered together." The phrase thus means "having gathered together those."

- **καὶ τοὺς περὶ τὰ τοιαῦτα ἐργάτας**: The word τοὺς is the accusative plural of ὁ, which means "the." The phrase περὶ τὰ τοιαῦτα means "concerning such things." The noun ἐργάτας is the accusative plural of ἐργάτης, which means "workers" or "laborers." The phrase thus means "the workers concerning such things."

- **εἶπεν**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said."

- **Ἄνδρες**: The word Ἄνδρες is a vocative noun meaning "men" or "people."

- **ἐπίστασθε**: The verb ἐπίστασθε is the second person plural present middle/passive indicative of ἐπίσταμαι, meaning "you know" or "you understand."

- **ὅτι**: The word ὅτι is a conjunction meaning "that" or "because."

- **ἐκ ταύτης τῆς ἐργασίας**: The phrase ἐκ ταύτης τῆς ἐργασίας means "from this work" or "because of this labor." The word ἐκ is a preposition meaning "from" or "because of," and the noun ἐργασίας is the genitive singular of ἐργασία, which means "work" or "labor."

- **ἡ εὐπορία ἡμῖν ἐστιν**: The noun εὐπορία is the nominative singular of εὐπορία, which means "prosperity" or "wealth." The pronoun ἡμῖν is the dative plural of ἐγώ, meaning "to us" or "for us." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is" or "exists." The phrase thus means "prosperity is to us."

The literal translation of the entire verse is: "Having gathered together those and the workers concerning such things, he said, 'Men, you know that from this work prosperity is to us.'"