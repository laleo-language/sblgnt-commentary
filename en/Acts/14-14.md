---
version: 1
---
- **ἀκούσαντες δὲ οἱ ἀπόστολοι**: The participle ἀκούσαντες is the nominative plural masculine of ἀκούω, which means "having heard." The definite article οἱ is the nominative plural masculine article, which means "the." The noun ἀπόστολοι is the nominative plural masculine of ἀπόστολος, meaning "apostles." The phrase thus means "the apostles, having heard."

- **Βαρναβᾶς καὶ Παῦλος**: The proper noun Βαρναβᾶς is the nominative singular masculine of Βαρναβᾶς, which is the name "Barnabas." The conjunction καὶ means "and." The proper noun Παῦλος is the nominative singular masculine of Παῦλος, which is the name "Paul." The phrase thus means "Barnabas and Paul."

- **διαρρήξαντες τὰ ἱμάτια αὐτῶν**: The participle διαρρήξαντες is the nominative plural masculine of διαρήσσω, which means "having torn." The definite article τὰ is the accusative plural neuter article, which means "the." The noun ἱμάτια is the accusative plural neuter of ἱμάτιον, meaning "garments" or "clothes." The pronoun αὐτῶν is the genitive plural masculine of αὐτός, which means "their." The phrase thus means "having torn their garments."

- **ἐξεπήδησαν εἰς τὸν ὄχλον κράζοντες**: The verb ἐξεπήδησαν is the third person plural aorist indicative of ἐκπηδάω, meaning "they leaped out." The preposition εἰς means "into." The definite article τὸν is the accusative singular masculine article, which means "the." The noun ὄχλον is the accusative singular masculine of ὄχλος, meaning "crowd" or "mob." The participle κράζοντες is the nominative plural masculine of κράζω, which means "shouting" or "crying out." The phrase thus means "they leaped out into the crowd, shouting."

- The literal translation of the entire verse is: "But when the apostles Barnabas and Paul heard, they tore their garments and leaped out into the crowd, shouting."