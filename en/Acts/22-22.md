---
version: 1
---
- **Ἤκουον δὲ αὐτοῦ**: The verb Ἤκουον is the third person plural imperfect indicative of ἀκούω, meaning "they were hearing" or "they were listening." The pronoun αὐτοῦ is the genitive singular of αὐτός, meaning "of him." So this phrase means "They were listening to him."

- **ἄχρι τούτου τοῦ λόγου**: The preposition ἄχρι means "until" or "up to." The demonstrative pronoun τούτου is the genitive singular of οὗτος, meaning "this." The noun λόγου is the genitive singular of λόγος, meaning "word" or "message." So this phrase means "until this word" or "up to this message."

- **καὶ ἐπῆραν τὴν φωνὴν αὐτῶν λέγοντες**: The conjunction καὶ means "and." The verb ἐπῆραν is the third person plural aorist indicative of ἐπαίρω, meaning "they raised." The noun φωνὴν is the accusative singular of φωνή, meaning "voice" or "sound." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The participle λέγοντες is the present active participle of λέγω, meaning "saying" or "speaking." So this phrase means "and they raised their voices, saying."

- **Αἶρε ἀπὸ τῆς γῆς τὸν τοιοῦτον**: The verb Αἶρε is the second person singular present imperative of αἴρω, meaning "take" or "remove." The preposition ἀπὸ means "from." The article τῆς is the genitive singular of ὁ, meaning "the." The noun γῆς is the genitive singular of γῆ, meaning "earth" or "land." The article τὸν is the accusative singular of ὁ. The adjective τοιοῦτον is the accusative singular of τοιοῦτος, meaning "such." So this phrase means "Take away from the land such."

- **οὐ γὰρ καθῆκεν αὐτὸν ζῆν**: The adverb οὐ means "not." The conjunction γὰρ means "for." The verb καθῆκεν is the third person singular aorist indicative of καθήκω, meaning "he ought" or "it is proper." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him." The verb ζῆν is the present infinitive of ζάω, meaning "to live." So this phrase means "For it is not proper for him to live."

Putting it all together, the literal translation of Acts 22:22 is: "They were listening to him until this message, and they raised their voices, saying, 'Take away such from the land, for it is not proper for him to live.'"