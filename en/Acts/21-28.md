---
version: 1
---
- **κράζοντες**: The participle κράζοντες is from the verb κράζω, which means "to cry out" or "to shout." The word κράζοντες is in the nominative plural masculine form, so it means "they were shouting."

- **Ἄνδρες Ἰσραηλῖται, βοηθεῖτε**: The phrase Ἄνδρες Ἰσραηλῖται means "Men of Israel." The word Ἄνδρες is the nominative plural masculine form of ἀνήρ, which means "man." The word Ἰσραηλῖται is the nominative plural form of Ἰσραηλίτης, which means "Israelite." The verb βοηθεῖτε is the imperative form of βοηθέω, which means "help." So the phrase means "Men of Israel, help!"

- **οὗτός ἐστιν ὁ ἄνθρωπος**: The phrase οὗτός ἐστιν ὁ ἄνθρωπος means "This is the man." The word οὗτός is a demonstrative pronoun meaning "this." The verb ἐστιν is the third person singular present indicative form of εἰμί, which means "to be." The word ἄνθρωπος is the nominative singular form of ἄνθρωπος, which means "man."

- **ὁ κατὰ τοῦ λαοῦ καὶ τοῦ νόμου καὶ τοῦ τόπου τούτου πάντας ⸀πανταχῇ διδάσκων**: This is a participial phrase modifying ὁ ἄνθρωπος. The article ὁ is the definite article meaning "the." The preposition κατὰ means "against" or "opposed to." The genitive nouns λαοῦ, νόμου, and τόπου mean "the people," "the law," and "the place" respectively. The phrase πάντας πανταχῇ means "everyone everywhere." The participle διδάσκων is from the verb διδάσκω, which means "to teach." So the phrase means "the man who is against the people, the law, and this place, teaching everyone everywhere."

- **ἔτι τε καὶ Ἕλληνας εἰσήγαγεν εἰς τὸ ἱερὸν**: The phrase ἔτι τε καὶ Ἕλληνας means "and even Greeks." The word ἔτι means "even" or "also." The word τε is a conjunction that means "and" or "both." The noun Ἕλληνας is the accusative plural form of Ἕλλην, which means "Greek." The verb εἰσήγαγεν is the third person singular aorist indicative form of εἰσάγω, which means "to bring in." The preposition εἰς means "into." The noun τὸ ἱερὸν means "the temple."

- **κεκοίνωκεν τὸν ἅγιον τόπον τοῦτον**: The verb κεκοίνωκεν is the third person singular perfect indicative form of κοινόω, which means "to defile" or "to make common." The accusative noun τὸν ἅγιον τόπον means "the holy place." The pronoun τοῦτον is a demonstrative pronoun meaning "this."

Putting it all together, the literal translation of Acts 21:28 is:

"Shouting, 'Men of Israel, help! This is the man who is against the people, the law, and this place, teaching everyone everywhere, and even Greeks he brought into the temple and has defiled this holy place.'"