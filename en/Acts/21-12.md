---
version: 1
---
- **ὡς δὲ ἠκούσαμεν ταῦτα**: The word ὡς is a conjunction meaning "when" or "as." The verb ἠκούσαμεν is the first person plural aorist indicative of ἀκούω, meaning "we heard." The noun ταῦτα is the accusative plural of οὗτος, meaning "these" or "these things." The phrase thus means "when we heard these things."

- **παρεκαλοῦμεν ἡμεῖς τε καὶ οἱ ἐντόπιοι**: The verb παρεκαλοῦμεν is the first person plural present indicative of παρακαλέω, meaning "we urged" or "we exhorted." The pronoun ἡμεῖς is the nominative plural of ἐγώ, meaning "we" or "us." The conjunction τε is used to connect two elements and can be translated as "both" or "and." The article οἱ is the nominative plural of ὁ, meaning "the." The adjective ἐντόπιοι is the nominative plural of ἐντόπιος, meaning "native" or "local." The phrase thus means "we and the natives."

- **τοῦ μὴ ἀναβαίνειν αὐτὸν εἰς Ἰερουσαλήμ**: The article τοῦ is the genitive singular of ὁ, meaning "of the." The particle μὴ is a negative particle indicating "not" or "that...not." The verb ἀναβαίνειν is the present infinitive of ἀναβαίνω, meaning "to go up" or "to ascend." The pronoun αὐτὸν is the accusative singular of αὐτός, meaning "him" or "he." The preposition εἰς is used with the accusative case and means "into" or "to." The noun Ἰερουσαλήμ is the accusative singular of Ἰερουσαλήμ, meaning "Jerusalem." The phrase thus means "that he would not go up to Jerusalem."

The literal translation of Acts 21:12 is: "When we heard these things, we and the natives urged him not to go up to Jerusalem."