---
version: 1
---
- **οἷς**: This is the dative plural of the relative pronoun ὅς, ἥ, ὅ, which means "who, which, that." It is used here to introduce a relative clause and refers back to the disciples mentioned earlier. The phrase οἷς καὶ παρέστησεν ἑαυτὸν can be translated as "to whom he also presented himself."

- **καὶ**: This is a coordinating conjunction meaning "and."

- **παρέστησεν**: This is the third person singular aorist indicative active of the verb παρίστημι, which means "to present, to offer." The subject of the verb is ἑαυτὸν (himself), and the direct object is ζῶντα (living). The phrase παρέστησεν ἑαυτὸν ζῶντα can be translated as "he presented himself alive."

- **ἑαυτὸν**: This is the accusative singular reflexive pronoun, which means "himself."

- **ζῶντα**: This is the accusative singular present participle of the verb ζάω, which means "to live." It is in the masculine gender, agreeing with ἑαυτὸν. The phrase ἑαυτὸν ζῶντα can be translated as "himself alive."

- **μετὰ τὸ παθεῖν αὐτὸν**: This is a prepositional phrase consisting of the preposition μετὰ (with) and the accusative neuter article τὸ followed by the aorist infinitive παθεῖν (to suffer) and the accusative pronoun αὐτὸν (him). It can be translated as "after his suffering."

- **ἐν πολλοῖς τεκμηρίοις**: This is a prepositional phrase consisting of the preposition ἐν (in) and the dative plural neuter noun πολλοῖς (many) followed by the dative plural neuter noun τεκμηρίοις (proofs, evidences). The phrase ἐν πολλοῖς τεκμηρίοις can be translated as "with many proofs."

- **διʼ ἡμερῶν τεσσεράκοντα**: This is a prepositional phrase consisting of the preposition διά (through) and the genitive plural feminine noun ἡμέρας (days) modified by the cardinal number τεσσεράκοντα (forty). The phrase διʼ ἡμερῶν τεσσεράκοντα can be translated as "for forty days."

- **ὀπτανόμενος**: This is the nominative singular masculine present participle middle/passive of the verb ὀπτάνομαι, which means "to see, to appear." The subject of the participle is ἑαυτοῖς (to them). The phrase ὀπτανόμενος αὐτοῖς can be translated as "appearing to them."

- **αὐτοῖς**: This is the dative plural pronoun meaning "to them."

- **καὶ**: This is a coordinating conjunction meaning "and."

- **λέγων**: This is the nominative singular masculine present participle active of the verb λέγω, which means "to say." The subject of the participle is ἑαυτοῖς (to them). The phrase λέγων τὰ περὶ τῆς βασιλείας τοῦ θεοῦ can be translated as "saying the things about the kingdom of God."

- **τὰ περὶ τῆς βασιλείας τοῦ θεοῦ**: This is a noun phrase consisting of the accusative neuter article τὰ followed by the preposition περὶ (about) and the genitive feminine noun βασιλείας (kingdom) modified by the genitive masculine noun θεοῦ (of God). The phrase τὰ περὶ τῆς βασιλείας τοῦ θεοῦ can be translated as "the things about the kingdom of God."

The literal translation of the verse is: "To whom also he presented himself alive after his suffering, with many proofs, appearing to them for forty days and saying the things about the kingdom of God."