---
version: 1
---
- **οὐ παντὶ τῷ λαῷ**: The word οὐ is a negation, meaning "not." The noun παντὶ is the dative singular of πᾶς, which means "all" or "every." The noun τῷ λαῷ is the dative singular of λαός, which means "people" or "nation." The phrase thus means "not to all the people."

- **ἀλλὰ μάρτυσι τοῖς προκεχειροτονημένοις ὑπὸ τοῦ θεοῦ**: The word ἀλλὰ means "but." The noun μάρτυσι is the dative plural of μάρτυς, which means "witnesses." The adjective προκεχειροτονημένοις is the perfect passive participle in the dative plural of προχειροτονέω, which means "to choose beforehand" or "to designate." The preposition ὑπὸ means "by" or "under." The noun τοῦ θεοῦ is the genitive singular of θεός, which means "God." The phrase thus means "but to the witnesses chosen beforehand by God."

- **ἡμῖν**: The word ἡμῖν is the dative plural of ἐγώ, which means "we" or "us." The phrase means "to us."

- **οἵτινες συνεφάγομεν καὶ συνεπίομεν αὐτῷ μετὰ τὸ ἀναστῆναι αὐτὸν ἐκ νεκρῶν**: The relative pronoun οἵτινες means "who" or "which." The verb συνεφάγομεν is the first person plural aorist indicative of συνεσθίω, which means "we ate together." The verb συνεπίομεν is the first person plural aorist indicative of συνπίνω, which means "we drank together." The pronoun αὐτῷ is the dative singular of αὐτός, which means "him." The preposition μετὰ means "with." The article τὸ before ἀναστῆναι is the accusative singular neuter of ὁ, which means "the." The verb ἀναστῆναι is the aorist infinitive of ἀνίστημι, which means "to rise" or "to stand up." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, which means "him." The preposition ἐκ means "from." The noun νεκρῶν is the genitive plural of νεκρός, which means "dead." The phrase thus means "who we ate and drank with him after his rising from the dead."

- The entire verse can be literally translated as: "Not to all the people, but to the witnesses chosen beforehand by God, to us who ate and drank with him after his rising from the dead."