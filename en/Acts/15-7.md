---
version: 1
---
- **πολλῆς δὲ ζητήσεως γενομένης**: The word πολλῆς is the genitive singular of πολύς, meaning "much" or "many." The word ζητήσεως is the genitive singular of ζήτησις, meaning "inquiry" or "question." The participle γενομένης is the genitive singular feminine form of γίνομαι, meaning "having happened" or "having occurred." The phrase can be translated as "with much inquiry having happened."

- **ἀναστὰς Πέτρος**: The participle ἀναστὰς is the nominative singular masculine form of ἀνίστημι, meaning "having stood up" or "rising." The name Πέτρος refers to the apostle Peter. The phrase means "Peter, having stood up."

- **εἶπεν πρὸς αὐτούς**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The preposition πρὸς means "to" or "towards." The pronoun αὐτούς is the accusative plural form of αὐτός, meaning "them." The phrase can be translated as "he said to them."

- **Ἄνδρες ἀδελφοί**: The word Ἄνδρες is a vocative form of ἀνήρ, meaning "men" or "people." The word ἀδελφοί is the nominative plural form of ἀδελφός, meaning "brothers" or "brothers and sisters." The phrase can be translated as "Men, brothers."

- **ὑμεῖς ἐπίστασθε**: The pronoun ὑμεῖς is the nominative plural form of σύ, meaning "you." The verb ἐπίστασθε is the second person plural present indicative middle form of ἐπίσταμαι, meaning "you know" or "you understand." The phrase means "you know."

- **ὅτι ἀφʼ ἡμερῶν ἀρχαίων**: The conjunction ὅτι means "that." The preposition ἀφʼ means "from" or "since." The genitive plural noun ἡμερῶν is the genitive plural of ἡμέρα, meaning "days." The adjective ἀρχαίων is the genitive plural masculine form of ἀρχαῖος, meaning "ancient" or "old." The phrase can be translated as "that from ancient days."

- **ἐν ὑμῖν ἐξελέξατο ὁ θεὸς**: The preposition ἐν means "in" or "among." The pronoun ὑμῖν is the plural form of σύ, meaning "you." The verb ἐξελέξατο is the third person singular aorist middle indicative of ἐκλέγω, meaning "he chose" or "he selected." The article ὁ indicates the definite article "the." The noun θεὸς means "God." The phrase can be translated as "God chose among you."

- **διὰ τοῦ στόματός μου ἀκοῦσαι τὰ ἔθνη τὸν λόγον τοῦ εὐαγγελίου καὶ πιστεῦσαι**: The preposition διὰ means "through" or "by means of." The genitive article τοῦ indicates the genitive case and means "of the." The noun στόματός is the genitive singular form of στόμα, meaning "mouth." The infinitive ἀκοῦσαι is the aorist infinitive form of ἀκούω, meaning "to hear." The accusative plural noun τὰ ἔθνη means "the nations" or "the Gentiles." The accusative singular noun τὸν λόγον means "the word" or "the message." The genitive noun τοῦ εὐαγγελίου means "of the gospel." The conjunction καὶ means "and." The infinitive πιστεῦσαι is the aorist infinitive form of πιστεύω, meaning "to believe." The phrase can be translated as "to hear the word of the gospel and to believe."

The literal translation of Acts 15:7 is: "With much inquiry having happened, Peter, having stood up, said to them: 'Men, brothers, you know that from ancient days God chose among you to hear the word of the gospel and to believe.'"