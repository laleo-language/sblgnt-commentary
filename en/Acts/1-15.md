---
version: 1
---
- **ἐν ταῖς ἡμέραις ταύταις**: The preposition ἐν means "in" and the article ταῖς is the feminine dative plural of ὁ, meaning "the." The noun ἡμέραις is the dative plural of ἡμέρα, which means "day." The adjective ταύταις is the feminine dative plural of οὗτος, meaning "these." The phrase thus means "in these days."

- **ἀναστὰς Πέτρος**: The participle ἀναστὰς is the nominative singular masculine of ἀνίστημι, meaning "having stood up" or "having risen." The noun Πέτρος is the nominative singular masculine of Πέτρος, which is a proper name meaning "Peter." The phrase thus means "Peter, having stood up."

- **ἐν μέσῳ τῶν ἀδελφῶν**: The preposition ἐν means "in" and the article τῶν is the genitive plural of ὁ, meaning "the." The noun μέσῳ is the dative singular of μέσος, which means "middle" or "midst." The noun ἀδελφῶν is the genitive plural of ἀδελφός, meaning "brothers" or "brethren." The phrase thus means "in the midst of the brothers."

- **εἶπεν**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said."

- **(ἦν τε ὄχλος ὀνομάτων ἐπὶ τὸ αὐτὸ ὡς ἑκατὸν εἴκοσι)**: This parenthetical phrase provides additional information. The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "it was." The conjunction τε is used to connect the clause with the previous clause. The noun ὄχλος is the nominative singular masculine of ὄχλος, meaning "crowd" or "multitude." The genitive plural noun ὀνομάτων is the genitive plural of ὄνομα, meaning "name." The preposition ἐπὶ means "over" or "about." The article τὸ is the neuter singular of ὁ, meaning "the." The adjective αὐτὸ is the neuter singular of αὐτός, meaning "same" or "exact." The adverb ὡς means "about" or "approximately." The number ἑκατὸν is the accusative singular of ἑκατόν, meaning "hundred." The number εἴκοσι is the accusative singular of εἴκοσι, meaning "twenty." The phrase thus means "(there was a crowd of names, about a hundred and twenty)."

- The literal translation of Acts 1:15 is "And in these days, having stood up, Peter in the midst of the brothers said (now there was a crowd of names, about a hundred and twenty)."