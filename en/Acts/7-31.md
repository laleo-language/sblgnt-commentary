---
version: 1
---
- **ὁ δὲ Μωϋσῆς**: The word δὲ is a conjunction that can be translated as "and" or "but." The name Μωϋσῆς is the nominative singular of Μωϋσῆς, which means "Moses." So this phrase means "And Moses."

- **ἰδὼν ἐθαύμασεν τὸ ὅραμα**: The verb ἰδὼν is the nominative singular masculine participle of ὁράω, which means "to see." The verb ἐθαύμασεν is the third person singular aorist indicative of θαυμάζω, which means "to marvel" or "to wonder." The noun τὸ ὅραμα is the accusative singular of ὅραμα, which means "vision" or "sight." So this phrase means "Moses saw the vision and marveled."

- **προσερχομένου δὲ αὐτοῦ κατανοῆσαι**: The participle προσερχομένου is the genitive singular masculine participle of προσέρχομαι, which means "to approach" or "to come near." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, which means "him" or "his." The verb κατανοῆσαι is the aorist infinitive of κατανοέω, which means "to understand" or "to perceive." So this phrase means "while he was approaching to understand."

- **ἐγένετο φωνὴ κυρίου**: The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, which means "to become" or "to happen." The noun φωνὴ is the nominative singular of φωνή, which means "voice" or "sound." The noun κυρίου is the genitive singular of κύριος, which means "Lord." So this phrase means "a voice of the Lord happened."

The entire verse can be literally translated as: "And Moses saw the vision and marveled, while he was approaching to understand, a voice of the Lord happened."