---
version: 1
---
- **περὶ δὲ τῶν πεπιστευκότων ἐθνῶν**: The preposition περὶ means "concerning" or "about." The genitive article τῶν indicates that the following noun is in the genitive case, which can indicate possession or description. The participle πεπιστευκότων is the genitive plural of πιστεύω, "to believe" or "to have faith," and it functions as an adjective modifying the noun ἐθνῶν, which means "Gentiles" or "nations." The phrase can be translated as "concerning the Gentile believers."

- **ἡμεῖς ἀπεστείλαμεν κρίναντες φυλάσσεσθαι αὐτοὺς**: The pronoun ἡμεῖς means "we" and indicates the subject of the verb. The verb ἀπεστείλαμεν is the first person plural aorist indicative of ἀποστέλλω, "to send." The participle κρίναντες is the nominative plural of κρίνω, "to judge" or "to decide." The infinitive φυλάσσεσθαι is the present infinitive of φυλάσσω, "to guard" or "to keep." The pronoun αὐτοὺς is the accusative plural of αὐτός, "they" or "them." The phrase can be translated as "we sent them, having decided that they should keep."

- **τό τε εἰδωλόθυτον καὶ αἷμα καὶ πνικτὸν καὶ πορνείαν**: The article τό indicates that the following noun is a neuter singular noun. The neuter noun εἰδωλόθυτον means "meat sacrificed to idols." The conjunction τε connects this noun with the following nouns. The noun αἷμα means "blood." The noun πνικτὸν means "strangled animals." The noun πορνείαν means "sexual immorality" or "fornication." The phrase can be translated as "both the meat sacrificed to idols and blood and strangled animals and sexual immorality."

Literal translation: "But concerning the Gentile believers, we sent them, having decided that they should keep both the meat sacrificed to idols and blood and strangled animals and sexual immorality."