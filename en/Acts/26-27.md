---
version: 1
---
- **πιστεύεις**: The verb πιστεύεις is the second person singular present indicative of πιστεύω, meaning "you believe" or "you have faith."
- **βασιλεῦ Ἀγρίππα**: The noun βασιλεῦ is the vocative singular of βασιλεύς, which means "king." The name Ἀγρίππα is the proper name "Agrippa." So the phrase is addressing King Agrippa.
- **τοῖς προφήταις**: The article τοῖς is the masculine dative plural form of ὁ, meaning "the." The noun προφήταις is the dative plural of προφήτης, which means "prophets." So the phrase means "to the prophets."
- **οἶδα**: The verb οἶδα is the first person singular perfect indicative of εἴδω, meaning "I know."
- **ὅτι**: The word ὅτι is a conjunction meaning "that."
- **πιστεύεις**: The verb πιστεύεις is the second person singular present indicative of πιστεύω, meaning "you believe" or "you have faith."

The literal translation of the verse is: "You believe, King Agrippa, to the prophets? I know that you believe."