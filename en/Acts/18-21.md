---
version: 1
---
- **ἀλλὰ ἀποταξάμενος καὶ**: The word ἀλλὰ means "but" and is used to contrast or indicate a change in direction. The verb ἀποταξάμενος is the aorist middle participle of ἀποτάσσομαι, which means "to say goodbye" or "to take leave." The phrase thus means "but after saying goodbye."

- **εἰπών**: The participle εἰπών is the aorist active participle of λέγω, which means "to say." It is used here as an introductory phrase to quote the words of the speaker. The phrase ἐἰπών means "saying."

- **Πάλιν ἀνακάμψω πρὸς ὑμᾶς**: The word Πάλιν means "again" or "once more." The verb ἀνακάμψω is the future indicative of ἀνακάμπτω, which means "to return" or "to come back." The preposition πρὸς means "to" or "towards." The pronoun ὑμᾶς is the accusative plural of ὑμεῖς, which means "you." The phrase thus means "I will return to you."

- **τοῦ θεοῦ θέλοντος**: The article τοῦ indicates possession and is translated as "of." The noun θεοῦ means "God." The participle θέλοντος is the present active participle of θέλω, which means "to will" or "to want." The phrase τοῦ θεοῦ θέλοντος means "God willing."

- **ἀνήχθη**: The verb ἀνήχθη is the aorist passive indicative of ἀνάγω, which means "to lead" or "to bring." The subject of the verb is not explicitly stated, but it is understood to be the speaker. The phrase ἀνήχθη means "he was brought."

- **ἀπὸ τῆς Ἐφέσου**: The preposition ἀπὸ means "from" or "out of." The definite article τῆς indicates that Ἐφέσου is a specific place and is translated as "from the." The phrase ἀπὸ τῆς Ἐφέσου means "from Ephesus."

Literal translation: "But after saying goodbye, [he] said, 'Again I will return to you, God willing,' and [he] was brought from Ephesus."

The verse is not syntactically ambiguous, but the subject of the verb ἀνήχθη is not explicitly stated. It is understood to be the speaker, who is leaving Ephesus.