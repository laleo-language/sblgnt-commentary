---
version: 1
---
- **ὅ τε ἱερεὺς τοῦ Διὸς τοῦ ὄντος**: The word ὅ is a relative pronoun that introduces a relative clause. The noun ἱερεὺς is the nominative singular of ἱερεύς, which means "priest." The genitive τοῦ Διὸς τοῦ ὄντος is a genitive construction that describes the priest. The word Διὸς is the genitive singular of Διός, which means "of Zeus." The word ὄντος is the genitive singular of ὤν, which means "being" or "existing." The phrase thus means "the priest of Zeus who exists."

- **πρὸ τῆς πόλεως**: The preposition πρὸ means "before" or "in front of." The genitive τῆς πόλεως is a genitive construction that indicates the location. The phrase means "in front of the city."

- **ταύρους καὶ στέμματα**: The noun ταύρους is the accusative plural of ταῦρος, which means "bulls." The noun στέμματα is the accusative plural of στέμμα, which means "crowns." The phrase means "bulls and crowns."

- **ἐπὶ τοὺς πυλῶνας**: The preposition ἐπὶ means "on" or "upon." The accusative τοὺς πυλῶνας is a prepositional phrase that indicates the location. The phrase means "on the gates."

- **ἐνέγκας σὺν τοῖς ὄχλοις**: The verb ἐνέγκας is the nominative singular aorist active participle of φέρω, which means "having brought" or "having carried." The preposition σὺν means "with." The dative τοῖς ὄχλοις is a dative construction that indicates the group of people. The phrase means "having brought with the crowd."

- **ἤθελεν θύειν**: The verb ἤθελεν is the third person singular imperfect indicative of θέλω, which means "he/she/it wanted" or "he/she/it was willing." The infinitive θύειν is the present infinitive of θύω, which means "to sacrifice." The phrase means "he wanted to sacrifice."

The literal translation of the entire verse is: "The priest of Zeus who exists, before the city, having brought bulls and crowns, on the gates, wanted to sacrifice with the crowd."