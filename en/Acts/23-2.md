---
version: 1
---
- **ὁ δὲ ἀρχιερεὺς** : The word ὁ is the definite article, indicating "the." The noun ἀρχιερεύς is the nominative singular of ἀρχιερεύς, which means "high priest." The phrase thus means "the high priest."

- **Ἁνανίας** : This is a proper noun, the name "Ananias."

- **ἐπέταξεν** : This is the third person singular aorist indicative of ἐπιτάσσω, which means "he commanded" or "he ordered."

- **τοῖς παρεστῶσιν αὐτῷ** : The article τοῖς is the plural definite article, indicating "the." The participle παρεστῶσιν is the dative plural of παρεῖμι, meaning "to be present" or "to stand by." The pronoun αὐτῷ is the dative singular of αὐτός, which means "him" or "to him." The phrase can be translated as "to those who were standing by him."

- **τύπτειν αὐτοῦ τὸ στόμα** : The infinitive τύπτειν is the present active infinitive of τύπτω, meaning "to strike" or "to hit." The pronoun αὐτοῦ is the genitive singular of αὐτός, which means "his" or "of him." The noun στόμα is the accusative singular of στόμα, which means "mouth." The phrase can be translated as "to strike his mouth."

The entire verse can be translated as "And the high priest Ananias commanded those who stood by him to strike him on the mouth."