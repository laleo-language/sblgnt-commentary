---
version: 1
---
- **ἱκανοὶ δὲ τῶν τὰ περίεργα πραξάντων**: The word ἱκανοὶ is the nominative plural of ἱκανός, which means "many" or "a large number of." The definite article τῶν is the genitive plural of ὁ, meaning "of the." The phrase τὰ περίεργα is the accusative plural of περίεργος, which means "curious things" or "magical arts." The participle πραξάντων is the genitive plural of πράσσω, which means "to do" or "to practice." The phrase thus means "many of those who had practiced curious things."

- **συνενέγκαντες τὰς βίβλους**: The participle συνενέγκαντες is the nominative plural masculine of συνενέγκω, which means "having brought together" or "having gathered." The definite article τὰς is the accusative plural of ὁ. The noun βίβλους is the accusative plural of βίβλος, which means "books." The phrase thus means "having brought together the books."

- **κατέκαιον ἐνώπιον πάντων**: The verb κατέκαιον is the third person plural imperfect indicative of κατακαίω, which means "they burned" or "they destroyed by fire." The adverb ἐνώπιον means "in the presence of" or "before." The pronoun πάντων is the genitive plural of πᾶς, meaning "all" or "everyone." The phrase thus means "they burned in the presence of everyone."

- **καὶ συνεψήφισαν τὰς τιμὰς αὐτῶν**: The conjunction καὶ means "and." The verb συνεψήφισαν is the third person plural aorist indicative of συνψηφίζω, which means "they counted together" or "they totaled up." The definite article τὰς is the accusative plural of ὁ. The noun τιμὰς is the accusative plural of τιμή, which means "value" or "price." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "and they totaled up their value."

- **καὶ εὗρον ἀργυρίου μυριάδας πέντε**: The conjunction καὶ means "and." The verb εὗρον is the third person plural aorist indicative of εὑρίσκω, which means "they found." The noun ἀργυρίου is the genitive singular of ἀργύριον, which means "silver" or "money." The noun μυριάδας is the accusative plural of μυριάς, which means "myriad" or "ten thousand." The noun πέντε is the accusative plural of πέντε, meaning "five." The phrase thus means "and they found five myriads of silver."

The entire verse, Acts 19:19, could be literally translated as: "And many of those who had practiced curious things brought together their books and burned them in the presence of everyone; and they totaled up their value and found it to be five myriads of silver."