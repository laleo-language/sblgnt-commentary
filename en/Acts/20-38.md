---
version: 1
---
- **ὀδυνώμενοι μάλιστα**: The word ὀδυνώμενοι is the present participle middle voice nominative plural of ὀδυνάομαι, which means "to grieve" or "to be distressed." The word μάλιστα is an adverb that means "especially" or "most of all." So this phrase means "grieving most of all."

- **ἐπὶ τῷ λόγῳ**: The word ἐπὶ is a preposition that can mean "on," "upon," or "about." The word τῷ λόγῳ is the dative singular of ὁ λόγος, which means "word" or "message." The phrase can be translated as "about the word" or "concerning the message."

- **ᾧ εἰρήκει ὅτι οὐκέτι μέλλουσιν τὸ πρόσωπον αὐτοῦ θεωρεῖν**: The word ᾧ is a relative pronoun which refers back to τῷ λόγῳ. The verb εἰρήκει is the third person singular perfect indicative active of εἶπον, which means "to say" or "to speak." The word ὅτι is a conjunction that introduces indirect speech. The verb μέλλουσιν is the third person plural present indicative active of μέλλω, which means "to be about to" or "to intend." The word τὸ πρόσωπον is the accusative singular of ὁ πρόσωπον, which means "face." The verb θεωρεῖν is the present infinitive of θεωρέω, which means "to see" or "to behold." The phrase can be translated as "which he had said that they will no longer see his face."

- **προέπεμπον δὲ αὐτὸν εἰς τὸ πλοῖον**: The word προέπεμπον is the imperfect indicative active of προπέμπω, which means "to send off" or "to accompany." The pronoun αὐτὸν is the accusative singular of αὐτός, which means "him." The word εἰς is a preposition that means "into" or "to." The word τὸ πλοῖον is the accusative singular of τὸ πλοῖον, which means "boat" or "ship." The phrase can be translated as "but they accompanied him to the ship."

The literal translation of Acts 20:38 is: "Grieving most of all about the message which he had said that they will no longer see his face, but they accompanied him to the ship."