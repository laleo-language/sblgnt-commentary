---
version: 1
---
- **συναλιζόμενος**: This is a present middle participle of the verb συναλίζομαι, which means "to gather together" or "to assemble." The participle form indicates that the subject is participating in the action of the verb, and the middle voice suggests that the subject is acting upon themselves. So, this phrase can be translated as "while he was gathering together."
- **παρήγγειλεν**: This is the third person singular aorist indicative of the verb παραγγέλλω, which means "to command" or "to order." The subject of the verb is implied to be Jesus, who is giving the command. So, this phrase can be translated as "he commanded."
- **αὐτοῖς**: This is the dative plural pronoun αὐτός, meaning "they" or "them." It is used here as the indirect object of the verb παρήγγειλεν. So, this phrase can be translated as "to them."
- **ἀπὸ Ἱεροσολύμων**: The preposition ἀπό means "from," and Ἱεροσόλυμα is the accusative plural form of Ἱεροσόλυμα, which means "Jerusalem." Together, this phrase means "from Jerusalem."
- **μὴ χωρίζεσθαι**: The negation particle μὴ indicates a negative command, and the verb χωρίζεσθαι is a present middle infinitive of the verb χωρίζομαι, meaning "to separate" or "to depart." So, this phrase can be translated as "not to separate" or "not to depart."
- **ἀλλὰ περιμένειν**: The conjunction ἀλλά means "but," and the verb περιμένειν is a present infinitive of the verb περιμένω, meaning "to wait." So, this phrase can be translated as "but to wait."
- **τὴν ἐπαγγελίαν**: The article τὴν is the accusative singular feminine definite article, and ἐπαγγελίαν is the accusative singular form of ἐπαγγελία, which means "promise." So, this phrase can be translated as "the promise."
- **τοῦ πατρὸς**: The genitive singular form of ὁ πατήρ, which means "the father." So, this phrase can be translated as "of the father."
- **ἣν ἠκούσατέ μου**: The relative pronoun ἣν is in the accusative singular feminine form and refers back to τὴν ἐπαγγελίαν. The verb ἠκούσατέ is the second person plural aorist indicative of the verb ἀκούω, meaning "to hear." The pronoun μου means "my." So, this phrase can be translated as "which you heard from me."

A possible literal translation of the entire verse could be: "While he was gathering them together, he commanded them not to depart from Jerusalem, but to wait for the promise of the Father which you heard from me."