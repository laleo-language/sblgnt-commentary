---
version: 1
---
- **τὴν φωνὴν τοῦ Πέτρου**: The word φωνὴν is the accusative singular of φωνή, which means "voice." The genitive τοῦ Πέτρου indicates possession, so it means "the voice of Peter."

- **ἀπὸ τῆς χαρᾶς**: The preposition ἀπὸ means "from" and the genitive τῆς χαρᾶς means "of joy." So this phrase is "from the joy."

- **οὐκ ἤνοιξεν τὸν πυλῶνα**: The verb ἤνοιξεν is the third person singular aorist indicative of ἀνοίγω, which means "to open." The accusative τὸν πυλῶνα means "the gate." So this phrase is "she did not open the gate."

- **εἰσδραμοῦσα δὲ**: The participle εἰσδραμοῦσα is the nominative singular feminine present active participle of εἰστρέχω, which means "to run in." The conjunction δὲ is used to indicate a contrast or transition, so this phrase is "but running in."

- **ἀπήγγειλεν ἑστάναι τὸν Πέτρον**: The verb ἀπήγγειλεν is the third person singular aorist indicative of ἀπαγγέλλω, which means "to announce" or "to report." The infinitive ἑστάναι is the present infinitive of ἵστημι, which means "to stand." The accusative τὸν Πέτρον means "Peter." So this phrase is "she reported that Peter is standing."

- **πρὸ τοῦ πυλῶνος**: The preposition πρὸ means "before" and the genitive τοῦ πυλῶνος means "of the gate." So this phrase is "in front of the gate."

The literal translation of the entire verse is: "And recognizing the voice of Peter from the joy, she did not open the gate, but running in, she reported that Peter is standing in front of the gate."