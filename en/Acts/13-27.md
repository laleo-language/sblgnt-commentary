---
version: 1
---
- **οἱ γὰρ κατοικοῦντες ἐν Ἰερουσαλὴμ**: The definite article οἱ is the nominative plural of ὁ, meaning "the." The participle κατοικοῦντες is the present active nominative plural of κατοικέω, meaning "those who dwell" or "those who live." The preposition ἐν means "in." The noun Ἰερουσαλὴμ is the accusative singular of Ἱερουσαλήμ, which means "Jerusalem." The phrase thus means "the ones who live in Jerusalem."

- **καὶ οἱ ἄρχοντες αὐτῶν**: The conjunction καὶ means "and." The noun ἄρχοντες is the nominative plural of ἄρχων, which means "rulers" or "leaders." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "and their rulers."

- **τοῦτον ἀγνοήσαντες**: The pronoun τοῦτον is the accusative singular of οὗτος, meaning "this." The participle ἀγνοήσαντες is the aorist active nominative plural of ἀγνοέω, meaning "not knowing" or "ignoring." The phrase thus means "not knowing this."

- **καὶ τὰς φωνὰς τῶν προφητῶν**: The conjunction καὶ means "and." The definite article τὰς is the accusative plural of ὁ, meaning "the." The noun φωνὰς is the accusative plural of φωνή, which means "voices" or "words." The genitive plural τῶν is the genitive plural of ὁ, meaning "of." The noun προφητῶν is the genitive plural of προφήτης, meaning "prophets." The phrase thus means "and the voices of the prophets."

- **τὰς κατὰ πᾶν σάββατον ἀναγινωσκομένας**: The definite article τὰς is the accusative plural of ὁ, meaning "the." The present participle ἀναγινωσκομένας is the present passive accusative plural of ἀναγινώσκω, meaning "being read." The preposition κατὰ means "according to" or "on." The adverb πᾶν means "every" or "all." The noun σάββατον is the accusative singular of σάββατον, meaning "Sabbath." The phrase thus means "the ones being read according to every Sabbath."

- **κρίναντες**: The participle κρίναντες is the aorist active nominative plural of κρίνω, meaning "judging" or "deciding."

- **ἐπλήρωσαν**: The verb ἐπλήρωσαν is the aorist active indicative third person plural of πληρόω, meaning "they fulfilled" or "they completed."

The entire verse can be translated as: "For those who live in Jerusalem and their rulers, not knowing this and the voices of the prophets which are read every Sabbath, fulfilled them by judging."