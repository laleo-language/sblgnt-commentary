---
version: 1
---
- **Τότε ἔδοξε**: The word Τότε means "then" or "at that time." The verb ἔδοξε is the third person singular aorist indicative of δοκέω, meaning "it seemed good" or "it was decided." The phrase thus means "Then it seemed good."

- **τοῖς ἀποστόλοις καὶ τοῖς πρεσβυτέροις**: The word τοῖς is the dative plural of the definite article ὁ, meaning "to the." The noun ἀποστόλοις is the dative plural of ἀπόστολος, which means "apostles." The conjunction καὶ means "and." The noun τοῖς is the dative plural of ὁ, meaning "to the." The noun πρεσβυτέροις is the dative plural of πρεσβύτερος, which means "elders." The phrase thus means "to the apostles and the elders."

- **σὺν ὅλῃ τῇ ἐκκλησίᾳ**: The preposition σὺν means "with." The adjective ὅλῃ is the dative singular feminine form of ὅλος, meaning "whole" or "entire." The noun τῇ is the dative singular of the definite article ὁ, meaning "the." The noun ἐκκλησίᾳ is the dative singular of ἐκκλησία, which means "church." The phrase thus means "with the whole church."

- **ἐκλεξαμένους ἄνδρας ἐξ αὐτῶν**: The verb ἐκλεξαμένους is the accusative plural masculine form of ἐκλέγω, meaning "choosing" or "selecting." The noun ἄνδρας is the accusative plural masculine form of ἀνήρ, which means "men" or "people." The preposition ἐξ means "from" or "out of." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "of them." The phrase thus means "choosing men from among them."

- **πέμψαι εἰς Ἀντιόχειαν**: The verb πέμψαι is the aorist infinitive of πέμπω, meaning "to send." The preposition εἰς means "to" or "into." The noun Ἀντιόχειαν is the accusative singular of Ἀντιόχεια, which is a place name. The phrase thus means "to send to Antioch."

- **σὺν τῷ Παύλῳ καὶ Βαρναβᾷ**: The preposition σὺν means "with." The definite article τῷ is the dative singular of ὁ, meaning "the." The noun Παύλῳ is the dative singular of Παῦλος, which is a personal name. The conjunction καὶ means "and." The noun Βαρναβᾷ is the dative singular of Βαρναβᾶς, which is a personal name. The phrase thus means "with Paul and Barnabas."

- **Ἰούδαν τὸν καλούμενον Βαρσαββᾶν καὶ Σιλᾶν**: The noun Ἰούδαν is the accusative singular of Ἰούδας, which is a personal name. The definite article τὸν is the accusative singular of ὁ, meaning "the." The participle καλούμενον is the accusative singular masculine form of καλέω, meaning "called" or "named." The noun Βαρσαββᾶν is the accusative singular of Βαρσαββᾶς, which is a personal name. The conjunction καὶ means "and." The noun Σιλᾶν is the accusative singular of Σίλας, which is a personal name. The phrase thus means "Judas called Barsabbas and Silas."

- **ἄνδρας ἡγουμένους ἐν τοῖς ἀδελφοῖς**: The noun ἄνδρας is the accusative plural masculine form of ἀνήρ, which means "men" or "people." The participle ἡγουμένους is the accusative plural masculine form of ἡγέομαι, meaning "leading" or "considering." The preposition ἐν means "among" or "in." The noun τοῖς is the dative plural of the definite article ὁ, meaning "the." The noun ἀδελφοῖς is the dative plural of ἀδελφός, which means "brothers" or "believers." The phrase thus means "leading men among the brothers."

The sentence can be translated as: "Then it seemed good to the apostles and the elders, with the whole church, to choose men from among them and send them to Antioch with Paul and Barnabas, Judas called Barsabbas, and Silas, leading men among the brothers."

This verse describes a decision made by the apostles, elders, and the whole church to select men from among them and send them to Antioch with Paul, Barnabas, Judas called Barsabbas, and Silas, who were considered leaders among the brothers.