---
version: 1
---
- **ὡς ἔτεσι τετρακοσίοις καὶ πεντήκοντα**: The word ὡς is an adverb meaning "about" or "approximately." The noun ἔτεσι is the dative plural of ἔτος, meaning "year." The adjective τετρακοσίοις is the dative plural of τετρακόσιοι, meaning "four hundred." The conjunction καὶ means "and." The adjective πεντήκοντα is the dative plural of πεντήκοντα, meaning "fifty." The phrase thus means "about four hundred and fifty years."

- **καὶ μετὰ ταῦτα**: The conjunction καὶ means "and." The preposition μετὰ means "after." The demonstrative pronoun ταῦτα means "this" or "these." The phrase thus means "and after this."

- **ἔδωκεν κριτὰς ἕως Σαμουὴλ**: The verb ἔδωκεν is the third person singular aorist indicative of δίδωμι, meaning "he gave." The noun κριτὰς is the accusative plural of κριτής, meaning "judges." The preposition ἕως means "until." The genitive Σαμουὴλ is the genitive form of Σαμουήλ, which is the name "Samuel." The phrase thus means "he gave judges until Samuel."

- **τοῦ προφήτου**: The genitive τοῦ is the genitive form of ὁ, meaning "of the." The noun προφήτου is the genitive singular of προφήτης, meaning "prophet." The phrase thus means "of the prophet."

The literal translation of Acts 13:20 is: "About four hundred and fifty years. And after this, he gave judges until Samuel the prophet."