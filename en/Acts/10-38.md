---
version: 1
---
- **Ἰησοῦν τὸν ἀπὸ Ναζαρέθ**: The word Ἰησοῦν is the accusative singular of Ἰησοῦς, which means "Jesus." The word τὸν is the accusative singular of the definite article ὁ, which means "the." The phrase τὸν ἀπὸ Ναζαρέθ means "the one from Nazareth."

- **ὡς ἔχρισεν αὐτὸν ὁ θεὸς πνεύματι ἁγίῳ καὶ δυνάμει**: The word ὡς means "as" or "when." The verb ἔχρισεν is the third person singular aorist indicative of χρίω, which means "he anointed." The pronoun αὐτὸν is the accusative singular of αὐτός, which means "him." The definite article ὁ means "the." The noun θεὸς means "God." The noun πνεῦματι means "spirit." The adjective ἁγίῳ means "holy." The conjunction καὶ means "and." The noun δυνάμει means "power" or "might." The phrase πνεύματι ἁγίῳ καὶ δυνάμει means "with the Holy Spirit and power."

- **ὃς διῆλθεν εὐεργετῶν**: The pronoun ὃς is the nominative singular of ὅς, which means "who" or "he who." The verb διῆλθεν is the third person singular aorist indicative of διέρχομαι, which means "he went through" or "he passed by." The participle εὐεργετῶν is the present active participle of εὐεργετέω, which means "doing good" or "benefitting."

- **καὶ ἰώμενος πάντας τοὺς καταδυναστευομένους ὑπὸ τοῦ διαβόλου**: The conjunction καὶ means "and." The participle ἰώμενος is the present middle participle of ἰάομαι, which means "healing" or "curing." The adjective πάντας means "all" or "everyone." The definite article τοὺς means "the." The participle καταδυναστευομένους is the present middle participle of καταδυναστεύω, which means "oppressed" or "tyrannized." The preposition ὑπὸ means "by" or "under." The definite article τοῦ means "the." The noun διαβόλου means "devil" or "slanderer."

- **ὅτι ὁ θεὸς ἦν μετʼ αὐτοῦ**: The conjunction ὅτι means "because" or "that." The noun θεὸς means "God." The verb ἦν is the third person singular imperfect indicative of εἰμί, which means "he was." The preposition μετʼ means "with." The pronoun αὐτοῦ is the genitive singular of αὐτός, which means "him." The phrase μετʼ αὐτοῦ means "with him."

The literal translation of the entire verse is: "Jesus, the one from Nazareth, how God anointed him with the Holy Spirit and power, who went about doing good and healing all who were oppressed by the devil because God was with him."