---
version: 1
---
- **ἀντιλεγόντων δὲ τῶν Ἰουδαίων**: The word ἀντιλεγόντων is the genitive plural
  masculine present participle of ἀντιλέγω, which means "to contradict" or
  "to argue against." The word τῶν Ἰουδαίων is the genitive plural of Ἰουδαῖος,
  which means "Jew" or "Jewish." The phrase thus means "while the Jews were
  contradicting."

- **ἠναγκάσθην ἐπικαλέσασθαι Καίσαρα**: The word ἠναγκάσθην is the first person
  singular aorist passive indicative of ἀναγκάζω, which means "I was compelled" or
  "I was forced." The infinitive ἐπικαλέσασθαι is the aorist infinitive of
  ἐπικαλέομαι, which means "to appeal to" or "to invoke." The noun Καίσαρα is
  the accusative singular of Καῖσαρ, which means "Caesar." The phrase thus means
  "I was compelled to appeal to Caesar."

- **οὐχ ὡς τοῦ ἔθνους μου ἔχων τι κατηγορεῖν**: The word οὐχ is a negative particle
  meaning "not." The word ὡς is a conjunction meaning "as" or "like." The word
  τοῦ ἔθνους is the genitive singular of ἔθνος, which means "nation" or "people."
  The word μου is the genitive singular of ἐγώ, which means "I" or "my." The verb
  ἔχων is the present active participle of ἔχω, which means "having" or "holding."
  The noun τι is an indefinite pronoun meaning "something" or "anything." The
  verb κατηγορεῖν is the present active infinitive of κατηγορέω, which means "to
  accuse" or "to charge." The phrase thus means "not having anything to accuse
  against my nation."

The entire verse, Acts 28:19, can be literally translated as: "But while the Jews were contradicting, I was compelled to appeal to Caesar, not having anything to accuse against my nation."