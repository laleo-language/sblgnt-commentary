---
version: 1
---
- **συνεκίνησάν τε τὸν λαὸν**: The verb συνεκίνησάν is the third person plural aorist indicative active of συνκινέω, which means "to stir up together" or "to agitate." The word τε is a conjunction that means "and." The noun τὸν λαὸν is the accusative singular of λαός, which means "the people." The phrase thus means "they stirred up the people."

- **καὶ τοὺς πρεσβυτέρους καὶ τοὺς γραμματεῖς**: The conjunction καὶ means "and." The noun τοὺς πρεσβυτέρους is the accusative plural of πρεσβύτερος, which means "the elders." The noun τοὺς γραμματεῖς is the accusative plural of γραμματεύς, which means "the scribes." The phrase thus means "and the elders and the scribes."

- **καὶ ἐπιστάντες συνήρπασαν αὐτὸν**: The conjunction καὶ means "and." The participle ἐπιστάντες is the nominative plural masculine of ἐπιστάντες, which means "standing by" or "coming forward." The verb συνήρπασαν is the third person plural aorist indicative active of συρπάω, which means "to seize" or "to snatch." The pronoun αὐτὸν is the accusative singular masculine of αὐτός, which means "him." The phrase thus means "and standing by, they seized him."

- **καὶ ἤγαγον εἰς τὸ συνέδριον**: The conjunction καὶ means "and." The verb ἤγαγον is the third person plural aorist indicative active of ἄγω, which means "they led" or "they brought." The preposition εἰς means "to" or "into." The article τὸ is the accusative singular neuter of ὁ, which means "the." The noun συνέδριον is the accusative singular neuter of συνέδριον, which means "council" or "assembly." The phrase thus means "and they brought him into the council."

The literal translation of Acts 6:12 is: "They stirred up the people and the elders and the scribes, and standing by, they seized him and brought him into the council."