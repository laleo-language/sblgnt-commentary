---
version: 1
---
- **Ἡμεῖς δὲ**: The word Ἡμεῖς is the nominative plural pronoun meaning "we" or "us." The particle δέ is a conjunction that can be translated as "but" or "and." The phrase thus means "But we."

- **τὸν πλοῦν διανύσαντες**: The article τὸν is the accusative singular masculine definite article. The noun πλοῦν is the accusative singular of πλοῦς, which means "voyage" or "journey." The participle διανύσαντες is the nominative plural masculine aorist active participle of διανύω, meaning "having completed" or "having finished." The phrase can be translated as "having completed the voyage."

- **ἀπὸ Τύρου**: The preposition ἀπὸ means "from." Τύρου is the genitive singular of Τύρος, which means "Tyre." The phrase can be translated as "from Tyre."

- **κατηντήσαμεν εἰς Πτολεμαΐδα**: The verb κατηντήσαμεν is the first person plural aorist indicative of καταντάω, meaning "we arrived" or "we came." The preposition εἰς means "to" or "into." Πτολεμαΐδα is the accusative singular of Πτολεμαΐς, which means "Ptolemais." The phrase can be translated as "we arrived into Ptolemais."

- **καὶ ἀσπασάμενοι τοὺς ἀδελφοὺς**: The conjunction καί means "and." The participle ἀσπασάμενοι is the nominative plural masculine aorist middle participle of ἀσπάζομαι, meaning "having greeted" or "having embraced." The article τοὺς is the accusative plural masculine definite article. The noun ἀδελφοὺς is the accusative plural of ἀδελφός, which means "brothers" or "brethren." The phrase can be translated as "and having greeted the brothers."

- **ἐμείναμεν ἡμέραν μίαν παρʼ αὐτοῖς**: The verb ἐμείναμεν is the first person plural aorist indicative of μένω, meaning "we stayed" or "we remained." The accusative noun ἡμέραν is the accusative singular of ἡμέρα, which means "day." The adjective μίαν is the accusative singular feminine form of εἷς, which means "one." The preposition παρʼ means "with" or "beside." The pronoun αὐτοῖς is the dative plural masculine form of αὐτός, which means "them." The phrase can be translated as "we stayed one day with them."

The literal translation of Acts 21:7 is: "But we, having completed the voyage from Tyre, arrived into Ptolemais, and having greeted the brothers, we stayed one day with them."