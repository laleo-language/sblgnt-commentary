---
version: 1
---
- **Ὡς δὲ ἐπληροῦντο ἡμέραι ἱκαναί**: The word Ὡς is a conjunction meaning "as" or "when." The word δὲ is a conjunction meaning "but" or "and." The verb ἐπληροῦντο is the third person plural imperfect indicative of πληρόω, meaning "they were being filled" or "they were coming to an end." The noun ἡμέραι is the nominative plural of ἡμέρα, meaning "days." The adjective ἱκαναί is the nominative plural of ἱκανός, meaning "sufficient" or "enough." The phrase thus means "But as the days were coming to an end."

- **συνεβουλεύσαντο οἱ Ἰουδαῖοι ἀνελεῖν αὐτόν**: The verb συνεβουλεύσαντο is the third person plural aorist middle indicative of συμβουλεύω, meaning "they plotted" or "they conspired." The article οἱ is the nominative plural masculine definite article, meaning "the." The noun Ἰουδαῖοι is the nominative plural masculine of Ἰουδαῖος, meaning "Jews." The verb ἀνελεῖν is the aorist infinitive of ἀναιρέω, meaning "to kill" or "to take away." The pronoun αὐτόν is the accusative singular masculine pronoun, meaning "him." The phrase thus means "the Jews plotted to kill him."

The literal translation of Acts 9:23 is: "But as the days were coming to an end, the Jews plotted to kill him."