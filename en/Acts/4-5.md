---
version: 1
---
- **Ἐγένετο δὲ ἐπὶ τὴν αὔριον**: The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, which means "it happened" or "it occurred." The preposition ἐπὶ here means "on" or "upon." The article τὴν is the accusative singular feminine of the definite article ὁ, which means "the." The noun αὔριον means "the next day." The phrase thus means "And it happened on the next day."

- **συναχθῆναι αὐτῶν τοὺς ἄρχοντας**: The verb συναχθῆναι is the aorist passive infinitive of συνάγω, which means "to gather" or "to assemble." The pronoun αὐτῶν is the genitive plural of αὐτός, which means "their." The noun τοὺς ἄρχοντας is the accusative plural of ἄρχων, which means "rulers" or "authorities." The phrase thus means "their rulers were gathered."

- **καὶ τοὺς πρεσβυτέρους**: The conjunction καὶ means "and." The article τοὺς is the accusative plural masculine of the definite article ὁ. The noun πρεσβυτέρους is the accusative plural masculine of πρεσβύτερος, which means "elders" or "presbyters." The phrase thus means "and the elders."

- **καὶ τοὺς γραμματεῖς**: The conjunction καὶ means "and." The article τοὺς is the accusative plural masculine of the definite article ὁ. The noun γραμματεῖς is the accusative plural masculine of γραμματεύς, which means "scribes" or "teachers of the law." The phrase thus means "and the scribes."

- **ἐν Ἰερουσαλήμ**: The preposition ἐν means "in." The noun Ἰερουσαλήμ is the accusative singular of Ἱερουσαλήμ, which means "Jerusalem." The phrase thus means "in Jerusalem."

The entire verse can be literally translated as: "And it happened on the next day, their rulers were gathered, and the elders and the scribes, in Jerusalem."