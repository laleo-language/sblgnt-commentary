---
version: 1
---
- **Κρατοῦντος δὲ αὐτοῦ τὸν Πέτρον καὶ τὸν Ἰωάννην**: The participle Κρατοῦντος is the genitive singular masculine of the verb Κρατέω, meaning "to hold" or "to seize." It is declined to agree with the genitive singular masculine pronoun αὐτοῦ, which means "his." The accusative singular masculine definite article τὸν is used with the proper names Πέτρον (Peter) and Ἰωάννην (John) to indicate that they are the direct objects of the verb. The phrase thus means "While he was holding onto Peter and John."

- **συνέδραμεν πᾶς ὁ λαὸς πρὸς αὐτοὺς**: The verb συνέδραμεν is the aorist indicative third person singular of the verb συντρέχω, meaning "to run together" or "to come together." It is used here with the subject πᾶς ὁ λαὸς, which means "all the people." The preposition πρὸς is used with the pronoun αὐτοὺς, which means "to them." The phrase thus means "All the people ran together to them."

- **ἐπὶ τῇ στοᾷ τῇ καλουμένῃ Σολομῶντος**: The preposition ἐπὶ is used with the dative case of τῇ στοᾷ, which means "at the porch." The definite article τῇ is used to indicate that στοᾷ is a specific porch. The phrase καλουμένῃ Σολομῶντος is a genitive absolute construction, where the genitive Σολομῶντος is declined to agree with the noun it modifies, καλουμένῃ. The phrase καλουμένῃ Σολομῶντος means "called Solomon's." The phrase thus means "At the porch called Solomon's."

- **ἔκθαμβοι**: This is an adjective in the nominative plural masculine form, agreeing with the noun λαὸς (people). It means "amazed" or "astonished." The phrase thus means "Amazed."

The literal translation of the entire verse would be: "While he was holding onto Peter and John, all the people ran together to them at the porch called Solomon's, amazed."