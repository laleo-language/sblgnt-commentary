---
version: 1
---
- **καὶ νῦν ἰδοὺ**: The word καὶ is a conjunction meaning "and." The word νῦν is an adverb meaning "now." The word ἰδοὺ is an interjection meaning "behold" or "look." The phrase thus means "and now, behold."

- **δεδεμένος ἐγὼ**: The word δεδεμένος is the nominative singular masculine participle of δέω, meaning "bound" or "tied up." The pronoun ἐγὼ is the first person singular pronoun, meaning "I" or "me." The phrase thus means "I am bound."

- **τῷ πνεύματι πορεύομαι**: The word τῷ is the dative singular masculine definite article, meaning "the." The word πνεύματι is the dative singular neuter form of πνεῦμα, meaning "spirit." The verb πορεύομαι is the first person singular present indicative middle/passive of πορεύομαι, meaning "I am going" or "I am traveling." The phrase thus means "I am going with the spirit."

- **εἰς Ἰερουσαλήμ**: The word εἰς is a preposition meaning "to" or "towards." The word Ἰερουσαλήμ is the accusative singular form of Ἱεροσόλυμα, meaning "Jerusalem." The phrase thus means "to Jerusalem."

- **τὰ ἐν αὐτῇ**: The word τὰ is the accusative plural neuter definite article, meaning "the." The preposition ἐν means "in" or "within." The pronoun αὐτῇ is the dative singular feminine pronoun, meaning "it" or "her." The phrase thus means "the things in it" or "the things within it."

- **συναντήσοντά μοι**: The verb συναντήσοντά is the accusative plural masculine participle of συναντάω, meaning "to meet" or "to encounter." The pronoun μοι is the dative singular pronoun, meaning "to me." The phrase thus means "the things that will meet me."

- **μὴ εἰδώς**: The word μὴ is a negative particle meaning "not." The verb εἰδώς is the nominative singular masculine participle of οἶδα, meaning "knowing" or "being aware." The phrase thus means "not knowing."

The entire verse can be literally translated as: "And now, behold, I am bound in the spirit, going to Jerusalem, not knowing the things that will meet me there."