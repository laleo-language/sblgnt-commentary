---
version: 1
---
- **Μετὰ ταῦτα**: The word Μετὰ is a preposition meaning "after" or "later," and ταῦτα is a pronoun meaning "these" or "these things." So the phrase means "after these things."

- **ἀναστρέψω**: This is the first person singular future indicative of the verb ἀναστρέφω, meaning "I will return" or "I will restore."

- **καὶ ἀνοικοδομήσω**: The word καὶ is a conjunction meaning "and," and ἀνοικοδομήσω is the first person singular future indicative of the verb ἀνοικοδομέω, meaning "I will rebuild" or "I will restore."

- **τὴν σκηνὴν Δαυὶδ**: The word τὴν is the definite article in the accusative singular form, and σκηνὴν is the accusative singular form of σκηνή, meaning "tent" or "tabernacle." The name Δαυὶδ is the genitive form of Δαυίδ, which means "David." So the phrase means "the tent of David."

- **τὴν πεπτωκυῖαν**: The word τὴν is the definite article in the accusative singular form, and πεπτωκυῖαν is the accusative singular feminine participle form of the verb πίπτω, meaning "fallen" or "collapsed." So the phrase means "the fallen."

- **καὶ τὰ κατεσκαμμένα αὐτῆς**: The word καὶ is a conjunction meaning "and," and τὰ is the neuter plural definite article in the nominative or accusative form. The adjective κατεσκαμμένα is the neuter plural form of the verb κατασκάπτω, meaning "destroyed" or "ruined." The pronoun αὐτῆς is the genitive form of αὐτός, meaning "its." So the phrase means "and the destroyed ones of it."

- **ἀνορθώσω αὐτήν**: The word ἀνορθώσω is the first person singular future indicative of the verb ἀνορθόω, meaning "I will restore" or "I will rebuild." The pronoun αὐτήν is the accusative singular form of αὐτός, meaning "it" or "her." So the phrase means "I will restore it."

Literal Translation: After these things, I will return and restore the tent of David, the fallen one, and I will rebuild its destroyed ones and restore it.

In this verse, the phrase "the tent of David" refers to the tabernacle or dwelling place of David, which symbolizes the kingdom of David. The phrase "the fallen one" and "its destroyed ones" refer to the state of decline and ruin that the kingdom of David has experienced. The speaker is expressing their intention to restore and rebuild the kingdom of David.