---
version: 1
---
- **κἀκεῖθεν εἰς Φιλίππους**: The word κἀκεῖθεν is a contraction of καὶ ἐκεῖθεν, which means "and from there." The preposition εἰς means "to." The noun Φιλίππους is the genitive singular of Φίλιππος, which means "Philippi." The phrase thus means "and from there to Philippi."

- **ἥτις ἐστὶν πρώτη τῆς μερίδος Μακεδονίας πόλις, κολωνία**: The relative pronoun ἥτις, which means "which," introduces a subordinate clause describing Φιλίππους. The verb ἐστὶν is the third person singular present indicative of εἰμί, which means "is." The adjective πρώτη, which means "first," agrees with the feminine noun πόλις, which means "city." The genitive phrase τῆς μερίδος Μακεδονίας means "of the region of Macedonia." The noun κολωνία, which means "colony," further describes Φιλίππους. The phrase thus means "which is a first city of the region of Macedonia, a colony."

- **ἦμεν δὲ ἐν ταύτῃ τῇ πόλει διατρίβοντες ἡμέρας τινάς**: The verb ἦμεν is the first person plural imperfect indicative of εἰμί, meaning "we were." The conjunction δὲ means "and" or "but." The preposition ἐν means "in." The demonstrative pronoun ταύτῃ, which means "this," refers to Φιλίππους. The definite article τῇ is the dative singular feminine of ὁ, meaning "the." The noun πόλει, which means "city," is in the dative case. The participle διατρίβοντες, which means "staying," agrees with the subject ἡμέρας, which means "days." The adjective τινάς means "some." The phrase thus means "But we were in this city, staying for some days."

The literal translation of Acts 16:12 is: "And from there to Philippi, which is a first city of the region of Macedonia, a colony. But we were in this city, staying for some days."