---
version: 1
---
- **εἶπεν δὲ ὁ Παῦλος**: The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The subject of the verb is ὁ Παῦλος, which means "Paul." The phrase thus means "Paul said."

- **Ἐγὼ ἄνθρωπος μέν εἰμι Ἰουδαῖος**: The word ἐγὼ means "I" or "me." The word ἄνθρωπος means "man" or "person." The word μέν is a particle that introduces a contrast or continuation of thought. The word εἰμι is the first person singular present indicative of εἰμί, meaning "I am." The word Ἰουδαῖος means "Jewish." The phrase thus means "I am a Jewish man."

- **Ταρσεὺς τῆς Κιλικίας**: The word Ταρσεὺς is a proper noun, referring to the city of Tarsus. The phrase τῆς Κιλικίας is a genitive phrase that means "of Cilicia." The phrase thus means "from Tarsus of Cilicia."

- **οὐκ ἀσήμου πόλεως πολίτης**: The word οὐκ means "not." The word ἀσήμου is the genitive singular of ἀσήμους, meaning "unimportant" or "unknown." The word πόλεως is the genitive singular of πόλις, meaning "city." The word πολίτης means "citizen." The phrase thus means "not of an unimportant city citizen."

- **δέομαι δέ σου**: The verb δέομαι is the first person singular present indicative of δέομαι, meaning "I ask" or "I beg." The word δέ is a particle that adds emphasis. The word σου means "you." The phrase thus means "I ask you."

- **ἐπίτρεψόν μοι λαλῆσαι πρὸς τὸν λαόν**: The verb ἐπίτρεψόν is the second person singular aorist imperative of ἐπιτρέπω, meaning "allow" or "permit." The word μοι means "me." The word λαλῆσαι is the aorist infinitive of λαλέω, meaning "to speak." The preposition πρὸς means "to" or "towards." The article τὸν is the accusative singular masculine definite article. The word λαόν means "people." The phrase thus means "Allow me to speak to the people."

- **Paul said, "I am a Jewish man from Tarsus of Cilicia, not of an unimportant city citizen. I ask you, allow me to speak to the people."**

The sentence in Acts 21:39 is a statement made by Paul, where he identifies himself as a Jewish man from the city of Tarsus in the region of Cilicia. He emphasizes that he is not an unimportant citizen of a city and requests permission to speak to the people.