---
version: 1
---
- **ἀπεκρίθη δὲ φωνὴ ἐκ δευτέρου ἐκ τοῦ οὐρανοῦ**: The verb ἀπεκρίθη is the third person singular aorist indicative of ἀποκρίνομαι, meaning "he answered" or "he responded." The noun φωνὴ is the nominative singular of φωνή, which means "voice." The phrase ἐκ δευτέρου ἐκ τοῦ οὐρανοῦ means "a second time from heaven."

- **Ἃ ὁ θεὸς ἐκαθάρισεν**: The pronoun Ἃ is the accusative plural neuter of ὅς, meaning "which" or "what." The verb ἐκαθάρισεν is the third person singular aorist indicative of καθαρίζω, meaning "he cleansed" or "he purified." The phrase ὁ θεὸς ἐκαθάρισεν means "God cleansed."

- **σὺ μὴ κοίνου**: The pronoun σὺ is the second person singular pronoun, meaning "you." The particle μὴ is a negation, meaning "not." The verb κοίνου is the second person singular present imperative of κοινόω, meaning "make common" or "profane." The phrase σὺ μὴ κοίνου means "you do not make common" or "you do not profane."

The sentence can be literally translated as: "And a voice answered a second time from heaven, 'What God has cleansed, you do not make common.'"