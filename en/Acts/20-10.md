---
version: 1
---
- **καταβὰς δὲ ὁ Παῦλος**: The participle καταβὰς is the nominative singular masculine of καταβαίνω, meaning "coming down." The definite article ὁ indicates that it is the subject of the sentence and refers to Paul. The phrase means "Paul, coming down."

- **ἐπέπεσεν αὐτῷ**: The verb ἐπέπεσεν is the third person singular aorist indicative of ἐπιπίπτω, meaning "he fell upon." The pronoun αὐτῷ is the dative singular masculine of αὐτός, meaning "to him." The phrase means "he fell upon him."

- **καὶ συμπεριλαβὼν εἶπεν**: The conjunction καὶ connects this phrase to the previous one. The participle συμπεριλαβὼν is the nominative singular masculine of συμπεριλαμβάνω, meaning "having embraced." The verb εἶπεν is the third person singular aorist indicative of λέγω, meaning "he said." The phrase means "and having embraced, he said."

- **Μὴ θορυβεῖσθε**: The negation μή indicates that the verb is in the imperative mood and means "do not." The verb θορυβεῖσθε is the second person plural present passive imperative of θορυβέω, meaning "be troubled" or "be disturbed." The phrase means "do not be troubled."

- **ἡ γὰρ ψυχὴ αὐτοῦ ἐν αὐτῷ ἐστιν**: The definite article ἡ indicates that ψυχή is the subject of the sentence and means "the soul." The pronoun αὐτοῦ is the genitive singular masculine of αὐτός, meaning "his." The pronoun αὐτῷ is the dative singular masculine of αὐτός, meaning "in him." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "it is." The phrase means "for his soul is in him."

Literal translation: "Paul, coming down, fell upon him and having embraced, he said, 'Do not be troubled, for his soul is in him.'"

In this verse, Paul falls upon a young man who had fallen asleep and died during his long sermon. Paul reassures the crowd, telling them not to be troubled, as the young man's soul is still in him.