---
version: 1
---
- **ἅμα καὶ**: The phrase ἅμα καὶ is made up of the adverb ἅμα, which means "together" or "at the same time," and the conjunction καὶ, which means "and." The phrase together means "at the same time" or "also."

- **ἐλπίζων**: The verb ἐλπίζων is the present active participle of ἐλπίζω, which means "I hope" or "I expect." The participle form indicates that the subject is also the one performing the action of hoping or expecting. The word here means "hoping" or "expecting."

- **ὅτι**: The word ὅτι is a conjunction that means "that." It is used to introduce a subordinate clause expressing indirect speech or thought. In this context, it introduces the content of what the subject is hoping or expecting. 

- **χρήματα**: The noun χρήματα is the accusative plural of χρῆμα, which means "money" or "property." It refers to financial resources or possessions. The word here means "money."

- **δοθήσεται**: The verb δοθήσεται is the third person singular future passive indicative of δίδωμι, which means "I give" or "I will give." The future passive form indicates that the subject will receive the action of the verb. The word here means "will be given."

- **αὐτῷ**: The pronoun αὐτῷ is the dative singular masculine of αὐτός, which means "he" or "him." It functions as the indirect object of the verb δοθήσεται, indicating to whom the money will be given. The word here means "to him."

- **ὑπὸ τοῦ ⸀Παύλου**: The preposition ὑπὸ means "by" or "from." It indicates the source or agent of the action. The article τοῦ is the genitive singular masculine of ὁ, which means "the." The name Παύλου is the genitive singular of Παῦλος, which means "Paul." The phrase ὑπὸ τοῦ Παύλου means "by Paul" or "from Paul."

- **διὸ καὶ**: The phrase διὸ καὶ is made up of the conjunction διὸ, which means "therefore" or "for this reason," and the conjunction καὶ, which means "and." The phrase together means "therefore also" or "for this reason also."

- **πυκνότερον**: The adjective πυκνότερον is the accusative singular neuter of πυκνός, which means "dense" or "frequent." It describes the frequency or intensity of an action. The word here means "more frequently."

- **αὐτὸν**: The pronoun αὐτὸν is the accusative singular masculine of αὐτός, which means "he" or "him." It functions as the direct object of the verb μεταπεμπόμενος, indicating who is being sent for. The word here means "him."

- **μεταπεμπόμενος**: The verb μεταπεμπόμενος is the present middle participle of μεταπέμπομαι, which means "I send for" or "I summon." The participle form indicates that the subject is also the one performing the action of sending or summoning. The word here means "sending for" or "summoning."

- **ὡμίλει**: The verb ὡμίλει is the third person singular imperfect indicative of ὁμιλέω, which means "I converse" or "I talk with." The imperfect form indicates ongoing or repeated action in the past. The word here means "he talked" or "he conversed."

- **αὐτῷ**: The pronoun αὐτῷ is the dative singular masculine of αὐτός, which means "he" or "him." It functions as the indirect object of the verb ὡμίλει, indicating with whom the subject talked. The word here means "with him."

A possible literal translation of the entire verse could be: "At the same time, also hoping that money will be given to him by Paul; therefore, also sending for him more frequently, he talked with him."