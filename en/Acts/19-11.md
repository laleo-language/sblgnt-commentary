---
version: 1
---
- **Δυνάμεις τε**: The noun δυνάμεις is the accusative plural of δύναμις, which means "powers" or "miracles." The particle τε is a conjunction that means "and." So this phrase means "miracles and."
- **οὐ τὰς τυχούσας**: The particle οὐ is a negation, meaning "not." The article τὰς is the accusative plural feminine definite article, meaning "the." The verb τυχούσας is the accusative plural feminine participle of τυγχάνω, which means "to happen" or "to occur." So this phrase means "not the ones happening" or "not the ones occurring."
- **ὁ θεὸς ἐποίει**: The article ὁ is the nominative singular masculine definite article, meaning "the." The noun θεὸς is the nominative singular masculine noun, meaning "God." The verb ἐποίει is the third person singular imperfect indicative of ποιέω, meaning "he was doing" or "he was performing." So this phrase means "God was doing."
- **διὰ τῶν χειρῶν Παύλου**: The preposition διὰ means "through" or "by means of." The article τῶν is the genitive plural masculine definite article, meaning "of the." The noun χειρῶν is the genitive plural feminine noun, meaning "hands." The proper noun Παύλου is the genitive singular masculine of Παῦλος, meaning "Paul." So this phrase means "through the hands of Paul."

The phrase can be translated as "miracles and not the ones happening God was doing through the hands of Paul."

The literal translation of the entire verse is: "Miracles and not the ones happening God was doing through the hands of Paul."