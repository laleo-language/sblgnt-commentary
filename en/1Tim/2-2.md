---
version: 1
---
- **ὑπὲρ βασιλέων**: The word ὑπὲρ is a preposition that means "for" or "concerning." The noun βασιλέων is the genitive plural of βασιλεύς, which means "kings." The phrase thus means "for kings."

- **καὶ πάντων τῶν ἐν ὑπεροχῇ ὄντων**: The word καὶ is a conjunction that means "and." The adjective πάντων is the genitive plural of πᾶς, which means "all." The article τῶν is the genitive plural of ὁ, which means "the." The noun ὑπεροχῇ is the dative singular of ὑπεροχή, which means "authority" or "dominion." The participle ὄντων is the genitive plural masculine of εἰμί, which means "being" or "existing." The phrase can be translated as "and of all who are in authority."

- **ἵνα ἤρεμον καὶ ἡσύχιον βίον διάγωμεν**: The word ἵνα is a conjunction that means "so that" or "in order to." The adjective ἤρεμον is the accusative singular of ἥρεμος, which means "tranquil" or "peaceful." The adjective ἡσύχιον is the accusative singular of ἡσύχιος, which means "quiet" or "calm." The noun βίον is the accusative singular of βίος, which means "life." The verb διάγωμεν is the first person plural present subjunctive of διάγω, which means "we may live" or "we may lead." The phrase can be translated as "so that we may live a tranquil and quiet life."

- **ἐν πάσῃ εὐσεβείᾳ καὶ σεμνότητι**: The preposition ἐν means "in" or "with." The adjective πάσῃ is the dative singular feminine of πᾶς, which means "all." The noun εὐσεβείᾳ is the dative singular of εὐσέβεια, which means "piety" or "godliness." The conjunction καὶ means "and." The noun σεμνότητι is the dative singular of σεμνότης, which means "reverence" or "dignity." The phrase can be translated as "in all piety and reverence."

The entire verse can be translated as: "For kings and all who are in authority, so that we may live a tranquil and quiet life in all piety and reverence."