---
version: 1
---
- **τὸ δὲ τέλος τῆς παραγγελίας**: The word τὸ is the definite article in neuter gender and singular number. The word δὲ is a conjunction meaning "but" or "and." The noun τέλος is the accusative singular form of τέλος, which means "end" or "goal." The genitive phrase τῆς παραγγελίας means "of the commandment" or "of the instruction." The phrase thus means "but the goal of the commandment."

- **ἐστὶν ἀγάπη**: The verb ἐστὶν is the third person singular present indicative form of εἰμί, which means "to be." The noun ἀγάπη is the nominative singular form of ἀγάπη, which means "love." The phrase means "is love."

- **ἐκ καθαρᾶς καρδίας**: The preposition ἐκ means "from" or "out of." The adjective καθαρᾶς is the genitive singular feminine form of καθαρός, which means "pure" or "clean." The noun καρδίας is the genitive singular form of καρδία, which means "heart." The phrase means "from a pure heart."

- **καὶ συνειδήσεως ἀγαθῆς**: The conjunction καὶ means "and." The noun συνειδήσεως is the genitive singular form of συνείδησις, which means "conscience." The adjective ἀγαθῆς is the genitive singular feminine form of ἀγαθός, which means "good" or "beneficial." The phrase means "and a good conscience."

- **καὶ πίστεως ἀνυποκρίτου**: The conjunction καὶ means "and." The noun πίστεως is the genitive singular form of πίστις, which means "faith" or "belief." The adjective ἀνυποκρίτου is the genitive singular masculine form of ἀνυπόκριτος, which means "sincere" or "without hypocrisy." The phrase means "and sincere faith."

The literal translation of the entire verse is: "But the goal of the commandment is love from a pure heart and a good conscience and sincere faith."