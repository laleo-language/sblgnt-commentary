---
version: 1
---
- **Τινῶν ἀνθρώπων**: The word Τινῶν is the genitive plural of Τις, which means "certain" or "some." The noun ἀνθρώπων is the genitive plural of ἄνθρωπος, meaning "people" or "men." The phrase thus means "of certain people" or "of some men."

- **αἱ ἁμαρτίαι**: The article αἱ is the nominative plural feminine definite article, meaning "the." The noun ἁμαρτίαι is the nominative plural of ἁμαρτία, which means "sins." The phrase means "the sins."

- **πρόδηλοί εἰσιν**: The adjective πρόδηλοί is the nominative plural masculine form of πρόδηλος, meaning "manifest" or "obvious." The verb εἰσιν is the third person plural present indicative of εἰμί, meaning "they are." The phrase means "they are obvious" or "they are manifest."

- **προάγουσαι εἰς κρίσιν**: The participle προάγουσαι is the nominative plural feminine present active participle of προάγω, meaning "leading" or "bringing." The preposition εἰς means "into" and the noun κρίσιν is the accusative singular of κρίσις, meaning "judgment." The phrase means "leading into judgment" or "bringing to judgment."

- **τισὶν δὲ καὶ ἐπακολουθοῦσιν**: The indefinite pronoun τισὶν is the dative plural form of τις, meaning "to some" or "to certain." The conjunction δέ means "but" or "and." The verb ἐπακολουθοῦσιν is the third person plural present indicative of ἐπακολουθέω, meaning "they follow" or "they accompany." The phrase means "but they also follow" or "but they also accompany."

The sentence can be translated as: "The sins of some people are obvious, leading them to judgment, but they also follow."