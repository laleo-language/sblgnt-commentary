---
version: 1
---
- **ἔχοντας τὸ μυστήριον**: The word ἔχοντας is the accusative plural of ἔχω, which means "having." The noun τὸ μυστήριον is the accusative singular of μυστήριον, meaning "mystery" or "secret." The phrase thus means "having the mystery."

- **τῆς πίστεως**: The noun τῆς πίστεως is the genitive singular of πίστις, which means "faith." The phrase means "of the faith."

- **ἐν καθαρᾷ συνειδήσει**: The preposition ἐν means "in." The adjective καθαρᾷ is the feminine singular of καθαρός, meaning "pure" or "clean." The noun συνειδήσει is the dative singular of συνείδησις, which means "conscience." The phrase means "in pure conscience."

The entire verse can be translated as: "having the mystery of the faith in a pure conscience."