---
version: 1
---
- **Πιστὸς ὁ λόγος**: The word πιστὸς is the nominative singular masculine of πιστός, which means "faithful" or "trustworthy." The article ὁ is the masculine singular nominative definite article, which means "the." The noun λόγος is the masculine singular nominative of λόγος, which means "word" or "saying." The phrase literally means "The word is faithful."

- **εἴ τις ἐπισκοπῆς ὀρέγεται**: The conjunction εἴ means "if." The indefinite pronoun τις is the nominative singular masculine of τις, which means "anyone" or "someone." The noun ἐπισκοπῆς is the genitive singular feminine of ἐπισκοπή, which means "overseer" or "bishop." The verb ὀρέγεται is the third person singular present indicative middle/passive of ὀρέγομαι, which means "he desires" or "he aspires." The phrase literally means "if anyone desires [the position of] overseer."

- **καλοῦ ἔργου ἐπιθυμεῖ**: The adjective καλοῦ is the genitive singular neuter of καλός, which means "good" or "noble." The noun ἔργου is the genitive singular neuter of ἔργον, which means "work" or "deed." The verb ἐπιθυμεῖ is the third person singular present indicative active of ἐπιθυμέω, which means "he desires" or "he longs for." The phrase literally means "he desires good work."

The entire verse, 1 Timothy 3:1, can be literally translated as: "The word is faithful; if anyone desires [the position of] overseer, he desires a good work."