---
version: 1
---
- **καὶ ὁμολογουμένως μέγα ἐστὶν τὸ τῆς εὐσεβείας μυστήριον**: The word καὶ is a conjunction that means "and." The adverb ὁμολογουμένως comes from the verb ὁμολογέω, which means "to confess" or "to acknowledge." The word μέγα is an adjective meaning "great." The verb ἐστὶν is the third person singular present indicative of εἰμί, which means "is" or "exists." The noun τὸ τῆς εὐσεβείας μυστήριον consists of the article τὸ, meaning "the," and the noun μυστήριον, which means "mystery," modified by the genitive form of the noun εὐσέβεια, meaning "godliness" or "piety." The phrase can be translated as "And confessedly great is the mystery of godliness."

- **Ὃς ἐφανερώθη ἐν σαρκί**: The relative pronoun Ὃς means "who" or "that." The verb ἐφανερώθη is the third person singular aorist passive indicative of φανερόω, which means "to reveal" or "to manifest." The preposition ἐν means "in." The noun σαρκί means "flesh." The phrase can be translated as "who was revealed in the flesh."

- **ἐδικαιώθη ἐν πνεύματι**: The verb ἐδικαιώθη is the third person singular aorist passive indicative of δικαιόω, which means "to justify" or "to declare righteous." The preposition ἐν means "in." The noun πνεύματι means "spirit." The phrase can be translated as "was justified in the spirit."

- **ὤφθη ἀγγέλοις**: The verb ὤφθη is the third person singular aorist passive indicative of ὁράω, which means "to see" or "to appear." The noun ἀγγέλοις means "angels." The phrase can be translated as "was seen by angels."

- **ἐκηρύχθη ἐν ἔθνεσιν**: The verb ἐκηρύχθη is the third person singular aorist passive indicative of κηρύσσω, which means "to proclaim" or "to preach." The preposition ἐν means "among" or "in." The noun ἔθνεσιν means "nations" or "Gentiles." The phrase can be translated as "was proclaimed among the Gentiles."

- **ἐπιστεύθη ἐν κόσμῳ**: The verb ἐπιστεύθη is the third person singular aorist passive indicative of πιστεύω, which means "to believe" or "to have faith." The preposition ἐν means "in." The noun κόσμῳ means "world." The phrase can be translated as "was believed in the world."

- **ἀνελήμφθη ἐν δόξῃ**: The verb ἀνελήμφθη is the third person singular aorist passive indicative of ἀναλαμβάνω, which means "to take up" or "to receive." The preposition ἐν means "in." The noun δόξῃ means "glory." The phrase can be translated as "was taken up in glory."

The entire verse can be translated as, "And confessedly great is the mystery of godliness: He who was revealed in the flesh, was justified in the spirit, was seen by angels, was proclaimed among the Gentiles, was believed in the world, was taken up in glory."