---
version: 1
---
- **τηρῆσαί σε τὴν ἐντολὴν**: The verb τηρῆσαί is the second person singular aorist infinitive of τηρέω, which means "to keep" or "to guard." The pronoun σε is the accusative singular form of σύ, meaning "you." The article τὴν is the accusative singular form of ὁ, which means "the." The noun ἐντολὴν is the accusative singular form of ἐντολή, which means "command" or "instruction." The phrase thus means "to keep the command."

- **ἄσπιλον ἀνεπίλημπτον**: The adjective ἄσπιλον is the accusative singular form of ἄσπιλος, which means "unblemished" or "spotless." The adjective ἀνεπίλημπτον is the accusative singular form of ἀνεπίλημπτος, which means "blameless" or "above reproach." The phrase describes the command mentioned earlier, and means "an unblemished and blameless command."

- **μέχρι τῆς ἐπιφανείας τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ**: The preposition μέχρι means "until" or "up to." The genitive noun τῆς ἐπιφανείας is the genitive singular form of ἐπιφάνεια, which means "appearance" or "coming." The genitive noun τοῦ κυρίου is the genitive singular form of κύριος, which means "lord" or "master." The genitive noun ἡμῶν is the genitive plural form of ἐγώ, meaning "our." The genitive noun Ἰησοῦ is the genitive singular form of Ἰησοῦς, which means "Jesus." The genitive noun Χριστοῦ is the genitive singular form of Χριστός, which means "Christ." The phrase describes the timeframe for keeping the command, and means "until the appearance of our Lord Jesus Christ."

- **τηρῆσαί σε τὴν ἐντολὴν ἄσπιλον ἀνεπίλημπτον μέχρι τῆς ἐπιφανείας τοῦ κυρίου ἡμῶν Ἰησοῦ Χριστοῦ**: The phrase combines all the previous phrases and means "to keep the unblemished and blameless command until the appearance of our Lord Jesus Christ."

In 1 Timothy 6:14, Paul instructs Timothy to keep the unblemished and blameless command until the appearance of our Lord Jesus Christ.