---
version: 1
---
- **Παῦλος** is the nominative singular of Παῦλος, which means "Paul."
- **δέσμιος** is the nominative singular of δέσμιος, which means "prisoner" or "captive."
- **Χριστοῦ** is the genitive singular of Χριστός, which means "Christ."
- **Ἰησοῦ** is the genitive singular of Ἰησοῦς, which means "Jesus."
- **καὶ** is a conjunction meaning "and."
- **Τιμόθεος** is the nominative singular of Τιμόθεος, which means "Timothy."
- **ὁ** is the nominative singular masculine article, meaning "the."
- **ἀδελφὸς** is the nominative singular masculine of ἀδελφός, which means "brother."
- **Φιλήμονι** is the dative singular of Φιλήμων, which means "Philemon."
- **τῷ** is the dative singular masculine article, meaning "to the."
- **ἀγαπητῷ** is the dative singular masculine of ἀγαπητός, which means "beloved."
- **καὶ** is a conjunction meaning "and."
- **συνεργῷ** is the dative singular masculine of συνεργός, which means "fellow worker."
- **ἡμῶν** is the genitive plural of ἐγώ, which means "our."

The phrase can be translated as "Paul, a prisoner of Christ Jesus, and Timothy, the brother, to Philemon, the beloved and our fellow worker."

The literal translation of the entire verse is: "Paul, a prisoner of Christ Jesus, and Timothy, the brother, to Philemon, the beloved and our fellow worker."