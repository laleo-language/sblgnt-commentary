---
version: 1
---
- **ὃν ἀνέπεμψά**: The word ὃν is the accusative singular masculine of ὅς, ἥ, ὅ, which means "whom" or "which." The verb ἀνέπεμψά is the first person singular aorist indicative of ἀναπέμπω, meaning "I have sent back" or "I have returned." The phrase thus means "whom I have sent back."

- **σοι αὐτόν**: The word σοι is the dative singular of σύ, meaning "to you." The word αὐτόν is the accusative singular masculine of αὐτός, αὐτή, αὐτό, which means "him" or "it." The phrase thus means "to you him" or "to you it."

- **τοῦτʼ ἔστιν τὰ ἐμὰ σπλάγχνα**: The word τοῦτʼ is a contraction of τοῦτο, which means "this." The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "is" or "exists." The phrase τὰ ἐμὰ σπλάγχνα consists of the article τὰ, meaning "the," the possessive pronoun ἐμὰ, meaning "my," and the noun σπλάγχνα, meaning "heart" or "innermost being." The phrase thus means "this is my heart" or "this is my innermost being."

The literal translation of the entire verse is: "Whom I have sent back to you, that is my heart."