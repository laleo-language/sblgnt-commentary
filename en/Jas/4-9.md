---
version: 1
---
- **ταλαιπωρήσατε**: The verb ταλαιπωρήσατε is the second person plural aorist imperative of ταλαιπωρέω, which means "to be afflicted" or "to be distressed." The imperative form here is a command, so it can be translated as "Be afflicted" or "Experience distress."

- **καὶ πενθήσατε**: The conjunction καὶ connects this phrase to the previous one. The verb πενθήσατε is the second person plural aorist imperative of πενθέω, which means "to mourn" or "to grieve." So this phrase can be translated as "And mourn."

- **καὶ κλαύσατε**: The conjunction καὶ connects this phrase to the previous one. The verb κλαύσατε is the second person plural aorist imperative of κλαίω, which means "to weep" or "to cry." So this phrase can be translated as "And weep."

- **ὁ γέλως ὑμῶν εἰς πένθος μετατραπήτω**: This phrase consists of a noun and two verbs. The noun γέλως means "laughter" or "joy." The pronoun ὑμῶν means "your." The preposition εἰς means "into" or "to." The noun πένθος means "mourning" or "sorrow." The verb μετατραπήτω is the third person singular aorist imperative middle/passive of μετατρέπω, which means "to change" or "to transform." So this phrase can be translated as "Let your laughter be turned into mourning."

- **καὶ ἡ χαρὰ εἰς κατήφειαν**: The conjunction καὶ connects this phrase to the previous one. The article ἡ indicates that χαρὰ is a noun, which means "joy" or "happiness." The preposition εἰς means "into" or "to." The noun κατήφεια means "dejection" or "gloom." So this phrase can be translated as "And joy into dejection."

The literal translation of the entire verse is: "Be afflicted and mourn and weep; let your laughter be turned into mourning and joy into dejection."