---
version: 1
---
- **ἀκούσατε**: The word ἀκούσατε is the second person plural aorist imperative of ἀκούω, meaning "listen" or "hear." The imperative form is used to give a command or instruction. The phrase thus means "Listen."

- **ἀδελφοί μου ἀγαπητοί**: The word ἀδελφοί is the nominative plural of ἀδελφός, which means "brothers" but can also be used more broadly to refer to a group of people. The phrase μου ἀγαπητοί means "my beloved" or "my dear." So the phrase as a whole means "my beloved brothers."

- **οὐχ ὁ θεὸς ἐξελέξατο τοὺς πτωχοὺς**: The word οὐχ is a negation, meaning "not" or "did not." The word θεὸς is the nominative singular of θεός, meaning "God." The verb ἐξελέξατο is the third person singular aorist middle indicative of ἐκλέγομαι, meaning "to choose" or "to select." The object τοὺς πτωχοὺς is the accusative plural of πτωχός, meaning "poor" or "needy." The phrase thus means "God did not choose the poor."

- **τῷ κόσμῳ πλουσίους ἐν πίστει καὶ κληρονόμους τῆς βασιλείας**: The word τῷ κόσμῳ is the dative singular of κόσμος, meaning "world" or "cosmos." The word πλουσίους is the accusative plural of πλούσιος, meaning "rich" or "wealthy." The phrase ἐν πίστει means "in faith." The word κληρονόμους is the accusative plural of κληρονόμος, meaning "heirs" or "inheritors." The phrase τῆς βασιλείας is the genitive singular of βασιλεία, meaning "kingdom." The phrase as a whole means "to the world, the rich in faith and heirs of the kingdom."

- **ἧς ἐπηγγείλατο τοῖς ἀγαπῶσιν αὐτόν**: The word ἧς is the genitive singular feminine of ὅς, meaning "which" or "whom." The verb ἐπηγγείλατο is the third person singular aorist middle indicative of ἐπαγγέλλομαι, meaning "to promise" or "to announce." The object τοῖς ἀγαπῶσιν is the dative plural of ἀγαπάω, meaning "to love." The pronoun αὐτόν is the accusative singular of αὐτός, meaning "him" or "it." The phrase thus means "which he promised to those who love him."

- **ἀκούσατε, ἀδελφοί μου ἀγαπητοί. οὐχ ὁ θεὸς ἐξελέξατο τοὺς πτωχοὺς τῷ κόσμῳ πλουσίους ἐν πίστει καὶ κληρονόμους τῆς βασιλείας ἧς ἐπηγγείλατο τοῖς ἀγαπῶσιν αὐτόν**: The literal translation of the entire verse is "Listen, my beloved brothers. Did not God choose the poor of the world to be rich in faith and heirs of the kingdom which he promised to those who love him."