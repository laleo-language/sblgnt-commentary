---
version: 1
---
- **ἰδοὺ**: The word ἰδοὺ is an interjection that can be translated as "behold" or "look." It is used to draw attention to something.

- **ὁ μισθὸς τῶν ἐργατῶν τῶν ἀμησάντων τὰς χώρας ὑμῶν**: This phrase consists of several words:
  - **ὁ μισθὸς**: The word μισθός is the nominative singular of μισθός, which means "wages" or "payment."
  - **τῶν ἐργατῶν**: The word ἐργάτης is the genitive plural of ἐργάτης, meaning "worker" or "laborer."
  - **τῶν ἀμησάντων**: The word ἀμείβω is the genitive plural participle of ἀμείβω, which means "to reap" or "to harvest." The phrase τῶν ἀμησάντων functions as an attributive participle, modifying τῶν ἐργατῶν.
  - **τὰς χώρας ὑμῶν**: The word χώρα is the accusative plural of χώρα, meaning "fields" or "land." The phrase τὰς χώρας ὑμῶν functions as the object of the participle ἀμησάντων.

- **ὁ ἀφυστερημένος ἀφʼ ὑμῶν**: This phrase consists of two words:
  - **ὁ ἀφυστερημένος**: The word ἀφυστερέω is the nominative singular participle of ἀφυστερέω, which means "to defraud" or "to withhold." The phrase ὁ ἀφυστερημένος functions as a noun and can be translated as "the one who withholds" or "the one who defrauds."
  - **ἀφʼ ὑμῶν**: The word ἀπό is a preposition meaning "from" and is combined with the pronoun ὑμῶν, which means "you" (plural). The phrase ἀφʼ ὑμῶν indicates the source or origin of the action.

- **κράζει**: The word κράζω is the third person singular present indicative of κράζω, meaning "to cry out" or "to shout."

- **καὶ αἱ βοαὶ τῶν θερισάντων**: This phrase consists of several words:
  - **αἱ βοαὶ**: The word βοὴ is the nominative plural of βοή, which means "cry" or "shout." The article αἱ indicates that it is a specific cry or shout.
  - **τῶν θερισάντων**: The word θερίζω is the genitive plural participle of θερίζω, which means "to reap" or "to harvest." The phrase τῶν θερισάντων functions as an attributive participle, modifying αἱ βοαὶ.

- **εἰς τὰ ὦτα Κυρίου Σαβαὼθ εἰσεληλύθασιν**: This phrase consists of several words:
  - **εἰς**: The word εἰς is a preposition meaning "into" or "to."
  - **τὰ ὦτα**: The word οὖς is the accusative plural of οὖς, which means "ear."
  - **Κυρίου Σαβαὼθ**: The phrase Κυρίου Σαβαὼθ can be translated as "of the Lord of hosts" or "of the Lord Almighty." Κυρίου is the genitive singular of Κύριος, meaning "Lord," and Σαβαὼθ is a transliteration of the Hebrew word for "hosts" or "armies."
  - **εἰσεληλύθασιν**: The word εἰσέρχομαι is the third person plural aorist indicative of εἰσέρχομαι, meaning "to enter" or "to come in."

The syntax of this verse is relatively straightforward. The main clause is "ὁ μισθὸς τῶν ἐργατῶν τῶν ἀμησάντων τὰς χώρας ὑμῶν κράζει" (the wages of the laborers who mowed your fields cry out). The participial phrases "τῶν ἀμησάντων τὰς χώρας ὑμῶν" (who mowed your fields) and "ὁ ἀφυστερημένος ἀφʼ ὑμῶν" (the one who withholds from you) provide additional description and clarification. The phrase "καὶ αἱ βοαὶ τῶν θερισάντων εἰς τὰ ὦτα Κυρίου Σαβαὼθ εἰσεληλύθασιν" (and the cries of the reapers have reached the ears of the Lord of hosts) adds further information about the impact and significance of the situation.

Literal translation: "Behold, the wages of the laborers who mowed your fields cry out, and the cries of the reapers have reached the ears of the Lord of hosts."