---
version: 1
---
- **εἶτα**: The word εἶτα means "then" or "next." It is an adverb that indicates the order of events.
- **ἡ ἐπιθυμία συλλαβοῦσα**: The word ἐπιθυμία is a noun that means "desire" or "lust." It is in the nominative singular form. The participle συλλαβοῦσα is the nominative singular feminine present active participle of the verb συλλαμβάνω, which means "to conceive" or "to take hold of." Together, this phrase can be translated as "the desire, having conceived."
- **τίκτει**: The word τίκτει is the third person singular present active indicative of the verb τίκτω, which means "to give birth to" or "to produce." In this sentence, it is used metaphorically to show the result of the previous action.
- **ἁμαρτίαν**: The word ἁμαρτίαν is the accusative singular of the noun ἁμαρτία, which means "sin." It is the direct object of the verb τίκτει and indicates what is being produced.
- **ἡ δὲ ἁμαρτία ἀποτελεσθεῖσα**: The word ἡ is the definite article and identifies the noun ἁμαρτία, which means "sin." It is in the nominative singular form. The participle ἀποτελεσθεῖσα is the nominative singular feminine present passive participle of the verb ἀποτελέω, which means "to complete" or "to bring to an end." Together, this phrase can be translated as "but sin, having been completed."
- **ἀποκύει**: The word ἀποκύει is the third person singular present active indicative of the verb ἀποκυέω, which means "to give birth to" or "to produce." It is used metaphorically to show the result of the previous action.
- **θάνατον**: The word θάνατον is the accusative singular of the noun θάνατος, which means "death." It is the direct object of the verb ἀποκύει and indicates what is being produced.

Literal translation: "Then desire, having conceived, gives birth to sin, and sin, having been completed, gives birth to death."

In this verse, James is explaining the process of how sin occurs. He uses the metaphor of conception and birth to illustrate that sin begins with desire and leads to death.