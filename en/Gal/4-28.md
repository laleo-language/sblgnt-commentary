---
version: 1
---
- **ὑμεῖς δέ**: The word ὑμεῖς is the nominative plural pronoun meaning "you." The particle δέ is used to contrast or add information, and can be translated as "but" or "and." The phrase thus means "but you."

- **ἀδελφοί**: The word ἀδελφοί is the nominative plural form of ἀδελφός, which means "brothers" or "brothers and sisters." The phrase means "brothers" or "siblings."

- **κατὰ Ἰσαὰκ ἐπαγγελίας**: The preposition κατὰ means "according to" or "in accordance with." The proper noun Ἰσαὰκ refers to Isaac. The genitive noun ἐπαγγελίας means "promise" or "covenant." The phrase means "according to the promise of Isaac."

- **τέκνα ἐστέ**: The noun τέκνα is the nominative plural form of τέκνον, meaning "children" or "sons." The verb ἐστέ is the second person plural present indicative form of εἰμί, which means "you are." The phrase means "you are children" or "you are sons."

The literal translation of the entire verse is: "But you, brothers, are children according to the promise of Isaac."