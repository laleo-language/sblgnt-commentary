---
version: 1
---
- **τέκνα μου**: The word τέκνα is the nominative plural of τέκνον, which means "children" or "sons." The possessive pronoun μου means "my." So the phrase means "my children."

- **οὓς πάλιν ὠδίνω**: The relative pronoun οὓς is in the accusative plural, referring back to τέκνα. The verb ὠδίνω is the first person singular present indicative of ὠδίνω, which means "I am in labor" or "I am experiencing pain." So the phrase means "whom I am again in labor for" or "whom I am again experiencing pain for."

- **μέχρις οὗ μορφωθῇ Χριστὸς ἐν ὑμῖν**: The preposition μέχρις means "until" or "up to." The relative pronoun οὗ is in the genitive case, and the verb μορφωθῇ is the third person singular aorist subjunctive passive of μορφόω, which means "to form" or "to shape." The noun Χριστὸς means "Christ" and is in the nominative case. The preposition ἐν means "in" and the pronoun ὑμῖν means "you" (plural). So the phrase means "until Christ is formed in you."

The entire verse could be literally translated as: "My children, whom I am again in labor for until Christ is formed in you."

In this verse, Paul is expressing his concern for the Galatians, whom he considers as his spiritual children. He uses the metaphor of childbirth to convey his intense desire for their spiritual growth and maturity, until they fully reflect the character and image of Christ.