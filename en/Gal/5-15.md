---
version: 1
---
- **εἰ δὲ ἀλλήλους δάκνετε καὶ κατεσθίετε**: The word εἰ is a conjunction meaning "if." The word δὲ is a conjunction meaning "but" or "and." The word ἀλλήλους is the accusative plural of ἀλλήλων, which means "one another." The verb δάκνετε is the second person plural present indicative of δάκνω, meaning "you bite." The verb κατεσθίετε is the second person plural present indicative of κατεσθίω, meaning "you consume" or "you devour."

- **βλέπετε μὴ ὑπʼ ἀλλήλων ἀναλωθῆτε**: The verb βλέπετε is the second person plural present imperative of βλέπω, meaning "you see" or "you pay attention." The particle μή is a negative particle, meaning "not." The preposition ὑπʼ means "by" or "under." The verb ἀναλωθῆτε is the second person plural aorist passive subjunctive of ἀναλίσκω, meaning "you may be consumed" or "you may be spent."

The phrase εἰ δὲ ἀλλήλους δάκνετε καὶ κατεσθίετε can be translated as "but if you bite and devour one another." The phrase βλέπετε μὴ ὑπʼ ἀλλήλων ἀναλωθῆτε can be translated as "watch out that you are not consumed by one another."

The literal translation of the entire verse is "But if you bite and devour one another, watch out that you are not consumed by one another." This verse is from Galatians 5:15.