---
version: 1
---
- **εἰ ζῶμεν πνεύματι**: The word εἰ is a conjunction meaning "if." The verb ζῶμεν is the first person plural present subjunctive of ζάω, meaning "we live." The noun πνεύματι is the dative singular of πνεῦμα, meaning "spirit." The phrase thus means "if we live by the Spirit."

- **πνεύματι καὶ στοιχῶμεν**: The noun πνεύματι is the dative singular of πνεῦμα, meaning "spirit." The conjunction καὶ means "and." The verb στοιχῶμεν is the first person plural present subjunctive of στοιχέω, meaning "we walk" or "we follow." The phrase thus means "we also walk by the Spirit."

The literal translation of the verse is: "If we live by the Spirit, let us also walk by the Spirit."