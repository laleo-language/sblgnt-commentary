---
version: 1
---
- **ὁ δὲ μεσίτης**: The word μεσίτης is the nominative singular of μεσίτης, which means "mediator" or "one who intervenes between two parties." The article ὁ indicates that it is a specific mediator. The phrase thus means "the mediator."
- **ἑνὸς οὐκ ἔστιν**: The word ἑνὸς is the genitive singular of εἷς, which means "one." The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "he/she/it is." The negation οὐκ indicates that something is not the case. The phrase thus means "is not of one."
- **ὁ δὲ θεὸς εἷς ἐστιν**: The word θεὸς is the nominative singular of θεός, which means "God." The article ὁ indicates that it is a specific God. The adjective εἷς means "one." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "he/she/it is." The phrase thus means "but God is one."

Literal translation: "The mediator is not of one, but God is one."

In this verse, Paul is emphasizing the uniqueness of God as the one true God and the contrast between a mediator (who represents multiple parties) and God (who is singular).