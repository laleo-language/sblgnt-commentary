---
version: 1
---
- **Ὁ οὖν νόμος**: The article ὁ indicates that the noun following it is definite, so it means "the law." The word οὖν is a particle that often indicates a logical connection, such as "therefore" or "so." So this phrase means "the law, therefore."

- **κατὰ τῶν ἐπαγγελιῶν τοῦ θεοῦ**: The preposition κατὰ means "according to" or "in accordance with." The genitive τῶν ἐπαγγελιῶν τοῦ θεοῦ means "of the promises of God." So this phrase means "according to the promises of God."

- **μὴ γένοιτο**: This is a negative expression that literally means "may it not be!" or "may it never happen!" It is often used to strongly deny or reject something.

- **εἰ γὰρ ἐδόθη νόμος**: The conjunction εἰ means "if." The verb ἐδόθη is the third person singular aorist passive indicative of δίδωμι, meaning "it was given." The noun νόμος means "law." So this phrase means "if the law was given."

- **ὁ δυνάμενος ζῳοποιῆσαι**: The article ὁ indicates that the noun following it is definite, so it means "the one who is able." The verb ζῳοποιῆσαι is the aorist infinitive of ζῳοποιέω, meaning "to give life" or "to make alive." So this phrase means "the one who is able to give life."

- **ὄντως ἐκ νόμου ἂν ἦν ἡ δικαιοσύνη**: The adverb ὄντως means "truly" or "indeed." The preposition ἐκ means "from." The noun νόμου means "law." The particle ἂν is often used in conditional sentences to indicate possibility or uncertainty. The verb ἦν is the third person singular imperfect indicative of εἰμί, meaning "it was." The noun ἡ δικαιοσύνη means "righteousness." So this phrase means "indeed, righteousness would be from the law."

Putting it all together, the literal translation of Galatians 3:21 is: "The law, therefore, according to the promises of God? May it never be! For if the law was given, the one who is able to give life, truly righteousness would be from the law."