---
version: 1
---
- **ἐλθούσης δὲ τῆς πίστεως**: The participle ἐλθούσης is the genitive singular feminine form of the verb ἔρχομαι, meaning "to come." It is modifying the noun πίστεως, which is the genitive singular form of πίστις, meaning "faith." The phrase can be translated as "when faith came" or "after faith came."

- **οὐκέτι ὑπὸ παιδαγωγόν**: The adverb οὐκέτι means "no longer" or "not anymore." The preposition ὑπὸ means "under" and it is followed by the accusative case of παιδαγωγός, which means "tutor" or "guardian." The phrase can be translated as "no longer under a tutor" or "not under a guardian anymore."

- **ἐσμεν**: This is the first person plural present indicative form of the verb εἰμί, meaning "to be." The verb form indicates that "we are."

The phrase can be translated as "when faith came, we are no longer under a tutor" or "after faith came, we are not under a guardian anymore."

The entire verse can be translated as: "But now that faith has come, we are no longer under a guardian."