---
version: 1
---
- **πάντες γὰρ υἱοὶ θεοῦ ἐστε**: The word πάντες is the nominative plural of πᾶς, which means "all" or "everyone." The word υἱοὶ is the nominative plural of υἱός, which means "sons." The word θεοῦ is the genitive singular of θεός, which means "God." The verb ἐστε is the second person plural present indicative of εἰμί, meaning "you are." The phrase thus means "you all are sons of God."

- **διὰ τῆς πίστεως**: The word διὰ is a preposition meaning "through" or "because of." The article τῆς is the genitive singular feminine definite article, meaning "the." The noun πίστεως is the genitive singular of πίστις, which means "faith" or "belief." The phrase thus means "through the faith" or "because of the faith."

- **ἐν Χριστῷ Ἰησοῦ**: The preposition ἐν means "in" or "with." The article Χριστῷ is the dative singular masculine definite article, meaning "the." The noun Χριστός is the dative singular of Χριστός, which means "Christ." The article Ἰησοῦ is the genitive singular masculine definite article, meaning "of the." The noun Ἰησοῦς is the genitive singular of Ἰησοῦς, which means "Jesus." The phrase thus means "in Christ Jesus."

The entire verse can be translated as: "For you all are sons of God through the faith in Christ Jesus."