---
version: 1
---
- **πάντας τιμήσατε**: The word πάντας is the accusative plural of πᾶς, which means "all" or "everyone." The verb τιμήσατε is the second person plural aorist imperative of τιμάω, meaning "to honor" or "to value." The phrase thus means "Honor everyone."
- **τὴν ἀδελφότητα ἀγαπᾶτε**: The word τὴν ἀδελφότητα is the accusative singular of ἀδελφότης, which means "brotherhood" or "fellowship." The verb ἀγαπᾶτε is the second person plural present imperative of ἀγαπάω, meaning "to love." The phrase thus means "Love the brotherhood."
- **τὸν θεὸν φοβεῖσθε**: The word τὸν θεὸν is the accusative singular of θεός, which means "God." The verb φοβεῖσθε is the second person plural present imperative middle/passive of φοβέομαι, meaning "to fear" or "to reverence." The phrase thus means "Fear God."
- **τὸν βασιλέα τιμᾶτε**: The word τὸν βασιλέα is the accusative singular of βασιλεύς, which means "king." The verb τιμᾶτε is the second person plural present imperative of τιμάω, meaning "to honor" or "to value." The phrase thus means "Honor the king."

Literal translation: "Honor everyone, love the brotherhood, fear God, honor the king."

In this verse, the author of 1 Peter is giving instructions to the readers on how to live in society. They are told to honor all people, show love to fellow believers, fear God, and honor the king. This passage emphasizes the importance of respecting and valuing others, both within the Christian community and in the broader society.