---
version: 1
---
- **τοὺς ἐν δυνάμει θεοῦ φρουρουμένους**: The article τοὺς is the accusative plural form of the definite article ὁ, meaning "the." The preposition ἐν means "in" and takes the dative case, so δυνάμει is the dative singular of δύναμις, which means "power." The genitive θεοῦ is the genitive singular of θεός, meaning "God." The present participle φρουρουμένους is the accusative plural masculine form of φρουρέω, meaning "to guard" or "to protect." So the phrase means "those who are being guarded by the power of God."

- **διὰ πίστεως**: The preposition διὰ means "through" or "by means of" and takes the genitive case, so πίστεως is the genitive singular of πίστις, meaning "faith." The phrase means "through faith."

- **εἰς σωτηρίαν ἑτοίμην**: The preposition εἰς means "for" or "to" and takes the accusative case, so σωτηρίαν is the accusative singular of σωτηρία, meaning "salvation." The adjective ἑτοίμην is the accusative singular feminine form of ἑτοῖμος, meaning "ready" or "prepared." So the phrase means "for ready salvation."

- **ἀποκαλυφθῆναι ἐν καιρῷ ἐσχάτῳ**: The infinitive ἀποκαλυφθῆναι is the aorist infinitive of ἀποκαλύπτω, meaning "to be revealed" or "to be disclosed." The preposition ἐν means "in" and takes the dative case, so καιρῷ is the dative singular of καιρός, meaning "time." The adjective ἐσχάτῳ is the dative singular masculine form of ἔσχατος, meaning "last" or "final." So the phrase means "to be revealed in the last time."

The literal translation of 1 Peter 1:5 is:
"those who are being guarded by the power of God through faith for ready salvation to be revealed in the last time."