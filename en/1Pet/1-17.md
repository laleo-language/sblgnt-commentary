---
version: 1
---
- **Καὶ εἰ πατέρα ἐπικαλεῖσθε**: The word Καὶ is a conjunction meaning "and." The word εἰ is a conjunction meaning "if." The noun πατέρα is the accusative singular of πατήρ, which means "father." The verb ἐπικαλεῖσθε is the second person plural present middle imperative of ἐπικαλέομαι, which means "to call upon" or "to invoke." The phrase thus means "And if you call upon the Father."

- **τὸν ἀπροσωπολήμπτως κρίνοντα**: The article τὸν is the accusative singular of ὁ, which means "the." The adjective ἀπροσωπολήμπτως is an adverb derived from the noun ἀπροσωπολήμπτος, which means "impartial" or "unbiased." The participle κρίνοντα is the accusative singular masculine present active participle of κρίνω, which means "to judge." The phrase can be translated as "the impartial judge."

- **κατὰ τὸ ἑκάστου ἔργον**: The preposition κατὰ means "according to" or "in accordance with." The article τὸ is the accusative singular of ὁ, which means "the." The pronoun ἑκάστου is the genitive singular of ἑκάστος, which means "each" or "every." The noun ἔργον is the accusative singular of ἔργον, which means "work" or "deed." The phrase means "according to each one's work."

- **ἐν φόβῳ τὸν τῆς παροικίας ὑμῶν χρόνον**: The preposition ἐν means "in" or "with." The noun φόβῳ is the dative singular of φόβος, which means "fear" or "reverence." The article τὸν is the accusative singular of ὁ, which means "the." The genitive τῆς is the genitive singular of ὁ, which means "the." The noun παροικίας is the genitive singular of παροικία, which means "sojourn" or "temporary residence." The pronoun ὑμῶν is the genitive plural of σύ, which means "you." The noun χρόνον is the accusative singular of χρόνος, which means "time" or "period." The phrase can be translated as "in the fear of your temporary residence."

- **ἀναστράφητε**: The verb ἀναστράφητε is the second person plural aorist passive imperative of ἀναστρέφω, which means "to conduct oneself" or "to behave." The phrase means "you should conduct yourselves."

The entire verse can be translated as: "And if you call upon the Father, the impartial judge, according to each one's work, conduct yourselves in the fear of your temporary residence."