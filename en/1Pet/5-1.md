---
version: 1
---
- **Πρεσβυτέρους** is the accusative plural of πρεσβύτερος, which means "elder" or "older person."
- **οὖν** is an adverb meaning "therefore" or "so."
- **ἐν ὑμῖν** is a prepositional phrase meaning "among you."
- **παρακαλῶ** is the first person singular present indicative of παρακαλέω, which means "I exhort" or "I urge."
- **ὁ συμπρεσβύτερος** means "the fellow elder."
- **καὶ μάρτυς** means "and witness."
- **τῶν τοῦ Χριστοῦ παθημάτων** is a genitive phrase meaning "of the sufferings of Christ."
- **ὁ καὶ τῆς μελλούσης ἀποκαλύπτεσθαι δόξης κοινωνός** means "the one who will also share in the future glory to be revealed."
  
Literal translation: "Therefore, I urge the elders among you, the fellow elder and witness of the sufferings of Christ, the one who will also share in the future glory to be revealed."