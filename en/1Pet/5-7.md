---
version: 1
---
- **πᾶσαν τὴν μέριμναν ὑμῶν**: The word πᾶσαν is the accusative singular feminine form of πᾶς, which means "all" or "every." The article τὴν is the accusative singular feminine form of ὁ, which means "the." The noun μέριμναν means "care" or "anxiety," and it is in the accusative case to show that it is the direct object of the verb. The pronoun ὑμῶν is the genitive plural form of σύ, which means "your." The phrase thus means "all your care" or "all your anxiety."

- **ἐπιρίψαντες ἐπʼ αὐτόν**: The word ἐπιρίψαντες is the nominative plural masculine form of ἐπιρίπτω, which means "to cast upon" or "to throw upon." The participle ἐπιρίψαντες is in the nominative case to agree with the implied subject of the verb. The preposition ἐπʼ means "upon" or "on." The pronoun αὐτόν is the accusative singular masculine form of αὐτός, which means "him" or "it." The phrase thus means "casting upon him" or "throwing upon him."

- **ὅτι αὐτῷ μέλει περὶ ὑμῶν**: The word ὅτι is a conjunction that introduces a subordinate clause and can mean "that" or "because." The pronoun αὐτῷ is the dative singular masculine form of αὐτός, which means "to him" or "for him." The verb μέλει is the third person singular present indicative of μέλω, which means "to care" or "to be concerned." The preposition περὶ means "about" or "concerning." The pronoun ὑμῶν is the genitive plural form of σύ, which means "your." The phrase thus means "because he cares about you."

Literal translation: "Casting all your care upon him, because he cares about you."

This verse from 1 Peter 5:7 encourages the readers to cast all their anxieties upon God, knowing that He cares for them. The phrase "πᾶσαν τὴν μέριμναν ὑμῶν" emphasizes the idea of surrendering all worries and concerns to God, and the phrase "ὅτι αὐτῷ μέλει περὶ ὑμῶν" reassures the readers that God genuinely cares for them.