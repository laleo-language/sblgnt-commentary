---
version: 1
---
- **ὃ**: This is a relative pronoun in the nominative neuter singular form, meaning "which" or "what." It refers back to something mentioned earlier in the discourse.

- **καὶ**: This is a coordinating conjunction meaning "and."

- **ὑμᾶς**: This is the accusative plural form of the pronoun ὑμεῖς, meaning "you."

- **ἀντίτυπον**: This is the accusative singular form of the noun ἀντίτυπος, meaning "antitype" or "counterpart."

- **νῦν**: This is an adverb meaning "now."

- **σῴζει**: This is the third person singular present indicative form of the verb σῴζω, meaning "he saves" or "he delivers."

- **βάπτισμα**: This is the accusative singular form of the noun βάπτισμα, meaning "baptism."

- **οὐ**: This is an adverb meaning "not."

- **σαρκὸς**: This is the genitive singular form of the noun σάρξ, meaning "flesh."

- **ἀπόθεσις**: This is the nominative singular form of the noun ἀπόθεσις, meaning "removal" or "putting off."

- **ῥύπου**: This is the genitive singular form of the noun ῥύπος, meaning "dirt" or "pollution."

- **ἀλλὰ**: This is a coordinating conjunction meaning "but."

- **συνειδήσεως**: This is the genitive singular form of the noun συνείδησις, meaning "conscience."

- **ἀγαθῆς**: This is the genitive singular feminine form of the adjective ἀγαθός, meaning "good."

- **ἐπερώτημα**: This is the accusative singular form of the noun ἐπερώτημα, meaning "inquiry" or "appeal."

- **εἰς**: This is a preposition meaning "for" or "toward."

- **θεόν**: This is the accusative singular form of the noun θεός, meaning "God."

- **διʼ**: This is a preposition meaning "through."

- **ἀναστάσεως**: This is the genitive singular form of the noun ἀνάστασις, meaning "resurrection."

- **Ἰησοῦ**: This is the genitive singular form of the noun Ἰησοῦς, meaning "Jesus."

- **Χριστοῦ**: This is the genitive singular form of the noun Χριστός, meaning "Christ."

The literal translation of the verse is: "which also now saves you, baptism, not a removal of dirt but an appeal to God for a good conscience, through the resurrection of Jesus Christ."