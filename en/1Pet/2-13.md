---
version: 1
---
- **Ὑποτάγητε πάσῃ ἀνθρωπίνῃ κτίσει**: The verb Ὑποτάγητε is the second person plural aorist imperative of ὑποτάσσω, which means "to submit" or "to subject oneself." The noun πάσῃ is the dative singular of πᾶς, meaning "all" or "every." The noun ἀνθρωπίνῃ is the dative singular of ἀνθρώπινος, which means "human" or "humanly." The noun κτίσει is the dative singular of κτίσις, which means "creation" or "creature." The phrase thus means "Submit yourselves to every human creation."

- **διὰ τὸν κύριον**: The preposition διὰ means "through" or "because of." The article τὸν is the accusative singular masculine of ὁ, which means "the." The noun κύριον is the accusative singular masculine of κύριος, which means "lord" or "master." The phrase can be translated as "because of the Lord" or "for the sake of the Lord."

- **εἴτε βασιλεῖ ὡς ὑπερέχοντι**: The conjunction εἴτε means "whether" or "either." The noun βασιλεῖ is the dative singular masculine of βασιλεύς, which means "king" or "ruler." The adverb ὡς means "as" or "like." The participle ὑπερέχοντι is the dative singular masculine present active participle of ὑπερέχω, which means "to excel" or "to surpass." The phrase can be translated as "whether to a king as one who excels."

- The entire verse can be translated as "Submit yourselves to every human creation because of the Lord, whether to a king as one who excels."