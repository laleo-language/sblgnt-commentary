---
version: 1
---
- **ἡ γὰρ ἁμαρτία ἀφορμὴν λαβοῦσα**: The article ἡ is the nominative singular feminine form of ὁ, which means "the." The noun ἁμαρτία is the nominative singular form of ἁμαρτία, meaning "sin." The participle ἀφορμὴν is the accusative singular feminine form of ἀφορμή, which means "occasion" or "opportunity." The participle λαβοῦσα is the nominative singular feminine form of λαμβάνω, meaning "having taken" or "having obtained." The phrase thus means "for sin, having obtained an opportunity."

- **διὰ τῆς ἐντολῆς**: The preposition διὰ means "through" or "by means of." The article τῆς is the genitive singular feminine form of ὁ, which means "the." The noun ἐντολῆς is the genitive singular form of ἐντολή, meaning "command" or "instruction." The phrase thus means "through the command."

- **ἐξηπάτησέν με**: The verb ἐξηπάτησέν is the third person singular aorist indicative active form of ἐξαπατάω, meaning "deceived" or "tricked." The pronoun με is the accusative singular form of ἐγώ, meaning "me." The phrase thus means "it deceived me."

- **καὶ διʼ αὐτῆς ἀπέκτεινεν**: The conjunction καὶ means "and." The preposition διʼ means "through" or "by means of." The pronoun αὐτῆς is the genitive singular feminine form of αὐτός, meaning "itself" or "her." The verb ἀπέκτεινεν is the third person singular aorist indicative active form of ἀποκτείνω, meaning "killed" or "put to death." The phrase thus means "and it put to death through itself."

The literal translation of the verse is: "For sin, having obtained an opportunity through the command, deceived me and put me to death through itself."