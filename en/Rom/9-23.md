---
version: 1
---
- **καὶ ἵνα γνωρίσῃ**: The conjunction καὶ means "and" and introduces a new phrase. The verb γνωρίσῃ is the third person singular aorist subjunctive of γνωρίζω, meaning "he/she might make known" or "he/she might reveal." The phrase thus means "and so that he might make known."

- **τὸν πλοῦτον τῆς δόξης αὐτοῦ**: The article τὸν is the accusative singular masculine form of ὁ, meaning "the." The noun πλοῦτον is the accusative singular of πλοῦτος, which means "wealth" or "abundance." The genitive τῆς δόξης is the genitive singular of δόξα, meaning "glory." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, meaning "his" or "of him." The phrase thus means "the wealth of his glory."

- **ἐπὶ σκεύη ἐλέους**: The preposition ἐπὶ means "upon" or "on." The noun σκεύη is the accusative plural of σκεῦος, which means "vessel" or "container." The genitive ἐλέους is the genitive singular of ἔλεος, meaning "mercy" or "compassion." The phrase thus means "upon vessels of mercy."

- **ἃ προητοίμασεν εἰς δόξαν**: The relative pronoun ἃ is the accusative plural neuter form of ὅς, meaning "which" or "that." The verb προητοίμασεν is the third person singular aorist indicative of προαιτιάομαι, meaning "he/she prepared beforehand" or "he/she predestined." The preposition εἰς means "for" or "to." The noun δόξαν is the accusative singular of δόξα, meaning "glory." The phrase thus means "which he prepared beforehand for glory."

- The entire verse can be literally translated as: "And so that he might make known the wealth of his glory upon vessels of mercy, which he prepared beforehand for glory."