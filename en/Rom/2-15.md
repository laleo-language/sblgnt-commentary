---
version: 1
---
- **οἵτινες ἐνδείκνυνται**: The word οἵτινες is a relative pronoun that means "who" or "which." It is in the nominative plural form. The verb ἐνδείκνυνται is the third person plural present indicative middle/passive of ἐνδείκνυμι, which means "to show" or "to demonstrate." So this phrase means "who demonstrate."

- **τὸ ἔργον τοῦ νόμου γραπτὸν**: The article τὸ specifies the noun ἔργον, which means "work" or "deed." The genitive τοῦ νόμου indicates that this work belongs to the law. The adjective γραπτὸν is the accusative singular neuter form of γραπτός, which means "written." So this phrase means "the written work of the law."

- **ἐν ταῖς καρδίαις αὐτῶν**: The preposition ἐν means "in." The article ταῖς specifies the noun καρδίαις, which means "hearts." The genitive αὐτῶν indicates that these hearts belong to someone else. So this phrase means "in their hearts."

- **συμμαρτυρούσης αὐτῶν τῆς συνειδήσεως**: The verb συμμαρτυρούσης is the genitive singular feminine form of συμμαρτυρέω, which means "to bear witness together." The genitive αὐτῶν indicates that this witness belongs to them. The noun συνείδησις means "conscience" and is in the genitive singular form. So this phrase means "their conscience bearing witness together."

- **καὶ μεταξὺ ἀλλήλων τῶν λογισμῶν κατηγορούντων ἢ καὶ ἀπολογουμένων**: The conjunction καὶ means "and." The adverb μεταξὺ means "among" or "between." The preposition ἀλλήλων means "one another." The genitive τῶν λογισμῶν specifies the noun κατηγορούντων, which is the genitive plural participle present active form of κατηγορέω, meaning "to accuse." The conjunction ἢ means "or." The verb ἀπολογουμένων is the genitive plural participle present middle/passive form of ἀπολογέομαι, which means "to defend oneself." So this phrase means "and among themselves, accusing or defending one another."

- The literal translation of the entire verse is: "who demonstrate the written work of the law in their hearts, their conscience bearing witness together, and among themselves, accusing or defending one another."