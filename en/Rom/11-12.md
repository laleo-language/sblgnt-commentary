---
version: 1
---
- **εἰ δὲ τὸ παράπτωμα αὐτῶν**: The word εἰ is a conjunction meaning "if." The word δὲ is a conjunction meaning "but" or "and." The article τὸ is the neuter singular nominative of the definite article, and it has no equivalent in English. The noun παράπτωμα is the neuter singular nominative of παράπτωμα, which means "transgression" or "trespass." The possessive pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "but their transgression."

- **πλοῦτος κόσμου**: The noun πλοῦτος is the genitive singular of πλοῦτος, which means "wealth" or "abundance." The noun κόσμος is the genitive singular of κόσμος, meaning "world" or "universe." The phrase thus means "wealth of the world."

- **καὶ τὸ ἥττημα αὐτῶν**: The conjunction καὶ means "and." The article τὸ is the neuter singular nominative of the definite article. The noun ἥττημα is the neuter singular nominative of ἥττημα, which means "defeat" or "loss." The possessive pronoun αὐτῶν is the genitive plural of αὐτός. The phrase thus means "and their defeat."

- **πλοῦτος ἐθνῶν**: The noun πλοῦτος is the genitive singular of πλοῦτος. The noun ἐθνῶν is the genitive plural of ἔθνος, meaning "Gentiles" or "nations." The phrase thus means "wealth of the Gentiles."

- **πόσῳ μᾶλλον τὸ πλήρωμα αὐτῶν**: The adverb πόσῳ means "how much" or "to what extent." The adverb μᾶλλον means "more" or "rather." The definite article τὸ is the neuter singular nominative of the definite article. The noun πλήρωμα is the nominative singular of πλήρωμα, meaning "fullness" or "completion." The possessive pronoun αὐτῶν is the genitive plural of αὐτός. The phrase thus means "how much more their fullness."

The phrase "εἰ δὲ τὸ παράπτωμα αὐτῶν πλοῦτος κόσμου καὶ τὸ ἥττημα αὐτῶν πλοῦτος ἐθνῶν, πόσῳ μᾶλλον τὸ πλήρωμα αὐτῶν" can be translated as "but their transgression means riches for the world, and their defeat means riches for the Gentiles, how much more their fullness."

This verse highlights the concept that the transgression and defeat of the Jewish people have resulted in blessings and riches for the world and the Gentiles. It emphasizes the abundance and completeness that will come from the Jewish people in the future.