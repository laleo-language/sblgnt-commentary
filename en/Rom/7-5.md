---
version: 1
---
- **ὅτε γὰρ ἦμεν ἐν τῇ σαρκί**: The word ὅτε is a conjunction meaning "when." The word γὰρ is a conjunction meaning "for" or "because." The verb ἦμεν is the first person plural imperfect indicative of εἰμί, meaning "we were." The preposition ἐν is used with the dative case, so τῇ σαρκί is the dative singular of σάρξ, meaning "in the flesh." The phrase thus means "when we were in the flesh."

- **τὰ παθήματα τῶν ἁμαρτιῶν**: The article τὰ is the nominative/accusative neuter plural form of ὁ, meaning "the." The noun παθήματα is the nominative/accusative neuter plural form of πάθημα, meaning "passions" or "afflictions." The genitive plural τῶν ἁμαρτιῶν is used to show possession, so it means "of sins." The phrase thus means "the passions of sins."

- **τὰ διὰ τοῦ νόμου**: The article τὰ is the nominative/accusative neuter plural form of ὁ, meaning "the." The preposition διὰ is used with the genitive case, so τοῦ νόμου is the genitive singular of νόμος, meaning "the law." The phrase thus means "the things through the law."

- **ἐνηργεῖτο ἐν τοῖς μέλεσιν ἡμῶν**: The verb ἐνηργεῖτο is the third person singular imperfect indicative middle/passive of ἐνεργέω, meaning "it was working" or "it was active." The preposition ἐν is used with the dative case, so τοῖς μέλεσιν is the dative plural of μέλος, meaning "in the members" or "in our bodies." The genitive plural ἡμῶν is used to show possession, so it means "of us." The phrase thus means "it was working in our bodies."

- **εἰς τὸ καρποφορῆσαι τῷ θανάτῳ**: The preposition εἰς is used with the accusative case, so τὸ καρποφορῆσαι is the accusative singular of the articular infinitive καρποφορέω, meaning "to bear fruit" or "to produce fruit." The dative singular τῷ θανάτῳ is used to show purpose, so it means "for death." The phrase thus means "to bear fruit for death."

- The literal translation of the entire verse is: "For when we were in the flesh, the passions of sins that were through the law were active in our bodies, producing fruit for death."