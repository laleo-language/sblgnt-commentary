---
version: 1
---
- **οἶδα καὶ πέπεισμαι ἐν κυρίῳ Ἰησοῦ**: The verb οἶδα is the first person singular perfect indicative of εἴδω, meaning "I know." The verb πέπεισμαι is the first person singular perfect indicative middle/passive of πείθω, meaning "I am convinced" or "I have confidence." The phrase ἐν κυρίῳ Ἰησοῦ means "in the Lord Jesus."

- **ὅτι οὐδὲν κοινὸν διʼ ἑαυτοῦ**: The word ὅτι is a conjunction meaning "that." The adjective οὐδὲν is the accusative neuter singular of οὐδείς, meaning "nothing." The adjective κοινὸν is the accusative neuter singular of κοινός, meaning "common" or "unclean." The preposition διʼ means "through" or "by means of." The reflexive pronoun ἑαυτοῦ means "itself" or "himself." The phrase means "that nothing is unclean in itself."

- **εἰ μὴ τῷ λογιζομένῳ τι κοινὸν εἶναι**: The conjunction εἰ means "if." The verb λογιζομένῳ is the dative singular present middle participle of λογίζομαι, meaning "considering" or "thinking." The pronoun τι is the accusative neuter singular of τις, meaning "something." The adjective κοινὸν is the accusative neuter singular of κοινός. The verb εἶναι is the present infinitive of εἰμί, meaning "to be." The phrase means "if someone considers something unclean."

- **ἐκείνῳ κοινόν**: The pronoun ἐκείνῳ means "to that one" or "to him." The adjective κοινόν is the accusative neuter singular of κοινός. The phrase means "to him it is unclean."

The literal translation of Romans 14:14 is: "I know and am convinced in the Lord Jesus that nothing is unclean in itself; but to the one who considers something unclean, to him it is unclean."