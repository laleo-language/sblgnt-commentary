---
version: 1
---
- **νόμος δὲ**: The noun νόμος means "law" and is in the nominative singular. The conjunction δὲ is used to indicate a contrast or continuation, often translated as "but" or "and." The phrase thus means "but the law."
- **παρεισῆλθεν**: The verb παρεισέρχομαι is in the third person singular aorist indicative. It is a compound verb formed by adding the preposition παρα- ("alongside") to the verb εἰσέρχομαι ("to enter"). The phrase means "entered alongside."
- **ἵνα πλεονάσῃ τὸ παράπτωμα**: The conjunction ἵνα introduces a purpose clause and is often translated as "in order that" or "so that." The verb πλεονάζω is in the third person singular aorist subjunctive, meaning "to increase" or "to abound." The noun παράπτωμα means "transgression" or "trespass" and is in the accusative singular. The phrase means "so that the transgression might increase."
- **οὗ δὲ ἐπλεόνασεν ἡ ἁμαρτία**: The relative pronoun οὗ is the genitive singular of ὅς and means "of which" or "where." The verb ἐπλεόναζω is in the third person singular aorist indicative. The noun ἁμαρτία means "sin" and is in the nominative singular. The phrase means "where sin increased."
- **ὑπερεπερίσσευσεν ἡ χάρις**: The verb ὑπερπερισσεύω is in the third person singular aorist indicative. It is a compound verb formed by adding the preposition ὑπέρ ("beyond") to the verb περισσεύω ("to abound"). The noun χάρις means "grace" and is in the nominative singular. The phrase means "grace abounded beyond measure."

Literal translation: "But the law entered alongside so that the transgression might increase; where sin increased, grace abounded beyond measure."

In this verse, Paul is contrasting the law with grace. He explains that the purpose of the law was to make transgressions more evident, but where sin increased, grace surpassed it. This highlights the idea that God's grace is greater than any sin or transgression.