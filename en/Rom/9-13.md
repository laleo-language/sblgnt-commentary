---
version: 1
---
- **καθὼς γέγραπται**: The word καθὼς is a conjunction meaning "just as" or "as." The verb γέγραπται is the third person singular perfect indicative of γράφω, meaning "it has been written." The phrase καθὼς γέγραπται means "just as it has been written."

- **Τὸν Ἰακὼβ ἠγάπησα**: The word Τὸν is the accusative singular article meaning "the." The name Ἰακὼβ is the accusative singular of Ἰάκωβος, which is the Greek form of Jacob. The verb ἠγάπησα is the first person singular aorist indicative of ἀγαπάω, meaning "I loved." The phrase Τὸν Ἰακὼβ ἠγάπησα means "I loved Jacob."

- **τὸν δὲ Ἠσαῦ ἐμίσησα**: The word τὸν is the accusative singular article meaning "the." The name Ἠσαῦ is the accusative singular of Ἠσαῦ, which is the Greek form of Esau. The verb ἐμίσησα is the first person singular aorist indicative of μισέω, meaning "I hated." The phrase τὸν δὲ Ἠσαῦ ἐμίσησα means "I hated Esau."

The literal translation of the verse is: "Just as it has been written: 'I loved Jacob, but I hated Esau.'"