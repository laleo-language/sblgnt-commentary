---
version: 1
---
- **ἐάν τε γὰρ ζῶμεν**: The word ἐάν is a conjunction meaning "if." The word τε is a particle that adds emphasis and means "both." The verb ζῶμεν is the first person plural present subjunctive of ζάω, which means "we live." The phrase thus means "if indeed we live."

- **τῷ κυρίῳ ζῶμεν**: The article τῷ is the dative singular of ὁ, which means "the." The noun κυρίῳ is the dative singular of κύριος, meaning "Lord." The verb ζῶμεν is the same as before. The phrase means "we live to the Lord."

- **ἐάν τε ἀποθνῄσκωμεν**: The verb ἀποθνῄσκωμεν is the first person plural present subjunctive of ἀποθνῄσκω, meaning "we die." The phrase means "if indeed we die."

- **τῷ κυρίῳ ἀποθνῄσκομεν**: The same as before, but with the verb ἀποθνῄσκομεν, meaning "we die to the Lord."

- **ἐάν τε οὖν ζῶμεν**: The particle οὖν adds a sense of conclusion or consequence and means "therefore." The phrase means "if therefore we live."

- **ἐάν τε ἀποθνῄσκωμεν**: The same as before, meaning "if we die."

- **τοῦ κυρίου ἐσμέν**: The article τοῦ is the genitive singular of ὁ, meaning "the." The noun κυρίου is the genitive singular of κύριος, meaning "Lord." The verb ἐσμέν is the first person plural present indicative of εἰμί, meaning "we are." The phrase means "we are of the Lord."

Literal translation: "If indeed we live, we live to the Lord; if indeed we die, we die to the Lord. Therefore, if we live or if we die, we are of the Lord."