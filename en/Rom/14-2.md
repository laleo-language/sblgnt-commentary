---
version: 1
---
- **ὃς μὲν πιστεύει φαγεῖν πάντα**: The relative pronoun ὃς is the nominative singular masculine form, meaning "who" or "he who." The verb πιστεύει is the third person singular present indicative of πιστεύω, meaning "he believes." The infinitive φαγεῖν is the aorist infinitive of ἐσθίω, meaning "to eat." The noun πάντα is the accusative neuter plural of πᾶς, meaning "everything" or "all." So this phrase can be translated as "he who believes to eat everything."

- **ὁ δὲ ἀσθενῶν λάχανα ἐσθίει**: The definite article ὁ is the nominative singular masculine, meaning "the." The verb ἀσθενῶν is the present participle of ἀσθενέω, meaning "he who is weak" or "the weak one." The noun λάχανα is the accusative neuter plural of λάχανον, meaning "vegetables" or "greens." The verb ἐσθίει is the third person singular present indicative of ἐσθίω, meaning "he eats." So this phrase can be translated as "the weak one eats vegetables."

- **ὃς μὲν πιστεύει φαγεῖν πάντα, ὁ δὲ ἀσθενῶν λάχανα ἐσθίει**: This verse can be translated as "One person believes he may eat everything, but the weak person eats only vegetables."

The syntax of this sentence is not particularly ambiguous. The main structure is a contrast between the two clauses introduced by μὲν...δὲ (on one hand...on the other hand). The first clause describes the belief of one person, while the second clause describes the action of the weak person.