---
version: 1
---
- **οὐκ ἔστιν φόβος θεοῦ**: The word οὐκ is the negative particle "not." The verb ἔστιν is the third person singular present indicative of εἰμί, meaning "is." The noun φόβος is the nominative singular of φόβος, which means "fear." The noun θεοῦ is the genitive singular of θεός, meaning "of God." The phrase thus means "there is no fear of God."

- **ἀπέναντι τῶν ὀφθαλμῶν αὐτῶν**: The preposition ἀπέναντι means "in front of" or "before." The definite article τῶν is the genitive plural of ὁ, meaning "the." The noun ὀφθαλμῶν is the genitive plural of ὀφθαλμός, which means "eyes." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "in front of their eyes."

- **οὐκ ἔστιν φόβος θεοῦ ἀπέναντι τῶν ὀφθαλμῶν αὐτῶν**: The entire verse translates to "There is no fear of God before their eyes."

The syntax of this sentence is not ambiguous, and the meaning is clear. It is stating that there is a lack of fear or reverence toward God in the presence of these individuals.