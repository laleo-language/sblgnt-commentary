---
version: 1
---
- **Πρῶτον μὲν**: The word Πρῶτον is an adverb meaning "first." The particle μὲν is a common Greek word indicating contrast or comparison. The phrase thus means "first of all."

- **εὐχαριστῶ τῷ θεῷ μου**: The verb εὐχαριστῶ is the first person singular present indicative of εὐχαριστέω, which means "I give thanks." The noun τῷ θεῷ is in the dative case, indicating the recipient of the thanks, and it means "to God." The pronoun μου is the genitive singular of ἐγώ, meaning "my." The phrase thus means "I give thanks to my God."

- **διὰ Ἰησοῦ Χριστοῦ**: The preposition διὰ is often translated as "through" or "by means of." The word Ἰησοῦ is the genitive singular of Ἰησοῦς, meaning "Jesus." The word Χριστοῦ is the genitive singular of Χριστός, meaning "Christ." The phrase can be translated as "through Jesus Christ."

- **⸀περὶ πάντων ὑμῶν**: The word περὶ is a preposition meaning "concerning" or "about." The word πάντων is the genitive plural of πᾶς, meaning "all" or "everyone." The pronoun ὑμῶν is the genitive plural of σύ, meaning "you." The phrase can be translated as "concerning all of you."

- **ὅτι ἡ πίστις ὑμῶν καταγγέλλεται ἐν ὅλῳ τῷ κόσμῳ**: The conjunction ὅτι introduces a subordinate clause and is often translated as "that." The article ἡ is the nominative singular feminine form of ὁ, meaning "the." The noun πίστις is in the nominative singular, meaning "faith." The pronoun ὑμῶν is the genitive plural of σύ, meaning "your." The verb καταγγέλλεται is the third person singular present indicative passive of καταγγέλλω, meaning "it is proclaimed" or "it is declared." The preposition ἐν is often translated as "in." The article τῷ is the dative singular masculine form of ὁ, meaning "the." The noun κόσμῳ is in the dative singular, meaning "world." The phrase can be translated as "that your faith is proclaimed in the whole world."

The literal translation of the entire verse is: "First of all, I give thanks to my God through Jesus Christ concerning all of you, that your faith is proclaimed in the whole world."