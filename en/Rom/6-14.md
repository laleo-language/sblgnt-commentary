---
version: 1
---
- **ἁμαρτία γὰρ ὑμῶν**: The noun ἁμαρτία is the nominative singular of ἁμαρτία, which means "sin." The word γὰρ is a conjunction that means "for" or "because." The pronoun ὑμῶν is the genitive plural of the second person pronoun σύ, meaning "of you." The phrase thus means "for your sin" or "because of your sin."

- **οὐ κυριεύσει**: The negative particle οὐ negates the verb κυριεύσει, which is the third person singular future indicative of κυριεύω, meaning "to have dominion" or "to rule." The phrase means "it will not have dominion" or "it will not rule."

- **οὐ γάρ ἐστε ὑπὸ νόμον**: The negative particle οὐ negates the verb ἐστε, which is the second person plural present indicative of εἰμί, meaning "you are." The word γάρ is a conjunction that means "for" or "because." The pronoun ὑπὸ is a preposition that means "under." The noun νόμον is the accusative singular of νόμος, which means "law." The phrase means "for you are not under the law" or "because you are not under the law."

- **ἀλλὰ ὑπὸ χάριν**: The conjunction ἀλλὰ means "but" or "rather." The preposition ὑπὸ means "under." The noun χάριν is the accusative singular of χάρις, which means "grace." The phrase means "but under grace."

The entire verse can be translated as: "For sin will not have dominion over you, for you are not under the law but under grace."

In this verse, Paul is addressing the believers in Rome and assuring them that sin will not have control or power over them because they are no longer under the law. Instead, they are under the grace of God. This highlights the transformative power of God's grace in the lives of believers, freeing them from the bondage of sin and the condemnation of the law.