---
version: 1
---
- **μαρτυρῶ γὰρ αὐτοῖς**: The verb μαρτυρῶ is the first person singular present indicative of μαρτυρέω, meaning "I testify" or "I bear witness." The pronoun αὐτοῖς is the dative plural of αὐτός, meaning "to them." The phrase thus means "I testify to them."

- **ὅτι ζῆλον θεοῦ ἔχουσιν**: The conjunction ὅτι introduces a clause and is often translated as "that." The noun ζῆλον is the accusative singular of ζῆλος, meaning "zeal" or "passion." The genitive θεοῦ indicates possession and means "of God." The verb ἔχουσιν is the third person plural present indicative of ἔχω, meaning "they have." The phrase can be translated as "that they have the zeal of God."

- **ἀλλʼ οὐ κατʼ ἐπίγνωσιν**: The conjunction ἀλλʼ is often translated as "but" and indicates a contrast. The adverb οὐ negates the following verb. The preposition κατὰ is often translated as "according to." The noun ἐπίγνωσις is the accusative singular of ἐπίγνωσις, meaning "knowledge" or "recognition." The phrase means "but not according to knowledge."

- The literal translation of the entire verse is: "For I testify to them that they have the zeal of God, but not according to knowledge."

In this verse, Paul is explaining that the people he is referring to have a strong passion for God, but their understanding or knowledge of God is lacking.