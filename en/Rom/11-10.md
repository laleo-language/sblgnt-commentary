---
version: 1
---
- **σκοτισθήτωσαν οἱ ὀφθαλμοὶ αὐτῶν**: The verb σκοτισθήτωσαν is the third person plural aorist passive subjunctive of σκοτίζω, meaning "let their eyes be darkened." The noun ὀφθαλμοὶ is the nominative plural of ὀφθαλμός, which means "eyes." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "let their eyes be darkened."

- **τοῦ μὴ βλέπειν**: The genitive article τοῦ indicates possession and is translated as "of." The negative particle μὴ is used with the verb βλέπειν, which is the present infinitive of βλέπω, meaning "to see." The phrase thus means "of not seeing."

- **καὶ τὸν νῶτον αὐτῶν**: The conjunction καὶ connects this phrase to the previous one. The article τὸν is the accusative singular of ὁ, which means "the." The noun νῶτον is the accusative singular of νῶτος, meaning "back." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "and their back."

- **διὰ παντὸς σύγκαμψον**: The preposition διὰ is used with the accusative case and can mean "through" or "by means of." The adverb παντὸς is the genitive singular of πᾶς, meaning "all" or "every." The verb σύγκαμψον is the second person singular aorist imperative of συγκάμπτω, meaning "bend" or "bow." The phrase thus means "bend through all."

The literal translation of Romans 11:10 is: "Let their eyes be darkened so that they do not see, and bend their back through all."