---
version: 1
---
- **εἰ δυνατόν**: The word εἰ is a particle that introduces a conditional statement and can be translated as "if." The adjective δυνατόν is the neuter singular of δυνατός, meaning "possible" or "able." The phrase thus means "if possible."

- **τὸ ἐξ ὑμῶν**: The article τὸ is the neuter singular accusative of ὁ, meaning "the." The preposition ἐξ means "out of" or "from." The pronoun ὑμῶν is the genitive plural of σύ, meaning "your." The phrase can be translated as "that which is from you" or "your part."

- **μετὰ πάντων ἀνθρώπων εἰρηνεύοντες**: The preposition μετὰ means "with." The adjective πάντων is the masculine plural genitive of πᾶς, meaning "all" or "every." The noun ἀνθρώπων is the genitive plural of ἄνθρωπος, meaning "people" or "men." The present participle εἰρηνεύοντες is derived from the verb εἰρηνεύω, meaning "to be at peace" or "to make peace." The phrase can be translated as "being at peace with all people."

The entire verse, Rom 12:18, can be translated as: "If possible, as far as it depends on you, be at peace with all people."