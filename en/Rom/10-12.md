---
version: 1
---
- **οὐ γάρ ἐστιν διαστολὴ**: The word οὐ is the negative particle, meaning "not." The word γάρ is a conjunction, meaning "for" or "because." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The noun διαστολὴ is the nominative singular of διαστολή, which means "distinction" or "difference." The phrase thus means "for there is no distinction."

- **Ἰουδαίου τε καὶ Ἕλληνος**: The word Ἰουδαίου is the genitive singular of Ἰουδαῖος, which means "Jew." The word τε is a conjunction, meaning "and." The word καὶ is also a conjunction, meaning "and." The word Ἕλληνος is the genitive singular of Ἕλλην, which means "Greek." The phrase thus means "of Jew and Greek."

- **ὁ γὰρ αὐτὸς κύριος πάντων**: The article ὁ is the definite article, meaning "the." The word γὰρ is a conjunction, meaning "for" or "because." The word αὐτὸς is a pronoun, meaning "he himself." The noun κύριος is the nominative singular of κύριος, which means "Lord." The adjective πάντων is the genitive plural of πᾶς, which means "all" or "every." The phrase thus means "for the same Lord of all."

- **πλουτῶν εἰς πάντας τοὺς ἐπικαλουμένους αὐτόν**: The verb πλουτῶν is the present active participle of πλουτέω, meaning "being rich" or "abounding." The preposition εἰς means "into" or "towards." The adjective πάντας is the accusative plural of πᾶς, meaning "all" or "every." The article τοὺς is the definite article, meaning "the." The verb ἐπικαλουμένους is the present middle participle of ἐπικαλέομαι, meaning "calling upon" or "invoking." The pronoun αὐτόν is the accusative singular of αὐτός, meaning "him." The phrase thus means "abounding towards all those who call upon him."

The literal translation of Romans 10:12 is: "For there is no distinction between Jew and Greek, for the same Lord of all is rich towards all who call upon him."