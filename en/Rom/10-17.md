---
version: 1
---
- **ἄρα ἡ πίστις ἐξ ἀκοῆς**: The word ἄρα is a conjunction that can be translated as "therefore" or "so." The noun πίστις is the nominative singular of πίστις, which means "faith" or "belief." The preposition ἐξ is followed by the genitive case, and it means "from" or "out of." The noun ἀκοῆς is the genitive singular of ἀκοή, which means "hearing" or "auditory perception." The phrase can be translated as "so faith comes from hearing."

- **ἡ δὲ ἀκοὴ διὰ ῥήματος Χριστοῦ**: The definite article ἡ indicates that ἀκοὴ is a specific hearing being referred to. The conjunction δὲ can be translated as "but" or "and." The noun ἀκοὴ is the nominative singular, and it is repeated from the previous phrase. The preposition διὰ is followed by the genitive case, and it means "through" or "by means of." The noun ῥῆμα is the genitive singular of ῥῆμα, which means "word" or "message." The genitive Χριστοῦ indicates possession and means "of Christ." The phrase can be translated as "but hearing through the word of Christ."

Literal translation: "Therefore, faith comes from hearing, and hearing through the word of Christ."

In this verse, Paul is emphasizing the importance of hearing the word of Christ as the source of faith. He argues that faith is not a result of one's own effort or understanding, but it comes from hearing the message of Christ.