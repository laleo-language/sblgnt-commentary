---
version: 1
---
- **δικαιούμενοι**: This is the present passive participle of the verb δικαιόω, meaning "to justify." The form here is masculine plural nominative, so it refers to a group of people who are being justified. The literal translation is "being justified."

- **δωρεὰν**: This is an adverb meaning "freely" or "as a gift." It modifies the verb δικαιούμενοι and emphasizes that the justification is not earned or deserved. The literal translation is "freely."

- **τῇ αὐτοῦ χάριτι**: The word τῇ is the dative singular feminine article, indicating that it is modifying a feminine noun. The noun here is χάρις, meaning "grace." The pronoun αὐτοῦ is a genitive singular masculine pronoun, meaning "his." So together, this phrase means "by his grace" or "through his grace." The literal translation is "by his grace."

- **διὰ τῆς ἀπολυτρώσεως**: The word διὰ is a preposition meaning "through" or "by means of." The noun ἀπολύτρωσις means "redemption" or "ransom." The form here is genitive singular, indicating possession or source. So this phrase means "through the redemption" or "by means of the redemption." The literal translation is "through the redemption."

- **τῆς ἐν Χριστῷ Ἰησοῦ**: The word τῆς is the genitive singular feminine article, modifying the following noun. The noun here is ἐν Χριστῷ Ἰησοῦ, which means "in Christ Jesus." This phrase indicates the location or sphere of the redemption. The literal translation is "of the one in Christ Jesus."

Putting it all together, the literal translation of the entire verse is: "being justified freely by his grace through the redemption that is in Christ Jesus."