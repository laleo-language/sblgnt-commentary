---
version: 1
---
- **τί οὖν;**: The word τί is the accusative singular of τίς, which means "what." The word οὖν is a particle that can be translated as "therefore" or "so." The phrase τί οὖν can be translated as "So what?"

- **ὃ ἐπιζητεῖ Ἰσραήλ**: The word ὃ is the accusative singular of ὅς, which means "which" or "what." The verb ἐπιζητεῖ is the third person singular present indicative of ἐπιζητέω, meaning "to seek" or "to desire." The noun Ἰσραήλ is the nominative singular of Ἰσραήλ, which means "Israel." The phrase ὃ ἐπιζητεῖ Ἰσραήλ can be translated as "what Israel seeks" or "what Israel desires."

- **τοῦτο οὐκ ἐπέτυχεν**: The word τοῦτο is the accusative singular of οὗτος, which means "this." The word οὐκ is the negative particle "not." The verb ἐπέτυχεν is the third person singular aorist indicative of ἐπιτυγχάνω, meaning "to obtain" or "to succeed." The phrase τοῦτο οὐκ ἐπέτυχεν can be translated as "this did not obtain" or "this did not succeed."

- **ἡ δὲ ἐκλογὴ ἐπέτυχεν**: The article ἡ is the nominative singular feminine definite article "the." The noun ἐκλογή is the nominative singular of ἐκλογή, which means "election" or "choice." The verb ἐπέτυχεν is the third person singular aorist indicative of ἐπιτυγχάνω. The phrase ἡ δὲ ἐκλογὴ ἐπέτυχεν can be translated as "the election succeeded" or "the choice obtained."

- **οἱ δὲ λοιποὶ ἐπωρώθησαν**: The article οἱ is the nominative plural masculine definite article "the." The adjective λοιποὶ is the nominative plural masculine form of λοιπός, which means "the rest" or "the remaining." The verb ἐπωρώθησαν is the third person plural aorist passive indicative of πωρόω, meaning "to harden" or "to make callous." The phrase οἱ δὲ λοιποὶ ἐπωρώθησαν can be translated as "but the rest were hardened" or "but the remaining were made callous."

- The phrase **τί οὖν;** can be translated as "So what?" or "What then?"
- The phrase **ὃ ἐπιζητεῖ Ἰσραήλ** can be translated as "what Israel seeks" or "what Israel desires."
- The phrase **τοῦτο οὐκ ἐπέτυχεν** can be translated as "this did not obtain" or "this did not succeed."
- The phrase **ἡ δὲ ἐκλογὴ ἐπέτυχεν** can be translated as "the election succeeded" or "the choice obtained."
- The phrase **οἱ δὲ λοιποὶ ἐπωρώθησαν** can be translated as "but the rest were hardened" or "but the remaining were made callous."

The literal translation of Romans 11:7 is: "So what? What Israel seeks, this did not obtain, but the election obtained; the rest were hardened."