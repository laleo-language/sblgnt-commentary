---
version: 1
---
- **εἰ δὲ Χριστὸς ἐν ὑμῖν**: The word εἰ is a conjunction meaning "if." The word δὲ is a conjunction meaning "but" or "and." The noun Χριστὸς is the nominative singular of Χριστός, which means "Christ." The preposition ἐν means "in." The pronoun ὑμῖν is the dative plural of σύ, meaning "you." The phrase thus means "but if Christ is in you."

- **τὸ μὲν σῶμα νεκρὸν διὰ ἁμαρτίαν**: The article τὸ is the neuter nominative singular of ὁ, meaning "the." The noun σῶμα is the nominative singular of σῶμα, which means "body." The adjective νεκρὸν is the accusative singular of νεκρός, meaning "dead." The preposition διὰ means "through" or "because of." The noun ἁμαρτίαν is the accusative singular of ἁμαρτία, meaning "sin." The phrase thus means "the body is dead because of sin."

- **τὸ δὲ πνεῦμα ζωὴ διὰ δικαιοσύνην**: The article τὸ is the neuter nominative singular of ὁ, meaning "the." The noun πνεῦμα is the nominative singular of πνεῦμα, which means "spirit." The noun ζωὴ is the nominative singular of ζωή, meaning "life." The preposition διὰ means "through" or "because of." The noun δικαιοσύνην is the accusative singular of δικαιοσύνη, meaning "righteousness." The phrase thus means "but the spirit is life because of righteousness."

The verse can be literally translated as: "But if Christ is in you, the body is dead because of sin, but the spirit is life because of righteousness."