---
version: 1
---
- **Διὸ**: This is a conjunction that means "therefore" or "for this reason."
- **προσλαμβάνεσθε**: This is a verb in the present middle or passive indicative second person plural of προσλαμβάνω, which means "to receive" or "to welcome." The middle or passive voice indicates that the action is being done to you or for your benefit. The present tense indicates that the action is happening currently or regularly. The phrase means "you are receiving" or "you are welcoming."
- **ἀλλήλους**: This is a pronoun in the accusative plural form of ἀλλήλων, which means "one another" or "each other." It indicates reciprocity or mutual action. The accusative case indicates that the action is being done to the pronoun. The phrase means "one another" or "each other."
- **καθὼς**: This is a conjunction that means "just as" or "in the same way."
- **καὶ**: This is a conjunction that means "and."
- **ὁ Χριστὸς**: This is the noun Χριστός in the nominative singular form, which means "Christ" or "the Messiah." The definite article ὁ indicates that it is a specific Christ being referred to.
- **προσελάβετο**: This is a verb in the aorist indicative third person singular form of προσλαμβάνω, which means "to receive" or "to welcome." The aorist tense indicates a past action that is viewed as a whole. The third person singular form indicates that Christ is the one who received or welcomed. The phrase means "he received" or "he welcomed."
- **ὑμᾶς**: This is a pronoun in the accusative plural form of ὑμεῖς, which means "you" (plural). The accusative case indicates that the action is being done to the pronoun. The phrase means "you."
- **εἰς δόξαν**: This is a prepositional phrase with the preposition εἰς, which means "into" or "for the purpose of," and the noun δόξα, which means "glory" or "honor." The phrase means "into glory" or "for the purpose of glory."
- **τοῦ θεοῦ**: This is the noun θεός in the genitive singular form, which means "God." The genitive case indicates possession or relationship. The phrase means "of God."

Literal translation: "Therefore, you are receiving one another, just as Christ received you into glory of God."

The verse is not syntactically ambiguous.