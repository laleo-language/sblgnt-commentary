---
version: 1
---
- **καὶ γινώσκεις τὸ θέλημα**: The word καὶ is a conjunction meaning "and." The verb γινώσκεις is the second person singular present indicative of γινώσκω, meaning "you know." The noun τὸ θέλημα is the accusative singular of θέλημα, which means "will" or "desire." The phrase thus means "and you know the will."

- **καὶ δοκιμάζεις τὰ διαφέροντα**: The conjunction καὶ means "and." The verb δοκιμάζεις is the second person singular present indicative of δοκιμάζω, meaning "you test" or "you examine." The article τὰ is the accusative plural neuter definite article, and the noun διαφέροντα is the accusative plural neuter of διαφέρω, which means "differences" or "variations." The phrase thus means "and you test the differences."

- **κατηχούμενος ἐκ τοῦ νόμου**: The participle κατηχούμενος is the nominative singular masculine present passive of κατηχέω, meaning "being instructed" or "being taught." The preposition ἐκ means "from" or "out of." The article τοῦ is the genitive singular masculine definite article, and the noun νόμου is the genitive singular masculine of νόμος, which means "law." The phrase thus means "being instructed from the law."

In this verse, the syntax is clear and unambiguous. The phrases can be translated as follows: "and you know the will, and you test the differences, being instructed from the law."

The literal translation of the entire verse would be: "And you know the will and test the differences, being instructed from the law."