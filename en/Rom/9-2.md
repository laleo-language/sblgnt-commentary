---
version: 1
---
- **ὅτι λύπη μοί ἐστιν μεγάλη**: The word λύπη is the nominative singular of λύπη, which means "sorrow" or "grief." The pronoun μοί is the dative singular form of ἐγώ, meaning "to me" or "for me." The verb ἐστιν is the third person singular present indicative of εἰμί, meaning "is." The phrase thus means "for me there is great sorrow."

- **καὶ ἀδιάλειπτος ὀδύνη**: The word ἀδιάλειπτος is an adjective meaning "unceasing" or "continuous." The noun ὀδύνη is the nominative singular of ὀδύνη, which means "pain" or "anguish." The phrase means "and continuous pain."

- **τῇ καρδίᾳ μου**: The article τῇ is the dative singular feminine form of ὁ, meaning "the." The noun καρδίᾳ is the dative singular of καρδία, which means "heart." The pronoun μου is the genitive singular form of ἐγώ, meaning "my." The phrase means "to my heart."

Putting it all together, the verse can be translated as: "For me there is great sorrow and continuous pain to my heart."

This verse does not contain any ambiguous syntax.