---
version: 1
---
- **ἐν τῇ ἀνοχῇ τοῦ θεοῦ**: The preposition ἐν means "in" and the noun τῇ ἀνοχῇ is the dative singular of ἀνοχή, which means "tolerance" or "forbearance." The genitive τοῦ θεοῦ means "of God." The phrase can be translated as "in the tolerance of God."

- **πρὸς τὴν ἔνδειξιν τῆς δικαιοσύνης αὐτοῦ**: The preposition πρὸς means "for" or "to." The article τὴν is the accusative singular feminine form, indicating that it is modifying the noun ἔνδειξιν. The noun ἔνδειξις means "demonstration" or "proof." The genitive τῆς δικαιοσύνης means "of righteousness." The pronoun αὐτοῦ is the genitive singular masculine form of αὐτός, meaning "his" or "his own." The phrase can be translated as "for the demonstration of his righteousness."

- **ἐν τῷ νῦν καιρῷ**: The preposition ἐν means "in" and the article τῷ is the dative singular masculine form, indicating that it is modifying the noun νῦν. The noun νῦν means "now." The noun καιρός means "time" or "occasion." The phrase can be translated as "in the present time."

- **εἰς τὸ εἶναι αὐτὸν δίκαιον καὶ δικαιοῦντα τὸν ἐκ πίστεως Ἰησοῦ**: The preposition εἰς means "in order to" or "for the purpose of." The article τὸ is the accusative singular neuter form, indicating that it is modifying the infinitive εἶναι. The infinitive εἶναι is the present active infinitive of εἰμί, meaning "to be." The pronoun αὐτὸν is the accusative singular masculine form of αὐτός, meaning "him." The adjective δίκαιον means "righteous." The participle δικαιοῦντα is the accusative singular masculine form of the present active participle δικαιοῦν, meaning "justifying." The article τὸν is the accusative singular masculine form, indicating that it is modifying the noun ἐκ πίστεως. The preposition ἐκ means "from" or "out of." The noun πίστις means "faith" or "belief." The phrase can be translated as "for the purpose of him being righteous and justifying the one who is from faith."

The entire verse can be translated as: "in the tolerance of God, for the demonstration of his righteousness at the present time, in order to be righteous and to justify the one who is from faith."