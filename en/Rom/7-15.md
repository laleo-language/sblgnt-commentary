---
version: 1
---
- **ὃ γὰρ κατεργάζομαι οὐ γινώσκω**: The word ὃ is the neuter singular accusative of ὅς, ἥ, ὅ, which means "what" or "that which." The verb κατεργάζομαι is the present middle indicative, first person singular of κατεργάζομαι, meaning "I work" or "I accomplish." The verb γινώσκω is the present indicative, first person singular of γινώσκω, meaning "I know" or "I understand." The phrase thus means "I do not understand what I am working."

- **οὐ γὰρ ὃ θέλω τοῦτο πράσσω**: The word οὐ is the negation particle meaning "not." The verb θέλω is the present indicative, first person singular of θέλω, meaning "I want" or "I desire." The demonstrative pronoun τοῦτο is the accusative singular neuter of οὗτος, αὕτη, τοῦτο, meaning "this." The verb πράσσω is the present indicative, first person singular of πράσσω, meaning "I do" or "I carry out." The phrase thus means "For what I want, I do not carry out this."

- **ἀλλʼ ὃ μισῶ τοῦτο ποιῶ**: The word ἀλλʼ is a conjunction meaning "but" or "however." The pronoun ὃ is the neuter singular accusative of ὅς, ἥ, ὅ, meaning "what" or "that which." The verb μισῶ is the present indicative, first person singular of μισέω, meaning "I hate" or "I detest." The demonstrative pronoun τοῦτο is the accusative singular neuter of οὗτος, αὕτη, τοῦτο, meaning "this." The verb ποιῶ is the present indicative, first person singular of ποιέω, meaning "I do" or "I make." The phrase thus means "But what I hate, I do this."

The literal translation of Romans 7:15 is: "For what I am working, I do not understand; for what I want, I do not carry out; but what I hate, I do."