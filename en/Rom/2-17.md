---
version: 1
---
- **Εἰ δὲ**: The word εἰ is a conjunction meaning "if." The word δὲ is a conjunction meaning "but" or "and." The phrase εἰ δὲ together means "but if" or "and if."

- **σὺ Ἰουδαῖος**: The word σὺ is the second person singular pronoun meaning "you." The word Ἰουδαῖος is the nominative singular form of Ἰουδαῖος, which means "Jew." The phrase σὺ Ἰουδαῖος means "you are a Jew."

- **ἐπονομάζῃ**: The verb ἐπονομάζῃ is the second person singular present subjunctive of ἐπονομάζω, which means "to be named" or "to be called." The phrase ἐπονομάζῃ means "you are called" or "you are named."

- **καὶ ἐπαναπαύῃ**: The word καὶ is a conjunction meaning "and." The verb ἐπαναπαύῃ is the second person singular present subjunctive of ἐπαναπαύω, which means "to rest" or "to take a break." The phrase καὶ ἐπαναπαύῃ means "and you rest" or "and you take a break."

- **νόμῳ**: The word νόμῳ is the dative singular form of νόμος, which means "law." The phrase νόμῳ means "by the law" or "according to the law."

- **καὶ καυχᾶσαι**: The word καὶ is a conjunction meaning "and." The verb καυχᾶσαι is the aorist infinitive of καυχάομαι, which means "to boast" or "to brag." The phrase καὶ καυχᾶσαι means "and to boast" or "and to brag."

- **ἐν θεῷ**: The word ἐν is a preposition meaning "in" or "by." The word θεῷ is the dative singular form of θεός, which means "God." The phrase ἐν θεῷ means "in God" or "by God."

The literal translation of the entire verse is: "But if you are called a Jew and rest in the law and boast in God."