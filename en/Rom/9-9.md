---
version: 1
---
- **ἐπαγγελίας γὰρ ὁ λόγος οὗτος**: The noun ἐπαγγελίας is the genitive singular of ἐπαγγελία, which means "promise." The article ὁ indicates that it is the subject of the sentence. The noun λόγος is the nominative singular of λόγος, which means "word" or "message." The demonstrative pronoun οὗτος means "this." The phrase could be translated as "For this is the promise."

- **Κατὰ τὸν καιρὸν τοῦτον ἐλεύσομαι**: The preposition Κατὰ means "according to" or "at." The article τὸν indicates the accusative case, and the noun καιρὸν is the accusative singular of καιρός, which means "time." The demonstrative pronoun τοῦτον means "this." The verb ἐλεύσομαι is the first person singular future indicative of ἔρχομαι, which means "I will come." The phrase could be translated as "At this time I will come."

- **καὶ ἔσται τῇ Σάρρᾳ υἱός**: The conjunction καὶ means "and." The verb ἔσται is the third person singular future indicative of εἰμί, which means "he/she/it will be." The dative article τῇ indicates the dative case, and the noun Σάρρᾳ is the dative singular of Σάρρα, which is the name "Sarah." The noun υἱός is the nominative singular of υἱός, which means "son." The phrase could be translated as "And there will be a son to Sarah."

Literal translation: "For this is the promise: At this time I will come, and there will be a son to Sarah."