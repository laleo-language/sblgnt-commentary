---
version: 1
---
- **Τίς γὰρ ἔγνω νοῦν κυρίου;**: The word Τίς is the nominative singular of τίς, which means "who" or "what." The verb ἔγνω is the third person singular aorist indicative of γινώσκω, meaning "he/she/it knew" or "he/she/it understood." The noun νοῦν is the accusative singular of νοῦς, which means "mind" or "understanding." The genitive κυρίου means "of the Lord." The phrase thus means "Who knew the mind of the Lord?"

- **ἢ τίς σύμβουλος αὐτοῦ ἐγένετο;**: The word ἢ is a conjunction that means "or." The word τίς is the nominative singular of τίς, which means "who" or "what." The noun σύμβουλος is the nominative singular of σύμβουλος, which means "counselor" or "advisor." The genitive αὐτοῦ means "of him." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "he/she/it became" or "he/she/it was." The phrase thus means "Or who became his counselor?"

The literal translation of the verse is: "For who knew the mind of the Lord? Or who became his counselor?"