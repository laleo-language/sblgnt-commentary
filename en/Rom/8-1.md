---
version: 1
---
- **Οὐδὲν ἄρα** is a phrase consisting of two words. Οὐδὲν is the nominative neuter singular of οὐδείς, meaning "nothing." ἄρα is a particle that can be translated as "therefore" or "so." The phrase Οὐδὲν ἄρα together means "therefore nothing."

- **νῦν** is an adverb meaning "now."

- **κατάκριμα** is the nominative neuter singular of κατάκριμα, which means "condemnation."

- **τοῖς ἐν Χριστῷ Ἰησοῦ** is a prepositional phrase consisting of three words. Τοῖς is the dative plural of the definite article ὁ, meaning "the." ἐν is a preposition meaning "in." Χριστῷ is the dative singular of Χριστός, meaning "Christ." Ἰησοῦ is the genitive singular of Ἰησοῦς, meaning "Jesus." The phrase τοῖς ἐν Χριστῷ Ἰησοῦ together means "to those in Christ Jesus."

- The symbol ⸀ before Ἰησοῦ indicates that there is a textual variant here. In this case, it means that some manuscripts include the word, while others omit it. 

Putting it all together, the literal translation of the verse is: "Therefore nothing now condemnation to those in Christ Jesus."