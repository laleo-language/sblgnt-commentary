---
version: 1
---
- **ὃς ἐν νόμῳ καυχᾶσαι**: The word ὃς is the nominative singular of ὅς, which means "who" or "which." The preposition ἐν means "in" and is followed by the dative case. The noun νόμῳ is the dative singular of νόμος, which means "law." The verb καυχᾶσαι is the present infinitive of καυχάομαι, which means "to boast" or "to glory." The phrase thus means "who boast in the law."

- **διὰ τῆς παραβάσεως τοῦ νόμου**: The preposition διὰ means "through" or "because of" and is followed by the genitive case. The noun παραβάσεως is the genitive singular of παράβασις, which means "transgression" or "violation." The article τῆς is the genitive singular feminine article, equivalent to "of the." The noun νόμου is the genitive singular of νόμος, which means "law." The phrase thus means "because of the transgression of the law."

- **τὸν θεὸν ἀτιμάζεις**: The article τὸν is the accusative singular masculine article, equivalent to "the." The noun θεὸν is the accusative singular of θεός, which means "God." The verb ἀτιμάζεις is the second person singular present indicative of ἀτιμάζω, which means "you dishonor" or "you disrespect." The phrase thus means "you dishonor God."

The sentence can be translated as: "Who boast in the law, through the transgression of the law, you dishonor God?"

This verse is not syntactically ambiguous.