---
version: 1
---
- **εἰ δὲ ἀπεθάνομεν σὺν Χριστῷ**: The word εἰ is a conjunction meaning "if." The word δὲ is a conjunction meaning "but" or "and." The verb ἀπεθάνομεν is the first person plural aorist indicative of ἀποθνῄσκω, meaning "we died." The preposition σὺν means "with." The noun Χριστῷ is the dative singular of Χριστός, meaning "Christ." The phrase thus means "but if we died with Christ."

- **πιστεύομεν ὅτι καὶ συζήσομεν αὐτῷ**: The verb πιστεύομεν is the first person plural present indicative of πιστεύω, meaning "we believe." The conjunction ὅτι means "that." The verb συζήσομεν is the first person plural future indicative of συζάω, meaning "we will live together." The pronoun αὐτῷ is the dative singular of αὐτός, meaning "to him." The phrase thus means "we believe that we will also live with him."

The literal translation of Romans 6:8 is: "But if we died with Christ, we believe that we will also live with him."