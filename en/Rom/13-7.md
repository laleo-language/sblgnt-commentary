---
version: 1
---
- **ἀπόδοτε πᾶσι τὰς ὀφειλάς**: The verb ἀπόδοτε is the second person plural imperative of ἀποδίδωμι, meaning "give back" or "pay." The noun πᾶσι is the dative plural of πᾶς, which means "all" or "everyone." The noun ὀφειλάς is the accusative plural of ὀφειλή, which means "debt" or "obligation." The phrase thus means "Give back/pay all the debts to everyone."

- **τῷ τὸν φόρον τὸν φόρον**: The article τῷ is the dative singular of the definite article ὁ, which means "the." The noun φόρον is the accusative singular of φόρος, which means "tax" or "tribute." The phrase is repeated for emphasis and means "to the tax, the tax."

- **τῷ τὸ τέλος τὸ τέλος**: The definite article τῷ is repeated for emphasis. The noun τέλος is the accusative singular of τέλος, which means "custom" or "duty." The phrase is repeated for emphasis and means "to the custom, the custom."

- **τῷ τὸν φόβον τὸν φόβον**: The definite article τῷ is repeated for emphasis. The noun φόβον is the accusative singular of φόβος, which means "fear" or "respect." The phrase is repeated for emphasis and means "to the fear, the fear."

- **τῷ τὴν τιμὴν τὴν τιμήν**: The definite article τῷ is repeated for emphasis. The noun τιμὴν is the accusative singular of τιμή, which means "honor" or "respect." The phrase is repeated for emphasis and means "to the honor, the honor."

The entire verse can be translated as follows: "Give back/pay all the debts to everyone, to the tax, the tax, to the custom, the custom, to the fear, the fear, to the honor, the honor."