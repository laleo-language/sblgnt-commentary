---
version: 1
---
- **Τί οὖν ἐροῦμεν**: The word Τί is an interrogative pronoun meaning "what." The verb ἐροῦμεν is the first person plural future indicative of λέγω, meaning "we will say" or "we will speak." The phrase thus means "What then will we say?"

- **εὑρηκέναι Ἀβραὰμ τὸν προπάτορα ἡμῶν κατὰ σάρκα**: The word εὑρηκέναι is the aorist infinitive of εὑρίσκω, meaning "to find" or "to discover." The name Ἀβραὰμ is declined in the accusative case, singular number. The phrase τὸν προπάτορα ἡμῶν means "our forefather." The preposition κατὰ means "according to" and is followed by the accusative case. The noun σάρξ means "flesh." The phrase thus means "to have found Abraham our forefather according to the flesh."

- The syntax of the sentence is not ambiguous.

Literal translation: "What then will we say? Have we found Abraham our forefather according to the flesh?"