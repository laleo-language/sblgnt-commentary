---
version: 1
---
- **οἱ γὰρ ἄρχοντες**: The word οἱ is the nominative plural definite article, meaning "the." The noun ἄρχοντες is the nominative plural of ἄρχων, which means "rulers" or "authorities." The phrase means "the rulers" or "the authorities."

- **οὐκ εἰσὶν φόβος**: The word οὐκ is the negative particle, meaning "not." The verb εἰσὶν is the third person plural present indicative of εἰμί, meaning "they are." The noun φόβος is the nominative singular of φόβος, which means "fear" or "terror." The phrase means "they are not fear" or "they do not inspire fear."

- **τῷ ἀγαθῷ ἔργῳ**: The article τῷ is the dative singular definite article, meaning "to the." The adjective ἀγαθῷ is the dative singular of ἀγαθός, which means "good." The noun ἔργῳ is the dative singular of ἔργον, which means "work" or "deed." The phrase means "to the good work" or "to the good deed."

- **ἀλλὰ τῷ κακῷ**: The conjunction ἀλλὰ means "but" or "instead." The article τῷ is the dative singular definite article, meaning "to the." The adjective κακῷ is the dative singular of κακός, which means "evil" or "bad." The phrase means "but to the evil" or "instead to the evil."

- **θέλεις δὲ μὴ φοβεῖσθαι τὴν ἐξουσίαν**: The verb θέλεις is the second person singular present indicative of θέλω, meaning "you want" or "you desire." The particle δὲ is a conjunction, meaning "but" or "and." The particle μὴ is a negative particle, meaning "not." The verb φοβεῖσθαι is the present middle infinitive of φοβέω, meaning "to fear." The article τὴν is the accusative singular definite article, meaning "the." The noun ἐξουσίαν is the accusative singular of ἐξουσία, which means "authority" or "power." The phrase means "but do you not want to fear the authority" or "but do you not want to be afraid of the authority."

- **τὸ ἀγαθὸν ποίει**: The article τὸ is the accusative singular definite article, meaning "the." The adjective ἀγαθὸν is the accusative singular of ἀγαθός, which means "good." The verb ποίει is the second person singular present imperative of ποιέω, meaning "do" or "make." The phrase means "do the good" or "do what is good."

- **καὶ ἕξεις ἔπαινον ἐξ αὐτῆς**: The conjunction καὶ means "and." The verb ἕξεις is the second person singular future indicative of ἔχω, meaning "you will have." The noun ἔπαινον is the accusative singular of ἔπαινος, which means "praise" or "commendation." The preposition ἐξ means "from" or "out of." The pronoun αὐτῆς is the genitive singular of αὐτός, meaning "it" or "from it." The phrase means "and you will have praise from it" or "and you will receive commendation from it."

The literal translation of the entire verse is:

"For the rulers are not fear to the good work, but to the evil. But do you not want to fear the authority? Do the good, and you will have praise from it."