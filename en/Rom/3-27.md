---
version: 1
---
- **Ποῦ οὖν ἡ καύχησις;**: The word Ποῦ is an interrogative adverb meaning "where." The word οὖν is a conjunction that can be translated as "therefore" or "so." The noun καύχησις is the nominative singular of καύχησις, which means "boasting" or "pride." The phrase could be translated as "Where then is the boasting?"

- **ἐξεκλείσθη**: The verb ἐξεκλείσθη is the third person singular aorist passive indicative of ἐκκλείω, meaning "to shut out" or "to exclude." The phrase thus means "it was shut out" or "it was excluded."

- **διὰ ποίου νόμου;**: The preposition διὰ means "through" or "by means of." The interrogative pronoun ποίου is the genitive singular masculine of ποῖος, which means "which" or "what." The noun νόμου is the genitive singular of νόμος, meaning "law." The phrase could be translated as "through which law?"

- **τῶν ἔργων;**: The article τῶν is the genitive plural of ὁ, which means "the." The noun ἔργων is the genitive plural of ἔργον, meaning "works." The phrase could be translated as "of works?"

- **οὐχί, ἀλλὰ διὰ νόμου πίστεως**: The adverb οὐχί means "not" or "no." The conjunction ἀλλὰ means "but." The preposition διὰ means "through" or "by means of." The noun νόμου is the genitive singular of νόμος, meaning "law." The genitive noun πίστεως is the genitive singular of πίστις, meaning "faith." The phrase could be translated as "not of works, but through the law of faith."

In this verse, Paul is addressing the question of boasting or pride. He asks where boasting is when it comes to the law. He then states that boasting has been excluded, not through the law of works, but through the law of faith. This highlights the importance of faith in the Christian belief system, rather than relying on one's own works.

Literal translation: "Where then is the boasting? It was shut out. Through which law? Of works? No, but through the law of faith."