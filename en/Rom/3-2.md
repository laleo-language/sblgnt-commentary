---
version: 1
---
- **πολὺ κατὰ πάντα τρόπον**: The word πολὺ is an adverb meaning "much" or "very." The word κατὰ is a preposition meaning "according to" or "in every respect." The phrase πολὺ κατὰ πάντα τρόπον means "in many ways" or "in every way."

- **πρῶτον μὲν γὰρ**: The word πρῶτον is an adverb meaning "first." The word μὲν is a particle indicating a contrast or comparison. The word γὰρ is a conjunction meaning "for" or "because." This phrase introduces a list of reasons or explanations.

- **ὅτι ἐπιστεύθησαν τὰ λόγια τοῦ θεοῦ**: The word ὅτι is a conjunction meaning "that." The verb ἐπιστεύθησαν is the third person plural aorist passive indicative of πιστεύω, meaning "they were believed" or "they were trusted." The article τὰ is the neuter plural accusative of ὁ, meaning "the." The noun λόγια is the neuter plural accusative of λόγιον, meaning "words" or "sayings." The genitive τοῦ θεοῦ is the genitive singular of θεός, meaning "of God." The phrase ὅτι ἐπιστεύθησαν τὰ λόγια τοῦ θεοῦ means "because the words of God were believed."

- The literal translation of the entire verse is: "Much in every way. First, because the words of God were believed."