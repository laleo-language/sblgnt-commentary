---
version: 1
---
- **τῇ γὰρ ματαιότητι**: The word τῇ is the dative singular feminine definite article, indicating that it modifies the noun γὰρ. The noun γὰρ means "vanity" or "emptiness". The phrase τῇ γὰρ ματαιότητι means "to the vanity" or "because of the vanity".

- **ἡ κτίσις ὑπετάγη**: The noun ἡ κτίσις means "creation" and is in the nominative singular feminine case. The verb ὑπετάγη is the third person singular aorist indicative passive of ὑποτάσσω, which means "to subject" or "to bring under". The phrase ἡ κτίσις ὑπετάγη means "the creation was subjected".

- **οὐχ ἑκοῦσα ἀλλὰ διὰ τὸν ὑποτάξαντα**: The word οὐχ is a negative particle, meaning "not". The word ἑκοῦσα is the nominative singular feminine participle of ἑκoύσιος, which means "voluntary" or "willing". The verb ἀλλὰ is a coordinating conjunction meaning "but". The preposition διὰ means "because of" or "through". The article τὸν is the accusative singular masculine definite article, modifying the verb ὑποτάξαντα. The verb ὑποτάξαντα is the aorist participle of ὑποτάσσω, meaning "having subjected". The phrase οὐχ ἑκοῦσα ἀλλὰ διὰ τὸν ὑποτάξαντα means "not willingly, but because of the one who subjected".

- **ἐφʼ ἑλπίδι**: The preposition ἐφʼ means "on" or "upon". The noun ἑλπίς means "hope". The phrase ἐφʼ ἑλπίδι means "in hope" or "based on hope".

The literal translation of the entire verse is: "For the creation was subjected to vanity, not willingly, but because of the one who subjected, in hope."