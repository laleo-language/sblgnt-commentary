---
version: 1
---
- **ἀλλὰ λέγω**, μὴ ⸂Ἰσραὴλ οὐκ ἔγνω⸃;: The phrase ἀλλὰ λέγω means "but I say." The word μὴ is a negative particle, which in this sentence negates the verb ἔγνω, meaning "to know." The phrase Ἰσραὴλ οὐκ ἔγνω means "Israel did not know."

- **πρῶτος Μωϋσῆς λέγει**: The phrase πρῶτος Μωϋσῆς means "first Moses." The verb λέγει is the third person singular present indicative of λέγω, meaning "he says." The phrase thus means "first Moses says."

- **Ἐγὼ παραζηλώσω ὑμᾶς** ἐπʼ οὐκ ἔθνει: The phrase Ἐγὼ παραζηλώσω ὑμᾶς means "I will provoke you." The verb παραζηλώσω is the first person singular future indicative of παραζηλόω, meaning "I will provoke." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase ἐπʼ οὐκ ἔθνει means "against a non-nation." The word οὐκ is a negative particle, and ἔθνει is the dative singular of ἔθνος, meaning "nation." The phrase thus means "I will provoke you against a non-nation."

- **ἐπʼ ἔθνει ἀσυνέτῳ παροργιῶ ὑμᾶς**: The phrase ἐπʼ ἔθνει ἀσυνέτῳ means "against a foolish nation." The word ἔθνει is the dative singular of ἔθνος, meaning "nation." The adjective ἀσυνέτῳ is the dative singular masculine form of ἀσύνετος, meaning "foolish" or "unintelligent." The verb παροργιῶ is the first person singular future indicative of παροργίζω, meaning "I will provoke." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase thus means "I will provoke you against a foolish nation."

The literal translation of the verse is: "But I say, did Israel not know? First Moses says, 'I will provoke you against a non-nation, against a foolish nation I will provoke you.'"