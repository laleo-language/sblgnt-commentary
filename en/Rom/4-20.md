---
version: 1
---
- **εἰς δὲ τὴν ἐπαγγελίαν τοῦ θεοῦ**: The preposition εἰς means "to" or "for." The article τὴν indicates that the noun ἐπαγγελίαν is in the accusative case and is a definite noun. The noun ἐπαγγελίαν means "promise" or "pledge." The genitive τοῦ θεοῦ indicates possession, so it means "the promise of God."

- **οὐ διεκρίθη τῇ ἀπιστίᾳ**: The negative particle οὐ indicates negation. The verb διεκρίθη is the third person singular aorist indicative passive of διακρίνω, meaning "to doubt" or "to be divided." The dative article τῇ indicates that the noun ἀπιστίᾳ is in the dative case and is a definite noun. The noun ἀπιστίᾳ means "doubt" or "unbelief." The phrase thus means "he was not divided by doubt."

- **ἀλλὰ ἐνεδυναμώθη τῇ πίστει**: The conjunction ἀλλὰ means "but" or "instead." The verb ἐνεδυναμώθη is the third person singular aorist indicative passive of ἐνδυναμόω, meaning "to strengthen" or "to empower." The dative article τῇ indicates that the noun πίστει is in the dative case and is a definite noun. The noun πίστει means "faith." The phrase thus means "but he was strengthened by faith."

- **δοὺς δόξαν τῷ θεῷ**: The participle δοὺς is the nominative masculine singular present active of δίδωμι, meaning "giving" or "granting." The accusative noun δόξαν means "glory" or "honor." The dative article τῷ indicates that the noun θεῷ is in the dative case and is a definite noun. The noun θεῷ means "God." The phrase thus means "giving glory to God."

The entire verse can be translated as: "But he did not waver in unbelief concerning the promise of God, but was strengthened by faith, giving glory to God."