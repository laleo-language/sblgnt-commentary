---
version: 1
---
- **ἀσπάσασθε τοὺς ἀδελφοὺς πάντας**: The verb ἀσπάσασθε is the second person plural aorist middle imperative of ἀσπάζομαι, which means "to greet" or "to welcome." The noun phrase τοὺς ἀδελφοὺς πάντας consists of the accusative plural of the noun ἀδελφός, meaning "brothers" (can also be translated as "brothers and sisters"), and the adjective πάντας, meaning "all" or "every." So the phrase means "Greet all the brothers (and sisters)."

- **ἐν φιλήματι ἁγίῳ**: The preposition ἐν means "in" or "with." The noun φιλήματι is the dative singular of φίλημα, which means "kiss." The adjective ἁγίῳ means "holy" or "sanctified." So the phrase means "with a holy kiss."

The entire verse can be translated as: "Greet all the brothers (and sisters) with a holy kiss."