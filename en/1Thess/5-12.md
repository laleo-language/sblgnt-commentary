---
version: 1
---
- **Ἐρωτῶμεν δὲ ὑμᾶς**: The verb Ἐρωτῶμεν is the first person plural present indicative of ἐρωτάω, meaning "we ask" or "we request." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase thus means "We ask you."

- **ἀδελφοί**: The noun ἀδελφοί is the nominative plural of ἀδελφός, meaning "brothers" or "brothers and sisters." It is used here as an address to the recipients of the letter, indicating that they are considered part of the same community or family.

- **εἰδέναι τοὺς κοπιῶντας ἐν ὑμῖν**: The verb εἰδέναι is the present infinitive of οἶδα, meaning "to know." The noun phrase τοὺς κοπιῶντας is in the accusative plural and is derived from the verb κοπιάω, meaning "to labor" or "to work hard." The preposition ἐν indicates the location or sphere in which the action takes place, and ὑμῖν is the dative plural of σύ, meaning "to/for you." The phrase thus means "to know those who labor among you."

- **καὶ προϊσταμένους ὑμῶν ἐν κυρίῳ**: The verb προϊσταμένους is the present participle in the accusative plural of προΐστημι, meaning "to lead" or "to preside over." The pronoun ὑμῶν is the genitive plural of σύ, meaning "your." The preposition ἐν indicates the sphere or context in which the action takes place, and κυρίῳ is the dative singular of κύριος, meaning "the Lord." The phrase thus means "and those who lead/preside over you in the Lord."

- **καὶ νουθετοῦντας ὑμᾶς**: The verb νουθετοῦντας is the present participle in the accusative plural of νουθετέω, meaning "to admonish" or "to warn." The pronoun ὑμᾶς is the accusative plural of σύ, meaning "you." The phrase thus means "and those who admonish/warn you."

The literal translation of the entire verse is: "We ask you, brothers, to know those who labor among you and lead/preside over you in the Lord, and those who admonish/warn you."