---
version: 1
---
- **καὶ ἡγεῖσθαι αὐτοὺς ὑπερεκπερισσοῦ ἐν ἀγάπῃ διὰ τὸ ἔργον αὐτῶν**: The word καὶ is a conjunction meaning "and." The verb ἡγεῖσθαι is the present middle/passive infinitive of ἡγέομαι, which means "to lead" or "to consider." The pronoun αὐτοὺς is the accusative plural of αὐτός, meaning "them." The adverb ὑπερεκπερισσοῦ means "beyond measure" or "exceedingly." The preposition ἐν means "in" or "with." The noun ἀγάπῃ is the dative singular of ἀγάπη, which means "love." The preposition διὰ means "through" or "because of." The article τὸ is the accusative singular of ὁ, which means "the." The noun ἔργον is the accusative singular of ἔργον, meaning "work." The pronoun αὐτῶν is the genitive plural of αὐτός, meaning "their." The phrase thus means "and to consider them beyond measure in love because of their work."

- **εἰρηνεύετε ἐν ἑαυτοῖς**: The verb εἰρηνεύετε is the present imperative of εἰρηνεύω, meaning "to live in peace" or "to be at peace with." The preposition ἐν means "in" or "with." The pronoun ἑαυτοῖς is the dative plural of ἑαυτός, meaning "yourselves." The phrase thus means "Live in peace with yourselves."

The entire verse can be translated as: "And consider them beyond measure in love because of their work. Live in peace with yourselves."