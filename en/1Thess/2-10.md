---
version: 1
---
- **ὑμεῖς μάρτυρες**: The pronoun ὑμεῖς is the nominative plural of σύ, meaning "you." The noun μάρτυρες is the nominative plural of μάρτυς, which means "witness." The phrase thus means "you (all) are witnesses."

- **καὶ ὁ θεός**: The conjunction καὶ means "and." The article ὁ is the nominative singular of ὁ, which means "the." The noun θεός is the nominative singular of θεός, which means "God." The phrase thus means "and God."

- **ὡς ὁσίως καὶ δικαίως καὶ ἀμέμπτως**: The adverb ὡς means "as" or "like." The adverbs ὁσίως, δικαίως, and ἀμέμπτως mean "holy," "righteously," and "blamelessly," respectively. The phrase thus means "as holy, righteously, and blamelessly."

- **ὑμῖν τοῖς πιστεύουσιν**: The pronoun ὑμῖν is the dative plural of σύ, meaning "to you." The article τοῖς is the dative plural of ὁ, which means "the." The participle πιστεύουσιν is the present active dative plural of πιστεύω, meaning "believing." The phrase thus means "to you (all) who believe."

- **ἐγενήθημεν**: The verb ἐγενήθημεν is the first person plural aorist indicative of γίνομαι, meaning "we became." The phrase thus means "we became."

The literal translation of 1 Thessalonians 2:10 is: "You are witnesses, and God, as holy, righteously, and blamelessly, we became to you who believe."