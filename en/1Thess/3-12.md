---
version: 1
---
- **ὑμᾶς δὲ**: The word ὑμᾶς is the accusative plural of σύ, which means "you." The conjunction δὲ means "but" or "and." The phrase thus means "but you."

- **ὁ κύριος**: The word ὁ is the definite article, meaning "the." The noun κύριος means "Lord." The phrase means "the Lord."

- **πλεονάσαι καὶ περισσεύσαι**: The verb πλεονάω is the aorist infinitive, meaning "to increase" or "to abound." The verb περισσεύω is the aorist infinitive, meaning "to abound" or "to overflow." The two verbs are connected by the conjunction καὶ, meaning "and." The phrase means "to increase and abound."

- **τῇ ἀγάπῃ**: The definite article τῇ is the dative singular of ὁ, meaning "the." The noun ἀγάπη means "love." The phrase means "in love."

- **εἰς ἀλλήλους καὶ εἰς πάντας**: The preposition εἰς means "into" or "to." The pronoun ἀλλήλους means "one another" or "each other." The conjunction καὶ means "and." The pronoun πάντας means "all" or "everyone." The phrase means "to one another and to everyone."

- **καθάπερ καὶ ἡμεῖς εἰς ὑμᾶς**: The adverb καθάπερ means "just as" or "even as." The conjunction καὶ means "and." The pronoun ἡμεῖς means "we." The preposition εἰς means "into" or "to." The pronoun ὑμᾶς means "you." The phrase means "just as we to you."

The literal translation of the entire verse is: "But you, the Lord, to increase and abound in love to one another and to everyone, just as we to you."