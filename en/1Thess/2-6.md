---
version: 1
---
- **οὔτε ζητοῦντες ἐξ ἀνθρώπων δόξαν**: The word οὔτε is a conjunction that means "neither" or "nor." The participle ζητοῦντες is the present active accusative plural of ζητέω, which means "to seek" or "to look for." The preposition ἐξ takes the genitive case, and in this context, it means "from" or "by means of." The noun ἀνθρώπων is the genitive plural of ἄνθρωπος, which means "man" or "human." The noun δόξαν is the accusative singular of δόξα, which means "glory" or "honor." The phrase can be translated as "nor seeking glory from men."

- **οὔτε ἀφʼ ὑμῶν οὔτε ἀπʼ ἄλλων**: The conjunction οὔτε is used again, meaning "neither" or "nor." The preposition ἀφʼ takes the genitive case and means "from" or "by." The pronoun ὑμῶν is the genitive plural of σύ, which means "you." The preposition ἀπʼ also takes the genitive case and has the same meaning as ἀφʼ. The noun ἄλλων is the genitive plural of ἄλλος, which means "other." The phrase can be translated as "nor from you nor from others."

The entire verse can be literally translated as "neither seeking glory from men, nor from you nor from others."