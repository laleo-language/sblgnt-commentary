---
version: 1
---
- **πιστὸς ὁ καλῶν ὑμᾶς**: The word πιστὸς is the nominative singular masculine form of πιστός, which means "faithful" or "trustworthy." The article ὁ is the definite article, meaning "the." The participle καλῶν is the nominative singular masculine form of καλέω, which means "calling" or "summoning." The pronoun ὑμᾶς is the accusative plural form of ὑμεῖς, which means "you." The phrase can be translated as "the faithful one who is calling you."

- **ὃς καὶ ποιήσει**: The pronoun ὃς is the nominative singular form of ὅς, which means "who" or "he who." The conjunction καὶ means "also" or "even." The verb ποιήσει is the third person singular future indicative form of ποιέω, which means "he will do" or "he will make." The phrase can be translated as "who will also do" or "who will also make."

The entire verse can be translated as "The faithful one who is calling you, he will also do."