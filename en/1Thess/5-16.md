---
version: 1
---
- **πάντοτε**: This is an adverb meaning "always" or "at all times."
- **χαίρετε**: This is the second person plural present imperative of the verb χαίρω, which means "to rejoice" or "to be glad." The imperative form is used to give a command or instruction. So, the phrase means "Rejoice always."

The literal translation of the verse is "Rejoice always."