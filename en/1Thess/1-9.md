---
version: 1
---
- **αὐτοὶ γὰρ**: The word αὐτοὶ is the nominative plural of αὐτός, which means "they" or "they themselves." The word γὰρ is a conjunction that means "for" or "because." The phrase thus means "for they themselves."

- **περὶ ἡμῶν**: The word περὶ is a preposition that means "about" or "concerning." The word ἡμῶν is the genitive plural of ἐγώ, which means "we" or "us." The phrase thus means "about us."

- **ἀπαγγέλλουσιν**: The verb ἀπαγγέλλουσιν is the third person plural present indicative active of ἀπαγγέλλω, which means "they proclaim" or "they announce."

- **ὁποίαν εἴσοδον ἔσχομεν πρὸς ὑμᾶς**: The word ὁποίαν is the accusative singular feminine of ὁποῖος, which means "what kind of" or "what sort of." The word εἴσοδον is the accusative singular of εἴσοδος, which means "entry" or "arrival." The word ἔσχομεν is the first person plural aorist indicative active of ἔχω, which means "we had" or "we obtained." The preposition πρὸς means "to" or "towards." The pronoun ὑμᾶς is the accusative plural of σύ, which means "you" (plural). The phrase thus means "what kind of entry we had to you."

- **καὶ πῶς ἐπεστρέψατε πρὸς τὸν θεὸν**: The word καὶ is a conjunction that means "and." The word πῶς is an adverb that means "how." The verb ἐπεστρέψατε is the second person plural aorist indicative active of ἐπιστρέφω, which means "you turned" or "you returned." The preposition πρὸς means "to" or "towards." The article τὸν is the accusative singular masculine of ὁ, which means "the." The noun θεὸν is the accusative singular masculine of θεός, which means "God." The phrase thus means "and how you turned to God."

- **ἀπὸ τῶν εἰδώλων δουλεύειν θεῷ ζῶντι καὶ ἀληθινῷ**: The preposition ἀπὸ means "from" or "away from." The article τῶν is the genitive plural masculine/neuter of ὁ, which means "the." The noun εἴδωλον is the genitive plural neuter of εἴδωλον, which means "idol" or "image." The verb δουλεύειν is the present infinitive of δουλεύω, which means "to serve" or "to worship." The noun θεῷ is the dative singular masculine of θεός, which means "God." The adjective ζῶντι is the dative singular masculine of ζῶν, which means "living." The adjective ἀληθινῷ is the dative singular masculine of ἀληθινός, which means "true." The phrase thus means "from the idols, to serve the living and true God."

The literal translation of the entire verse is: "For they themselves report about us what kind of entry we had to you, and how you turned to God from the idols to serve the living and true God."