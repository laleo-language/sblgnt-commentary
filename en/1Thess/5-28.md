---
version: 1
---
- **ἡ χάρις τοῦ κυρίου ἡμῶν**: The word χάρις is the nominative singular of χάρις, which means "grace" or "favor." The genitive phrase τοῦ κυρίου ἡμῶν means "of our Lord." So this phrase can be translated as "the grace of our Lord."

- **Ἰησοῦ Χριστοῦ**: The words Ἰησοῦ Χριστοῦ are the genitive form of Ἰησοῦς Χριστός, which means "Jesus Christ." This phrase can be translated as "of Jesus Christ."

- **μεθʼ ὑμῶν**: The word μεθ' is a preposition meaning "with." The pronoun ὑμῶν is the genitive plural of ὑμεῖς, which means "you." So this phrase can be translated as "with you."

The literal translation of the entire verse is: "The grace of our Lord Jesus Christ be with you."