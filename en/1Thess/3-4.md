---
version: 1
---
- **καὶ γὰρ**: The conjunction καὶ means "and" and γὰρ is a coordinating conjunction meaning "for" or "because." Together, they can be translated as "for" or "because also."

- **ὅτε πρὸς ὑμᾶς ἦμεν**: The word ὅτε is a subordinating conjunction meaning "when." The preposition πρὸς means "to" or "toward." The pronoun ὑμᾶς is the accusative plural of the second person pronoun ὑμεῖς, meaning "you." The verb ἦμεν is the first person plural imperfect indicative of εἰμί, meaning "we were." The phrase can be translated as "when we were with you."

- **προελέγομεν ὑμῖν**: The verb προελέγομεν is the first person plural imperfect indicative of προλέγω, meaning "we were foretelling" or "we were predicting." The pronoun ὑμῖν is the dative plural of the second person pronoun ὑμεῖς, meaning "to you." The phrase can be translated as "we were foretelling to you."

- **ὅτι μέλλομεν θλίβεσθαι**: The word ὅτι is a subordinating conjunction meaning "that." The verb μέλλομεν is the first person plural present indicative of μέλλω, meaning "we are about to" or "we will." The verb θλίβεσθαι is the present middle/passive infinitive of θλίβω, meaning "to be afflicted" or "to suffer." The phrase can be translated as "that we are about to suffer."

- **καθὼς καὶ ἐγένετο**: The adverb καθὼς means "just as" or "in the same way." The conjunction καὶ means "also" or "even." The verb ἐγένετο is the third person singular aorist indicative of γίνομαι, meaning "it happened" or "it occurred." The phrase can be translated as "just as it also happened."

- **καὶ οἴδατε**: The conjunction καὶ means "and" or "also." The verb οἴδατε is the second person plural present indicative of οἶδα, meaning "you know." The phrase can be translated as "and you know."

Overall, this verse is expressing that when the speaker was with the recipients, they foretold that they would suffer, just as it happened and as the recipients already know.

Literal translation: "And for when we were with you, we were foretelling to you that we are about to suffer, just as it also happened and you know."