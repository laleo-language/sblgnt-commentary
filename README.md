# SBLGNT Commentary Project

The goal of the SBLGNT Commentary Project is to produce a verse-by-verse
commentary on the grammar and language of the [Society for Biblical Literature](https://www.sbl-site.org/)'s
[Greek New Testament](https://sblgnt.com/), primarily aimed at learners of New
Testament Greek.

This repository will contain A machine-parseable verse-by-verse
commentary.  This will initially be seeded by input from GPT-4, but
the intent is that it be reviewed and updated by experts and learners
as they interact with it.

# License

Copyright questions in relation to generated text are still an open question; at
then time of writing, courts have generally ruled that AI-generated content is
not copyrightable.  All copyright-able content will be under a 
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) license; any contributions 
must be submitted under that license.
